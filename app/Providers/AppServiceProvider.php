<?php

namespace App\Providers;

use App\Contracts\Repository\CompanyGroupEmployeeRepository;
use App\Contracts\Repository\ResetPasswordRepository;
use App\Contracts\Repository\SystemEmailRepository;
use App\Contracts\Repository\UserRepository;
use App\Contracts\Repository\UserSettingRepository;
use App\Contracts\Service\ResetPasswordContact;
use App\Contracts\Service\SystemEmailContact;
use App\Contracts\Service\UserContact;
use App\Repositories\CompanyGroupEmployeeRepositoryEloquent;
use App\Repositories\ResetPasswordRepositoryEloquent;
use App\Repositories\SystemEmailRepositoryEloquent;
use App\Repositories\UserRepositoryEloquent;
use App\Repositories\UserSettingRepositoryEloquent;
use App\Services\ResetPasswordService;
use App\Services\SystemEmailService;
use App\Services\UserService;
use App\User;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // BINDING ALL SERVICE
        $this->app->bind(UserContact::class, UserService::class);
        $this->app->bind(SystemEmailContact::class, SystemEmailService::class);

        // BINDING ALL REPOSITORY
        $this->app->bind(UserRepository::class, UserRepositoryEloquent::class);
        $this->app->bind(UserSettingRepository::class, UserSettingRepositoryEloquent::class);
        $this->app->bind(CompanyGroupEmployeeRepository::class, CompanyGroupEmployeeRepositoryEloquent::class);
        $this->app->bind(SystemEmailRepository::class, SystemEmailRepositoryEloquent::class);
        $this->app->bind(ResetPasswordContact::class, ResetPasswordService::class);
        $this->app->bind(ResetPasswordRepository::class, ResetPasswordRepositoryEloquent::class);

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (env('APP_ENV') != 'local') {
            $this->app['request']->server->set('HTTPS', true);
        }
    }
}
