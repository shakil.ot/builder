<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Modules\Package\Entities\UserBalance;
use Modules\Package\Entities\UserCurrentPackage;

class User extends Authenticatable
{
    use Notifiable;


    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_UNREGISTERED = 2;

    const USER_CREATED_FROM_DIRECT_REGISTRATION = 1;
    const USER_CREATED_FROM_ADMIN = 2;
    const USER_CREATED_FROM_API = 3;

    const COUNTRY_INTERNATIONAL = 1;
    const COUNTRY_BANGLADESH = 2;

    const USER_TYPE_GENERAL = 1;
    const USER_TYPE_TEMPLATE = 2;

    const NO_PACKAGE = 0;
    const PACKAGE_CREATED_FROM_DIRECT_REGISTRATION = 1;
    const PACKAGE_CREATED_FROM_ADMIN = 2;
    const PACKAGE_CREATED_FROM_API = 3;


    const DEMO_USER = 'quicksend-demo@gmail.com';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'email_verified_at',
        'password',
        'parent_id',
        'status',
        'access_token',
        'auth_email',
        'currency',
        'brand_image',
        'phone',
        'facebook_pixel_id',
        'google_pixel_id',
        'journey_mode',
        'journey_mode_last_landing_step',
        'user_created_from',
        'country',
        'ip',
        'user_type',
        'sendfox_credentials',
        'congratulation_mail_set_permission'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function userCurrentPackages()
    {
        return $this->hasOne(UserCurrentPackage::class);
    }

    public function user_balances()
    {
        return $this->hasOne(UserBalance::class);
    }

}
