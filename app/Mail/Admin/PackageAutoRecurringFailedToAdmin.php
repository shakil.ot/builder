<?php

namespace App\Mail\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PackageAutoRecurringFailedToAdmin extends Mailable
{
    use Queueable, SerializesModels;

    private $userId;
    private $packageName;
    private $price;
    private $message;

    /**
     * PackageAutoRecurringFailed constructor.
     * @param $userId
     * @param $packageName
     * @param $price
     * @param $message
     */
    public function __construct($userId, $packageName, $price, $message)
    {
        $this->userId = $userId;
        $this->packageName = $packageName;
        $this->price = $price;
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['userId'] = $this->userId;
        $data['packageName'] = $this->packageName;
        $data['subscriptionFee'] = $this->price;
        $data['message'] = $this->message;

        return $this->subject(env('SITE_NAME') . ' Recurring Failed!')->markdown('emails.admin.auto-recurring-failed')->with($data);
    }
}
