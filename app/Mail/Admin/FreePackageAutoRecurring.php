<?php

namespace App\Mail\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Carbon;

class FreePackageAutoRecurring extends Mailable
{
    use Queueable, SerializesModels;

    private $receiver;
    private $packageDetails;

    /**
     * Create a new message instance.
     *
     * @param $receiver
     * @param $packageDetails
     */
    public function __construct($receiver, $packageDetails)
    {
        $this->receiver = $receiver;
        $this->packageDetails = $packageDetails;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['userName'] = $this->receiver->first_name.' '.$this->receiver->last_name;
        $data['email'] = $this->receiver->email;
        $data['packageName'] = $this->packageDetails->name;
        $data['nextRecurringDate'] = Carbon::today()->addDays((int)$this->packageDetails->life_line);

        return $this->markdown('emails.admin.free-package-auto-recurring')->with($data);
    }
}
