<?php

namespace App\Mail\User;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PackageAutoRecurringFailedToUser extends Mailable
{
    use Queueable, SerializesModels;

    private $user;
    private $packageName;
    private $subscriptionFee;
    private $message;
    /**
     * @var null
     */
    private $body;
    private $error;
    private $cardInfo;

    /**
     * PackageAutoRecurringFailed constructor.
     * @param $user
     * @param $packageName
     * @param $subscriptionFee
     * @param $cardInfo
     * @param $error
     * @param null $subject
     * @param null $body
     */
    public function __construct($user, $packageName, $subscriptionFee, $cardInfo, $error, $subject = null, $body = null)
    {
        $this->user = $user;
        $this->packageName = $packageName;
        $this->subscriptionFee = $subscriptionFee;
        $this->subject = $subject;
        $this->body = $body;
        $this->error = $error;
        $this->cardInfo = $cardInfo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['userName'] = $this->user->first_name.' '.$this->user->last_name;
        $data['packageName'] = $this->packageName;
        $data['subscriptionFee'] = $this->subscriptionFee;
        $data['cardNumber'] = $this->cardInfo->card_number;
        $data['body'] = $this->body;
        $data['error'] = $this->error;

        if ($this->subject && $this->body) {
            return $this->subject(env('SITE_NAME') . ' Recurring Failed!')->markdown('emails.user.auto-recurring-failed-to-user')->with($data);
        } else {
            return $this->subject(env('SITE_NAME') . ' Recurring Failed!')->markdown('emails.user.auto-recurring-failed-to-user')->with($data);
        }
    }
}
