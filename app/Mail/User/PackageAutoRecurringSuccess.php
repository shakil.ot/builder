<?php

namespace App\Mail\User;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PackageAutoRecurringSuccess extends Mailable
{
    use Queueable, SerializesModels;

    private $receiver;
    private $packageDetails;
    private $price;
    private $cardNumber;
    /**
     * @var null
     */
    private $body;

    /**
     * Create a new message instance.
     *
     * @param $receiver
     * @param $packageDetails
     * @param $price
     * @param $cardNumber
     * @param null $subject
     * @param null $body
     */
    public function __construct($receiver, $packageDetails, $price, $cardNumber, $subject = null, $body = null)
    {
        $this->receiver = $receiver;
        $this->packageDetails = $packageDetails;
        $this->price = $price;
        $this->cardNumber = $cardNumber;
        $this->subject = $subject;
        $this->body = $body;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['userName'] = $this->receiver->first_name.' '.$this->receiver->last_name;
        $data['email'] = $this->receiver->email;
        $data['packageName'] = $this->packageDetails->name;
        $data['subscriptionFee'] = $this->price;
        $data['cardNumber'] = $this->cardNumber;

        if ($this->subject && $this->body) {
            $data['body'] = $this->body;

            return $this->subject($this->subject)->markdown('emails.user.auto-recurring-success')->with($data);
        }
        return $this->markdown('emails.user.auto-recurring-success')->with($data);
    }
}
