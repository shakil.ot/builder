<?php

namespace App\Mail\User;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewUserCreated extends Mailable
{
    use Queueable, SerializesModels;

    private $receiver;
    private $password;

    /**
     * Create a new message instance.
     *
     * @param $receiver
     * @param $password
     */
    public function __construct($receiver, $password)
    {
        $this->receiver = $receiver;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['userName'] = $this->receiver->first_name.' '.$this->receiver->last_name;
        $data['email'] = $this->receiver->email;
        $data['password'] = $this->password;
        $data['url'] = route('dashboard');

        return $this->markdown('emails.user.new-user')->with($data);
    }
}
