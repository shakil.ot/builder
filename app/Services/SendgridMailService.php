<?php
/**
 * Created by PhpStorm.
 * User: mohi
 * Date: 3/23/17
 * Time: 1:43 PM
 */

namespace App\Services;


use App\Contracts\Service\UserContact;
use Auth;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Mockery\Exception;


class SendgridMailService
{


    /**
     * @var LocationContact
     */
    private $locationService;

    public function __construct()
    {
    }


    public function adminSendMailWithSendGrid($subject, $to, $from = "", $message)
    {
        $getKeyData = env('SENDGRID_KEY');
        $from = env('FROM_EMAIL');
        $userName = $to;
        $agencyName = env('SITE_NAME');

        Log::info($to);
        Log::info($subject);

        $email = new \SendGrid\Mail\Mail();
        $email->setFrom($from, $agencyName);
        $email->setSubject($subject);
        $email->addTo($to, $userName);
        $email->addContent(
            "text/html", $message
        );
        $sendgrid = new \SendGrid($getKeyData);
        try {
            $sendgrid->send($email);
            return true;

        } catch (Exception $e) {

            Log::info(' Send mail error : ' . $e->getMessage());
            return false;
        }


    }


    public function sendMailWithSendGrid($subject, $to, $from = null, $message, $companyName, $trackingId)
    {

        $getKeyData = env('SENDGRID_KEY');

        if($from == null){
            $from = env('FROM_EMAIL');
        }

        $userName = $to;
        $agencyName = $from;

        Log::info('companyName : '.$companyName);

        $email = new \SendGrid\Mail\Mail();
        $email->setFrom($from, $companyName);
        $email->setSubject($subject);
        $email->addTo($to, $userName);
        $email->addCustomArg("tracking_id", $trackingId);
        $email->addContent(
            "text/html", $message
        );

        $sendgrid = new \SendGrid($getKeyData);
        try {
            $sendgrid->send($email);
            return true;

        } catch (Exception $e) {

            Log::info(' Send mail error : ' . $e->getMessage());
            return $e;
        }


    }




}
