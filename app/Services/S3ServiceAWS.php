<?php
/**
 * Created by PhpStorm.
 * User: Riyad
 * Date: 4/01/2020
 * Time: 1:51 AM
 */

namespace App\Services;

use App\Helper\UtilityHelper;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use PHPUnit\Exception;

class S3ServiceAWS
{
    const FILE_USER_ACHIEVEMENT_IMAGE = 'user/achievement_image';
    const FILE_USER_REWARD_IMAGE = 'user/reward_image';
    const FILE_CATEGORY_EMPLOYEE_IMAGE = 'employee/image';
    const FILE_CATEGORY_USER_PROFILE = 'user_profile/image';
    const FILE_CATEGORY_INBOX_CONTENTS = 'inbox';
    const UPLOAD_IMAGE_FROM_SUMMERNOTE = 'email/summernote/images';

    const FILE_CATEGORY_CONTENT_GIFT = 'gift/image';
    const FILE_CATEGORY_CONTENT_FRONT_IMAGE = 'postcard/front_image';
    const FILE_CATEGORY_CONTENT_BACK_IMAGE = 'postcard/back_image';
    const FILE_IMAGE_TEMPLATE = 'admin/template/reward';
    const FILE_IMAGE_ACHIEVEMENT_TEMPLATE = 'admin/template/achievement';

    const USER_ADMIN = 'admin';

    const SECTION = 'olk9funnel';

    public function __construct()
    {
    }

    public function uploadFileToS3($filePath, $type, $fileName, $userId = null)
    {
        try {
            $path = self::SECTION  . "/" . $type . '/';
            if($userId != null){
                $path = self::SECTION . '/' . $userId . "/" . $type . '/';
            }

            $this->makeDir($path);
            $this->makeDir($path)
                ->moveFile($filePath, $path . $fileName)
                ->setVisibility($path . $fileName);
            try {
                File::delete($filePath);
            } catch (Exception $e) {
                return ['success' => false];
            }
            return [
                'success' => true,
                'url' => $this->generateUrl($userId, $type, $fileName),
                'file_name' => $fileName,
            ];

        } catch (Exception $e) {
            Log::info("File upload error : " . $e->getMessage() . "User id :" . $userId);
            return ['success' => false, 'message' => 'File upload error', 'url' => null];
        }
    }

    public function uploadImageWithRandomNameAndResizeByFilePath($filePath, $ext, $type, $userId, $maxWidth = 225, $maxHeight = 225)
    {
        $randomString = Utility::generateRandomString(5) . time();
        $path = $filePath;
        $fileName = $randomString . '.' . $ext;
        //Should create this directory from other page like admin login page.

        $response = $this->uploadFileToS3($path, $type, $fileName, $userId);
        try {
            File::delete($path);
        } catch (Exception $e) {
            return ['status' => 'error'];
        }
        return $response;
    }


    public function uploadToS3WithRandomName(UploadedFile $file, $type, $userId)
    {
        $randomName = Utility::generateRandomString(5) . time() . '.' . $file->getClientOriginalExtension();
        return $this->uploadFileToS3($file->path(), $type, $randomName, $userId);
    }

    public function uploadImageWithRandomNameAndResize(UploadedFile $file, $type, $userId, $maxWidth = 225, $maxHeight = 225)
    {
        $randomString = Utility::generateRandomString(5) . time();
        $ext = $file->getClientOriginalExtension();
        $path = $file->path();
        $fileName = $randomString . '.' . $ext;
        $resize_file_name = $randomString . '_small.' . $ext;

        //Should create this directory from other page like admin login page.
        $temporaryPath = public_path() . '/upload/temporaryFiles/' . $resize_file_name;

        UtilityHelper::image_resize($path, $temporaryPath, $maxWidth, $maxHeight, $ext);
        $response = $this->uploadFileToS3($path, $type, $fileName, $userId);
        $responseSmall = $this->uploadFileToS3($temporaryPath, $type, $resize_file_name, $userId);
        if ($response['success']) {
            if ($responseSmall['success']) {
                $response['resizedFileName'] = $responseSmall['file_name'];
                $response['resizedFileUrl'] = $responseSmall['url'];
            }
        } else {
            $this->removeFile($userId, $type, $resize_file_name);
        }
        return $response;
    }

    public function uploadImageWithRandomNameAndResizeForAdmin(UploadedFile $file, $type, $maxWidth = 225, $maxHeight = 225)
    {
        $randomString = Utility::generateRandomString(5) . time();
        $ext = $file->getClientOriginalExtension();
        $path = $file->path();
        $fileName = $randomString . '.' . $ext;
        $resize_file_name = $randomString . '_small.' . $ext;

        //Should create this directory from other page like admin login page.
        $temporaryPath = public_path() . '/upload/temporaryFiles/' . $resize_file_name;

        UtilityHelper::image_resize($path, $temporaryPath, $maxWidth, $maxHeight, $ext);
        $response = $this->uploadFileToS3($path, $type, $fileName);
        $responseSmall = $this->uploadFileToS3($temporaryPath, $type, $resize_file_name);
        if ($response['success']) {
            if ($responseSmall['success']) {
                $response['resizedFileName'] = $responseSmall['file_name'];
                $response['resizedFileUrl'] = $responseSmall['url'];
            }
        } else {
            $this->removeFile(null, $type, $resize_file_name);
        }
        return $response;
    }

    public function hasFile($fullPath)
    {
        $fullPath = str_replace("https://s3.amazonaws.com/" . env('S3_BUCKET_NAME') . "/", '', $fullPath);
        return Storage::disk('s3')->exists($fullPath);
    }

    public function getFiles($prefix = '/admin', $treeIndex = 1)
    {
        $storage = Storage::disk('s3');
        $client = $storage->getAdapter()->getClient();
        $command = $client->getCommand('ListObjects');
        $command['Bucket'] = $storage->getAdapter()->getBucket();
        $command['Prefix'] = self::SECTION . $prefix;
        $result = $client->execute($command);
        dd($result);
        $arr = [];
        if (isset($result['Contents'])) {
            foreach ($result['Contents'] as $content) {
                $folders = explode('/', $content['Key']);
                if (isset($folders[$treeIndex])) {
                    $arr[$folders[$treeIndex]] = $folders[$treeIndex];
                }
            }
        }
        return $arr;
    }

    public function removeFile($userId = null, $type, $fileName)
    {
        $path = self::SECTION  . '/' . $type . '/' . $fileName;
        if($userId != null){
            $path = self::SECTION . '/' . $userId . '/' . $type . '/' . $fileName;
        }

        if (Storage::disk('s3')->exists($path)) {
            return Storage::disk('s3')->delete($path);
        }
        return false;
    }

    public function removeFileWithFullUrl($fullPath)
    {
        $fullPath = str_replace("https://s3.amazonaws.com/" . env('S3_BUCKET_NAME') . "/", '', $fullPath);
        if (Storage::disk('s3')->exists($fullPath)) {
            return Storage::disk('s3')->delete($fullPath);
        }
        return false;
    }

    public function downloadFile($userId, $type, $fileName, $originalName)
    {
        $path = self::SECTION . '/' . $userId . '/' . $type . '/' . $fileName;
        return Storage::disk('s3')->download($path, $originalName);
    }

    public function downloadFileFromUrl($fileUrl, $originalName, $type)
    {
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=" . $originalName);
        header("Content-Type: " . $type);
        return readfile($fileUrl);
    }

    public function generateUrl($userId = null, $type, $fileName)
    {
        if($userId !=null ){
            return "https://s3.amazonaws.com/" . env('S3_BUCKET_NAME') . "/" . S3ServiceAWS::SECTION . "/" . $userId . "/" . $type . "/" . $fileName;
        }
        return "https://s3.amazonaws.com/" . env('S3_BUCKET_NAME') . "/" . S3ServiceAWS::SECTION . "/"  . $type . "/" . $fileName;


    }

    private function makeDir($path)
    {
        if (!Storage::disk('s3')->exists($path)) {
            $result = Storage::disk('s3')->makeDirectory($path);
            Log::info("On make dir");
            Log::info($result);
        }
        return $this;
    }


    private function moveStreamFile($from, $to)
    {
        $result = Storage::disk('s3')->put($to, $from);
        Log::info("On make dir");
        Log::info($result);
        return $this;
    }

    private function moveFile($from, $to)
    {
        $result = Storage::disk('s3')->put($to, file_get_contents($from));
        return $this;
    }


    private function setVisibility($path, $status = 'public')
    {
        Storage::disk('s3')->setVisibility($path, $status);
        return $this;
    }

}
