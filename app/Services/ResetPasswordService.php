<?php

namespace App\Services;


use App\Contracts\Repository\ResetPasswordRepository;
use App\Contracts\Service\ResetPasswordContact;

class ResetPasswordService implements ResetPasswordContact
{
    /**
     * @var ResetPasswordRepository
     */
    private $resetPasswordRepository;

    public function __construct(ResetPasswordRepository $resetPasswordRepository)
    {
        $this->resetPasswordRepository = $resetPasswordRepository;
    }

    public function insertResetPasswordRequest($param)
    {
        return $this->resetPasswordRepository->insertResetPasswordRequest($param);
    }

    public function getInformationByEmail($token, $email)
    {
        return $this->resetPasswordRepository->getInformationByEmail($token, $email);
    }
    public function getInformationByLastUpdatedEmail($email){
        return $this->resetPasswordRepository->getInformationByLastUpdatedEmail($email);
    }

    public function deleteInformationByEmail($email)
    {
        return $this->resetPasswordRepository->deleteInformationByEmail($email);
    }
    public function updateResetPasswordById($id, $data)
    {
        return $this->resetPasswordRepository->updateResetPasswordById($id, $data);

    }
}

