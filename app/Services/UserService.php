<?php

namespace App\Services;

use App\Contracts\Repository\UserRepository;
use App\Contracts\Repository\UserSettingRepository;
use App\Contracts\Service\ResetPasswordContact;
use App\Contracts\Service\UserContact;
use App\Helper\UtilityHelper;
use App\Models\PasswordReset;
use App\Models\UserSetting;
use App\User;
use Carbon\Carbon;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Modules\Broadcast\Contracts\Repositories\BroadcastLogRepository;
use Modules\Package\Contracts\Repositories\UserCurrentPackageRepository;
use Modules\UserCompany\Contracts\Repositories\CompanyGroupRepository;
use Modules\UserDashboard\Contracts\Repositories\SingleSendStatReportRepository;
use Modules\UserEmployee\Contracts\Repositories\EmployeeRepository;
use Yajra\DataTables\DataTables;


class UserService implements UserContact
{


    /**
     * @var UserRepository
     */
    public $userRepository;
    /**
     * @var UserSettingRepository
     */
    private $userSettingRepository;
    /**
     * @var UserCurrentPackageRepository
     */
    private $userCurrentPackageRepository;
    /**
     * @var CompanyGroupRepository
     */
    private $companyGroupRepo;
    /**
     * @var S3ServiceAWS
     */
    private $s3ServiceAWS;
    private $resetPasswordService;
    /**
     * @var BroadcastLogRepository
     */
    private $broadcastLogRepository;
    /**
     * @var SingleSendStatReportRepository
     */
    private $singleSendStatReportRepository;

    /**
     * UserService constructor.
     * @param UserRepository $userRepository
     * @param UserSettingRepository $userSettingRepository
     * @param S3ServiceAWS $s3ServiceAWS
     * @param UserCurrentPackageRepository $userCurrentPackageRepository
     * @param ResetPasswordContact $resetPasswordService
     * @param BroadcastLogRepository $broadcastLogRepository
     */
    public function __construct(UserRepository $userRepository,
                                UserSettingRepository $userSettingRepository,
                                S3ServiceAWS $s3ServiceAWS,
                                UserCurrentPackageRepository $userCurrentPackageRepository,
                                ResetPasswordContact $resetPasswordService,
                                BroadcastLogRepository $broadcastLogRepository,
                                SingleSendStatReportRepository $singleSendStatReportRepository)
    {
        $this->userRepository = $userRepository;

        $this->userSettingRepository = $userSettingRepository;
        $this->userCurrentPackageRepository = $userCurrentPackageRepository;
        $this->s3ServiceAWS = $s3ServiceAWS;
        $this->resetPasswordService = $resetPasswordService;
        $this->broadcastLogRepository = $broadcastLogRepository;
        $this->singleSendStatReportRepository = $singleSendStatReportRepository;
    }

    /****#### GET ALL USER  FUNCTION   ###****/
    /****#### get all user from user table using this function  ###****/
    public function getAllUser($request = null)
    {
        return $this->userRepository->getAll($request);
    }

    public function getTotalUser()
    {
        return $this->userRepository->getTotalUser();
    }

    public function getUserByEmail($email)
    {
        return $this->userRepository->getUserByEmail($email);
    }

    public function forgetPasswordCodeSet($response)
    {
        $forget_code = Utility::generateRandomNumericString(6);
        $param = [
            'email' => $response->email,
            'token' => $forget_code,
            'expire_time' => Carbon::now()->addMinute(5),
            'is_used' => PasswordReset::FORGET_PASSWORD_NOT_USED
        ];

        $resetPasswordRequest = $this->resetPasswordService->insertResetPasswordRequest($param);
        $encryptedEmail = Crypt::encrypt($response->email);
        $link = env('APP_URL') . '/password/change/' . $encryptedEmail;
        if ($resetPasswordRequest) {

            try {
                $message = $this->resetPasswordMail($response->email, $forget_code, $link);
                $subject = "Forget Password Link";
                $send = App::make(SendgridMailService::class)->adminSendMailWithSendGrid($subject, $response->email, $from = "", $message);
                if ($send) {
                    return [
                        'status' => 'success',
                        'html' => 'Please check your email for reset link.'
                    ];
                } else {
                    return [
                        'status' => 'error',
                        'html' => 'Password not reset. Please try again later.'
                    ];
                }
            } catch (Exception $e) {
                Log::info('Error Email sending' . $e);
            }
        }

    }

    private function resetPasswordMail($email, $forget_code, $link)
    {
        $image = URL::To('/') . "/" . env('LOGO_MAIN');

        $message = '<html><body>';

        $message .= '<h4>Please go to below link and reset your password </h4>';

        $message .= '<table cellspacing="0" style="border: none; width: 100%;">
                        <tr>
                            <th style="text-align: left">Email</th><td> : </td><td>' . $email . '</td>
                        </tr>
                        <tr>
                            <th style="text-align: left">Code</th><td> : </td><td>' . $forget_code . '</td>
                        </tr>
                        <tr>
                            <th style="text-align: left">Link</th><td> : </td><td>Click here to change password <a href="' . $link . '"> Reset Password Link  </a></td>
                        </tr>
                    </table>';
        $message .= '<div style="margin-top: 60px; border-top: 1px solid #f1f3f4">';
//        $message .= '<p>This email was sent to you by UPREVIEW. You are receiving this email because you signed up for UpReview.</p>';
        $message .= '<table cellspacing="0" style="border: none; width: 100%;margin-top:20px">
                        <tr>
                            <td>Sincerely,</td>
                        </tr>
                        <tr>
                            <td><img src="' . $image . '" style="width: 200px;" /></td>
                        </tr>

                    </table>';
        $message .= '</div></body></html>';
        return $message;
    }

    public function newForgetPasswordReset($request)
    {
        $email = Crypt::decrypt($request->email);

        if (empty($email) || is_null($email)) {
            return [
                'status' => 'error',
                'html' => 'Email not found'
            ];
        }

        if (empty($request->code) || is_null($request->code)) {
            return [
                'status' => 'error',
                'html' => 'Verify Code is required'
            ];
        }

        if (empty($request->password) || is_null($request->password)) {
            return [
                'status' => 'error',
                'html' => 'Password is required'
            ];
        }

        if (empty($request->password_confirmation) || is_null($request->password_confirmation)) {
            return [
                'status' => 'error',
                'html' => 'Password confirmation is required'
            ];
        }
        if ($request->password_confirmation != $request->password) {
            return [
                'status' => 'error',
                'html' => 'Password confirmation doesnt match'
            ];
        }

        $resetPasswordEntry = $this->resetPasswordService->getInformationByLastUpdatedEmail($email);
        if (empty($resetPasswordEntry) || is_null($resetPasswordEntry)) {
            return [
                'status' => 'error',
                'html' => 'Something is wrong! Please try again'
            ];
        }

        if (($resetPasswordEntry->token) != $request->code) {
            return [
                'status' => 'error',
                'html' => 'Verify code doesnt match!'
            ];
        }
        if ($resetPasswordEntry['is_used'] == PasswordReset::FORGET_PASSWORD_NOT_USED) {
            $updateUser = $this->updateUserAfterResetPasswordReset($request);
            $data = [
                'is_used' => PasswordReset::FORGET_PASSWORD_USED
            ];
            $this->resetPasswordService->updateResetPasswordById($resetPasswordEntry['id'], $data);
            if ($updateUser) {
                return [
                    'status' => 'success',
                    'html' => 'Password changed successfully.'
                ];
            }
            return [
                'status' => 'error',
                'html' => 'Password change failed! Please try again'
            ];
        }
        return [
            'status' => 'error',
            'html' => 'Link Expired! Please Try Again'
        ];

    }

    public function updateUserAfterResetPasswordReset($request)
    {
        $email = Crypt::decrypt($request->email);

        $where = [
            'email' => $email
        ];
        $param = [
            'password' => Hash::make($request->password)
        ];
        return $this->userRepository->updateUserByParam($where, $param);
    }


    /****#### NEW USER CREATE FUNCTION   ###****/
    /****#### Add a new a user  using this function  ###****/
    public function createUser($request)
    {
        $password = Utility::generateRandomPassword(8);

        $user = [
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'phone' => $request->phone,
            'access_token' => Utility::generateRandomString(10) . time(),
            'password' => Hash::make($password),
            'user_created_from' => User::USER_CREATED_FROM_ADMIN,
            'country' => User::COUNTRY_INTERNATIONAL
        ];

        $response_transaction = DB::transaction(function () use ($request, $user, $password) {

            $response = $this->userRepository->createUser($user);

            if ($response) {
                $user_id = $response->id;

                /**** USER SETTING ***/
                $this->userSettingSave($user_id);

//                $message = "Dear user, Welcome to our site. You can login using password $password";
                try {
                    $message = $this->mailHtml($request->email, $password, $request);
                    $subject = "Successfully Registration in " . env('SITE_NAME');
                    App::make(SendgridMailService::class)->adminSendMailWithSendGrid($subject, $request->email, $from = "", $message);
                } catch (Exception $e) {
                    Log::info('Error Email sending' . $e);
                }

//                return RETURN_STATUS_FORMATE('success', 'User added successfully');
                return [
                    'html' => "New User Created Successfully",
                    'status' => 'success'
                ];
            } else {
                return [
                    'html' => "User creation failed!. Submit the form again",
                    'status' => 'error'
                ];
            }
        });
        return [
            'html' => $response_transaction['html'],
            'status' => $response_transaction['status']
        ];
    }


    /****#### USER BASIC SETTING FUNCTION   ###****/
    /****#### setup user basic setting using this function  ###****/
    public function userSettingSave($user_id)
    {
        $data = [
            [
                'user_id' => $user_id,
                'key' => UserSetting::USER_TIME_ZONE,
                'value' => env('TIMEZONE')
            ]
        ];

        $this->userSettingRepository->insertUserSetting($data[0]);

        //Insert Recurrent status
        $recurrent_status = [
            'user_id' => $user_id,
            'key' => UserSetting::RECURRENT_ON_OFF,
            'value' => UserSetting::RECURRENT_ON_STATUS
        ];
        $this->userSettingRepository->insertUserSetting($recurrent_status);

    }

    /****#### SEND MAIL FUNCTION   ###****/
    /****#### send a mail when user created using this function  ###****/
    public function mailHtml($email, $password, $request)
    {
        $image = URL::To('/') . "/" . env('MAIN_LOGO');
        $message = '<html><body>';

        $message .= '<h4 style="color: blue">Dear ' . $request['first_name'] . ' ' . $request['last_name'] . '</h4><br/>';
        $message .= '<h5>Your account has been created successfully in ' . env('APP_URL') . '. Thank you for joining with us!</h5>';

        $message .= '<table cellspacing="0" style="border: none; width: 100%;">


            <tr style="background: #eee"><td>Your ' . env('SITE_NAME') . ' account has been successfully created. We appreciate your business and we\'re confident you\'ll soon become a raving fan of our software!</td></tr>
            <tr><td>So what are you waiting for? Let\'s get started!</td></tr>
            <tr>
            <th style="text-align: left">Email: ' . $email . ' </th><td> </td><td></td>
            </tr>
            <tr>
            <th style="text-align: left">Password: ' . $password . '</th><td> </td><td></td>
            </tr>
            <tr><td><strong>Please login ::</strong> <a href="' . URL('/') . '">' . URL('/') . '</a></td></tr>
            </table>';
        $message .= '<div style="margin-top: 60px; border-top: 1px solid #f1f3f4">';
        $message .= '<p>This email was sent to you by ' . env('SITE_NAME') . 'You are receiving this email because you signed up for ' . env('SITE_NAME') . '.</p>';
        $message .= '<table cellspacing="0" style="border: none; width: 100%;margin-top:20px">
            <tr>
            <td>Sincerely,</td>
            </tr>
            <tr>
            <td><img src="' . $image . '" style="width: 200px;" /></td>
            </tr>
            <tr>
            <td><span style="color:white; text-decoration: none">' . env('SITE_URL') . '</span></td>
            </tr>
            <tr>
            <td><i>This is an automated system email. Please do not reply to this email.</i></td>
            </tr>
            </table>';
        $message .= '</div></body></html>';

        return $message;
    }



    /** #### MAKE A USERLIST :: GET ALL USER FROM USER TABLE #### */
    /** #### MAKE A USERLIST :: GET ALL USER FROM USER TABLE #### */
    public function listUserData($request)
    {
        $users = $this->getAllUser($request);

        return Datatables::of($users)
            ->addColumn('action', function ($users) {
                $recurrentShowInAction = '';
                $recurrentInfo = $this->userSettingRepository->getPackageSettingByUser($users->id);

                if ($recurrentInfo) {
                    if ($recurrentInfo['value'] == UserSetting::RECURRENT_ON_STATUS) {
                        $recurrentShowInAction = '<a class="dropdown-item action" href="javascript:void(0)"  data-value="stop-auto-recurring" data-id="' . $users->id . '">
                                                    <i class="la la-stop-circle  text-blue mr-5"></i> Stop Auto Recurring</span>
                                                </a>';
                    } else {
                        $recurrentShowInAction = '<a class="dropdown-item action" href="javascript:void(0)"  data-value="start-auto-recurring" data-id="' . $users->id . '">
                                                    <i class="la la-play-circle-o text-blue mr-5"></i> Start Auto Recurring</span>
                                                </a>';
                    }
                }
                $encryptedUserId = Crypt::encrypt($users->id);
                $packageAllocate = '';
                $creditAllocate = '';

                $packageAllocate = '<a class="dropdown-item action" href="javascript:void(0)"  data-value="package-allocate" data-id="' . $users->id . '">
                                            <i class="flat flaticon-file text-blue mr-5"></i> Package Allocate</span>
                                        </a>';

                return '<div class="dropdown dropdown-inline mr-4">
                            <button type="button" class="btn btn-light-blue btn-icon btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="ki ki-bold-more-hor"></i>
                            </button>
                            <div class="dropdown-menu">
                             <a class="dropdown-item action" href="javascript:void(0)"  data-value="user-details" data-id="' . $users->id . '">
                                <i class="flat flaticon-user text-blue mr-5"></i> User Details</span>
                            </a>
                            <a class="dropdown-item action" href="javascript:void(0)"  data-value="edit-user" data-id="' . $users->id . '">
                                <i class="flat flaticon-edit text-blue mr-5"></i> Edit</span>
                            </a>

                                 ' . $packageAllocate . '
                                   ' . $creditAllocate . $recurrentShowInAction . '

                            <a class="dropdown-item action" href="javascript:void(0)"  data-value="login-as-user" data-id="' . $encryptedUserId . '">
                                <i class="flat flaticon-lock text-blue mr-5"></i> Login as user</span>
                            </a>


                           <a class="dropdown-item"  href="' . route("admin.transaction-report-user-wise", [base64_encode($users->id)]) . '"  target="_blank">
                                <i class="flat flaticon-list text-blue mr-5"></i> Payment Transaction</span>
                            </a>


                            <div class="dropdown-divider"></div>

                            ' . (($users->status == User::STATUS_ACTIVE) ?
                        '<a class="dropdown-item action" href="javascript:void(0)"  data-value="deactivate-user" data-id="' . $users->id . '">
                                        <i class="flat flaticon2-cross text-danger mr-5"></i>
                                        <span class="label label-danger label-dot mr-2"></span>
                                        <span class="font-weight-bold text-danger">Deactivate</span>
                                 </a>' :

                        '<a class="dropdown-item action" href="javascript:void(0)"  data-value="active-user" data-id="' . $users->id . '">
                                        <i class="flat flaticon2-check-mark text-green mr-5"></i>
                                        <span class="label label-danger label-dot mr-2"></span>
                                        <span class="font-weight-bold text-danger">Activate</span>
                                 </a>') . '

                            <a class="dropdown-item action" href="javascript:void(0)"  data-value="delete-user" data-id="' . $users->id . '">
                                <i class="flat flaticon-delete text-blue mr-5"></i> Delete</span>
                            </a>
                            </div>
                        </div>';


            })
            ->addColumn('my_status', function ($users) {
                if ($users->status == User::STATUS_ACTIVE) {
                    return '<span class="label label-blue label-dot mr-2"></span>
                            <span class="font-weight-bold text-green">
                            <span title="active" style="color:#6ae73d"><i class="la la-check" style="color: green"></i></span></span>';

                } else {
                    return '<span class="label label-danger label-dot mr-2"></span>
                            <span class="font-weight-bold text-danger">
                            <span title="Inactive" style="color:#ee1a1a"><i class="la la-close" style="color: red"></i></span></span>';

                }
            })
            ->addColumn('user_info', function ($users) {
                $phone = $users->phone;
                if (($users->phone) == "") {
                    $phone = "<span style='color: darkred'>Phone not found</span>";
                }
                return "<span ><i class='fa fa-user'></i> <a  class='label label-lg label-light-primary label-inline action'  data-value='user-details' data-id=' " . $users->id . "' style='font-size: 17px; cursor:pointer'> " . $users->first_name . ' ' . $users->last_name . "</a></span><br>
                        <span><i class='fa fa-envelope'></i> " . $users->email . "</span><br>
                        <span><i class='fa fa-phone'></i> " . $phone . "</span>";
            })
            ->addColumn('package_info', function ($users) {
                $packageData = $this->getUserPackageInformationByUserId($users->id);
                if ($packageData) {
                    return '<div class="d-flex align-items-center">

                                <!--begin::Text-->
                                <div class="d-flex flex-column flex-grow-1">
                                    <a href="#" class="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1">' . $packageData->package_name . '</a>
                                    <span class="text-muted font-weight-bold">Subscription fee : ' . $packageData->subscription_fee . '</span>
                                    <span class="text-muted font-weight-bold">Expire Date : ' . $packageData->expire_date . '</span>
                                </div>
                                <!--end::Text-->
                            </div>';
                } else {
                    return "<span ><span class='label label-lg label-light-danger label-inline' style='font-size: 14px'>
                            Package not found</span></span><br>";
                }
            })
            ->addColumn('billing_status', function ($users) {
                // return "Hello";
                $recurrentShowInAction = '';
                $recurrentInfo = $this->userSettingRepository->getPackageSettingByUser($users->id);

                if ($recurrentInfo['value'] == UserSetting::RECURRENT_ON_STATUS) {
                    return '<span class="label label-lg label-light-primary label-inline"> ON</span>';

                } else {
                    return '<span class="label label-lg label-light-danger label-inline"> OFF</span>';

                }
            })
            ->rawColumns(['action', 'my_status', 'user_info', 'package_info', 'billing_status', 'total_giveaway'])
            ->make(true);
    }


    public function getUserById($user_id)
    {
        return $this->userRepository->getUserById($user_id);
    }

    /****#### UPDATE USER CREATE FUNCTION   ###****/
    /****#### Update a user  using this function  ###****/
    public function update($request)
    {
        $userUpdate = $this->userRepository->update($request->except('_token', 'user_id'), $request->user_id);

        if ($request->hasFile('profile_image')) {
            $this->updateProfileImage($request->file('profile_image'), $request->user_id);
        }

        if ($userUpdate) {
            return ['status' => true, 'message' => 'User has been updated successfully'];
        } else {
            return ['status' => false, 'message' => 'User update failed!!'];
        }
    }

    public function updateUser($request)
    {
        $where = [
            'id' => $request['user_id']
        ];

        $param = [
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'phone' => $request->phone,
            'address' => $request->address,
            'state' => $request->state,
            'city' => $request->city,
            'zip_code' => $request->zip_code,
            'country' => User::COUNTRY_INTERNATIONAL
        ];
        $response = $this->userRepository->updateUserByParam($where, $param);

        if ($response) {
            return UtilityHelper::RETURN_STATUS_FORMATE('success', 'User has been updated successfully');

        } else {
            return UtilityHelper::RETURN_STATUS_FORMATE('error', 'User update failed!!');
        }
    }


    public function getCountryByUserId($userId)
    {
        return $this->userRepository->getCountryByUserId($userId);
    }

    public function updateUserByParam($where, $param)
    {
        return $this->userRepository->updateUserByParam($where, $param);
    }

    public function updateProfileImage($file, $userId)
    {
        try {
            $user = $this->userRepository->find($userId);
            if (!empty($user->profile_image)) {
                $this->s3ServiceAWS->removeFileWithFullUrl($user->profile_image);
            }

            $s3Response = $this->s3ServiceAWS->uploadToS3WithRandomName($file, S3ServiceAWS::FILE_CATEGORY_USER_PROFILE, $user->company_id);
            if ($s3Response['success']) {
                if ($this->userRepository->update(['profile_image' => $s3Response['url']], $user->id)) {
                    return ['status' => true, 'message' => 'Profile Image Updated Successfully'];
                }
            }
            return ['status' => true, 'message' => 'Profile Image Not Updated'];
        } catch (\Exception $e) {
            Log::error($e);
            return ['status' => true, 'message' => 'Profile Image Not Updated'];
        }
    }


    /****#### ACTIVATED USER FUNCTION   ###****/
    /****#### activated an user  using this function  ###****/
    public function activateUser($userId)
    {
        $response = $this->userRepository->activateUser($userId);
        if ($response) {

            $subUsers = $this->userRepository->getSubUsers($userId);
            if ($subUsers->count() > 0) {
                foreach ($subUsers as $each) {
                    $this->userRepository->activateUser($each->id);
                }
            }
            return UtilityHelper::RETURN_STATUS_FORMATE('success', 'User has been activated successfully');

        }
        return UtilityHelper::RETURN_STATUS_FORMATE('error', 'User activation failed! Try again');

    }


    /****#### DEACTIVATED USER FUNCTION   ###****/
    /****#### deactivated an user  using this function  ###****/
    public function deactivateUser($userId)
    {
        $response = $this->userRepository->deactivateUser($userId);
        if ($response) {
            $subUsers = $this->userRepository->getSubUsers($userId);
            if ($subUsers->count() > 0) {
                foreach ($subUsers as $each) {
                    $this->userRepository->deactivateUser($each->id);
                }
            }
            return UtilityHelper::RETURN_STATUS_FORMATE('success', 'User has been deactivated successfully');

        }
        return UtilityHelper::RETURN_STATUS_FORMATE('error', 'User deactivation failed! Try again!');

    }


    /****#### USER DETAILS FUNCTION   ###****/
    /****#### View a user details using this function  ###****/
    public function getViewUserInformation($request)
    {
        $userInfo = $this->getUserById($request->get('user_id'));
        $encryptedUserId = Crypt::encrypt($request->get('user_id'));
        $packageData = $this->getUserPackageInformationByUserId($request->get('user_id'));
        return Response::json([
            'html' => view('admin.user.package-info')->with([
                'packageData' => $packageData,
                'userInfo' => $userInfo,
                'encryptedUserId' => $encryptedUserId
            ])->render(),
            'status' => 'success'
        ]);
    }


    /****#### GET USER CURRNENT PACKAGE INFORMATION FUNCTION   ###****/
    /****#### Get user current package  using this function  ###****/
    public function getUserPackageInformationByUserId($userId)
    {
        return $this->userCurrentPackageRepository->getByPackageByUserId($userId);
    }



    /****#### STOP AUTO RECURRING BILLING FUNCTION   ###****/
    /****#### stop auto recurring billing of an user using this function  ###****/
    public function stopAutoRecurring($userId)
    {
        //Update Recurrent status
        $where = [
            'user_id' => $userId,
            'key' => UserSetting::RECURRENT_ON_OFF
        ];
        $recurrent_status = [
            'value' => UserSetting::RECURRENT_OFF_STATUS
        ];
        $response = $this->userSettingRepository->updateUserSettingByParam($where, $recurrent_status);
        if ($response) {
            return UtilityHelper::RETURN_STATUS_FORMATE('success', 'Auto Recurring Stopped For This User');
        }
        return UtilityHelper::RETURN_STATUS_FORMATE('error', 'Stop Auto Recurring Failed! Please Try Again');


    }

    /****#### START AUTO RECURRING BILLING FUNCTION   ###****/
    /****#### start auto recurring billing of an user using this function  ###****/
    public function startAutoRecurring($userId)
    {
        //Update Recurrent status
        $where = [
            'user_id' => $userId,
            'key' => UserSetting::RECURRENT_ON_OFF
        ];
        $recurrent_status = [
            'value' => UserSetting::RECURRENT_ON_STATUS
        ];
        $response = $this->userSettingRepository->updateUserSettingByParam($where, $recurrent_status);
        if ($response) {
            return UtilityHelper::RETURN_STATUS_FORMATE('success', 'Auto Recurring Started For This User');
        }
        return UtilityHelper::RETURN_STATUS_FORMATE('error', 'Start Auto Recurring Failed! Please Try Again');

    }

    public function getFilterData($company_id, $type)
    {
        if ($type == ACCESS_INDIVIDUAL) {
            $filter_data = $this->employeeRepo->findWhere(['company_id' => $company_id], ['id', 'name']);
        } elseif ($type == ACCESS_GROUP) {
            $filter_data = $this->companyGroupRepo->findWhere(['company_id' => $company_id], ['id', 'name']);
        } else {
            $filter_data = false;
        }

        if ($filter_data) {
            return ['status' => true, 'filter_data' => $filter_data];
        } else {
            return ['status' => false, 'message' => 'Filter data not found'];
        }
    }



    /****#### LOGIN IN AS USER FUNCTION   ###****/
    /****#### login as user  using this function  ###****/
    public function forceLogin($userId)
    {
        $decryptedUserId = Crypt::decrypt($userId);
        $user = $this->userRepository->getUserById($decryptedUserId);
        if (!empty($user)) {
            \Auth::logout();
            \Auth::loginUsingId($user->id);
            return redirect()->route('dashboard');
        } else {
            return $this->redirectFailure('login', 'Invalid User.');
        }
    }

    public function changePassword($request, $user_id)
    {
        $passwordUpdate = $this->userRepository->update(['password' => Hash::make($request->new_password)], $user_id);

        if ($passwordUpdate) {
            return ['status' => true, 'message' => "Password changed successfully!"];
        } else {
            return ['status' => false, 'message' => "Password not changed!"];
        }
    }

    public function getUserTimeZoneByUser($userId)
    {
        return $this->userSettingRepository->getUserTimeZoneByUser($userId);
    }

    public function insertOrUpdateUserSetting($request, $userId)
    {
        //Preventing resubmission of user timezone
        if ($request['key'] == UserSetting::USER_TIME_ZONE && isset($request['isFromJourneyMode'])) {
            $isFromJourneyMode = $request['isFromJourneyMode'];
            if ($isFromJourneyMode && $request['journeyMode'] == '0') {
                return ['status' => true, 'message' => 'Already added!'];
            }
        }

        $response = $this->userSettingRepository->insertOrUpdate(['key' => $request['key'], 'value' => $request['value']], $userId);
        if ($response) {
            return ['status' => true, 'message' => 'Setting Updated!'];
        } else {
            return ['status' => false, 'message' => 'Setting Not Updated!'];
        }
    }


    public function getApiKeyByUserId($userId)
    {
        return $this->userRepository->getApiKeyByUserId($userId);
    }

    public function findByEmail($email)
    {
        return $this->userRepository->findByEmail($email);
    }


    public function getUserInfoByAuthTokenAndEmail($email, $accessToken)
    {
        return $this->userRepository->getUserInfoByAuthTokenAndEmail($email, $accessToken);
    }

    public function deleteUser($user_id)
    {
        // TODO: Implement deleteUser() method.
        $user_profile_image = $this->getUserById($user_id)['profile_image'];
        $where = [
            'user_id' => $user_id
        ];
        $this->broadcastLogRepository->deleteByWhere($where);
        $this->singleSendStatReportRepository->deleteByWhere($where);

        $delete = $this->userRepository->deleteUserById($user_id);

        if ($delete) {

            return UtilityHelper::RETURN_STATUS_FORMATE('success', 'User has been deleted successfully');

        }
        return UtilityHelper::RETURN_STATUS_FORMATE('error', 'User delete failed! Try again');
    }
}

