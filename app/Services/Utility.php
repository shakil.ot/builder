<?php
namespace App\Services;

use App;
use App\Contracts\Repositories\SettingRepository;
use Bitly;
use Carbon\Carbon;
use DateTimeZone;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use wapmorgan\Mp3Info\Mp3Info;

class Utility
{


    public static function generateRandomStringUpperCase($length = 10)
    {
        $characters = 'ABCDEFGHIJKL0123456789ABCDEFGHIJKL1234567890MNOPQRSTUVWXYZ1234567890MNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function generateRandomString($length = 10)
    {
        $characters = 'MNOPQRSTUVWXYZ12345678900123456789ABCDEFGHIJKL1234567890ABCDEFGHIJKL1234567890MNOPQRSTUVWXYZ1234567890';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function generateRandomStringForCoupon($length = 10)
    {
        $characters = 'MNOPQRSTUVWXYZmnopqrstuvwxyz12345678900123456789ABCDEFGHIJKLabcdefghijkl1234567890ABCDEFGHIJKLabcdefghijkl1234567890MNOPQRSTUVWXYZmnopqrstuvwxyz1234567890';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function generateRandomPassword($length = 10)
    {
        $characters = 'MNOPQRSTUVWXYZ*#@%12345678900123456789ABCDEFGHIJKL1234567890*#@%abcdefghijkl1234567890mnopqrstuvwxyz*#@%';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function generateRandomNumericString($length = 5)
    {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 1; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return '1'.$randomString;
    }



    public static function removeSpecialCharacter($perm1)
    {
        $specialCharacter = array('&', '!', '@', '#', '$', '%', '^', '*', '(', ')', '+', '{', '}', '[', ']', ':', ';', '"', '<', '>', '?', '\\', '/', '~', '`', '=', '|');

        $perm1 = str_replace($specialCharacter, '', $perm1); //remove special charachter
        return preg_replace('/-+/', '-', $perm1); //remove multiple dashes with single
    }

    public static function cleanString($string = null)
    {

        return trim(filter_var($string, FILTER_SANITIZE_STRING));
    }


    public static function addContryCode($mobile)
    {
        if (preg_match("/^(017)|(019)|(018)|(016)|(015)$/", substr($mobile, 0, 3))) {
            return "+88" . $mobile;
        }
        return $mobile;
    }



    public static function shortUrl($url)
    {
        $result = App::make(SettingRepository::class)->getShortnerSettings();
        if ($result && $result['value'] == 2) {
            return Utility::shortUrlBitly($url);
        } else {
            return Utility::shortUrlGoogle($url);
        }
    }

    public static function shortUrlGoogle($url)
    {
        $googl = new GoogleShortner(env('GOOGLE_SHORTNER_API_KEY'));
        return $googl->shorten($url);
    }

    public static function shortUrlBitly($url)
    {
        return Bitly::getUrl($url);
    }

    public static function getVoiceFilePath($userId, $url)
    {
        return asset("/public/upload" . "/message_template/voice/" . $userId . "/" . $url);
    }

    public static function getMMSFilePath($userId, $url)
    {
        return asset("/public/upload/mediaLibrary/mms/" . $userId . "/" . $url);
    }


    public static function msisdnMakeFormat($msisdn){

        $msisdn = trim($msisdn);
        $msisdn = str_replace(")","",$msisdn);
        $msisdn = str_replace("(","",$msisdn);
        $msisdn = str_replace("-","",$msisdn);
        $msisdn = str_replace(" ","",$msisdn);
        $msisdn = str_replace("  ","",$msisdn);
        $msisdn = str_replace("+","",$msisdn);
        $msisdn = trim($msisdn);

        if(is_numeric($msisdn)) {

            if( (strlen($msisdn)==11) && substr($msisdn,0,1)=="1")

                return $msisdn;

            else if(strlen($msisdn)==10)

                return $msisdn =  "1".$msisdn ;

            else
                return false;

        }else {

            return false;

        }
    }

    public static function countCharacterForTexts($text){
        $count = strlen($text);
        if($count == 0) return 1;

        $bar = array(160,310,460,610,760,910,1060,1210,1360,1500);
        $preValue = 0;

        foreach ($bar as $key => $value){
            if($count > $preValue && $count <= $value){
                return ++$key;
            }
            $preValue = $value;
        }
    }

    public static function facebookServiceLog($e,$name){
        Log::error('error code: '.$e->getCode().' Message: '.$e->getMessage().' on '.$name);
    }

    public static function getDemoCampaignMMSFilePath($url)
    {
        return asset("/upload/admin/demo_campaign/mms/" . $url);
    }

    public static function getDemoCampaignVoiceFilePath($url)
    {
        return asset("/upload/admin/demo_campaign/voice/" . $url);
    }

    public static function getAudioFileDuration($target)
    {
        $audio = new Mp3Info($target, true);
        return (floor($audio->duration / 60).'.'.($audio->duration % 60));
    }

    public function fireNodeNotification($params)
    {

        try{
            $this->client = new Client(['base_uri' => env('NODE_SERVER'), 'verify' => false]);

            $this->client->request('POST', "/send-notification", [
                'form_params' => [
                    'room' => "room:" . $params['user_id'],
                    'params' => \GuzzleHttp\json_encode(array_merge($params, array('created_at' => Carbon::now()->toDateTimeString())))
                ]
            ]);
        } catch(\Exception $e){
            Log::error("Error found in Node server connect : ",[$e]);
        }

    }


    public static function format_telephone($phone_number)
    {
        $cleaned = preg_replace('/[^[:digit:]]/', '', $phone_number);
        if (strlen($cleaned) != 11){
            return $cleaned;
        }else{
            preg_match('/(\d{1})(\d{3})(\d{3})(\d{4})/', $cleaned, $matches);
        }
        return "+$matches[1] ($matches[2]) $matches[3]-$matches[4]";
    }


    public static function timezoneSetup()
    {


        $timezone_identifiers =
            DateTimeZone::listIdentifiers(DateTimeZone::ALL);

        $Africa = range(0, 51);
        $America = range(52, 198);
        $Asia = range(211, 292);
        $tz_stamp = time();

        echo "<center><select class='form-control' id='timezone-select' name='timezone'
                        data-parsley-trigger='change focusout'
                        data-parsley-required-message='Timezone is required!'>";

//        echo "<option style='color:#FFF;font-family:Cambria;
//		background-color:#a09;'><h3>Africa</h3>
//	</option>";
//
//        foreach ($Africa as $x) {
//            $tzone[$x] = date_default_timezone_set(
//                $timezone_identifiers[$x]);
//
//            echo "<option value='.$timezone_identifiers[$x].'>" . $timezone_identifiers[$x] .
//                ' @ ' . date('P', $tz_stamp);
//            "</option>";
//        }

        echo "<option style='color:#FFF;font-family:Cambria;
		background-color:#a09;font-size:15px;' disabled='disabled'>
		<h3 >America</h3></option>";

        foreach ($America as $key=>$x) {
            $tzone[$x] = date_default_timezone_set(
                $timezone_identifiers[$x]);
            if($timezone_identifiers[$x] == 'America/New_York'){
                echo "<option value='$timezone_identifiers[$x]' selected>" . str_replace('_',' ',$timezone_identifiers[$x]) .
                    ' @ ' . date('P', $tz_stamp);
                "</option>";
            }
            else{
                echo "<option value='$timezone_identifiers[$x]'>" . str_replace('_',' ',$timezone_identifiers[$x]) .
                    ' @ ' . date('P', $tz_stamp);
                "</option>";
            }

        }

//        echo "<option style='color:#FFF;font-family:Cambria;
//		background-color:#a09;font-size:15px;'>
//		<h3>Asia</h3></option>";
//
//        foreach ($Asia as $x) {
//            $tzone[$x] = date_default_timezone_set(
//                $timezone_identifiers[$x]);
//
//            echo "<option>" . $timezone_identifiers[$x] .
//                ' @ ' . date('P', $tz_stamp);
//            "</option>";
//        }

        echo "</select></center>";


    }

    public static function timezoneSetupForCampaign()
    {


        $timezone_identifiers =
            DateTimeZone::listIdentifiers(DateTimeZone::ALL);

        $Africa = range(0, 51);
        $America = range(52, 198);
        $Asia = range(211, 292);
        $tz_stamp = time();

        echo "<center><select class='form-control' id='timezone-select' name='timezone'
                        data-parsley-trigger='change focusout'
                        data-parsley-required-message='Timezone is required!'>";

//        echo "<option style='color:#FFF;font-family:Cambria;
//		background-color:#a09;'><h3>Africa</h3>
//	</option>";
//
//        foreach ($Africa as $x) {
//            $tzone[$x] = date_default_timezone_set(
//                $timezone_identifiers[$x]);
//
//            echo "<option value='.$timezone_identifiers[$x].'>" . $timezone_identifiers[$x] .
//                ' @ ' . date('P', $tz_stamp);
//            "</option>";
//        }

        echo "<option style='color:#FFF;font-family:Cambria;
		background-color:#a09;font-size:15px;' disabled='disabled'>
		<h3 >America</h3></option>";

        foreach ($America as $key=>$x) {
            $tzone[$x] = date_default_timezone_set(
                $timezone_identifiers[$x]);
            if($timezone_identifiers[$x] == 'America/New_York'){
                echo "<option value='$timezone_identifiers[$x]' selected>".TIMEZONE_FOR_CAMPAIGN[$timezone_identifiers[$x]].
                    ' @ ' . date('P', $tz_stamp);
                "</option>";
            }
            elseif($timezone_identifiers[$x] == 'America/Los_Angeles'){
                echo "<option value='$timezone_identifiers[$x]' selected>".TIMEZONE_FOR_CAMPAIGN[$timezone_identifiers[$x]].
                    ' @ ' . date('P', $tz_stamp);
                "</option>";
            }
            elseif($timezone_identifiers[$x] == 'America/Denver'){
                echo "<option value='$timezone_identifiers[$x]' selected>".TIMEZONE_FOR_CAMPAIGN[$timezone_identifiers[$x]].
                    ' @ ' . date('P', $tz_stamp);
                "</option>";
            }
            elseif($timezone_identifiers[$x] == 'America/Chicago'){
                echo "<option value='$timezone_identifiers[$x]' selected>".TIMEZONE_FOR_CAMPAIGN[$timezone_identifiers[$x]].
                    ' @ ' . date('P', $tz_stamp);
                "</option>";
            }
            elseif($timezone_identifiers[$x] == 'America/Anchorage'){
                echo "<option value='$timezone_identifiers[$x]' selected>".TIMEZONE_FOR_CAMPAIGN[$timezone_identifiers[$x]].
                    ' @ ' . date('P', $tz_stamp);
                "</option>";
            }


//            else{
//                echo "<option value='$timezone_identifiers[$x]'>" . str_replace('_',' ',$timezone_identifiers[$x]) .
//                    ' @ ' . date('P', $tz_stamp);
//                "</option>";
//            }

        }

//        echo "<option style='color:#FFF;font-family:Cambria;
//		background-color:#a09;font-size:15px;'>
//		<h3>Asia</h3></option>";
//
//        foreach ($Asia as $x) {
//            $tzone[$x] = date_default_timezone_set(
//                $timezone_identifiers[$x]);
//
//            echo "<option>" . $timezone_identifiers[$x] .
//                ' @ ' . date('P', $tz_stamp);
//            "</option>";
//        }

        echo "</select></center>";


    }

    public static function achievementFrequency($frequency)
    {
        switch ($frequency) {
            case '0':
                return 'No Restriction';
            case '1':
                return 'Daily';
            case '2':
                return 'Weekly';
            case '3':
                return 'Monthly';
            case '4':
                return 'Yearly';

            default:
                return 'Unidentified';
        }
    }

    public static function filter_type($filter_type)
    {
        switch ($filter_type) {
            case '1':
                return 'EveryOne';
            case '2':
                return 'Groups';
            case '3':
                return 'Individual';
            default:
                return 'Unidentified';
        }
    }



    public static function mailHtml($companyAddress)
    {
        $message = '<html><body>';

        $message .= '<div style="margin-top: 60px; border-top: 1px solid #c0b5c2; text-align: center; background-color: grey; ">';
        $message .= '<table cellspacing="0" style="border: none; width: 100%; font-size: 15px; margin-left: 10px ;margin-top:20px">
            <tr>
            <td>Mailing Address,</td>
            </tr>
           
            <tr>
            <td> 
            '.(isset($companyAddress->address) ? $companyAddress->address." ," : "" ).'
            '.(isset($companyAddress->city) ? $companyAddress->city." ," : "" ).'
            '.(isset($companyAddress->state) ? $companyAddress->state." ," : "" ).'
            '.(isset($companyAddress->zip) ? $companyAddress->zip." ," : "" ).',
            '.(isset($companyAddress->country) ? $companyAddress->country." " : "" ).'
            </td>
            </tr>
            
            </table>';
        $message .= '</div></body></html>';

        return $message;
    }

}
