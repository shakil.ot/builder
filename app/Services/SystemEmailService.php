<?php
/**
 * User: Rafiqul Islam
 * Date: 7/16/20
 * Time: 12:56 PM
 */

namespace App\Services;


use App\Contracts\Repository\SystemEmailRepository;
use App\Contracts\Service\SystemEmailContact;

class SystemEmailService implements SystemEmailContact
{
    /**
     * @var SystemEmailRepository
     */
    private $systemEmailRepo;

    /**
     * SystemEmailService constructor.
     * @param SystemEmailRepository $systemEmailRepo
     */
    public function __construct(SystemEmailRepository $systemEmailRepo)
    {

        $this->systemEmailRepo = $systemEmailRepo;
    }

    public function getSystemEmailByType($type)
    {
        return $this->systemEmailRepo->getSystemEmailByType($type);
    }
}
