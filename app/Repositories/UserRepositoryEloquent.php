<?php

namespace App\Repositories;

use App\Contracts\Repository\UserRepository;
use App\Repositories\BaseRepository\BaseRepository;
use App\User;
use Modules\Package\Entities\UserBalance;

class UserRepositoryEloquent extends BaseRepository implements UserRepository
{
    protected function model()
    {
        return new User();
    }

    public function getAll($request = null)
    {

        $allUser = $this->model->orderBy('id', 'desc');
        if ($request != null) {


            if ($request->parent_email_search != null) {
                $email = $request->parent_email_search;
                $allUser->where('email', 'like', '%' . $email . '%');
            }

            if ($request->created_at_from_search != null) {
                $allUser = $allUser->whereDate('created_at', '>=', $request->created_at_from_search);
            }

            if ($request->created_at_to_search != null) {
                $allUser = $allUser->whereDate('created_at', '<=', $request->created_at_to_search);
            }
        }


        return $allUser;
    }

    public function getTotalUser()
    {
        return $this->model()::count();
    }

    public function getAllSubUser($request = null, $parent_id)
    {
        $allSubUser = User::orderBy('id', 'desc')->where('parent_id', $parent_id);
        if ($request != null) {
            if ($request->parent_email_search != null) {
                $email = $request->parent_email_search;
                $allSubUser->where('email', 'like', '%' . $email . '%');
            }

            if ($request->created_at_from_search != null) {
                $allSubUser = $allSubUser->whereDate('created_at', '>=', $request->created_at_from_search);
            }

            if ($request->created_at_to_search != null) {
                $allSubUser = $allSubUser->whereDate('created_at', '<=', $request->created_at_to_search);
            }
        }
        return $allSubUser;
    }

    public function getAllUnregisteredUsers()
    {
        return $this->model->where('status',User::STATUS_UNREGISTERED)->orderBy('updated_at','desc');
    }

    public function createUser($request)
    {
        return $this->model->create($request);
    }

    public function updateUserByParam($where, $param)
    {
        return $this->model->where($where)
            ->update($param);
    }

    public function getCountryByUserId($userId)
    {
        return $this->model->where('id',$userId)
            ->select('country')
            ->first();
    }

    public function getUserById($user_id)
    {
        return $this->model->where('id', $user_id)->first();
    }

    public function getUserByName($userName)
    {
        return $this->model->where('first_name', $userName)->first();
    }

    public function getUserByEmail($email)
    {
        return $this->model->where('email', $email)->first();
    }

    public function deleteUserById($user_id)
    {
        return $this->model->where('id', $user_id)->delete();
    }

    public function deleteUserByParam($where)
    {
        return $this->model->where($where)->delete();
    }

    public function deactivateUser($userId)
    {
        return $this->model->where('id', $userId)
            ->update(['status' => User::STATUS_INACTIVE]);
    }

    public function activateUser($userId)
    {
        return $this->model->where('id', $userId)
            ->update(['status' => User::STATUS_ACTIVE]);
    }

    public function login_as_user($user_id)
    {
        Auth::guard('web')->logout();
        return Auth::guard('web')->loginUsingId($user_id);
    }

    public function getSubUsers($parentId)
    {
        return $this->model->orderBy('users.id', 'desc')
            ->get();
    }



    public function checkHasUserEmailExists($email)
    {
        // TODO: Implement checkHasUserEmailExist() method.
        return $this->model->where('email', $email)->exists();
    }

    public function checkHasUserPhoneExists($phone)
    {
        // TODO: Implement checkHasUserPhoneExists() method.
        return $this->model->where('phone', $phone)->exists();
    }




    public function CheckParentAndGetId($id)
    {
        // TODO: Implement CheckParentAndGetId() method.
        return $this->model->where('id', '=', $id)->first();
    }

    public function setEmailAuthToken($userId,$email,$token)
    {
        return $this->model->where('id', '=', $userId)
            ->update([
                'access_token' => $token,
                'auth_email' => $email
            ]);
    }

    public function disconnectEmail($userId)
    {
        return $this->model->where('id', '=', $userId)
            ->update([
                'access_token' => null,
                'auth_email' => null
            ]);
    }

    public function getUserStatusByParam($where)
    {
        return $this->model->where($where)->select('status')->first();
    }

    public function getUserAuthToken($userId){
        return $this->model->where('id', $userId)->select('auth_token')
            ->first();
    }

    public function setOnboardStep($userId, $current_step)
    {
        return $this->model->where('id', $userId)
            ->where('journey_mode_last_landing_step', '<', $current_step)
            ->orWhereNull('journey_mode_last_landing_step', '=', null)
            ->update(['journey_mode_last_landing_step' => $current_step]);
    }

    public function getApiKeyByUserId($userId)
    {
        return User::where('id', $userId)->select('access_token')->first();

    }

    public function findByEmail($email)
    {
        return User::where('email', $email)
            ->select('access_token','password')
            ->first();
    }



    public function getUserInfoByAuthTokenAndEmail($email, $accessToken)
    {
        return User::where('email', $email)
            ->where('access_token', $accessToken)
            ->first();

    }

}

