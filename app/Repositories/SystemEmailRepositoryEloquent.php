<?php
/**
 * User: Rafiqul Islam
 * Date: 7/16/20
 * Time: 12:57 PM
 */

namespace App\Repositories;


use App\Contracts\Repository\SystemEmailRepository;
use App\Models\SystemEmail;
use App\Repositories\BaseRepository\BaseRepository;

class SystemEmailRepositoryEloquent extends BaseRepository implements SystemEmailRepository
{

    protected function model()
    {
        return new SystemEmail();
    }

    public function getSystemEmailByType($type)
    {
        return $this->model->where('type', $type)->first();
    }
}
