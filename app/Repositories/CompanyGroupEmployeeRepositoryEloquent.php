<?php
/**
 * User: Rafiqul Islam
 * Date: 6/24/20
 * Time: 12:45 PM
 */

namespace App\Repositories;


use App\Contracts\Repository\CompanyGroupEmployeeRepository;
use App\Repositories\BaseRepository\BaseRepository;
use Modules\UserEmployee\Entities\CompanyGroupEmployee;

class CompanyGroupEmployeeRepositoryEloquent extends BaseRepository implements CompanyGroupEmployeeRepository
{
    public function model()
    {
        return new CompanyGroupEmployee();
    }


    public function getCompanyGroupIdByEmployeeId($employeeId)
    {
        return $this->model
            ->with(['company_rewards'])
            ->where(['employee_id'=> $employeeId ])
            ->get();
    }


}
