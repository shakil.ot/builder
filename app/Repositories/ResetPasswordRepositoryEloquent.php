<?php

namespace App\Repositories;
use App\Contracts\Repository\ResetPasswordRepository;
use App\Models\PasswordReset;
use App\User;

class ResetPasswordRepositoryEloquent implements ResetPasswordRepository
{

    public function insertResetPasswordRequest($param)
    {
        return PasswordReset::create($param);
    }

    public function getInformationByEmail($token, $email)
    {
        return PasswordReset::where(['email'=>$email, 'is_used'=> PasswordReset::FORGET_PASSWORD_NOT_USED])->orderBy('created_at','desc')->get();
    }
    public function getInformationByLastUpdatedEmail($email)
    {
        return PasswordReset::where('email',$email)
            ->orderBy('updated_at','desc')->first();
    }

    public function deleteInformationByEmail($email)
    {
        return PasswordReset::where(['email'=>$email])->delete();
    }
    public function updateResetPasswordById($id, $data)
    {
        return PasswordReset::where('id',$id)->update($data);
    }
}

