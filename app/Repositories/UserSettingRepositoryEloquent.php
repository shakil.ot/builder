<?php

namespace App\Repositories;

use App;
use App\Contracts\Repository\UserSettingRepository;
use App\Models\UserSetting;
use App\Repositories\BaseRepository\BaseRepository;
use DB;


/**
 * Class UserRepositoryEloquent
 * @package namespace App\Repositories;
 */
class UserSettingRepositoryEloquent extends BaseRepository implements UserSettingRepository
{


    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return new UserSetting();
    }

    public function insertUserSetting($data)
    {
        return $this->create($data);
    }

    public function insertOrUpdate($request, $userId)
    {

        return $this->updateOrCreate([
            'user_id' => $userId,
            'key' => $request['key']
        ], [
            'value' => $request['value']
        ]);
    }
    public function updateUserSettingByParam($where, $data)
    {
        return $this->model()->where($where)->update($data);
    }

    public function getSettingByUserId($userId)
    {
        return $this->model()
            ->where('user_id', $userId)
            ->get();
    }

    public function getPackageSettingByUser($user_id)
    {
        return $this->model()
            ->where('user_id', $user_id)
            ->where('key', UserSetting::RECURRENT_ON_OFF)
            ->first();
    }

    public function getValueByKey($key)
    {
        return $this->findByField('key', $key)->first();
    }

    public function getCutOffStartHourByUser($userId)
    {
        return $this->model()
            ->where('user_id', $userId)
            ->where('key', UserSetting::CUT_OFF_START_TIME)
            ->first();
    }


    public function getCutOffEndHourByUser($userId)
    {
        return $this->model()
            ->where('user_id', $userId)
            ->where('key', UserSetting::CUT_OFF_END_TIME)
            ->first();
    }

    public function getCutOffTimeByUser($userId)
    {
        return $this->model()
            ->where('user_id', $userId)
            ->whereIn('key', [UserSetting::CUT_OFF_START_TIME,UserSetting::CUT_OFF_END_TIME])
            ->select('key','value')
            ->get();
    }

    public function getCutOffDayByUser($userId)
    {
        return $this->model()
            ->where('user_id', $userId)
            ->whereIn('key', [UserSetting::SEND_FOLLOWUP_ON_SATURDAY,UserSetting::SEND_FOLLOWUP_ON_SUNDAY])
            ->select('key','value')
            ->get();
    }

    public function getUserTimeZoneByUser($userId)
    {
        return $this->model()
            ->where('user_id', $userId)
            ->where('key', UserSetting::USER_TIME_ZONE)
            ->first();
    }

    public function getSettingByUserIdAndKey($userId, $key)
    {
        return $this->model()
            ->where('user_id', $userId)
            ->where('key', $key)
            ->first();
    }

    public function checkCreditStatusByUserIdAndKey($userId, $key)
    {
        $settings = $this->model()
            ->where('user_id', $userId)
            ->where('key', $key)
            ->first();

        if ($settings) {
            $settings = $settings->status;
            return $settings;
        } else {
            return null;
        }
    }

    public function getTimezoneByUser($userId)
    {
        return UserSetting::where('user_id', $userId)
            ->where('key', UserSetting::USER_TIME_ZONE)
            ->first();
    }
}
