<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSetting extends Model
{

    const RECURRENT_ON_OFF = 1;
    const RECURRENT_ON_STATUS = 1;
    const RECURRENT_OFF_STATUS = 0;
    const CUT_OFF_START_TIME = 3;
    const CUT_OFF_END_TIME = 4;
    const CUT_OFF_DATE = 5;
    const USER_TIME_ZONE = 6;
    const SEND_FOLLOWUP_ON_SATURDAY = 7;
    const SEND_FOLLOWUP_ON_SUNDAY = 8;

    const SEND_ON_SAT_TRUE = 1;
    const SEND_ON_SAT_FALSE = 0;
    const SEND_ON_SUN_TRUE = 1;
    const SEND_ON_SUN_FALSE = 0;


    const LOOK_UP_PERMISSION_KEY = 13;
    const LOOK_UP_PERMISSION_STATUS_ON = 1;
    const LOOK_UP_PERMISSION_STATUS_OFF = 0;

    protected $fillable = [
        'user_id',
        'key',
        'value',
        'status',
    ];


    public function getTimezoneByUser($userId)
    {
        return UserSetting::where('user_id', $userId)
            ->where('key', UserSetting::USER_TIME_ZONE)
            ->first();
    }
}
