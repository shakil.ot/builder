<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{
    const FORGET_PASSWORD_USED = 1;
    const FORGET_PASSWORD_NOT_USED = 0;

    protected $fillable = [
        'email', 'token', 'expire_time', 'is_used'
    ];
}
