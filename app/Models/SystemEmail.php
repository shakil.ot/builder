<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SystemEmail extends Model
{
    const RECURRING_FAIL = 1;
    const RECHARGE_FAIL = 2;
    const CUT_OF_TIME = 3;
    const ACCOUNT_DELETE = 4;
    const ACCOUNT_CREATE = 5;
    const RECURRING_SUCCESS = 6;

    protected $fillable = [
        'type',
        'subject',
        'body'
    ];

    public static $allType = [
        self::RECURRING_FAIL => 'Send email on subscription recurring fail',
        self::RECHARGE_FAIL => 'Send email on auto credit recharge fail',
        self::CUT_OF_TIME => 'Send email if campaign is in cut off time',
        self::ACCOUNT_DELETE => 'Send email on account delete',
        self::ACCOUNT_CREATE => 'Send email on account registration/create',
        self::RECURRING_SUCCESS => 'Send email on subscription recurring success'
    ];

    public static function getType($type)
    {
        return !empty(self::$allType[$type]) ? self::$allType[$type] : '';
    }
}
