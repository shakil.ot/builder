<?php
namespace App\Models;

class PersonalizeType
{

    const FIRST_NAME = 'First name';
    const LAST_NAME = 'Last Name';
    const EMAIL = 'Email';
    const NUMBER = 'Number';

    //New added
    const MY_FIRST_NAME = 'My First Name';
    const MY_LAST_NAME = 'My Last Name';
    const MY_EMAIL = 'My Email';
    const MY_NUMBER = 'My Number';

    const OFFER_PAGE = "Offer Page";

    public static $allType = [

        self::FIRST_NAME => '[[first_name]]',
        self::LAST_NAME => '[[last_name]]',
        self::EMAIL => '[[email]]',
        self::NUMBER => '[[number]]',
        self::MY_FIRST_NAME => '[[my_first_name]]',
        self::MY_LAST_NAME => '[[my_last_name]]',
        self::MY_EMAIL => '[[my_email]]',
        self::MY_NUMBER => '[[my_number]]',
        self::OFFER_PAGE => '[[offer_page]]'
    ];

    public static function  getTypes()
    {
        return self::$allType;
    }

    public static function  getTypesById($id)
    {
        return !empty(self::$allType[$id]) ? self::$allType[$id] : '';
    }




}
