<?php
namespace App\Contracts\Service;


Interface ResetPasswordContact
{
    public function insertResetPasswordRequest($param);

    public function getInformationByEmail($token, $email);

    public function getInformationByLastUpdatedEmail($email);

    public function deleteInformationByEmail($email);

    public function updateResetPasswordById($id, $data);

}
