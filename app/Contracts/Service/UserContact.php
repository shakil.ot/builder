<?php
/**
 * Created by PhpStorm.
 * User: mohi
 * Date: 3/23/17
 * Time: 1:33 PM
 */

namespace App\Contracts\Service;


interface UserContact
{
    public function getAllUser($request = null);

    public function getTotalUser();

    public function getUserByEmail($email);

    public function forgetPasswordCodeSet($response);

    public function createUser($request);

    public function listUserData($request);

    public function getUserById($user_id);

    public function update($request);

    public function updateUser($request);

    public function getCountryByUserId($userId);

    public function updateUserByParam($where, $param);


    public function updateProfileImage($file, $userId);

    public function getViewUserInformation($request);

    public function stopAutoRecurring($userId);

    public function startAutoRecurring($userId);

    public function forceLogin($userId);

    public function getFilterData($company_id, $type);

    public function changePassword($request, $user_id);

    public function getUserTimeZoneByUser($userId);

    public function insertOrUpdateUserSetting($request, $userId);

    public function getApiKeyByUserId($userId);

    public function findByEmail($email);

    public function getUserInfoByAuthTokenAndEmail($email, $accessToken);

    public function deleteUser($user_id);

}
