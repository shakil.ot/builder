<?php
/**
 * User: Rafiqul Islam
 * Date: 7/16/20
 * Time: 12:55 PM
 */

namespace App\Contracts\Service;


interface SystemEmailContact
{

    public function getSystemEmailByType($type);
}
