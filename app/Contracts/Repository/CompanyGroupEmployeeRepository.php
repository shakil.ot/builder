<?php

namespace App\Contracts\Repository;

use Illuminate\Http\Request;

/**
 * Interface UserRepository
 * @package namespace App\Contracts\Repositories;
 */
interface CompanyGroupEmployeeRepository
{

    public function getCompanyGroupIdByEmployeeId($employeeId);

}
