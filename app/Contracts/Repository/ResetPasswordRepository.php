<?php

namespace App\Contracts\Repository;

interface ResetPasswordRepository
{
    public function insertResetPasswordRequest($param);

    public function getInformationByEmail($token, $email);

    public function getInformationByLastUpdatedEmail($email);

    public function deleteInformationByEmail($email);

    public function updateResetPasswordById($id, $data);

}
