<?php
/**
 * User: Rafiqul Islam
 * Date: 7/16/20
 * Time: 12:56 PM
 */

namespace App\Contracts\Repository;


interface SystemEmailRepository
{

    public function getSystemEmailByType($type);
}
