<?php

namespace App\Contracts\Repository;

interface UserRepository
{
    public function getAll($request = null);

    public function getTotalUser();

    public function getAllSubUser($request = null, $parent_id);

    public function getAllUnregisteredUsers();

    public function createUser($request);

    public function updateUserByParam($where, $param);

    public function getCountryByUserId($userId);

    public function getUserById($user_id);

    public function getUserByName($userName);

    public function getUserByEmail($email);

    public function deleteUserById($user_id);

    public function deleteUserByParam($where);

    public function deactivateUser($user_id);

    public function activateUser($user_id);

    public function login_as_user($user_id);

    public function getSubUsers($user_id);


    public function checkHasUserEmailExists($email);

    public function checkHasUserPhoneExists($phone);


    public function CheckParentAndGetId($id);

    public function setEmailAuthToken($userId, $email, $token);

    public function disconnectEmail($userId);

    public function getUserStatusByParam($where);

    public function getUserAuthToken($userId);

    public function setOnboardStep($user_id, $current_step);

    public function getApiKeyByUserId($userId);

    public function findByEmail($email);

    public function getUserInfoByAuthTokenAndEmail($email, $accessToken);



}
