<?php

namespace App\Contracts\Repository;

use Illuminate\Http\Request;

/**
 * Interface UserRepository
 * @package namespace App\Contracts\Repositories;
 */
interface UserSettingRepository
{

    public function insertOrUpdate($request, $userId);

    public function updateUserSettingByParam($where, $data);

    public function getSettingByUserId($userId);

    public function getTimezoneByUser($userId);

    public function getPackageSettingByUser($user_id);

    public function getValueByKey($key);

    public function getCutOffStartHourByUser($userId);

    public function getCutOffEndHourByUser($userId);

    public function getCutOffTimeByUser($userId); //For both cut off start and end.

    public function getCutOffDayByUser($userId);   //For both cutoff day saturday and sunday

    public function getUserTimeZoneByUser($userId);

    public function getSettingByUserIdAndKey($userId, $key);

    public function insertUserSetting($data);

    public function checkCreditStatusByUserIdAndKey($userId, $key);
}
