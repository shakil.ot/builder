<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Modules\UserDashboard\Contracts\Service\AutoFollowupContact;

class AutoFollowupSend extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:auto-followup-send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Auto Follow up message send ';
    /**
     * @var AutoFollowupContact
     */
    private $autoFollowupService;

    public function __construct(AutoFollowupContact $autoFollowupService)
    {
        parent::__construct();

        $this->autoFollowupService = $autoFollowupService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {

            $this->autoFollowupService->sendAutoFollowupMessageToSubscriber();

        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            Log::info('Broadcast exception');;
        }
    }
}
