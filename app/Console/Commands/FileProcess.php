<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Modules\Contact\Http\Services\FileService;

class FileProcess extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:file-process';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'File Processing start every five minutes';
    /**
     * @var FileService
     */
    private $fileService;

    /**
     * Create a new command instance.
     *
     * @param FileService $fileService
     */
    public function __construct(FileService $fileService)
    {
        parent::__construct();
        $this->fileService = $fileService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->fileService->processFileList();

        }catch (\Exception $e){

        }
    }
}
