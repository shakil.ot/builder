<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Modules\Package\Contracts\Service\PackageContact;

class AutoRenewPackage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:renew-package';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process user Renew Package';

    private $fileService;
    /**
     * @var PackageContact
     */
    private $packageService;


    public function __construct(PackageContact $packageService)
    {
        parent::__construct();

        $this->packageService = $packageService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            if ($this->packageService->autoRenew()) {
                Log::info("Auto renew run at: ".date("Y-m-d"));
            }
        }
        catch (Exception $e) {
            Log::error("Package Re-new Error On: ". $e);
        }
    }
}
