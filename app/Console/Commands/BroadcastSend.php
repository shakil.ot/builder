<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Modules\Broadcast\Http\Services\BroadcastService;

class BroadcastSend extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:broadcast';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Broadcast send ';
    /**
     * @var BroadcastService
     */
    private $broadcastService;

    /**
     * Create a new command instance.
     *
     * @param BroadcastService $broadcastService
     */
    public function __construct(BroadcastService $broadcastService)
    {
        parent::__construct();
        $this->broadcastService = $broadcastService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {

            $this->broadcastService->broadcastProcess();

        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            Log::info('Broadcast exception');;
        }
    }
}
