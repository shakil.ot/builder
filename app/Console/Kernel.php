<?php

namespace App\Console;

use App\Console\Commands\AutoFollowupSend;
use App\Console\Commands\AutoRenewPackage;
use App\Console\Commands\BroadcastSend;
use App\Console\Commands\FileProcess;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        AutoRenewPackage::class,
        FileProcess::class,
        BroadcastSend::class,
        AutoFollowupSend::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('process:auto-followup-send')->everyMinute();
        $schedule->command('process:broadcast')->everyFiveMinutes();
        $schedule->command('process:file-process')->everyFiveMinutes();
        $schedule->command('process:renew-package')->dailyAt(01);
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
