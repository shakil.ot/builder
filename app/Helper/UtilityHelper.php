<?php

namespace App\Helper;
//use App\Contracts\Service\VirtualNumberContact;
use Carbon\Carbon;
use DateTime;
use DateTimeZone;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

use wapmorgan\Mp3Info\Mp3Info;

class UtilityHelper
{

    public static function image_resize($target, $image, $w, $h, $ext)
    {
        list($w_orginal, $h_orginal) = getimagesize($target);
        $scale_ratio = $w_orginal / $h_orginal;
        if ($ext == 'png' || $ext == 'PNG') {
            $img = imagecreatefrompng($target);
        } else if ($ext == 'jpeg' || $ext == 'JPEG') {
            $img = imagecreatefromjpeg($target);
        } else {
            $img = imagecreatefromjpeg($target);
        }

        $tci = imagecreatetruecolor($w, $h);
        //Below two lines are used for making image background transparent
        $white = imagecolorallocate($tci, 255, 255, 255);
        imagefill($tci, 0, 0, $white);
        imagecopyresampled($tci, $img, 0, 0, 0, 0, $w, $h, $w_orginal, $h_orginal);
        imagejpeg($tci, $image, 80);
        chmod($image, 0777);
        return true;
    }

    public static function image_resizeFromFilePath($target, $image, $w, $h, $ext)
    {
        list($w_orginal, $h_orginal) = getimagesize($target);
        $scale_ratio = $w_orginal / $h_orginal;
        if ($ext == 'png' || $ext == 'PNG') {
            $img = imagecreatefrompng($target);
        } else if ($ext == 'jpeg' || $ext == 'JPEG') {
            $img = imagecreatefromjpeg($target);
        } else {
            $img = imagecreatefromjpeg($target);
        }

        $tci = imagecreatetruecolor($w, $h);
        imagecopyresampled($tci, $img, 0, 0, 0, 0, $w, $h, $w_orginal, $h_orginal);
        imagejpeg($tci, $image, 80);
        chmod($image, 0777);
        return true;
    }

    public static function compoundingPeriod($day)
    {
        $day = (int)$day;

        if ($day == 30) {
            return 'Monthly';
        }

        if ($day == 365) {
            return 'Yearly';
        }

        if ($day == 180) {
            return 'Half yearly';
        }
        if ($day == 90) {
            return 'Quarterly';
        }

        if ($day > 730) {
            return 'Lifetime';
        }

        return 'By '. $day.' Days';

    }

    public static function getUserLocation()
    {
        $ip = '';
        if (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '');
        }

        $endpoint = "http://ip-api.com/php/" . $ip;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $endpoint);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $output = curl_exec($ch);
        curl_close($ch);

        $response = unserialize($output);
        return $response['status'] == "fail" ? "International" : $response['country'];
    }

    public static function getUserIp()
    {
        $ip = '';
        if (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '');
        }
        return $ip;
    }
    public static function countCharacterForTexts($text)
    {
        $count = strlen($text);
        if ($count == 0) return 1;

        $bar = array(160, 310, 460, 610, 760, 910, 1060, 1210, 1360, 1500);
        $preValue = 0;

        foreach ($bar as $key => $value) {
            if ($count > $preValue && $count <= $value) {
                return ++$key;
            }
            $preValue = $value;
        }
    }

    public static function getAudioFileDuration($target)
    {

        $audio = new Mp3Info($target, true);
        return (floor($audio->duration / 60) . '.' . ($audio->duration % 60));
    }

    public static function preparedPagination(
        $limit, $active_list, $total_list, $select_structure = 7,
        $click_class = 'conversation-pagination-list', $active_class_custom = 'active-pagination-list-conversation',
        $extra_classes_in_nav = '', $extra_classes_in_ul = '', $extra_classes_in_li = '', $extra_classes_in_a = '',
        $remove_nav = false)
    {
        //total list is for total count
        //total selected structure = 3,5,7
        //click class for each click on li
        //active class custom for change the design of active li
        // remove nav for show or hide nav tag
        $pagination = '';

        if ($total_list / $limit > 1) {
            if ($remove_nav == false) {
                $pagination .= '<nav aria-label="Page navigation ' . $extra_classes_in_nav . '">';
            }
            $pagination .= '<ul class="pagination ' . $extra_classes_in_ul . '">';
            for ($i = 0; $i < (int)ceil($total_list / $limit); $i++) {
                //create structure for different selected structure
                if ($select_structure == 7) {
                    $active = '';
                    if ($total_list > 7) {
                        if ($active_list < 4) {
                            if ($active_list == $i) {
                                $active = $active_class_custom;
                            }
                            if ($i < 5) {
                                $pagination .= '<li data-id="' . $i . '" class="page-item ' . $click_class . ' ' . $extra_classes_in_li . '"> <a class="page-link ' . $active . ' ' . $extra_classes_in_a . '" href="javascript:void(0)">' . ($i + 1) . '</a></li>';
                            } else {
                                if ($i == ((int)ceil($total_list / $limit) - 2)) {
                                    $pagination .= '<li data-id="null" class="page-item disabled' . $extra_classes_in_li . '"> <a class="page-link ' . $active . ' ' . $extra_classes_in_a . '" href="javascript:void(0)">...</a></li>';
                                } elseif ($i == ((int)ceil($total_list / $limit) - 1)) {
                                    $pagination .= '<li data-id="' . $i . '" class="page-item ' . $click_class . ' ' . $extra_classes_in_li . '"> <a class="page-link ' . $active . ' ' . $extra_classes_in_a . '" href="javascript:void(0)">>></a></li>';
                                }
                            }
                        } else if (((int)ceil($total_list / $limit) - $active_list) <= 4) {
                            if ($active_list == $i) {
                                $active = $active_class_custom;
                            }
                            if ($i >= ((int)ceil($total_list / $limit) - 5)) {
                                $pagination .= '<li data-id="' . $i . '" class="page-item ' . $click_class . ' ' . $extra_classes_in_li . '"> <a class="page-link ' . $active . ' ' . $extra_classes_in_a . '" href="javascript:void(0)">' . ($i + 1) . '</a></li>';
                            } else {
                                if ($i == 0) {
                                    $pagination .= '<li data-id="' . $i . '" class="page-item ' . $click_class . ' ' . $extra_classes_in_li . '"> <a class="page-link ' . $extra_classes_in_a . '" href="javascript:void(0)"><<</a></li>';
                                } elseif ($i == 1) {
                                    $pagination .= '<li data-id="null" class="page-item disabled' . $extra_classes_in_li . '"> <a class="page-link ' . $extra_classes_in_a . '" href="javascript:void(0)">...</a></li>';
                                } else {
                                    continue;
                                }
                            }
                        } else {
                            $pagination .= '<li data-id="0" class="page-item ' . $click_class . ' ' . $extra_classes_in_li . '"> <a class="page-link ' . $extra_classes_in_a . '" href="javascript:void(0)"><<</a></li>';

                            $pagination .= '<li data-id="null" class="page-item disabled' . $extra_classes_in_li . '"> <a class="page-link ' . $active . ' ' . $extra_classes_in_a . '" href="javascript:void(0)">...</a></li>';

                            $pagination .= '<li data-id="' . ($active_list - 1) . '" class="page-item ' . $click_class . ' ' . $extra_classes_in_li . '"> <a class="page-link ' . $extra_classes_in_a . '" href="javascript:void(0)">' . ($active_list) . '</a></li>';

                            $pagination .= '<li data-id="' . ($active_list) . '" class="page-item ' . $click_class . ' ' . $extra_classes_in_li . '"> <a class="page-link ' . $active_class_custom . ' ' . $extra_classes_in_a . '" href="javascript:void(0)">' . ($active_list + 1) . '</a></li>';

                            $pagination .= '<li data-id="' . ($active_list + 1) . '" class="page-item ' . $click_class . ' ' . $extra_classes_in_li . '"> <a class="page-link ' . $extra_classes_in_a . '" href="javascript:void(0)">' . ($active_list + 2) . '</a></li>';

                            $pagination .= '<li data-id="null" class="page-item disabled ' . $extra_classes_in_li . '"> <a class="page-link ' . $extra_classes_in_a . '" href="javascript:void(0)">...</a></li>';

                            $pagination .= '<li data-id="' . (ceil($total_list / $limit) - 1) . '" class="page-item ' . $click_class . ' ' . $extra_classes_in_li . '"> <a class="page-link ' . $extra_classes_in_a . '" href="javascript:void(0)">>></a></li>';

                            break;
                        }
                    } else {
                        if ($active_list == $i) {
                            $active = $active_class_custom;
                        }
                        $pagination .= '<li data-id="' . $i . '" class="page-item ' . $click_class . ' ' . $extra_classes_in_li . '"> <a class="page-link ' . $active . ' ' . $extra_classes_in_a . '" href="javascript:void(0)">' . ($i + 1) . '</a></li>';
                    }

                } elseif ($select_structure == 5) {

                } elseif ($select_structure == 3) {
                    if ($total_list > 3) {
                        if ($active_list = 0) {

                        } elseif ($active_list == ((int)ceil(($total_list / $limit)) - 1)) {

                        } else {

                        }
                    } else {

                    }

                } elseif ($select_structure > 7) {
                    $active = '';
                    if ($total_list > $select_structure) {
                        if ($active_list < ($select_structure - 3)) {
                            if ($active_list == $i) {
                                $active = $active_class_custom;
                            }
                            if ($i < ($select_structure - 2)) {
                                $pagination .= '<li data-id="' . $i . '" class="page-item ' . $click_class . ' ' . $extra_classes_in_li . '"> <a class="page-link ' . $active . ' ' . $extra_classes_in_a . '" href="javascript:void(0)">' . ($i + 1) . '</a></li>';
                            } else {
                                if ($i == ((int)ceil($total_list / $limit) - 2)) {
                                    $pagination .= '<li data-id="null" class="page-item disabled' . $extra_classes_in_li . '"> <a class="page-link ' . $active . ' ' . $extra_classes_in_a . '" href="javascript:void(0)">...</a></li>';
                                } elseif ($i == ((int)ceil($total_list / $limit) - 1)) {
                                    $pagination .= '<li data-id="' . $i . '" class="page-item ' . $click_class . ' ' . $extra_classes_in_li . '"> <a class="page-link ' . $active . ' ' . $extra_classes_in_a . '" href="javascript:void(0)">>></a></li>';
                                }
                            }
                        } else if (((int)ceil($total_list / $limit) - $active_list) <= ($select_structure - 3)) {
                            if ($active_list == $i) {
                                $active = $active_class_custom;
                            }
                            if ($i >= ((int)ceil($total_list / $limit) - ($select_structure - 2))) {
                                $pagination .= '<li data-id="' . $i . '" class="page-item ' . $click_class . ' ' . $extra_classes_in_li . '"> <a class="page-link ' . $active . ' ' . $extra_classes_in_a . '" href="javascript:void(0)">' . ($i + 1) . '</a></li>';
                            } else {
                                if ($i == 0) {
                                    $pagination .= '<li data-id="' . $i . '" class="page-item ' . $click_class . ' ' . $extra_classes_in_li . '"> <a class="page-link ' . $extra_classes_in_a . '" href="javascript:void(0)"><<</a></li>';
                                } elseif ($i == 1) {
                                    $pagination .= '<li data-id="null" class="page-item disabled' . $extra_classes_in_li . '"> <a class="page-link ' . $extra_classes_in_a . '" href="javascript:void(0)">...</a></li>';
                                } else {
                                    continue;
                                }
                            }
                        } else {
                            //todo first two list
                            $pagination .= '<li data-id="0" class="page-item ' . $click_class . ' ' . $extra_classes_in_li . '"> <a class="page-link ' . $extra_classes_in_a . '" href="javascript:void(0)"><<</a></li>';

                            $pagination .= '<li data-id="null" class="page-item disabled' . $extra_classes_in_li . '"> <a class="page-link ' . $active . ' ' . $extra_classes_in_a . '" href="javascript:void(0)">...</a></li>';

                            //todo dynamic list start
                            //todo before active list
                            for ($loop = 0; $loop < (int)ceil(($select_structure - 5) / 2); $loop++) {
                                $pagination .= '<li data-id="' . ($active_list - ($loop + 1)) . '" class="page-item ' . $click_class . ' ' . $extra_classes_in_li . '"> <a class="page-link ' . $extra_classes_in_a . '" href="javascript:void(0)">' . ($active_list - $loop) . '</a></li>';
                            }

                            $pagination .= '<li data-id="' . ($active_list) . '" class="page-item ' . $click_class . ' ' . $extra_classes_in_li . '"> <a class="page-link ' . $active_class_custom . ' ' . $extra_classes_in_a . '" href="javascript:void(0)">' . ($active_list + 1) . '</a></li>';

                            //todo after active list
                            for ($loop = 0; $loop < (int)floor(($select_structure - 5) / 2); $loop++) {
                                $pagination .= '<li data-id="' . ($active_list + ($loop + 1)) . '" class="page-item ' . $click_class . ' ' . $extra_classes_in_li . '"> <a class="page-link ' . $extra_classes_in_a . '" href="javascript:void(0)">' . ($active_list + ($loop + 2)) . '</a></li>';
                            }
                            //todo dynamic list end


                            //todo last two list
                            $pagination .= '<li data-id="null" class="page-item disabled ' . $extra_classes_in_li . '"> <a class="page-link ' . $extra_classes_in_a . '" href="javascript:void(0)">...</a></li>';

                            $pagination .= '<li data-id="' . (ceil($total_list / $limit) - 1) . '" class="page-item ' . $click_class . ' ' . $extra_classes_in_li . '"> <a class="page-link ' . $extra_classes_in_a . '" href="javascript:void(0)">>></a></li>';

                            break;
                        }
                    } else {
                        if ($active_list == $i) {
                            $active = $active_class_custom;
                        }
                        $pagination .= '<li data-id="' . $i . '" class="page-item ' . $click_class . ' ' . $extra_classes_in_li . '"> <a class="page-link ' . $active . ' ' . $extra_classes_in_a . '" href="javascript:void(0)">' . ($i + 1) . '</a></li>';
                    }
                }
            }
            $pagination .= '</ul>';
            if ($remove_nav == false) {
                $pagination .= '</nav>';
            }
        }

        return $pagination;
    }

    public static function getVirtualNumberDataById($id)
    {
        //$data =  App::make(VirtualNumberContact::class)->getById($id);
        return [
            'number' => 1234567654
        ];
    }

    public static function concatStringInsideString($string, $replaceString, $find)
    {
        return substr_replace($string, $replaceString, strrpos($string, $find), 0);
    }

    public static function envUpdate($key, $oldValue, $value)
    {
        $path = base_path('.env');

        if (file_exists($path)) {
            file_put_contents($path, str_replace(
                $key . '=' . $oldValue, $key . '=' . $value, file_get_contents($path)
            ));
        }
    }

    public static function dateTimeChangeWithTimeZone($datetime, $previousTimeZone, $newTimeZone, $desiredFormat = 'Y-m-d H:i:s')
    {
        $datetime = new DateTime(date($desiredFormat, strtotime($datetime)), new DateTimeZone($previousTimeZone));
        $datetime = $datetime->setTimezone(new DateTimeZone($newTimeZone));
        return $datetime->format($desiredFormat);
    }

    public static function getOnlyTimeChangeWithTimeZone($datetime, $previousTimeZone, $newTimeZone, $desiredFormat = 'G:i')
    {

        $datetime = new DateTime(date($desiredFormat, strtotime($datetime)), new DateTimeZone($previousTimeZone));
        $datetime = $datetime->setTimezone(new DateTimeZone($newTimeZone));
        return $datetime->format($desiredFormat);
    }

    public static function getContentCreditByContentTypeAndSize($contentType, $cardSize)
    {

        return App::make(\Modules\Content\Contracts\Service\ContentCreditContact::class)->getContentCreditByContentTypeAndSize($contentType, $cardSize);

    }

    public static function RETURN_STATUS_FORMATE($status, $message, $optional = '')
    {
        return [
            'status' => $status,
            'html' => $message,
            'data' => $optional,
        ];
    }

    public static function generateRandomString($length = 10)
    {
        $characters = 'MNOPQRSTUVWXYZ12345678900123456789ABCDEFGHIJKL1234567890ABCDEFGHIJKL1234567890MNOPQRSTUVWXYZ1234567890';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}
