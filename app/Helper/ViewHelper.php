<?php

/**
 * Created by PhpStorm.
 * User: mohi
 * Date: 4/1/19
 * Time: 12:47 AM
 */

use App\Contracts\Service\UserContact;
use App\Models\Broadcast;
use App\Services\UserService;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Modules\Package\Entities\UserCurrentPackage;


class ViewHelper
{

    public static function activeMenu($urlName, $isParent = false)
    {
        if ($isParent == true) {
            return request()->is($urlName) ? 'menu-item-open' : '';
        }
        return request()->is($urlName) ? 'menu-item-active' : '';
    }

    public static function activeSubMenu($urlName)
    {
        return request()->is($urlName) ? 'm-menu__item--active' : '';
    }

    public static function convertTimeFromYear($time)
    {
        return Carbon::parse($time)->format('Y');
    }

    public static function convertTimeFrom12($time)
    {
        return Carbon::parse($time)->format('d M  g:i a');
    }

    public static function getEmailStatus($status)
    {
        if ($status == \Modules\Broadcast\Entities\BroadcastLog::EMAIL_STATUS_LOG_CREATED) {
            return "Sent";
        } else if ($status == \Modules\Broadcast\Entities\BroadcastLog::EMAIL_STATUS_SENT) {
            return "Delivered";
        } else if ($status == \Modules\Broadcast\Entities\BroadcastLog::EMAIL_STATUS_OPENED) {
            return "Opened";
        } else if ($status == \Modules\Broadcast\Entities\BroadcastLog::EMAIL_STATUS_CLICKED) {
            return "Clicked";
        }
    }
    public static function getMinifiedName($firstName, $lastName)
    {
        return (($firstName != '') ? strtoupper($firstName[0]) : "") . (($lastName != '') ? strtoupper($lastName[0]) : "");
    }

    public static function randomID()
    {

        return substr(md5(mt_rand()), 0, 7);
    }

    public static function grapesJsSelectAndSaveOption()
    {
        $option = '';
        foreach (\Modules\AdminWebBuilder\Entities\AdminWebTemplate::ADMIN_WEB_TYPE_ARRAY as $key => $value) {
            $option .= "<option value=" . $key . " >" . $value . "</option>";
        }

        return "<div class=row><table class=save__as__table><tr><td>Title</td><td><input type=text class=template_title placeholder></td></tr><tr><td>Template Type</td><td><select class=template-type>" . $option . "</select></td></tr><tr><td></td><td><button class=template_save_btn>Save Template</button></td></tr></table></div>";
    }

    public static function grapesJsEbookSelectAndSaveOption()
    {
        $option = '';
        foreach (\Modules\AdminWebBuilder\Entities\AdminWebTemplate::ADMIN_WEB_TYPE_ARRAY as $key => $value) {
            $option .= "<option value=" . $key . " >" . $value . "</option>";
        }

        return "<div class=row><table class=save__as__table><tr><td>Title</td><td><input type=text class=template_title placeholder></td></tr><tr><td></td><td><button class=template_save_btn>Save Ebook</button></td></tr></table></div>";
    }

    public static function grapesJsSelectAndSaveOptionEdit($type)
    {
        $option = '';

        foreach (\Modules\AdminWebBuilder\Entities\AdminWebTemplate::ADMIN_WEB_TYPE_ARRAY as $key => $value) {
            if ($type == $key) {
                $option .= "<option value=" . $key . " selected >" . $value . "</option>";
            } else {
                $option .= "<option value=" . $key . "  >" . $value . "</option>";
            }
        }

//        return "<div class=" . 'row' ."> <div class=" . 'template_cell' ." > Title : <input type=" . 'text' ."  class=".'template_title'." value=".''."> <br></div> <div class=" . 'template_cell' ." > Template Type :  <select class=". 'template-type' ."> ".$option."</select> </div> <div  class=" . 'template_cell' ."> <button class=" . 'template_save_btn' ." > Save  </button> </div> </div>  </div>";
        return "<div class=row><table class=save__as__table><tr><td>Title</td><td><input type=text class=template_title placeholder></td></tr><tr><td>Template Type</td><td><select class=template-type>" . $option . "</select></td></tr><tr><td></td><td><button class=template_save_btn>Update Template</button></td></tr></table></div>";
    }

    public static function personalizedReplace($message, $contactInfo, $userInfo, $offerPageUrl = null)
    {

        $message = str_replace('[[first_name]]', $contactInfo->first_name ? $contactInfo->first_name : '', $message);
        $message = str_replace('[[last_name]]', $contactInfo->last_name ? $contactInfo->last_name : '', $message);
        $message = str_replace('[[number]]', $contactInfo->number ? $contactInfo->number : '', $message);
        $message = str_replace('[[email]]', $contactInfo->email ? $contactInfo->email : '', $message);
        $message = str_replace('[[my_first_name]]', $userInfo->first_name ? $userInfo->first_name : '', $message);
        $message = str_replace('[[my_last_name]]', $userInfo->last_name ? $userInfo->last_name : '', $message);
        $message = str_replace('[[my_email]]', $userInfo->email ? $userInfo->email : '', $message);
        $message = str_replace('[[my_number]]', $userInfo->phone ? $userInfo->phone : '', $message);
        $message = str_replace('[[offer_page]]', $offerPageUrl ? $offerPageUrl : '', $message);

        return $message;
    }


    public static function getShareUrl($shareId, $funnelUrl, $title, $description)
    {

        switch ($shareId) {
            case "TWITTER_SHARE" :
                return 'https://twitter.com/intent/tweet?url=' . $funnelUrl . '&via=' . env('APP_NAME') . '&hashtags=giveaway,win&text=' . $title;
                break;
            case "FACEBOOK_SHARE":
                return 'https://www.facebook.com/sharer/sharer.php?u=' . $funnelUrl . '&title=' . $title;
                break;
            case "MESSENGER_SHARE":
                return 'https://www.facebook.com/dialog/send?app_id=' . env('FACEBOOK_CLIENT_ID') . '&link=' . $funnelUrl . '&redirect_uri=' . $funnelUrl;
                break;
            case "LINKEDIN_SHARE":
                return 'https://www.linkedin.com/sharing/share-offsite/?url=' . ($funnelUrl);
                break;
            case "PINTEREST_SHARE":
                return 'http://pinterest.com/pin/create/button/?url=' . $funnelUrl;
                break;
            case "GMAIL_SHARE":
                return 'mailto:somebody@somewhere.com?subject=' . $title . '&body=' . $description . ' #dogfunnel #funnel %0A%0A%0A' . $funnelUrl;
                break;
        }
    }

}
