<?php

namespace App\Http\Requests;

use App\Services\Utility;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AddUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = array(
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|max:255|email|unique:users,email'.$this->id,
            'phone' => 'required|max:20|unique:users,phone'
        );

        return $rules;
    }
    public function messages()
    {
        return[
            'email.unique' => 'Email Already Taken!',
            'phone.unique' => 'Phone Number Already Taken!'
        ];
    }
}
