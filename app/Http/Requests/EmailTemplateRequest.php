<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmailTemplateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = array(
            'title' => 'required|max:255|unique:email_templates,title'.$this->id,
        );
        return $rules;
    }
    public function messages()
    {
        return[
            'title.unique' => 'Title Already Taken!',
        ];
    }

}
