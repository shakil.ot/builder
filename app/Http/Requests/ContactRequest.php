<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = array(
            'email' => 'required|max:255|email|unique:users,email,'.$this->id,
            'number' => 'max:20|unique:users,number,'.$this->id
        );
        return $rules;
    }
    public function messages()
    {
        return[
            'email.unique' => 'Email Already Taken!',
            'phone.unique' => 'Phone Number Already Taken!'
        ];
    }
}
