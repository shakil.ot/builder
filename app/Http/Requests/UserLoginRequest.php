<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'email' => 'required|email|exists:users,email|max:256',
            'password' => 'required|min:6'
        ];

        return $rules;

    }

    public function messages()
    {
        $messages = [
            'email.required' => __('Email field is required.'),
             'email.email' => __('Invalid Email formate.'),
             'email.exists' => __('Email doesn\'t exist in our system.'),
             'email.max' => __('Email can\'t be more than 256 character.'),
             'password.required' => __('Password field is required.')
        ];

        return $messages;
    }
}
