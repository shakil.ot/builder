<?php

namespace App\Http\Controllers\User;

use App\Contracts\Service\UserContact;
use App\Http\Controllers\Controller;
use App\Services\SendgridMailService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class HomeController extends Controller
{
    /**
     * @var UserContact
     */
    private $userService;

    /**
     * Create a new controller instance.
     *
     * @param UserContact $userService
     */
    public function __construct(UserContact $userService)
    {
        $this->middleware('auth');
        $this->userService = $userService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function getFilterData(Request $request)
    {
        return response()->json($this->userService->getFilterData(auth()->user()->company_id, $request->get('type')));
    }

    public function appScreen()
    {
        return view('user.app.index');
    }
    public function test()
    {
        $subject = "Hey there";
        $to= 'mishu.das35bng@gmail.com';
        $from = env('FROM_EMAIL');
        $message = "<a href='http://app.mapyoursales.com/'>Click here</a>Good job";
        $companyName = "company";

        $result = App::make(SendgridMailService::class)->sendMailWithSendGrid($subject, $to, $from, $message, $companyName,"ABCDef");
        dd($result);

    }
}
