<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminLoginRequest;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\URL;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }

    public function showLoginForm()
    {
        return view('admin.auth.login');
    }


//    public function logout(Request $request)
//    {
//        $this->guard('admin')->logout();
//
//        $request->session()->invalidate();
//
////        return $this->loggedOut($request) ?: redirect('/admin');
//        return redirect('/admin');
//    }

    public function login(AdminLoginRequest $request)
    {
        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            return Response::json([
                'html' => 'You have been Successfully login.Thank you',
                'path' => URL::previous(),
                'status' => 'success'
            ]);
        } else {
            return Response::json([
                'html' => 'Login Failed',
                'path' => URL::previous(),
                'status' => 'error'
            ]);
        }
    }

    public function logout(Request $request)
    {
        auth('admin')->logout();
        return $this->loggedOut($request) ?: redirect('/admin');
    }
}
