<?php

namespace App\Http\Controllers\Admin\User;

use App\Contracts\Service\UserContact;
use App\Http\Controllers\Controller;
use App\Http\Requests\AddUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Response;
use Modules\Package\Contracts\Service\PackageContact;
use ViewHelper;
use Yajra\DataTables\DataTables;

class UsersController extends Controller
{


    /**
     * @var UserContact
     */
    private $userService;
    /**
     * @var PackageContact
     */
    private $packageService;

    public function __construct(UserContact $userService,
                                PackageContact $packageService)
    {

        $this->userService = $userService;
        $this->packageService = $packageService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.user.index');
    }


    public function listUserData(Request $request)
    {
        if (!$request->ajax()) {
            return redirect()->route('admin.home');
        }

        return $this->userService->listUserData($request);
    }


    public function getUserAddForm(Request $request)
    {
        return Response::json([
            'html' => view('admin.user.form')->with([
                'submit_button' => 'Add This User',
                'email_option_display' => true,
                'heading_email' => ''
            ])->render(),
            'status' => 'success'
        ]);
    }


    public function userAddFormSubmit(AddUserRequest $request)
    {
        return Response::json($this->userService->createUser($request));
    }


    public function getUserEditForm(Request $request)
    {

        $UserData = $this->userService->getUserById($request->get('user_id'));

        if (empty($UserData)) {
            return Response::json([
                'html' => "No data found",
                'status' => 'error',
            ]);
        }

        return Response::json([
            'html' => view('admin.user.form')->with([
                'userData' => $UserData,
                'submit_button' => 'Update User'
            ])->render(),
            'status' => 'success'
        ]);
    }

    public function getAllowPermissionForm(Request $request)
    {

        $UserData = $this->userService->getUserById($request->get('user_id'));

        if (empty($UserData)) {
            return Response::json([
                'html' => "No data found",
                'status' => 'error',
            ]);
        }

        return Response::json([
            'html' => view('admin.user.permission.form')->with([
                'userData' => $UserData,
                'submit_button' => 'Update User Permission'
            ])->render(),
            'status' => 'success'
        ]);
    }

    public function allowPermissionFormSubmit(Request $request)
    {
        if (!$request->ajax()) {
            return redirect()->route('admin.user');
        }

        return Response::json($this->userService->allowPermissionFormSubmit($request));
    }


    public function userEditFormSubmit(UpdateUserRequest $request)
    {
        if (!$request->ajax()) {
            return redirect()->route('admin.user');
        }

        return Response::json($this->userService->updateUser($request));
    }


    public function deactivateUser(Request $request)
    {
        if (!$request->ajax()) {
            return redirect()->route('admin.home');
        }

        $response = $this->userService->deactivateUser($request->user_id);
        return Response::json($response);

    }

    public function activateUser(Request $request)
    {
        if (!$request->ajax()) {
            return redirect()->route('admin.home');
        }
        $response = $this->userService->activateUser($request->get('user_id'));

        return Response::json($response);

    }


    public function getViewUserInformation(Request $request)
    {
        if (!$request->ajax()) {
            return redirect()->route('admin.home');
        }
        return $this->userService->getViewUserInformation($request);
    }


    public function stopAutoRecurring(Request $request)
    {
        if (!$request->ajax()) {
            return redirect()->route('admin.home');
        }

        $response = $this->userService->stopAutoRecurring($request->user_id);
        return Response::json($response);

    }

    public function startAutoRecurring(Request $request)
    {
        if (!$request->ajax()) {
            return redirect()->route('admin.home');
        }

        $response = $this->userService->startAutoRecurring($request->user_id);
        return Response::json($response);

    }

    public function forceLogin($userId)
    {
        return $this->userService->forceLogin($userId);
    }


    public function packageAllocateView(Request $request)
    {
        if (!$request->ajax()) {
            return redirect()->route('admin.home');
        }
        $userId = $request->user_id;
        $userDetails = $this->userService->getUserById($userId);
        if ($userDetails['country'] == User::COUNTRY_BANGLADESH) {
            $packages = $this->packageService->getAllBangladeshiFreePackage();
        } else {
            $packages = $this->packageService->getAllInternationalFreePackage();
        }


        return Response::json([
            'html' => view('admin.user.package-allocate')->with([
                'packages' => $packages,
                'user_id' => $userId,
            ])->render(),
            'status' => 'success'
        ]);

    }

    public function packageAllocateInUser(Request $request)
    {

        $response = $this->packageService->packageAllocate($request);

        return Response::json($response);
    }

    public function deleteUser(Request $request)
    {
        if (!$request->ajax()) {
            return redirect()->route('admin.home');
        }

        $response = $this->userService->deleteUser($request->user_id);
        return Response::json($response);

    }

}
