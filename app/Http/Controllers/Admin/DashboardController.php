<?php
/**
 * User: Rafiqul Islam
 * Date: 6/11/20
 * Time: 3:25 PM
 */

namespace App\Http\Controllers\Admin;

use App\Contracts\Service\UserContact;
use App\Http\Controllers\Controller;
use Modules\AdminEmailTemplate\Contracts\Service\EmailTemplateContact;
use Modules\Package\Contracts\Service\PackageContact;

class DashboardController extends Controller
{
    /**
     * @var UserContact
     */
    private $userService;
    /**
     * @var PackageContact
     */
    private $packageService;
    /**
     * @var EmailTemplateContact
     */
    private $emailTemplateService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserContact $userService,
                                PackageContact $packageService,
                                EmailTemplateContact $emailTemplateService)
    {
        $this->userService = $userService;
        $this->packageService = $packageService;
        $this->emailTemplateService = $emailTemplateService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $totalUsers = $this->userService->getTotalUser();
        $totalPackages = $this->packageService->getTotalPackage();
        $totalEmailTemplates = $this->emailTemplateService->getTotalEmailTemplate();
        return view('admin.dashboard', compact('totalUsers', 'totalPackages','totalEmailTemplates'));
    }
}
