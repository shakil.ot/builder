<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'webbuilder/update-image',
        'webbuilder/action-url-for-subscription',
        'webbuilder/action-url-for-download-instruction',
        'webbuilder/action-url-for-thank-you',
        'thank-you-page-add',
        'email/status/receive',
        'webbuilder/action-url-for-offer'
    ];
}
