<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Modules\Package\Contracts\Service\UserCurrentPackageContact;

class CheckIfUserHasActivePackage
{
    /**
     * @var UserCurrentPackageContact
     */
    private $userCurrentPackageService;

    public function __construct(UserCurrentPackageContact $userCurrentPackageService)
    {
        $this->userCurrentPackageService = $userCurrentPackageService;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userId = Auth::id();
        $package = $this->userCurrentPackageService->getUserActiveCurrentPackageInformationByUserId($userId);
        if ($package === null) {
        return redirect()->route('user.show-package');
        }
        else if ((strtotime(date('Y-m-d H:i:s')) > strtotime($package->expire_date))) {
            return redirect()->route('user.package-renew');
        }

        return $next($request);
    }
}
