<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Modules\Package\Entities\UserCurrentPackage;
use Illuminate\Support\Facades\Auth;

class CheckUserJourneyMode
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $journeyMode = Auth::user()->journey_mode;

        if ($journeyMode == 1) {

            Session::flash('flash-message-error', "Sorry !! You have to setup all ");
            return redirect()->route('user.onboarding');
        }

        return $next($request);
    }


}
