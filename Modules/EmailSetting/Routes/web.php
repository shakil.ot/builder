<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('emailsetting')->middleware('auth')->group(function() {
    Route::get('/', 'EmailSettingController@index')->name('email-setting');
    Route::post('/', 'EmailSettingController@updateSetting')->name('update-email-setting');
    Route::post('/single/update', 'EmailSettingController@updateSingleEmail')->name('update-single-email');
    Route::post('/states', 'EmailSettingController@getStates')->name('state-list');
    Route::post('image-upload-from-summernote','EmailSettingController@uploadImageFromSummernote')->name('image-upload-from-summernote');

});
