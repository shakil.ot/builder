<?php

namespace Modules\EmailSetting\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;

class EmailSetting extends Model
{
    protected $table = 'email_settings';

    protected $fillable = [
        'user_id',
        'email',
        'country',
        'state',
        'city',
        'address',
        'zip',
        'status',
        'company_name'
    ];

}
