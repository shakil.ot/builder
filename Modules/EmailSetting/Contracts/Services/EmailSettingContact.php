<?php

namespace Modules\EmailSetting\Contracts\Services;

interface EmailSettingContact
{
    public function updateEmailSetting($userId, $request);

    public function updateSingleEmail($request, $userId);

    public function getEmailDataByUserId($userId);

    public function getStatesOnCountry($request);

    public function uploadImageFromSummernote($request, $userId);
}