<?php

namespace Modules\EmailSetting\Contracts\Repositories;

interface EmailSettingRepository
{
    public function updateOrCreateByParam($where, $param);

    public function getEmailDataByUserId($userId);
}