<?php

namespace Modules\EmailSetting\Http\Services;

use App\Services\S3ServiceAWS;
use CountryState;
use Modules\EmailSetting\Contracts\Repositories\EmailSettingRepository;
use Modules\EmailSetting\Contracts\Services\EmailSettingContact;

class EmailSettingService implements EmailSettingContact
{
    private $emailSettingRepository;
    /**
     * @var S3ServiceAWS
     */
    private $s3ServiceAWS;

    public function __construct(EmailSettingRepository $emailSettingRepository,
                                S3ServiceAWS $s3ServiceAWS)
    {
        $this->emailSettingRepository= $emailSettingRepository;
        $this->s3ServiceAWS = $s3ServiceAWS;
    }

    public function updateEmailSetting($userId, $request)
    {
        $where = [
            'user_id' => $userId
        ];

        $data = [
            'country' => $request->country ? $request->country : '',
            'state' => $request->state ? $request->state : '',
            'city' => $request->city ? $request->city : '',
            'address' => $request->address ? $request->address : '',
            'zip' => $request->zip_code ? $request->zip_code : '',
            'company_name' => $request->company_name ? $request->company_name : '',
        ];

        $result = $this->emailSettingRepository->updateOrCreateByParam($where, $data);
        if($result)
        {
            return [
                'html' => 'Email Setting Updated!',
                'status' => 'success'
            ];
        }

        return [
            'html' => 'Failed! Please try again',
            'status' => 'error'
        ];
    }

    public function updateSingleEmail($request, $userId)
    {
        $data = [
          'email' => $request->email,
        ];

        $where = [
            'user_id' => $userId
        ];

        if($data['email'] == '' || $data['email'] == null)
        {
            return [
                'html' => 'Enter a valid email!',
                'status' => 'error'
            ];
        }

        $result = $this->emailSettingRepository->updateOrCreateByParam($where, $data);

        if($result)
        {
            return [
                'html' => 'Email Added!',
                'status' => 'success'
            ];
        }

        return [
            'html' => 'Failed! Please try again',
            'status' => 'error'
        ];
    }

    public function getEmailDataByUserId($userId)
    {
        return $this->emailSettingRepository->getEmailDataByUserId($userId);
    }

    public function getStatesOnCountry($request)
    {
        $countryValue = $request->countryValue;
        $htmlData = ' <select class="form-control state" id="event_action" name="state">';
        foreach (CountryState::getStates($countryValue) as $key => $value){
            $htmlData .='<option value="'.$key.'">'.$value.'</option>';
        }
        $htmlData .='</select>';

        return $htmlData;
    }
    public function uploadImageFromSummernote($request, $userId)
    {
        if ($_FILES['file']['name']) {
            if (!$_FILES['file']['error']) {
                $file = $_FILES['file'];
                $name = md5(rand(100, 200));
                $ext = explode('.', $_FILES['file']['name']);
                $filename = $name . '.' . $ext[1];
                $s3response = $this->s3ServiceAWS->uploadFileToS3($file['tmp_name'], S3ServiceAWS::UPLOAD_IMAGE_FROM_SUMMERNOTE, $name, $userId);
                return [
                    'status' => 'success',
                    'url' => $s3response['url']
                ];

            } else {
                return [
                    'status' => 'error',
                    'message' => 'Sorry! Image upload failed! Please try again'
                ];
            }
        }
    }
}