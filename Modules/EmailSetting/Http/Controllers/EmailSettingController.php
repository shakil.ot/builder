<?php

namespace Modules\EmailSetting\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Modules\EmailSetting\Contracts\Services\EmailSettingContact;

class EmailSettingController extends Controller
{
    private $emailSettingService;
    public function __construct(EmailSettingContact $emailSettingService)
    {
        $this->emailSettingService = $emailSettingService;
    }

    public function index()
    {
        $emailData = $this->emailSettingService->getEmailDataByUserId(Auth::id());
        return view('emailsetting::index', compact('emailData'));
    }

    public function updateSetting(Request $request)
    {
        $result = $this->emailSettingService->updateEmailSetting(auth()->id(), $request);
        return Response::json([
            'html' => $result['html'],
            'status' => $result['status'],
        ]);
    }

    public function updateSingleEmail(Request $request)
    {
        $result = $this->emailSettingService->updateSingleEmail($request, Auth::id());
        return Response::json([
            'html' => $result['html'],
            'status' => $result['status'],
        ]);
    }

    public function getStates(Request $request)
    {
        $data = $this->emailSettingService->getStatesOnCountry($request);
        return Response::json($data);
    }

    public function uploadImageFromSummernote(Request $request)
    {
        $userId = Auth::id();
        $response = $this->emailSettingService->uploadImageFromSummernote($request, $userId);
        return Response::json($response);
    }
}
