<?php

namespace Modules\EmailSetting\Http\Repositories;

use App\Repositories\BaseRepository\BaseRepository;
use Modules\EmailSetting\Contracts\Repositories\EmailSettingRepository;
use Modules\EmailSetting\Entities\EmailSetting;
use phpDocumentor\Reflection\Types\This;

class EmailSettingRepositoryEloquent extends BaseRepository implements EmailSettingRepository
{
    protected function model()
    {
        return new EmailSetting();
    }

    public function updateOrCreateByParam($where, $data)
    {
        return $this->updateOrCreate($where, $data);
    }


    public function getEmailDataByUserId($userId)
    {
        return $this->model()::where('user_id', $userId)->first();
    }

}