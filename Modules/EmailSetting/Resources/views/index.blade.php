@extends('user.layout.app', ['menu' => 'email-setting'])
@section('title')
    Email Settings
@endsection
@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Card-->
                <div class="d-flex flex-row" data-sticky-container="">
                    <!--begin::Content-->
                    <div class="flex-row-fluid ml-lg-8">
                        <!--begin::Row-->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card card-custom">
                                    <div>


                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                    <div class="" style="margin-top: 13px; margin-left: 13px">
                                                        @if(isset($emailData->email) && $emailData->email != null)
                                                            <span style="font-size: 100%">Your emails are being sent from <strong>{{$emailData->email}}</strong></span>
                                                        @endif
                                                    </div>
                                                <section style="background-color: #f1f3f4; margin-top: 35px; padding: 1px 0; border-radius: 10px;">
                                                    <div class="heading" style="margin-top: 40px">
                                                        <h3 class=" text-dark-75" style="font-weight: 300; font-size: 21px; margin-left: 14px;">
                                                            Mailing Address
                                                        </h3>
                                                    </div>
                                                    <div class="form">
                                                        <form _lpchecked="1" id="email-setting-form" autocomplete="off">
                                                            @csrf
                                                            <input type="hidden" name="user_id" value="{{\Illuminate\Support\Facades\Auth::id()}}">
                                                            <p style="font-size: 14px; margin-left: 15px;">Your address details are shown in the footer of your emails.</p>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group col-lg-12">
                                                                        <label for="Country" class="col-form-label ml-2">Country</label>
                                                                        <select class="form-control countrySelect select2" id="country" name="country" autocomplete="nope">
                                                                            @foreach(CountryState::getCountries() as $key=>$value)
                                                                                <option value="{{$key}}" >{{$value}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        <div class="custom-error-message"></div>
                                                                    </div>
                                                                    <div class="form-group col-lg-12">
                                                                        <label for="state" class="col-form-label ml-2">State</label>
                                                                        @php
                                                                            $country = (isset($emailData->country) && $emailData->country != "") ? $emailData->country : 'US';
                                                                            $state = (isset($emailData->state) && $emailData->state != "") ? $emailData->state : '';
                                                                        @endphp
                                                                        <select class="form-control select2" id="state" name="state" autocomplete="nope">
                                                                            @foreach(CountryState::getStates($country) as $key=>$value)
                                                                                <option value="{{$key}}" {{$state == $key ? 'selected' : ''}}  >{{$value}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        <div class="custom-error-message"></div>
                                                                    </div>
                                                                    <div class="form-group col-lg-12">
                                                                        <label for="city" class="col-form-label ml-2">City</label>
                                                                        <input class="form-control" type="text" name="city" value="{{isset($emailData->city) ? $emailData->city : ''}}" placeholder="Enter City">
                                                                        <div class="custom-error-message"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">

                                                                    <div class="form-group col-lg-12">
                                                                        <label for="company" class="col-form-label ml-2">Company Name</label>
                                                                        <input class="form-control" type="text" name="company_name" value="{{isset($emailData->company_name) ? $emailData->company_name : ''}}" placeholder="Enter Company Name">
                                                                        <div class="custom-error-message"></div>
                                                                    </div>

                                                                    <div class="form-group col-lg-12">
                                                                        <label for="address" class="col-form-label ml-2">Address</label>
                                                                        <input class="form-control" type="text" name="address" value="{{isset($emailData->address) ? $emailData->address : ''}}" placeholder="Enter Address">
                                                                        <div class="custom-error-message"></div>
                                                                    </div>
                                                                    <div class="form-group col-lg-12">
                                                                        <label for="zip" class="col-form-label ml-2">Zip Code</label>
                                                                        <input class="form-control" type="text" name="zip_code" value="{{isset($emailData->zip) ? $emailData->zip : ''}}" placeholder="Enter Zip Code">
                                                                        <div class="custom-error-message"></div>
                                                                    </div>
{{--                                                                    <div class="form-group col-lg-12">--}}
{{--                                                                        <label for="email" class="col-form-label ml-2">Email</label>--}}
{{--                                                                        <input class="form-control" type="email" name="email" value="{{isset($emailData->email) ? $emailData->email : ''}}" placeholder="Enter Email Address" >--}}
{{--                                                                        <div class="custom-error-message"></div>--}}
{{--                                                                    </div>--}}

                                                                    <div class="form-group float-right" style="background-color: #f1f3f4; margin-right: 18px;">
                                                                        <button type="submit" class="btn btn-green font-weight-bold">
                                                                    <span class="svg-icon svg-icon-white">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                                                <path d="M6,2 L18,2 C19.6568542,2 21,3.34314575 21,5 L21,19 C21,20.6568542 19.6568542,22 18,22 L6,22 C4.34314575,22 3,20.6568542 3,19 L3,5 C3,3.34314575 4.34314575,2 6,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000"></path>
                                                                            </g>
                                                                        </svg>
                                                                    </span>
                                                                            Update Information
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </section>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="">
                                                    <form action="" id="updateEmail">
                                                        @csrf
                                                        <div class="input-group">
                                                            <input class="form-control" type="email" name="email" placeholder="Enter Email Address" >
                                                            <span class="input-group-btn">
                                                                <button type="submit" class="btn btn-green font-weight-bold ml-2">Update Email</button>
                                                            </span>
                                                        </div>
                                                    </form>
                                                </div>
                                                <section style="height: 200px; color: white; background-color: #023c64; border-radius:10px">
                                                    <div>
                                                        <div class="six-columns" style="margin-top: 29px">
                                                            <div class="table-header blue-gradient button-height">
                                                                &nbsp;
                                                            </div>
                                                            <table id="" class="table" style="margin-top: 41px;">
                                                                <thead>
                                                                <tr>
                                                                    <th style="background: #d6dadf; color: black">Emails</th>
                                                                    <th style="width:150px; background: #d6dadf; color: black">Manage</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <td class="button-height " style="width:60%; background: white">
                                                                        {{ (isset($emailData->email) && $emailData->email !='') ? $emailData->email : Auth::user()->email }}
                                                                    </td>
                                                                    <td class="button-height" style="background: white">
                                                                        <span class="button-group">
                                                                            <span style="font-weight: bold;" class="green">
                                                                                <span class="icon-tick">
                                                                                    <i class="icon-x ki ki-bold-check-1" style="color: #99c624;"></i>
                                                                                    Active
                                                                                </span></span>
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                            <div class="table-footer blue-gradient button-height large-margin-bottom">
                                                                &nbsp;
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Row-->
                    </div>
                    <!--end::Content-->
                </div>
                <!--end::Card-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>

@endsection

@section('script')
    <script>
        $(document).ready(function(){
            $('#country').select2({
                placeholder: "Select a Country"
            });
            $('#state').select2({
                placeholder: "Select a State"
            });

            let country = null;
            let data =   <?php echo json_encode($emailData); ?>;

            if(data != null)
            {
                country = data.country;
            }

            // if(country != null && country != "")
            // {
            //     country = data.country;
            // }

            else
            {
                country = 'US';
            }


            {{--let data = '{{$emailData}}';--}}
            // $('#country option[value="BD"]').attr('selected','selected');

            $('#country').val(country);
            $('#country').select2().trigger('change');
        });



    </script>
    <script src="{{ mix('/js/email-setting.js') }}"></script>
@endsection
