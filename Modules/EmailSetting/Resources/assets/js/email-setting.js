$(function () {
    let _token = document.head.querySelector("[property=csrf-token]").content;

    (function (global, $, _token) {

        // For Email Setting Form
        $('#email-setting-form').on('submit', function (e) {
            e.preventDefault();
            $('#preloader').show();
            let data = $(this).serialize();

            global.mySiteAjax({
                url: route('update-email-setting'),
                type: 'post',
                data: data,
                loadSpinner: true,
                success: function (response) {
                    if (response.status === 'validation-error') {
                        bootstrapNotify.error(response.html, 'error');
                        $('#preloader').hide();
                    } else if (response.status === 'success') {
                        bootstrapNotify.success(response.html, 'success');
                        $('#preloader').hide();
                    } else if (response.status == 'error') {
                        $('#preloader').hide();
                        bootstrapNotify.error(response.html, 'error');
                    }
                },
                error: function () {
                    $('#preloader').hide();
                }

            })
        });


        // For single email update

        $('#updateEmail').on('submit', function (e) {
            e.preventDefault();
            let data = $(this).serialize();

            global.mySiteAjax({
                url: route('update-single-email'),
                type: 'post',
                data: data,
                loadSpinner: true,
                success: function (response) {
                    if (response.status === 'validation-error') {
                        bootstrapNotify.error(response.html, 'error');
                        $('#preloader').hide();
                    } else if (response.status === 'success') {
                        $("#updateEmail")[0].reset();
                        bootstrapNotify.success(response.html, 'success');
                        location.reload();

                    } else if (response.status == 'error') {
                        $('#preloader').hide();
                        bootstrapNotify.error(response.html, 'error');
                    }
                },
                error: function () {
                    $('#preloader').hide();
                }

            })
        });



        // Filtering
        $(document).on('change', '.countrySelect', function (e) {
            let countryValue = $('#country option:selected').val();

            global.mySiteAjax({
                url: route('state-list'),
                type: 'post',
                data: {
                    'countryValue': countryValue,
                    "_token": _token
                },
                loadSpinner: true,
                success: function (response) {
                    $('#state').html(response);
                    // console.log(response);
                    // return false;

                },
                error: function () {
                    // $('#preloader').hide();
                }
            })
            e.preventDefault();
        });



    })(window, jQuery, _token);
});