<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('admin')->name('admin.')->middleware(['admin', 'web'])->group(function(){

    Route::prefix('package')->group(function () {

        Route::get('/', 'Package\PackageController@index')->name('package');
        Route::get('/list', 'Package\PackageController@packageList')->name('package-list');
        Route::post('/create', 'Package\PackageController@PackageCreate')->name('package-create');
        Route::get('/get/from', 'Package\PackageController@packageGetFrom')->name('package-get-from');

        Route::post('/edit/get/from', 'Package\PackageController@packageEditGetFrom')->name('package-edit-get-from');
        Route::post('/update', 'Package\PackageController@packageUpdate')->name('package-update');
        Route::post('/package/status/update', 'Package\PackageController@packageStatusUpdate')->name('package-status-update');
    });

});
