<?php

namespace Modules\Package\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Modules\Package\Contracts\Repositories\ArchivePackageRepository;
use Modules\Package\Contracts\Repositories\PackageRepository;
use Modules\Package\Contracts\Repositories\PaymentRepository;
use Modules\Package\Contracts\Repositories\PaymentTransactionRepository;
use Modules\Package\Contracts\Repositories\RequestPackageRepository;
use Modules\Package\Contracts\Repositories\StripeErrorRepository;
use Modules\Package\Contracts\Repositories\UserBalanceRepository;
use Modules\Package\Contracts\Repositories\UserCurrentPackageRepository;
use Modules\Package\Contracts\Service\PackageContact;
use Modules\Package\Contracts\Service\PaymentContact;
use Modules\Package\Contracts\Service\PaymentTransactionContact;
use Modules\Package\Contracts\Service\UserBalanceContact;
use Modules\Package\Contracts\Service\UserCurrentPackageContact;
use Modules\Package\Http\Repositories\ArchivePackageRepositoryEloquent;
use Modules\Package\Http\Repositories\PackageRepositoryEloquent;
use Modules\Package\Http\Repositories\PaymentRepositoryEloquent;
use Modules\Package\Http\Repositories\PaymentTransactionRepositoryEloquent;
use Modules\Package\Http\Repositories\RequestPackageRepositoryEloquent;
use Modules\Package\Http\Repositories\StripeErrorRepositoryEloquent;
use Modules\Package\Http\Repositories\UserBalanceRepositoryEloquent;
use Modules\Package\Http\Repositories\UserCurrentPackageRepositoryEloquent;
use Modules\Package\Http\Services\PackageService;
use Modules\Package\Http\Services\PaymentService;
use Modules\Package\Http\Services\PaymentTransactionService;
use Modules\Package\Http\Services\UserBalanceService;
use Modules\Package\Http\Services\UserCurrentPackageService;

class PackageServiceProvider extends ServiceProvider
{
    /**
     * @var string $moduleName
     */
    protected $moduleName = 'Package';

    /**
     * @var string $moduleNameLower
     */
    protected $moduleNameLower = 'package';

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(module_path($this->moduleName, 'Database/Migrations'));
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);


        // BINDING ALL SERVICE
        $this->app->bind(PackageContact::class, PackageService::class);
        $this->app->bind(PaymentTransactionContact::class, PaymentTransactionService::class);
        $this->app->bind(UserCurrentPackageContact::class, UserCurrentPackageService::class);
        $this->app->bind(UserBalanceContact::class, UserBalanceService::class);
        $this->app->bind(PaymentContact::class, PaymentService::class);

        // BINDING ALL REPOSITORY
        $this->app->bind(PackageRepository::class, PackageRepositoryEloquent::class);
        $this->app->bind(UserCurrentPackageRepository::class, UserCurrentPackageRepositoryEloquent::class);
        $this->app->bind(ArchivePackageRepository::class, ArchivePackageRepositoryEloquent::class);
        $this->app->bind(PaymentTransactionRepository::class, PaymentTransactionRepositoryEloquent::class);
        $this->app->bind(UserBalanceRepository::class, UserBalanceRepositoryEloquent::class);
        $this->app->bind(RequestPackageRepository::class, RequestPackageRepositoryEloquent::class);
        $this->app->bind(PaymentRepository::class, PaymentRepositoryEloquent::class);
        $this->app->bind(StripeErrorRepository::class, StripeErrorRepositoryEloquent::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path($this->moduleName, 'Config/config.php') => config_path($this->moduleNameLower . '.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path($this->moduleName, 'Config/config.php'), $this->moduleNameLower
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/' . $this->moduleNameLower);

        $sourcePath = module_path($this->moduleName, 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], ['views', $this->moduleNameLower . '-module-views']);

        $this->loadViewsFrom(array_merge($this->getPublishableViewPaths(), [$sourcePath]), $this->moduleNameLower);
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/' . $this->moduleNameLower);

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, $this->moduleNameLower);
        } else {
            $this->loadTranslationsFrom(module_path($this->moduleName, 'Resources/lang'), $this->moduleNameLower);
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path($this->moduleName, 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function getPublishableViewPaths(): array
    {
        $paths = [];
        foreach (\Config::get('view.paths') as $path) {
            if (is_dir($path . '/modules/' . $this->moduleNameLower)) {
                $paths[] = $path . '/modules/' . $this->moduleNameLower;
            }
        }
        return $paths;
    }
}
