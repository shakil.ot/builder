<?php

namespace Modules\Package\Http\Repositories;

use Illuminate\Support\Facades\DB;
use Modules\Package\Contracts\Repositories\PaymentTransactionRepository;
use Modules\Package\Entities\PaymentTransaction;
use Twilio\TwiML\Voice\Pay;


class PaymentTransactionRepositoryEloquent implements PaymentTransactionRepository
{

    public function transactionCreate($data)
    {
        return PaymentTransaction::create($data);
    }

    public function getAll($request = null, $users_id = null)
    {
        $paymentTransaction = PaymentTransaction::with('users')->orderBy('updated_at', 'desc');
        if ($request != null) {
            if ($request->created_at_from_search != null) {
                $paymentTransaction = $paymentTransaction->whereDate('created_at', '>=', $request->created_at_from_search);
            }
            if ($request->created_at_to_search != null) {
                $paymentTransaction = $paymentTransaction->whereDate('created_at', '<=', $request->created_at_to_search);
            }
        }
        if ($users_id != null) {
            if (count($users_id) > 0) {
                $paymentTransaction = $paymentTransaction->whereIn('user_id', $users_id);
            }
        }

        if (isset($request['user_id'])) {
            $paymentTransaction = $paymentTransaction->where('user_id', '=', $request['user_id']);
        }
        return $paymentTransaction;
    }


    public function getThisMonthAutoRechargeByUserId($userId)
    {
        return PaymentTransaction::where('user_id', $userId)
            ->where('type_for', PaymentTransaction::PAYMENT_TRANSACTION_TYPE_FOR_AUTO_RECHARGE)
            ->whereRaw('MONTH(created_at) = ?', [date('m')])
            ->sum('credit_amount');

    }

    public function getAllTransactionFromUser($request = null, $subUsersId)
    {
        $userPaymentTransaction = PaymentTransaction::join('users', 'users.id', '=', 'payment_transactions.user_id')
            ->whereIn('user_id', $subUsersId)
            ->select(DB::raw('users.first_name as first_name,
                     payment_transactions.type as type,
                     payment_transactions.type_for as type_for,
                     payment_transactions.package_type as package_type,
                     payment_transactions.transaction_id as transaction_id,
                     payment_transactions.credit_amount as credit_amount,
                     payment_transactions.price as price
                     '));
        if ($request != null) {
            if ($request->created_at_from_search != null) {
                $userPaymentTransaction = $userPaymentTransaction->whereDate('payment_transactions.created_at', '>=', $request->created_at_from_search);
            }
            if ($request->created_at_to_search != null) {
                $userPaymentTransaction = $userPaymentTransaction->whereDate('payment_transactions.created_at', '<=', $request->created_at_to_search);
            }
        }
        return $userPaymentTransaction;
    }

    public function getUserWiseDataForChart($user_id, $fromDate, $toDate)
    {
        return PaymentTransaction::whereBetween('created_at', [$fromDate, $toDate])
            ->select(DB::raw('Date(created_at) as created_at'), DB::raw('sum(credit_amount) as total'))
            ->groupBy(DB::raw('Date(created_at)'))
            ->get();
    }

    public function getAllDataByUserIdAndType($userId, $type, $limit = 10)
    {
        return PaymentTransaction::with('users')
            ->where('user_id', $userId)
            ->where('type', $type)
            ->whereRaw('MONTH(created_at) = ?', [date('m')])
            ->limit($limit)
            ->get();

    }

    public function getThisMonthSumCreditAmountByUserIdAndType($userId, $type)
    {
        return PaymentTransaction::with('users')
            ->where('user_id', $userId)
            ->where('type', $type)
            ->whereRaw('MONTH(created_at) = ?', [date('m')])
            ->sum('credit_amount');

    }

    public function getTransactionByDateRange($request, $users_id)
    {
        // TODO: Implement getTransactionByDateRange() method.

        $paymentTransaction = PaymentTransaction::with('users')->where('user_id', '!=', "");
        if ($request != null) {
            if ($request->created_at_from_search != null) {
                $paymentTransaction = $paymentTransaction->whereDate('created_at', '>=', $request->created_at_from_search);
            }
            if ($request->created_at_to_search != null) {
                $paymentTransaction = $paymentTransaction->whereDate('created_at', '<=', $request->created_at_to_search);
            }
        }
        if (count($users_id) > 0) {
            $paymentTransaction = $paymentTransaction->whereIn('user_id', '=', $users_id);
        }
        if (isset($request['user_id'])) {
            $paymentTransaction = $paymentTransaction->where('user_id', '=', $request['user_id']);
        }
        return $paymentTransaction;
    }

    public function getTransactionById($id)
    { // TODO: Implement getTransactionById() method.
        return PaymentTransaction::where('id', '=', $id)->first();
    }

    public function countTotalTransactionReports()
    {
        return PaymentTransaction::count();
    }


    public function getAllByUserId($request = null, $users_id = null)
    {

        $paymentTransaction = PaymentTransaction::with('users')->orderBy('updated_at', 'desc');
        if ($request != null) {
            if ($request->created_at_from_search != null) {
                $paymentTransaction = $paymentTransaction->whereDate('created_at', '>=', $request->created_at_from_search);
            }
            if ($request->created_at_to_search != null) {
                $paymentTransaction = $paymentTransaction->whereDate('created_at', '<=', $request->created_at_to_search);
            }
        }
        if ($users_id != null) {
            $paymentTransaction = $paymentTransaction->where('user_id', $users_id);
        }

        if (isset($request['user_id'])) {
            $paymentTransaction = $paymentTransaction->where('user_id', '=', $request['user_id']);
        }

        return $paymentTransaction;
    }

    public function getTransactionsByUserId($userId)
    {
        return PaymentTransaction::where('user_id', $userId)
            ->where('price', '>', 0)
            ->orderBy('created_at', 'desc')->get();
    }

}


