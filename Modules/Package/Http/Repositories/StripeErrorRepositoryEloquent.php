<?php

namespace Modules\Package\Http\Repositories;


use App\Repositories\BaseRepository\BaseRepository;
use Modules\Package\Contracts\Repositories\StripeErrorRepository;
use Modules\Package\Entities\StripeError;

class StripeErrorRepositoryEloquent extends BaseRepository implements StripeErrorRepository
{
    protected function model()
    {
       return new StripeError();
    }

    public function StripeErrorCreate($request)
    {
        // TODO: Implement ArchivePackageCreate() method.
        return $this->model->create($request);
    }

    public function getStripeErrorByUserId($user_id)
    {
        // TODO: Implement getArchiveListByUserId() method.
        return $this->model->where('user_id', $user_id)->get();
    }

    public function getAllStripeError()
    {
        // TODO: Implement getAllStripeError() method.
        return $this->model->leftjoin('users', 'users.id', '=', 'stripe_errors.id')
            ->select('users.email as email', 'stripe_errors.error as error', 'stripe_errors.ip as ip');
    }

    public function countTotalStripeErrors()
    {
        return $this->model->count();
    }
}

