<?php

namespace Modules\Package\Http\Repositories;

use App\Repositories\BaseRepository\BaseRepository;
use Modules\Package\Contracts\Repositories\PackageRepository;
use Modules\Package\Entities\Package;
use Modules\Package\Entities\StripeError;

class PackageRepositoryEloquent extends BaseRepository implements PackageRepository
{

    protected function model()
    {
        return new Package();
    }

    public function PackageCreate($request)
    {
        return $this->model->create($request);
    }

    public function getTotalPackage()
    {
        return $this->model()::count();
    }

    public function getAllPackage()
    {
         return $this->model->withCount('countTotalUsersWithinAPackage')->get();
    }

    public function getAllPaidPackageForBangladesh()
    {

        return $this->model->where('package_type', Package::PACKAGE_TYPE_PAID)
            ->where('status', Package::PACKAGE_ACTIVE)
            ->where('country', Package::COUNTRY_BANGLADESH)->get();
    }

    public function getAllPaidPackageForInternational()
    {
        return $this->model->where('package_type', Package::PACKAGE_TYPE_PAID)->where('country', Package::COUNTRY_INTERNATIONAL)->get();
    }
    public function getAllUpdatedPaidPackageForBangladesh($subscriptionFee)
    {
        return $this->model->where('package_type', Package::PACKAGE_TYPE_PAID)
            ->where('country', Package::COUNTRY_BANGLADESH)
            ->where('subscription_fee', '>', $subscriptionFee)
            ->get();
    }

    public function getAllUpdatedPaidPackageForInternational($subscriptionFee)
    {
        return $this->model->where('package_type', Package::PACKAGE_TYPE_PAID)
            ->where('country', Package::COUNTRY_INTERNATIONAL)
            ->where('subscription_fee', '>', $subscriptionFee)
            ->get();
    }
    public function getAllBangladeshiFreePackage()
    {
        return $this->model->where('package_type', Package::PACKAGE_TYPE_FREE)
            ->where('status', Package::PACKAGE_ACTIVE)
            ->where('country', Package::COUNTRY_BANGLADESH)->get();
    }

    public function getAllInternationalFreePackage()
    {
        return $this->model->where('package_type', Package::PACKAGE_TYPE_FREE)
            ->where('status', Package::PACKAGE_ACTIVE)
            ->where('country', Package::COUNTRY_INTERNATIONAL)->get();
    }

    public function getByPackageId($id)
    {
        return $this->model->where(['id' => $id])->first();
    }

    public function PackageDelete($id)
    {
        return $this->model->where('id', $id)->delete();
    }

    public function PackageUpdate($where, $update)
    {
        return $this->model->updateOrCreate($where, $update);
    }

    public function getPackageWithoutFreePackage()
    {
        // TODO: Implement getPackageWithoutFreePackage() method.
        return $this->model->where('package_type', Package::PACKAGE_TYPE_PAID)
            ->orderBy('subscription_fee', 'ASC')
            ->get();
    }


    public function getAllFreePackage()
    {
        // TODO: Implement getAllFreePackage() method.
        return $this->model->where('package_type', Package::PACKAGE_TYPE_FREE)->get();
    }


    public function getAllPaidPackage()
    {

        return $this->model->where('package_type', Package::PACKAGE_TYPE_PAID)->get();
    }

    public function getAllPackageWithoutFreeAndActive()
    {
        // TODO: Implement getAllPackageWithoutFreeAndActive() method.
        return $this->model->where('package_type', Package::PACKAGE_TYPE_PAID)->where('status', Package::PACKAGE_ACTIVE)->get();
    }

    public function countTotalStripeErrors()
    {
        return StripeError::count();
    }
}
