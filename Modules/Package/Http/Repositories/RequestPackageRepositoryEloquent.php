<?php

namespace Modules\Package\Http\Repositories;

use Illuminate\Http\Request;
use Modules\Package\Contracts\Repositories\RequestPackageRepository;
use Modules\Package\Entities\RequestPackage;


class RequestPackageRepositoryEloquent implements RequestPackageRepository
{


    public function requestPackageUpdateOrCreate($where, $data)
    {
        return RequestPackage::updateOrCreate($where , $data);
    }


    public function getRequestDataByUserId($userId)
    {
        return RequestPackage::where('user_id',$userId)
            ->where('status', RequestPackage::STATUS_PENDING)
            ->first();
    }

    public function updateRequestStatusById($id, $status)
    {
        return RequestPackage::where('id', $id)
            ->update(['status' => $status ]);
    }

}


