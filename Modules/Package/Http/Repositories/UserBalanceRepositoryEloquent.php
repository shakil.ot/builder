<?php

namespace Modules\Package\Http\Repositories;

use Modules\Package\Contracts\Repositories\UserBalanceRepository;
use Modules\Package\Entities\UserBalance;
use Modules\Package\Entities\PaymentTransaction;



class UserBalanceRepositoryEloquent implements UserBalanceRepository
{
    public function userBalanceUpdateOrCreate($where , $data )
    {
        // TODO: Implement UserBalanceUpdate() method.
        return UserBalance::updateOrCreate($where , $data);
    }

    public function userBalanceCreate($data)
    {
        // TODO: Implement UserBalanceUpdate() method.
        return UserBalance::create($data);
    }

    public function getUserBalanceByUserId($id)
    {
        return UserBalance::where('user_id', $id)->first();
    }

    public function addUserBalance($user_balance)
    {
        return UserBalance::create($user_balance);
    }

    public function sumUserBalance($where, $credit)
    {
       return UserBalance::where($where)
            ->increment('current_credit', $credit);
    }

    public function getCurrentCreditByUserId($user_id)
    {
        return UserBalance::where('user_id',$user_id)->first();
    }

    public function updateCurrentCredit($userId, $newCredit)
    {
        return UserBalance::where('user_id', '=', $userId)
            ->update([
                'current_credit' => $newCredit
            ]);
    }

    public function addCreditInUser($userId, $credit)
    {
        return UserBalance::where('user_id',$userId)
            ->increment('current_credit', $credit);
    }

    public function getByCondition($array)
    {
        // TODO: Implement getBillingInformationByUserId() method.
        return UserBalance::where($array)->first();
    }

    public function getUserCurrentBalance($user_id, $user_current_package_id)
    {
        return UserBalance::where('user_id',$user_id)
            ->where('user_package_id',$user_current_package_id)->first();
    }


    public function getUserAutoRecharge()
    {
        return UserBalance::where('auto_load_status', UserBalance::AUTO_LOAD_STATUS_ON)
            ->get();
    }

    /***## REDUCE BALANCE BY USER ID AND TYPE ##***/
    public function reduceBalanceByUserIdAndType($userId, $type, $credit)
    {
        return UserBalance::where('user_id',$userId)
            ->decrement($type, $credit);
    }
    /***## INCREASE BALANCE BY USER ID AND TYPE ##***/
    public function increaseBalanceByUserIdAndType($userId, $type, $credit)
    {
        return UserBalance::where('user_id',$userId)
            ->increment($type, $credit);
    }

    /***## GET USER BALANCE BY USER ID AND TYPE ##***/
    public function checkBalanceByUserIdAndType($userId)
    {
        return UserBalance::where('user_id',$userId)
            ->first();
    }

    
}
