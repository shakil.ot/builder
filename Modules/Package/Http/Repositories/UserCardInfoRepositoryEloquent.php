<?php

namespace Modules\Package\Http\Repositories;

use App\Repositories\BaseRepository\BaseRepository;
use Illuminate\Support\Facades\Auth;
use Modules\Package\Contracts\Repositories\UserCardInfoRepository;
use Modules\Package\Entities\UserCardInfo;


class UserCardInfoRepositoryEloquent extends BaseRepository implements UserCardInfoRepository
{
    protected function model()
    {
        return new UserCardInfo();
    }

    public function addUserCardInfo($card_info)
    {
        return $this->model->create($card_info);
    }

    public function getAllCardInfo($userId)
    {
        // TODO: Implement getAllCardInfo() method.
        return $this->model->where('user_id', $userId)->get();
    }

    public function paymentCardDelete($id)
    {
        // TODO: Implement paymentCardDelete() method.
        return $this->model->where('id', $id)->where('status', PAYMENT_DEFAULT_CARD_OFF)->delete();
    }

    public function getPaymentCardById($id)
    {
        // TODO: Implement getPaymentCardById() method.
        return $this->model->where('id', $id)->first();
    }

    public function getCardInfoByUserId($user_id)
    {
        return $this->model->where('user_id', $user_id)
            ->where('status', USER_CARD_ACTIVE)->first();
    }

    public function getCardInfoById($id)
    {
        return $this->model->where('id', $id)->first();
    }

    public function getExistingCard($user_id)
    {
        return $this->model->where('user_id', $user_id)->get();
    }

    public function disableUserExistingCard($card_id)
    {
        return $this->model->where('id', $card_id)
            ->update(['status' => USER_CARD_INACTIVE]);
    }

    public function enableUserNewCard($card_id)
    {
        return $this->model->where('id', $card_id)
            ->update(['status' => USER_CARD_ACTIVE]);
    }


    public function getUserDefaultCard()
    {
        // TODO: Implement getUserDefaultCard() method.
        return $this->model->where('user_id', Auth::id())->where('status', PAYMENT_DEFAULT_CARD_ON)->first();
    }

    public function paymentCardExistCheckByUserIdCardLastDigit($user_id, $card_number)
    {
        // TODO: Implement paymentCardExistCheckByUserIdCardLastDigit() method.

        return $this->model->where('user_id', $user_id)->where('card_number', $card_number)->first();
    }

    public function createCustomerCard($data)
    {
        // TODO: Implement createCustomerCard() method.
        return $this->model->create($data);
    }


    public function updateCustomerCard($where, $data)
    {
        // TODO: Implement updateCustomerCard() method.

        return $this->model->where($where)->update($data);
    }


    public function getUserDefaultCardByUserId($userId)
    {
        // TODO: Implement getUserDefaultCard() method.
        return $this->model->where('user_id', $userId)
            ->where('status', UserCardInfo::CARD_STATUS_ACTIVE)
            ->first();
    }

}
