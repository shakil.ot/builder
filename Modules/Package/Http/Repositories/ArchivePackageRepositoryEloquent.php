<?php

namespace Modules\Package\Http\Repositories;


use Modules\Package\Contracts\Repositories\ArchivePackageRepository;
use Modules\Package\Entities\ArchivePackage;

class ArchivePackageRepositoryEloquent implements ArchivePackageRepository
{
    public function ArchivePackageCreate($request)
    {
        // TODO: Implement ArchivePackageCreate() method.
        return ArchivePackage::create($request);
    }

    public function getArchiveListByUserId($user_id)
    {
        // TODO: Implement getArchiveListByUserId() method.
        return ArchivePackage::where('user_id',$user_id)->get();
    }
}

