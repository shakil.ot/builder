<?php

namespace Modules\Package\Http\Repositories;

use Illuminate\Support\Carbon;
use Modules\Package\Contracts\Repositories\UserCurrentPackageRepository;
use Modules\Package\Entities\UserCurrentPackage;


class UserCurrentPackageRepositoryEloquent implements UserCurrentPackageRepository
{
    public function getUserCurrentPackageInformationByUserId($userId)
    {
        // TODO: Implement getUserCurrentPackageInformationByUserId() method.
        return UserCurrentPackage::where('user_id', $userId)->with('packages')->first();
    }

    public function userCurrentPackageUpdateOrCreate($where, $data)
    {
        // TODO: Implement getUserCurrentPackageUpdateOrCreate() method.
        return UserCurrentPackage::updateOrCreate($where, $data);
    }

    public function addUserCurrentPackage($user_current_package)
    {
        return UserCurrentPackage::create($user_current_package);
    }

    public function getUserActiveCurrentPackageInformationByUserId($user_id)
    {
       return UserCurrentPackage::where('user_id',$user_id)
           ->where('status',UserCurrentPackage::ACTIVE)
           ->with('packages')
           ->first();
    }

    public function getByPackageByUserId($userId)
    {
        return UserCurrentPackage::where(['user_id' => $userId])->first();
    }

    public function getRenewablePackage()
    {
        return UserCurrentPackage::where('status', '=', UserCurrentPackage::ACTIVE)
            ->whereDate('expire_date', '<=', Carbon::today()->format('Y-m-d'))
            ->get();
    }
    public function getPackagesWhichWillBeExpiredAfterNDays($day){
        //Getting packages which will be expired after $day days
        return UserCurrentPackage::where('status','=',UserCurrentPackage::ACTIVE)
            ->WhereDate('expire_date','<=',Carbon::today()->addDays($day)->format('Y-m-d'))
            ->get();
    }

    public function getByPackageById($id)
    {
        return UserCurrentPackage::where(['id' => $id])->first();
    }

    public function getByPackageByUserIdAndPackageId($userId,$packageId)
    {
        return UserCurrentPackage::where([
            'user_id' => $userId,
            'package_id' => $packageId
        ])->first();
    }

    public function getCurrentUsePackageByPackageId($id)
    {
        // TODO: Implement getCurrentUsePackageCountByPackageId() method.
        return UserCurrentPackage::where(['package_id' => $id])->get();
    }

    public function changeAutoRecurringStatus($userId, $status)
    {
        return UserCurrentPackage::where('user_id',$userId)->update(['auto_recurring' => $status]);
    }

    public function getCurrentPackageInfoById($id)
    {
        return UserCurrentPackage::where('id',$id)->first();
    }
    public function deleteCurrentPackageByUserId($userId)
    {
        return UserCurrentPackage::where('user_id',$userId)->delete();
    }


}
