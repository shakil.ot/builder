<?php

namespace Modules\Package\Http\Repositories;

use Mockery\Exception;
use Modules\Package\Contracts\Repositories\PaymentRepository;
use Modules\Package\Entities\UserCardInfo;
use Modules\Package\Http\Services\PaymentGateway\StripeGateway;

class PaymentRepositoryEloquent implements PaymentRepository
{


    public function createCustomerCard($data)
    {
        // TODO: Implement createCustomerCard() method.
        return UserCardInfo::create($data);
    }

    public function stripePayment($request)
    {
        try {
            $stripe = new StripeGateway();

            try {
                if (!isset($request['customer_id'])) {
                    $customer = $stripe->createCustomer($request);
                    $customer_id = $customer->id;
                } else {
                    $customer_id = $request['customer_id'];
                }

                $amount = round($request['subscription_fee'], 2) * 100;

                $charge = $stripe->chargeCustomerForPackage($amount,
                    $customer_id,
                    $request['package_id'],
                    $request['package_name'],
                    $request['description']);
            } catch (\Exception $e) {
                return RETURN_STATUS_FORMATE('error', $e->getMessage() , $e->getJsonBody());
            }

        } catch (\Stripe\Exception\CardException $e) {
            // Since it's a decline, \Stripe\Exception\CardException will be caught
            return RETURN_STATUS_FORMATE('error', 'Something is wrong !! ', $e);
        } catch (\Stripe\Exception\RateLimitException $e) {
            return RETURN_STATUS_FORMATE('error', 'Too many requests made to the API too quickly!! ', $e);
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            return RETURN_STATUS_FORMATE('error', ' Invalid parameters were supplied to API !! ', $e);
        } catch (\Stripe\Exception\AuthenticationException $e) {
            // (maybe you changed API keys recently)
            return RETURN_STATUS_FORMATE('error', ' Authentication with API failed !! ', $e);
        } catch (\Stripe\Exception\ApiConnectionException $e) {
            return RETURN_STATUS_FORMATE('error', ' Network communication with  failed ', $e);
        } catch (\Stripe\Exception\ApiErrorException $e) {
            return RETURN_STATUS_FORMATE('error', ' Display a very generic error to the user, and maybe send ', $e);
            // yourself an email
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            return RETURN_STATUS_FORMATE('error', 'Something else happened, completely unrelated ', $e);
        }

        return $charge;
    }

    public function customerCreateInStripe($request)
    {

        try {
            $stripe = new StripeGateway();
            try {
                $customer = $stripe->createCustomer($request);
                return $customer;
            } catch (\Exception $e) {
                return RETURN_STATUS_FORMATE('error', $e->getMessage());
            }
        } catch (\Stripe\Exception\CardException $e) {
            // Since it's a decline, \Stripe\Exception\CardException will be caught
            return RETURN_STATUS_FORMATE('error', 'Something is wrong !! ');
        } catch (\Stripe\Exception\RateLimitException $e) {
            return RETURN_STATUS_FORMATE('error', 'Too many requests made to the API too quickly!! ');
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            return RETURN_STATUS_FORMATE('error', ' Invalid parameters were supplied to API !! ');
        } catch (\Stripe\Exception\AuthenticationException $e) {
            // (maybe you changed API keys recently)
            return RETURN_STATUS_FORMATE('error', ' Authentication with API failed !! ');
        } catch (\Stripe\Exception\ApiConnectionException $e) {
            return RETURN_STATUS_FORMATE('error', ' Network communication with  failed ');
        } catch (\Stripe\Exception\ApiErrorException $e) {
            return RETURN_STATUS_FORMATE('error', ' Display a very generic error to the user, and maybe send ');
            // yourself an email
        } catch (\Exception $e) {
            return RETURN_STATUS_FORMATE('error', $e->getMessage());
        }

    }


    public function getCustomerIdByUserId($userId)
    {
        // TODO: Implement createCustomerCard() method.
        return UserCardInfo::where('user_id', $userId)
            ->where('status' , UserCardInfo::CARD_STATUS_ACTIVE)
            ->first();
    }

}
