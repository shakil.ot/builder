<?php

namespace Modules\Package\Http\Controllers\Package;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Package\Contracts\Service\PackageContact;
use Modules\Package\Entities\Package;
use Modules\Package\Http\Requests\PackageRequest;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Response;

class PackageController extends Controller
{

    /**
     * @var PackageContact
     */
    public $packageService;

    public function __construct(PackageContact $packageService)
    {
        $this->packageService = $packageService;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('package::Package.index');
    }

    /**
     * package data list in here
     * make dataTable
     * for edit and delete use setting class and data-id, data-action
     */
    public function packageList(Request $request)
    {

        if (!$request->ajax()) {
            return redirect()->route('admin.package');
        }

        return $this->packageService->getAllPackageInTable();


    }

    public function packageGetFrom(Request $request)
    {

        return Response::json([
            'html' => view('package::Package.form')->with([
                'submit_button' => 'Add New Package',
                'email_option_display' => true,
                'heading_email' => ''
            ])->render(),
            'status' => 'success'
        ]);
    }


    /**
     * package data insert in here
     * @param PackageRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function PackageCreate(PackageRequest $request)
    {
        $response = $this->packageService->PackageCreate($request);

        return Response::json($response);

    }


    public function packageEditGetFrom(Request $request)
    {


        $packageEdit = $this->packageService->getByPackageId($request['id']);

        return Response::json([
            'html' => view('package::Package.edit-form')->with([
                'submit_button' => 'Edit New Package',
                'email_option_display' => true,
                'heading_email' => '',
                'packageEdit' => $packageEdit
            ])->render(),
            'status' => 'success'
        ]);
    }


    /**
     * Package data update in here
     */
    public function packageUpdate(PackageRequest $request)
    {
        $response = $this->packageService->PackageUpdate($request);

        return Response::json($response);
    }


    public function packageStatusUpdate(Request $request)
    {
        return $this->packageService->packageStatusUpdate($request);
    }



}
