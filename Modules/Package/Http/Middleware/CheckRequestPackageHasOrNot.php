<?php

namespace Modules\Package\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Package\Entities\RequestPackage;

class CheckRequestPackageHasOrNot
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $result = RequestPackage::where('status','=', RequestPackage::STATUS_DEACTIVATE)->where('user_id','=',Auth::id())->first();

        if (!is_null($result)){
            return redirect()->route('request-package-available-message-show');
        }

        return $next($request);
    }
}
