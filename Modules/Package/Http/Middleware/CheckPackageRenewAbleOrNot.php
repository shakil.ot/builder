<?php

namespace Modules\Package\Http\Middleware;

use App\Contracts\Service\UserContact;
use App\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Modules\Package\Contracts\Service\UserCurrentPackageContact;
use Modules\Package\Entities\RequestPackage;

class CheckPackageRenewAbleOrNot
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::user()->parent_id == null) {
            $user_id = Auth::id();
        } else {
            $parentInfo = App::make(UserContact::class)->getParentByUserId(Auth::id());
            $user_id = $parentInfo['id'];
        }
        $userCurrentPackage = App::make(UserCurrentPackageContact::class)->getUserCurrentPackageInformationByUserId($user_id);

        if ((strtotime(date('Y-m-d H:i:s')) > strtotime($userCurrentPackage->expire_date))) {
            if (Auth::user()->parent_id == null) {
                return redirect()->route('payment-renew');
            } else {
                return redirect()->route('child-user-message', ['status' => 'renew']);
            }
        }
        return $next($request);
    }
}
