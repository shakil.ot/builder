<?php

namespace Modules\Package\Http\Services;


use App\Helper\UtilityHelper;
use App\Services\Utility;
use Faker\Provider\Payment;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;
use Modules\Package\Contracts\Repositories\PackageRepository;
use Modules\Package\Contracts\Repositories\PaymentRepository;
use Modules\Package\Contracts\Service\PackageContact;
use Modules\Package\Contracts\Service\PaymentContact;
use Modules\Package\Contracts\Repositories\PaymentTransactionRepository;
use Modules\Package\Contracts\Service\UserBalanceContact;
use Modules\Package\Entities\Package;
use Modules\Package\Entities\PaymentTransaction;
use Modules\Package\Entities\UserBalance;
use Modules\Package\Entities\UserCurrentPackage;
use App\Contracts\Service\UserContact;
use Modules\Package\Contracts\Repositories\UserCurrentPackageRepository;

class PaymentService implements PaymentContact
{

    private $paymentRepository;
    private $paymentTransactionRepository;
    private $userCurrentPackageRepository;
    private $userService;
    private $packageService;
    private $packageRepository;
    /**
     * @var UserBalanceContact
     */
    private $userBalanceService;


    public function __construct(PaymentRepository $paymentRepository,
                                PaymentTransactionRepository $paymentTransactionRepository,
                                UserCurrentPackageRepository $userCurrentPackageRepository,
                                UserContact $userService,
                                PackageContact $packageService,
                                PackageRepository $packageRepository,
                                UserBalanceContact $userBalanceService)
    {

        $this->paymentRepository = $paymentRepository;
        $this->paymentTransactionRepository = $paymentTransactionRepository;
        $this->userCurrentPackageRepository = $userCurrentPackageRepository;
        $this->userService = $userService;
        $this->packageService = $packageService;
        $this->packageRepository = $packageRepository;
        $this->userBalanceService = $userBalanceService;
    }


    public function createCustomerCard($data)
    {
        // TODO: Implement createCustomerCard() method.
        $response = $this->paymentRepository->createCustomerCard($data);
        if ($response) {
            return UtilityHelper::RETURN_STATUS_FORMATE('success', 'Card Saved Successfully ');
        }

        return UtilityHelper::RETURN_STATUS_FORMATE('error', 'Sorry! Card saving failed! Please try again');
    }


    public function successPayment($userId, $packageDetail, $cardInfo, $type, $requestDowngradePackageArray = null, $transaction_id = null, $billing_period, $subscription_fee)
    {
        $transactionId = $this->paymentTransactionRepository->transactionCreate([
            'user_id' => $userId,
            'type' => PaymentTransaction::TRANSACTION_IN,
            'price' => $subscription_fee,
            'type_for' => PaymentTransaction::PAYMENT_TRANSACTION_TYPE_FOR_PACKAGE_SUBSCRIPTION,
            'package_type' => Package::PACKAGE_RENEW,
            'transaction_id' => $transaction_id,
        ]);

        $this->insertToUserPackage($packageDetail, $cardInfo->id, $userId, $trial = false, $subscription_fee, $requestDowngradePackageArray, $transactionId, $billing_period);
        $this->addUserBalance($packageDetail,$userId);
        return true;
    }

    public function addUserBalance($packageDetail, $userId)
    {
        $where = [
            'user_id' => $userId
        ];
        $userBalance = [
            'user_package_id' => $packageDetail->id,
            'remaining_no_of_visitors' => $packageDetail->max_visitors,
            'remaining_no_of_contacts' => $packageDetail->max_contacts,
            'remaining_no_of_auto_followups' => $packageDetail->max_auto_followups,
            'remaining_no_of_email_send_per_month' => $packageDetail->max_email_send_per_month,
        ];
        $this->userBalanceService->userBalanceUpdateOrCreate($where, $userBalance);
    }


    public function insertToUserPackage($package, $paymentId, $userId, $trial = false, $subscriptionFee, $requestDowngradePackageArray, $transactionId = null, $billing_period)
    {
        if ($requestDowngradePackageArray != null) {
            $expireDate = Carbon::today()->addDays((int)$requestDowngradePackageArray->life_line);
            $trial_pack = 0;
            $lifeLine = $requestDowngradePackageArray->life_line;
            $trial_period = $requestDowngradePackageArray->trial_period;
            $max_user_count = $requestDowngradePackageArray->max_user_count;
            $max_employee_count = $requestDowngradePackageArray->max_employee_count;
            $paymentId = $requestDowngradePackageArray->user_payment_transaction_id;
            $packageId = $requestDowngradePackageArray->package_id;

            App::make(PackageContact::class)->packageArchiveInsertFunction($package->package_id, $package->package_name, $package->user_payment_transaction_id, $package->trial_period,
                $package->expire_date, $package->subscription_fee, $userId, $package->max_user_count, $package->max_employee_count,
                $package->created_at, $package->status);

        } else {

            $expireDate = Carbon::today()->addDays((int)($package->life_line));
            $trial_pack = 0;
            $lifeLine = $package->life_line;

            $trial_period = $package->trial_period;
            $paymentId = $transactionId->id;
            $packageId = $package->id;

        }
        $packageDetail = $this->packageService->getByPackageId($packageId);


        $data = [
            'package_id' => $packageId,
            'package_name' => $packageDetail['name'],
            'user_payment_transaction_id' => $paymentId,
            'life_line' => $lifeLine,
            'subscription_fee' => $subscriptionFee,
            'expire_date' => $expireDate,
            'charged_amount' => $subscriptionFee,
            'trial_period' => $trial_period,
            'is_use_trial_or_paid' => UserCurrentPackage::IS_NOT_IN_TRIAL_PERIOD
        ];

        $where = ['user_id' => $userId];

        return $this->userCurrentPackageRepository->userCurrentPackageUpdateOrCreate($where, $data);
    }


    public function insertInUserBalanceTable($request)
    {
//        Log::info($request);

        return UserBalance::updateOrCreate([
            'user_id' => $request['user_id'],
            'user_package_id' => $request['user_package_id'],
        ], [
            'current_credit' => $request['current_credit']
        ]);

    }

    public function chargeCreditFromQs($request)
    {
        $data = [
            'user_id' => $request['userId'],
            'type' => PaymentTransaction::TRANSACTION_OUT,
            'credit_amount' => $request['total_cost'],
            'type_for' => CATEGORY_FOR[$request['content_type']],
            'package_type' => Package::PACKAGE_TYPE_PAID,
            'transaction_id' => 'txn_' . Utility::generateRandomString() . time(),
            'price' => PaymentTransaction::TOTAL_CASH_COST_WHEN_BROADCAST
//            'note' => $note,
        ];
        $this->paymentTransactionRepository->transactionCreate($data);
    }

    public function chargeCreditFromVMBuy($request)
    {
        $data = [
            'user_id' => $request['userId'],
            'type' => PaymentTransaction::TRANSACTION_OUT,
            'credit_amount' => $request['total_cost'],
            'type_for' => PaymentTransaction::PAYMENT_TRANSACTION_TYPE_FOR_VIRTUAL_NUMBER,
            'package_type' => Package::PACKAGE_TYPE_PAID,
            'transaction_id' => 'txn_' . Utility::generateRandomString() . time(),
            'price' => PaymentTransaction::TOTAL_CASH_COST_WHEN_BROADCAST
//            'note' => $note,
        ];
        $this->paymentTransactionRepository->transactionCreate($data);
    }


    public function failPayment($userId)
    {
        $this->userService->deactivateUser($userId);

        return true;
    }
}
