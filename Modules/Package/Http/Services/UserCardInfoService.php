<?php

namespace Modules\Package\Http\Services;


use Illuminate\Support\Facades\Auth;
use Modules\Package\Contracts\Repositories\CategoryRepository;
use Modules\Package\Contracts\Repositories\PaymentRepository;
use Modules\Package\Contracts\Repositories\UserCardInfoRepository;
use Modules\Package\Contracts\Service\UserCardInfoContact;

class UserCardInfoService implements UserCardInfoContact
{
    /**
     * @var UserBalanceRepository
     */
    private $userCardInfoRepository;


    /**
     * @var CategoryRepository
     */


    public function __construct(UserCardInfoRepository $userCardInfoRepository)
    {
        $this->userCardInfoRepository = $userCardInfoRepository;
    }


    public function addUserCardInfo($card_info)
    {
        return $this->userCardInfoRepository->addUserCardInfo($card_info);
    }

    public function getAllCardInfo($userId)
    {
        // TODO: Implement getAllCardInfo() method.
        return $this->userCardInfoRepository->getAllCardInfo($userId);
    }

    public function paymentCardDelete($id)
    {
        // TODO: Implement paymentCardDelete() method.

        $result = $this->userCardInfoRepository->getPaymentCardById($id);

        if ($result) {
            if ($result->status == PAYMENT_DEFAULT_CARD_ON) {
                return ['status' => false, 'message' => 'Sorry! Default card can not be deleted!'];
            }
        }

        $response = $this->userCardInfoRepository->paymentCardDelete($id);

        if ($response) {
            return ['status' => true, 'message' => 'Card deleted successfully'];
        }
    }

    public function getUserDefaultCard()
    {
        // TODO: Implement getUserDefaultCard() method.
        return $this->userCardInfoRepository->getUserDefaultCard();

    }

    public function getUserDefaultCardByUserId($userId)
    {
        return $this->userCardInfoRepository->getUserDefaultCardByUserId($userId);
    }

    public function paymentDefaultCardSet($id)
    {
        // TODO: Implement paymentDefaultCardSet() method.
        $where = [
            'user_id' => Auth::id(),
            'status' => PAYMENT_DEFAULT_CARD_ON,
        ];

        $response = $this->userCardInfoRepository->updateCustomerCard($where, [
            'status' => PAYMENT_DEFAULT_CARD_OFF
        ]);

        if ($response) {
            $responseResult = $this->userCardInfoRepository->updateCustomerCard([
                'id' => $id
            ], [
                'status' => PAYMENT_DEFAULT_CARD_ON
            ]);

            if ($responseResult) {
                return ['status' => true, 'message' => 'Card set dafault successfully'];
            }
        }

        return ['status' => false, 'message' => 'Sorry! Could not set card as default! Please try again'];
    }

    public function paymentCardCreate($request)
    {
        // TODO: Implement paymentCardCreate() method.
        $response =  $this->paymentRepository->customerCreateInStripe($request);

        if ($response['status'] == 'error'){
            return $response;
        }

        $card_info = [
            'stripe_customer_id' => $response->id,
            'card_number' => $response->sources->data[0]->last4,
            'token' => $request['stripeToken'],
            'status' => 0,
            'user_id' => Auth::id(),
        ];

        $result = $this->userCardInfoRepository->addUserCardInfo($card_info);

        if ($result){
            return ['status' => true, 'message' => 'Card added successfully'];
        }

        return ['status' => false, 'message' => 'Sorry! Card addition failed! Please try again'];

    }

    public function getCardInfoByUserId($user_id)
    {
        return $this->userCardInfoRepository->getCardInfoByUserId($user_id);
    }

    public function getCardInfoById($id)
    {
        return $this->userCardInfoRepository->getCardInfoById($id);
    }

    public function getExistingCard($user_id)
    {
        return $this->userCardInfoRepository->getExistingCard($user_id);
    }

    public function disableUserExistingCard($card_id)
    {
        return $this->userCardInfoRepository->disableUserExistingCard($card_id);

    }

    public function enableUserNewCard($card_id)
    {
        return $this->userCardInfoRepository->enableUserNewCard($card_id);

    }
}
