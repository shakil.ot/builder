<?php

namespace Modules\Package\Http\Services;


use Modules\Package\Contracts\Repositories\PaymentTransactionRepository;
use Modules\Package\Contracts\Service\PaymentTransactionContact;
use Modules\Package\Entities\PaymentTransaction;
use Yajra\DataTables\DataTables;

class PaymentTransactionService implements PaymentTransactionContact
{


    /**
     * @var PaymentTransactionRepository
     */
    private $paymentTransactionRepository;

    public function __construct(PaymentTransactionRepository $paymentTransactionRepository)
    {

        $this->paymentTransactionRepository = $paymentTransactionRepository;
    }


    public function transactionAdd($data)
    {
        return $this->paymentTransactionRepository->transactionCreate($data);
    }

    public function listTransactions($userId)
    {
        $transactions = $this->paymentTransactionRepository->getTransactionsByUserId($userId);

        return DataTables::of($transactions)

            ->addColumn('data_plan', function ($transactions){
                return "#InvoiceOLKFN".$transactions->id;
            })
            ->addColumn('data_created_at', function ($transactions){
                if ($transactions->created_at == null) {
                    return '<span class="label label-lg label-light-danger  label-inline">N/A</span>';
                } else {
                    return $transactions->created_at->format('Y-m-d');
                }
            })
            ->addColumn('data_amount', function ($transactions){
                    return '$ '.$transactions->price;
            })

            ->addColumn('action', function ($transactions){
                $btn ='<div>'.
                    '<a href="/invoices/details/'. $transactions->id .'" target="_blank" class=""
                            data-value=" data-id=""><i class="fas fa-file-download"></i> Download
                            </a>
                       </div>'
                ;
                return $btn;
            })
            ->rawColumns(['data_plan', 'data_created_at', 'data_amount', 'action'])
            ->make(true);
    }

    public function getTransactionDetailsById($id)
    {
        return $this->paymentTransactionRepository->getTransactionById($id);
    }
}
