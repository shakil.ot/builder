<?php

namespace Modules\Package\Http\Services;


use App\User;
use Illuminate\Support\Facades\Auth;
use ImmutableCarbon\Carbon;
use Modules\Package\Contracts\Repositories\CategoryRepository;
use Modules\Package\Contracts\Repositories\UserCurrentPackageRepository;
use Modules\Package\Contracts\Service\UserCurrentPackageContact;
use Modules\Package\Entities\UserCurrentPackage;

class UserCurrentPackageService implements UserCurrentPackageContact
{
    /**
     * @var UserCurrentPackageRepository
     */
    private $currentPackageRepository;

    /**
     * @var CategoryRepository
     */


    public function __construct(UserCurrentPackageRepository $currentPackageRepository)
    {
        $this->currentPackageRepository = $currentPackageRepository;
    }


    public function getUserCurrentPackageInformationByUserId($id)
    {
        // TODO: Implement getUserCurrentPackageInformationByUserId() method.
        return $this->currentPackageRepository->getUserCurrentPackageInformationByUserId($id);
    }

    public function addUserCurrentPackage($user_current_package)
    {
        return $this->currentPackageRepository->addUserCurrentPackage($user_current_package);
    }
    public function userCurrentPackageUpdateOrCreate($where, $data)
    {
        return $this->currentPackageRepository->userCurrentPackageUpdateOrCreate($where, $data);

    }

    public function getUserActiveCurrentPackageInformationByUserId($user_id)
    {
        // TODO: Implement getUserCurrentPackageInformationByUserId() method.
        return $this->currentPackageRepository->getUserActiveCurrentPackageInformationByUserId($user_id);
    }

    public function CheckIfUserHasActivePackage($user_id = null)
    {
        if ($user_id != null) {
            $package = $this->currentPackageRepository->getUserActiveCurrentPackageInformationByUserId($user_id);
        } else {
            if (Auth::user()->parent_id == null) {
                $user_id = Auth::id();
            } else {
                $parentUserInfo = User::where('id', Auth::user()->parent_id)->first();
                $user_id = $parentUserInfo->id;
            }
            $packageInfo = UserCurrentPackage::where('user_id', $user_id)->select('expire_date')->first();

//            if ($packageInfo['expire_date'] < Carbon::today()->format('Y-m-d')) {
//                $package = $this->currentPackageRepository->getUserActiveCurrentPackageInformationByUserId($user_id);
//
//            }
            if ((strtotime(date('Y-m-d H:i:s')) > strtotime($packageInfo['expire_date']))) {
                $package = null;
            }

            else {
                $package = $this->currentPackageRepository->getUserActiveCurrentPackageInformationByUserId($user_id);
            }
        }

        if ($package === null)
            return false;

        return true;
    }

    public function getCurrentPackageInfoById($id)
    {
        return $this->currentPackageRepository->getCurrentPackageInfoById($id);
    }
}
