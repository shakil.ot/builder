<?php

namespace Modules\Package\Http\Services;

use App\Contracts\Repository\UserRepository;
use App\Contracts\Repository\UserSettingRepository;
use App\Contracts\Service\SystemEmailContact;
use App\Helper\UtilityHelper;
use App\Mail\Admin\FreePackageAutoRecurring;
use App\Mail\Admin\PackageAutoRecurringFailedToAdmin;
use App\Mail\User\PackageAutoRecurringFailedToUser;
use App\Mail\User\PackageAutoRecurringSuccess;
use App\Models\SystemEmail;
use App\Models\UserSetting;
use App\User;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use http\Exception;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use Modules\Package\Contracts\Repositories\ArchivePackageRepository;
use Modules\Package\Contracts\Repositories\PackageRepository;
use Modules\Package\Contracts\Repositories\PaymentTransactionRepository;
use Modules\Package\Contracts\Repositories\RequestPackageRepository;
use Modules\Package\Contracts\Repositories\StripeErrorRepository;
use Modules\Package\Contracts\Repositories\UserBalanceRepository;
use Modules\Package\Contracts\Repositories\UserCardInfoRepository;
use Modules\Package\Contracts\Repositories\UserCurrentPackageRepository;
use Modules\Package\Contracts\Service\PackageContact;
use Modules\Package\Contracts\Service\PaymentContact;
use Modules\Package\Entities\Package;
use Modules\Package\Entities\PaymentTransaction;
use Modules\Package\Entities\RequestPackage;
use Modules\Package\Entities\StripeError;
use Modules\Package\Entities\UserCurrentPackage;
use Modules\UserRegistration\Http\Services\StripeGateway;
use Yajra\DataTables\DataTables;

class PackageService implements PackageContact
{


    private $packageRepository;

    private $stopRecurringEmailRepository;
    /**
     * @var UserCurrentPackageRepository
     */
    private $userCurrentPackageRepository;
    /**
     * @var ArchivePackageRepository
     */
    private $archivePackageRepository;
    /**
     * @var PaymentTransactionRepository
     */
    private $paymentTransactionRepository;
    /**
     * @var UserSettingRepository
     */
    private $userSettingRepo;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var RequestPackageRepository
     */
    private $requestPackageRepo;
    /**
     * @var UserCardInfoRepository
     */
    private $cardInfoRepo;
    /**
     * @var UserBalanceRepository
     */
    private $userBalanceRepo;
    /**
     * @var SystemEmailContact
     */
    private $systemEmailService;
    /**
     * @var StripeErrorRepository
     */
    private $stripeErrorRepo;

    /**
     * PackageService constructor.
     * @param UserCurrentPackageRepository $userCurrentPackageRepository
     * @param ArchivePackageRepository $archivePackageRepository
     * @param UserSettingRepository $userSettingRepo
     * @param UserRepository $userRepository
     * @param RequestPackageRepository $requestPackageRepo
     * @param UserCardInfoRepository $cardInfoRepo
     * @param UserBalanceRepository $userBalanceRepo
     * @param SystemEmailContact $systemEmailService
     * @param StripeErrorRepository $stripeErrorRepo
     * @param PaymentTransactionRepository $paymentTransactionRepository
     */
    public function __construct(PackageRepository $packageRepository,
                                UserCurrentPackageRepository $userCurrentPackageRepository,
                                ArchivePackageRepository $archivePackageRepository,
                                UserSettingRepository $userSettingRepo,
                                UserRepository $userRepository,
                                RequestPackageRepository $requestPackageRepo,
                                UserCardInfoRepository $cardInfoRepo,
                                UserBalanceRepository $userBalanceRepo,
                                SystemEmailContact $systemEmailService,
                                StripeErrorRepository $stripeErrorRepo,
                                PaymentTransactionRepository $paymentTransactionRepository)
    {
        $this->packageRepository = $packageRepository;
        $this->userCurrentPackageRepository = $userCurrentPackageRepository;
        $this->archivePackageRepository = $archivePackageRepository;
        $this->paymentTransactionRepository = $paymentTransactionRepository;
        $this->userSettingRepo = $userSettingRepo;
        $this->userRepository = $userRepository;
        $this->requestPackageRepo = $requestPackageRepo;
        $this->cardInfoRepo = $cardInfoRepo;
        $this->userBalanceRepo = $userBalanceRepo;
        $this->systemEmailService = $systemEmailService;
        $this->stripeErrorRepo = $stripeErrorRepo;
    }


    public function PackageCreate($request)
    {
        //Max Visitors
        if (isset($request['max_visitors_limitation_type']) && $request['max_visitors_limitation_type'] == 'true') {
            $request['max_visitors_limitation_type'] = Package::MAX_VISITORS_UNLIMITED;
            $request['max_visitors'] = env('MAX_VISITORS_UNLIMITED');
        } else {
            $request['max_visitors_limitation_type'] = Package::MAX_VISITORS_LIMITED;
        }

        //Max Contacts
        if (isset($request['max_contacts_limitation_type']) && $request['max_contacts_limitation_type'] == 'true') {
            $request['max_contacts_limitation_type'] = Package::MAX_CONTACTS_UNLIMITED;
            $request['max_contacts'] = env('MAX_CONTACTS_UNLIMITED');
        } else {
            $request['max_contacts_limitation_type'] = Package::MAX_CONTACTS_LIMITED;
        }

        //Max Auto Followups
        if (isset($request['max_auto_followups_limitation_type']) && $request['max_auto_followups_limitation_type'] == 'true') {
            $request['max_auto_followups_limitation_type'] = Package::MAX_AUTO_FOLLOWUPS_UNLIMITED;
            $request['max_auto_followups'] = env('MAX_AUTO_FOLLOWUPS_UNLIMITED');
        } else {
            $request['max_auto_followups_limitation_type'] = Package::MAX_AUTO_FOLLOWUPS_LIMITED;
        }

        //Max Email send per month
        if (isset($request['max_email_send_limitation_type']) && $request['max_email_send_limitation_type'] == 'true') {
            $request['max_email_send_limitation_type'] = Package::MAX_EMAIL_SEND_UNLIMITED;
            $request['max_email_send'] = env('MAX_EMAIL_SEND_PER_MONTH_UNLIMITED');
        } else {
            $request['max_email_send_limitation_type'] = Package::MAX_EMAIL_SEND_LIMITED;
        }

        //Trial Package Check
        if (isset($request['trail_package']) && $request['trail_package'] == 'true') {
            $request['trail_package'] = Package::PACKAGE_TRIAL_YES;
        } else {
            $request['trail_package'] = Package::PACKAGE_TRIAL_NO;
        }

        if (!is_null($request['trial_period']) && $request['trial_period'] < 0) {
            return UtilityHelper::RETURN_STATUS_FORMATE('error', 'Trial Period can not be a negative number !!');
        }

        if (is_null($request['trial_period'])) {
            $request['trial_period'] = 0;
        }


        if ($request['package_type'] == Package::PACKAGE_TYPE_FREE) {
            $request['trail_package_edit'] = false;
            $request['trial_period_edit'] = 0;
            $request['subscription_fee'] = 0;
        }
        $data = [
            'name' => $request['package_name'],
            'description' => !empty($request['description']) ? $request['description'] : "This is a package " . $request['package_name'],
            'life_line' => $request['life_line'],
            'trial_pack' => $request['trail_package'],
            'subscription_fee' => $request['subscription_fee'],
            'status' => Package::PACKAGE_ACTIVE,
            'package_type' => (int)$request['package_type'],
            'yearly_discount_percentage' => $request['yearly_discount_percentage'],
            'yearly_discount_price' => $request['yearly_discount_price'],
            'trial_period' => $request['trial_period'],
            'max_visitors_limitation_type' => $request['max_visitors_limitation_type'],
            'max_visitors' => $request['max_visitors'],
            'max_contacts_limitation_type' => $request['max_contacts_limitation_type'],
            'max_contacts' => $request['max_contacts'],
            'max_auto_followups_limitation_type' => $request['max_auto_followups_limitation_type'],
            'max_auto_followups' => $request['max_auto_followups'],
            'max_email_send_limitation_type' => $request['max_email_send_limitation_type'],
            'max_email_send_per_month' => $request['max_email_send'],

        ];

        $response = $this->packageRepository->PackageCreate($data);

        if ($response) {

            return UtilityHelper::RETURN_STATUS_FORMATE('success', 'Package created successfully');
        }

        return UtilityHelper::RETURN_STATUS_FORMATE('error', 'Sorry! Package creation failed! Please try again');
    }

    public function getTotalPackage()
    {
        return $this->packageRepository->getTotalPackage();
    }

    public function getAllPackageInTable()
    {
        $package = $this->packageRepository->getAllPackage();

        return Datatables::of($package)
            ->addColumn('trial_pack', function ($data) {
                $trial_pack = '<span class="label label-lg label-light-danger label-inline">NO</span>';
                if ($data->trial_pack == 1) {
                    $trial_pack = '<span class="label label-lg label-light-primary label-inline">Yes</span>';
                }
                return $trial_pack;
            })
            ->addColumn('package_type', function ($data) {

                $package_type = '<span class="label label-lg label-light-success label-inline">Free</span>';
                if ($data->package_type == Package::PACKAGE_TYPE_PAID) {
                    $package_type = '<span class="label label-lg label-light-primary label-inline">Paid</span>';
                }
                return $package_type;
            })
            ->addColumn('my_country', function ($data) {
                if ($data->country == Package::COUNTRY_BANGLADESH) {
                    return "Bangladesh";
                } else {
                    return "International";
                }
            })
            ->addColumn('status', function ($data) {
                $status = '<span class="label label-lg label-light-danger label-inline">Inactive</span>';
                if ($data->status == Package::PACKAGE_ACTIVE) {
                    $status = '<span class="label label-lg label-light-success label-inline">Active</span>';
                }
                return $status;
            })
            ->addColumn('action', function ($data) {
                $status = 'Active';
                $status_icon = 'la la-unlock-alt';
                if ($data->status == Package::PACKAGE_ACTIVE) {
                    $status = 'Inactive';
                    $status_icon = 'la la-unlock';
                }
                $inactive = '<a class="dropdown-item action" href="javascript:void(0)"  data-value="active-deactivate" data-id="' . $data->id . '">
                                <i class="' . $status_icon . ' text-success mr-5"></i>
                                <span class="label label-danger label-dot mr-2"></span>
                                <span class="font-weight-bold text-' . ($status == 1 ? 'success' : 'danger') . '"> ' . $status . '</span>
                            </a>';

                return '<div class="dropdown dropdown-inline mr-4">
                            <button type="button" class="btn btn-light-primary btn-icon btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="ki ki-bold-more-hor"></i>
                            </button>
                            <div class="dropdown-menu">

                            <a class="dropdown-item action" href="javascript:void(0)"  data-value="edit" data-id="' . $data->id . '">
                                <i class="flat flaticon-edit text-success mr-5"></i> Edit</span>
                            </a>

                          <div class="dropdown-divider"></div>
                            ' . $inactive . '
                            </div>
                        </div>';

            })
            ->addColumn('yearly_discount', function ($data) {
                return '<span class="label label-lg label-light-success label-inline">' . $data->yearly_discount_percentage . '%</span>';
            })
            ->addColumn('yearly_discount_price', function ($data) {
                return '<span class="label label-lg label-light-primary label-inline">' . $data->yearly_discount_price . '</span>';
            })
            ->rawColumns(['trial_pack', 'my_country', 'package_type', 'status', 'action', 'yearly_discount', 'yearly_discount_price'])
            ->make(true);
    }

    public function getAllPackage()
    {
        // TODO: Implement getAll() method.
        return $this->packageRepository->getAllPackage();
    }

    public function getAllPaidPackageForBangladesh()
    {
        return $this->packageRepository->getAllPaidPackageForBangladesh();

    }

    public function getAllPaidPackageForInternational()
    {
        return $this->packageRepository->getAllPaidPackageForInternational();

    }

    public function getAllUpdatedPaidPackageForBangladesh($subscriptionFee)
    {
        return $this->packageRepository->getAllUpdatedPaidPackageForBangladesh($subscriptionFee);
    }

    public function getAllUpdatedPaidPackageForInternational($subscriptionFee)
    {
        return $this->packageRepository->getAllUpdatedPaidPackageForInternational($subscriptionFee);
    }

    public function getAllBangladeshiFreePackage()
    {
        return $this->packageRepository->getAllBangladeshiFreePackage();
    }

    public function getAllInternationalFreePackage()
    {
        return $this->packageRepository->getAllInternationalFreePackage();
    }

    public function getByPackageId($id)
    {
        // TODO: Implement getById() method.
        return $this->packageRepository->getByPackageId($id);
    }

    public function PackageUpdate($request)
    {
        //Max Visitors
        if(isset($request['edit_max_visitors_limitation_type']) && $request['edit_max_visitors_limitation_type'] == 'true'){
            $request['max_visitors_limitation_type'] = Package::MAX_VISITORS_UNLIMITED;
            $request['edit_max_visitors'] = env('MAX_VISITORS_UNLIMITED');
        }
        else{
            $request['max_visitors_limitation_type'] = Package::MAX_VISITORS_LIMITED;
        }

        //Max Contacts
        if(isset($request['edit_max_contacts_limitation_type']) && $request['edit_max_contacts_limitation_type'] == 'true'){
            $request['max_contacts_limitation_type'] = Package::MAX_CONTACTS_UNLIMITED;
            $request['edit_max_contacts'] = env('MAX_CONTACTS_UNLIMITED');
        }
        else{
            $request['max_contacts_limitation_type'] = Package::MAX_CONTACTS_LIMITED;
        }

        //Max Auto Followups
        if(isset($request['edit_max_auto_followups_limitation_type']) && $request['edit_max_auto_followups_limitation_type'] == 'true'){
            $request['max_auto_followups_limitation_type'] = Package::MAX_AUTO_FOLLOWUPS_UNLIMITED;
            $request['edit_max_auto_followups'] = env('MAX_AUTO_FOLLOWUPS_UNLIMITED');
        }
        else{
            $request['max_auto_followups_limitation_type'] = Package::MAX_AUTO_FOLLOWUPS_LIMITED;
        }

        //Max Email Send
        if(isset($request['edit_max_email_send_limitation_type']) && $request['edit_max_email_send_limitation_type'] == 'true'){
            $request['max_email_send_limitation_type'] = Package::MAX_EMAIL_SEND_UNLIMITED;
            $request['edit_max_email_send'] = env('MAX_EMAIL_SEND_PER_MONTH_UNLIMITED');
        }
        else{
            $request['max_email_send_limitation_type'] = Package::MAX_EMAIL_SEND_LIMITED;
        }
        if (isset($request['package_status_edit'])) {
            $request['package_status_edit'] = Package::PACKAGE_ACTIVE;
        } else {
            $request['package_status_edit'] = Package::PACKAGE_INACTIVE;
        }

        //Trial Package
        if (isset($request['trail_package_edit'])) {
            $request['trail_package_edit'] = true;
        } else {
            $request['trail_package_edit'] = false;
            $request['trial_period_edit'] = 0;
        }

        if ($request['package_type'] == Package::PACKAGE_TYPE_FREE) {
            $request['trail_package_edit'] = false;
            $request['trial_period_edit'] = 0;
            $request['subscription_fee'] = 0;
        }
        if (!is_null($request['trial_period']) && $request['trial_period'] < 0) {
            return UtilityHelper::RETURN_STATUS_FORMATE('error', 'Trial Period can not be a negative number !!');
        }

        $where = [
            'id' => $request['hidden_id_of_package'],
        ];

        $update = [
            'name' => $request['package_name'],
            'life_line' => $request['life_line'],
            'trial_pack' => $request['trail_package_edit'],
            'subscription_fee' => $request['subscription_fee'],
            'package_type' => (int)$request['package_type'],
            'trial_period' => (int)$request['trial_period_edit'],
            'yearly_discount_percentage' => $request['yearly_discount_percentage'],
            'yearly_discount_price' => $request['yearly_discount_price'],
            'description' => !empty($request['description']) ? $request['description'] : "This is a package " . $request['package_name'],
            'country' => Package::COUNTRY_INTERNATIONAL,
            'max_visitors_limitation_type' => $request['max_visitors_limitation_type'],
            'max_visitors' => $request['edit_max_visitors'],
            'max_contacts_limitation_type' => $request['max_contacts_limitation_type'],
            'max_contacts' => $request['edit_max_contacts'],
            'max_auto_followups_limitation_type' => $request['max_auto_followups_limitation_type'],
            'max_auto_followups' => $request['edit_max_auto_followups'],
            'max_email_send_limitation_type' => $request['max_email_send_limitation_type'],
            'max_email_send_per_month' => $request['edit_max_email_send'],
        ];

        $response = $this->packageRepository->PackageUpdate($where, $update);

        if ($response) {
            try {
                if ($update['package_type'] == Package::PACKAGE_TYPE_PAID) {

//                    $currentUser = $this->userCurrentPackageRepository->getCurrentUsePackageByPackageId($request['hidden_id_of_package']);
//
//                    foreach ($currentUser as $value) {
//                        $user = $this->userRepository->getUserById($value->user_id);
//                        $message = $this->packageMailHtml($update);//
//                        $sentMail = $this->sendMailService->adminSendMailWithSendGrid("Registration Successful!", $user->email, "", $message);
//                    }
                }
            } catch (Exception $e) {
                Log::info('Error Email sending' . $e);
            }
            return UtilityHelper::RETURN_STATUS_FORMATE('success', 'Package updated successfully');
        }

        return UtilityHelper::RETURN_STATUS_FORMATE('error', 'Sorry! Package updating failed! Please try again');
    }

    public function packageStatusUpdate($request)
    {
        $status = Package::PACKAGE_INACTIVE;
        $package = $this->packageRepository->getByPackageId($request->id);


        if ($package->status == Package::PACKAGE_INACTIVE) {
            $status = Package::PACKAGE_ACTIVE;
        }

        // TODO: Implement packageStatusUpdate() method.
        $result = $this->packageRepository->PackageUpdate([
            'id' => $request->id
        ], [
            'status' => $status
        ]);

        if ($result) {
            return UtilityHelper::RETURN_STATUS_FORMATE('success', 'Package status updated successfully');
        }

        return UtilityHelper::RETURN_STATUS_FORMATE('error', 'Sorry! Package status updating failed! Please try again');
    }

    public function getAllFreePackage()
    {
        // TODO: Implement getAllFreePackage() method.
        return $this->packageRepository->getAllFreePackage();
    }

    public function getAllPaidPackage()
    {
        // TODO: Implement getAllFreePackage() method.
        return $this->packageRepository->getAllPaidPackage();
    }

    public function packageAllocate($request)
    {
        $user_id = $request['userId'];

        $package = $this->packageRepository->getByPackageId($request['package_id']);
        $autoRecurringStatus = UserCurrentPackage::AUTO_RECURRING_ON;
        $isUseTrialOrPaid = UserCurrentPackage::IS_NOT_IN_TRIAL_PERIOD;
        $chargedAmount = $package->subscription_fee;
        if ($package['package_type'] == Package::PACKAGE_TYPE_FREE) {
            $autoRecurringStatus = UserCurrentPackage::AUTO_RECURRING_OFF;
            $isUseTrialOrPaid = UserCurrentPackage::IS_FREE_PERIOD;
        }
        if ($package['trial_pack'] == Package::PACKAGE_TRIAL_YES) {
            $isUseTrialOrPaid = UserCurrentPackage::IS_IN_TRIAL_PERIOD;
            $chargedAmount = 0;
        }
        $currentPackage = $this->userCurrentPackageRepository->getUserCurrentPackageInformationByUserId($user_id);

        $transactionAdd = $this->paymentTransactions(PaymentTransaction::TRANSACTION_IN, PaymentTransaction::PACKAGE_TYPE_FREE, $package->package_type, 0.00, 0.00, "free_package_allocate", $user_id);

        $expire_date = CarbonImmutable::now()->add($package->life_line, 'day');

        $userCurrentPackageResponse = $this->userCurrentPackageRepository->userCurrentPackageUpdateOrCreate(['user_id' => $user_id],
            [
                'package_id' => $package->id,
                'package_name' => $package->name,
                'user_payment_transaction_id' => $transactionAdd->id,
                'trial_period' => $package->trial_period,
                'is_use_trial_or_paid' => $isUseTrialOrPaid,
                'billing_period' => $package->life_line,
                'life_line' => $package->life_line,
                'expire_date' => $expire_date,
                'auto_recurring' => $autoRecurringStatus,
                'subscription_fee' => $package->subscription_fee,
                'charged_amount' => $chargedAmount,
                'status' => UserCurrentPackage::ACTIVE,
                'max_visitors' => $package->max_visitors,
                'max_contacts' => $package->max_contacts,
                'max_auto_followups' => $package->max_auto_followups,
                'max_email_send_per_month' => $package->max_email_send_per_month
            ]);


            /****########### INSERT IN USER BALANCE ###########****/
            $where = [
              'user_id' => $user_id
            ];
            $userBalance = [
              'user_package_id' => $package->id,
              'remaining_no_of_visitors' => $package->max_visitors,
              'remaining_no_of_contacts' => $package->max_contacts,
              'remaining_no_of_auto_followups' => $package->max_auto_followups,
              'remaining_no_of_email_send_per_month' => $package->max_email_send_per_month,
            ];
            $this->userBalanceRepo->userBalanceUpdateOrCreate($where, $userBalance);

        return UtilityHelper::RETURN_STATUS_FORMATE('success', 'Package allocate successfully');
    }

    public function autoRenew()
    {
        $packages = $this->userCurrentPackageRepository->getRenewablePackage();
        if (isset($packages[0])) {
            $stripe = new StripeGateway();
            foreach ($packages as $package) {
                $userId = $package->user_id;
                $userSetting = $this->userSettingRepo->getPackageSettingByUser($userId);
                $userInfo = $this->userRepository->getUserById($userId);
                if ($userSetting && $userInfo) {

                    $autoRecurringStatus = $userSetting['value'];
                    if ($autoRecurringStatus == UserSetting::RECURRENT_ON_STATUS && $userInfo['status'] == User::STATUS_ACTIVE) {
                        $requestDowngradePackage = $this->requestPackageRepo->getRequestDataByUserId($userId);
                        if ($requestDowngradePackage) {
//                            $this->requestPackageRepo->updateRequestStatusById($requestDowngradePackage->id, RequestPackage::STATUS_PROCESSING);
//                            $this->packageDowngrade($userId, $requestDowngradePackage);
                        } else {
                            $this->packageUpgrade($userId, $userInfo, $package, $stripe);
                        }
                    }
                }
            }
        }
        return true;
    }

    public function packageDowngrade($userId, $requestDowngradePackageArray)
    {
        $cardInfo = $this->cardInfoRepo->getCardInfoByUserId($userId);
        if ($cardInfo) {
            $packageDetail = $this->userCurrentPackageRepository->getByPackageByUserId($userId);
            if ($packageDetail) {
                try {
                    App::make(PaymentContact::class)->insertToUserPackage($packageDetail, $cardInfo->id, $userId, $trial = false, $requestDowngradePackageArray->subscription_fee, $requestDowngradePackageArray, $requestDowngradePackageArray->user_payment_transaction_id);
                    $this->requestPackageRepo->updateRequestStatusById($requestDowngradePackageArray->id, RequestPackage::STATUS_COMPLETED);

                    Log::info("Payment Success of user_id: " . $userId);

                } catch (\Exception $e) {
                    App::make(PaymentContact::class)->failPayment($userId);
                    Log::error($e->getMessage());
                    $this->requestPackageRepo->updateRequestStatusById($requestDowngradePackageArray->id, RequestPackage::STATUS_FAILED);

                }
            }

            return true;
        } else {
            Log::info("Package downgrade: Card Information is not set for user id : " . $userId);
        }

    }

    public function packageUpgrade($userId, $userInfo, $userPackage, $stripe)
    {
        $cardInfo = $this->cardInfoRepo->getCardInfoByUserId($userId);
        if ($cardInfo) {
            $packageDetail = $this->packageRepository->getByPackageId($userPackage->package_id);
            $isCharged = false;
            $transaction_id = null;

            if ($packageDetail) {
                $subscription_fee = $packageDetail->subscription_fee;
                $price = $subscription_fee * 100;
                try {
                    if ($price == 0) {
                        $status = 'free';
                    } else {
                        $charge = $stripe->chargeCustomerForPackage($price,
                            $cardInfo->stripe_customer_id,
                            $packageDetail->id,
                            $packageDetail->name,
                            $packageDetail->description);
                        $status = isset($charge->status) ? $charge->status : $charge;
                        if (isset($charge->balance_transaction)) {
                            $transaction_id = $charge->balance_transaction;
                        }
                    }

                    if ($status == 'free') {
                        //Free user
                        Mail::to('admin@gmail.com')->send(new FreePackageAutoRecurring($userInfo, $packageDetail));
                    } else if ($status == "succeeded") {
                        //Charged success
                        $isCharged = true;
                        App::make(PaymentContact::class)->successPayment($userId, $packageDetail, $cardInfo, Package::PACKAGE_RENEW, null, $transaction_id, $userPackage->billing_period, $subscription_fee);
                        Log::info("Payment Success of user id: " . $userId);

//                        $userBalance = $this->userBalanceRepo->getUserBalanceByUserId($userId);

                        $recurringSuccessEmail = $this->systemEmailService->getSystemEmailByType(SystemEmail::RECURRING_SUCCESS);

                        if ($recurringSuccessEmail) {
                            $emailSubject = \ViewHelper::personalizedReplace($recurringSuccessEmail['subject'], $userInfo);
                            $emailBody = \ViewHelper::personalizedReplaceForRecurringSuccess($recurringSuccessEmail['body'], $userInfo, $cardInfo, $packageDetail, null, $subscription_fee);
                            Mail::to($userInfo->email)->send(new PackageAutoRecurringSuccess($userInfo, $packageDetail, $subscription_fee, $cardInfo->card_number, $emailSubject, $emailBody));

                        } else {
                            Mail::to($userInfo->email)->send(new PackageAutoRecurringSuccess($userInfo, $packageDetail, $subscription_fee, $cardInfo->card_number));
                        }

                    } else {
                        //Not charged
                        $this->stripeErrorRepo->StripeErrorCreate([
                            'user_id' => $userId,
                            'error' => $status,
                            'ip' => '',
                            'type' => StripeError::TYPE_RECURRING,
                        ]);
                        Log::info("Payment Fail of user_id: " . $userId);
//                            App::make(PaymentContact::class)->failPayment($userId);
                        //todo: here send user a mail regarding his/her payment failed as well as developer
                        $recurringFailEmail = $this->systemEmailService->getSystemEmailByType(SystemEmail::RECURRING_FAIL);
                        if ($recurringFailEmail) {
                            $emailSubject = \ViewHelper::personalizedReplace($recurringFailEmail['subject'], $userInfo);
                            $emailBody = \ViewHelper::personalizedReplaceCardNumberAlso($recurringFailEmail['body'], $userInfo, $cardInfo);
                            Mail::to($userInfo->email)->send(new PackageAutoRecurringFailedToUser($userInfo, $packageDetail, $subscription_fee, $cardInfo, $status, $emailSubject, $emailBody));
                        } else {
                            Mail::to($userInfo->email)->send(new PackageAutoRecurringFailedToUser($userInfo, $packageDetail, $subscription_fee, $cardInfo, $status));
                        }

                    }
                } catch (\Exception $e) {
                    if ($isCharged) {
                        //Charged and server error
                        $this->userCurrentPackageRepository->changeAutoRecurringStatus($userId, UserCurrentPackage::AUTO_RECURRING_OFF);
                        $this->send_error_email($userId, $packageDetail->name, $subscription_fee, 'ACCOUNT CHARGED BUT GOT ERROR<br>' . $e->getMessage());
                    } else {
                        //Not charged but server error
                        $this->send_error_email($userId, $packageDetail->name, $subscription_fee, $e->getMessage());
                    }
                    $this->stripeErrorRepo->StripeErrorCreate([
                        'user_id' => $userId,
                        'error' => $e->getMessage(),
                        'ip' => '',
                        'type' => StripeError::TYPE_RECURRING,
                    ]);
                    Log::error($e);
                }
            }

            return true;
        } else {
            Log::info("Package upgrade: Card Information is not set for user id : " . $userId);
        }

    }

    public function paymentTransactions($type, $type_for, $package_type, $credit_amount, $price, $transaction_id, $user_id)
    {
        $paymentTransactionData = [
            'type' => $type,
            'type_for' => $type_for,
            'package_type' => $package_type,
            'price' => $price,
            'transaction_id' => $transaction_id,
            'user_id' => $user_id,
        ];

        return $this->paymentTransactionRepository->transactionCreate($paymentTransactionData);
    }

    private function send_error_email($userId, $packageName, $subscriptionFee, $message)
    {
        $receivers = [
            'rafiq.rangetoolz@gmail.com'
        ];

        Mail::to($receivers)->send(new PackageAutoRecurringFailedToAdmin($userId, $packageName, $subscriptionFee, $message));
    }

    public function packageArchiveInsertFunction($packageId, $packageName, $paymentTransactionId, $trialPeriod,
                                                 $expireDate, $subscriptionFee, $userId, $maxUserCount, $maxEmployeeCount,
                                                 $createdAt, $status)
    {
        $this->archivePackageRepository->ArchivePackageCreate([
            'package_id' => $packageId,
            'package_name' => $packageName,
            'user_payment_transaction_id' => $paymentTransactionId,
            'trial_period' => $trialPeriod,
            'expire_date' => $expireDate,
            'subscription_fee' => $subscriptionFee,
            'user_id' => $userId,
            'max_user_count' => $maxUserCount,
            'max_employee_count' => $maxEmployeeCount,
            'package_taken_date' => $createdAt,
            'status' => $status
        ]);
    }

}
