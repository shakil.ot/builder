<?php

namespace Modules\Package\Http\Services;


use App\Services\UserService;
use App\Services\Utility;
use App\User;
use Illuminate\Support\Facades\Auth;
use Modules\Package\Contracts\Repositories\CategoryRepository;
use Modules\Package\Contracts\Repositories\UserBalanceRepository;
use Modules\Package\Contracts\Service\PaymentContact;
use Modules\Package\Contracts\Service\UserBalanceContact;
use Modules\Package\Entities\Package;
use Modules\Package\Entities\PaymentTransaction;

class UserBalanceService implements UserBalanceContact
{
    /**
     * @var UserBalanceRepository
     */
    private $balanceRepository;


    /**
     * @var CategoryRepository
     */
    private $paymentService;
    /**
     * @var UserService
     */
    private $userService;

    public function __construct(UserBalanceRepository $balanceRepository,
                                UserService $userService)
    {
        $this->balanceRepository = $balanceRepository;
        $this->userService = $userService;
    }

    public function userBalanceUpdateOrCreate($where, $data)
    {
        return $this->balanceRepository->userBalanceUpdateOrCreate($where, $data);
    }

    public function addUserBalance($user_balance)
    {
        return $this->balanceRepository->addUserBalance($user_balance);
    }

    public function sumUserBalance($where, $credit)
    {
        return $this->balanceRepository->sumUserBalance($where, $credit);
    }

    public function getCurrentCreditByUserId($user_id)
    {
        $userInfo = $this->userService->getUserById($user_id);
        if ($userInfo['parent_id'] == null) {

        } else {
            $parentUserInfo = $this->userService->getParentByUserId($user_id);
            $user_id = $parentUserInfo->id;
        }
        $balance = $this->balanceRepository->getCurrentCreditByUserId($user_id);
        return $balance['current_credit'];
    }

    public function getCurrentCreditAmountByUserId($user_id)
    {
        $balance = $this->balanceRepository->getCurrentCreditByUserId($user_id);
        return $balance['current_credit'];
    }

    public function billingSetupUpdate($request)
    {
        // TODO: Implement billingSetupUpdate() method.

        $where = [
            'user_id' => $request->user_id,
        ];

        if (isset($request['auto_load_status'])) {
            $request['auto_load_status'] = true;
        } else {
            $request['auto_load_status'] = false;
        }

        if (isset($request['max_monthly_limit_status'])) {
            $request['max_monthly_limit_status'] = true;
        } else {
            $request['max_monthly_limit_status'] = false;
        }


        $data = [
            "auto_load_status" => $request['auto_load_status'],
            "auto_load_credit_level" => $request['auto_load_credit_level'],
            "auto_load_credit_amount" => $request['auto_load_credit_amount'],
            "max_monthly_limit_status" => $request['max_monthly_limit_status'],
            "max_monthly_limit" => $request['max_monthly_limit'],

        ];

        $response = $this->balanceRepository->userBalanceUpdateOrCreate($where, $data);

        if ($response) {

            return RETURN_STATUS_FORMATE('success', 'Balance setup updated successfully');

        }

        return RETURN_STATUS_FORMATE('error', 'Sorry! Balance setup updating failed! Please try again');

    }

    public function getByCondition($array)
    {
        // TODO: Implement getBillingInformationByUserId() method.
        return $this->balanceRepository->getByCondition($array);
    }

    public function getUserCurrentBalance($user_id, $user_current_package_id)
    {
        return $this->balanceRepository->getUserCurrentBalance($user_id, $user_current_package_id);
    }

    public function updateCurrentCredit($userId, $newCredit)
    {
        $userInfo = $this->userService->getUserById($userId);
        if ($userInfo['parent_id'] == null) {

        } else {
            $parentUserInfo = $this->userService->getParentByUserId($userId);
            $userId = $parentUserInfo->id;
        }
        return $this->balanceRepository->updateCurrentCredit($userId, $newCredit);
    }

    public function updateCurrentCreditForVR($userId, $newCredit)
    {

        return $this->balanceRepository->updateCurrentCredit($userId, $newCredit);
    }

    public function cutCreditFromUser($userId, $cutAmount)
    {

        $currentBalance = $this->getCurrentCreditAmountByUserId($userId);

        if ($currentBalance > 0) {

            $newBalance = $currentBalance - $cutAmount;
            if ($newBalance >= 0) {
                $response = $this->updateCurrentCreditForVR($userId, $newBalance);
                if ($response) {
                    $request['user_id'] = $userId;
                    $request['userId'] = $userId;
                    $request['type'] = PaymentTransaction::TRANSACTION_OUT;
                    $request['credit_amount'] = $cutAmount;
                    $request['total_cost'] = $cutAmount;
                    $request['type_for'] = 'Virtual Number';
                    $request['content_type'] = CATEGORY_FOR['Virtual_number'];
                    $request['package_type'] = Package::PACKAGE_TYPE_PAID;
                    $request['transaction_id'] = 'txn_' . Utility::generateRandomString() . time();
                    $request['price'] = PaymentTransaction::TOTAL_CASH_COST_WHEN_BUY_VIRTUALNUMBER;
                    $this->paymentService->chargeCreditFromVMBuy($request);

                    return ['status' => 'success', 'remaining_balance' => '$newBalance'];
                } else {
                    return ['status' => 'error'];
                }

            } else {
                return ['status' => 'error'];
            }
        } else {
            return ['status' => 'error'];
        }

    }

    public function addCreditInUser($userId, $credit)
    {
        return $this->balanceRepository->addCreditInUser($userId, $credit);
    }

    public function reduceBalanceByUserIdAndType($userId, $type, $credit)
    {
        return $this->balanceRepository->reduceBalanceByUserIdAndType($userId, $type, $credit);
    }

    public function increaseBalanceByUserIdAndType($userId, $type, $credit)
    {
        return $this->balanceRepository->increaseBalanceByUserIdAndType($userId, $type, $credit);
    }

    public function checkBalanceByUserIdAndType($userId)
    {
        return $this->balanceRepository->checkBalanceByUserIdAndType($userId);
    }
}
