<?php

namespace Modules\Package\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PackageRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = null;
        if (array_key_exists("hidden_id_of_package",$this->all())) {
            $id = $this->get('hidden_id_of_package');
        }

        return [
            'package_name' => 'required|max:50|unique:packages,name,' . $id . ',id',
            'life_line' => 'required|min:0|integer'
        ];
        //'name' => 'unique:contact_groups,name,' . $id . ',id,user_id,'.$userId,
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * @return array
     */
    public function messages()
    {
        return [
            'package_name.required' => 'Package name is required',
            'package_name.unique' => 'Package name must be unique',
            'package_name.max' => 'Package name must be no longer than 50 characters',
            'subscription_fee.required'  => 'Subscription fee is required',
            'life_line.required'  => 'Package Life line is required',
            'life_line.min'  => 'Package Life line need minimum value',
            'life_line.integer'  => 'Package Life line need Positive number',
        ];
    }



}
