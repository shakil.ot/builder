<?php

namespace Modules\Package\Contracts\Service;

interface PackageContact
{
    public function PackageCreate($request);

    public function getTotalPackage();

    public function getAllPackageInTable();

    public function getAllPackage();

    public function getAllPaidPackageForBangladesh();

    public function getAllPaidPackageForInternational();

    public function getAllUpdatedPaidPackageForBangladesh($subscriptionFee);

    public function getAllUpdatedPaidPackageForInternational($subscriptionFee);

    public function getAllBangladeshiFreePackage();

    public function getAllInternationalFreePackage();

    public function getByPackageId($id);

    public function packageStatusUpdate($request);

    public function getAllFreePackage();

    public function getAllPaidPackage();

    public function packageAllocate($request);

    public function autoRenew();

}
