<?php

namespace Modules\Package\Contracts\Service;

interface PaymentTransactionContact
{
    public function transactionAdd($data);

    public function listTransactions($userId);

    public function getTransactionDetailsById($id);

}
