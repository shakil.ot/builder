<?php

namespace Modules\Package\Contracts\Service;

interface UserCurrentPackageContact
{
    public function getUserCurrentPackageInformationByUserId($id);

    public function addUserCurrentPackage($user_current_package);

    public function userCurrentPackageUpdateOrCreate($where, $data);

    public function getUserActiveCurrentPackageInformationByUserId($user_id);

    public function CheckIfUserHasActivePackage($user_id = null);

    public function getCurrentPackageInfoById($id);

}
