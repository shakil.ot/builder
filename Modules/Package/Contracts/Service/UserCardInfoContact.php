<?php

namespace Modules\Package\Contracts\Service;

interface UserCardInfoContact
{

    public function addUserCardInfo($card_info);

    public function getAllCardInfo($userId);

    public function paymentCardDelete($id);

    public function paymentDefaultCardSet($id);

    public function getUserDefaultCard();

    public function getUserDefaultCardByUserId($userId);

    public function getCardInfoByUserId($user_id);

    public function getCardInfoById($id);

    public function getExistingCard($user_id);

    public function disableUserExistingCard($card_id);

    public function enableUserNewCard($card_id);

    public function paymentCardCreate($request);
}
