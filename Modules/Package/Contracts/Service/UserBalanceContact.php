<?php

namespace Modules\Package\Contracts\Service;

interface UserBalanceContact
{
    public function userBalanceUpdateOrCreate($where, $data);

    public function addUserBalance($user_balance);

    public function billingSetupUpdate($request);

    public function getByCondition($array);

    public function sumUserBalance($where, $credit);

    public function getCurrentCreditByUserId($user_id);

    public function getCurrentCreditAmountByUserId($user_id);

    public function getUserCurrentBalance($user_id, $user_current_package_id);

    public function updateCurrentCredit($userId, $newCredit);

    public function addCreditInUser($userId, $credit);

    public function cutCreditFromUser($userId, $cutAmount);

    public function reduceBalanceByUserIdAndType($userId, $type, $credit);

    public function increaseBalanceByUserIdAndType($userId, $type, $credit);

    public function checkBalanceByUserIdAndType($userId);
}
