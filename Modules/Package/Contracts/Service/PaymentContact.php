<?php

namespace Modules\Package\Contracts\Service;

interface PaymentContact
{
    public function createCustomerCard($data);

    public function chargeCreditFromQs($request);

    public function chargeCreditFromVMBuy($request);

    public function successPayment($userId, $packageDetail, $cardInfo, $type,$requestDowngradePackageArray = null, $transaction_id = null, $billing_period, $subscription_fee);

    public function failPayment($userId);

}
