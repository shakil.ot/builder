<?php

namespace Modules\Package\Contracts\Repositories;


interface RequestPackageRepository{

    public function  requestPackageUpdateOrCreate($where , $data);

    public function getRequestDataByUserId($userId);

    public function updateRequestStatusById($id, $status);

}
