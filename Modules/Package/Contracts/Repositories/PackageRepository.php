<?php

namespace Modules\Package\Contracts\Repositories;

interface PackageRepository
{
    public function PackageCreate($request);

    public function getTotalPackage();

    public function getAllPackage();

    public function getAllPaidPackageForBangladesh();

    public function getAllPaidPackageForInternational();

    public function getAllUpdatedPaidPackageForBangladesh($subscriptionFee);

    public function getAllUpdatedPaidPackageForInternational($subscriptionFee);

    public function getAllBangladeshiFreePackage();

    public function getAllInternationalFreePackage();

    public function getByPackageId($id);

    public function PackageDelete($id);

    public function PackageUpdate($where, $update);

    public function getPackageWithoutFreePackage();

    public function getAllFreePackage();

    public function getAllPaidPackage();

    public function getAllPackageWithoutFreeAndActive();

    public function countTotalStripeErrors();


}
