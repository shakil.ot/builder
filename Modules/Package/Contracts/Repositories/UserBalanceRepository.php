<?php

namespace Modules\Package\Contracts\Repositories;

interface UserBalanceRepository
{
    public function userBalanceUpdateOrCreate($where, $data);

    public function userBalanceCreate($data);

    public function getUserBalanceByUserId($id);

    public function addUserBalance($user_balance);

    public function sumUserBalance($where, $credit);

    public function getCurrentCreditByUserId($user_id);

    public function getByCondition($array);

    public function getUserCurrentBalance($user_id, $user_current_package_id);

    public function getUserAutoRecharge();

    public function updateCurrentCredit($userId, $newCredit);

    public function addCreditInUser($userId, $credit);

    public function reduceBalanceByUserIdAndType($userId, $type, $credit);

    public function increaseBalanceByUserIdAndType($userId, $type, $credit);

    public function checkBalanceByUserIdAndType($userId);


}
