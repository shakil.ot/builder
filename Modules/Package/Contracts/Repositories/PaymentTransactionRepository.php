<?php

namespace Modules\Package\Contracts\Repositories;

interface PaymentTransactionRepository
{
    public function transactionCreate($data);

    public function getAll($request = null , $users_id = null);

    public function getTransactionById($id);

    public function getAllTransactionFromUser($request = null, $subUsersId);

    public function getUserWiseDataForChart($user_id, $fromDate, $toDate);

    public function getThisMonthAutoRechargeByUserId($userId);

    public function getAllDataByUserIdAndType($userId, $type, $limit = 10);

    public function getThisMonthSumCreditAmountByUserIdAndType($userId, $type);

    public function getTransactionByDateRange($request , $users_id);

    public function countTotalTransactionReports();

    public function getAllByUserId($request = null , $users_id = null);

    public function getTransactionsByUserId($userId);
}
