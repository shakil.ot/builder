<?php

namespace Modules\Package\Contracts\Repositories;

interface UserCardInfoRepository
{
    public function addUserCardInfo($card_info);

    public function getAllCardInfo($userId);

    public function paymentCardDelete($id);

    public function getCardInfoByUserId($user_id);

    public function getCardInfoById($id);

    public function getExistingCard($user_id);

    public function disableUserExistingCard($card_id);

    public function enableUserNewCard($card_id);

    public function getPaymentCardById($id);

    public function getUserDefaultCard();

    public function createCustomerCard($data);

    public function updateCustomerCard($where, $data);

    public function paymentCardExistCheckByUserIdCardLastDigit($user_id, $card_number_last_four_digit);

    public function getUserDefaultCardByUserId($userId);

}
