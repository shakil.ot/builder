<?php

namespace Modules\Package\Contracts\Repositories;


interface StripeErrorRepository
{

    public function StripeErrorCreate($request);

    public function getStripeErrorByUserId($user_id);

    public function getAllStripeError();

    public function countTotalStripeErrors();

}
