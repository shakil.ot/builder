<?php

namespace Modules\Package\Contracts\Repositories;

interface ArchivePackageRepository
{
    public function ArchivePackageCreate($request);

    public function getArchiveListByUserId($user_id);
}
