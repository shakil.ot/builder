<?php

namespace Modules\Package\Contracts\Repositories;

interface UserCurrentPackageRepository
{
    public function getUserCurrentPackageInformationByUserId($userId);

    public function userCurrentPackageUpdateOrCreate($where, $data);

    public function addUserCurrentPackage($user_current_package);

    public function getUserActiveCurrentPackageInformationByUserId($user_id);

    public function getByPackageByUserId($userId);

    public function getCurrentUsePackageByPackageId($id);

    public function getByPackageByUserIdAndPackageId($userId, $packageId);

    public function changeAutoRecurringStatus($userId, $status);

    public function getCurrentPackageInfoById($id);

    public function deleteCurrentPackageByUserId($userId);
}
