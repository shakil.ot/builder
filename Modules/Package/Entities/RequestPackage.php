<?php

namespace Modules\Package\Entities;

use Illuminate\Database\Eloquent\Model;

class RequestPackage extends Model
{
    const STATUS_DEACTIVATE = 0;

    const STATUS_PENDING = 0;
    const STATUS_PROCESSING = 1;
    const STATUS_COMPLETED = 2;
    const STATUS_FAILED = 3;

    protected $fillable = [
        'user_id' ,
        'package_id' ,
        'user_payment_transaction_id' ,
        'subscription_fee' ,
        'trial_period' ,
        'expire_date' ,
        'life_line' ,
        'status' ,
        'free_credits',
        'max_user_count',
        'user_payment_transaction_id'
    ];
}
