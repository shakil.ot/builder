<?php

namespace Modules\Package\Entities;

use Illuminate\Database\Eloquent\Model;

class UserCurrentPackage extends Model
{
    const ACTIVE = 1;
    const INACTIVE = 2;

    const AUTO_RECURRING_ON = 1;
    const AUTO_RECURRING_OFF = 0;

    const IS_IN_TRIAL_PERIOD = 1;
    const IS_NOT_IN_TRIAL_PERIOD = 2;
    const IS_FREE_PERIOD = 3;


    const BILLING_PERIOD_MONTHLY = 1;
    const BILLING_PERIOD_YEARLY = 2;
    const BILLING_PERIOD_DAYWISE = 3;

    protected $fillable = [
        'user_id',
        'package_id',
        'user_payment_transaction_id',
        'subscription_fee',
        'charged_amount',
        'life_line',
        'trial_period',
        'is_use_trial_or_paid',
        'expire_date',
        'status',
        'billing_period',
        'package_name',
        'auto_recurring',
        'user_payment_transaction_id',
        'max_visitors',
        'max_contacts',
        'max_auto_followups',
        'max_email_send_per_month',
    ];

    public function packages()
    {
        return $this->belongsTo(Package::class, 'package_id');
    }






}
