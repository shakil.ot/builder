<?php

namespace Modules\Package\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;

class StripeError extends Model
{

    const TYPE_REGISTRATION = 1;
    const TYPE_RECURRING = 2;

    protected $fillable = [
        'user_id',
        'error',
        'ip',
        'type',
    ];

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
