<?php

namespace Modules\Package\Entities;

use Illuminate\Database\Eloquent\Model;

class UserCardInfo extends Model
{
    const CARD_STATUS_ACTIVE = 1;
    const CARD_STATUS_INACTIVE = 0;
    protected $fillable = ['stripe_customer_id', 'card_number', 'token', 'status', 'user_id'];
}
