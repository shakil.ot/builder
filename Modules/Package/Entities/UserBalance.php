<?php

namespace Modules\Package\Entities;

use Illuminate\Database\Eloquent\Model;

class UserBalance extends Model
{
    const AUTO_LOAD_STATUS_ON = 1;
    const AUTO_LOAD_STATUS_OFF = 0;

    protected $fillable = [
        'user_package_id',
        'user_id',
        'no_of_sub_user',
        'no_of_employee',
        'remaining_no_of_visitors',
        'remaining_no_of_contacts',
        'remaining_no_of_auto_followups',
        'remaining_no_of_email_send_per_month'
    ];


}
