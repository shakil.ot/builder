<?php

namespace Modules\Package\Entities;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    const PACKAGE_TYPE_PAID = 1;
    const PACKAGE_TYPE_FREE = 2;

    const PACKAGE_ACTIVE = 1;
    const PACKAGE_INACTIVE = 0;

    const PACKAGE_RENEW = 1;

    const PACKAGE_TRIAL_YES = 1;
    const PACKAGE_TRIAL_NO = 0;

    const COUNTRY_INTERNATIONAL = 1;
    const COUNTRY_BANGLADESH = 2;

    const IS_TWELVE_MONTHS_PAYMENT_REQUIRED = 1;
    const IS_TWELVE_MONTHS_PAYMENT_NOT_REQUIRED = 0;

    const NO_DISCOUNT = 0;

    const MAX_VISITORS_LIMITED = 1;
    const MAX_VISITORS_UNLIMITED = 2;
    const MAX_CONTACTS_LIMITED = 1;
    const MAX_CONTACTS_UNLIMITED = 2;
    const MAX_AUTO_FOLLOWUPS_LIMITED = 1;
    const MAX_AUTO_FOLLOWUPS_UNLIMITED = 2;
    const MAX_EMAIL_SEND_LIMITED = 1;
    const MAX_EMAIL_SEND_UNLIMITED = 2;




    protected $fillable = [
        'id',
        'name',
        'description',
        'life_line',
        'trial_pack',
        'subscription_fee',
        'free_credits',
        'status',
        'max_user_count',
        'package_type',
        'yearly_discount_percentage',
        'yearly_discount_price',
        'trial_period',
        'free_credits_for_trial_period',
        'country',
        'max_visitors_limitation_type',
        'max_visitors',
        'max_contacts',
        'max_contacts_limitation_type',
        'max_auto_followups',
        'max_auto_followups_limitation_type',
        'max_email_send_per_month',
        'max_email_send_limitation_type'
    ];

    public function credit()
    {
        return $this->hasOne(Credit::class);
    }

    public function userCurrentPackages()
    {
        return $this->hasMany(UserCurrentPackage::class);
    }

    public function countTotalUsersWithinAPackage()
    {
        return $this->hasMany(UserCurrentPackage::class);
    }

}
