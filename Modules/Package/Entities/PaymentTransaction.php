<?php

namespace Modules\Package\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;

class PaymentTransaction extends Model
{
    const TOTAL_CASH_COST_WHEN_BROADCAST = 0;
    const TOTAL_CASH_COST_WHEN_BUY_VIRTUALNUMBER = 0;

    const REGULAR_PACKAGE = 1;
    const PAY_AS_YOU_GO_PACKAGE = 2;


    //For Payment Transactions table
    const TRANSACTION_IN = 1;
    const TRANSACTION_OUT = 2;

    const PAYMENT_TRANSACTION_TYPE_FOR_SMS = 1;
    const PAYMENT_TRANSACTION_TYPE_FOR_MMS = 2;
    const PAYMENT_TRANSACTION_TYPE_FOR_VOICEMAIL = 3;
    const PAYMENT_TRANSACTION_TYPE_FOR_EMAIL = 4;
    const PAYMENT_TRANSACTION_TYPE_FOR_PACKAGE_SUBSCRIPTION = 5;
    const PAYMENT_TRANSACTION_TYPE_FOR_PACKAGE_RENEW = 6;
    const PAYMENT_TRANSACTION_TYPE_FOR_FREE_PACKAGE_ALLOCATE = 7;
    const PAYMENT_TRANSACTION_TYPE_FOR_AUTO_RECHARGE = 8;

    const PACKAGE_TYPE_PAID = 1;
    const PACKAGE_TYPE_FREE = 2;

    public static function getAllTypeFor()
    {
        return [
            self::PAYMENT_TRANSACTION_TYPE_FOR_SMS => 'SMS',
            self::PAYMENT_TRANSACTION_TYPE_FOR_MMS => 'MMS',
            self::PAYMENT_TRANSACTION_TYPE_FOR_VOICEMAIL => 'Voicemail',
            self::PAYMENT_TRANSACTION_TYPE_FOR_EMAIL => 'Email',
            self::PAYMENT_TRANSACTION_TYPE_FOR_PACKAGE_SUBSCRIPTION => 'Package Subscription',
            self::PAYMENT_TRANSACTION_TYPE_FOR_PACKAGE_RENEW => 'Package Renew',

        ];
    }


    protected $fillable = ['type', 'type_for', 'package_type', 'credit_amount', 'transaction_id', 'user_id', 'price'];

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
