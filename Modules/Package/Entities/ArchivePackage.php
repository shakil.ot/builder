<?php

namespace Modules\Package\Entities;

use Illuminate\Database\Eloquent\Model;

class ArchivePackage extends Model
{
    protected $fillable = [
        'user_id' ,
        'package_id',
        'user_payment_transaction_id' ,
        'package_taken_date',
        'subscription_fee' ,
        'trial_period' ,
        'expire_date' ,
        'status',
        'package_name',
        'max_user_count',
        'max_employee_count',
        'free_credits',
        'user_payment_transaction_id'

    ];
}
