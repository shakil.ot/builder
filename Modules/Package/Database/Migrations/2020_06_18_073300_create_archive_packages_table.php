<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArchivePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('archive_packages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->bigInteger('package_id');
            $table->string('package_name')->nullable();
            $table->bigInteger('user_payment_transaction_id');
            $table->float('subscription_fee');
            $table->integer('trial_period');
            $table->dateTime('expire_date');
            $table->integer('max_user_count');
            $table->integer('max_employee_count');
            $table->tinyInteger('status');
            $table->dateTime('package_taken_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('archive_packages');
    }
}
