<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->text('description');
            $table->integer('life_line');
            $table->boolean('trial_pack');
            $table->integer('trial_period')->default(0);
            $table->float('subscription_fee');
            $table->integer('yearly_discount_percentage')->nullable();
            $table->float('yearly_discount_price')->nullable();
            $table->boolean('status')->default(true);
            $table->tinyInteger('package_type')->comment("1=paid,2=free");
            $table->tinyInteger('country')->default(1)->comment('1=International | 2=Bangladesh');
            $table->unsignedBigInteger('max_visitors')->default(0);
            $table->tinyInteger('max_visitors_limitation_type')->default(2)->comment('1=limited|2=unlimited');
            $table->unsignedBigInteger('max_contacts')->default(0);
            $table->tinyInteger('max_contacts_limitation_type')->default(2)->comment('1=limited|2=unlimited');
            $table->unsignedBigInteger('max_auto_followups')->default(0);
            $table->tinyInteger('max_auto_followups_limitation_type')->default(2)->comment('1=limited|2=unlimited');
            $table->unsignedBigInteger('max_email_send_per_month')->default(0);
            $table->tinyInteger('max_email_send_limitation_type')->default(2)->comment('1=limited|2=unlimited');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
