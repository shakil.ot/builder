<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserBalancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_balances', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_package_id')->unsigned();
//            $table->foreign('user_package_id')->references('id')->on('user_current_packages')->onDelete('cascade');
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('remaining_no_of_visitors')->default(0);
            $table->unsignedBigInteger('remaining_no_of_contacts')->default(0);
            $table->unsignedBigInteger('remaining_no_of_auto_followups')->default(0);
            $table->unsignedBigInteger('remaining_no_of_email_send_per_month')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_balances');
    }
}
