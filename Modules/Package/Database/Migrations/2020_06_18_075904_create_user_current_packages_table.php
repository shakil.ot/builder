<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCurrentPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_current_packages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->bigInteger('package_id')->unsigned();
            $table->foreign('package_id')->references('id')->on('packages');
            $table->string('package_name')->nullable();
            $table->bigInteger('user_payment_transaction_id')->unsigned();
            $table->foreign('user_payment_transaction_id')->references('id')->on('payment_transactions')->onDelete('cascade');
            $table->float('subscription_fee');
            $table->float('charged_amount')->nullable();
            $table->integer('life_line');
            $table->integer('trial_period');
            $table->tinyInteger('is_use_trial_or_paid')->comment('1=using_trial,2=not_using_trial');
            $table->tinyInteger('billing_period')->comment('1=monthly, 2=yearly');
            $table->dateTime('expire_date');
            $table->tinyInteger('auto_recurring')->default(1)->comment('0 = Not Auto Recurring,1 = Auto Recurring');
            $table->tinyInteger('status')->default(1)->index();
            $table->unsignedBigInteger('max_visitors')->default(0);
            $table->unsignedBigInteger('max_contacts')->default(0);
            $table->unsignedBigInteger('max_auto_followups')->default(0);
            $table->unsignedBigInteger('max_email_send_per_month')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_current_packages');
    }
}
