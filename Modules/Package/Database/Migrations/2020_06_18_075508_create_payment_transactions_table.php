<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            //type is for in and out, 1=in, 2=out
            $table->tinyInteger('type')->comment('1=in| 2=out');
            $table->tinyInteger('type_for')->comment('5=package subscription');
            //Package Type=1 for paid, 2=for free
            $table->tinyInteger('package_type')->comment('1=paid|2=free');
            $table->float('price')->comment('Price of credit');
            $table->string('transaction_id')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::disableForeignKeyConstraints();
//        Schema::drop('sent_contact_responses');
//        Schema::enableForeignKeyConstraints();

        Schema::dropIfExists('payment_transactions');
    }
}
