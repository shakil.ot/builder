<form class="m-form
            m-form--fit
            m-form--label-align-right"
      id="package_create_form" data-parsley-validate="">
    @csrf
    <div class="m-portlet__body">
        <div class="m-portlet__body">

            <div class="form-group row">
                <div class="col-lg-6">
                    <label>
                        Package Type:
                    </label> <br>


                    <label class="m-radio m-radio--bold m-radio--state-brand">
                        <input type="radio" name="package_type" checked=""
                               value="{{ \Modules\Package\Entities\Package::PACKAGE_TYPE_PAID }}"> Paid &nbsp; &nbsp;
                        <span></span>
                    </label>
                    <label class="m-radio m-radio--bold m-radio--state-brand">
                        <input type="radio" name="package_type"
                               value="{{ \Modules\Package\Entities\Package::PACKAGE_TYPE_FREE }}"> Free
                        <span></span>
                    </label>

                </div>

            </div>


            <div class="row">
                <div class="col-md-6">
                    <div class="form-group custom_column">
                        <div class="custom-error-message">
                            <label>
                                Package Name:
                            </label>
                            <div class="m-input-icon m-input-icon--right tst">
                                <input type="text"
                                       class="form-control m-input"
                                       name="package_name"
                                       required
                                       data-parsley-trigger="change focusout"
                                       data-parsley-required-message="Package Name is required!">

                            </div>
                        </div>
                    </div>
                </div>
{{--                <div class="col-md-4">--}}
{{--                    <div class="form-group">--}}
{{--                        <div class="col-lg-12">--}}
{{--                            <label>--}}
{{--                                Country:--}}
{{--                            </label>--}}
{{--                            <div class="m-input-icon m-input-icon--right">--}}
{{--                                <select name="country" class="form-control country" id="" required>--}}
{{--                                    <option value="{{\Modules\Package\Entities\Package::COUNTRY_INTERNATIONAL}}">--}}
{{--                                        International--}}
{{--                                    </option>--}}
{{--                                    <option value="{{\Modules\Package\Entities\Package::COUNTRY_BANGLADESH}}">--}}
{{--                                        Bangladesh--}}
{{--                                    </option>--}}
{{--                                </select>--}}

{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}

                <div class="col-md-6">
                    <div class="form-group custom_column">
                        <div id="subscription-fee">
                            <label>
                                Subscription Fee:
                            </label>
                            <div class="m-input-icon m-input-icon--right">
                                <input type="number"
                                       class="form-control m-input"
                                       name="subscription_fee"
                                       id="subscription_fee"
                                       min="1"
                                       data-parsley-min="0.0"
                                       data-parsley-trigger="change focusout"
                                       required
                                       data-parsley-error-message="Subscription Fee should be greater than or equal to $1">

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group row discount-section">


                <div class="col-lg-6">
                    <label>
                        Yearly Discount Percentage :
                    </label>
                    <div class="m-input-icon m-input-icon--right">
                        <input type="number"
                               data-parsley-type="number"
                               class="form-control m-input yearly_discount_percentage"
                               name="yearly_discount_percentage"
                               required=""
                               id="yearly_discount_percentage"
                               data-parsley-trigger="change focusout"
                               maxlength="3"
                               data-parsley-maxlength="3"
                               data-parsley-error-message="Yearly discount percentage should be 0 to 100 %"
                               min="0"
                               data-parsley-min="0"
                               max="100"
                               data-parsley-max="100"
                               data-parsley-multiple-of="3"
                               value=""
                               data-parsley-required-message="Yearly discount percentage is required!">
                    </div>
                </div>


                <div class="col-lg-6">
                    <label>
                        Yearly discount price:
                    </label>
                    <div class="m-input-icon m-input-icon--right">
                        <input type="number" readonly
                               class="form-control m-input"
                               name="yearly_discount_price"
                               required=""
                               id="yearly_discount_price"
                               data-parsley-type="number"
                               data-parsley-trigger="change focusout"
                               min="1"
                               value="">
                    </div>

                </div>
            </div>

            <div class="form-group row">
                <div class="col-lg-6 custom-error-message">
                    <label>
                        Package Validity Time in Day:
                    </label>
                    <div class="m-input-icon m-input-icon--right">

                        <input type="number"
                               class="form-control m-input"
                               name="life_line"
                               id="life_line_for_package"
                               required=""
                               data-parsley-type="number"
                               data-parsley-trigger="change focusout"
                               min="1"
                               data-parsley-min="1"
                               data-parsley-required-message="Package Validity Time in Day is required!"
                               data-parsley-error-message="Package Validity Day minimum value of 1">
                    </div>

                </div>
                <div class="col-lg-6"></div>
            </div>

            <div class="form-group row trail_package_display">
                <div class="col-lg-6">
                    <label>
                        Trial Package:
                    </label>
                    <div class="m-input-icon m-input-icon--right">
                        <span class="m-switch m-switch--sm m-switch--icon">
                            <label>
                                 <input type="checkbox" name="trail_package" id="trail_package_option" value="false">
                                <span></span>
                            </label>

                        </span>
                    </div>

                </div>

                <div class="col-lg-6 trial_period_display">
                    <label>
                        Trial Period:
                    </label>
                    <div class="m-input-icon m-input-icon--right">
                        <input type="number"
                               class="form-control m-input"
                               name="trial_period"
                               id="trial_period"
                               value="">

                    </div>
                </div>
            </div>

            <div class="form-group row max_visitors_limitation_type_display">
                <div class="col-lg-6">
                    <label>
                        Max Visitors' Limitation:
                    </label>
                    <div class="m-input-icon m-input-icon--right">
                        <span class="m-switch m-switch--sm m-switch--icon">
                            <label>
                                 <input type="checkbox" name="max_visitors_limitation_type" checked id="max_visitors_limitation_type_option" value="true">
                                <label for="" class="max_visitors_limitation_text">Unlimited</label>
                                <span></span>
                            </label>

                        </span>
                    </div>

                </div>

                <div class="col-lg-6 max_visitors_display">
                    <label>
                        Max Visitors:
                    </label>
                    <div class="m-input-icon m-input-icon--right">
                        <input type="number"
                               class="form-control m-input"
                               name="max_visitors"
                               id="max_visitors"
                               value="" min="0">

                    </div>
                </div>
            </div>
            <div class="form-group row max_contacts_limitation_type_display">
                <div class="col-lg-6">
                    <label>
                        Max Contacts' Limitation:
                    </label>
                    <div class="m-input-icon m-input-icon--right">
                        <span class="m-switch m-switch--sm m-switch--icon">
                            <label>
                                 <input type="checkbox" name="max_contacts_limitation_type" checked id="max_contacts_limitation_type_option" value="true">
                                <label for="" class="max_contacts_limitation_text">Unlimited</label>
                                <span></span>
                            </label>

                        </span>
                    </div>

                </div>

                <div class="col-lg-6 max_contacts_display">
                    <label>
                        Max Contacts:
                    </label>
                    <div class="m-input-icon m-input-icon--right">
                        <input type="number"
                               class="form-control m-input"
                               name="max_contacts"
                               id="max_contacts"
                               value="" min="0">

                    </div>
                </div>
            </div>
            <div class="form-group row max_auto_followups_limitation_type_display">
                <div class="col-lg-6">
                    <label>
                        Max Auto Followups' Limitation:
                    </label>
                    <div class="m-input-icon m-input-icon--right">
                        <span class="m-switch m-switch--sm m-switch--icon">
                            <label>
                                 <input type="checkbox" name="max_auto_followups_limitation_type" checked id="max_auto_followups_limitation_type_option" value="true">
                                <label for="" class="max_auto_followups_limitation_text">Unlimited</label>
                                <span></span>
                            </label>

                        </span>
                    </div>

                </div>

                <div class="col-lg-6 max_auto_followups_display">
                    <label>
                        Max Auto Followups:
                    </label>
                    <div class="m-input-icon m-input-icon--right">
                        <input type="number"
                               class="form-control m-input"
                               name="max_auto_followups"
                               id="max_auto_followups"
                               value="" min="0">

                    </div>
                </div>
            </div>
            <div class="form-group row max_email_send_limitation_type_display">
                <div class="col-lg-6">
                    <label>
                        Max Email Send' Limitation Per Month:
                    </label>
                    <div class="m-input-icon m-input-icon--right">
                        <span class="m-switch m-switch--sm m-switch--icon">
                            <label>
                                 <input type="checkbox" name="max_email_send_limitation_type" checked id="max_email_send_limitation_type_option" value="true">
                                <label for="" class="max_email_send_limitation_text">Unlimited</label>
                                <span></span>
                            </label>

                        </span>
                    </div>

                </div>

                <div class="col-lg-6 max_email_send_display">
                    <label>
                        Max Email Send Per Month:
                    </label>
                    <div class="m-input-icon m-input-icon--right">
                        <input type="number"
                               class="form-control m-input"
                               name="max_email_send"
                               id="max_email_send"
                               value="" min="0">

                    </div>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-lg-12">
                    <label>
                        Description:
                    </label>
                    <div class="m-input-icon m-input-icon--right">
                                            <textarea class="form-control m-input"
                                                      name="description"
                                                      rows="3"></textarea>
                    </div>
                </div>


            </div>





            <div class="m-form__actions  text-right">
                <button type="submit" class="btn btn-success">
                    <span>
                        <i class="la la-plus-circle"></i>
                    </span>
                    Add Package
                </button>
            </div>
        </div>
    </div>
</form>
