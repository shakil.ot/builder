<form class="m-form
            m-form--fit
            m-form--label-align-right"
      id="package_edit_form" data-parsley-validate="">
    <input type="hidden" value="{{ $packageEdit->id}}" id="hidden_id_of_package" name="hidden_id_of_package">
    @csrf
    <div class="m-portlet__body">
        <div class="m-portlet__body">

            <div class="form-group row">


                <div class="col-lg-6">
                    <label>
                        Package Type:
                    </label> <br>


                    <label class="m-radio m-radio--bold m-radio--state-brand">
                        <input type="radio" name="package_type" id="package_type_paid" class="package_type_edit_paid"
                               {{ $packageEdit->package_type == \Modules\Package\Entities\Package::PACKAGE_TYPE_PAID ? 'checked' : '' }}
                               value="{{ \Modules\Package\Entities\Package::PACKAGE_TYPE_PAID }}"> Paid &nbsp; &nbsp;
                        <span></span>
                    </label>
                    <label class="m-radio m-radio--bold m-radio--state-brand">
                        <input type="radio" name="package_type" id="package_type_free" class="package_type_edit"
                               {{ $packageEdit->package_type == \Modules\Package\Entities\Package::PACKAGE_TYPE_FREE ? 'checked' : '' }}
                               value="{{ \Modules\Package\Entities\Package::PACKAGE_TYPE_FREE }}"> Free
                        <span></span>
                    </label>

                </div>

            </div>


            <div class="row">
                <div class="col-md-6">
                    <div class="form-group custom_column">
                        <div class="col-lg-12">
                            <label>
                                Package Name:
                            </label>
                            <div class="m-input-icon m-input-icon--right tst">
                                <input type="text"
                                       class="form-control m-input"
                                       name="package_name"
                                       required=""
                                       value="{{ $packageEdit->name }}"
                                       data-parsley-trigger="change focusout"
                                       data-parsley-required-message="Package Name is required!">

                            </div>
                        </div>
                    </div>
                </div>

                {{--                <div class="col-md-4">--}}
                {{--                    <div class="form-group custom_column">--}}
                {{--                        <div class="col-lg-12 ">--}}
                {{--                            <label>--}}
                {{--                                Country:--}}
                {{--                            </label>--}}
                {{--                            <div class="m-input-icon m-input-icon--right">--}}
                {{--                                <select name="country" class="form-control country-edit" id="" required>--}}
                {{--                                    <option--}}
                {{--                                        value="{{\Modules\Package\Entities\Package::COUNTRY_INTERNATIONAL}}" {{$packageEdit->country == \Modules\Package\Entities\Package::COUNTRY_INTERNATIONAL ? 'selected':''}}>--}}
                {{--                                        International--}}
                {{--                                    </option>--}}
                {{--                                    <option--}}
                {{--                                        value="{{\Modules\Package\Entities\Package::COUNTRY_BANGLADESH}}" {{$packageEdit->country == \Modules\Package\Entities\Package::COUNTRY_BANGLADESH ? 'selected':''}}>--}}
                {{--                                        Bangladesh--}}
                {{--                                    </option>--}}
                {{--                                </select>--}}

                {{--                            </div>--}}
                {{--                        </div>--}}
                {{--                    </div>--}}
                {{--                </div>--}}

                @if($packageEdit->package_type == \Modules\Package\Entities\Package::PACKAGE_TYPE_PAID)
                    <div class="col-md-6">
                        <div class="form-group custom_column">
                            <div
                                class="col-lg-12 {{ $packageEdit->package_type == \Modules\Package\Entities\Package::PACKAGE_TYPE_PAID ? 'for-paid-package' : 'for-free-package' }}"
                                id="subscription-fee">
                                <label>
                                    Subscription Fee:
                                </label>
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="number"
                                           class="form-control m-input"
                                           name="subscription_fee"
                                           id="subscription_fee"
                                           value="{{ $packageEdit->subscription_fee }}"
                                           min="{{$packageEdit->package_type == \Modules\Package\Entities\Package::PACKAGE_TYPE_PAID ? '1':'0'}}"
                                           data-parsley-min="0.0"
                                           data-parsley-trigger="change focusout"
                                           {{ $packageEdit->subscription_fee > 0 ? 'required' : '' }}
                                           data-parsley-error-message="Subscription Fee should be greater than or equal to $1">

                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>

            @if($packageEdit->package_type == \Modules\Package\Entities\Package::PACKAGE_TYPE_PAID)

                <div class="form-group row">

                    <div class="col-lg-6" id="yearlyDiscountPercentage">
                        <label>
                            Yearly Discount Percentage :
                        </label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="number"
                                   data-parsley-type="number"
                                   class="form-control m-input yearly_discount_percentage"
                                   name="yearly_discount_percentage"
                                   id="yearly_discount_percentage"
                                   required=""
                                   data-parsley-trigger="change focusout"
                                   maxlength="3"
                                   data-parsley-maxlength="3"
                                   data-parsley-error-message="Maximum percentage should be 0 to 100"
                                   min="0"
                                   data-parsley-min="0"
                                   max="100"
                                   data-parsley-max="100"
                                   data-parsley-multiple-of="3"
                                   value="{{ $packageEdit->yearly_discount_percentage  }}"
                                   data-parsley-required-message="Yearly discount percentage is required!">
                        </div>
                    </div>


                    <div class="col-lg-6" id="yearlyDiscountPrice">
                        <label>
                            Yearly discount price:
                        </label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="number"
                                   class="form-control m-input yearly_discount_price"
                                   name="yearly_discount_price"
                                   required=""
                                   id="yearly_discount_price"
                                   data-parsley-type="number"
                                   data-parsley-trigger="change focusout"
                                   min="1"
                                   value="{{ $packageEdit->yearly_discount_price }}"
                                   data-parsley-error-message="Package Validity Day minimum value of 1">
                        </div>

                    </div>
                </div>
            @endif


            <div class="form-group row">
                <div class="col-lg-6">
                    <label>
                        Package Validity Time in Day:
                    </label>
                    <div class="m-input-icon m-input-icon--right">
                        <input type="number"
                               class="form-control m-input"
                               name="life_line"
                               required=""
                               id="life_line"
                               data-parsley-type="number"
                               data-parsley-trigger="change focusout"
                               min="1"
                               value="{{ $packageEdit->life_line }}"
                               data-parsley-min="1"
                               data-parsley-required-message="Package Validity Time in Day is required!"
                               data-parsley-error-message="Package Validity Day minimum value of 1">
                    </div>

                </div>
                <div class="col-lg-6"></div>
            </div>

            @if($packageEdit->package_type == \Modules\Package\Entities\Package::PACKAGE_TYPE_PAID)
                <div class="form-group row trail_package_display common-hide-class
{{ $packageEdit->package_type == \Modules\Package\Entities\Package::PACKAGE_TYPE_PAID ? 'for-paid-package' : 'for-free-package' }}">
                    <div class="col-lg-6">
                        <label>
                            Trial Package:
                        </label>
                        <div class="m-input-icon m-input-icon--right">
                        <span class="m-switch m-switch--sm m-switch--icon">
                            <label>
{{--                                <input type="checkbox" name="trail_package" id="trail_package_option" value="false">--}}
                                 <input type="checkbox"
                                        name="trail_package_edit"
                                        id="trail_package_edit"
                                       {{ $packageEdit->trial_pack == 1 ? 'checked' : '' }}
                                 >
                                <span></span>
                            </label>
                        </span>
                        </div>

                    </div>

                    <div
                        class="col-lg-6 trial_period_display {{ $packageEdit->trial_pack == 1 ? 'for-trial-period' : 'for-trial-period-hide' }}">
                        <label>
                            Trial Period:
                        </label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="number"
                                   class="form-control m-input"
                                   name="trial_period_edit"
                                   id="trial_period_edit"
                                   value="{{ $packageEdit->trial_period }}"
                            >

                        </div>
                    </div>

                </div>
            @endif

            <div class="form-group row edit_max_visitors_limitation_type_display">
                <div class="col-lg-6">
                    <label>
                        Max Visitors' Limitation:
                    </label>
                    <div class="m-input-icon m-input-icon--right">
                        <span class="m-switch m-switch--sm m-switch--icon">
                            <label>
                                 <input type="checkbox" name="edit_max_visitors_limitation_type"
                                        id="edit_max_visitors_limitation_type_option" value="true" {{$packageEdit->max_visitors_limitation_type ==\Modules\Package\Entities\Package::MAX_VISITORS_UNLIMITED ? 'checked':''}}>
                                <label for="" class="edit_max_visitors_limitation_text">Unlimited</label>
                                <span></span>
                            </label>

                        </span>
                    </div>

                </div>

                <div
                    class="col-lg-6 edit_max_visitors_display {{$packageEdit->max_visitors_limitation_type ==\Modules\Package\Entities\Package::MAX_VISITORS_UNLIMITED ? 'd-none':''}}">
                    <label>
                        Max Visitors:
                    </label>
                    <div class="m-input-icon m-input-icon--right">
                        <input type="number"
                               class="form-control m-input"
                               name="edit_max_visitors"
                               id="edit_max_visitors"
                               value="{{$packageEdit->max_visitors}}" min="0">

                    </div>
                </div>
            </div>
            <div class="form-group row edit_max_contacts_limitation_type_display">
                <div class="col-lg-6">
                    <label>
                        Max Contacts' Limitation:
                    </label>
                    <div class="m-input-icon m-input-icon--right">
                        <span class="m-switch m-switch--sm m-switch--icon">
                            <label>
                                 <input type="checkbox" name="edit_max_contacts_limitation_type"
                                        id="edit_max_contacts_limitation_type_option" value="true" {{$packageEdit->max_contacts_limitation_type ==\Modules\Package\Entities\Package::MAX_CONTACTS_UNLIMITED ? 'checked':''}}>
                                <label for="" class="edit_max_contacts_limitation_text">Unlimited</label>
                                <span></span>
                            </label>

                        </span>
                    </div>

                </div>

                <div
                    class="col-lg-6 edit_max_contacts_display {{$packageEdit->max_contacts_limitation_type ==\Modules\Package\Entities\Package::MAX_CONTACTS_UNLIMITED ? 'd-none':''}}">
                    <label>
                        Max Contacts:
                    </label>
                    <div class="m-input-icon m-input-icon--right">
                        <input type="number"
                               class="form-control m-input"
                               name="edit_max_contacts"
                               id="edit_max_contacts"
                               value="{{$packageEdit->max_contacts}}" min="0">

                    </div>
                </div>
            </div>
            <div class="form-group row edit_max_auto_followups_limitation_type_display">
                <div class="col-lg-6">
                    <label>
                        Max Auto Followups' Limitation:
                    </label>
                    <div class="m-input-icon m-input-icon--right">
                        <span class="m-switch m-switch--sm m-switch--icon">
                            <label>
                                 <input type="checkbox" name="edit_max_auto_followups_limitation_type"
                                        id="edit_max_auto_followups_limitation_type_option" value="true" {{$packageEdit->max_auto_followups_limitation_type ==\Modules\Package\Entities\Package::MAX_AUTO_FOLLOWUPS_UNLIMITED ? 'checked':''}}>
                                <label for="" class="edit_max_auto_followups_limitation_text">Unlimited</label>
                                <span></span>
                            </label>

                        </span>
                    </div>

                </div>

                <div
                    class="col-lg-6 edit_max_auto_followups_display {{$packageEdit->max_auto_followups_limitation_type ==\Modules\Package\Entities\Package::MAX_AUTO_FOLLOWUPS_UNLIMITED ? 'd-none':''}}">
                    <label>
                        Max Auto Followups:
                    </label>
                    <div class="m-input-icon m-input-icon--right">
                        <input type="number"
                               class="form-control m-input"
                               name="edit_max_auto_followups"
                               id="edit_max_auto_followups"
                               value="{{$packageEdit->max_auto_followups}}" min="0">

                    </div>
                </div>
            </div>
            <div class="form-group row edit_max_email_send_limitation_type_display">
                <div class="col-lg-6">
                    <label>
                        Max Email Send Limitation Per Month:
                    </label>
                    <div class="m-input-icon m-input-icon--right">
                        <span class="m-switch m-switch--sm m-switch--icon">
                            <label>
                                 <input type="checkbox" name="edit_max_email_send_limitation_type"
                                        id="edit_max_email_send_limitation_type_option" value="true" {{$packageEdit->max_email_send_limitation_type ==\Modules\Package\Entities\Package::MAX_EMAIL_SEND_UNLIMITED ? 'checked':''}}>
                                <label for="" class="edit_max_email_send_limitation_text">Unlimited</label>
                                <span></span>
                            </label>

                        </span>
                    </div>

                </div>

                <div
                    class="col-lg-6 edit_max_email_send_display {{$packageEdit->max_email_send_limitation_type ==\Modules\Package\Entities\Package::MAX_EMAIL_SEND_UNLIMITED ? 'd-none':''}}">
                    <label>
                        Max Email Send Per Month:
                    </label>
                    <div class="m-input-icon m-input-icon--right">
                        <input type="number"
                               class="form-control m-input"
                               name="edit_max_email_send"
                               id="edit_max_email_send"
                               value="{{$packageEdit->max_email_send_per_month}}" min="0">

                    </div>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-lg-12">
                    <label>
                        Description:
                    </label>
                    <div class="m-input-icon m-input-icon--right">
                                            <textarea class="form-control m-input"
                                                      name="description" id="description"
                                                      rows="3">{{ $packageEdit->description }}</textarea>
                    </div>
                </div>


            </div>


            <div class="m-form__actions  text-right">
                <button type="submit" class="btn" style="background-color: #023c64; color: #ffffff">
                    <span>
                        <i class="la la-refresh" style="color: #ffffff"></i>
                    </span>
                    Update Package
                </button>
            </div>
        </div>
    </div>
</form>
