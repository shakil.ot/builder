var PackageManage = (function (PackageObj) {

    $(function () {

    const _token = document.head.querySelector("[property=csrf-token]").content;


    (function (global, $, _token) {


        // #### DATATABLE FOR USER-LIST. ALL USER LIST FITCH FROM DATABASED
        var dataTable = $('#package-table').DataTable({
            responsive: true,
            // Pagination settings
            // read more: https://datatables.net/examples/basic_init/dom.html
            lengthMenu: [5, 10, 25, 50, 100, 500],
            pageLength: 10,
            language: {
                'lengthMenu': 'Display _MENU_',
            },
            searchDelay: 500,
            processing: true,
            serverSide: true,
            ajax:  {
                url:route('admin.package-list'),
                type: 'get'
            },
            columns: [

                {data: 'name', name: 'name', "orderable": true, "searchable": true, width: "15%"},
                {data: 'subscription_fee', name: 'subscription_fee',"orderable": true,"searchable": true,width: "15%"},
                {data: 'life_line', name: 'life_line', "orderable": true, "searchable": true, width: "15%"},
                {data: 'package_type', name: 'package_type', "orderable": true, "searchable": true, width: "15%"},
                {data: 'trial_pack', name: 'trial_pack', "orderable": true, "searchable": true, width: "10%"},
                {data: 'yearly_discount', name: 'yearly_discount', "orderable": true, "searchable": true, width: "5%"},
                {data: 'yearly_discount_price', name: 'yearly_discount_price', "orderable": true, "searchable": true, width: "5%"},
                {data: 'status', name: 'status', "orderable": true, "searchable": true, width: "5%"},
                {data: 'count_total_users_within_a_package_count', name: 'count_total_users_within_a_package_count', "orderable": false, "searchable": false, width: "20%"},
                {data: 'action', name: 'action', "orderable": false, "searchable": false, width: "20%"},

            ],
            columnDefs: [
                {responsivePriority: 1, targets: 7},
                {responsivePriority: 2, targets: 0}
            ],
            dom: '<"top-toolbar"<"top-left-toolbar"lB><"top-right-toolbar"f>>rt<"bottom-toolbar"<"bottom-left-toolbar"i><"bottom-right-toolbar"p>>',
            buttons: [
                {
                    extend: 'colvis',
                    text: '<i class="fa \tfa-list-ul pr-1"></i>Columns',
                    attr: {
                        id: "custom-colvis-button",
                        class: 'btn btn-outline-primary btn-sm m-btn',
                        style: '',
                        title: 'Columns Visibility'
                    },
                    columns: ':not(.noVis)',
                    columnText: function (dt, idx, title) {
                        // return (idx+1)+': '+title;
                        return title;
                    },
                    postfixButtons: ['colvisRestore']
                },
                {
                    extend: 'colvisGroup',
                    text: '<i class="fa fa-refresh pr-1"></i>Refresh',
                    attr: {
                        class: 'btn btn-outline-primary btn-sm m-btn  m-btn--icon',
                        id: 'refresh-button'
                    },
                    show: ':hidden'
                }
            ],
            "createdRow": function (row, data) {
            }
        });
        //Columns visibility buttons' css
        $("#custom-colvis-button").click(function () {
            $(".buttons-columnVisibility").removeClass("dt-button").addClass("btn btn-outline-primary btn-sm m-btn btn-block");
            $(".buttons-colvisRestore").removeClass("dt-button").addClass("btn btn-outline-brand btn-sm m-btn btn-block");
        });
        $("#refresh-button").click(function () {
            refreshTable();
        });

        function refreshTable(params) {
            dataTable.columns().search('');
            dataTable.search('');
            dataTable.ajax.reload();
        }



        var package_type_paid = PackageObj.packageTypePaid;
        var package_type_free = PackageObj.packageTypeFree;
        /****### -ADD-BUTTON FUNCTION START- ###***/
        /****###  ADD NEW USER FUNCTION. WHEN CLICK .add-new-item IT WILL OPEN A MODAL WITH A FORM  ###***/
        var addButton = $(".add-new-item");
        addButton.on('click', function () {
            $('#preloader').show();

            var detailPopUp = global.customPopup.show({
                header: 'Add Package',
                message: '',
                dialogSize: 'lg',
                dialogClass: 'prevent-to-show'
            });

            detailPopUp.on('shown.bs.modal', function () {
                var modal = $(this);

                global.mySiteAjax({
                    url: route('admin.package-get-from'),
                    type: 'get',
                    loadSpinner: true,
                    success: function (response) {
                        $('#preloader').hide();
                        window.nbUtility.ajaxErrorHandling(response, function () {
                            modal.find('.modal-body').html(response.html);
                            modal.removeClass('prevent-to-show');

                            $('.trial_period_display').hide();

                            package_type_paid = package_type_paid;

                            modal.on('change', 'input[name=package_type]:checked', function () {
                                var package_type_value = $(this).val();
                                package_type_paid = package_type_value;
                                if (package_type_value == package_type_paid ) {
                                    $('.free_credit_display').show();
                                    $('.trail_package_display').show();
                                    $('#subscription-fee').show();
                                    $('#free_credit').attr("required","");
                                    $('#subscription_fee').attr("required","");
                                    $('#trial_period').attr("required","");
                                    $('#free_credits_for_trial_period').attr("required","");

                                }

                                if (package_type_value == package_type_free) {
                                    $('.free_credit_display').hide();
                                    $('.trail_package_display').hide();
                                    $('#subscription-fee').hide();
                                    $('#free_credit').removeAttr("required");
                                    $('#subscription_fee').removeAttr("required");
                                    $('#trial_period').removeAttr("required");


                                }
                            });

                            modal.on('change', '#trail_package_option', function () {
                                if ($('#trail_package_option').is(':checked') == 'true' || $('#trail_package_option').is(':checked') == true) {
                                    $('.trial_period_display').show();
                                    $('#trial_period').attr("required", '');
                                    $('#free_credits_for_trial_period').attr("required", '');
                                } else {
                                    $('.trial_period_display').hide();
                                    $('#trial_period').removeAttr("required");
                                    $('#free_credits_for_trial_period').removeAttr("required");
                                }
                            });


                            modal.find('#trail_package_option').click(function () {
                                var trail_package_option = modal.find('#trail_package_option').val();
                                if (trail_package_option == 'true') {
                                    modal.find('#trail_package_option').val('false');
                                } else {
                                    modal.find('#trail_package_option').val('true');
                                }
                            });


                            $('#package_create_form')
                                .parsley(global.parsleySettings.settings)
                                .on('form:success', function (formInstance) {

                                    if (formInstance.isValid()) {
                                        if (package_type_paid == package_type_paid){
                                            if (modal.find('#trail_package_option').val() == true || modal.find('#trail_package_option').val() == "true") {
                                                if (parseInt(modal.find('#trial_period').val()) >= parseInt(modal.find('#life_line_for_package').val())) {
                                                    toastr.error("Trial period can not greater than package validity time in day", "success");
                                                    return false;
                                                }
                                            }
                                        }

                                        global.mySiteAjax({
                                            url: route('admin.package-create'),
                                            type: 'post',
                                            data: $('#package_create_form').serializeArray(),
                                            loadSpinner: true,
                                            success: function (response) {
                                                /* parsley css here end */
                                                if (response.status == 'validation-error')
                                                    global.generateErrorMessage(formInstance, response.html);
                                                else if (response.status == 'success') {
                                                    modal.modal('hide');
                                                    bootstrapNotify.success(response.html);
                                                    dataTable.ajax.reload();
                                                } else if (response.status == 'error') {
                                                    route('package-update').error(response.html, "Server Notification");
                                                }
                                            },
                                            complete: function (response) {
                                                if (response.status == 422) {
                                                    global.generateErrorMessage(formInstance, response.responseJSON.errors);
                                                    if (response.responseJSON.errors.package_name !== undefined) {
                                                        bootstrapNotify.error(response.responseJSON.errors.package_name, "success");
                                                    } else if (response.responseJSON.errors.max_user_count !== undefined) {
                                                        bootstrapNotify.error(response.responseJSON.errors.max_user_count, "success");
                                                    } else if (response.responseJSON.errors.life_line !== undefined) {
                                                        bootstrapNotify.error(response.responseJSON.errors.life_line, "success");
                                                    }
                                                }
                                            }
                                        });

                                    }
                                }).on('form:submit', function () {
                                return false;
                            });
                        });
                    }
                });
            });

            detailPopUp.on('hidden.bs.modal', function () {
                $(this).remove();
            });
        });




        /****### ACTION FUNCTION START ####***/

        $(document).on('click','.action', function (e) {
            var id = $(this).attr('data-id');


            var settingsType = $(this).attr('data-value');

            if (!settingsType)
                bootstrapNotify.error('Value is empty!', 'error')


            switch (settingsType) {

                case "edit":
                    edit(id);
                    break;
                case "active-deactivate":
                    packageStatusUpdate(id);
                    break;

                default :
                    bootstrapNotify.error("Invalid Setting type", "Server Notification");
                    break;

            }

            function edit(id) {

                $('#preloader').show();

                var detailPopUp = global.customPopup.show({
                    header: 'Edit Package',
                    message: '',
                    dialogSize: 'lg',
                    dialogClass: 'prevent-to-show'
                });

                detailPopUp.on('shown.bs.modal', function () {
                    var modal = $(this);

                    modal.on('change', 'input[name=package_type]:checked', function () {
                        var package_type_value = $(this).val();


                        if (package_type_value == package_type_paid ) {
                            $('.common-hide-class').show();
                            $('#subscription-fee').show();
                            modal.find('#free_credit_edit').attr("required", '');
                            modal.find('#subscription_fee').attr("min", '1');
                            modal.find('#subscription_fee').attr("data-parsley-min", '0.0');
                            modal.find('#subscription_fee').attr("required", '');
                            modal.find('#trial_period_edit').attr("required", '');
                            modal.find('#free_credits_for_trial_period_edit').attr("required", '');
                        }

                        if (package_type_value == package_type_free ) {
                            $('.common-hide-class').hide();
                            $('#subscription-fee').hide();
                            modal.find('#free_credit_edit').removeAttr("required");
                            modal.find('#subscription_fee').removeAttr("min");
                            modal.find('#subscription_fee').removeAttr("data-parsley-min");
                            modal.find('#trial_period_edit').removeAttr("required");
                            modal.find('#free_credits_for_trial_period_edit').removeAttr("required");

                        }
                    });

                    modal.on('change', '#trail_package_edit', function () {
                        if ($('#trail_package_edit').is(':checked') == 'true' || $('#trail_package_edit').is(':checked') == true) {
                            $('.for-trial-period-hide').show();
                            $('.for-trial-period').show();
                            modal.find('#trial_period_edit').attr("required", '');
                            modal.find('#free_credits_for_trial_period_edit').attr("required", '');
                        } else {
                            $('.for-trial-period-hide').hide();
                            $('.for-trial-period').hide();
                            modal.find('#trial_period_edit').removeAttr("required");
                            modal.find('#free_credits_for_trial_period_edit').removeAttr("required");
                        }
                    });


                    global.mySiteAjax({
                        url: route('admin.package-edit-get-from'),
                        type: 'post',
                        data: {'id': id, _token: _token},
                        loadSpinner: true,
                        success: function (response) {
                            $('#preloader').hide();
                            window.nbUtility.ajaxErrorHandling(response, function () {
                                modal.find('.modal-body').html(response.html);
                                modal.removeClass('prevent-to-show');

                                $('#package_edit_form')
                                    .parsley(global.parsleySettings.settings)
                                    .on('form:success', function (formInstance) {

                                        if (formInstance.isValid()) {
                                            if ($('.package_type_edit_paid').is(':checked') == "true" || $('.package_type_edit_paid').is(':checked') == true) {
                                                if ($('#trail_package_edit').is(':checked') == true || $('#trail_package_edit').is(':checked') == "true") {
                                                    if (parseInt(modal.find('#trial_period_edit').val()) >= parseInt(modal.find('#life_line').val())) {
                                                        bootstrapNotify.error("Trial period must be less than package validity time in day", "error");
                                                        return false;
                                                    }
                                                }
                                            }


                                            $('#preloader').show();
                                            global.mySiteAjax({
                                                url: route('admin.package-update'),
                                                type: 'post',
                                                data: $('#package_edit_form').serialize(),
                                                loadSpinner: true,
                                                success: function (response) {
                                                    if (response.status == 'validation-error') {
                                                        global.generateErrorMessage(formInstance, response.html, 'div.form-div:last-child');
                                                        $('#preloader').hide();
                                                    } else if (response.status == 'success') {
                                                        modal.modal('hide');
                                                        bootstrapNotify.success(response.html, '');
                                                        $('#preloader').hide();
                                                        dataTable.draw();
                                                        // dataContactTable.draw();
                                                    } else if (response.status == 'error') {
                                                        bootstrapNotify.error(response.html, '');
                                                        $('#preloader').hide();
                                                        global.generateErrorMessage(formInstance, response.html, 'div.form-div:last-child');
                                                    }
                                                },
                                                complete: function (data) {
                                                    $('#preloader').hide();
                                                    if(data.status == 422){
                                                        if (data.responseJSON.errors.package_name !== undefined)
                                                            bootstrapNotify.error(data.responseJSON.errors.package_name, '', {"closeButton": true})

                                                        if (data.responseJSON.errors.life_line !== undefined)
                                                            bootstrapNotify.error(data.responseJSON.errors.life_line, '', {"closeButton": true})

                                                        if (data.responseJSON.errors.max_user_count !== undefined)
                                                            bootstrapNotify.error(data.responseJSON.errors.max_user_count, '', {"closeButton": true})
                                                    }

                                                }
                                            });

                                        }
                                    }).on('form:submit', function () {
                                    return false;
                                });
                            });
                        }
                    });
                });

                detailPopUp.on('hidden.bs.modal', function () {
                    $(this).remove();
                });
            }

            function packageStatusUpdate(id) {

                $('#preloader').show();
                global.mySiteAjax({
                    url: route('admin.package-status-update'),
                    type: 'post',
                    data:  {
                        id : id,
                        _token : _token
                    },
                    loadSpinner: true,
                    success: function (response) {
                        $('#preloader').hide();
                        if (response.status == 'validation-error') {

                        } else if (response.status == 'success') {
                            bootstrapNotify.success(response.html, '');
                            dataTable.draw();
                        } else if (response.status == 'error') {
                            bootstrapNotify.error(response.html, '');
                        }
                    }
                });
            }

        });



        $(document).on('keyup', '.yearly_discount_percentage', function () {

            var subscription_fee = (document.getElementById('subscription_fee').value);
            var yearly_discount_percentage = (document.getElementById('yearly_discount_percentage').value);
            var yearly_discount_price = 0;

            yearly_discount_price = parseInt((subscription_fee * yearly_discount_percentage) / 100).toFixed(3);
            console.log(yearly_discount_price);
            var new_subscription_fee = parseInt((subscription_fee - yearly_discount_price)).toFixed(3);

            document.getElementById('yearly_discount_price').value = new_subscription_fee;

        })

    })(window, jQuery, _token);
});

})(PackageObj);

