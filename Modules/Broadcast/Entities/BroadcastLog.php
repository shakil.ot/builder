<?php

namespace Modules\Broadcast\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Campaign\Entities\CampaignSetting;

class BroadcastLog extends Model
{

    const BROADCAST_TYPE_SINGLE = 1;
    const BROADCAST_TYPE_BULK = 2;

    const SENT_TYPE_SCHEDULE = 1;
    const SENT_TYPE_INSTANT = 2;

    const DELIVERED = 1;
    const UNDELIVERED = 2;
    const FAILED = 3;
    const RECEIVED = 4;

    const CONTENT_TYPE_EMAIL = 1;

    const EMAIL_STATUS_LOG_CREATED = 0;
    const EMAIL_STATUS_SENT = 1;
    const EMAIL_STATUS_OPENED = 2;
    const EMAIL_STATUS_CLICKED = 3;




    protected $fillable = [
        'user_id',
        'broadcast_id',
        'auto_followup_id',
        'tracking_id',
        'contact_id',
        'to_email',
        'content_type',
        'send_date',
        'subject',
        'message_body',
        'error_code',
        'gateway_response',
        'email_status',
        'status'
    ];


}
