<?php

namespace Modules\Broadcast\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Modules\UserDashboard\Entities\SingleSendStatReport;

class Broadcast extends Model
{
    const STATUS_PENDING = 0;
    const STATUS_PROCESSING = 1;
    const STATUS_SUCCESS = 2;
    const STATUS_FAILED = 3;

    protected $fillable = [
        'user_id',
        'email_subject',
        'email_body',
        'send_to',
        'status',
        'send_date'
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getStatCount()
    {
        return $this->hasMany(SingleSendStatReport::class)
            ->selectRaw('broadcast_id, sum(email_sent) as total_email_sent')
            ->selectRaw('broadcast_id, sum(email_opened) as total_email_opened')
            ->selectRaw('broadcast_id, sum(email_clicked) as total_email_clicked')
            ->groupBy('broadcast_id');
    }
}
