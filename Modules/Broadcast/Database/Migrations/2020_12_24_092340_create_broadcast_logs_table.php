<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBroadcastLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('broadcast_logs', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('broadcast_id')->nullable();
            $table->foreign('broadcast_id')->references('id')->on('broadcasts');

            $table->unsignedBigInteger('auto_followup_id')->nullable();
            $table->foreign('auto_followup_id')->references('id')->on('auto_followups');

            $table->string('tracking_id');

            $table->unsignedBigInteger('user_id')->comment('user as client');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->unsignedBigInteger('contact_id');
            $table->string('to_email')->index();

            $table->tinyInteger('content_type')->index()->comment('1=Email');
            $table->tinyInteger('send_type')->default(2)->comment('1=schedule, 2=instant');
            $table->text('subject')->nullable();
            $table->text('message_body')->nullable();
            $table->string('error_code')->nullable();
            $table->longText('gateway_response')->nullable();
            $table->dateTime('send_date');
            $table->tinyInteger('email_status')->default(0)->comment('1=sent | 2=opened | 3 = clicked');
            $table->tinyInteger('status')->index()->comment('1=delivered, 2=undelivered, 3=failed, 4=receive')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('broadcast_logs');
    }
}
