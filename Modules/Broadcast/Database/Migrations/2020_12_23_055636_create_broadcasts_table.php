<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBroadcastsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('broadcasts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('email_subject');
            $table->text('email_body');
            $table->tinyInteger('send_to')->default(0)->comment('0=Subscribers |1=Leads | 2=Clients');
            $table->tinyInteger('status')->index()->comment('0=pending, 1=processing, 2=success,3=failed, 4=paused, 5=resumed, 6=stopped')->default(0);
            $table->dateTime('send_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('broadcasts');
    }
}
