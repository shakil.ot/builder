<?php

namespace Modules\Broadcast\Contracts\Service;

interface BroadcastContact
{
    public function createBroadcast($request);

    public function listBroadcast($request);

    public function listBroadcastDetails($request);

    public function getBroadcastById($broadcastId, $select = '*');

    public function listReport($userId);

    public function getBroadcastDetailsById($broadcastId);

    public function deleteReportById($request);

    public function listBroadcastReport($userId);

    public function autoFollowupQueList($request , $userId);

    public function getFollowupMessageById($followupId);
}
