<?php

namespace Modules\Broadcast\Contracts\Repositories;

interface BroadcastRepository
{
    public function createBroadcastByParam($data);

    public function getBroadcastsByUserIdDatatable($userId);

    public function getBroadcastById($broadcastId, $select = '*');

    public function getPending();

    public function updateBroadcastByCondition($where, $update);
}
