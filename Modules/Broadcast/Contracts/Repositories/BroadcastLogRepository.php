<?php

namespace Modules\Broadcast\Contracts\Repositories;


interface BroadcastLogRepository
{


    public function broadcastLogCreate($array);

    public function getDataByTrackingId($id);

    public function updateBroadcastLogEmailStatus($where, $update);

    public function getBroadcastDetailsByBroadcastIdDatatable($broadcastId);

    public function getDetailsById($broadcastLogId, $select = '*');

    public function deleteByWhere($where);

    public function listReport($userId);

    public function getBroadcastDetailsById($broadcastId);

    public function listBroadcastReport($where);

    public function getTotalDataByContactId($userId, $contactId, $paginate);

    public function getAutoFollowupDataByContactId($userId, $contactId, $paginate);

    public function getBroadcastDataByContactId($userId, $contactId, $paginate);

    public function deleteSingleConversationByConversationId($conversationId);


}

