<?php

namespace Modules\Broadcast\Http\Services;

use App\Helper\UtilityHelper;
use App\Services\SendgridMailService;
use App\Services\Utility;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Modules\Broadcast\Contracts\Repositories\BroadcastLogRepository;
use Modules\Broadcast\Contracts\Repositories\BroadcastRepository;
use Modules\Broadcast\Contracts\Service\BroadcastContact;
use Modules\Broadcast\Entities\Broadcast;
use Modules\Broadcast\Entities\BroadcastLog;
use Modules\Contact\Contracts\Repositories\ContactRepository;
use Modules\Contact\Entities\Contact;
use Modules\EmailSetting\Contracts\Repositories\EmailSettingRepository;
use Modules\Package\Contracts\Service\UserBalanceContact;
use Modules\UserDashboard\Contracts\Repositories\AutoFollowupQueueRepository;
use Modules\UserDashboard\Contracts\Repositories\AutoFollowupRepository;
use Modules\WebBuilder\Contracts\Repositories\WebBuilderRepository;
use Modules\WebBuilder\Entities\WebBuilder;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Str;

class BroadcastService implements BroadcastContact
{
    /**
     * @var BroadcastRepository
     */
    private $broadcastRepository;
    /**
     * @var ContactRepository
     */
    private $contactRepository;
    /**
     * @var BroadcastLogRepository
     */
    private $broadcastLogRepository;
    /**
     * @var UserBalanceContact
     */
    private $userBalanceService;
    /**
     * @var WebBuilderRepository
     */
    private $webBuilderRepository;

    private $autoFollowupQueueRepository;
    private $autoFollowupRepository;

    public function __construct(BroadcastRepository $broadcastRepository,
                                ContactRepository $contactRepository,
                                BroadcastLogRepository $broadcastLogRepository,
                                UserBalanceContact $userBalanceService,
                                AutoFollowupQueueRepository $autoFollowupQueueRepository,
                                WebBuilderRepository $webBuilderRepository,
                                AutoFollowupRepository $autoFollowupRepository
    )
    {
        $this->broadcastRepository = $broadcastRepository;
        $this->contactRepository = $contactRepository;
        $this->broadcastLogRepository = $broadcastLogRepository;
        $this->userBalanceService = $userBalanceService;
        $this->webBuilderRepository = $webBuilderRepository;
        $this->autoFollowupQueueRepository = $autoFollowupQueueRepository;
        $this->autoFollowupRepository = $autoFollowupRepository;
    }

    public function createBroadcast($request)
    {
        if ($request->get('subject') == null || $request->get('subject') == '') {
            return UtilityHelper::RETURN_STATUS_FORMATE(false, 'Email subject is required!');
        }
        if ($request->get('send_to') == null || $request->get('send_to') == '') {
            return UtilityHelper::RETURN_STATUS_FORMATE(false, 'Send datetime is required!');
        }
        if ($request->get('schedule_time') == null || $request->get('schedule_time') == '') {
            return UtilityHelper::RETURN_STATUS_FORMATE(false, 'Schedule time is required!');
        }
        if ($request->get('body') == null || $request->get('body') == '') {
            return UtilityHelper::RETURN_STATUS_FORMATE(false, 'Email body is required!');
        }
        $data = [
            'user_id' => $request->get('userId'),
            'email_subject' => $request->get('subject'),
            'email_body' => $request->get('body'),
            'send_to' => $request->get('send_to'),
            'send_date' => $request->get('schedule_time'),
            'status' => Broadcast::STATUS_PENDING
        ];
        $response = $this->broadcastRepository->createBroadcastByParam($data);
        if ($response) {
            return UtilityHelper::RETURN_STATUS_FORMATE(true, 'Campaign created successfully');
        }
        return UtilityHelper::RETURN_STATUS_FORMATE(false, 'Broadcast creation failed!');

    }

    public function listBroadcast($request)
    {
        $broadcasts = $this->broadcastRepository->getBroadcastsByUserIdDatatable($request->get('userId'));

        return Datatables::of($broadcasts)
            ->addColumn('my_send_to', function ($broadcast) {
                if ($broadcast->send_to == Contact::SUBSCRIBER) {
                    return "Subscribers";
                } else if ($broadcast->send_to == Contact::LEAD) {
                    return "Leads";
                } else {
                    return "Clients";
                }
            })
            ->addColumn('email_body', function ($broadcasts) {
                if($broadcasts->email_body == null){
                    return "N/A";
                }

                return '<button class="btn btn-sm settings" style="background-color: #03385d; color: #ffffff; font-size: 11px;" data-value="viewEmail" data-id=" '.$broadcasts->id.'" data-value="viewReport">'.'<i class="fa fa-eye"></i> View Email'.'</span>';
//                return Str::limit($reports->autoFollowup->email_body, 15);
            })
            ->addColumn('my_status', function ($broadcast) {
                if ($broadcast->status == Broadcast::STATUS_PENDING) {
                    return "Pending";
                }
                else if ($broadcast->status == Broadcast::STATUS_PROCESSING) {
                    return "Processing";
                }
                else if ($broadcast->status == Broadcast::STATUS_SUCCESS) {
                    return "Success";
                }
                else {
                    return "Failed";
                }
            })
            ->addColumn('action', function ($broadcast) {
                return '<div class="dropdown dropup" data-dropdown-toggle="click" aria-expanded="true">
                        <a href="#" class="" data-toggle="dropdown" aria-expanded="false">
                            <button class="btn btn-sm m-btn dropdown-toggle btn-green" style="background-color: #03385d">Select an Action</button>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                           
                            <a href="javascript:void(0)" class="settings m-nav__link dropdown-item action" id="contactId" data-value="viewEmail"
                            data-id="' .$broadcast->id . '">

                            <span class="m-nav__link-text"><i class="fa fa-eye"></i> View </span>
                            </a>
                              <a href="' . route("broadcast.broadcast-details", [$broadcast->id]) . '" target="_blank" class="m-nav__link dropdown-item"
                            data-id="' .$broadcast->id . '">

                            <span class="m-nav__link-text"><i class="m-nav__link-icon flaticon-search"></i> Campaign Details </span>
                            </a>
                            <span class="m-nav__separator m-nav__separator--fit"></span>
                            <a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm d-block" style="margin: 0 20px !important;"> Close </a>

                        </div>

                    </div>';

            })
            ->addColumn('my_send_date', function ($broadcast) {
                return "Scheduled-" . $broadcast->send_date;
            })
            ->addColumn('email_sent', function ($broadcast) {
                if (isset($broadcast->getStatCount[0])) {
                    return $broadcast->getStatCount[0]->total_email_sent;
                }
                return "0";
            })
            ->addColumn('email_opened', function ($broadcast) {
                if (isset($broadcast->getStatCount[0])) {

                    return $broadcast->getStatCount[0]->total_email_opened;
                }
                return "0";
            })
            ->addColumn('email_clicked', function ($broadcast) {
                if (isset($broadcast->getStatCount[0])) {

                    return $broadcast->getStatCount[0]->total_email_clicked;
                }
                return "0";
            })
            ->rawColumns(['my_send_to', 'email_body', 'my_send_date', 'my_status', 'email_sent', 'email_opened', 'email_clicked', 'action'])
            ->make(true);
    }

    public function listBroadcastDetails($request)
    {
        $broadcastDetails = $this->broadcastLogRepository->getBroadcastDetailsByBroadcastIdDatatable($request->get('broadcastId'));

        return Datatables::of($broadcastDetails)
            ->addColumn('to_email', function ($detail) {
               return $detail->to_email;
            })
            ->addColumn('send_date', function ($detail) {
               return $detail->send_date;
            })
            ->addColumn('email_sent', function ($detail) {
               if($detail->email_status == BroadcastLog::EMAIL_STATUS_SENT || $detail->email_status == BroadcastLog::EMAIL_STATUS_OPENED || $detail->email_status == BroadcastLog::EMAIL_STATUS_CLICKED){
                   return "<i class='la la-check' style='color: green'></i>";
               }
                return "<i class='la la-close' style='color: red'></i>";
            })
            ->addColumn('email_opened', function ($detail) {
                if($detail->email_status == BroadcastLog::EMAIL_STATUS_OPENED || $detail->email_status == BroadcastLog::EMAIL_STATUS_CLICKED){
                    return "<i class='la la-check' style='color: green'></i>";
                }
                return "<i class='la la-close' style='color: red'></i>";
            })
            ->addColumn('email_clicked', function ($detail) {
                if($detail->email_status == BroadcastLog::EMAIL_STATUS_CLICKED){
                    return "<i class='la la-check' style='color: green'></i>";
                }
                return "<i class='la la-close' style='color: red'></i>";
            })
            ->rawColumns(['to_email', 'send_date', 'email_sent', 'email_opened', 'email_clicked'])
            ->make(true);
    }

    public function getBroadcastById($broadcastId, $select = '*')
    {
        return $this->broadcastRepository->getBroadcastById($broadcastId, $select);
    }


    public function broadcastProcess()
    {
        Log::info("Broadcast cron started");
        $pendingBroadcast = $this->broadcastRepository->getPending();

        if (!is_null($pendingBroadcast)) {

            foreach ($pendingBroadcast as $broadcast) {
                $this->broadcastRepository->updateBroadcastByCondition(['id' => $broadcast->id] , [ 'status' => Broadcast::STATUS_PROCESSING]);
                $this->broadcastScheduled($broadcast);
            }
        }
    }


    private function broadcastScheduled($broadcast)
    {

        $broadcastPaused = 0;
        $contactIdArray = [];

        $contactInfos = $this->contactRepository->getAllContactsByUserIdAndType($broadcast->user_id, $broadcast->send_to);

        $counter = 0;
        foreach ($contactInfos as $contactData) {
            $subject = $broadcast->email_subject;
            $to = $contactData->email;
            $contactStatus = $contactData->status;
            $from = env('FROM_EMAIL');
            $message = $broadcast->email_body;

            $trackingId = UtilityHelper::generateRandomString(10).strtotime("now");

            $userInfo = $broadcast['user'];

            $companyName = $userInfo->first_name;

            $companyInfo = App::make(EmailSettingRepository::class)->getEmailDataByUserId($userInfo->id);
            $from = isset($companyInfo->email) ? $companyInfo->email : "";

            /***## CONVERT PERSONALIZED MESSAGE ##***/
            $webbuilderOfferPageData = $this->webBuilderRepository->getTemplateByCondition(['user_id'=>$userInfo->id, 'type'=>WebBuilder::WEB_BUILDER_TYPE_OFFER]);

            $offerPageUrl = $url = env('APP_URL') . '/offer/' . $webbuilderOfferPageData->id . '/v';
            $message = \ViewHelper::personalizedReplace($message, $contactData, $userInfo, $offerPageUrl);
            $subject = \ViewHelper::personalizedReplace($subject, $contactData, $userInfo);

            $companyName = !is_null($companyInfo) ? !is_null($companyInfo->company_name) ? $companyInfo->company_name : '' : '';

            if (empty($companyName)){
                $companyName = $userInfo->first_name;
            }

            /*ADD FOOTER */
            if($companyInfo){

                if($companyInfo->address != null && !empty($companyInfo->address) && $companyInfo->country != null && !empty($companyInfo->country)){
                    $message .= Utility::mailHtml($companyInfo);
                }
            }

            /***## CHECK USER BALANCE ##***/
            $checkUserBalance = $this->userBalanceService->checkBalanceByUserIdAndType($userInfo->id);

            $status = BroadcastLog::FAILED;

            if ($checkUserBalance->remaining_no_of_email_send_per_month > 0 && $contactStatus == Contact::ACTIVE) {

                $result = App::make(SendgridMailService::class)->sendMailWithSendGrid($subject, $to, $from, $message, $companyName, $trackingId);

                $type = 'remaining_no_of_email_send_per_month';
                App::make(UserBalanceContact::class)->reduceBalanceByUserIdAndType($broadcast->user_id, $type, 1);

                if ($result) {
                    $status = BroadcastLog::DELIVERED;
                }

            } else {
                $result = 'Insufficient Balance';
                if($contactStatus != Contact::ACTIVE){
                    $result = 'Contact is blocked';
                }
            }

            $this->broadcastLogRepository->broadcastLogCreate([
                'broadcast_id' => $broadcast->id,
                'user_id' => $broadcast->user_id,
                'tracking_id' => $trackingId,
                'contact_id' => $contactData->id,
                'to_email' => $to,
                'content_type' => BroadcastLog::CONTENT_TYPE_EMAIL,
                'send_date' => $broadcast->send_date,
                'subject' => $subject,
                'message_body' => $message,
                'error_code' => null,
                'gateway_response' => json_encode($result),
                'status' => $status
            ]);

        }

        $this->broadcastRepository->updateBroadcastByCondition(['id' => $broadcast->id], ['status' => Broadcast::STATUS_SUCCESS]);

    }

    public function listReport($userId)
    {
        $where = [
          'user_id' => $userId,
        ];
        $reports = $this->broadcastLogRepository->listReport($where);

        return Datatables::of($reports)
            ->addColumn('to_email', function ($reports) {
                return $reports->to_email;
            })
            ->addColumn('send_date', function ($reports) {
                if($reports->send_date == null){
                    return "N/A";
                }
                return $reports->send_date;
            })
            ->addColumn('message_body', function ($reports) {
                if($reports->message_body == null){
                    return "N/A";
                }
//                $message = Str::limit($reports->message_body, 15);
                return '<button class="btn btn-sm settings viewMessage" style="background-color: #03385d; color: #ffffff; font-size: 11px;" data-id=" '.$reports->id.'" data-value="viewReport">'. '<i class="fa fa-eye"></i> View Email' .'</button>';
            })
            ->addColumn('status', function ($reports) {
                if($reports->status == BroadcastLog::DELIVERED){
                    return "Delivered";
                }
                elseif($reports->status == BroadcastLog::UNDELIVERED){
                    return "Undelivered";
                }
                elseif($reports->status == BroadcastLog::FAILED){
                    return "Failed";
                }
                elseif($reports->status == BroadcastLog::RECEIVED){
                    return "Received";
                }
                return "N/A";
            })
            ->addColumn('email_sent', function ($reports) {
                if($reports->email_status == BroadcastLog::EMAIL_STATUS_SENT || $reports->email_status == BroadcastLog::EMAIL_STATUS_OPENED || $reports->email_status == BroadcastLog::EMAIL_STATUS_CLICKED){
                    return "<i class='la la-check' style='color: green'></i>";
                }
                return "<i class='la la-close' style='color: red'></i>";
            })
            ->addColumn('email_opened', function ($reports) {
                if($reports->email_status == BroadcastLog::EMAIL_STATUS_OPENED || $reports->email_status == BroadcastLog::EMAIL_STATUS_CLICKED){
                    return "<i class='la la-check' style='color: green'></i>";
                }
                return "<i class='la la-close' style='color: red'></i>";
            })
            ->addColumn('email_clicked', function ($reports) {
                if($reports->email_status == BroadcastLog::EMAIL_STATUS_CLICKED){
                    return "<i class='la la-check' style='color: green'></i>";
                }
                return "<i class='la la-close' style='color: red'></i>";
            })
            ->addColumn('action', function ($reports) {
                return '<div class="dropdown dropup" data-dropdown-toggle="click" aria-expanded="true">
                        <a href="#" class="" data-toggle="dropdown" aria-expanded="false">
                            <button class="btn btn-sm m-btn dropdown-toggle btn-green" style="background-color: #03385d">Select an Action</button>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                           
                            <a href="javascript:void(0)" class="settings m-nav__link dropdown-item action" id="reportId" data-value="viewReport"
                            data-id="' .$reports->id . '">

                            <span class="m-nav__link-text"><i class="fa fa-eye"></i> View </span>
                            </a>
                            
                            <a href="javascript:void(0)" class="settings m-nav__link dropdown-item action" id="reportid" data-value="deleteReport"
                            data-id="' .$reports->id . '">

                            <span class="m-nav__link-text"><i class="fa fa-trash"></i> Delete </span>
                            </a>
                            <span class="m-nav__separator m-nav__separator--fit"></span>
                            <a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm d-block" style="margin: 0 20px !important;"> Close </a>

                        </div>

                    </div>';

            })

            ->rawColumns(['to_email', 'send_date', 'message_body', 'status', 'email_sent', 'email_opened', 'email_clicked', 'action'])
            ->make(true);
    }

    public function getBroadcastDetailsById($broadcastId)
    {
        $where = [
          'id' => $broadcastId
        ];
        return $this->broadcastLogRepository->getBroadcastDetailsById($where);
    }

    public function deleteReportById($request)
    {
        $broadcastId = $request->broadcastId;

        $where = [
            'id' => $broadcastId
        ];

        $result =  $this->broadcastLogRepository->deleteByWhere($where);

        if($result)
        {
            return [
                'html' => 'Record Deleted!',
                'status' => 'success'
            ];
        }

        else {
            return [
                'html' => "Failed! Please try again",
                'status' => 'error',
            ];
        }
    }

    public function listBroadcastReport($userId)
    {
        $where = [
            'user_id' => $userId,
        ];
        $reports = $this->broadcastLogRepository->listBroadcastReport($where);

        return Datatables::of($reports)
            ->addColumn('to_email', function ($reports) {
                return $reports->to_email;
            })
            ->addColumn('send_date', function ($reports) {
                if($reports->send_date == null){
                    return "N/A";
                }
                return $reports->send_date;
            })
            ->addColumn('message_body', function ($reports) {
                if($reports->message_body == null){
                    return "N/A";
                }
                $message = Str::limit($reports->message_body, 15);
                return '<button class="btn btn-sm settings viewMessage" style="background-color: #03385d; color: #ffffff; font-size: 11px;" data-id=" '.$reports->id.'" data-value="viewReport">'. '<i class="fa fa-eye"></i> View Email' .'</button>';
            })
            ->addColumn('status', function ($reports) {
                if($reports->status == BroadcastLog::DELIVERED){
                    return "Delivered";
                }
                elseif($reports->status == BroadcastLog::UNDELIVERED){
                    return "Undelivered";
                }
                elseif($reports->status == BroadcastLog::FAILED){
                    return "Failed";
                }
                elseif($reports->status == BroadcastLog::RECEIVED){
                    return "Received";
                }
                return "N/A";
            })
            ->addColumn('email_sent', function ($reports) {
                if($reports->email_status == BroadcastLog::EMAIL_STATUS_SENT || $reports->email_status == BroadcastLog::EMAIL_STATUS_OPENED || $reports->email_status == BroadcastLog::EMAIL_STATUS_CLICKED){
                    return "<i class='la la-check' style='color: green'></i>";
                }
                return "<i class='la la-close' style='color: red'></i>";
            })
            ->addColumn('email_opened', function ($reports) {
                if($reports->email_status == BroadcastLog::EMAIL_STATUS_OPENED || $reports->email_status == BroadcastLog::EMAIL_STATUS_CLICKED){
                    return "<i class='la la-check' style='color: green'></i>";
                }
                return "<i class='la la-close' style='color: red'></i>";
            })
            ->addColumn('email_clicked', function ($reports) {
                if($reports->email_status == BroadcastLog::EMAIL_STATUS_CLICKED){
                    return "<i class='la la-check' style='color: green'></i>";
                }
                return "<i class='la la-close' style='color: red'></i>";
            })
            ->addColumn('action', function ($reports) {
                return '<div class="dropdown dropup" data-dropdown-toggle="click" aria-expanded="true">
                        <a href="#" class="" data-toggle="dropdown" aria-expanded="false">
                            <button class="btn btn-sm m-btn dropdown-toggle btn-green" style="background-color: #03385d">Select an Action</button>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                           
                            <a href="javascript:void(0)" class="settings m-nav__link dropdown-item action" id="reportId" data-value="viewReport"
                            data-id="' .$reports->id . '">

                            <span class="m-nav__link-text"><i class="fa fa-eye"></i> View </span>
                            </a>
                            
                            <a href="javascript:void(0)" class="settings m-nav__link dropdown-item action" id="reportid" data-value="deleteReport"
                            data-id="' .$reports->id . '">

                            <span class="m-nav__link-text"><i class="fa fa-trash"></i> Delete </span>
                            </a>
                            <span class="m-nav__separator m-nav__separator--fit"></span>
                            <a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm d-block" style="margin: 0 20px !important;"> Close </a>

                        </div>

                    </div>';

            })

            ->rawColumns(['to_email', 'send_date', 'message_body', 'status', 'email_sent', 'email_opened', 'email_clicked', 'action'])
            ->make(true);
    }


    public function autoFollowupQueList($request, $userId)
    {
        $reports = $this->autoFollowupQueueRepository->getAllFollowupByUserId($userId);

        return Datatables::of($reports)
            ->addColumn('to_email', function ($reports) {
                return $reports->contact->email;
            })
            ->addColumn('send_date', function ($reports) {
                if($reports->schedule_time == null){
                    return "N/A";
                }
                return $reports->schedule_time;
            })
            ->addColumn('message_body', function ($reports) {
                if($reports->autoFollowup->email_body == null){
                    return "N/A";
                }

//                $message = Str::limit($reports->autoFollowup->email_body, 15);
                return '<button class="btn btn-sm settings viewMessage" style="background-color: #03385d; color: #ffffff; font-size: 11px;" data-id=" '.$reports->auto_followup_id.'" data-value="viewReport">'.'<i class="fa fa-eye"></i> View Email'.'</span>';
//                return Str::limit($reports->autoFollowup->email_body, 15);
            })
            ->addColumn('day', function ($reports) {
                return $reports->day;
            })

            ->addColumn('action', function ($reports) {
                return '<div class="dropdown dropup" data-dropdown-toggle="click" aria-expanded="true">
                        <a href="#" class="" data-toggle="dropdown" aria-expanded="false">
                            <button class="btn btn-sm m-btn dropdown-toggle btn-green" style="background-color: #03385d">Select an Action</button>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                           
                            <a href="/timeline/' . $reports->contact->id . '" 
                            class="settings m-nav__link dropdown-item" 
                            id="reportId" data-value="viewReport"
                            >

                            <span class="m-nav__link-text"><i class="fa fa-eye"></i> Contact Details </span>
                            </a>
                            
                            <a href="javascript:void(0)" class="m-nav__link dropdown-item delete-followup-not-add" 
                            next_send="' . false . '"
                             data-contact-id="' .$reports->contact->id . '"
                            data-id="' . $reports->autoFollowup->id . '"  data-queue-id="' .$reports->id . '"   data-day="' . $reports->day . '"
                                >

                            <span class="m-nav__link-text"><i class="fa fa-trash"></i> Delete from Auto followup campaign </span>
                            </a>
                            
                             <a href="javascript:void(0)" class="m-nav__link dropdown-item delete-followup"                
                            next_send="' . true . '"
                            data-contact-id="' .$reports->contact->id . '"
                            data-id="' .$reports->autoFollowup->id . '"  data-queue-id="' .$reports->id . '"   data-day="' .$reports->day . '" >

                            <span class="m-nav__link-text"><i class="fa fa-trash"></i> Delete This message </span>
                            </a>
                            <span class="m-nav__separator m-nav__separator--fit"></span>
                            <a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm d-block" style="margin: 0 20px !important;"> Close </a>

                        </div>

                    </div>';

            })

            ->rawColumns(['to_email', 'send_date', 'message_body', 'status', 'day', 'action'])
            ->make(true);
    }

    public function getFollowupMessageById($followupId)
    {

        return $this->autoFollowupRepository->getFollowupById($followupId, ['email_subject', 'email_body']);

    }
}
