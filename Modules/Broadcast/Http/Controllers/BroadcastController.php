<?php

namespace Modules\Broadcast\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\AdminEmailTemplate\Contracts\Service\EmailTemplateContact;
use Modules\Broadcast\Contracts\Service\BroadcastContact;
use Response;

class BroadcastController extends Controller
{
    /**
     * @var EmailTemplateContact
     */
    private $emailTemplateService;
    /**
     * @var BroadcastContact
     */
    private $broadcastService;

    public function __construct(EmailTemplateContact $emailTemplateService,
                                BroadcastContact $broadcastService)
    {
        $this->emailTemplateService = $emailTemplateService;
        $this->broadcastService = $broadcastService;
    }

    public function index()
    {
        return view('broadcast::index');
    }

    public function broadcastCreateForm(Request $request)
    {
        if (!$request->ajax()) {
            return redirect()->route('Dashboard');
        }
        $emailTemplates = $this->emailTemplateService->getAllEmailTemplate();

        return Response::json([
            'html' => view('broadcast::create-form')
                ->with([
                    'emailTemplates' => $emailTemplates
                ])->render(),
            'status' => 'success'
        ]);
    }

    public function createBroadcast(Request $request)
    {
        $request['userId'] = auth()->user()->id;
        return response()->json($this->broadcastService->createBroadcast($request));
    }

    public function broadcastList(Request $request)
    {
        if (!$request->ajax()) {
            return redirect()->route('Dashboard');
        }
        $request['userId'] = auth()->user()->id;

        return $this->broadcastService->listBroadcast($request);

    }

    public function getEmailViewForm(Request $request)
    {
        $broadcastId = $request->get('id');
        $broadcastDetails = $this->broadcastService->getBroadcastById($broadcastId, ['email_subject', 'email_body']);
        return Response::json([
            'html' => view('broadcast::view-email')
                ->with([
                    'broadcastDetails' => $broadcastDetails
                ])->render(),
            'status' => 'success'
        ]);
    }

    public function broadcastDetailsPage($broadcastId)
    {
        return view('broadcast::broadcast-details.index')->with([
            'broadcastId' => $broadcastId
        ]);
    }
    public function broadcastDetailsList(Request $request)
    {
        if (!$request->ajax()) {
            return redirect()->route('Dashboard');
        }
        return $this->broadcastService->listBroadcastDetails($request);
    }

    public function showReport()
    {
        return view('broadcast::report');
    }

    public function reportList(Request $request)
    {
        if (!$request->ajax()) {
            return redirect()->route('Dashboard');
        }

        return $this->broadcastService->listReport(auth()->id());
    }

    public function getReportViewForm(Request $request)
    {
        $broadcastId = $request->get('id');
        $reportDetails = $this->broadcastService->getBroadcastDetailsById($broadcastId);

        return Response::json([
            'html' => view('broadcast::view-report')
                ->with([
                    'reportDetails' => $reportDetails
                ])->render(),
            'status' => 'success'
        ]);
    }

    public function deleteReport(Request $request)
    {
        return $this->broadcastService->deleteReportById($request);
    }

    public function showBroadcastReport()
    {
        return view('broadcast::report-broadcast');
    }

    public function listBroadcasts(Request $request)
    {
        if (!$request->ajax()) {
            return redirect()->route('Dashboard');
        }

        return $this->broadcastService->listBroadcastReport(auth()->id());
    }

    public function autoFollowupQue()
    {
        return view('broadcast::auto-followup-que-message');
    }

    public function autoFollowUpQueued(Request $request)
    {
        if (!$request->ajax()) {
            return redirect()->route('Dashboard');
        }

        return $this->broadcastService->autoFollowupQueList($request, auth()->id());
    }

    public function getAutoFollowupViewForm(Request $request)
    {
        if (!$request->ajax()) {
            return redirect()->route('Dashboard');
        }

        $followupId = $request->get('id');
        $reportDetails = $this->broadcastService->getFollowupMessageById($followupId);

        return Response::json([
            'html' => view('broadcast::autofollowup-report')
                ->with([
                    'reportDetails' => $reportDetails
                ])->render(),
            'status' => 'success'
        ]);
    }


}
