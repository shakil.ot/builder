<?php

namespace Modules\Broadcast\Http\Repositories;

use App\Repositories\BaseRepository\BaseRepository;
use Illuminate\Support\Carbon;
use Modules\Broadcast\Contracts\Repositories\BroadcastRepository;
use Modules\Broadcast\Entities\Broadcast;

class BroadcastRepositoryEloquent extends BaseRepository implements BroadcastRepository
{

    protected function model()
    {
        return new Broadcast();
    }

    public function createBroadcastByParam($data)
    {
        return $this->create($data);
    }

    public function getBroadcastsByUserIdDatatable($userId)
    {
        return $this->model()
            ->with(['getStatCount'])
            ->where('user_id', $userId)
            ->orderBy('updated_at', 'desc');
    }

    public function getBroadcastById($broadcastId, $select = '*')
    {
        return $this->model()
            ->where('id',$broadcastId)
            ->select($select)
            ->first();
    }

    public function getPending()
    {
        $dateStart = Carbon::now()
            ->subMinutes(100)
            ->format('Y-m-d H:i:s');

        $dateEnd = Carbon::now()
            ->addMinutes(100)
            ->format('Y-m-d H:i:s');


        $conversation = $this->model->with(['user'])->where(['status' => Broadcast::STATUS_PENDING]);
        $conversation->whereRaw('send_date >= ?', [$dateStart])
            ->whereRaw('send_date <= ?', [$dateEnd])
            ->select('email_subject','email_body','send_to', 'user_id', 'id', 'send_date');


        return $conversation->get();
    }

    public function updateBroadcastByCondition($where, $update)
    {
        return $this->model->where($where)->update($update);

    }
}
