<?php

namespace Modules\Broadcast\Http\Repositories;


use App\Repositories\BaseRepository\BaseRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Modules\Broadcast\Contracts\Repositories\BroadcastLogRepository;
use Modules\Broadcast\Entities\BroadcastLog;


class BroadcastLogRepositoryEloquent extends BaseRepository implements BroadcastLogRepository
{
    protected function model()
    {
        return new BroadcastLog();
    }


    public function broadcastLogCreate($array)
    {
        return $this->model->create($array);
    }

    public function getDataByTrackingId($trackingId)
    {
        return $this->model()
            ->where('tracking_id', $trackingId)
            ->first();
    }

    public function updateBroadcastLogEmailStatus($where, $update)
    {
        return $this->model()
            ->where($where)
            ->update($update);
    }

    public function getBroadcastDetailsByBroadcastIdDatatable($broadcastId)
    {
//        return $this->model
//            ->leftjoin('single_send_stat_reports', 'single_send_stat_reports.broadcast_id', '=', 'broadcast_logs.broadcast_id')
//            ->where('broadcast_logs.broadcast_id', $broadcastId)
//            ->select('broadcast_logs.to_email as to_email','broadcast_logs.send_date as send_date', 'single_send_stat_reports.email_sent as email_sent', 'single_send_stat_reports.email_opened as email_opened', 'single_send_stat_reports.email_clicked as email_clicked')
//            ->orderBy('broadcast_logs.id', 'desc');

        return $this->model
            ->where('broadcast_id', $broadcastId)
            ->select('to_email', 'send_date', 'email_status')
            ->orderBy('updated_at', 'desc');
    }

    public function getDetailsById($broadcastLogId, $select = '*')
    {
        return $this->model()
            ->where('id', $broadcastLogId)
            ->select($select)
            ->first();
    }

    public function deleteByWhere($where)
    {
        return $this->model()
            ->where($where)
            ->delete();
    }


    public function listReport($where)
    {
        return $this->model()
            ->where($where)
            ->whereNull('broadcast_id')
            ->whereNotNull('auto_followup_id')
            ->orderBy('created_at', 'desc');
    }

    public function getBroadcastDetailsById($where)
    {
        return $this->model()
            ->where($where)
            ->first();
    }

    public function listBroadcastReport($where)
    {
        return $this->model()
            ->where($where)
            ->whereNull('auto_followup_id')
            ->whereNotNull('broadcast_id')
            ->orderBy('created_at', 'desc');
    }


    public function getTotalDataByContactId($userId, $contactId, $paginate)
    {
        $broadcastLogData = $this->model()
            ->where(['user_id' => $userId, 'contact_id' => $contactId])
            ->orderBy('id', 'desc');
        return ($paginate) ? $broadcastLogData->paginate(10) : $broadcastLogData->get();
    }

    public function getAutoFollowupDataByContactId($userId, $contactId, $paginate)
    {
        $broadcastLogData = $this->model()
            ->where(['user_id' => $userId, 'contact_id' => $contactId])
            ->where('auto_followup_id', '!=', null)
            ->orderBy('id', 'desc');
        return ($paginate) ? $broadcastLogData->paginate(10) : $broadcastLogData->get();
    }

    public function getBroadcastDataByContactId($userId, $contactId, $paginate)
    {
        $broadcastLogData = $this->model()
            ->where(['user_id' => $userId, 'contact_id' => $contactId])
            ->where('broadcast_id', '!=', null)
            ->orderBy('id', 'desc');
        return ($paginate) ? $broadcastLogData->paginate(10) : $broadcastLogData->get();
    }

    public function deleteSingleConversationByConversationId($conversationId)
    {
        $response = BroadcastLog::where([
            'id' => $conversationId
        ])->delete();
        if ($response) {
            return [
                'html' => 'History Deleted',
                'status' => 'success'
            ];
        }
    }
}
