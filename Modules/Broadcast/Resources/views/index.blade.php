@extends('user.layout.app', ['menu' => 'broadcasts'])
@section('title')
    Campaigns
@endsection
@section('css')
    <style>
        .modal-body {
            padding-top: 0px;
        }
        .modal .modal-header .close span {
            display: block;
        }
    </style>
@endsection
@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="d-flex flex-column-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-custom card-stretch gutter-b">
                            <!--begin::Header-->

                            <!--end::Header-->
                            <!--begin::Body-->
                            <div class="card-body pt-0 pb-3">
                                <!--begin::Subheader-->
                                <div class="py-2 py-lg-4 subheader-solid" id="kt_subheader"
                                     style=" margin-bottom: -50px;color: #6c7293; margin-left: -20px; margin-right: -30px; height: 54px;top: 65px;left: 0;right: 0; background-color: #ffffff;border-top: 1px solid #ECF0F3;">
                                    <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                                        <!--begin::Info-->
                                        <div class="d-flex align-items-center flex-wrap mr-1">
                                            <!--begin::Page Heading-->
                                            <div class="d-flex align-items-baseline mr-5">
                                                <!--begin::Page Title-->
                                                <i class="icon-xl fas fa-bullhorn" style="color: #023B64;"></i>
                                                <h3 class="text-dark font-weight-bold my-2 ml-2 mr-5" style="color: #464E5F">Campaigns</h3>
                                                <!--end::Page Title-->
                                            </div>
                                            <!--end::Page Heading-->
                                        </div>
                                        <!--end::Info-->
                                        <!--begin::Toolbar-->
                                        <div class="d-flex align-items-center">
                                            <!--begin::Actions-->
                                            <ul class="nav nav-pills nav-pills--brand m-nav-pills--align-right m-nav-pills--btn-pill m-nav-pills--btn-sm" role="tablist">
                                                <li class="nav-item m-tabs__item">  </li>
                                            </ul>
                                            <!--end::Actions-->
                                        </div>
                                        <!--end::Toolbar-->
                                    </div>
                                </div>
                                <!--end::Subheader-->
                                <div style="margin:0 auto">
                                    <div class="pane-section-content open">
                                        <div class="m-subheader">
                                            <input type="hidden" id="contactId" value="1">
                                        </div>

                                        <!-- END: Subheader -->
                                        <div class="m-content">
                                            <div class="row">
                                                <div class="col-xl-12 margin-sm-dev">
                                                    <div class="m-portlet alterC">
                                                        <button
                                                            class="btn btn-sm btn-primary btn-green submit-file float-right createBroadcast"
                                                            style="margin-bottom: 15px; margin-top: 10px;"
                                                            id="createBroadcastBtn">New Campaign
                                                        </button>
                                                        <div class="m-portlet__body list_view_map_view">
                                                            <div class="tab-content quick-sends">
                                                                <div class="tab-pane active show" id="" role="tabpanel">
                                                                    <div class="table-design-border-wrapper">
                                                                        <div
                                                                            class="m-portlet__body table-responsive table-design-border mcOverflowVisible">
                                                                            <table id="broadcast-table"
                                                                                   class="mt-1 table table-hover contact-dataTable">
                                                                                <colgroup>
                                                                                    <col span="1" style="width: 15%;">
                                                                                    <col span="1" style="width: 30%;">
                                                                                    <col span="1" style="width: 15%;">
                                                                                    <col span="1" style="width: 15%;">
                                                                                    <col span="1" style="width: 15%;">
                                                                                    <col span="1" style="width: 25%;">
                                                                                </colgroup>
                                                                                <thead>
                                                                                <tr>
                                                                                    <th><span>Email Subject</span></th>
                                                                                    <th><span>Email Body</span></th>
                                                                                    <th><span>Sent To</span></th>
                                                                                    <th><span>Schedule</span></th>
                                                                                    <th><span>Status</span></th>
                                                                                    <th><span>Sent</span></th>
                                                                                    <th><span>Opened</span></th>
                                                                                    <th><span>Clicked</span></th>
                                                                                    <th><span>Action</span></th>
                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!--end::Body-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

    <script>
        $(function () {

            let _token = document.head.querySelector("[property=csrf-token]").content;
            let contactId = $('#contactId').val();
            $.extend(true, $.fn.dataTable.defaults, {
                "language": {
                    "search": "",
                    "searchPlaceholder": "Search",
                    "sLengthMenu": "_MENU_ records",
                    "paginate": {
                        "first": "<<",
                        "previous": '<span class="fa fa-chevron-left"></span>',
                        "next": '<span class="fa fa-chevron-right"></span>',
                        "last": ">>"
                    },
                }
            });

            (function (global, $, _token) {

                var dataTable = $('#broadcast-table').DataTable({
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    stateSave: false,
                    "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                    pageLength: 10,

                    ajax: {
                        url: route('broadcast.broadcast-list'),
                        type: 'get',
                    },
                    columns: [
                        {data: 'email_subject', name: 'email_subject', "orderable": true, "searchable": true, width: "30%"},
                        {data: 'email_body', name: 'email_body', "orderable": true, "searchable": true, width: "40%"},
                        {data: 'my_send_to', name: 'send_to', "orderable": true, "searchable": false, width: "25%"},
                        {data: 'my_send_date', name: 'send_date', "orderable": true, "searchable": false, width: "25%"},
                        {data: 'my_status', name: 'status', "orderable": true, "searchable": false, width: "30%"},
                        {data: 'email_sent', name: 'email_sent', "orderable": true, "searchable": false, width: "20%"},
                        {data: 'email_opened', name: 'email_opened', "orderable": true, "searchable": false, width: "20%"},
                        {data: 'email_clicked', name: 'email_clicked', "orderable": true, "searchable": false, width: "20%"},
                        {data: 'action', name: 'action', "orderable": true, "searchable": false, width: "40%"}
                    ]
                });

                $(document).on('click', '.createBroadcast', function (e) {
                    e.preventDefault();
                    e.stopImmediatePropagation();
                    $('#preloader').show();
                    $('.preloader-text').html('Loading!!');
                    let detailPopUp = global.customPopup.show({
                        header: 'Create New Email Campaign',
                        message: '',
                        dialogSize: 'lg editContactModalAlterC',
                        dialogClass: 'prevent-to-show'
                    });

                    detailPopUp.on('shown.bs.modal', function () {
                        let modal = $(this);
                        modal.find('.modal-body').html();
                        $('#preloader').show();

                        global.mySiteAjax({
                            url: route('broadcast.create-form'),
                            type: 'post',
                            data: {"_token": _token},
                            loadSpinner: true,
                            success: function (response) {
                                window.nbUtility.ajaxErrorHandling(response, function () {
                                    modal.find('.modal-body').html(response.html);
                                    $("#custom-modal").css('overflow-y', 'auto');
                                    $('#preloader').hide();

                                    $('#kt_datetimepicker_3_modal').datetimepicker({
                                        todayHighlight: true,
                                        autoclose: true,
                                        pickerPosition: 'top-right',
                                        // todayBtn: true,
                                        format: 'yyyy/mm/dd hh:ii'
                                    });
                                    $('#create-broadcast-form').on('submit', function (e) {
                                        e.preventDefault();

                                        Swal.fire({
                                            title: 'Are you sure?',
                                            html: "<b class='text-danger'> Once you click create campaign you won't be able to cancel or edit this email and it will be sent at your chosen schedule with the setting you chose. Please review everything and Watch the preview before you schedule your email.</b>",
                                            type: 'warning',
                                            confirmButtonText: 'Yes, create campaign',
                                            confirmButtonClass: 'btn btn-danger',
                                            showCancelButton: true,
                                            cancelButtonText: 'Review Again',
                                            cancelButtonClass: 'btn btn-green'
                                        }).then(function (result) {
                                            if (result.value) {
                                                $('#preloader').show();
                                                let request = {
                                                    'subject': $('#subject').val(),
                                                    'send_to': $('#send_to').val(),
                                                    'schedule_time': $('#kt_datetimepicker_3_modal').val(),
                                                    'body': $('#emailBody').summernote('code'),
                                                    _token: _token
                                                }
                                                global.mySiteAjax({
                                                    url: route('broadcast.create-broadcast'),
                                                    type: 'POST',
                                                    data: request,
                                                    contentType: false,
                                                    processData: false,
                                                    loadSpinner: true,
                                                    success: function (response) {
                                                        if (response.status === true) {
                                                            bootstrapNotify.success(response.html, 'success');
                                                            modal.modal('hide');
                                                            $("#broadcast-table").DataTable().ajax.reload();
                                                            $('#preloader').hide();

                                                        } else if (response.status == false) {
                                                            $('#preloader').hide();
                                                            bootstrapNotify.error(response.html, 'error');
                                                        }
                                                    }
                                                    , error: function () {
                                                        $('#preloader').hide();
                                                    }

                                                })
                                            } else {
                                                $('#preloader').hide();
                                            }
                                        });

                                    });


                                });
                            }
                        });
                        $('#preloader').hide();
                        global.spinnerDialog.hide();
                    });

                    detailPopUp.on('hidden.bs.modal', function () {
                        $(this).remove();
                        $('#preloader').hide();
                    });
                });

                $(document).on('click', '.settings', function () {

                    let broadcastId = $(this).attr('data-id');
                    let settingsType = $(this).attr('data-value');
                    if (!settingsType)
                        return false;

                    switch (settingsType) {

                        //Block Contact
                        case "viewEmail":
                            viewEmail(broadcastId);
                            break;

                    }

                    function viewEmail(broadcastId)
                    {
                            $('#preloader').show();
                            $('.preloader-text').html('Loading...');
                            let detailPopUp = global.customPopup.show({
                                header: 'Preview Email',
                                message: '',
                                dialogSize: 'lg editContactModalAlterC',
                                dialogClass: 'prevent-to-show'
                            });

                            detailPopUp.on('shown.bs.modal', function () {
                                let modal = $(this);
                                global.mySiteAjax({
                                    url: route('broadcast.email-view-form'),
                                    type: 'post',
                                    data: {'id': broadcastId, "_token": _token},
                                    loadSpinner: false,
                                    success: function (response) {
                                        window.nbUtility.ajaxErrorHandling(response, function () {
                                            modal.find('.modal-body').html(response.html);
                                            $('#preloader').hide();
                                            $("#custom-modal").css('overflow-y', 'auto');
                                            $('#viewEmailBody').summernote({
                                                height: 300,
                                            });
                                        });
                                    }
                                });
                                modal.removeClass('prevent-to-show');
                                $('#preloader').hide();
                                global.spinnerDialog.hide();
                            });

                            detailPopUp.on('hidden.bs.modal', function () {
                                $(this).remove();
                                $('#preloader').hide();
                            });
                    }

                });

                $(document).on('click','.email-template',function () {
                    let element = $(this);
                    let id = element.attr('data-id');
                    $.ajax({
                        url: route('autofollowup.get-email-template-by-id'),
                        type: "post",
                        data: {id:id, _token: _token},
                        success:function (response) {
                            $('#subject').val(response.subject);
                            $('#emailBody').summernote('code',response.body);

                        }
                    })
                });


            })(window, jQuery, _token);
        });

    </script>
@endsection

