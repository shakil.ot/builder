@extends('user.layout.app', ['menu' => 'autofollowup-schedule-report'])
@section('title')
    Auto follow up Queued
@endsection
@section('css')
@endsection
@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="d-flex flex-column-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-custom card-stretch gutter-b">
                            <!--begin::Header-->

                            <!--end::Header-->
                            <!--begin::Body-->
                            <div class="card-body pt-0 pb-3">
                                <!--begin::Subheader-->
                                <div class="py-2 py-lg-4 subheader-solid" id="kt_subheader"
                                     style=" margin-bottom: 24px;color: #6c7293; margin-left: -20px; margin-right: -30px; height: 54px;top: 65px;left: 0;right: 0; background-color: #ffffff;border-top: 1px solid #ECF0F3;">
                                    <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                                        <!--begin::Info-->
                                        <div class="d-flex align-items-center flex-wrap mr-1">
                                            <!--begin::Page Heading-->
                                            <div class="d-flex align-items-baseline mr-5">
                                                <!--begin::Page Title-->
                                                <i class="icon-xl fa fa-file" style="color: #023B64;"></i>
                                                <h3 class="text-dark font-weight-bold my-2 ml-2 mr-5" style="color: #464E5F">Auto Followup Schedule Message</h3>
                                                <!--end::Page Title-->
                                            </div>
                                            <!--end::Page Heading-->
                                        </div>
                                        <!--end::Info-->
                                        <!--begin::Toolbar-->
                                        <div class="d-flex align-items-center">
                                            <!--begin::Actions-->
                                            <ul class="nav nav-pills nav-pills--brand m-nav-pills--align-right m-nav-pills--btn-pill m-nav-pills--btn-sm" role="tablist">
                                                <li class="nav-item m-tabs__item">  </li>
                                            </ul>
                                            <!--end::Actions-->
                                        </div>
                                        <!--end::Toolbar-->
                                    </div>
                                </div>
                                <!--end::Subheader-->
                                <div style="margin:0 auto">
                                    <div class="pane-section-content open">
                                        <div class="m-subheader">
                                            <input type="hidden" id="contactId" value="1">
                                        </div>

                                        <!-- END: Subheader -->
                                        <div class="m-content">
                                            <div class="row">
                                                <div class="col-xl-12 margin-sm-dev">
                                                    <div class="m-portlet alterC">
                                                        <div class="m-portlet__body list_view_map_view">
                                                            <div class="tab-content quick-sends">
                                                                <div class="tab-pane active show" id="" role="tabpanel">
                                                                    <div class="table-design-border-wrapper">
                                                                        <div
                                                                                class="m-portlet__body table-responsive table-design-border mcOverflowVisible">
                                                                            <table id="report-table"
                                                                                   class="mt-1 table table-hover">
                                                                                <colgroup>
                                                                                    <col span="1" style="width: 15%;">
                                                                                    <col span="1" style="width: 15%;">
                                                                                    <col span="1" style="width: 15%;">
                                                                                    <col span="1" style="width: 10%;">
                                                                                    <col span="1" style="width: 10%;">
                                                                                </colgroup>
                                                                                <thead>
                                                                                <tr>
                                                                                    <th><span>Sent To</span></th>
                                                                                    <th><span>Schedule time</span></th>
                                                                                    <th><span>Email Body</span></th>
                                                                                    <th><span>Day</span></th>
                                                                                    <th><span>Action</span></th>
                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!--end::Body-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

    <script>
        $(function () {

            let _token = document.head.querySelector("[property=csrf-token]").content;
            let contactId = $('#contactId').val();
            $.extend(true, $.fn.dataTable.defaults, {
                "language": {
                    "search": "",
                    "searchPlaceholder": "Search",
                    "sLengthMenu": "_MENU_ records",
                    "paginate": {
                        "first": "<<",
                        "previous": '<span class="fa fa-chevron-left"></span>',
                        "next": '<span class="fa fa-chevron-right"></span>',
                        "last": ">>"
                    },
                }
            });

            (function (global, $, _token) {

                let dataTable = $('#report-table').DataTable({
                    processing: true,
                    // serverSide: true,
                    responsive: true,
                    stateSave: false,
                    "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                    pageLength: 10,

                    ajax: {
                        url: route('broadcast.auto-follow-up-queued'),
                        type: 'get',
                    },
                    columns: [
                        {data: 'to_email', name: 'to_email', "orderable": true, "searchable": true, width: "15%"},
                        {data: 'send_date', name: 'send_date', "orderable": true, "searchable": false, width: "15%"},
                        {data: 'message_body', name: 'message_body', "orderable": true, "searchable": false, width: "40%"},
                        {data: 'day', name: 'day', "orderable": true, "searchable": false, width: "15%"},
                        {data: 'action', name: 'email_status', "action": true, "searchable": false, width: "15%"},
                    ]
                });


                $(document).on('click', '.settings', function () {

                    let broadcastId = $(this).attr('data-id');
                    let settingsType = $(this).attr('data-value');
                    if (!settingsType)
                        return false;

                    switch (settingsType) {

                        case "viewReport":
                            viewReport(broadcastId);
                            break;

                        case "deleteReport":
                            deleteReport(broadcastId);
                            break;

                    }


                    // view message subject and body
                    function viewReport(autoFollowupId)
                    {
                        $('#preloader').show();
                        $('.preloader-text').html('Loading...');
                        let detailPopUp = global.customPopup.show({
                            header: 'Email Details',
                            message: '',
                            dialogSize: 'lg editContactModalAlterC',
                            dialogClass: 'prevent-to-show'
                        });

                        detailPopUp.on('shown.bs.modal', function () {
                            let modal = $(this);
                            global.mySiteAjax({
                                url: route('broadcast.auto-followup-view-form'),
                                type: 'post',
                                data: {'id': autoFollowupId, "_token": _token},
                                loadSpinner: false,
                                success: function (response) {
                                    window.nbUtility.ajaxErrorHandling(response, function () {
                                        modal.find('.modal-body').html(response.html);
                                        $('#preloader').hide();
                                        $("#custom-modal").css('overflow-y', 'auto');
                                        $('#viewEmailBody').summernote({
                                            height: 300,
                                        });
                                    });
                                }
                            });
                            modal.removeClass('prevent-to-show');
                            $('#preloader').hide();
                            global.spinnerDialog.hide();
                        });

                        detailPopUp.on('hidden.bs.modal', function () {
                            $(this).remove();
                            $('#preloader').hide();
                        });
                    }

                    // delete record
                    function deleteReport(broadcastId)
                    {
                        Swal.fire({
                            title: 'Are you sure?',
                            html: "<b class='text-danger'> Sure to Delete this report?</b>",
                            type: 'warning',
                            confirmButtonText: 'Yes, delete the report',
                            confirmButtonClass: 'btn btn-danger',
                            showCancelButton: true,
                            cancelButtonText: 'No, keep it!',
                            cancelButtonClass: 'btn btn-green'
                        }).then(function(result) {
                            if (result.value) {

                                global.mySiteAjax({
                                    url: route('broadcast.report-delete'),
                                    type: 'post',
                                    data: {broadcastId: broadcastId, _token: _token},
                                    loadSpinner: true,

                                    beforeSend: function () {
                                        $('#preloader').show();
                                    },
                                    complete: function (xhr, stat) {
                                        $('#preloader').hide();
                                    },
                                    success: function (response) {
                                        if (response.status == 'success') {
                                            toastr.success(response.html, "Success");
                                            $("#report-table").DataTable().ajax.reload();

                                        } else if (response.status == 'error') {
                                            toastr.error(response.html, "Error");
                                        }

                                    }
                                });
                            }

                            else {
                                $('#preloader').hide();
                            }
                        });
                    }




                });

                $(document).on('click', '.delete-followup', function () {
                    var that = this;
                    Swal.fire({
                        title: 'Are you sure to delete?',
                        html: "<b class='text-success'>Next Followup will be sent if available</b>",
                        type: 'warning',
                        confirmButtonText: 'Yes, delete it',
                        confirmButtonClass: 'btn btn-danger',
                        showCancelButton: true,
                        cancelButtonText: 'No, keep it!',
                        cancelButtonClass: 'btn btn-green'
                    }).then(function (result) {
                        if (result.value) {
                            var autoFollowupId = $(that).attr('data-id');
                            var autoFollowupQueueId = $(that).attr('data-queue-id');
                            var day = $(that).attr('data-day');
                            var next_send = $(that).attr('next_send');
                            var _contactId = $(that).attr('data-contact-id');

                            global.mySiteAjax({
                                url: route('next-followup-delete'),
                                type: 'post',
                                data: {
                                    autoFollowupId: autoFollowupId,
                                    autoFollowupQueueId: autoFollowupQueueId,
                                    day: day,
                                    next_send: next_send,
                                    contactId: _contactId,
                                    "_token": _token
                                },
                                loadSpinner: true,
                                beforeSend: function beforeSend() {
                                    $('#preloader').show();
                                },
                                complete: function complete(xhr, stat) {
                                    $('#preloader').hide();
                                },
                                success: function success(response) {
                                    if (response.status === 'success') {
                                        toastr.success(response.html, "success");
                                        location.reload();
                                        $('#preloader').hide();
                                    } else if (response.status === 'error') {
                                        $('#preloader').hide();
                                    }
                                }
                            });
                        } else {
                            $('#preloader').hide();
                        }
                    });
                });

                $(document).on('click', '.delete-followup-not-add', function () {
                    var that = this;
                    Swal.fire({
                        title: 'Are you sure to delete?',
                        html: "<b class='text-success'>Next Followup will be sent if available</b>",
                        type: 'warning',
                        confirmButtonText: 'Yes, delete it',
                        confirmButtonClass: 'btn btn-danger',
                        showCancelButton: true,
                        cancelButtonText: 'No, keep it!',
                        cancelButtonClass: 'btn btn-green'
                    }).then(function (result) {
                        if (result.value) {
                            var autoFollowupId = $(that).attr('data-id');
                            var autoFollowupQueueId = $(that).attr('data-queue-id');
                            var day = $(that).attr('data-day');
                            var _contactId = $(that).attr('data-contact-id');

                            global.mySiteAjax({
                                url: route('next-followup-delete'),
                                type: 'post',
                                data: {
                                    autoFollowupId: autoFollowupId,
                                    autoFollowupQueueId: autoFollowupQueueId,
                                    day: day,
                                    contactId: _contactId,
                                    "_token": _token
                                },
                                loadSpinner: true,
                                beforeSend: function beforeSend() {
                                    $('#preloader').show();
                                },
                                complete: function complete(xhr, stat) {
                                    $('#preloader').hide();
                                },
                                success: function success(response) {
                                    if (response.status === 'success') {
                                        toastr.success(response.html, "success");
                                        location.reload();
                                        $('#preloader').hide();
                                    } else if (response.status === 'error') {
                                        $('#preloader').hide();
                                    }
                                }
                            });
                        } else {
                            $('#preloader').hide();
                        }
                    });
                });

            })(window, jQuery, _token);
        });

    </script>
@endsection

