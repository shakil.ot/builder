@extends('user.layout.app', ['menu' => 'dashboard'])
@section('title')
    Campaign-Details
@endsection
@section('css')
    <style>
        .modal-body {
            padding-top: 0px;
        }
        .modal .modal-header .close span {
            display: block;
        }
    </style>
@endsection
@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="d-flex flex-column-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-custom card-stretch gutter-b">
                            <!--begin::Header-->

                            <!--end::Header-->
                            <!--begin::Body-->
                            <div class="card-body pt-0 pb-3" style="margin-top: 30px">
                                <div style="margin:0 auto">
                                    <div class="pane-section-content open">
                                        <div class="m-subheader">
                                            <input type="hidden" id="contactId" value="1">
                                        </div>

                                        <!-- END: Subheader -->
                                        <div class="m-content">
                                            <div class="row">
                                                <div class="col-xl-12 margin-sm-dev">
                                                    <div class="m-portlet alterC">
                                                        <div class="m-portlet__body list_view_map_view">
                                                            <div class="tab-content quick-sends">
                                                                <div class="tab-pane active show" id="" role="tabpanel">
                                                                    <div class="table-design-border-wrapper">
                                                                        <div
                                                                                class="m-portlet__body table-responsive table-design-border mcOverflowVisible">
                                                                            <table id="broadcast-details-table"
                                                                                   class="mt-1 table table-hover contact-dataTable">
                                                                                <colgroup>
                                                                                    <col span="1" style="width: 15%;">
                                                                                    <col span="1" style="width: 15%;">
                                                                                    <col span="1" style="width: 15%;">
                                                                                    <col span="1" style="width: 15%;">
                                                                                    <col span="1" style="width: 15%;">
                                                                                </colgroup>
                                                                                <thead>
                                                                                <tr>
                                                                                    <th><span>Contact email</span></th>
                                                                                    <th><span>Email Sent</span></th>
                                                                                    <th><span>Email Opened</span></th>
                                                                                    <th><span>Email Clicked</span></th>
                                                                                    <th><span>Sent date</span></th>
                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!--end::Body-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

    <script>
        $(function () {

            let _token = document.head.querySelector("[property=csrf-token]").content;
            let contactId = $('#contactId').val();
            $.extend(true, $.fn.dataTable.defaults, {
                "language": {
                    "search": "",
                    "searchPlaceholder": "Search",
                    "sLengthMenu": "_MENU_ records",
                    "paginate": {
                        "first": "<<",
                        "previous": '<span class="fa fa-chevron-left"></span>',
                        "next": '<span class="fa fa-chevron-right"></span>',
                        "last": ">>"
                    },
                }
            });

            (function (global, $, _token) {
                let broadcastId = "{{$broadcastId}}";

                var dataTable = $('#broadcast-details-table').DataTable({
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    stateSave: false,
                    "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                    pageLength: 10,

                    ajax: {
                        url: route('broadcast.broadcast-details-list'),
                        type: 'get',
                        data: function (data) {
                            data.broadcastId = broadcastId;
                        }
                    },
                    columns: [
                        {data: 'to_email', name: 'broadcast_logs.to_email', "orderable": true, "searchable": true, width: "30%"},
                        {data: 'email_sent', name: 'single_send_stat_reports.email_sent', "orderable": true, "searchable": false, width: "30%"},
                        {data: 'email_opened', name: 'single_send_stat_reports.email_opened', "orderable": true, "searchable": false, width: "30%"},
                        {data: 'email_clicked', name: 'single_send_stat_reports.email_clicked', "orderable": true, "searchable": false, width: "30%"},
                        {data: 'send_date', name: 'broadcast_logs.send_date', "orderable": true, "searchable": false, width: "30%"}
                    ]
                });


            })(window, jQuery, _token);
        });

    </script>
@endsection

