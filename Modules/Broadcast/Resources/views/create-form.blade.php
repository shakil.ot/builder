<style>
    .note-dropdown-menu{
        height: 450px;
        overflow-y: auto;
    }
</style>
<div class="card-body">
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="personal_info" role="tabpanel" aria-labelledby="kt_tab_pane_2">
            <div class="btn-group" role="group" aria-label="Button group with nested dropdown" style="float:right">
                <div class="btn-group" role="group">
                    <button id="btnGroupDrop1" type="button" class="btn dropdown-toggle" style="color: #ffffff;background-color: #03375D;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Select Email Template
                    </button>
                    <div class="dropdown-menu" style="">
                        @if(isset($emailTemplates[0]))
                            @foreach($emailTemplates as $emailTemplate)
                                <a class="dropdown-item email-template" data-id="{{$emailTemplate->id}}" href="javascript:void(0)" data-title="message-get">{{$emailTemplate->title}}</a>
                            @endforeach
                        @else
                            No templates available
                        @endif
                    </div>
                </div>

            </div>
            <form id="create-broadcast-form" autocomplete="off" data-parsley-validate>
                @csrf
                <button type="button" title="Personalize"
                        class="btn btn-secondary dropdown-toggle personalize-toggle" data-toggle="dropdown"
                        style="margin-left: -10px; color: #ffffff;background-color: #03375D;"
                        aria-haspopup="true" aria-expanded="true">Subject Personalize</button>
                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end"
                     style="height:400px; overflow-y:auto; position: absolute; transform: translate3d(-20px, 37px, 0px); top: 0px; left: 0px; will-change: transform;">
                    @foreach(\App\Models\PersonalizeType::$allType as $each => $key)
                        <button class="dropdown-item url-get-inbox-in-email" type="button"
                                data-title-email="message-get--email" data-url-email="{{$key}}">{{$each}}
                        </button>
                    @endforeach
                </div>
                <div class="form-group row w-100">
                    <label for="subject" class="col-form-label">Email Subject</label>
                    <input class="form-control" type="text" name="subject" id="subject"
                           placeholder="Enter Email Subject" required>
                    <div class="custom-error-message"></div>
                </div>
                <div class="form-group row w-100">
                            <label for="Type">Send To</label>
                            <select class="form-control type" id="send_to" name="send_to" autocomplete="nope">
                                <option value="{{\Modules\Contact\Entities\Contact::SUBSCRIBER}}">Subscribers</option>
                                <option value="{{\Modules\Contact\Entities\Contact::LEAD}}">Leads</option>
                                <option value="{{\Modules\Contact\Entities\Contact::CLIENT}}">Clients</option>
                            </select>
                </div>

                <div class="form-group row w-100">
                    <label for="Type">Schedule Time</label>
                    <div class="input-group date" data-z-index="1100">
                        <input type="text" class="form-control date" name="schedule_time" readonly="readonly" value="{{date("Y/m/d h:i")}}" id="kt_datetimepicker_3_modal" />
                        <div class="input-group-append">
                            <span class="input-group-text">
                                <i class="la la-calendar-plus-o glyphicon-th"></i>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group row w-100">
                    <label for="body" class="col-form-label">Email Body</label>
                    <textarea class="form-control body" name="body" id="emailBody" cols="30"
                              rows="10"></textarea>
                    <div class="custom-error-message"></div>
                </div>

                <div class="form-group mt-10 w-100">
                    <button type="submit" class="btn btn-green float-right font-weight-bold">
                                    <span class="svg-icon svg-icon-white">
                                        <svg xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                             viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                <path
                                                    d="M6,2 L18,2 C19.6568542,2 21,3.34314575 21,5 L21,19 C21,20.6568542 19.6568542,22 18,22 L6,22 C4.34314575,22 3,20.6568542 3,19 L3,5 C3,3.34314575 4.34314575,2 6,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z"
                                                    fill="#000000"></path>
                                            </g>
                                        </svg>
                                    </span>
                        Create Campaign
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        function sendFile(file, editor) {
            $('#preloader').show();
            var data;
            let _token = "{{csrf_token()}}";
            data = new FormData();
            data.append("file", file);
            data.append("_token", _token);
            $.ajax({
                data: data,
                type: "POST",
                url: "{{route('image-upload-from-summernote')}}",
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    $('#preloader').hide();
                    if (data.status == 'success') {
                        var image = $('<img>').attr('src', data.url);
                        editor.summernote("insertNode", image[0]);
                    } else {
                        bootstrapNotify.error(data.message,'error');
                    }

                }
            });
        }

        var editor_broadcast = function () {
            var elem = $('#emailBody');
            var demos = function () {
                $('#emailBody').summernote({
                    airMode: false,
                    height: 300,
                    focus: true,
                    dialogsInBody: true,
                    placeholder: "Enter Text Here... Type @ with a whitespace to get personalize tag",
                    maximumImageFileSize: 500 * 512, // 500 KB

                    callbacks: {
                        onInit: function () {
                            var editor = elem.next(),
                                placeholder = editor.find(".note-placeholder");

                            function isEditorEmpty() {
                                var code = elem.summernote("code");
                                return code === "<p><br></p>" || code === "";
                            }

                            editor.on("focusin focusout", ".note-editable", function (e) {
                                if (isEditorEmpty()) {
                                    placeholder[e.type === "focusout" ? "show" : "show"]();
                                }
                            });
                        },
                        onImageUpload: function (files) {
                            var editor = $(this);
                            sendFile(files[0], editor);
                        },
                        onImageUploadError: function(msg){
                            bootstrapNotify.error('FileSize too large !!!','error');
                        }
                    },
                    hint: {
                        mentions: ['first_name', 'last_name', 'email', 'number', 'my_first_name', 'my_last_name','my_email','my_number'],
                        match: /\B@(\w*)$/,
                        search: function (keyword, callback) {
                            callback($.grep(this.mentions, function (item) {
                                return item.indexOf(keyword) == 0;
                            }));
                        },
                        content: function (item) {
                            return '[' + '[' + item + ']' + ']';
                        }
                    },
                    toolbar: [
                        ['style', ['highlight', 'personalize', 'bold', 'italic', 'underline', 'clear']],
                        ['font', ['strikethrough', 'superscript', 'subscript']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['table', [
                            ['add', ['addRowDown', 'addRowUp', 'addColLeft', 'addColRight']],
                            ['delete', ['deleteRow', 'deleteCol', 'deleteTable']],
                        ]],
                        ['image', [
                            ['image', ['resizeFull', 'resizeHalf', 'resizeQuarter', 'resizeNone']],
                            ['float', ['floatLeft', 'floatRight', 'floatNone']],
                            ['remove', ['removeMedia']]
                        ]],
                        ['insert', ['link', 'picture','table']],
                        ['height', ['height']],
                        ['color', ['color']],
                        ['fontsize', ['fontsize']],
                        ['misc',['undo','redo']]
                    ],
                    buttons: {
                        personalize: personalizeDropDown
                    }
                });
            };
            return {
                // public functions
                init: function () {
                    demos();
                }
            };
        }();
        jQuery(document).ready(function () {
            editor_broadcast.init();
        });
        var emailControls = [
            ['FIRST_NAME', '[[first_name]]'],
            ['LAST_NAME', '[[last_name]]'],
            ['EMAIL', '[[email]]'],
            ['NUMBER', '[[number]]'],
            ['MY_FIRST_NAME', '[[my_first_name]]'],
            ['MY_LAST_NAME', '[[my_last_name]]'],
            ['MY_EMAIL', '[[my_email]]'],
            ['MY_NUMBER', '[[my_number]]'],
        ];
        var personalizeDropDown = function () {
            var ui = $.summernote.ui;
            var keywordMap = new Map(emailControls);
            var list = Array.from(keywordMap.keys());

            var button = ui.buttonGroup([
                ui.button({
                    contents: 'Personalize <i class="fa fa-caret-down" aria-hidden="true"></i>',
                    tooltip: 'Personalize Tag',
                    data: {
                        toggle: 'dropdown'
                    }
                }),
                ui.dropdown({
                    items: list,
                    callback: function (items) {
                        $(items).find('a.dropdown-item').click(function (e) {
                            var array_lenth = emailControls.length;
                            var data_value = $(this).attr('data-value');
                            var get_text = '';
                            for (var increment = 0; increment < array_lenth; increment++) {
                                var key = Array.from(keywordMap.keys())[increment];
                                if (key == data_value) {
                                    get_text = keywordMap.get(key);
                                }
                            }
                            $('#emailBody').summernote('editor.insertText', ' ' + get_text);
                            e.preventDefault();
                        })
                    }
                })
            ]);
            return button.render();
        }

        $(document).on('click', '.url-get-inbox-in-email', function (e) {
            e.preventDefault();
            var element = $(this);
            var areaId = element.data('title-email');
            var text_h = element.data('url-email');

            var text = $('#subject');
            text.val(text.val() + text_h);

        });

    });

</script>
