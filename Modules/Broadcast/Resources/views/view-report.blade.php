<div class="card-body" style="margin-top: -55px;">
    <div class="tab-content mt-5" id="myTabContent">
        <div class="tab-pane fade show active" id="personal_info" role="tabpanel" aria-labelledby="kt_tab_pane_2">

            <div class="form-group row w-100">
                <label for="subject" class="col-form-label">Email Subject</label>
                <input class="form-control" type="text" name="subject" id="subject"
                       placeholder="Enter Email Subject" value="{{$reportDetails->subject}}" readonly>
                <div class="custom-error-message"></div>
            </div>

            <div class="form-group row w-100">
                <label for="body" class="col-form-label">Email Body</label>
                <textarea class="form-control body" name="body" id="viewEmailBody" cols="30"
                          rows="10" readonly>{{$reportDetails->message_body}}</textarea>
                <div class="custom-error-message"></div>
            </div>
        </div>
    </div>
</div>
