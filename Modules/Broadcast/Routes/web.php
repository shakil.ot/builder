<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('broadcast')->group(function () {
    Route::get('/', 'BroadcastController@index');
});

Route::prefix('campaign')->name('broadcast.')->middleware(['auth', 'web', 'userHasActivePackage'])->group(function () {
    Route::get('/', 'BroadcastController@index')->name('index');
    Route::post('/create-form', 'BroadcastController@broadcastCreateForm')->name('create-form');
    Route::post('/create/broadcast', 'BroadcastController@createBroadcast')->name('create-broadcast');
    Route::get('/broadcast-list', 'BroadcastController@broadcastList')->name('broadcast-list');
    Route::get('/broadcast-report', 'BroadcastController@reportList')->name('report-list');
    Route::get('/details/{broadcastId}', 'BroadcastController@broadcastDetailsPage')->name('broadcast-details');
    Route::get('/broadcast/details/list', 'BroadcastController@broadcastDetailsList')->name('broadcast-details-list');
    Route::post('/email-view-form', 'BroadcastController@getEmailViewForm')->name('email-view-form');
    Route::get('/report/auto-followup', 'BroadcastController@showReport')->name('report');
    Route::post('/report-view-form', 'BroadcastController@getReportViewForm')->name('report-view-form');
    Route::post('/report-delete', 'BroadcastController@deleteReport')->name('report-delete');
    Route::get('/report/broadcast', 'BroadcastController@showBroadcastReport')->name('report-broadcast');
    Route::get('/report/autoFollowupScheduleMessage', 'BroadcastController@autoFollowupQue')->name('report-autoFollowupQue');
    Route::get('/report/auto-follow-up-queued', 'BroadcastController@autoFollowUpQueued')->name('auto-follow-up-queued');
    Route::get('/broadcast/report', 'BroadcastController@listBroadcasts')->name('broadcast-report-list');
    Route::post('/auto-followup-view-form', 'BroadcastController@getAutoFollowupViewForm')->name('auto-followup-view-form');
});

