<?php
/**
 * User: Rafiqul Islam
 * Date: 7/13/20
 * Time: 10:22 AM
 */

namespace Modules\UserRegistration\Http\Services;


use App\Contracts\Repository\UserRepository;
use App\Contracts\Service\UserContact;
use App\Helper\UtilityHelper;
use App\Mail\User\NewUserCreated;
use App\Services\Utility;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Mockery\Exception;
use Log;
use Modules\Package\Contracts\Service\PackageContact;
use Modules\Package\Contracts\Service\PaymentTransactionContact;
use Modules\Package\Contracts\Service\UserBalanceContact;
use Modules\Package\Contracts\Service\UserCardInfoContact;
use Modules\Package\Contracts\Service\UserCurrentPackageContact;
use Modules\Package\Entities\Package;
use Modules\Package\Entities\PaymentTransaction;
use Modules\Package\Entities\UserCurrentPackage;
use Modules\UserRegistration\Http\Services\StripeGateway;
use Modules\UserCompany\Contracts\Repositories\CompanyRepository;
use Modules\UserRegistration\Contracts\Services\UserRegistrationContact;

class UserRegistrationService implements UserRegistrationContact
{
    private $companyRepo;
    /**
     * @var UserRepository
     */
    private $userRepo;
    /**
     * @var PackageContact
     */
    private $packageService;
    /**
     * @var UserCardInfoContact
     */
    private $userCardInfoService;
    /**
     * @var PaymentTransactionContact
     */
    private $paymentTransactionService;
    /**
     * @var UserCurrentPackageContact
     */
    private $userCurrentPackageService;
    /**
     * @var UserBalanceContact
     */
    private $userBalanceService;
    /**
     * @var UserContact
     */
    private $userService;

    /**
     * UserRegistrationService constructor.
     * @param PackageContact $packageService
     * @param UserCardInfoContact $userCardInfoService
     * @param PaymentTransactionContact $paymentTransactionService
     * @param UserCurrentPackageContact $userCurrentPackageService
     * @param UserBalanceContact $userBalanceService
     * @param UserContact $userService
     * @param UserRepository $userRepo
     */
    public function __construct(PackageContact $packageService,
                                UserCardInfoContact $userCardInfoService,
                                PaymentTransactionContact $paymentTransactionService,
                                UserCurrentPackageContact $userCurrentPackageService,
                                UserBalanceContact $userBalanceService,
                                UserContact $userService,
                                UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
        $this->packageService = $packageService;
        $this->userCardInfoService = $userCardInfoService;
        $this->paymentTransactionService = $paymentTransactionService;
        $this->userCurrentPackageService = $userCurrentPackageService;
        $this->userBalanceService = $userBalanceService;
        $this->userService = $userService;
    }

    public function registerNewUser($request)
    {
        $response_transaction = DB::transaction(function () use ($request) {
            $password = Str::random(10);
//            $fetchedCountry = UtilityHelper::getUserLocation();
//            if($fetchedCountry == "International"){
//               $country = User::COUNTRY_INTERNATIONAL;
//            }else{
//                $country = User::COUNTRY_BANGLADESH;
//            }
            $country = User::COUNTRY_INTERNATIONAL;
            $ip = UtilityHelper::getUserIp();
            $request['country'] = $country;
            $request['ip'] = $ip;
            $user = $this->createUser($request, $password);
            $this->userService->userSettingSave($user['id']);

            return ['status' => true, 'message' => 'Account Created Successfully. Please Check Email To Get Login Info', 'user' => $user, 'password' => $password];
        });

        if ($response_transaction['status']) {
            Mail::to($response_transaction['user']->email)->send(new NewUserCreated($response_transaction['user'], $response_transaction['password']));
            return ['status' => true, 'message' => $response_transaction['message']];
        }
    }

    public function submitPayment($request, $userId)
    {
        $response_transaction = DB::transaction(function () use ($userId, $request) {
            $user = $this->userService->getUserById($userId);
            //Trying to deduct payment from existing card
            $userCardInfo = "";
            if (isset($request['isExistCard'])) {
                $isExisitingCard = 1;
                if ($request['isExistCard'] == 1) {
                    // Check user existing card
                    $userCardInfo = $this->userCardInfoService->getUserDefaultCardByUserId($userId);
                    if ($userCardInfo) {
                        $customerID = $userCardInfo['stripe_customer_id'];
                    } else {
                        return [
                            'status' => false,
                            'html' => 'Sorry. Could not charge by this card. Enter a new card'
                        ];
                    }

                }
            } else {
                $isExisitingCard = 0;
                $customer = $this->createCustomer($request);
                if (!$customer['status']) {
                    return [
                        'status' => false,
                        'html' => 'Sorry. Could not create customer in stripe!'
                    ];
                }
                $customerID = $customer['customerID'];
            }

            $this->completePaymentStripe($request, $customerID, $user, $isExisitingCard, $userCardInfo);

            return ['status' => true, 'message' => 'your payment has been successfully processed'];
        });

        if ($response_transaction['status']) {
            return ['status' => true, 'message' => $response_transaction['message']];
        }
    }


    public function completePaymentStripe($request, $customerID, $user, $isExisitingCard = 0, $userCardInfo = null)
    {
        $billingType = $request->get('billingType');
        $isRenew = $request->get('isRenew');
        $packageDetail = $this->packageService->getByPackageId($request->get('packageId'));

        if ($packageDetail->trial_pack == Package::PACKAGE_TRIAL_YES || $packageDetail->subscription_fee == 0 && $isRenew == 0) {
            if ($isExisitingCard == 1) {
                $updateUserCardInfo = $userCardInfo;
            } else {
                $updateUserCardInfo = $this->updateUserCardInfo($request, $user->id, $customerID);
            }
            if (!$updateUserCardInfo) {
                return [
                    'status' => false,
                    'html' => 'Sorry. An error occurred! Please try again.'
                ];
            }
            //Update Payment transaction table
            $addPayment = $this->addTransaction(PaymentTransaction::TRANSACTION_IN, PaymentTransaction::PAYMENT_TRANSACTION_TYPE_FOR_PACKAGE_SUBSCRIPTION, PACKAGE_TYPE_FREE, "", $user->id, 0);
            $payment_transaction_id = $addPayment->id;

            if ($packageDetail->subscription_fee == 0) {
                $isUseTrialOrPaid = UserCurrentPackage::IS_FREE_PERIOD;
            } else {
                $isUseTrialOrPaid = UserCurrentPackage::IS_IN_TRIAL_PERIOD;
            }

            //Update User Current packages table
            $addUserPackage = $this->addUserPackage($packageDetail, $user->id, $payment_transaction_id, $billingType, 0, $isUseTrialOrPaid);
            $user_current_package_id = $addUserPackage->id;
            if ($isExisitingCard == 0) {
                $this->userService->updateUserByParam(['id' => $user->id], ['package_created_from' => User::PACKAGE_CREATED_FROM_DIRECT_REGISTRATION]);
            }

            /****########### INSERT IN USER BALANCE ###########****/
            $this->addUserBalance($packageDetail, $user->id);

        } else {
            $subscription_fee = $packageDetail->subscription_fee;
            if ($billingType == UserCurrentPackage::BILLING_PERIOD_YEARLY) {
                $subscription_fee = intval(($packageDetail->yearly_discount_price * 360) / $packageDetail->life_line);
            }
            $charge = $this->chargeCustomer($subscription_fee, $customerID, $packageDetail->id, $packageDetail->name, $packageDetail->description);

            if ($charge->status == "succeeded") {
                $transaction_id = $charge->balance_transaction;
                //Update User Card Info table
                if ($isExisitingCard == 1) {
                    $updateUserCardInfo = $userCardInfo;
                } else {
                    $updateUserCardInfo = $this->updateUserCardInfo($request, $user->id, $customerID);
                }
                //Update Payment transaction table
                $addPayment = $this->addTransaction(PaymentTransaction::TRANSACTION_IN, PaymentTransaction::PAYMENT_TRANSACTION_TYPE_FOR_PACKAGE_SUBSCRIPTION, PACKAGE_TYPE_PAID, $transaction_id, $user->id, $subscription_fee);
                $payment_transaction_id = $addPayment->id;
            } else {
                return [
                    'status' => 'error',
                    'html' => 'Sorry. Could not charge your card!'
                ];
            }
            //Update User Current packages table
            $addUserPackage = $this->addUserPackage($packageDetail, $user->id, $payment_transaction_id, $billingType, $subscription_fee, UserCurrentPackage::IS_NOT_IN_TRIAL_PERIOD);
            $user_current_package_id = $addUserPackage->id;
            if ($isExisitingCard == 0) {
                $this->userService->updateUserByParam(['id' => $user->id], ['package_created_from' => User::PACKAGE_CREATED_FROM_DIRECT_REGISTRATION]);
            }
            /****########### INSERT IN USER BALANCE ###########****/
            $this->addUserBalance($packageDetail, $user->id, $billingType);
        }
    }


    public function createUser($request, $password)
    {
        $user = $this->userRepo->create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'phone' => $request->phone,
            'email' => $request->email,
            'country' => $request->country,
            'ip' => $request->ip,
            'user_created_from' => User::USER_CREATED_FROM_DIRECT_REGISTRATION,
            'package_created_from' => User::NO_PACKAGE,
            'password' => bcrypt($password)
        ]);
        return $user;
    }

    public function createCustomer($request)
    {
        try {
            $stripe = new StripeGateway();
            try {
                $customer = $stripe->createCustomer($request);
            } catch (\Exception $e) {
                return ['status' => 'error', 'message' => $e->getMessage(), 'data' => $e->getJsonBody()];
            }
        } catch (\Exception $e) {
            return ['status' => 'error', 'message' => $e->getMessage(), 'data' => $e->getJsonBody()];
        }

        return ['status' => true, 'message' => 'Customer created successfully', 'customerID' => $customer->id];
    }

    public function updateUserCardInfo($request, $User_id, $customerID)
    {
        $card_info = [
            'stripe_customer_id' => $customerID,
            'card_number' => $request->get('cardNumber'),
            'token' => $request->get('stripeToken'),
            'status' => USER_CARD_ACTIVE,
            'user_id' => $User_id
        ];
        return $this->userCardInfoService->addUserCardInfo($card_info);
    }

    public function addTransaction($type, $type_for, $package_type, $transaction_id = "", $user_id, $price)
    {
        $payment_transaction = [
            'user_id' => $user_id,
            'type' => $type,
            'type_for' => $type_for,
            'package_type' => $package_type,
            'price' => $price,
            'transaction_id' => $transaction_id,
        ];
        return $this->paymentTransactionService->transactionAdd($payment_transaction);
    }

    public function addUserPackage($packageDetail, $userId, $payment_transaction_id, $billing_period, $chargedAmount, $isUseTrialOrPaid = UserCurrentPackage::IS_IN_TRIAL_PERIOD)
    {
        if ($packageDetail->trial_pack == Package::PACKAGE_TRIAL_YES) {
            $expireDate = Carbon::today()->addDays((int)$packageDetail->trial_period);
            $trial_pack = 1;
            $lifeLine = $packageDetail->trial_period;
        } elseif ($billing_period == UserCurrentPackage::BILLING_PERIOD_YEARLY) {
            $expireDate = Carbon::today()->addDays(365);
            $trial_pack = 0;
            $lifeLine = 365;
        } else {
            $expireDate = Carbon::today()->addDays((int)$packageDetail->life_line);
            $trial_pack = 0;
            $lifeLine = $packageDetail->life_line;
        }

        $maxVisitors = $packageDetail->max_visitors;
        $maxContacts = $packageDetail->max_contacts;
        $maxAutoFollowups = $packageDetail->max_auto_followups;
        $maxEmailSend = $packageDetail->max_email_send_per_month;
        if($billing_period == UserCurrentPackage::BILLING_PERIOD_YEARLY && $packageDetail->trial_pack == 0 && $packageDetail->subscription_fee > 0) {
            $maxVisitors = $maxVisitors * 12;
            $maxContacts = $maxContacts * 12;
            $maxAutoFollowups = $maxAutoFollowups * 12;
            $maxEmailSend = $maxEmailSend * 12;
        }

        $user_current_package = [
            'user_id' => $userId,
            'package_id' => $packageDetail->id,
            'package_name' => $packageDetail->name,
            'user_payment_transaction_id' => $payment_transaction_id,
            'trial_period' => $trial_pack,
            'life_line' => $lifeLine,
            'charged_amount' => $chargedAmount,
            'is_use_trial_or_paid' => $isUseTrialOrPaid,
            'subscription_fee' => $packageDetail->subscription_fee,
            'billing_period' => $billing_period,
            'expire_date' => $expireDate,
            'status' => $packageDetail->status,
            'max_visitors' => $maxVisitors,
            'max_contacts' => $maxContacts,
            'max_auto_followups' => $maxAutoFollowups,
            'max_email_send_per_month' => $maxEmailSend
        ];
        return $this->userCurrentPackageService->userCurrentPackageUpdateOrCreate(['user_id' => $userId], $user_current_package);

    }

    public function addUserBalance($packageDetail, $userId, $billingType = UserCurrentPackage::BILLING_PERIOD_MONTHLY)
    {
        $maxVisitors = $packageDetail->max_visitors;
        $maxContacts = $packageDetail->max_contacts;
        $maxAutoFollowups = $packageDetail->max_auto_followups;
        $maxEmailSend = $packageDetail->max_email_send_per_month;
        if($billingType == UserCurrentPackage::BILLING_PERIOD_YEARLY) {
            $maxVisitors = $maxVisitors * 12;
            $maxContacts = $maxContacts * 12;
            $maxAutoFollowups = $maxAutoFollowups * 12;
            $maxEmailSend = $maxEmailSend * 12;
        }
        $where = [
            'user_id' => $userId
        ];
        $userBalance = [
            'user_package_id' => $packageDetail->id,
            'remaining_no_of_visitors' => $maxVisitors,
            'remaining_no_of_contacts' => $maxContacts,
            'remaining_no_of_auto_followups' => $maxAutoFollowups,
            'remaining_no_of_email_send_per_month' => $maxEmailSend,
        ];
        $this->userBalanceService->userBalanceUpdateOrCreate($where, $userBalance);
    }

    public function chargeCustomer($subscription_fee, $customer_id, $package_id, $package_name, $package_description)
    {
        try {
            $stripe = new StripeGateway();

            try {
                $amount = round($subscription_fee, 2) * 100;
                $charge = $stripe->chargeCustomerForPackage($amount, $customer_id, $package_id, $package_name, $package_description);
            } catch (\Exception $e) {
                return RETURN_RESPONSE('error', $e->getMessage(), $e->getJsonBody());
            }

        } catch (\Stripe\Exception\CardException $e) {
            // Since it's a decline, \Stripe\Exception\CardException will be caught
            return RETURN_RESPONSE('error', 'Something is wrong !! ', $e);
        } catch (\Stripe\Exception\RateLimitException $e) {
            return RETURN_RESPONSE('error', 'Too many requests made to the API too quickly!! ', $e);
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            return RETURN_RESPONSE('error', ' Invalid parameters were supplied to API !! ', $e);
        } catch (\Stripe\Exception\AuthenticationException $e) {
            // (maybe you changed API keys recently)
            return RETURN_RESPONSE('error', ' Authentication with API failed !! ', $e);
        } catch (\Stripe\Exception\ApiConnectionException $e) {
            return RETURN_RESPONSE('error', ' Network communication with  failed ', $e);
        } catch (\Stripe\Exception\ApiErrorException $e) {
            return RETURN_RESPONSE('error', ' Display a very generic error to the user, and maybe send ', $e);
            // yourself an email
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            return RETURN_RESPONSE('error', 'Something else happened, completely unrelated ', $e);
        }

        return $charge;
    }
}
