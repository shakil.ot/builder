<?php

namespace Modules\UserRegistration\Http\Services;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;
use Stripe\Charge;
use Stripe\Customer;
use Stripe\Error\Api;
use Stripe\Stripe;

class StripeGateway
{
    public function __construct()
    {
        Stripe::setApiKey(env('STRIPE_SECRET_KEY'));
    }


    public function createCustomer($request)
    {
        $email = $request->get('email');
        $token = $request->get('stripeToken');
        $name = $request->get('first_name').' '.$request->get('last_name');
        try {
            return Customer::create([
                'email' => $email,
                'source' => $token,
                'name' => $name
            ]);
        } catch (Exception $exception) {
            $exception->getMessage();
        }

    }

    public function chargeCustomerForPackage($amount, $customerID, $package_id, $package_name, $package_description)
    {
        try {
            return Charge::create([
                'amount' => $amount,
                'currency' => 'usd',
                'customer' => $customerID,
                'metadata' => [
                    'package_id' => $package_id,
                    'package_name' => $package_name,
                    'package_description' => $package_description
                ]
            ]);

        } catch (Exception $exception) {
            return $exception->getMessage();
        }

    }

    public function chargeToCreateSubUser($amount, $customerID, $full_name, $email, $phone)
    {
        try {
            return Charge::create([
                'amount' => $amount,
                'currency' => 'usd',
                'customer' => $customerID,
                'metadata' => [
                    'full_name' => $full_name,
                    'email' => $email,
                    'phone' => $phone
                ]
            ]);
        } catch (\Exception $exception) {
            Log::error('Error occurred to charge', [$exception->getMessage()]);
            $userId = isset(Auth::user()->id) ? Auth::id() : null;
            App::make(StripeErrorRepository::class)->insert($userId, $full_name, 'Failed to add charge in stripe payment: ' . $exception->getMessage());
            return false;
        }
    }


}
