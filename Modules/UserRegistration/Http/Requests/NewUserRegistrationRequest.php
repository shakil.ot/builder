<?php

namespace Modules\UserRegistration\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewUserRegistrationRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = array(
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|max:255|email|unique:users,email',
            'phone' => 'required|max:20|unique:users,phone'
        );

        return $rules;
    }
    public function messages()
    {
        return[
            'email.unique' => 'Email Already Taken!',
            'phone.unique' => 'Phone Number Already Taken!'
        ];
    }
}
