<?php

namespace Modules\UserRegistration\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Package\Contracts\Service\PackageContact;
use Modules\Package\Entities\Package;
use Modules\UserRegistration\Contracts\Services\UserRegistrationContact;
use Modules\UserRegistration\Http\Requests\NewUserRegistrationRequest;

class UserRegistrationController extends Controller
{
    /**
     * @var PackageContact
     */
    private $packageService;
    /**
     * @var UserRegistrationContact
     */
    private $userRegistrationService;

    /**
     * UserRegistrationController constructor.
     * @param PackageContact $packageService
     * @param UserRegistrationContact $userRegistrationService
     */
    public function __construct(PackageContact $packageService, UserRegistrationContact $userRegistrationService)
    {
        $this->packageService = $packageService;
        $this->userRegistrationService = $userRegistrationService;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $data['packages'] = $this->packageService->getAllPaidPackage();
        return view('userregistration::index')->with($data);
    }

    public function newIndex()
    {
        return view('userregistration::newIndex');
    }

    public function orderProcess($packageId, $billingType)
    {
        $data['package'] = $this->packageService->getByPackageId(base64_decode($packageId));
        $data['billingType'] = base64_decode($billingType);
        return view('userregistration::order-process')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('userregistration::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function submit(NewUserRegistrationRequest $request)
    {
        if (!$request->ajax()) {
            return redirect()->route('user.registration.index');
        }

        return response()->json($this->userRegistrationService->registerNewUser($request));
    }
    public function submitPayment(Request $request)
    {
        if (!$request->ajax()) {
            return redirect()->route('user.buy-package');
        }
        $request['email'] = auth()->user()->email;
        $request['first_name'] = auth()->user()->first_name;
        $request['last_name'] = auth()->user()->last_name;

        return response()->json($this->userRegistrationService->submitPayment($request, auth()->id()));
    }

    public function submitExistingCardPayment(Request $request)
    {
        if (!$request->ajax()) {
            return redirect()->route('user.buy-package');
        }

        return response()->json($this->userRegistrationService->submitPayment($request, auth()->id()));
    }
    public function verify($token)
    {
        return $this->userService->verify(decrypt($token));
    }

    public function submitSSLPayment(Request $request)
    {
        $this->userRegistrationService->submitSSLPayment($request, auth()->user());
    }

    public function successSSLPayment(Request $request)
    {
        $paymentStatus = $this->userRegistrationService->successSSLPayment($request);
        if ($paymentStatus['status']) {
            return redirect()->route('dashboard', auth()->user()->domain_name())->with('message', $paymentStatus['message']);
        } else {
            return redirect()->route('user.show-package', auth()->user()->domain_name())->with('error', $paymentStatus['message']);
        }
    }

    public function ipnSSL(Request $request)
    {
        $this->userRegistrationService->ipnSSL($request);
    }
}
