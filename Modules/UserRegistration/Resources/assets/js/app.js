$(function () {

    const _token = document.head.querySelector("[property=csrf-token]").content;

    (function (global, $, _token) {

        $('#user-registration-form')
            .parsley(global.parsleySettings.settings)
            .on('form:success', function (formInstance) {

                if (formInstance.isValid()) {
                    $('#preloader').show();
                    $.ajax({
                        url: route('user.registration.submit'),
                        type: 'post',
                        data: $('#user-registration-form').serializeArray(),
                        loadSpinner: true,
                        success: function (response) {
                            $('#preloader').hide();

                            if(response.status){
                                bootstrapNotify.success(response.message, 'Success');
                                sessionStorage.setItem("new_login", true);
                                setTimeout(function(){
                                    window.location.href = route('dashboard');
                                }, 2000);
                            } else {
                                bootstrapNotify.error(response.message, 'Error');
                            }
                        },
                        error: function (response) {
                            if (response.status == 422) {
                                if (response.responseJSON.errors.first_name !== undefined)
                                    bootstrapNotify.error(response.responseJSON.errors.first_name, 'Error');
                                if (response.responseJSON.errors.last_name !== undefined)
                                    bootstrapNotify.error(response.responseJSON.errors.last_name, 'Error');
                                if (response.responseJSON.errors.email !== undefined)
                                    bootstrapNotify.error(response.responseJSON.errors.email, 'Error');
                                if (response.responseJSON.errors.phone !== undefined)
                                    bootstrapNotify.error(response.responseJSON.errors.phone, 'Error');
                            }
                            $('#preloader').hide();
                        }
                    });

                }
            }).on('form:submit', function () {
            return false;
        });
    })(window, jQuery, _token);
});
