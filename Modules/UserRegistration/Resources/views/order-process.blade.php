@extends('userregistration::layouts.master')

@section('css')
    <link href="{{ asset('assets/theme/css/pages/wizard/wizard-1.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .wizard .wizard-nav{
            top: -135px;
        }
    </style>
@endsection

@section('content')
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="card card-custom mt-20">
                        <div class="card-body p-0">
                            <!--begin::Wizard-->
                            <div class="wizard wizard-1" id="user-registration-wizard" data-wizard-state="first" data-wizard-clickable="false">
                                <!--begin::Wizard Nav-->
                                <div class="wizard-nav position-absolute">
                                    <div class="wizard-steps p-8 p-lg-10">
                                        <!--begin::Wizard Step 1 Nav-->
                                        <div class="wizard-step" data-wizard-type="step" data-wizard-state="current">
                                            <div class="wizard-label">
                                                <i class="wizard-icon flaticon-user font-size-h1-xl"></i>
                                                <h3 class="wizard-title">Profile</h3>
                                            </div>
                                            <span class="svg-icon svg-icon-xl wizard-arrow">
                                                <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Arrow-right.svg-->
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                        <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000)" x="11" y="5" width="2" height="14" rx="1"></rect>
                                                        <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)"></path>
                                                    </g>
                                                </svg>
                                                <!--end::Svg Icon-->
                                            </span>
                                        </div>
                                        <!--end::Wizard Step 1 Nav-->
                                        <!--begin::Wizard Step 2 Nav-->
                                        <div class="wizard-step" data-wizard-type="step" data-wizard-state="pending">
                                            <div class="wizard-label">
                                                <i class="wizard-icon flaticon-browser font-size-h1-xl"></i>
                                                <h3 class="wizard-title">Billing</h3>
                                            </div>
                                            <span class="svg-icon svg-icon-xl wizard-arrow">
                                                <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Arrow-right.svg-->
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                        <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000)" x="11" y="5" width="2" height="14" rx="1"></rect>
                                                        <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)"></path>
                                                    </g>
                                                </svg>
                                                <!--end::Svg Icon-->
                                            </span>
                                        </div>
                                        <!--end::Wizard Step 2 Nav-->
                                        <!--begin::Wizard Step 3 Nav-->
                                        <div class="wizard-step" data-wizard-type="step" data-wizard-state="pending">
                                            <div class="wizard-label">
                                                <i class="wizard-icon flaticon-responsive font-size-h1-xl"></i>
                                                <h3 class="wizard-title">Review Order</h3>
                                            </div>
                                            <span class="svg-icon svg-icon-xl wizard-arrow last">
                                                <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Arrow-right.svg-->
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                        <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000)" x="11" y="5" width="2" height="14" rx="1"></rect>
                                                        <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)"></path>
                                                    </g>
                                                </svg>
                                                <!--end::Svg Icon-->
                                            </span>
                                        </div>
                                        <!--end::Wizard Step 3 Nav-->
                                    </div>
                                </div>
                                <!--end::Wizard Nav-->
                                <!--begin::Wizard Body-->
                                <div class="row justify-content-center my-10 px-10">
                                    <div class="col-xl-12 col-xxl-7">
                                        <!--begin::Wizard Form-->
                                        <form class="form fv-plugins-bootstrap fv-plugins-framework" id="user-registration-form" data-stripe-publishable-key="{{ env('STRIPE_PUB_KEY') }}" autoComplete="off">
                                            <input type="hidden" name="package_id" value="{{ $package->id }}">
                                            <input type="hidden" name="billing_type" value="{{ $billingType }}">
                                            <!--begin::Wizard Step 1-->
                                            <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                                <h3 class="font-size-lg text-dark-75 font-weight-bold">
                                                    <span class="svg-icon svg-icon-green svg-icon-2x">
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                                <path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="#000000" opacity="0.3"></path>
                                                                <path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000"></path>
                                                            </g>
                                                        </svg><!--end::Svg Icon-->
                                                    </span>
                                                    Setup Your Profile
                                                </h3>
                                                <hr>
{{--                                                <h3 class="mb-10 font-weight-bold text-dark">Setup Your Current Location</h3>--}}
                                                <!--begin::Input-->
                                                <div class="form-group fv-plugins-icon-container has-success">
                                                    <label>Company Name</label>
                                                    <input type="text" class="form-control company_name is_valid" name="company_name" placeholder="Enter Company Name">
                                                    <div class="fv-plugins-message-container"></div>
                                                </div>
                                                <!--end::Input-->
                                                <div class="row">
                                                    <div class="col-xl-6">
                                                        <!--begin::Input-->
                                                        <div class="form-group">
                                                            <label>First Name</label>
                                                            <input type="text" class="form-control" name="first_name" placeholder="Enter Your First Name">
                                                        </div>
                                                        <!--end::Input-->
                                                    </div>
                                                    <div class="col-xl-6">
                                                        <!--begin::Input-->
                                                        <div class="form-group">
                                                            <label>Last Name</label>
                                                            <input type="text" class="form-control" name="last_name" placeholder="Enter Your Last Name">
                                                        </div>
                                                        <!--end::Input-->
                                                    </div>
                                                </div>
                                                <!--begin::Input-->
                                                <div class="form-group">
                                                    <label>Phone</label>
                                                    <input type="text" class="form-control phone" name="phone" placeholder="Enter Your Phone Number">
                                                </div>
                                                <!--end::Input-->
                                                <!--begin::Input-->
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input type="text" class="form-control" name="email" placeholder="Enter Your Email Address">
                                                </div>
                                                <!--end::Input-->
                                            </div>
                                            <!--end::Wizard Step 1-->
                                            <!--begin::Wizard Step 2-->
                                            <div class="pb-5" data-wizard-type="step-content">
                                                <h3 class="font-size-lg text-dark-75 font-weight-bold">
                                                    <span class="svg-icon svg-icon-green svg-icon-2x">
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"/>
                                                                <rect fill="#000000" opacity="0.3" x="2" y="5" width="20" height="14" rx="2"/>
                                                                <rect fill="#000000" x="2" y="8" width="20" height="3"/>
                                                                <rect fill="#000000" opacity="0.3" x="16" y="14" width="4" height="2" rx="1"/>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                    Setup Your Billing Details
                                                </h3>
                                                <hr>
                                                <!--begin::Input-->
                                                <div class="form-group">
                                                    <script src='https://js.stripe.com/v3/' type='text/javascript'></script>
                                                    <div class="form-group">
                                                        <label>
                                                            Enter card details
                                                            <span id="panel-description-text" data-trigger="hover" data-toggle="popover" title="Why do we ask for card details?" data-html="true" data-content="We ask for your credit card to prevent interruption of your Sales Accelerator account. Having your card on file will keep your account active, and you can cancel anytime if you decide the product isn’t for you.">
                                                                <span class="svg-icon svg-icon-green">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                            <rect x="0" y="0" width="24" height="24"/>
                                                                            <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10"/>
                                                                            <path d="M12,16 C12.5522847,16 13,16.4477153 13,17 C13,17.5522847 12.5522847,18 12,18 C11.4477153,18 11,17.5522847 11,17 C11,16.4477153 11.4477153,16 12,16 Z M10.591,14.868 L10.591,13.209 L11.851,13.209 C13.447,13.209 14.602,11.991 14.602,10.395 C14.602,8.799 13.447,7.581 11.851,7.581 C10.234,7.581 9.121,8.799 9.121,10.395 L7.336,10.395 C7.336,7.875 9.31,5.922 11.851,5.922 C14.392,5.922 16.387,7.875 16.387,10.395 C16.387,12.915 14.392,14.868 11.851,14.868 L10.591,14.868 Z" fill="#000000"/>
                                                                        </g>
                                                                    </svg><!--end::Svg Icon-->
                                                                </span>
                                                            </span>
                                                        </label>
                                                        <div class="payment-block-top-bar">
                                                            <div id="card-topbar" class="form-control"></div>
                                                            <div id="card-error-topbar" class="error-topbar"></div>
                                                            <div class="fv-plugins-message-container"></div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <!--end::Input-->
                                            </div>
                                            <!--end::Wizard Step 2-->
                                            <!--begin::Wizard Step 3-->
                                            <div class="pb-5" data-wizard-type="step-content">
                                                <h3 class="font-size-lg text-dark-75 font-weight-bold">
                                                    <span class="svg-icon svg-icon-green svg-icon-2x">
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"/>
                                                                <path d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z" fill="#000000" opacity="0.3"/>
                                                                <path d="M10.875,15.75 C10.6354167,15.75 10.3958333,15.6541667 10.2041667,15.4625 L8.2875,13.5458333 C7.90416667,13.1625 7.90416667,12.5875 8.2875,12.2041667 C8.67083333,11.8208333 9.29375,11.8208333 9.62916667,12.2041667 L10.875,13.45 L14.0375,10.2875 C14.4208333,9.90416667 14.9958333,9.90416667 15.3791667,10.2875 C15.7625,10.6708333 15.7625,11.2458333 15.3791667,11.6291667 L11.5458333,15.4625 C11.3541667,15.6541667 11.1145833,15.75 10.875,15.75 Z" fill="#000000"/>
                                                                <path d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z" fill="#000000"/>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                    Review your Information
                                                </h3>
                                                <hr>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <h3 class="text-dark font-size-h3-lg font-weight-bold">Account Information</h3>
                                                        <div class="table-responsive">
                                                            <table class="table-borderless table-vertical-center">
                                                                <tbody>
                                                                <tr>
                                                                    <td class="pl-0">
                                                                        <p class="text-dark-75 font-weight-bolder mb-1 font-size-md">Company Name</p>
                                                                    </td>
                                                                    <td>:</td>
                                                                    <td class="text-right pl-3">
                                                                        <span class="text-dark-75 font-weight-bolder d-block font-size-md review_company_name"></span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="pl-0">
                                                                        <p class="text-dark-75 font-weight-bolder mb-1 font-size-md">Name</p>
                                                                    </td>
                                                                    <td>:</td>
                                                                    <td class="text-right pl-3">
                                                                        <span class="text-dark-75 font-weight-bolder d-block font-size-md review_name"></span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="pl-0">
                                                                        <p class="text-dark-75 font-weight-bolder mb-1 font-size-md">Email</p>
                                                                    </td>
                                                                    <td>:</td>
                                                                    <td class="text-right pl-3">
                                                                        <span class="text-dark-75 font-weight-bolder d-block font-size-md review_email"></span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="pl-0">
                                                                        <p class="text-dark-75 font-weight-bolder mb-1 font-size-md">Phone</p>
                                                                    </td>
                                                                    <td>:</td>
                                                                    <td class="text-right pl-3">
                                                                        <span class="text-dark-75 font-weight-bolder d-block font-size-md review_phone"></span>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <h3 class="text-dark font-size-h3-lg font-weight-bold">Billing Information</h3>
                                                        <p class="text-dark-75 font-weight-bolder mb-1 font-size-lg review_name"></p>
                                                        <span class="text-dark-75 font-weight-bolder mb-1 d-block font-size-md review_email"></span>
                                                        <span class="text-dark-75 font-weight-bolder d-block font-size-md review_phone"></span>
                                                    </div>
                                                </div>
                                                <div class="row mt-10">
                                                    <div class="col-md-6 mx-auto">
                                                        <h3 class="text-dark font-size-h3-lg font-weight-bold">Payment Information</h3>
                                                        <p class="text-dark-75 font-weight-bolder mb-1 font-size-lg">**** **** **** <span class="review_card"></span></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--end::Wizard Step 3-->
                                            <!--begin::Wizard Actions-->
                                            <div class="d-flex justify-content-between border-top mt-5 pt-10">
                                                <div class="mr-2">
                                                    <button type="button" class="btn btn-light-green font-weight-bold text-uppercase" data-wizard-type="action-prev">Previous</button>
                                                </div>
                                                <div>
                                                    <button type="button" class="btn btn-green font-weight-bold text-uppercase action-submit" data-wizard-type="action-submit">Create My Account</button>
                                                    <button type="button" class="btn btn-green font-weight-bold text-uppercase action-next" data-wizard-type="action-next">Next</button>
                                                </div>
                                            </div>
                                            <!--end::Wizard Actions-->
                                        </form>
                                        <!--end::Wizard Form-->
                                    </div>
                                </div>
                                <!--end::Wizard Body-->
                            </div>
                            <!--end::Wizard-->
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card mt-20">
                        <div class="card-body">
                            <h3 class="font-size-lg text-dark-75 font-weight-bold">
                                <span class="svg-icon svg-icon-green svg-icon-2x">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24"/>
                                            <path d="M18.1446364,11.84388 L17.4471627,16.0287218 C17.4463569,16.0335568 17.4455155,16.0383857 17.4446387,16.0432083 C17.345843,16.5865846 16.8252597,16.9469884 16.2818833,16.8481927 L4.91303792,14.7811299 C4.53842737,14.7130189 4.23500006,14.4380834 4.13039941,14.0719812 L2.30560137,7.68518803 C2.28007524,7.59584656 2.26712532,7.50338343 2.26712532,7.4104669 C2.26712532,6.85818215 2.71484057,6.4104669 3.26712532,6.4104669 L16.9929851,6.4104669 L17.606173,3.78251876 C17.7307772,3.24850086 18.2068633,2.87071314 18.7552257,2.87071314 L20.8200821,2.87071314 C21.4717328,2.87071314 22,3.39898039 22,4.05063106 C22,4.70228173 21.4717328,5.23054898 20.8200821,5.23054898 L19.6915238,5.23054898 L18.1446364,11.84388 Z" fill="#000000" opacity="0.3"/>
                                            <path d="M6.5,21 C5.67157288,21 5,20.3284271 5,19.5 C5,18.6715729 5.67157288,18 6.5,18 C7.32842712,18 8,18.6715729 8,19.5 C8,20.3284271 7.32842712,21 6.5,21 Z M15.5,21 C14.6715729,21 14,20.3284271 14,19.5 C14,18.6715729 14.6715729,18 15.5,18 C16.3284271,18 17,18.6715729 17,19.5 C17,20.3284271 16.3284271,21 15.5,21 Z" fill="#000000"/>
                                        </g>
                                    </svg>
                                </span>
                                My Cart
                            </h3>
                            <hr>
                            <div class="table-responsive">
                                <table class="table table-borderless table-vertical-center">
                                    <thead>
                                    <tr>
                                        <th class="p-0" style="min-width: 150px"></th>
                                        <th class="p-0" style="min-width: 70px"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="pl-0">
                                            <a href="#" class="text-dark font-weight-bolder text-hover-primary mb-1 font-size-lg">{{ $package->name }}</a>
                                            <span class="text-muted font-weight-bold d-block">1 {{ $billingType == \Modules\Package\Entities\UserCurrentPackage::BILLING_PERIOD_YEARLY ? 'Year' : 'Month' }} Subscription</span>
                                        </td>
                                        <td class="text-right">
                                            @if($billingType == \Modules\Package\Entities\UserCurrentPackage::BILLING_PERIOD_YEARLY)
                                                <del class="font-weight-bold d-block font-size-h5-lg">${{ $package->subscription_fee * 12 }}</del>
                                                <span class="d-block font-size-sm">Discount <span class="text-danger">-${{ ($package->subscription_fee * 12) - ($package->yearly_discount_price * 12) }}</span></span>
                                                <span class="text-dark-75 font-weight-bolder d-block font-size-h3-lg">${{ $package->yearly_discount_price * 12 }}</span>
                                                <small class="text-dark-75 font-weight-bolder d-block">(${{ $package->yearly_discount_price }} /mo)</small>
                                            @else
                                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">${{ $package->subscription_fee }}</span>
                                            @endif
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
@endsection

@section('script')
    <script src="{{ asset('js/user-registration.js') }}"></script>
@endsection
