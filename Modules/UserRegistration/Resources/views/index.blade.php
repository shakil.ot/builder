@extends('userregistration::layouts.master')

@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
                <!--begin::Entry-->
                <div class="d-flex flex-column-fluid">
                    <!--begin::Container-->
                    <div class="container">
                        <!--begin::Dashboard-->
                        <div class="card card-custom gutter-b">
                            <div class="card-header">
                                <div class="card-title">
											<span class="card-icon">
												<i class="flaticon2-chart text-primary"></i>
											</span>
                                    <h3 class="card-label">Buy package to get started</h3>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row justify-content-center">
                                    @foreach($packages as $key => $package)
                                    <!--begin: Pricing-->
                                    <div class="col-md-4 col-xxl-3 my-10 {{ $packages->count() > 3 ? 'border-bottom' : '' }} {{ $key + 1 != $packages->count() ? 'border-right' : '' }}">
                                        <div class="pb-15 px-5 text-center">
                                            <!--begin::Icon-->
                                            <div class="d-flex flex-center position-relative">
                                                <h1>{{ $package->name }}</h1>
                                            </div>
                                            <!--end::Icon-->
                                            <!--begin::Content-->
                                            <span class="font-size-h1 d-block font-weight-boldest text-dark-75 py-2">{{ $package->subscription_fee }}
                                                <sup class="font-size-h3 font-weight-normal pl-1">$</sup>
                                            </span>
                                            <p class="mb-15 d-flex flex-column">
                                                <span>{{ $package->description }}</span>
                                            </p>

                                            <div class="dropdown">
                                                <button class="btn btn-green dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Purchase Now
                                                </button>
                                                <div class="dropdown-menu p-0" aria-labelledby="dropdownMenuButton">
                                                    <div class="d-flex justify-content-between text-center">
                                                        <a href="{{ route('user.registration.order-process', ['packageId' => base64_encode($package->id), 'billingType' => base64_encode(\Modules\Package\Entities\UserCurrentPackage::BILLING_PERIOD_YEARLY)]) }}" class="px-10 py-3 bg-hover-primary text-hover-white" data-package_id="{{ $package->id }}" data-billing_period="{{ \Modules\Package\Entities\UserCurrentPackage::BILLING_PERIOD_YEARLY }}">
                                                            <div class="font-weight-bolder font-size-sm">Yearly</div>
                                                            <div class="font-weight-bolder font-size-h5">
													            <span class="font-weight-bold">$</span>{{ $package->yearly_discount_price }}/mo<sup class="text-danger">-{{ $package->yearly_discount_percentage }}%</sup>
                                                            </div>
                                                        </a>
                                                        <a href="{{ route('user.registration.order-process', ['packageId' => base64_encode($package->id), 'billingType' => base64_encode(\Modules\Package\Entities\UserCurrentPackage::BILLING_PERIOD_MONTHLY)]) }}" class="px-10 py-3 bg-green text-white" data-package_id="{{ $package->id }}" data-billing_period="{{ \Modules\Package\Entities\UserCurrentPackage::BILLING_PERIOD_MONTHLY }}">
                                                            <div class="font-weight-bolder font-size-sm">Monthly</div>
                                                            <div class="font-weight-bolder font-size-h5">
													            <span class="font-weight-bold">$</span>{{ $package->subscription_fee }}/mo
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--end::Content-->
                                        </div>
                                    </div>
                                    <!--end: Pricing-->
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <!--end::Dashboard-->
                    </div>
                    <!--end::Container-->
                </div>
                <!--end::Entry-->
            </div>
@endsection
