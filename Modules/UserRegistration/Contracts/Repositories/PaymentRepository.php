<?php

namespace Modules\UserRegistration\Contracts\Repositories;

interface PaymentRepository
{
    public function createCustomerCard($data);

    public function stripePayment($request);

    public function customerCreateInStripe($data);

    public function getCustomerIdByUserId($userId);
}
