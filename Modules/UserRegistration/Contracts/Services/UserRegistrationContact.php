<?php
/**
 * User: Rafiqul Islam
 * Date: 7/13/20
 * Time: 10:22 AM
 */

namespace Modules\UserRegistration\Contracts\Services;


interface UserRegistrationContact
{

    public function registerNewUser($request);

    public function submitPayment($request, $userId);
}
