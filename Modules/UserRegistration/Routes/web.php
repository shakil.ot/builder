<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::prefix('user')->name('user.')->middleware(['web'])->group(function() {
//    Route::Resource('registration', 'UserRegistrationController');
//    Route::get('registrations/order-process/{packageId}/{billingType}', 'UserRegistrationController@orderProcess')->name('registration.order-process');
//    Route::post('registrations/submit', 'UserRegistrationController@submit')->name('registration.submit');
//});

Route::prefix('user')->name('user.')->middleware(['web'])->group(function () {
    Route::post('grow-email/submit', 'FF@growEmailSubmit')->name('grow-email-submit');
    Route::get('registration', 'UserRegistrationController@index')->name('registration');
    Route::get('registration/index', 'UserRegistrationController@newIndex')->name('registration.index');
    Route::get('registrations/order-process/{packageId}/{billingType}', 'UserRegistrationController@orderProcess')->name('registration.order-process');
    Route::post('registrations/submit', 'UserRegistrationController@submit')->name('registration.submit');
    Route::get('registrations/verify/{token}', 'UserRegistrationController@verify')->name('verify');

    Route::post('payment/submit', 'UserRegistrationController@submitPayment')->name('payment.submit');
    Route::post('payment/existing_card_submit', 'UserRegistrationController@submitExistingCardPayment')->name('payment.existing_card_submit');

});

Route::middleware(['web'])->group(function() {
    /* SSL COMMERZ ROUTES */
    Route::post('/pay', 'SslCommerzPaymentController@index');
    Route::post('/pay-via-ajax', 'UserRegistrationController@submitSSLPayment');

    Route::post('/success', 'UserRegistrationController@successSSLPayment');
    Route::post('/fail', 'SslCommerzPaymentController@fail');
    Route::post('/cancel', 'SslCommerzPaymentController@cancel');

    Route::post('/ipn', 'UserRegistrationController@ipnSSL');
});
