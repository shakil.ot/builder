<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>EBook</title>
    <link href="{{ asset('builder/grapes.min.css') }}" rel="stylesheet">
    <link href="{{ asset('builder/grapesjs-preset-newsletter.css') }}" rel="stylesheet">
    <script src="{{ asset('assets/global/plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('builder/aviary.js') }}"></script>
{{--    <script src="{{ asset('builder/grapes.min.js') }}"></script>--}}
    <script src="{{ asset('https://grapesjs.com/js/grapes.min.js?v0.16.34') }}"></script>
    <script src="{{ asset('builder/ckeditor.js') }}"></script>

    <script src="{{ asset('builder/grapesjs-plugin-ckeditor.min.js') }}"></script>
    <script src="{{ asset('builder/grapesjs-preset-newsletter.min.js') }}"></script>
    <script src="{{ asset('builder/grapesjs-aviary.min.js') }}"></script>

    <script src="{{ asset('assets/global/plugins/toastr/toastr.min.js') }}"></script>
    <link href="{{ asset('assets/global/plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{asset('builder/css/jquery-ui.css')}}" rel="stylesheet">


    <script src="https://unpkg.com/grapesjs-plugin-forms"></script>

    <script src="{{ asset('builder/sweetalert2.min.js') }}" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="{{ asset('builder/sweetalert2.min.css') }}" crossorigin="anonymous"/>
    <link rel="stylesheet" href="{{asset('assets/preloader/preloader.css')}}">
    <link rel="stylesheet" href="{{asset('builder/css/book.css')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{ asset(env('LOGO_FAV')) }}"/>

</head>
<body>


<!--begin::Preloader-->
<div id="preloader">
    <div class="loading-wrapper">
        <div class="loading">
            <img class="logo-loading" src="{{ asset(env('LOGO_FAV')) }}" alt="logo">
            <span class="preloader-text">Loading...</span>
        </div>
    </div>
</div>
<!--begin::Preloader-->


<div class="editor-row">
    <p class="total__pages"><span>{{ $page_no }}</span> of {{ count($all_pages) }} Pages</p>
    <div class="panel__right">
        <div class="layers-container connectedSortable" id="sortable">
            @foreach($all_pages  as $page)
                <div data-page-no="{{ $page->page_no }}"
                     class="page_list_box {{ $page->id == $id  ?  'sk_active' : '' }} ui-state-default change-page-content"
                     id="main_page_div_{{ $page->id }}"
                     data-id="{{ $page->id }}"
                     style="background-image: url({!!  $page->image  !!}); cursor: pointer;">
                    <span id="page__number" class="page_number_set"><i class="fa fa-list-ul"></i> {{ $page->page_no }} </span>
                <!-- <img src=" {{ $page->image }}" alt="">  -->
                </div>
            @endforeach
        </div>
    </div>
    <div class="editor-canvas">
        <div id="gjs">{!! $content !!}</div>
    </div>
</div>
<div id="gjs-new-div"></div>

<script src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


<script type="text/template" id="script_id">
    <div class="sk_overflow_y">
        @foreach($eBookTemplate as $value)
            <div class="sk_box get-e-book-template-id" data-id="{{ $value->id }}">
                <span> <img class="library-image" src="{{ $value->image }}" alt="{{ $value->title }}">   </span>
            </div>
        @endforeach
    </div>

</script>


<script type="text/template" id="script_book_library_id">
    <div class="sk_overflow_y">
        @foreach($eBook as $bookValue)
            <div class="sk_box e-book-template-select" data-id="{{ $bookValue->id }}">
                <span> <img class="library-image" src="{{ $bookValue->image }}" alt="{{ $bookValue->title }}"> </span>
            </div>
        @endforeach
    </div>

</script>


<script type="text/javascript">

    var ebookBuilderObj = {
        id: '{{ $id }}',
        pdf_download_url: "{{ route('pdf_download_url' , [ 'user_id' => \Illuminate\Support\Facades\Auth::id() ])}}",
        ebook_edit_url: "{{ route('ebook.user-page-edit')}}",
        new_page_add: "{{ route('ebook.user-new-page-add')}}",
        total_page: "{{ $total_page }}",
        update_image_url: '{!! route('update-image') !!}',
        ebook_redirect_url: "{{ route('ebook.index')}}",
        default_style: '{{ \Modules\WebBuilder\Entities\WebBuilder::DEFAULT_STYLE }}',
        app_url: "{{ env('APP_URL') }}",
        sort_pages_url: "{{ route('ebook.sort-pages')  }}",
        delete_page: "{{ route('ebook.delete-single-page')  }}",
    };

</script>
<script type="text/javascript">

    const _token = $('meta[name="csrf-token"]').attr('content');

    $(function () {
        var sortIds = "";
        $("#sortable").sortable({
            stop: function (event, ui) {
                sortIds = "";
                var counter = 1;
                $(this).each(function () {
                    $(this).find("div").each(function () {
                        $(this).find('span').html('<i class="fa fa-list-ul"></i> ' + counter);
                        sortIds += $(this).attr('data-id') + ",";
                        counter++;
                    });
                });

                counter = 1;
                console.log(sortIds)
                if (sortIds != '') {
                    $.ajax({
                        url: ebookBuilderObj.sort_pages_url,
                        type: "POST",
                        data: {
                            sortIds: sortIds,
                            _token: _token
                        },
                        success: function (response) {
                            if (response.status) {
                                toastr.success(response.html);
                            } else {
                                toastr.error(response.html);
                            }
                        }
                    });
                }
            }
        });
        $("#sortable").disableSelection();
    });

    window.addEventListener("load", function(){
        console.log("loading done ..");
        $('#preloader').hide();

    });

    localStorage.removeItem('gjscomponents');
    localStorage.removeItem('gjsassets');
    localStorage.removeItem('gjshtml');
    localStorage.removeItem('gjscss');
    localStorage.removeItem('gjsstyles');

    var host = '//artf.github.io/grapesjs/';
    var images = [];


    var ebookContent = '';

    // Set up GrapesJS editor with the Newsletter plugin
    var editor = grapesjs.init({
        clearOnRender: true,
        height: '100%',
        storageManager: {
            id: 'gjs',
        },
        assetManager: {
            assets: images,
            upload: 0,
            uploadText: 'Uploading is not available in this demo',
        },
        container: '#gjs',
        fromElement: true,
        plugins: ['gjs-preset-newsletter', 'gjs-plugin-ckeditor'],
        pluginsOpts: {
            'gjs-preset-newsletter': {
                modalLabelImport: 'Paste all your code here below and click import',
                modalLabelExport: 'Copy the code and use it wherever you want',
                codeViewerTheme: 'material',
                //defaultTemplate: templateImport,
                importPlaceholder: '<table class="table"><tr><td class="cell">Hello world!</td></tr></table>',
                cellStyle: {
                    'font-size': '12px',
                    'font-weight': 300,
                    'vertical-align': 'top',
                    color: 'rgb(111, 119, 125)',
                    margin: 0,
                    padding: 0,
                }
            },
            'gjs-plugin-ckeditor': {
                position: 'center',
                options: {
                    startupFocus: true,
                    extraAllowedContent: '*(*);*{*}', // Allows any class and any inline style
                    allowedContent: true, // Disable auto-formatting, class removing, etc.
                    enterMode: CKEDITOR.ENTER_BR,
                    extraPlugins: 'sharedspace,justify,colorbutton,panelbutton,font',
                    toolbar: [
                        {name: 'styles', items: ['Font', 'FontSize']},
                        ['Bold', 'Italic', 'Underline', 'Strike'],
                        {name: 'paragraph', items: ['NumberedList', 'BulletedList']},
                        {name: 'links', items: ['Link', 'Unlink']},
                        {name: 'colors', items: ['TextColor', 'BGColor']},
                    ],
                }
            }
        }
    });

    // Let's add in this demo the possibility to test our newsletters
    var mdlClass = 'gjs-mdl-dialog-sm';
    var pnm = editor.Panels;
    var cmdm = editor.Commands;
    var md = editor.Modal;

    // Add info command
    var infoContainer = document.getElementById("info-panel");
    cmdm.add('open-info', {
        run: function (editor, sender) {
            sender.set('active', 0);
            var mdlDialog = document.querySelector('.gjs-mdl-dialog');
            mdlDialog.className += ' ' + mdlClass;
            infoContainer.style.display = 'block';
            md.setTitle('About this demo');
            md.setContent('');
            md.setContent(infoContainer);
            md.open();
            md.getModel().once('change:open', function () {
                mdlDialog.className = mdlDialog.className.replace(mdlClass, '');
            })
        }
    });
    pnm.addButton('options', [{
        id: 'undo',
        className: 'fa fa-undo',
        attributes: {title: 'Undo'},
        command: function () {
            editor.runCommand('core:undo')
        }
    }, {
        id: 'redo',
        className: 'fa fa-repeat',
        attributes: {title: 'Redo'},
        command: function () {
            editor.runCommand('core:redo')
        }
    }, {
        id: 'clear-all',
        className: 'fa fa-refresh icon-blank',
        attributes: {title: 'Clear canvas'},
        command: {
            run: function (editor, sender) {
                sender && sender.set('active', false);
                if (confirm('Are you sure to clean the canvas?')) {
                    editor.DomComponents.clear();
                    setTimeout(function () {
                        localStorage.clear()
                    }, 0)
                }
            }
        }
    },
        //     {
        //     id: 'view-info',
        //     className: 'fa fa-question-circle',
        //     command: 'open-info',
        //     attributes: {
        //         'title': 'About',
        //         'data-tooltip-pos': 'bottom',
        //     },
        // }
    ]);


    editor.Panels.addButton
    ('options',
        [{
            id: 'delete-page-db',
            className: 'fa fa-trash',
            command: 'delete-page-db',
            attributes: {title: 'Delete Page'}
        }]
    );

    editor.Commands.add('delete-page-db', {
        run: function (editor, sender) {

            Swal.fire({
                title: 'Are you sure to remove this page ?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: ebookBuilderObj.delete_page,
                        type: "POST",
                        data: {
                            id: ebookBuilderObj.id,
                            _token: _token
                        },
                        success: function (response) {
                            if (response.status) {
                                Swal.fire(
                                    'Deleted!',
                                    response.html,
                                    'success'
                                );
                                window.location.href = response.redirect_url;
                            } else {
                                toastr.error(response.html);
                            }
                            $('#gjs-new-div').html("");
                        }
                    });
                }
            })
        }
    });


    editor.Panels.addButton
    ('options',
        [{
            id: 'save-db',
            className: 'fa fa-floppy-o',
            command: 'save-db',
            attributes: {title: 'Upload'}
        }]
    );

    editor.Panels.addButton
    ('options',
        [{
            id: 'pdf-download-link-sk',
            className: 'fa fa-file-pdf-o',
            command: 'pdf-download-link-sk',
            attributes: {title: 'Download Pdf'}
        }]
    );

    editor.Panels.addButton
    ('options',
        [{
            id: 'add-page',
            className: 'fa fa-plus-circle',
            command: 'add-page',
            attributes: {title: 'Add Page'}
        }]
    );


    // Simple warn notifier
    var origWarn = console.warn;
    toastr.options = {
        closeButton: true,
        preventDuplicates: true,
        showDuration: 250,
        hideDuration: 150
    };
    console.warn = function (msg) {
        toastr.warning(msg);
        origWarn(msg);
    };

    // Beautify tooltips
    var titles = document.querySelectorAll('*[title]');
    for (var i = 0; i < titles.length; i++) {
        var el = titles[i];
        var title = el.getAttribute('title');
        title = title ? title.trim() : '';
        if (!title)
            break;
        el.setAttribute('data-tooltip', title);
        el.setAttribute('title', '');
    }


    // Do stuff on load
    editor.on('load', function () {
        var $ = grapesjs.$;

        //Do something here after load
    });

    // Add the command
    var loadModal = editor.Modal;
    editor.Commands.add('save-db', {
        run: function (editor, sender) {
            $('#preloader').show();
            ebookContent = editor.runCommand('gjs-get-inlined-html');
            if (ebookContent === '') {
                toastr.error('Please Build a Template');
                $('#preloader').hide();
                return false;
            }

            if ($('.template_title').val() === '') {
                toastr.error('Template Title Required');
                $('#preloader').hide();
                return false;
            }

            const ebookBuilder = {
                ebookContent: ebookContent,
                id: ebookBuilderObj.id,
                _token: _token,
            }
            $('#gjs-new-div').html(ebookContent);

            html2canvas(document.getElementById("gjs-new-div") , {
                height: 1050
            }).then(function (canvas) {

                var image_data = canvas.toDataURL("image/jpeg", 0.9);
                ebookBuilder.image = image_data;
                $('#main_page_div_'+ ebookBuilderObj.id).css('background-image','url('+ image_data +')');
                console.log(image_data);
                $('#gjs-new-div').html("");

                $.ajax({
                    url: ebookBuilderObj.ebook_edit_url,
                    type: "POST",
                    data: ebookBuilder,
                    success: function (response) {
                        if (response.status) {
                            toastr.success(response.html);
                            // location.reload();
                        } else {
                            toastr.error(response.html);
                        }

                        $('#preloader').hide();
                    }
                });

            });

        }
    });

    var loadModal = editor.Modal;
    var loadCommands = editor.Commands;
    var mdlClass = 'gjs-mdl-dialog-sm';

    editor.Commands.add('add-page', {
        run: function (editor, sender) {

            var modalContent = loadModal.getContentEl();
            var mdlDialog = document.querySelector('.gjs-mdl-dialog');
            // var cmdGetCode = cmdm.get('gjs-get-inlined-html');
            // contentEl.value = cmdGetCode && cmdGetCode.run(editor);
            mdlDialog.className += ' ' + mdlClass;
            // testContainer.style.display = 'block';
            loadModal.setTitle('Save Your Template');
            // md.setContent('');

            loadModal.setContent($('#script_id').html());
            loadModal.open();
            $('.gjs-mdl-dialog').find('.template_title').attr('placeholder', 'Enter your template title')
            loadModal.getModel().once('change:open', function () {
                mdlDialog.className = mdlDialog.className.replace(mdlClass, '');
                document.getElementsByClassName("template_title").placeholder = "Type name here..";
                //clean status
            });
        }
    });

    $(document).on('click', '.get-e-book-template-id', function () {
        $.ajax({
            url: ebookBuilderObj.new_page_add,
            type: "POST",
            data: {
                id: ebookBuilderObj.id,
                total_page: ebookBuilderObj.total_page,
                admin_template_id: $(this).attr('data-id'),
                _token: _token,
            },
            success: function (response) {
                if (response.status) {
                    toastr.success(response.html);
                    setTimeout(function () {
                        window.location = response.url
                    }, 1000);

                } else {
                    toastr.error(response.html);
                }
            }
        });
    })

    editor.Panels.addButton
    ('options',
        [{
            id: 'book-library',
            className: 'fa fa-book',
            command: 'book-library',
            attributes: {title: 'Book Library'}
        }]
    );

    editor.Commands.add('book-library', {
        run: function (editor, sender) {

            var modalContent = loadModal.getContentEl();
            var mdlDialog = document.querySelector('.gjs-mdl-dialog');
            // var cmdGetCode = cmdm.get('gjs-get-inlined-html');
            // contentEl.value = cmdGetCode && cmdGetCode.run(editor);
            mdlDialog.className += ' ' + mdlClass;
            // testContainer.style.display = 'block';
            loadModal.setTitle('Ebook Library');
            // md.setContent('');

            loadModal.setContent($('#script_book_library_id').html());
            loadModal.open();
            $('.gjs-mdl-dialog').find('.template_title').attr('placeholder', 'Enter your template title')
            loadModal.getModel().once('change:open', function () {
                mdlDialog.className = mdlDialog.className.replace(mdlClass, '');
                document.getElementsByClassName("template_title").placeholder = "Type name here..";
                //clean status
            });
        }
    });

    editor.Commands.add('pdf-download-link-sk', {
        run: function (editor, sender) {
            window.open(ebookBuilderObj.pdf_download_url, "_blank");
        }
    });

    editor.Panels.addButton
    ('options',
        [{
            id: 'redirect-template',
            className: 'fa fa-external-link',
            command: 'redirect-template',
            attributes: {title: 'Redirect to dashboard'}
        }]
    );

    editor.Commands.add
    ('redirect-template',
        {
            run: function (editor, sender) {

                window.location.href = ebookBuilderObj.app_url;
            }
     });

    $(document).on('click', '.e-book-template-select', function () {

        if ($(this).attr('data-id') == null) {
            return false;
        }


        $.ajax({
            url: "{{ route('ebook.set-book-by-select') }}",
            type: "POST",
            data: {
                id: $(this).attr('data-id'),
                _token: _token,
            },
            success: function (response) {
                if (response.status) {
                    toastr.success(response.html);
                    setTimeout(function () {
                        location.href = '{{ route('ebook.index') }}';
                    }, 1000);

                } else {
                    toastr.error(response.html);
                }
            }
        });
    });

    $(document).on('click', '.change-page-content', function () {
        var el = $(this);
        if (el.attr('data-id') == null) {
            return false;
        }
        var id = el.attr('data-id');

        $.ajax({
            url: "{{ route('ebook.get-single-content-by-id') }}",
            type: "POST",
            data: {
                id: id,
                _token: _token,
            },
            success: function (response) {
                if (response.status) {
                    editor.setComponents(response.data.content);
                    ebookBuilderObj.id = response.data.id;
                    $('.change-page-content').removeClass('sk_active');
                    el.addClass('sk_active');
                }
            }
        });
    });


</script>
</body>
</html>
