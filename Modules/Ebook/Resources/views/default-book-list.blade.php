@extends('user.layout.app', ['menu' => 'dashboard'])

@section('css')

    <style>
        .image_style {
            width: 100%;
            height: 200px;
            cursor: pointer;
            margin-bottom: 2px;
        }
    </style>

@endsection

@section('content')



    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="d-flex flex-column-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-custom card-stretch gutter-b">

                            <!--begin::Body-->
                            <div class="card-body p-10">
                                <div class="row">
                                    @if(count($eBookTemplate) > 0)
                                        @foreach($eBookTemplate as $book)
                                            <div class="col-md-3">
                                                <img src="{{ $book->image }}"
                                                     class="image_style e-book-template-select"
                                                     data-id="{{ $book->id }}" alt="{{ $book->title }}">
                                            </div>
                                        @endforeach
                                    @else
                                        <h1> Book Not Found !! </h1>
                                    @endif
                                </div>

                            </div>
                            <!--end::Body-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection

@section('script')

    <script>
        $(document).on('click', '.e-book-template-select', function () {

            if ($(this).attr('data-id') == null) {
                return false;
            }
            const _token = document.head.querySelector("[property=csrf-token]").content;

            $.ajax({
                url: "{{ route('ebook.set-book-by-select') }}",
                type: "POST",
                data: {
                    id: $(this).attr('data-id'),
                    _token: _token,
                },
                success: function (response) {
                    if (response.status) {
                        toastr.success(response.html);
                        setTimeout(function () {
                            location.reload();
                        }, 1000);

                    } else {
                        toastr.error(response.html);
                    }
                }
            });
        });


    </script>



@endsection

