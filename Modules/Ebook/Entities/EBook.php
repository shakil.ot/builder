<?php


namespace Modules\Ebook\Entities;

use Illuminate\Database\Eloquent\Model;

class EBook extends Model
{
    protected $table = 'e_books';

    protected $fillable = [
        'user_id',
        'page_no',
        'image',
        'content',
        'status'
    ];

}