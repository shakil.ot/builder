<?php

namespace Modules\Ebook\Http\Services;

use Modules\AdminEbook\Contracts\Repositories\AdminEbookTemplateDetailsRepository;
use Modules\AdminWebBuilder\Contracts\Repositories\AdminWebTemplateRepository;
use Modules\Ebook\Contracts\Repositories\EBookRepository;
use Modules\Ebook\Contracts\Service\EBookContact;
use DB;
use Log;
use PDF;

class EBookService implements EBookContact
{

    /**
     * @var EBookRepository
     */
    private $eBookRepository;
    /**
     * @var AdminWebTemplateRepository
     */
    private $adminWebTemplateRepository;
    /**
     * @var AdminEbookTemplateDetailsRepository
     */
    private $adminEbookTemplateDetailsRepository;

    public function __construct(EBookRepository $eBookRepository,
                                AdminWebTemplateRepository $adminWebTemplateRepository,
                                AdminEbookTemplateDetailsRepository $adminEbookTemplateDetailsRepository
    )
    {
        $this->eBookRepository = $eBookRepository;
        $this->adminWebTemplateRepository = $adminWebTemplateRepository;
        $this->adminEbookTemplateDetailsRepository = $adminEbookTemplateDetailsRepository;
    }


    public function getAllPagesByUserId($user_id)
    {
        return $this->eBookRepository->getAllByCondition([
            'user_id' => $user_id,
        ]);
    }

    public function getPageDataByUserIdAndPageId($user_id, $pageNo)
    {
        return $this->eBookRepository->getByCondition([
            'user_id' => $user_id,
            'page_no' => $pageNo
        ]);
    }

    public function getFirstPageDataByUserId($user_id)
    {
        return $this->eBookRepository->getByCondition([
            'user_id' => $user_id,
            'page_no' => 1
        ]);
    }

    public function eBookUpdate($request, $user_id)
    {
        $result = $this->eBookRepository->updateOrCreateData([
            'user_id' => $user_id,
            'id' => $request['id'],

        ], [
            'content' => $request['ebookContent'],
            'image' => $request['image'],
        ]);

        if ($result) {
            return [
                'status' => true,
                'html' => 'Ebook Page Edited Successfully!',
            ];
        }

        return [
            'status' => false,
            'html' => 'Something is wrong !!',
        ];

    }

    public function newPageAdd($request, $user_id)
    {
        $template = $this->adminWebTemplateRepository->getTemplateByConditionFirst([
            'id' => $request['admin_template_id'],
        ]);

        $result = $this->eBookRepository->updateOrCreateData([
            'user_id' => $user_id,
            'page_no' => $request['total_page'] + 1,

        ], [
            'content' => $template->content,
        ]);

        if ($result) {
            return [
                'status' => true,
                'html' => 'Ebook Page Created Successfully!',
                'pageId' => $result->id,
                'url' => route('ebook.index', ['pageNo' => $result->page_no])

            ];
        }

        return [
            'status' => false,
            'html' => 'Something is wrong !!',
        ];

    }

    public function setBookBySelect($request, $user_id)
    {

        $this->eBookRepository->deleteAllBookByUserId($user_id);

        $result = $this->adminEbookTemplateDetailsRepository->getAllByCondition([
            'admin_ebook_template_id' => $request['id'],
        ]);

        if (count($result) > 0) {
            foreach ($result as $value) {
                $result = $this->eBookRepository->updateOrCreateData([
                    'user_id' => $user_id,
                    'page_no' => $value->page_no,

                ], [
                    'content' => $value->content,
                    'image' => $value->image,
                ]);
            }

            return [
                'status' => true,
                'html' => 'Ebook Added Successfully!',
            ];

        }

        return [
            'status' => false,
            'html' => 'Something is wrong !!',
        ];

    }

    public function pdfDownloadUrl($user_id)
    {
        $result = $this->eBookRepository->getAllPagesById($user_id);

        $html = '';

        foreach ($result as $value) {
            $view = view('pdf.document')->with(['data' => $value->content]);
            $html .= $view->render() . '<div style="page-break-before: always;"></div>';

        }

        $start = '<html><body>';
        $end = '</body></html>';

        $pdf = PDF::loadHTML($start . $html . $end);

        return $pdf->stream('document.pdf');
    }

    public function sortPages($request, $user_id)
    {
        $count = 1;
        if (!is_null($request['sortIds'])){
            foreach (explode(",", rtrim($request['sortIds'], ",")) as $id) {

                $this->eBookRepository->updateOrCreateData([
                    'user_id' => $user_id,
                    'id' => $id,

                ], [
                    'page_no' => $count,
                ]);
                $count++;
            }

            return [
                'html' => 'Page move successfully ',
                'status' => true
            ];
        }
    }

    public function getAllPageSortByPage($user_id)
    {
        return $this->eBookRepository->getAllPageSortByPage($user_id);
    }

    public function deleteSinglePage($request, $user_id)
    {
        $allPages = $this->getAllPageSortByPage($user_id);

        if (count($allPages) == 1){
            return [
                'html' => 'Page can not delete. There must be at least one in the EBook !! ',
                'status' => false,
            ];
        }

        $response = $this->eBookRepository->deleteByCondition([
            'id' => $request['id'],
            'user_id' => $user_id,
        ]);

        if ($response) {
            $count = 1;

            $allPages = $this->getAllPageSortByPage($user_id);

            foreach ($allPages as $value) {

                $this->eBookRepository->updateOrCreateData([
                    'user_id' => $user_id,
                    'id' => $value->id,

                ], [
                    'page_no' => $count,
                ]);
                $count++;
            }

            return [
                'html' => 'Page deleted successfully',
                'status' => true,
                'redirect_url' => route('ebook.index', ['pageNo' => 1])
            ];
        }

        return [
            'html' => 'Somethings is wrong !!',
            'status' => false
        ];

    }

    public function getSingleContentById($request, $user_id)
    {
        $result = $this->eBookRepository->getByCondition([
            'id' => $request['id'],
            'user_id' => $user_id,
        ]);

        if ($result) {
            return [
                'html' => '',
                'status' => true,
                'data' => $result,
            ];
        }

        return [
            'html' => 'Somethings is wrong !!',
            'status' => false
        ];

    }
}
