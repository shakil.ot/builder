<?php

namespace Modules\Ebook\Http\Repositories;

use App\Repositories\BaseRepository\BaseRepository;
use Modules\Ebook\Contracts\Repositories\EBookRepository;
use Modules\Ebook\Entities\EBook;


class EBookRepositoryEloquent extends BaseRepository implements EBookRepository
{
    protected function model()
    {
        return new EBook();
    }

    public function storeEbookPage($data)
    {
        return $this->model->create($data);
    }

    public function getByCondition($where)
    {
        return $this->model->where($where)->first();
    }

    public function updateOrCreateData($where, $data)
    {
        return $this->model->updateOrCreate($where, $data);
    }

    public function getAllByCondition($where)
    {
        return $this->model->where($where)->get();
    }

    public function deleteAllBookByUserId($user_id)
    {
        return $this->model->where(['user_id' => $user_id])->delete();
    }

    public function getAllPagesById($user_id)
    {
        return $this->model->where(['user_id' => $user_id])->orderBy('page_no', 'asc')->get();
    }

    public function getAllPageSortByPage($user_id)
    {
        return $this->model->where(['user_id' => $user_id])->orderBy('page_no', 'asc')->get();
    }

    public function deleteByCondition($where)
    {
        return $this->model->where($where)->delete();
    }
}
