<?php

namespace Modules\Ebook\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\AdminEbook\Contracts\Service\AdminEbookTemplateContact;
use Modules\AdminWebBuilder\Contracts\Service\AdminWebTemplateContact;
use Modules\Ebook\Contracts\Service\EBookContact;

class EbookController extends Controller
{
    private $bookService;
    private $adminWebTemplateService;
    private $adminEbookTemplateService;

    public function __construct(EBookContact $bookService,
                                AdminEbookTemplateContact $adminEbookTemplateService,
                                AdminWebTemplateContact $adminWebTemplateService)
    {
        $this->bookService = $bookService;
        $this->adminWebTemplateService = $adminWebTemplateService;
        $this->adminEbookTemplateService = $adminEbookTemplateService;

    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($pageNo = null)
    {
        if (is_null($pageNo)) {
            $pageData = $this->bookService->getFirstPageDataByUserId(auth()->id());
        } else {
            $pageData = $this->bookService->getPageDataByUserIdAndPageId(auth()->id(), $pageNo);
        }

        $eBook = $this->adminEbookTemplateService->getEBookMainPage();


        $eBookTemplate = $this->adminWebTemplateService->getAllEBookTemplate();


        $result = $this->bookService->getAllPageSortByPage(auth()->id());

        return view('ebook::index')->with([
            'content' => $pageData->content,
            'page_no' => $pageData->page_no,
            'id' => $pageData->id,
            'all_pages' => $result,
            'total_page' => count($result),
            'eBookTemplate' => $eBookTemplate,
            'eBook' => $eBook,
        ]);

    }

    public function pageEdit(Request $request)
    {
        return $this->bookService->eBookUpdate($request, auth()->id());

    }

    public function newPageAdd(Request $request)
    {
        return $this->bookService->newPageAdd($request, auth()->id());
    }

    public function setBookBySelect(Request $request)
    {
        return $this->bookService->setBookBySelect($request, auth()->id());
    }

    public function pdfDownloadUrl($user_id)
    {
      return $this->bookService->pdfDownloadUrl($user_id);
    }

    public function sortPages(Request $request)
    {
        return $this->bookService->sortPages($request , auth()->id());
    }

    public function deleteSinglePage(Request $request)
    {
        return $this->bookService->deleteSinglePage($request , auth()->id());
    }

    public function getSingleContentById(Request $request)
    {
        return $this->bookService->getSingleContentById($request , auth()->id());
    }

}
