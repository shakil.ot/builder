<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('ebook')->name('ebook.')->middleware(['auth', 'web', 'userHasActivePackage'])->group(function() {
    Route::get('/{pageNo?}', 'EbookController@index')->name('index');
    Route::post('/user-page-edit', 'EbookController@pageEdit')->name('user-page-edit');
    Route::post('/user-new-page-add', 'EbookController@newPageAdd')->name('user-new-page-add');
    Route::post('/set-book-by-select' , 'EbookController@setBookBySelect')->name('set-book-by-select');
    Route::post('/sort-pages' , 'EbookController@sortPages')->name('sort-pages');
    Route::post('/delete-single-page' , 'EbookController@deleteSinglePage')->name('delete-single-page');
    Route::post('/get-single-content-by-id' , 'EbookController@getSingleContentById')->name('get-single-content-by-id');
//    Route::get('/pdf_download_url/{user_id}' , 'EbookController@pdfDownloadUrl')->name('pdf_download_url');
});

Route::get('/pdf_download_url/{user_id}' , 'EbookController@pdfDownloadUrl')->name('pdf_download_url');
