<?php

namespace Modules\Ebook\Contracts\Repositories;

interface EBookRepository
{
    public function storeEbookPage($data);

    public function getByCondition($where);

    public function updateOrCreateData($where, $data);

    public function getAllByCondition($where);

    public function deleteAllBookByUserId($user_id);

    public function getAllPagesById($user_id);

    public function getAllPageSortByPage($user_id);

    public function deleteByCondition($where);

}
