<?php

namespace Modules\Ebook\Contracts\Service;

interface EBookContact
{
    public function getAllPagesByUserId($user_id);

    public function getPageDataByUserIdAndPageId($user_id , $pageNo);

    public function getFirstPageDataByUserId($user_id);

    public function eBookUpdate($request , $user_id);

    public function newPageAdd($request , $user_id);

    public function setBookBySelect($request , $user_id);

    public function pdfDownloadUrl($user_id);

    public function sortPages($request , $user_id);

    public function getAllPageSortByPage($user_id);

    public function deleteSinglePage($request , $user_id);

    public function getSingleContentById($request , $user_id);

}
