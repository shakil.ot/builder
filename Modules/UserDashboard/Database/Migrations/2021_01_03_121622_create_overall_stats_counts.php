<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOverallStatsCounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('overall_stats_counts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->dateTime('activity_date_string')->index();
            $table->unsignedInteger('total_unique_visitors')->default(0);
            $table->unsignedInteger('total_subscription_visits')->default(0);
            $table->unsignedInteger('total_subscription_submits')->default(0);
            $table->unsignedInteger('total_download_instruction_visits')->default(0);
            $table->unsignedInteger('total_offer_visits')->default(0);
            $table->unsignedInteger('total_offer_submits')->default(0);
            $table->unsignedInteger('total_thank_you_visits')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('overall_stats_counts');
    }
}
