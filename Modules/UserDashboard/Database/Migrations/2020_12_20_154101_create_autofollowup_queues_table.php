<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutofollowupQueuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auto_followup_queues', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->bigInteger('auto_followup_id')->unsigned();
            $table->foreign('auto_followup_id')
                ->references('id')->on('auto_followups')
                ->onDelete('cascade');

            $table->bigInteger('contact_id')->unsigned();
            $table->foreign('contact_id')
                ->references('id')->on('contacts')
                ->onDelete('cascade');

            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');

            $table->integer('day');
            $table->dateTime('schedule_time');
            $table->tinyInteger('status')->default(1)->comment('');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('auto_followup_queues');
        Schema::enableForeignKeyConstraints();
    }
}
