<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSingleSendStatReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('single_send_stat_reports', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('user_id')->comment('user as client');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->unsignedBigInteger('broadcast_id')->nullable();
            $table->foreign('broadcast_id')->references('id')->on('broadcasts');

            $table->unsignedBigInteger('auto_followup_id')->nullable();
            $table->foreign('auto_followup_id')->references('id')->on('auto_followups');

            $table->string('tracking_id')->index();
            $table->date('activity_date_string')->index();
            $table->unsignedInteger('email_sent')->default(0);
            $table->unsignedInteger('email_opened')->default(0);
            $table->unsignedInteger('email_clicked')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('single_send_stat_reports');
    }
}
