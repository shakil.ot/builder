let dashboardObj = {
    isUserTour: "1"
};

let Dashboard = (function (dashboardObj) {
    $(function () {
        const _token = document.head.querySelector("[property=csrf-token]").content;

        (function (global, $, _token) {
            let durationType = $('#show-date-wise').children("option:selected").attr('data-value');

            $(document).ready(function (){
                loadCountStat(durationType);
                loadFunnelStepsCount();
            });

            function loadCountStat(durationType) {
                global.mySiteAjax({
                    url: route('dashboard-duration-wise-count'),
                    type: 'post',
                    data: {durationType: durationType, _token: _token},
                    success: function (response) {
                        $("#totalUniqueVisitors").html(response.total_unique_visitors);
                        $("#totalSubscribers").html(response.total_subscribers);
                        $("#totalLeads").html(response.total_leads);
                        $("#totalEmailSent").html(response.total_sent_email);
                        $("#totalEmailOpened").html(response.total_opened_email);
                        $("#totalEmailClicked").html(response.total_clicked_email);
                    }
                });
            }

            $('#show-date-wise').click(function () {
                let durationType = $(this).children("option:selected").attr('data-value');
                loadCountStat(durationType);
            });

            function loadFunnelStepsCount() {
                global.mySiteAjax({
                    url: route('dashboard-funnel-steps-count'),
                    type: 'get',
                    success: function (response) {
                        $("#1_funnel").html(response.total_subscription_visits);
                        $("#1_funnel2").html(response.total_subscription_submits);
                        $("#2_funnel").html(response.total_download_instruction_visits);
                        $("#3_funnel").html(response.total_offer_visits);
                        $("#3_funnel2").html(response.total_offer_submits);
                        $("#4_funnel").html(response.total_thank_you_visits);
                    }
                });
            }

        })(window, jQuery, _token);
    });

})(dashboardObj);