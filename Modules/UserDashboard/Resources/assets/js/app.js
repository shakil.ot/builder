let businessSetting = (function (BusinessSettingObj) {

    $(function () {


        const _token = document.head.querySelector("[property=csrf-token]").content;


        (function (global, $, _token) {
            $('#business-settings-form')
                .parsley(global.parsleySettings.settings)
                .on('form:success', function (formInstance) {

                    if (formInstance.isValid()) {

                        global.mySiteAjax({
                            url: route('user.settings-add'),
                            type: 'post',
                            data: $('#business-settings-form').serializeArray(),
                            loadSpinner: true,
                            success: function (response) {
                                /* parsley css here end */
                                if (response.status == 'validation-error')
                                    global.generateErrorMessage(formInstance, response.html);
                                else if (response.status == 'success') {
                                    bootstrapNotify.success(response.html);
                                } else if (response.status == 'error') {
                                    route('package-update').error(response.html, "Server Notification");
                                }
                            },
                            complete: function (response) {
                                if (response.status == 422) {
                                    // console.log(response.responseJSON.errors.shop_name_on_whatsapp[0]);
                                    global.generateErrorMessage(formInstance, response.responseJSON.errors);
                                    if (response.responseJSON.errors.brand_name[0] !== undefined) {
                                        bootstrapNotify.error(response.responseJSON.errors.brand_name[0], "Error");
                                    }
                                    if (response.responseJSON.errors.shop_name_on_whatsapp[0] !== undefined) {
                                        bootstrapNotify.error(response.responseJSON.errors.shop_name_on_whatsapp[0], "Error");
                                    }
                                }
                            }
                        });

                    }
                }).on('form:submit', function () {

                return false;
            });

        })(window, jQuery, _token);
    });

})(BusinessSettingObj);


