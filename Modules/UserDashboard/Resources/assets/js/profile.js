$(function () {

    const _token = document.head.querySelector("[property=csrf-token]").content;

    (function (global, $, _token) {
        /*Image selection initialize*/
        new KTImageInput('profile_image');

        /*Sticky left profile side*/
        // if (KTLayoutAsideToggle && KTLayoutAsideToggle.onToggle) {
        //     var sticky = new Sticky('.sticky');
        //
        //     KTLayoutAsideToggle.onToggle(function() {
        //         setTimeout(function() {
        //             sticky.update(); // update sticky positions on aside toggle
        //         }, 500);
        //     });
        // }

        /*Submit add form*/
        $('#update-profile-form')
            .parsley(global.parsleySettings.settings)
            .on('form:success', function (formInstance) {
                if (formInstance.isValid()) {
                    $('#preloader').show();


                    var formData = new FormData($('#update-profile-form').get(0));

                    $.ajax({
                        url: route('user.profile.update'),
                        type: 'post',
                        data: formData,
                        contentType: false,
                        processData: false,
                        loadSpinner: true,
                        success: function (response) {
                            $('#preloader').hide();
                            response.status ? bootstrapNotify.success(response.message, 'Success') : bootstrapNotify.error(response.message, 'Error');
                        },
                        error: function (response) {
                            if (response.status == 422) {
                                global.generateErrorMessage(formInstance, response.responseJSON.errors);
                                bootstrapNotify.error(response.responseJSON.message, "Error");
                            }
                            $('#preloader').hide();
                        }
                    });
                }
            }).on('form:submit', function () {
            return false;
        });

        /*Change password*/
        $('#change-password-form')
            .parsley(global.parsleySettings.settings)
            .on('form:success', function (formInstance) {
                if (formInstance.isValid()) {
                    $('#preloader').show();

                    $.ajax({
                        url: route('user.password.change'),
                        type: 'post',
                        data: $('#change-password-form').serialize(),
                        loadSpinner: true,
                        success: function (response) {
                            $('#preloader').hide();
                            $('#change-password-form').trigger('reset');
                            response.status ? bootstrapNotify.success(response.message, 'Success') : bootstrapNotify.error(response.message, 'Error');
                        },
                        error: function (response) {
                            if (response.status == 422) {
                                console.log(response);
                                global.generateErrorMessage(formInstance, response.responseJSON.errors);
                                // bootstrapNotify.error(response.responseJSON.message, "Error");
                            }
                            $('#preloader').hide();
                        }
                    });
                }
            }).on('form:submit', function () {
            return false;
        });


    })(window, jQuery, _token);
});
