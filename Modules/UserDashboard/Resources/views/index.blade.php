@extends('user.layout.app', ['menu' => 'dashboard'])
@section('css')
    <link href="{{ asset('assets/global/dashboard.css') }}" rel="stylesheet">
    <style>
        .modal .modal-header .close span {
            display: block;
        }
        /* .add_new_followup {
            background-color: #023c64;
            border-color: #99B1C1;
            color: #ffffff;
        } */
        .edit-followup {
            cursor: pointer;
        }
        .delete-followup {
            cursor: pointer;
        }
        .modal-body{
            padding-top: 0px;
        }


        .copy-share button, select#show-date-wise{
            background-color: #023c64;
            color: #99B1C1;
        }
        .copy-share button:hover,.copy-share button:hover i{
            color: #fff;
        }

        .welcome-filter-share{
            background: -webkit-linear-gradient(left, rgba(0, 0, 0, 0.25), rgba(0, 0, 0, 0) 40%, rgba(0, 0, 0, 0) 60%, rgba(0, 0, 0, 0.25)), -webkit-linear-gradient(top, #023c64, #0e171f);
            color: #fff;
        }

       #kt_content .card .card-header{
            background-color: #03385d!important;
        }
        #kt_content .card .card-header h3{
            color: #fff!important;
        }
        #kt_content table#funne-steptr td{
            text-align: center;
        }
        #kt_content  tr td span{
            margin: 5px 0;
        }

        span.text-center.stat_count {
            font-style: inherit;
            font-size: 20px;
            color: #053a6f;
        }


		/* Social Share Start */
		.df__social__share {
			opacity: 0;
			position: absolute;
			top: 100%;
			right: 0;
			transition: all 0.4s ease-in-out;
			visibility: hidden;
			margin-top: 5px;
		}
		.df__social__share.active {
			opacity: 1;
			visibility: visible;
		}
		.df__social__share ul {
			display: -webkit-box;
			display: -ms-flexbox;
			display: -webkit-flex;
			display: flex;
			list-style: none;
		}
		.df__social__share ul a{
			padding: 5px;
			width: 45px;
			height: 45px;
			background-color: #3b5998;
			color: #fff;
			display: inline-flex;
			align-items: center;
			justify-content: center;
		}
		.df__social__share ul a i{
			color: inherit;
			font-size: 18px;
		}
		.df__social__share ul a.twitter{
			background-color: #00acec;
		}
		.df__social__share ul a.li{
			background-color: #0085af;
		}
		.df__social__share ul a.env{
			background-color: #efbe00;
		}


		.btn.shareBtn.has-arrow {
		  position: relative;
		}
		.btn.shareBtn.has-arrow:after {
		  content: '';
		  width: 20px;
		  height: 20px;
		  background: #023c64;
		  display: block;
		  position: absolute;
		  z-index: 1;
		  transform: rotate(45deg);
		  left: 50%;
		  right: ;
		  top: 70%;
		}
		/* Social Share Start */
		
		/* Dashboard Visitor Info Start */
		.df__visitor__info__item .df__item__col{
			background-color: #fff;
			transition: all 0.4s ease-in-out;
		}
		
		.df__visitor__info__item .df__item__col:hover { 
			background: #f5f5f5;
			box-shadow:  5px 5px 10px #b0b0b0,
			             -5px -5px 10px #ffffff;
		}
		.df__visitor__info__item .df__item__col a {
			font-size: 18px !important;
			font-weight: 600 !important;
			color: #03385d  !important;
			margin-top: 8px !important;
			display: block;
		}
		.df__visitor__info__item .df__item__col .start__count__col {
			text-align: right !important;
		}
		.lead__img {
			width: 38px;
			height: auto;
		}
		/* Dashboard Visitor Info End */
    </style>
@endsection
@section('content')
    <section class="welcome-filter-share bg-info bg-info-o-10 bg-diagonal-info px-3 py-5">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-5 col-12">
                    <div class="row align-items-center justify-content-start">
                        <div class="pr-5 py-2 ">
                            <h3 class="mb-0">Welcome {{auth()->user()->first_name}}</h3>
                        </div>
                        <div class="pl-lg-5 py-2">
                            <label for="show-date-wise"> Show data for: </label>
                            <select class="form-control d-inline-block" id="show-date-wise"  style="width: 150px">
                                <option data-value="week" selected>Last 7 days</option>
                                <option data-value="month">Last 30 days</option>
                                <option data-value="forever">Forever</option>

                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-7 col-12">
                   <div class="row d-flex align-items-center justify-content-lg-end">
                       <div class="pr-5 py-2">
                           <div class="copy-url" >
                               <label class="font-weight-bold">{{env('SITE_NAME')}} link:</label>
                               <div class="input-icon d-inline-block" style="width: 300px">
                                   <input type="text" value="{{ url()->current(). '/subscriber/'. $pageId.'/v' }}" readonly id="copy-clipboard" class="form-control d-inline-block"  placeholder="Disabled input">
                                   <span><i class="flaticon2-clip-symbol icon-md"></i></span>
                               </div>
                           </div>
                       </div>
                       <div class="pl-lg-5 py-2">
                           <div class="copy-share">
                               <div class="btn-group" role="group" aria-label="Basic example">

                                   <button type="button" class="btn copy-clipboard-click"> Copy <i class="la la-files-o icon-xl"></i></button>
                                   <button type="button" class="btn shareBtn">Share <i class="fas fa-share-alt"></i></button>

                               </div>
                           </div>
                       </div>

					   <div class="df__social__share">
					   	<ul>
						   	<li>
                                <a href="{{ ViewHelper::getShareUrl("FACEBOOK_SHARE", "https://offleashk9training.yourfunnel.net/subscribe", "Subscriber", "Subscriber") }}"
                                   target="_blank"><i class="la la-facebook"></i></a>
                            </li>
						   	<li>
                                <a class="twitter" href="{{ ViewHelper::getShareUrl("TWITTER_SHARE", "https://offleashk9training.yourfunnel.net/subscribe", "Subscriber", "Subscriber") }}"
                                   target="_blank"><i class="la la-twitter"></i></a>
                            </li>
						   	<li>
                                <a class="li" href="{{ ViewHelper::getShareUrl("LINKEDIN_SHARE", "https://offleashk9training.yourfunnel.net/subscribe", "Subscriber", "Subscriber") }}"
                                   target="_blank"><i class="la la-linkedin"></i></a>
                            </li>
						   	<li>
                                <a class="env" href="{{ ViewHelper::getShareUrl("GMAIL_SHARE", "https://offleashk9training.yourfunnel.net/subscribe", "Subscriber", "Subscriber") }}"
                                   target="_blank"><i class="la la-envelope"></i></a>
                            </li>
						   </ul>
					   </div>
                   </div>
                </div>

            </div>
        </div>
    </section>

    <section class="visitor-info mt-5 pt-5">
        <div class="container">
            <div class="row">

                <div class="col-4 col-sm-4 col-md-2 d-flex align-items-center justify-content-center df__visitor__info__item">

                    <div class="col df__item__col px-6 py-8 rounded-xl mb-7">
                        <div class="row">
                            <div class="col-md-6">
                            <span class="svg-icon svg-icon-3x svg-icon-primary d-block my-2">
                                    <!-- <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                            <path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                            <path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero"></path>
                                        </g>
									</svg> -->
									<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg>
                                </span>

                            </div>
                            <div class="col-md-6 start__count__col">
                                <span class="text-center stat_count" id="totalUniqueVisitors">0</span>
                            </div>
                        </div>
                        <a href="javascript:void(0)" class="text-primary font-weight-bold font-size-h6 mt-2"> Unique Visitors</a>
                    </div>
                </div>

                <div class="col-4   col-sm-4  col-md-2 d-flex align-items-center justify-content-center df__visitor__info__item">
                    <div class="col df__item__col px-6 py-8 rounded-xl mb-7">
                        <div class="row">
                            <div class="col-md-6">
                            <span class="svg-icon svg-icon-3x svg-icon-primary d-block my-2">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                            <path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                            <path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero"></path>
                                        </g>
                                    </svg>
                                </span>


                            </div>
                            <div class="col-md-6 start__count__col">
                                <span class="text-center stat_count" id="totalSubscribers">0</span>
                            </div>
                        </div>
                        <a href="javascript:void(0)" class="text-primary font-weight-bold font-size-h6 mt-2"> Subscribers</a>
                    </div>

                </div>

                <div class="col-4  col-sm-4 col-md-2  d-flex align-items-center justify-content-center df__visitor__info__item">
                    <div class="col df__item__col px-6 py-8 rounded-xl mb-7">
                        <div class="row">
                            <div class="col-md-6">
                        <span class="svg-icon svg-icon-3x svg-icon-primary d-block my-2">
                                <!-- <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                        <path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                        <path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero"></path>
                                    </g>
								</svg> -->
								
								<img class="lead__img " src="assets{{'/images/lead.png'}}" alt="" srcset="">
                            </span>


                            </div>
                            <div class="col-md-6 start__count__col">
                                <span class="text-center stat_count" id="totalLeads">0</span>
                            </div>
                        </div>

                        <a href="javascript:void(0)" class="text-primary font-weight-bold font-size-h6 mt-2"> Leads</a>
                    </div>
                </div>

                <div class="col-4  col-sm-4   col-md-2 d-flex align-items-center justify-content-center df__visitor__info__item">
                    <div class="col df__item__col px-6 py-8 rounded-xl mb-7">
                        <div class="row">
                            <div class="col-md-6">
                                <span class="svg-icon svg-icon-3x svg-icon-primary d-block my-2">
								<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline></svg>
                                </span>


                            </div>
                            <div class="col-md-6 start__count__col">
                                <span class="text-center stat_count" id="totalEmailSent">0</span>
                            </div>
                        </div>

                        <a href="javascript:void(0)" class="text-primary font-weight-bold font-size-h6 mt-2"> Emails Sent</a>
                    </div>
                </div>

                <div class="col-4  col-sm-4  col-md-2 d-flex align-items-center justify-content-center df__visitor__info__item">
                    <div class="col df__item__col px-6 py-8 rounded-xl mb-7">
                        <div class="row">
                            <div class="col-md-6">
                                <span class="svg-icon svg-icon-3x svg-icon-primary d-block my-2">
                                    <!-- <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                            <path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                            <path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero"></path>
                                        </g>
									</svg> -->
									<img class="lead__img " src="assets{{'/images/email-open.png'}}" alt="" srcset="">
                                </span>


                            </div>
                            <div class="col-md-6 start__count__col">
                                <span class="text-center stat_count" id="totalEmailOpened">0</span>
                            </div>
                        </div>

                        <a href="javascript:void(0)" class="text-primary font-weight-bold font-size-h6 mt-2"> Emails Opened</a>
                    </div>
                </div>

                <div class="col-4  col-sm-4  col-md-2 d-flex align-items-center justify-content-center df__visitor__info__item">
                    <div class="col df__item__col px-6 py-8 rounded-xl mb-7">
                        <div class="row">
                            <div class="col-md-6">
                                <span class="svg-icon svg-icon-3x svg-icon-primary d-block my-2">
								<img class="lead__img " src="assets{{'/images/email-clicked.png'}}" alt="" srcset="">
                                </span>


                            </div>
                            <div class="col-md-6 start__count__col">
                                <span class="text-center stat_count" id="totalEmailClicked">0</span>
                            </div>
                        </div>

                        <a href="javascript:void(0)" class="text-primary font-weight-bold font-size-h6 mt-2">Emails Clicked</a>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <div class="content d-flex flex-column flex-column-fluid df__dashboard__wrapper" id="kt_content">
        <div class="d-flex flex-column-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                       @include('userdashboard::funnel.funnel_step');
                    </div>
                    <div class="col-lg-6">
                        <div class="card card-custom card-stretch gutter-b rightBox">
                            <!--begin::Header-->
                            <div class="card-header border-0 py-5">
                                <h3 class="card-title m-0 d-flex align-items-center"> Auto Follow-ups</h3>
                                <div class="card-toolbar my-0">
                                    <a href="javascript:void(0)"class="btn font-weight-bolder df__alt__btn font-size-sm add_new_followup">Add New</a>
                                </div>
                            </div>
                            <!--end::Header-->
                            <!--begin::Body-->
                            <div class="card-body pt-0 pb-3 custom__padding">
                                <div class="tab-content">
                                    <!--begin::Table-->
                                    <div class="table-responsive">
                                        <table
                                            class="table table-head-custom table-head-bg table-borderless table-vertical-center table-striped">
                                            <tbody>
                                            @if(isset($autoFollowupCount[0]))
                                                @foreach($autoFollowupCount as $key=>$followup)

                                                    <tr>
                                                        <td>
                                                            <div class="d-flex align-items-center">
                                                                <div>
                                                                    <a href="#"
                                                                       class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg df__email__title__">{{$followup->email_subject}}</a>
                                                                    <span class="text-muted font-weight-bold d-block">Sent {{$followup->send_day_after_previous_email == 0 ? 'right ' : $followup->cumulative_send_day." days "}} after subscription</span>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td class="df__table__actions">
                                                            <span
                                                                class="text-dark-75 font-weight-bolder d-block font-size-lg edit-followup customBtn"
                                                                data-id="{{$followup->id}}"><i class="la la-pencil"></i>Edit</span>
                                                            <span
                                                                class="text-dark-75 font-weight-bolder d-block font-size-lg delete-followup customBtn"
                                                                data-id="{{$followup->id}}"><i class="la la-trash"></i>Delete</span>
                                                        </td>
                                                        <td>
                                                            <span
                                                                class="text-dark-75 font-weight-bolder d-block font-size-lg"
                                                                title="Number of emails that were sent in the last 7 days"><i
                                                                    class="fa fa-envelope"></i></span>
                                                            <span class="text-muted font-weight-bold"><div id="sentEmail">{{isset($followup->getCount[0]->email_sent) ? $followup->getCount[0]->email_sent : 0}}</div></span>
                                                        </td>
                                                        <td>
                                                            <span
                                                                class="text-dark-75 font-weight-bolder d-block font-size-lg"
                                                                title="Number of emails that were opened in the last 7 days"><i
                                                                    class="fa fa-eye"></i></span>
                                                            <span class="text-muted font-weight-bold"><div id="openedEmail">{{isset($followup->getCount[0]->email_opened) ? $followup->getCount[0]->email_opened : 0}}</div></span>
                                                        </td>
                                                        <td>
                                                            <span
                                                                class="text-dark-75 font-weight-bolder d-block font-size-lg"
                                                                title="Number of emails that resulted in a link click in the last 7 days"><i
                                                                    class="fa fa-paperclip"></i></span>
                                                            <span class="text-muted font-weight-bold"><div id="clickedEmail">{{isset($followup->getCount[0]->email_clicked) ? $followup->getCount[0]->email_clicked : 0}}</div></span>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <div class="no__items__found">
													<img src="{{ asset('assets/images/empty_box.jpg') }}" alt="No Item Found.">
													<p>No Followups Added Yet</p>
													<a href="javascript:void(0)" class="btn btn__add__item add_new_followup">Add New Steps</a>
												</div>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--end::Table-->
                                </div>
                            </div>
                            <!--end::Body-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ mix('/js/overall-stat-count.js') }}"></script>

    <script src="{{ mix('/js/dashboard.js') }}"></script>
		<script>
			$(document).ready(function(){
				$('.shareBtn').on('click', function(){
					$(this).addClass('has-arrow');
					$('.df__social__share').addClass('active');
				});
			})
		</script>

@endsection

