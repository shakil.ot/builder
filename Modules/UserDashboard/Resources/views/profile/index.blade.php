@extends('user.layout.app', ['menu' => 'profile'])
@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Card-->
                <div class="d-flex flex-row" data-sticky-container="">
                    <!--begin::Aside-->
                    <div class="flex-row-auto offcanvas-mobile w-300px w-xl-350px" id="kt_profile_aside">
                        <!--begin::Card-->
                        <div class="card card-custom sticky" data-sticky="true" data-margin-top="140px" data-sticky-for="1023" data-sticky-class="kt-sticky">
                            <!--begin::Body-->
                            <div class="card-body pt-5">
                                <!--begin::User-->
                                <div class="d-flex align-items-center mt-5">
                                    <div class="symbol symbol-100 mr-5">
                                        <div class="symbol-label">
                                            <img style="border-radius: 5px; width: 100%" src="{{ !empty(auth()->user()->profile_image) ? auth()->user()->profile_image : asset('assets/theme/media/users/blank.png') }}" alt="">
                                        </div>
                                        <i class="symbol-badge bg-success"></i>
                                    </div>
                                    <div class="d-flex flex-column w-100">
                                        <a href="#" class="font-weight-bold font-size-h5 text-dark-75 text-hover-primary">{{ $user->first_name.' '.$user->last_name }}</a>
                                        <div class="navi mt-2">
                                            <a href="mailto:{{ $user->email }}" class="navi-item">
                                                <span class="navi-link p-0 pb-2">
                                                    <span class="navi-icon mr-1">
                                                        <span class="svg-icon svg-icon-lg svg-icon-primary">
                                                            <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Mail-notification.svg-->
                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                                    <path d="M21,12.0829584 C20.6747915,12.0283988 20.3407122,12 20,12 C16.6862915,12 14,14.6862915 14,18 C14,18.3407122 14.0283988,18.6747915 14.0829584,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,8 C3,6.8954305 3.8954305,6 5,6 L19,6 C20.1045695,6 21,6.8954305 21,8 L21,12.0829584 Z M18.1444251,7.83964668 L12,11.1481833 L5.85557487,7.83964668 C5.4908718,7.6432681 5.03602525,7.77972206 4.83964668,8.14442513 C4.6432681,8.5091282 4.77972206,8.96397475 5.14442513,9.16035332 L11.6444251,12.6603533 C11.8664074,12.7798822 12.1335926,12.7798822 12.3555749,12.6603533 L18.8555749,9.16035332 C19.2202779,8.96397475 19.3567319,8.5091282 19.1603533,8.14442513 C18.9639747,7.77972206 18.5091282,7.6432681 18.1444251,7.83964668 Z" fill="#000000"></path>
                                                                    <circle fill="#000000" opacity="0.3" cx="19.5" cy="17.5" r="2.5"></circle>
                                                                </g>
                                                            </svg>
                                                            <!--end::Svg Icon-->
                                                        </span>
                                                    </span>
                                                    <span class="navi-text text-muted text-hover-green">{{ $user->email }}</span>
                                                </span>
                                            </a>
                                        </div>
                                        <div class="navi">
                                            <a href="tel:{{ $user->phone }}" class="navi-item">
                                                <span class="navi-link p-0 pb-2">
                                                    <span class="navi-icon mr-1">
                                                        <span class="svg-icon svg-icon-lg svg-icon-primary">
                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                    <rect x="0" y="0" width="24" height="24"/>
                                                                    <path d="M13.0799676,14.7839934 L15.2839934,12.5799676 C15.8927139,11.9712471 16.0436229,11.0413042 15.6586342,10.2713269 L15.5337539,10.0215663 C15.1487653,9.25158901 15.2996742,8.3216461 15.9083948,7.71292558 L18.6411989,4.98012149 C18.836461,4.78485934 19.1530435,4.78485934 19.3483056,4.98012149 C19.3863063,5.01812215 19.4179321,5.06200062 19.4419658,5.11006808 L20.5459415,7.31801948 C21.3904962,9.0071287 21.0594452,11.0471565 19.7240871,12.3825146 L13.7252616,18.3813401 C12.2717221,19.8348796 10.1217008,20.3424308 8.17157288,19.6923882 L5.75709327,18.8875616 C5.49512161,18.8002377 5.35354162,18.5170777 5.4408655,18.2551061 C5.46541191,18.1814669 5.50676633,18.114554 5.56165376,18.0596666 L8.21292558,15.4083948 C8.8216461,14.7996742 9.75158901,14.6487653 10.5215663,15.0337539 L10.7713269,15.1586342 C11.5413042,15.5436229 12.4712471,15.3927139 13.0799676,14.7839934 Z" fill="#000000"/>
                                                                    <path d="M14.1480759,6.00715131 L13.9566988,7.99797396 C12.4781389,7.8558405 11.0097207,8.36895892 9.93933983,9.43933983 C8.8724631,10.5062166 8.35911588,11.9685602 8.49664195,13.4426352 L6.50528978,13.6284215 C6.31304559,11.5678496 7.03283934,9.51741319 8.52512627,8.02512627 C10.0223249,6.52792766 12.0812426,5.80846733 14.1480759,6.00715131 Z M14.4980938,2.02230302 L14.313049,4.01372424 C11.6618299,3.76737046 9.03000738,4.69181803 7.1109127,6.6109127 C5.19447112,8.52735429 4.26985715,11.1545872 4.51274152,13.802405 L2.52110319,13.985098 C2.22450978,10.7517681 3.35562581,7.53777247 5.69669914,5.19669914 C8.04101739,2.85238089 11.2606138,1.72147333 14.4980938,2.02230302 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                                                </g>
                                                            </svg><!--end::Svg Icon-->
                                                        </span>
                                                    </span>
                                                    <span class="navi-text text-muted text-hover-green">{{ \App\Services\Utility::format_telephone($user->phone) }}</span>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <!--end::User-->

                                <div class="separator separator-dashed mt-8 mb-5"></div>

                                <!--begin::Nav-->
                                <ul class="nav">
                                    <li class="nav-item w-100 text-center">
                                        <a class="btn btn-hover-light-green font-weight-bold py-3 px-6 mb-2 w-100 active" data-toggle="tab" href="#personal_info">
                                            <span class="svg-icon svg-icon-green">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <path d="M6,2 L18,2 C19.6568542,2 21,3.34314575 21,5 L21,19 C21,20.6568542 19.6568542,22 18,22 L6,22 C4.34314575,22 3,20.6568542 3,19 L3,5 C3,3.34314575 4.34314575,2 6,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                            Personal Information
                                        </a>
                                    </li>
                                    <li class="nav-item w-100 text-center">
                                        <a class="btn btn-hover-light-green font-weight-bold py-3 px-6 w-100 mb-2" data-toggle="tab" href="#password_change">
                                            <span class="svg-icon svg-icon-green">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <path d="M7.38979581,2.8349582 C8.65216735,2.29743306 10.0413491,2 11.5,2 C17.2989899,2 22,6.70101013 22,12.5 C22,18.2989899 17.2989899,23 11.5,23 C5.70101013,23 1,18.2989899 1,12.5 C1,11.5151324 1.13559454,10.5619345 1.38913364,9.65805651 L3.31481075,10.1982117 C3.10672013,10.940064 3,11.7119264 3,12.5 C3,17.1944204 6.80557963,21 11.5,21 C16.1944204,21 20,17.1944204 20,12.5 C20,7.80557963 16.1944204,4 11.5,4 C10.54876,4 9.62236069,4.15592757 8.74872191,4.45446326 L9.93948308,5.87355717 C10.0088058,5.95617272 10.0495583,6.05898805 10.05566,6.16666224 C10.0712834,6.4423623 9.86044965,6.67852665 9.5847496,6.69415008 L4.71777931,6.96995273 C4.66931162,6.97269931 4.62070229,6.96837279 4.57348157,6.95710938 C4.30487471,6.89303938 4.13906482,6.62335149 4.20313482,6.35474463 L5.33163823,1.62361064 C5.35654118,1.51920756 5.41437908,1.4255891 5.49660017,1.35659741 C5.7081375,1.17909652 6.0235153,1.2066885 6.2010162,1.41822583 L7.38979581,2.8349582 Z" fill="#000000" opacity="0.3"></path>
                                                        <path d="M14.5,11 C15.0522847,11 15.5,11.4477153 15.5,12 L15.5,15 C15.5,15.5522847 15.0522847,16 14.5,16 L9.5,16 C8.94771525,16 8.5,15.5522847 8.5,15 L8.5,12 C8.5,11.4477153 8.94771525,11 9.5,11 L9.5,10.5 C9.5,9.11928813 10.6192881,8 12,8 C13.3807119,8 14.5,9.11928813 14.5,10.5 L14.5,11 Z M12,9 C11.1715729,9 10.5,9.67157288 10.5,10.5 L10.5,11 L13.5,11 L13.5,10.5 C13.5,9.67157288 12.8284271,9 12,9 Z" fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                            Change Password
                                        </a>
                                    </li>
                                </ul>
                                <!--end::Nav-->
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::Card-->
                    </div>
                    <!--end::Aside-->
                    <!--begin::Content-->
                    <div class="flex-row-fluid ml-lg-8">
                        <!--begin::Row-->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card card-custom">
                                    <div class="card-body">
                                        <div class="tab-content mt-5" id="myTabContent">
                                            <div class="tab-pane fade show active" id="personal_info" role="tabpanel" aria-labelledby="kt_tab_pane_2">
                                                <form _lpchecked="1" id="update-profile-form" autocomplete="off">
                                                    @csrf
                                                    <input type="hidden" name="user_id" value="{{ $user->id }}">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <h3 class="font-size-lg text-dark-75 font-weight-bold">
                                                                        <span class="svg-icon svg-icon-green svg-icon-2x">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                                                    <path d="M6,2 L18,2 C19.6568542,2 21,3.34314575 21,5 L21,19 C21,20.6568542 19.6568542,22 18,22 L6,22 C4.34314575,22 3,20.6568542 3,19 L3,5 C3,3.34314575 4.34314575,2 6,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000"></path>
                                                                                </g>
                                                                            </svg>
                                                                        </span>
                                                                        Profile Details
                                                                    </h3>
                                                                    <hr>
                                                                    <div class="form-group">
                                                                        <label for="first_name" class="col-form-label">First Name</label>
                                                                        <input class="form-control" type="text" name="first_name" value="{{ $user->first_name }}" placeholder="Enter First Name" required>
                                                                        <div class="custom-error-message"></div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="last_name" class="col-form-label">Last Name</label>
                                                                        <input class="form-control" type="text" name="last_name" value="{{ $user->last_name }}" placeholder="Enter Last Name" required>
                                                                        <div class="custom-error-message"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <h3 class="font-size-lg text-dark-75 font-weight-bold">
                                                                        <span class="svg-icon svg-icon-green svg-icon-2x">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                                    <rect x="0" y="0" width="24" height="24"/>
                                                                                    <path d="M5,7 L19,7 C20.1045695,7 21,7.8954305 21,9 L21,17 C21,18.1045695 20.1045695,19 19,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,9 C3,7.8954305 3.8954305,7 5,7 Z M12,17 C14.209139,17 16,15.209139 16,13 C16,10.790861 14.209139,9 12,9 C9.790861,9 8,10.790861 8,13 C8,15.209139 9.790861,17 12,17 Z" fill="#000000"/>
                                                                                    <rect fill="#000000" opacity="0.3" x="9" y="4" width="6" height="2" rx="1"/>
                                                                                    <circle fill="#000000" opacity="0.3" cx="12" cy="13" r="2"/>
                                                                                </g>
                                                                            </svg>
                                                                        </span>
                                                                        Update Profile Photo
                                                                    </h3>
                                                                    <hr>
                                                                    <div class="form-group row">
                                                                        <label class="col-xl-3 col-lg-3 col-form-label text-right"></label>
                                                                        <div class="col-lg-9 col-xl-6 text-center">
                                                                            <div class="image-input image-input-outline" id="profile_image">
                                                                                <div class="image-input-wrapper" style="background-image: url({{ !empty(auth()->user()->profile_image) ? auth()->user()->profile_image : asset('assets/theme/media/users/blank.png') }});"></div>
                                                                                <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                                                                                    <i class="fa fa-pen icon-sm text-muted"></i>
                                                                                    <input type="file" id="image" name="profile_image" accept=".png, .jpg, .jpeg">
                                                                                </label>
                                                                            </div>
                                                                            <span class="form-text text-muted">Allowed file types: png, jpg, jpeg.</span>
                                                                        </div>
                                                                        <div class="custom-error-message"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label for="last_name" class="col-form-label">Email</label>
                                                                    <input class="form-control" type="text" name="email" value="{{ $user->email }}" placeholder="Enter Email Address">
                                                                    <div class="custom-error-message"></div>
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <label for="last_name" class="col-form-label">Phone</label>
                                                                    <input class="form-control" type="text" name="phone" value="{{ $user->phone }}" placeholder="Enter Phone No.">
                                                                    <div class="custom-error-message"></div>
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <label for="address" class="col-form-label">Address</label>
                                                                    <input class="form-control" type="text" name="address" value="{{ $user->address }}" placeholder="Enter Address">
                                                                    <div class="custom-error-message"></div>
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <label for="state" class="col-form-label">State</label>
                                                                    <input class="form-control" type="text" name="state" value="{{ $user->state }}" placeholder="Enter State">
                                                                    <div class="custom-error-message"></div>
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <label for="city" class="col-form-label">City</label>
                                                                    <input class="form-control" type="text" name="city" value="{{ $user->city }}" placeholder="Enter City">
                                                                    <div class="custom-error-message"></div>
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <label for="zip" class="col-form-label">Zip Code</label>
                                                                    <input class="form-control" type="text" name="zip_code" value="{{ $user->zip_code }}" placeholder="Enter Zip Code">
                                                                    <div class="custom-error-message"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group mt-10">
                                                        <button type="submit" class="btn btn-green font-weight-bold">
                                                            <span class="svg-icon svg-icon-white">
                                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                                        <path d="M6,2 L18,2 C19.6568542,2 21,3.34314575 21,5 L21,19 C21,20.6568542 19.6568542,22 18,22 L6,22 C4.34314575,22 3,20.6568542 3,19 L3,5 C3,3.34314575 4.34314575,2 6,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000"></path>
                                                                    </g>
                                                                </svg>
                                                            </span>
                                                            Update Profile
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="tab-pane fade" id="password_change" role="tabpanel" aria-labelledby="kt_tab_pane_2">
                                                <form _lpchecked="1" id="change-password-form" autocomplete="off">
                                                    @csrf
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h3 class="font-size-lg text-dark-75 font-weight-bold">
                                                                <span class="svg-icon svg-icon-green svg-icon-2x">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                            <rect x="0" y="0" width="24" height="24"/>
                                                                            <path d="M7.38979581,2.8349582 C8.65216735,2.29743306 10.0413491,2 11.5,2 C17.2989899,2 22,6.70101013 22,12.5 C22,18.2989899 17.2989899,23 11.5,23 C5.70101013,23 1,18.2989899 1,12.5 C1,11.5151324 1.13559454,10.5619345 1.38913364,9.65805651 L3.31481075,10.1982117 C3.10672013,10.940064 3,11.7119264 3,12.5 C3,17.1944204 6.80557963,21 11.5,21 C16.1944204,21 20,17.1944204 20,12.5 C20,7.80557963 16.1944204,4 11.5,4 C10.54876,4 9.62236069,4.15592757 8.74872191,4.45446326 L9.93948308,5.87355717 C10.0088058,5.95617272 10.0495583,6.05898805 10.05566,6.16666224 C10.0712834,6.4423623 9.86044965,6.67852665 9.5847496,6.69415008 L4.71777931,6.96995273 C4.66931162,6.97269931 4.62070229,6.96837279 4.57348157,6.95710938 C4.30487471,6.89303938 4.13906482,6.62335149 4.20313482,6.35474463 L5.33163823,1.62361064 C5.35654118,1.51920756 5.41437908,1.4255891 5.49660017,1.35659741 C5.7081375,1.17909652 6.0235153,1.2066885 6.2010162,1.41822583 L7.38979581,2.8349582 Z" fill="#000000" opacity="0.3"/>
                                                                            <path d="M14.5,11 C15.0522847,11 15.5,11.4477153 15.5,12 L15.5,15 C15.5,15.5522847 15.0522847,16 14.5,16 L9.5,16 C8.94771525,16 8.5,15.5522847 8.5,15 L8.5,12 C8.5,11.4477153 8.94771525,11 9.5,11 L9.5,10.5 C9.5,9.11928813 10.6192881,8 12,8 C13.3807119,8 14.5,9.11928813 14.5,10.5 L14.5,11 Z M12,9 C11.1715729,9 10.5,9.67157288 10.5,10.5 L10.5,11 L13.5,11 L13.5,10.5 C13.5,9.67157288 12.8284271,9 12,9 Z" fill="#000000"/>
                                                                        </g>
                                                                    </svg>
                                                                </span>
                                                                Change Password
                                                            </h3>
                                                            <hr>
{{--                                                            <div class="form-group">--}}
{{--                                                                <label for="old_password" class="col-form-label">Old Password</label>--}}
{{--                                                                <input class="form-control" id="old_password" type="password" name="old_password" placeholder="Enter Old Password" required>--}}
{{--                                                                <div class="custom-error-message"></div>--}}
{{--                                                            </div>--}}
                                                            <div class="form-group">
                                                                <label for="new_password" class="col-form-label">New Password</label>
                                                                <input class="form-control" type="password" id="new_password" name="new_password" placeholder="Enter New Password" required>
                                                                <div class="custom-error-message"></div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="confirm_password" class="col-form-label">Confirm Password</label>
                                                                <input class="form-control" type="password" id="confirm_password" name="confirm_password" placeholder="Confirm New Password" data-parsley-equalto="#new_password" required>
                                                                <div class="custom-error-message"></div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group mt-10">
                                                        <button type="submit" class="btn btn-green font-weight-bold">
                                                            <span class="svg-icon svg-icon-white">
                                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                        <rect x="0" y="0" width="24" height="24"/>
                                                                        <path d="M7.38979581,2.8349582 C8.65216735,2.29743306 10.0413491,2 11.5,2 C17.2989899,2 22,6.70101013 22,12.5 C22,18.2989899 17.2989899,23 11.5,23 C5.70101013,23 1,18.2989899 1,12.5 C1,11.5151324 1.13559454,10.5619345 1.38913364,9.65805651 L3.31481075,10.1982117 C3.10672013,10.940064 3,11.7119264 3,12.5 C3,17.1944204 6.80557963,21 11.5,21 C16.1944204,21 20,17.1944204 20,12.5 C20,7.80557963 16.1944204,4 11.5,4 C10.54876,4 9.62236069,4.15592757 8.74872191,4.45446326 L9.93948308,5.87355717 C10.0088058,5.95617272 10.0495583,6.05898805 10.05566,6.16666224 C10.0712834,6.4423623 9.86044965,6.67852665 9.5847496,6.69415008 L4.71777931,6.96995273 C4.66931162,6.97269931 4.62070229,6.96837279 4.57348157,6.95710938 C4.30487471,6.89303938 4.13906482,6.62335149 4.20313482,6.35474463 L5.33163823,1.62361064 C5.35654118,1.51920756 5.41437908,1.4255891 5.49660017,1.35659741 C5.7081375,1.17909652 6.0235153,1.2066885 6.2010162,1.41822583 L7.38979581,2.8349582 Z" fill="#000000" opacity="0.3"/>
                                                                        <path d="M14.5,11 C15.0522847,11 15.5,11.4477153 15.5,12 L15.5,15 C15.5,15.5522847 15.0522847,16 14.5,16 L9.5,16 C8.94771525,16 8.5,15.5522847 8.5,15 L8.5,12 C8.5,11.4477153 8.94771525,11 9.5,11 L9.5,10.5 C9.5,9.11928813 10.6192881,8 12,8 C13.3807119,8 14.5,9.11928813 14.5,10.5 L14.5,11 Z M12,9 C11.1715729,9 10.5,9.67157288 10.5,10.5 L10.5,11 L13.5,11 L13.5,10.5 C13.5,9.67157288 12.8284271,9 12,9 Z" fill="#000000"/>
                                                                    </g>
                                                                </svg>
                                                            </span>
                                                            Change Password
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Row-->
                    </div>
                    <!--end::Content-->
                </div>
                <!--end::Card-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
@endsection

@section('script')
    <script src="{{ asset('js/user-profile.js') }}"></script>
@endsection
