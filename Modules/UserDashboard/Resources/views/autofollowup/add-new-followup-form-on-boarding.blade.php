@foreach($emailTemplates as $emailTemplate)

    <div class="d-flex align-items-center bg-light-success rounded p-3 mb-4" style="background: #84848459 !important;">
        <!--begin::Icon-->
        <span class="svg-icon svg-icon-success mr-5">
        <span class="svg-icon svg-icon-lg">
            <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Write.svg-->
           <i class="fa fa-envelope-open-text text-primary mr-5"></i>
            <!--end::Svg Icon-->
        </span>
    </span>
        <!--end::Icon-->
        <!--begin::Title-->
        <div class="d-flex flex-column flex-grow-1 mr-2">
            <a href="#" class="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1">{{ $emailTemplate->title  }}</a>
            <span class="text-muted font-weight-bold"></span>
        </div>
        <!--end::Title-->
        <!--begin::Lable-->
        <span class="font-weight-bolder text-success py-1 font-size-lg">


            <span class="btn btn-light-warning btn-sm mr-3 float-right view-emailTemplate-new " data-id="{{$emailTemplate->id}}" >
              <i class="far fa-eye"></i>  view
            </span>

             <span class="btn btn-light-primary btn-sm mr-3 float-right {{ $emailTemplatesId == $emailTemplate->id ? 'email-template-change-selected' : 'email-template-change' }}"
                   data-id="{{$emailTemplate->id}}"
                   data-key="{{ $key }}"
                   data-title="{{ $emailTemplate->title }}"
             >  {!!  $emailTemplatesId == $emailTemplate->id ? '<i class="fa fa-check"></i> Selected' : 'Select' !!}
            </span>


        </span>
        <!--end::Lable-->
    </div>





{{--    <div class="dropdown-item {{ $emailTemplatesId == $emailTemplate->id  ? 'bg-danger text-white' : 'bg-secondary text-black' }}  mb-2 p-2"--}}
{{--         style="cursor: context-menu;display: flex;justify-content: space-between;align-items: center;"--}}
{{--    >{{ $emailTemplate->title }}--}}
{{--        <button class="btn btn-sm btn-primary float-right {{ $emailTemplatesId == $emailTemplate->id ? 'email-template-change-selected' : 'email-template-change' }}"--}}
{{--                data-id="{{$emailTemplate->id}}"--}}
{{--                data-key="{{ $key }}"--}}
{{--                data-title="{{ $emailTemplate->title }}"--}}
{{--        >  {!!  $emailTemplatesId == $emailTemplate->id ? 'Selected <i class="fa fa-check"></i>' : 'Select' !!}--}}
{{--        </button>--}}
{{--    </div>--}}
@endforeach



