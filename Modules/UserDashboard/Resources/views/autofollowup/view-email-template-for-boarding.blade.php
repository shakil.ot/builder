<div class="row">
    <div class="col-md-12">
        <div class="card card-custom bgi-no-repeat card-stretch gutter-b pt-0">
            <!--begin::Body-->
            <div class="card-body pt-0">
                <div class="font-weight-bold text-black-50 mt-9 mb-5"> <b class="text-black-50"> Subject : </b> {!! $emailTemplates->subject !!}</div>
               <b class="text-black-50">Email Body</b> {!! $emailTemplates->body !!}
            </div>
            <!--end::Body-->
        </div>
    </div>
</div>

