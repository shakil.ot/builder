<div class="card card-custom card-stretch gutter-b leftBox">
    <!--begin::Header-->
    <div class="card-header border-0 py-5">
        <h3 class="card-title m-0 d-flex align-items-center">Funnel Steps</h3>
    </div>
    <!--end::Header-->
    <!--begin::Body-->
    <div class="card-body pt-0 pb-3 px-0">
        <div class="tab-content">
            <!--begin::Table-->
            <div class="table-responsive">
                <table class="table table-head-custom table-head-bg table-borderless table-vertical-center table-striped funnel__steps__wrapper">
                    <tbody>
                    @php
                        $stepCount = 1;
                    @endphp
                    @foreach(\Modules\WebBuilder\Entities\WebBuilder::FUNNEL_STEPS as $key => $step)
                        <tr>
                            <td>
                                <div class="d-flex align-items-center">
                                    <div class="symbol symbol-50 symbol-light mr-4">
                                    <span class="symbol-label">
                                        <img src="https://s3.amazonaws.com/salespype/mapyoursales/admin/postcard/front_image/6KA111599712740.jpg"
                                             class="h-75 align-self-end" alt="">
                                    </span>
                                    </div>
                                    <div class="title__note">
                                        <a href="#" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg funnel__item__title">{{ $step }} </a>
                                        <span class="text-muted font-weight-bold d-block funnel__item__step">Step #{{ $stepCount++ }}</span>
                                    </div>
                                </div>
                            </td>

                            <td class="df__table__actions"> @if(\Modules\WebBuilder\Entities\WebBuilder::WEB_BUILDER_TYPE_AUTO_FOLLOWUPS != $key)
                                    <a class="text-dark-75 font-weight-bolder d-block font-size-lg" href="{{ route('template-view-for-edit', [encrypt($key)]) }}">
                                    <i class="la la-pencil"></i>Edit</a>
                                    <a class="text-dark-75 font-weight-bolder d-block font-size-lg" target="_blank" href="{{ route('template-view', [encrypt($key)]) }}">
                                    <i class="la la-eye"></i>View</a>
                                @endif
                            </td>

                            <td>
                                @if(\Modules\WebBuilder\Entities\WebBuilder::WEB_BUILDER_TYPE_AUTO_FOLLOWUPS != $key)
                                    <span class="text-dark-75 font-weight-bolder d-block font-size-lg"
                                          title="Number of people that visited this page in the last 7 days">
                                <i class="la la-users"></i>
                            </span>
                                    <span class="text-muted font-weight-bold" id="{{$key."_funnel"}}">4</span>
                                @endif
                            </td>
                            <td>
                                @if(\Modules\WebBuilder\Entities\WebBuilder::WEB_BUILDER_TYPE_AUTO_FOLLOWUPS != $key)
                                    <span class="text-dark-75 font-weight-bolder d-block font-size-lg"
                                          title="Number of people that submitted a form on this page in the last 7 days">
                                <i class="fa fa-paper-plane"></i>
                            </span>
                                    <span class="text-muted font-weight-bold" id="{{$key."_funnel2"}}">0</span>
                                @endif
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
            <!--end::Table-->
        </div>
    </div>
    <!--end::Body-->
</div>
