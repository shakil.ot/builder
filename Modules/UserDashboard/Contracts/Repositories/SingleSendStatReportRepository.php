<?php

namespace Modules\UserDashboard\Contracts\Repositories;

interface SingleSendStatReportRepository
{
    public function getTotalEmailByDurationAndParam($userId, $durationType, $fromDate, $toDate, $param);

    public function getDataByWhere($where);

    public function updateOrCreateByParam($where, $data);

    public function getAutoFollowupCount($userId);

    public function incrementByWhereAndFieldName($where, $fieldName);

    public function deleteByWhere($where);


}