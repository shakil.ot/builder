<?php

namespace Modules\UserDashboard\Contracts\Repositories;

interface AutoFollowupRepository
{
    public function addAutoFollowupByParam($data);

    public function getLastAddedFollowupByUserId($userId);

    public function getFollowupsByUserId($userId);

    public function getFollowupById($followupId, $select = '*');

    public function updateFollowupByParam($where, $data);

    public function getAllFollowupsNewerThanThis($followupId);

    public function deleteFollowupById($followupId);

    public function getFirstFollowupMessageByUserId($userId);

    public function getNextDayFollowupMessageByUserIdAndDay($userId, $day);

}
