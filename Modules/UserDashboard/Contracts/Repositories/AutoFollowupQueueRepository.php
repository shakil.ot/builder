<?php

namespace Modules\UserDashboard\Contracts\Repositories;

interface AutoFollowupQueueRepository
{

    /*****## CHECK DATA EXITS OR NOT BY CONTACT ID  ##*****/
    public function checkDataExitsByContactId($contactId);

    /*****## INSERT AUTO-FOLLOWUP MESSAGE  ##*****/
    public function addAutoFollowupMessage($data);

    public function getAllDataByDateIsNow();

    public function getFollowupByUserIdAndContactId($userId, $contactId);

    public function deleteQueuesByContactByCondition($where);

    public function deleteById($id);

    public function getAllFollowupByUserId($userId);


}
