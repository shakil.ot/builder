<?php

namespace Modules\UserDashboard\Contracts\Repositories;

interface OverallStatRepository
{
    public function getTotalUniqueVisitorsByDurationType($userId, $durationType, $fromDate, $toDate);

    public function updateOrCreateByParam($where, $data);

    public function getDashboardFunnelStepsCountByUserId($userId, $params);

    public function insertOverallStatisticsData($request, $userId);

    public function updateTotalCount($dataArray, $request, $key);

    public function checkDataIsExitsByDate($userId, $activityDate);
}
