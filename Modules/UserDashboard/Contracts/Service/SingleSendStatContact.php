<?php

namespace Modules\UserDashboard\Contracts\Service;

interface SingleSendStatContact
{
    public function getTotalEmailByDurationAndParam($userId, $durationType, $fromDate, $toDate, $params);

    public function getDataByWhere($where);

    public function updateOrCreateByParam($where, $data);

    public function getAutoFollowupCount($userId);

    public function incrementByWhereAndFieldName($where, $fieldName);

}
