<?php

namespace Modules\UserDashboard\Contracts\Service;

interface AutoFollowupContact
{
    public function addAutoFollowup($request);

    public function updateFollowup($request);

    public function deleteFollowup($request);

    public function getFollowupsByUserId($userId);

    public function getFollowupById($followupId, $select='*');

    public function addFirstAutoFollowupMessageInCampaignQueueForNewSubscriber($contactId, $userId);

    public function sendAutoFollowupMessageToSubscriber();

    public function nextFollowupDelete($request);
}
