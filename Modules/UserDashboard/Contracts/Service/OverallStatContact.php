<?php

namespace Modules\UserDashboard\Contracts\Service;

interface OverallStatContact
{
    public function getDashboardDurationwiseCount($request);

    public function getDashboardFunnelStepsCount($request);
}
