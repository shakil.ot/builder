<?php

namespace Modules\UserDashboard\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Modules\UserDashboard\Contracts\Repositories\AutoFollowupQueueRepository;
use Modules\UserDashboard\Contracts\Repositories\AutoFollowupRepository;
use Modules\UserDashboard\Contracts\Repositories\OverallStatRepository;
use Modules\UserDashboard\Contracts\Repositories\SingleSendStatReportRepository;
use Modules\UserDashboard\Contracts\Service\AutoFollowupContact;
use Modules\UserDashboard\Contracts\Service\OverallStatContact;
use Modules\UserDashboard\Contracts\Service\SingleSendStatContact;
use Modules\UserDashboard\Http\Repositories\AutoFollowupQueueRepositoryEloquent;
use Modules\UserDashboard\Http\Repositories\AutoFollowupRepositoryEloquent;
use Modules\UserDashboard\Http\Repositories\OverallStatRepositoryEloquent;
use Modules\UserDashboard\Http\Repositories\SingleSendStatReportRepositoryEloquent;
use Modules\UserDashboard\Http\Services\AutoFollowupService;
use Modules\UserDashboard\Http\Services\OverallStatService;
use Modules\UserDashboard\Http\Services\SingleSendStatService;

class DashboardServiceProvider extends ServiceProvider
{
    /**
     * @var string $moduleName
     */
    protected $moduleName = 'UserDashboard';

    /**
     * @var string $moduleNameLower
     */
    protected $moduleNameLower = 'userdashboard';

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(module_path($this->moduleName, 'Database/Migrations'));
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
        $this->app->bind(AutoFollowupContact::class, AutoFollowupService::class);
        $this->app->bind(AutoFollowupRepository::class, AutoFollowupRepositoryEloquent::class);
        $this->app->bind(AutoFollowupQueueRepository::class, AutoFollowupQueueRepositoryEloquent::class);
        $this->app->bind(OverallStatContact::class, OverallStatService::class);
        $this->app->bind(OverallStatRepository::class, OverallStatRepositoryEloquent::class);
        $this->app->bind(SingleSendStatContact::class, SingleSendStatService::class);
        $this->app->bind(SingleSendStatReportRepository::class, SingleSendStatReportRepositoryEloquent::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path($this->moduleName, 'Config/config.php') => config_path($this->moduleNameLower . '.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path($this->moduleName, 'Config/config.php'), $this->moduleNameLower
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/' . $this->moduleNameLower);

        $sourcePath = module_path($this->moduleName, 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], ['views', $this->moduleNameLower . '-module-views']);

        $this->loadViewsFrom(array_merge($this->getPublishableViewPaths(), [$sourcePath]), $this->moduleNameLower);
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/' . $this->moduleNameLower);

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, $this->moduleNameLower);
        } else {
            $this->loadTranslationsFrom(module_path($this->moduleName, 'Resources/lang'), $this->moduleNameLower);
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path($this->moduleName, 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function getPublishableViewPaths(): array
    {
        $paths = [];
        foreach (\Config::get('view.paths') as $path) {
            if (is_dir($path . '/modules/' . $this->moduleNameLower)) {
                $paths[] = $path . '/modules/' . $this->moduleNameLower;
            }
        }
        return $paths;
    }
}
