<?php

namespace Modules\UserDashboard\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = array(
            'first_name' => 'required',
            'last_name' => 'required',
            'profile_image' => 'image|mimes:jpeg,png,jpg',
            'phone' => 'max:20|unique:users,phone,'.$this->user_id
        );
        return $rules;
    }
    public function messages()
    {
        return[
            'phone.unique' => 'Phone Number Already Taken!'
        ];
    }
}
