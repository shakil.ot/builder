<?php

namespace Modules\UserDashboard\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Modules\AdminEmailTemplate\Contracts\Service\EmailTemplateContact;
use Modules\UserDashboard\Contracts\Service\AutoFollowupContact;
use Response;
use Illuminate\Routing\Controller;

class AutoFollowupController extends Controller
{
    /**
     * @var AutoFollowupContact
     */
    private $autoFollowupService;
    private $emailTemplateService;

    public function __construct(AutoFollowupContact $autoFollowupService,
                                EmailTemplateContact $emailTemplateService)
    {
        $this->autoFollowupService = $autoFollowupService;
        $this->emailTemplateService = $emailTemplateService;
    }

    public function addNewFollowupForm()
    {
        $emailTemplates = $this->emailTemplateService->getAllEmailTemplate();
        return response()->json([
            'html' => view('userdashboard::autofollowup.add-new-followup-form')->with([
                'emailTemplates' => $emailTemplates
            ])->render(),
            'status' => 'success'
        ]);
    }

    public function followupFormSubmit(Request $request)
    {
        $request['userId'] = auth()->user()->id;
        return response()->json($this->autoFollowupService->addAutoFollowup($request));
    }

    public function followupEditForm(Request $request)
    {
        $followupId = $request->get('followupId');
        $followup = $this->autoFollowupService->getFollowupById($followupId);
        $emailTemplates = $this->emailTemplateService->getAllEmailTemplate();
        return response()->json([
            'html' => view('userdashboard::autofollowup.edit-followup-form')->with([
                'followup' => $followup,
                'followupId' => $followupId,
                'emailTemplates' => $emailTemplates
            ])->render(),
            'status' => 'success'
        ]);
    }

    public function editFollowupFormSubmit(Request $request)
    {
        $request['userId'] = auth()->user()->id;
        return response()->json($this->autoFollowupService->updateFollowup($request));
    }

    public function deleteFollowup(Request $request)
    {
        $request['userId'] = auth()->user()->id;
        return response()->json($this->autoFollowupService->deleteFollowup($request));
    }

    public function getEmailTemplateById(Request $request)
    {
        $emailTemplateId = $request->get('id');
        $response = $this->emailTemplateService->getTemplateById($emailTemplateId);
        return response()->json($response);
    }


    public function addNewFollowupFormOnBoarding(Request $request)
    {

        $emailTemplates = $this->emailTemplateService->getAllEmailTemplate();

        return response()->json([
            'html' => view('userdashboard::autofollowup.add-new-followup-form-on-boarding')->with([
                'emailTemplates' => $emailTemplates,
                'emailTemplatesId' => $request['id'],
                'key' => $request['keyValue'],
            ])->render(),
            'status' => 'success'
        ]);
    }

    public function addInOnboarding(Request $request)
    {
        $request['userId'] = auth()->user()->id;
        foreach ($request['followUpIdsArray'] as $key => $value) {
            $result = $this->emailTemplateService->getTemplateById($value);
            if ($result) {
                $request['subject'] = $result->subject;
                $request['body'] = $result->body;
                $request['day'] =  $key * 2;
                $request['cumulative_send_day'] = 2;
                $this->autoFollowupService->addAutoFollowup($request);
            }
        }

        return [
            'html' => 'Auto Followup added successfully',
            'status' => true
        ];
    }


    public function viewEmailTemplateOnBoarding(Request $request)
    {
        $emailTemplates = $this->emailTemplateService->getTemplateById($request['id']);

        return response()->json([
            'html' => view('userdashboard::autofollowup.view-email-template-for-boarding')->with([
                'emailTemplates' => $emailTemplates,
            ])->render(),
            'status' => 'success'
        ]);

    }

}
