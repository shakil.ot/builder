<?php

namespace Modules\UserDashboard\Http\Controllers;

use App\Contracts\Service\UserContact;

use App\Http\Requests\ResetPasswordSubmitRequest;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Modules\Broadcast\Contracts\Repositories\BroadcastLogRepository;
use Modules\Broadcast\Entities\BroadcastLog;
use Modules\UserDashboard\Contracts\Service\AutoFollowupContact;
use Modules\UserDashboard\Contracts\Service\OverallStatContact;
use Modules\UserDashboard\Contracts\Service\SingleSendStatContact;
use Modules\UserDashboard\Http\Requests\UpdateProfileImageRequest;
use Modules\UserDashboard\Http\Requests\UpdateProfileRequest;
use Modules\WebBuilder\Contracts\Services\WebBuilderContact;
use Modules\WebBuilder\Entities\WebBuilder;
use function GuzzleHttp\Promise\all;


class DashboardController extends Controller
{
    /**
     * @var UserContact
     */
    private $userService;
    /**
     * @var AutoFollowupContact
     */
    private $autoFollowupService;
    /**
     * @var OverallStatContact
     */
    private $overallStatService;
    private $broadcastLogRepository;
    /**
     * @var SingleSendStatContact
     */
    private $singleSendStatService;

    private $webBuilderService;

    /**
     * DashboardController constructor.
     * @param UserContact $userService
     * @param AutoFollowupContact $autoFollowupService
     * @param OverallStatContact $overallStatService
     * @param BroadcastLogRepository $broadcastLogRepository
     * @param SingleSendStatContact $singleSendStatService
     * @param WebBuilderContact $webBuilderService
     */
    public function __construct(UserContact $userService,
                                AutoFollowupContact $autoFollowupService,
                                OverallStatContact $overallStatService,
                                BroadcastLogRepository $broadcastLogRepository,
                                SingleSendStatContact $singleSendStatService,
                                WebBuilderContact $webBuilderService
    )
    {
        $this->userService = $userService;
        $this->autoFollowupService = $autoFollowupService;
        $this->overallStatService = $overallStatService;
        $this->broadcastLogRepository = $broadcastLogRepository;
        $this->singleSendStatService = $singleSendStatService;
        $this->webBuilderService = $webBuilderService;
    }


    public function index()
    {
        $userId = Auth::id();

        $autoFollowupCount = $this->singleSendStatService->getAutoFollowupCount($userId);

        $webBuilder = $this->webBuilderService->getTemplateDataForView(WebBuilder::WEB_BUILDER_TYPE_SUBSCRIPTION, $userId);

        return view('userdashboard::index')->with([
            'pageId' => $webBuilder['id'],
            'autoFollowupCount' => $autoFollowupCount,
        ]);
    }

    public function userProfile()
    {
        $data['user'] = auth()->user();
        return view('userdashboard::profile.index')->with($data);
    }

    public function updateUserProfile(UpdateProfileRequest $request)
    {
        if (!$request->ajax()) {
            return redirect()->route('dashboard');
        }

        return response()->json($this->userService->update($request));
    }

    public function updateUserProfileImage(UpdateProfileImageRequest $request)
    {
        if (!$request->ajax()) {
            return redirect()->route('dashboard');
        }

        if ($request->hasFile('profile_image')) {
            return response()->json($this->userService->updateProfileImage($request->file('profile_image'), auth()->id()));
        } else {
            return response()->json(['status' => false, 'message' => 'Invalid file uploaded!']);
        }

    }

    public function changePassword(ResetPasswordSubmitRequest $request)
    {
        if (!$request->ajax()) {
            return redirect()->route('dashboard');
        }

        return $this->userService->changePassword($request, auth()->id());
    }

    public function durationWiseCount(Request $request)
    {
        if (!$request->ajax()) {
            return redirect()->route('dashboard');
        }
        $request['userId'] = auth()->id();
        return response()->json($this->overallStatService->getDashboardDurationwiseCount($request));
    }

    public function funnelStepsCount(Request $request)
    {
        if (!$request->ajax()) {
            return redirect()->route('dashboard');
        }
        $request['userId'] = auth()->id();
        return response()->json($this->overallStatService->getDashboardFunnelStepsCount($request));
    }

    public function emailStatusReceive(Request $request)
    {
        Log::info("Status Received");
        Log::info($request->all());
        if ($request->get('0')['event'] == "delivered") {
            if (isset($request->get('0')['tracking_id'])) {
                $trackingId = $request->get('0')['tracking_id'];
                Log::info($trackingId);
                $broadcastLogData = $this->broadcastLogRepository->getDataByTrackingId($trackingId);
                if ($broadcastLogData) {
                    if ($broadcastLogData['email_status'] == BroadcastLog::EMAIL_STATUS_LOG_CREATED) {
                        if ($broadcastLogData['broadcast_id'] != null && $broadcastLogData['broadcast_id'] != '') {
                            $where = [
                                'user_id' => $broadcastLogData['user_id'],
                                'broadcast_id' => $broadcastLogData['broadcast_id'],
                                'activity_date_string' => date("Y-m-d")
                            ];
                        } else if ($broadcastLogData['auto_followup_id'] != null && $broadcastLogData['auto_followup_id'] != '') {
                            $where = [
                                'user_id' => $broadcastLogData['user_id'],
                                'auto_followup_id' => $broadcastLogData['auto_followup_id'],
                                'activity_date_string' => date("Y-m-d")
                            ];
                        }
                        $existingData = $this->singleSendStatService->getDataByWhere($where);
                        if ($existingData) {
                            $this->singleSendStatService->incrementByWhereAndFieldName($where, 'email_sent');
                        } else {
                            $singleSendData = [
                                'email_sent' => 1
                            ];
                            $this->singleSendStatService->updateOrCreateByParam($where, $singleSendData);
                        }
                        $where = [
                            'tracking_id' => $trackingId
                        ];
                        $update = [
                            'email_status' => BroadcastLog::EMAIL_STATUS_SENT
                        ];
                        $this->broadcastLogRepository->updateBroadcastLogEmailStatus($where, $update);

                    }

                } else {
                    Log::info("No broadcast log data found by the tracking id");
                }
            } else {
                Log::info("Tracking id not get");
            }
        }
        else if ($request->get('0')['event'] == "open") {
            if (isset($request->get('0')['tracking_id'])) {
                $trackingId = $request->get('0')['tracking_id'];
                $broadcastLogData = $this->broadcastLogRepository->getDataByTrackingId($trackingId);
                if ($broadcastLogData) {
                    if ($broadcastLogData['email_status'] == BroadcastLog::EMAIL_STATUS_LOG_CREATED || $broadcastLogData['email_status'] == BroadcastLog::EMAIL_STATUS_SENT) {
                        if ($broadcastLogData['broadcast_id'] != null && $broadcastLogData['broadcast_id'] != '') {
                            $where = [
                                'broadcast_id' => $broadcastLogData['broadcast_id'],
                                'activity_date_string' => date("Y-m-d")
                            ];
                        } else if ($broadcastLogData['auto_followup_id'] != null && $broadcastLogData['auto_followup_id'] != '') {
                            $where = [
                                'auto_followup_id' => $broadcastLogData['auto_followup_id'],
                                'activity_date_string' => date("Y-m-d")
                            ];
                        }
                        $existingData = $this->singleSendStatService->getDataByWhere($where);
                        if ($existingData) {
                            $this->singleSendStatService->incrementByWhereAndFieldName($where, 'email_opened');
                        } else {
                            $singleSendData = [
                                'email_opened' => 1
                            ];
                            $this->singleSendStatService->updateOrCreateByParam($where, $singleSendData);
                        }
                        $where = [
                            'tracking_id' => $trackingId
                        ];
                        $update = [
                            'email_status' => BroadcastLog::EMAIL_STATUS_OPENED
                        ];
                        $this->broadcastLogRepository->updateBroadcastLogEmailStatus($where, $update);

                    }

                } else {
                    Log::info("No broadcast log data found by the tracking id");
                }
            } else {
                Log::info("Tracking id not get");
            }
        }
        else if ($request->get('0')['event'] == "click") {
            if (isset($request->get('0')['tracking_id'])) {
                $trackingId = $request->get('0')['tracking_id'];
                $broadcastLogData = $this->broadcastLogRepository->getDataByTrackingId($trackingId);
                if ($broadcastLogData) {
                    if ($broadcastLogData['email_status'] == BroadcastLog::EMAIL_STATUS_LOG_CREATED || $broadcastLogData['email_status'] == BroadcastLog::EMAIL_STATUS_SENT || $broadcastLogData['email_status'] == BroadcastLog::EMAIL_STATUS_OPENED) {
                        if ($broadcastLogData['broadcast_id'] != null && $broadcastLogData['broadcast_id'] != '') {
                            $where = [
                                'broadcast_id' => $broadcastLogData['broadcast_id'],
                                'activity_date_string' => date("Y-m-d")
                            ];
                        } else if ($broadcastLogData['auto_followup_id'] != null && $broadcastLogData['auto_followup_id'] != '') {
                            $where = [
                                'auto_followup_id' => $broadcastLogData['auto_followup_id'],
                                'activity_date_string' => date("Y-m-d")
                            ];
                        }
                        $existingData = $this->singleSendStatService->getDataByWhere($where);
                        if ($existingData) {
                            $this->singleSendStatService->incrementByWhereAndFieldName($where, 'email_clicked');
                        } else {
                            $singleSendData = [
                                'email_clicked' => 1
                            ];
                            $this->singleSendStatService->updateOrCreateByParam($where, $singleSendData);
                        }
                        $where = [
                            'tracking_id' => $trackingId
                        ];
                        $update = [
                            'email_status' => BroadcastLog::EMAIL_STATUS_CLICKED
                        ];
                        $this->broadcastLogRepository->updateBroadcastLogEmailStatus($where, $update);

                    }

                } else {
                    Log::info("No broadcast log data found by the tracking id");
                }
            } else {
                Log::info("Tracking id not get");
            }
        }

    }
}
