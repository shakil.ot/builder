<?php

namespace Modules\UserDashboard\Http\Repositories;

use App\Repositories\BaseRepository\BaseRepository;
use App\Services\Utility;
use Illuminate\Support\Carbon;
use Modules\Broadcast\Entities\Broadcast;
use Modules\UserDashboard\Contracts\Repositories\AutoFollowupQueueRepository;
use Modules\UserDashboard\Entities\AutoFollowupQueue;

class AutoFollowupQueueRepositoryEloquent extends BaseRepository implements AutoFollowupQueueRepository
{

    protected function model()
    {
        return new AutoFollowupQueue();
    }


    /*****## CHECK DATA EXITS OR NOT BY CONTACT ID  ##*****/
    public function checkDataExitsByContactId($contactId)
    {
        return $this->model
            ->where('contact_id', $contactId)
            ->exists();
    }

    /*****## INSERT AUTO-FOLLOWUP MESSAGE  ##*****/
    public function addAutoFollowupMessage($data)
    {
        return $this->create($data);
    }


    /*****## Get PENDING ITEM NOW(date)+1 AND NOW(date)-1  ##*****/
    public function getAllDataByDateIsNow()
    {
        $dateStart = Carbon::now()
            ->subMinutes(100)
            ->format('Y-m-d H:i:s');

        $dateEnd = Carbon::now()
            ->addMinutes(100)
            ->format('Y-m-d H:i:s');

        $conversation = $this->model
            ->with(['autoFollowup', 'contact', 'user'])
            ->whereRaw('schedule_time >= ?', [$dateStart])
            ->whereRaw('schedule_time <= ?', [$dateEnd])
            ->select('auto_followup_id', 'contact_id', 'day', 'schedule_time', 'id', 'user_id', 'schedule_time');

        return $conversation->get();
    }

    public function getFollowupByUserIdAndContactId($userId, $contactId)
    {
        return $this->model()
            ->where(['user_id' => $userId, 'contact_id' => $contactId])
            ->with(['autoFollowup'])
            ->first();
    }

    public function deleteQueuesByContactByCondition($where)
    {
        return $this->model()
            ->where($where)
            ->delete();
    }

    public function deleteById($id)
    {
        return $this->model
            ->where('id', $id)
            ->delete();
    }

    public function getAllFollowupByUserId($userId)
    {
        return $this->model
            ->where('user_id', $userId)
            ->with(['contact' , 'autoFollowup'])->get();
    }
}
