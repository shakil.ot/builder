<?php

namespace Modules\UserDashboard\Http\Repositories;

use App\Repositories\BaseRepository\BaseRepository;
use Modules\UserDashboard\Contracts\Repositories\AutoFollowupRepository;
use Modules\UserDashboard\Entities\AutoFollowup;

class AutoFollowupRepositoryEloquent extends BaseRepository implements AutoFollowupRepository
{

    protected function model()
    {
        return new AutoFollowup();
    }

    public function addAutoFollowupByParam($data)
    {
        return $this->create($data);
    }

    public function getLastAddedFollowupByUserId($userId)
    {
        return $this->model()
            ->where('user_id', $userId)
            ->orderBy('id', 'desc')
            ->first();
    }

    public function getFollowupsByUserId($userId)
    {
        return $this->model()
            ->where('user_id', $userId)
            ->orderBy('cumulative_send_day', 'asc')
            ->get();
    }

    public function getFollowupById($followupId, $select='*')
    {
        return $this->model()
            ->where('id', $followupId)
            ->select('*')
            ->first();
    }

    public function updateFollowupByParam($where, $data)
    {
        return $this->model()
            ->where($where)
            ->update($data);
    }

    public function getAllFollowupsNewerThanThis($followupId)
    {
        return $this->model()
            ->where('id', '>=', $followupId)
            ->get();
    }

    public function deleteFollowupById($followupId)
    {
        return $this->model()
            ->where('id', $followupId)
            ->delete();
    }

    public function getFirstFollowupMessageByUserId($userId)
    {
        return $this->model()
            ->where('user_id', $userId)
            ->orderBy('cumulative_send_day', 'asc')
            ->select('cumulative_send_day', 'id')
            ->first();
    }

    public function getNextDayFollowupMessageByUserIdAndDay($userId, $day)
    {
        return $this->model()
            ->where('user_id', $userId)
            ->where('cumulative_send_day','>', $day)
            ->orderBy('cumulative_send_day', 'asc')
            ->select('cumulative_send_day', 'id')
            ->first();
    }
}
