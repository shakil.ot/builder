<?php

namespace Modules\UserDashboard\Http\Repositories;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Repositories\BaseRepository\BaseRepository;
use Modules\UserDashboard\Contracts\Repositories\SingleSendStatReportRepository;
use Modules\UserDashboard\Entities\AutoFollowup;
use Modules\UserDashboard\Entities\SingleSendStatReport;
use phpDocumentor\Reflection\Types\This;

class SingleSendStatReportRepositoryEloquent extends BaseRepository implements SingleSendStatReportRepository
{
    protected function model()
    {
        return new SingleSendStatReport();
    }

    public function getTotalEmailByDurationAndParam($userId, $durationType, $fromDate, $toDate, $param)
    {
        if ($durationType == "week" || $durationType == "month") {
            return $this->model()
                ->where('user_id', $userId)
                ->whereBetween('activity_date_string', [$fromDate, $toDate])
                ->sum($param);
        } else {
            return $this->model()
                ->where('user_id', $userId)
                ->sum($param);
        }
    }

    public function getDataByWhere($where)
    {
        return $this->model()
            ->where($where)
            ->first();
    }

    public function updateOrCreateByParam($where, $data)
    {
        return $this->updateOrCreate($where, $data);
    }

    public function getAutoFollowupCount($userId)
    {
        return AutoFollowup::with(['getCount'])->orderBy('created_at', 'desc')
            ->where('user_id',$userId)
            ->get();
    }

    public function incrementByWhereAndFieldName($where, $fieldName)
    {
        return $this->model()
            ->where($where)
            ->increment($fieldName, 1);
    }
    public function deleteByWhere($where)
    {
        return $this->model()
            ->where($where)
            ->delete();
    }

}
