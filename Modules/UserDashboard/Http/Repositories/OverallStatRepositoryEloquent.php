<?php

namespace Modules\UserDashboard\Http\Repositories;

use App\Models\OverallStatsCounts;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Repositories\BaseRepository\BaseRepository;
use Modules\UserDashboard\Contracts\Repositories\OverallStatRepository;
use Modules\UserDashboard\Entities\OverallStatsCount;

class OverallStatRepositoryEloquent extends BaseRepository implements OverallStatRepository
{
    protected function model()
    {
        return new OverallStatsCount();
    }

    public function getTotalUniqueVisitorsByDurationType($userId, $durationType, $fromDate, $toDate)
    {
        if ($durationType == "week" || $durationType == "month") {
            return $this->model()
                ->where('user_id', $userId)
                ->whereBetween('activity_date_string', [$fromDate, $toDate])
                ->sum('total_unique_visitors');
        } else {
            return $this->model()
                ->where('user_id', $userId)
                ->sum('total_unique_visitors');
        }
    }

    public function updateOrCreateByParam($where, $data)
    {
        return $this->updateOrCreate($where, $data);
    }

    public function getDashboardFunnelStepsCountByUserId($userId, $params)
    {
        return DB::table('overall_stats_counts')
            ->select([
                DB::raw("SUM(total_subscription_visits) as total_subscription_visits"),
                DB::raw("SUM(total_subscription_submits) as total_subscription_submits"),
                DB::raw("SUM(total_download_instruction_visits) as total_download_instruction_visits"),
                DB::raw("SUM(total_offer_visits) as total_offer_visits"),
                DB::raw("SUM(total_offer_submits) as total_offer_submits"),
                DB::raw("SUM(total_thank_you_visits) as total_thank_you_visits"),
            ])
            ->where('user_id', $userId)
            ->get();
    }



    public function insertOverallStatisticsData($request, $userId)
    {
        return $this->model->create([
            'user_id' => $userId,
            'activity_date_string' => $request['activity_date_string'],
            'total_unique_visitors' => isset($request['total_unique_visitors']) ? $request['total_unique_visitors'] : 0,
            'total_subscription_visits' => isset($request['total_subscription_visits']) ? $request['total_subscription_visits'] : 0,
            'total_subscription_submits' => isset($request['total_subscription_submits']) ? $request['total_subscription_submits'] : 0,
            'total_download_instruction_visits' => isset($request['total_download_instruction_visits']) ? $request['total_download_instruction_visits'] : 0,
            'total_offer_visits' => isset($request['total_offer_visits']) ? $request['total_offer_visits'] : 0,
            'total_offer_submits' => isset($request['total_offer_submits']) ? $request['total_offer_submits'] : 0,
            'total_thank_you_visits' => isset($request['total_thank_you_visits']) ? $request['total_thank_you_visits'] : 0
        ]);
    }

    public function updateTotalCount($dataArray, $request, $key)
    {
        return $this->model->where($dataArray)
            ->increment($key, $request[$key]);
    }

    public function checkDataIsExitsByDate($userId, $activityDate)
    {
        return $this->model->where('user_id', $userId)
            ->where('activity_date_string', $activityDate)
            ->exists();

    }

}
