<?php

namespace Modules\UserDashboard\Http\Services;

use Modules\Contact\Contracts\Services\ContactContact;
use Modules\UserDashboard\Contracts\Repositories\OverallStatRepository;
use Modules\UserDashboard\Contracts\Service\OverallStatContact;
use DateTime;
use Modules\UserDashboard\Contracts\Service\SingleSendStatContact;

class OverallStatService implements OverallStatContact
{
    private $overallStatRepository;
    private $contactService;
    private $singleSendStatService;

    public function __construct(OverallStatRepository $overallStatRepository,
                                SingleSendStatContact $singleSendStatService,
                                ContactContact $contactService)
    {
        $this->overallStatRepository = $overallStatRepository;
        $this->contactService = $contactService;
        $this->singleSendStatService = $singleSendStatService;
    }

    public function getDashboardDurationwiseCount($request)
    {
        $userId = $request->get('userId');
        $durationType = $request->get('durationType');
        $fromDate = null;
        $toDate = null;
        if ($durationType != "forever") {
            $fromDate = $this->fromDate($durationType);
            $toDate = $this->toDate($durationType, $fromDate);
        }

        $results['total_unique_visitors'] = $this->overallStatRepository->getTotalUniqueVisitorsByDurationType($userId, $durationType, $fromDate, $toDate);
        $params = ['email_sent', 'email_opened', 'email_clicked'];
        $results['total_sent_email'] = $this->singleSendStatService->getTotalEmailByDurationAndParam($userId, $durationType, $fromDate, $toDate, $params[0]);
        $results['total_opened_email'] = $this->singleSendStatService->getTotalEmailByDurationAndParam($userId, $durationType, $fromDate, $toDate, $params[1]);
        $results['total_clicked_email'] = $this->singleSendStatService->getTotalEmailByDurationAndParam($userId, $durationType, $fromDate, $toDate, $params[2]);
        $results['total_subscribers'] = $this->contactService->getTotalSubscribersByDuration($userId, $durationType);
        $results['total_leads'] = $this->contactService->getTotalLeadsByDuration($userId, $durationType);
        return $results;
    }

    public function getDashboardFunnelStepsCount($request)
    {
        $userId = $request->get('userId');
        $params = ['total_subscription_visits', 'total_subscription_submits', 'total_download_instruction_visits', 'total_offer_visits', 'total_offer_submits', 'total_thank_you_visits'];
        $result = $this->overallStatRepository->getDashboardFunnelStepsCountByUserId($userId, $params);

        foreach ($params as $param) {
            if (!is_null($result[0]->{$param})) {
                $results[$param] = $result[0]->{$param};
            } else {
                $results[$param] = 0;
            }
        }
        return $results;
    }


    private function fromDate($type)
    {
        if ($type == 'week') {
            $date = (new \DateTime())->modify('-6 day');
            $dateString = $date->format('Y-m-d');
            $dateString .= ' 00:00:00';
            $date = DateTime::createFromFormat('Y-m-d H:i:s', $dateString);
            return $date->format('Y-m-d H:i:s');
        } else if ($type == 'month') {
            $date = (new \DateTime())->modify('-29 day');
            $dateString = $date->format('Y-m-d');
            $dateString .= ' 00:00:00';
            $date = DateTime::createFromFormat('Y-m-d H:i:s', $dateString);
            return $date->format('Y-m-d H:i:s');
        }
    }


    private function toDate($type, $fromDate)
    {
        if ($type == 'week') {
            $date = DateTime::createFromFormat('Y-m-d H:i:s', $fromDate);
            $date = $date->modify('+7 day');
            $date = $date->modify('-1 second');
            return $date->format('Y-m-d H:i:s');
        } else if ($type == 'month') {
            $date = DateTime::createFromFormat('Y-m-d H:i:s', $fromDate);
            $date = $date->modify('+30 day');
            $date = $date->modify('-1 second');
            return $date->format('Y-m-d H:i:s');
        }
    }
}
