<?php

namespace Modules\UserDashboard\Http\Services;


use App\Helper\UtilityHelper;
use App\Services\SendgridMailService;
use App\Services\Utility;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;
use Modules\Broadcast\Contracts\Repositories\BroadcastLogRepository;
use Modules\Broadcast\Entities\BroadcastLog;
use Modules\Contact\Entities\Contact;
use Modules\EmailSetting\Contracts\Repositories\EmailSettingRepository;
use Modules\Package\Contracts\Service\UserBalanceContact;
use Modules\UserDashboard\Contracts\Repositories\AutoFollowupQueueRepository;
use Modules\UserDashboard\Contracts\Repositories\AutoFollowupRepository;
use Modules\UserDashboard\Contracts\Service\AutoFollowupContact;
use Modules\WebBuilder\Contracts\Repositories\WebBuilderRepository;
use Modules\WebBuilder\Entities\WebBuilder;

class AutoFollowupService implements AutoFollowupContact
{
    /**
     * @var AutoFollowupRepository
     */
    private $autoFollowupRepository;
    /**
     * @var AutoFollowupQueueRepository
     */
    private $autoFollowupQueueRepository;
    /**
     * @var UserBalanceContact
     */
    private $userBalanceService;
    /**
     * @var BroadcastLogRepository
     */
    private $broadcastLogRepository;
    /**
     * @var WebBuilderRepository
     */
    private $webBuilderRepository;

    public function __construct(AutoFollowupRepository $autoFollowupRepository,
                                AutoFollowupQueueRepository $autoFollowupQueueRepository,
                                UserBalanceContact $userBalanceService,
                                BroadcastLogRepository $broadcastLogRepository,
                                WebBuilderRepository $webBuilderRepository)
    {
        $this->autoFollowupRepository = $autoFollowupRepository;
        $this->autoFollowupQueueRepository = $autoFollowupQueueRepository;
        $this->userBalanceService = $userBalanceService;
        $this->broadcastLogRepository = $broadcastLogRepository;
        $this->webBuilderRepository = $webBuilderRepository;
    }

    public function addAutoFollowup($request)
    {
        $userId = $request->get('userId');
        if ($request->get('subject') == null || $request->get('subject') == '') {
            return UtilityHelper::RETURN_STATUS_FORMATE(false, 'Please enter email subject');
        }
        if ($request->get('body') == null || $request->get('body') == '') {
            return UtilityHelper::RETURN_STATUS_FORMATE(false, 'Please enter email body');
        }
        $sendDayAfterPreviousEmail = 0;
        $cumulativeSendDay = 0;
        if ($request->get('day') != null && $request->get('day') != '') {
            $sendDayAfterPreviousEmail = $request->get('day');
            $cumulativeSendDay = $request->get('day');
        }
        $lastAddedFollowup = $this->autoFollowupRepository->getLastAddedFollowupByUserId($userId);
        if ($lastAddedFollowup) {
            $cumulativeSendDay = $cumulativeSendDay + $lastAddedFollowup['cumulative_send_day'];
        }
        $data = [
            'user_id' => $userId,
            'email_subject' => $request->get('subject'),
            'email_body' => $request->get('body'),
            'send_day_after_previous_email' => $sendDayAfterPreviousEmail,
            'cumulative_send_day' => $cumulativeSendDay
        ];
        $checkUserBalance = $this->userBalanceService->checkBalanceByUserIdAndType($userId);
        if ($checkUserBalance->remaining_no_of_auto_followups <= 0) {
            return UtilityHelper::RETURN_STATUS_FORMATE(false, 'Sorry! You have reached auto followup balance limit!');
        }

        $response = $this->autoFollowupRepository->addAutoFollowupByParam($data);
        if ($response) {
            if ($checkUserBalance->remaining_no_of_auto_followups > 0) {
                App::make(UserBalanceContact::class)->reduceBalanceByUserIdAndType($userId, 'remaining_no_of_auto_followups', 1);
            }

            return UtilityHelper::RETURN_STATUS_FORMATE(true, 'Followup Setting Added');
        }
        return UtilityHelper::RETURN_STATUS_FORMATE(false, 'Followup Setting Addition Failed! Please Try Again');
    }

    public function updateFollowup($request)
    {
        $followupId = $request->get('followupId');
        $userId = $request->get('userId');
        if ($request->get('subject') == null || $request->get('subject') == '') {
            return UtilityHelper::RETURN_STATUS_FORMATE(false, 'Please enter email subject');
        }
        if ($request->get('body') == null || $request->get('body') == '') {
            return UtilityHelper::RETURN_STATUS_FORMATE(false, 'Please enter email body');
        }
        $sendDayAfterPreviousEmail = 0;
        if ($request->get('day') != null && $request->get('day') != '') {
            $sendDayAfterPreviousEmail = $request->get('day');
        }
        $where = [
            'id' => $followupId
        ];
        $data = [
            'user_id' => $userId,
            'email_subject' => $request->get('subject'),
            'email_body' => $request->get('body'),
            'send_day_after_previous_email' => $sendDayAfterPreviousEmail,
        ];
        $response = $this->autoFollowupRepository->updateFollowupByParam($where, $data);
        if ($response) {
            $previousSendDay = $request->get('previousSendDay');
            $allNewerFollowups = $this->autoFollowupRepository->getAllFollowupsNewerThanThis($followupId);
            if ($previousSendDay > $sendDayAfterPreviousEmail) {
                // Day decreased
                $decreasedDay = $previousSendDay - $sendDayAfterPreviousEmail;
                foreach ($allNewerFollowups as $followup) {
                    $where = [
                        'id' => $followup->id
                    ];
                    $data = [
                        'cumulative_send_day' => $followup->cumulative_send_day - $decreasedDay
                    ];
                    $this->autoFollowupRepository->updateFollowupByParam($where, $data);
                }
            } else if ($previousSendDay < $sendDayAfterPreviousEmail) {
                //Day increased
                $increasedDay = $sendDayAfterPreviousEmail - $previousSendDay;
                foreach ($allNewerFollowups as $followup) {
                    $where = [
                        'id' => $followup->id
                    ];
                    $data = [
                        'cumulative_send_day' => $followup->cumulative_send_day + $increasedDay
                    ];
                    $this->autoFollowupRepository->updateFollowupByParam($where, $data);
                }
            }
            return UtilityHelper::RETURN_STATUS_FORMATE(true, 'Followup Setting Updated Successfully');

        }
        return UtilityHelper::RETURN_STATUS_FORMATE(false, 'Followup Setting Updating Failed! Please Try Again');


    }

    public function deleteFollowup($request)
    {
        $followupId = $request->get('followupId');
        $userId = $request->get('userId');
        $followupDetails = $this->autoFollowupRepository->getFollowupById($followupId);
        if ($followupDetails['send_day_after_previous_email'] > 0) {
            $allNewerFollowups = $this->autoFollowupRepository->getAllFollowupsNewerThanThis($followupId);
            foreach ($allNewerFollowups as $followup) {
                $where = [
                    'id' => $followup->id
                ];
                $data = [
                    'cumulative_send_day' => $followup->cumulative_send_day - $followupDetails['send_day_after_previous_email']
                ];
                $this->autoFollowupRepository->updateFollowupByParam($where, $data);
            }
        }
        $response = $this->autoFollowupRepository->deleteFollowupById($followupId);
        if ($response) {
            App::make(UserBalanceContact::class)->increaseBalanceByUserIdAndType($userId, 'remaining_no_of_auto_followups', 1);
            return UtilityHelper::RETURN_STATUS_FORMATE(true, 'Followup Setting Deleted Successfully');
        }
        return UtilityHelper::RETURN_STATUS_FORMATE(true, 'Followup Setting Deletion Failed! Please Try Again');
    }

    public function getFollowupsByUserId($userId)
    {
        return $this->autoFollowupRepository->getFollowupsByUserId($userId);
    }

    public function getFollowupById($followupId, $select = '*')
    {
        return $this->autoFollowupRepository->getFollowupById($followupId, $select);
    }



    /******### SUBSCRIPTION CONTACT FIRST FOLLOWUP MESSAGE INSERT IN CAMPAIGN QUEUE TABLE  ###******/
    /******### when a new subscriber coming in system then call this function
     *     ### for insert first auto-followup message in campaign queue table ###*****
     */

    public function addFirstAutoFollowupMessageInCampaignQueueForNewSubscriber($contactId, $userId)
    {
        /*** Check this contact already exits or not ***/
        $checkIsExists = $this->autoFollowupQueueRepository->checkDataExitsByContactId($contactId);
        if (!$checkIsExists) {
            /*** Get auto followup message data from auto-followup table ***/
            $getAutoFollowupMessage = $this->autoFollowupRepository->getFirstFollowupMessageByUserId($userId);
            if ($getAutoFollowupMessage) {

                /*** Add Day if day is ( > ) 0  ***/
                if ($getAutoFollowupMessage->cumulative_send_day > 0) {
                    $scheduleTime = Carbon::now()
                        ->addDays($getAutoFollowupMessage->cumulative_send_day)
                        ->format('Y-m-d 10:i:s');

//                    $parsConvertDate = Carbon::parse($convertTime);
                } else {
                    /*** Add minutes  ***/
                    $scheduleTime = Carbon::now()->addMinutes(2)
                        ->format('Y-m-d H:i:s');
                }


                $data = [
                    'auto_followup_id' => $getAutoFollowupMessage->id,
                    'user_id' => $userId,
                    'contact_id' => $contactId,
                    'day' => $getAutoFollowupMessage->cumulative_send_day,
                    'schedule_time' => $scheduleTime
                ];


                $this->autoFollowupQueueRepository->addAutoFollowupMessage($data);

            }
        }

    }


    /******### SEND FOLLOWUP MESSAGE FROM AUTO-FOLLOWUP-QUEUE TABLE  ###******/
    /******### its automatically send auto-followup message send one by one, Pick a data
     *     ### from the table and send message . when it will be sent successfully then insert next followup message in table
     *     ### from AUTO-FOLLOWUP-QUEUE table  ###*****
     */

    public function sendAutoFollowupMessageToSubscriber()
    {
        /*** Get Pending data where date is now ***/
        $getAutoFollowUpQueueData = $this->autoFollowupQueueRepository->getAllDataByDateIsNow();

        if ($getAutoFollowUpQueueData) {

            foreach ($getAutoFollowUpQueueData as $row) {

                $id = $row->id;
                $day = $row->day;
                $contactId = $row->contact_id;
                $subject = $row['autoFollowup']->email_subject;
                $message = $row['autoFollowup']->email_body;
                $user_id = $row['autoFollowup']->user_id;

                $userInfo = $row['user'];
                $contactInfo = $row['contact'];
                $contactStatus = $contactInfo['status'];
                $toEmail = $row['contact']->email;
                $from = env('FROM_EMAIL');
                $companyName = "";

                $companyInfo = App::make(EmailSettingRepository::class)->getEmailDataByUserId($userInfo->id);
                $from = isset($companyInfo->email) ? $companyInfo->email : "";

                $trackingId = UtilityHelper::generateRandomString(10) . strtotime("now");

                $status = BroadcastLog::FAILED;
                /***## GET USER BALANCE ##***/
                $getUserBalance = $this->userBalanceService->checkBalanceByUserIdAndType($user_id);
                if ($getUserBalance) {
                    if ($getUserBalance->remaining_no_of_email_send_per_month > 0 && $contactStatus == Contact::ACTIVE) {

                        /***## CONVERT PERSONALIZED MESSAGE ##***/
                        $webbuilderOfferPageData = $this->webBuilderRepository->getTemplateByCondition(['user_id' => $user_id, 'type' => WebBuilder::WEB_BUILDER_TYPE_OFFER]);
                        $offerPageUrl = $url = env('APP_URL') . '/offer/' . $webbuilderOfferPageData->id . '/v';

                        $message = \ViewHelper::personalizedReplace($message, $contactInfo, $userInfo, $offerPageUrl);
                        $subject = \ViewHelper::personalizedReplace($subject, $contactInfo, $userInfo);

                        $companyName = $userInfo->first_name;

                        $companyName = !is_null($companyInfo) ? !is_null($companyInfo->company_name) ? $companyInfo->company_name : '' : '';

                        if (empty($companyName)){
                            $companyName = $userInfo->first_name;
                        }

                        /*ADD FOOTER */
                        if ($companyInfo) {
                            if ($companyInfo->address != null && !empty($companyInfo->address) && $companyInfo->country != null && !empty($companyInfo->country)) {
                                $message .= Utility::mailHtml($companyInfo);
                            }
                        }

                        /***## Send mail using sendgrid API ##***/
                        $result = App::make(SendgridMailService::class)->sendMailWithSendGrid($subject, $toEmail, $from, $message, $companyName, $trackingId);

                        /***## REDUCE USER BALANCE ##***/
                        $this->userBalanceService->reduceBalanceByUserIdAndType($user_id, "remaining_no_of_email_send_per_month", 1);


                        if ($result) {
                            $status = BroadcastLog::DELIVERED;
                        }

                    }
                } else {
                    $result = 'Insufficient Balance';
                    if($contactStatus != Contact::ACTIVE){
                        $result = 'Contact is blocked';
                    }
                }

                $this->broadcastLogRepository->broadcastLogCreate([
                    'auto_followup_id' => $row->auto_followup_id,
                    'user_id' => $row->user_id,
                    'tracking_id' => $trackingId,
                    'contact_id' => $contactInfo->id,
                    'to_email' => $toEmail,
                    'content_type' => BroadcastLog::CONTENT_TYPE_EMAIL,
                    'send_date' => $row->schedule_time,
                    'subject' => $subject,
                    'message_body' => $message,
                    'error_code' => null,
                    'gateway_response' => json_encode($result),
                    'status' => $status
                ]);;
                /***## DELETE DATA FROM AUTO-FOLLOWUP-QUEUE TABLE BY ID ##***/
                $this->autoFollowupQueueRepository->deleteById($id);

                /***## Get Next Day Data for next schedule ##***/
                $getAutoFollowupMessage = $this->autoFollowupRepository->getNextDayFollowupMessageByUserIdAndDay($user_id, $day);

                if ($getAutoFollowupMessage) {

                    $convertTime = Carbon::now()
                        ->addDays($getAutoFollowupMessage->cumulative_send_day)
                        ->format('Y-m-d H:i:s');

                    $time = Carbon::parse($convertTime);

                    $scheduleTime = $time->addMinutes(2)
                        ->format('Y-m-d H:i:s');

                    $data = [
                        'auto_followup_id' => $getAutoFollowupMessage->id,
                        'user_id' => $user_id,
                        'contact_id' => $contactId,
                        'day' => $getAutoFollowupMessage->cumulative_send_day,
                        'schedule_time' => $scheduleTime
                    ];


                    $this->autoFollowupQueueRepository->addAutoFollowupMessage($data);
                } else {
                    Log::info("Offer");
                    $data['subject'] = "Offer!!! Don’t wait. You’ll miss out on the sale of the year";
//                    $data['email'] = '';

                    $webBuilderData = $this->webBuilderRepository->getTemplateByCondition([
                        'user_id' => $user_id,
                        'type' => WebBuilder::WEB_BUILDER_TYPE_OFFER,
                    ]);

                    $data['link'] = env('APP_URL') . '/offer/' . $webBuilderData->id . '/v';
                    $message = View::make('webbuilder::mail.offer', $data)->render();

                    /***## Send mail using sendgrid API ##***/
                    $result = App::make(SendgridMailService::class)->sendMailWithSendGrid($data['subject'], $toEmail, $from, $message, $companyName, $trackingId);

                }

            }
        }

    }

    public function nextFollowupDelete($request)
    {
        $userId = $request->get('userId');
        $autoFollowupId = $request->get('autoFollowupId');
        $autoFollowupQueueId = $request->get('autoFollowupQueueId');
        $contactId = $request->get('contactId');
        $day = $request->get('day');
        $getAutoFollowupMessage = $this->autoFollowupRepository->getNextDayFollowupMessageByUserIdAndDay($userId, $day);

        if ($getAutoFollowupMessage) {

            $convertTime = Carbon::now()
                ->addDays($getAutoFollowupMessage->cumulative_send_day)
                ->format('Y-m-d H:i:s');

            $time = Carbon::parse($convertTime);

            $scheduleTime = $time->addMinutes(2)
                ->format('Y-m-d H:i:s');

            $data = [
                'auto_followup_id' => $getAutoFollowupMessage->id,
                'user_id' => $userId,
                'contact_id' => $contactId,
                'day' => $getAutoFollowupMessage->cumulative_send_day,
                'schedule_time' => $scheduleTime
            ];

            if (!isset($request['next_send'])){
                $this->autoFollowupQueueRepository->addAutoFollowupMessage($data);
            }
        }

        $response = $this->autoFollowupQueueRepository->deleteById($autoFollowupQueueId);
        if ($response) {
            return [
                'status' => 'success',
                'html' => 'Followup deleted successfully',
            ];
        }

        return [
            'status' => 'error',
            'html' => 'Followup deletion failed',
        ];

    }


}
