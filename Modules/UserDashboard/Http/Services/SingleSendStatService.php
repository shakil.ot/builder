<?php

namespace Modules\UserDashboard\Http\Services;


use Modules\UserDashboard\Contracts\Repositories\SingleSendStatReportRepository;
use Modules\UserDashboard\Contracts\Service\SingleSendStatContact;

class SingleSendStatService implements SingleSendStatContact
{

    /**
     * @var SingleSendStatReportRepository
     */
    private $singleSendStatRepository;

    public function __construct(SingleSendStatReportRepository $singleSendStatRepository)
    {
        $this->singleSendStatRepository = $singleSendStatRepository;
    }

    public function getTotalEmailByDurationAndParam($userId, $durationType, $fromDate, $toDate, $param)
    {
        return $this->singleSendStatRepository->getTotalEmailByDurationAndParam($userId, $durationType, $fromDate, $toDate, $param);
    }
    public function getDataByWhere($where)
    {
        return $this->singleSendStatRepository->getDataByWhere($where);
    }

    public function updateOrCreateByParam($where, $data)
    {
        return $this->singleSendStatRepository->updateOrCreateByParam($where, $data);
    }

    public function getAutoFollowupCount($userId)
    {
        return $this->singleSendStatRepository->getAutoFollowupCount($userId);
    }
    public function incrementByWhereAndFieldName($where, $fieldName)
    {
        return $this->singleSendStatRepository->incrementByWhereAndFieldName($where, $fieldName);
    }
}
