<?php

namespace Modules\UserDashboard\Entities;

use Illuminate\Database\Eloquent\Model;

class AutoFollowup extends Model
{
    protected $fillable = [
        'user_id',
        'email_subject',
        'email_body',
        'send_day_after_previous_email',
        'cumulative_send_day'
    ];


    public function getCount(){
        return $this->hasMany(SingleSendStatReport::class)
            ->selectRaw('auto_followup_id, sum(email_sent) as email_sent')
            ->selectRaw('auto_followup_id, sum(email_opened) as email_opened')
            ->selectRaw('auto_followup_id, sum(email_clicked) as email_clicked')
            ->groupBy('auto_followup_id');
    }



//    public function getCount()
//    {
//        return $this->hasMany(SingleSendStatReport::class, 'auto_followup_id')->select(['id', 'email_sent', 'email_clicked', 'email_opened', 'auto_followup_id']);
//    }
}
