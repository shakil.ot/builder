<?php

namespace Modules\UserDashboard\Entities;


use Illuminate\Database\Eloquent\Model;

class OverallStatsCount extends Model
{
    protected $fillable = [
        'user_id',
        'activity_date_string',
        'total_unique_visitors',
        'total_sent_email',
        'total_opened_email',
        'total_clicked_email',
        'total_subscription_visits',
        'total_subscription_submits',
    ];
}
