<?php

namespace Modules\UserDashboard\Entities;

use Illuminate\Database\Eloquent\Model;

class SingleSendStatReport extends Model
{
    protected $fillable = [
        'user_id',
        'broadcast_id',
        'auto_followup_id',
        'tracking_id',
        'activity_date_string',
        'email_sent',
        'email_opened',
        'email_clicked',
        'activity_date_string'
    ];

    public function autoFollowup()
    {
        return $this->hasMany(AutoFollowup::class);
    }
}
