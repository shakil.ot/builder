<?php

namespace Modules\UserDashboard\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Modules\Contact\Entities\Contact;

class AutoFollowupQueue extends Model
{


    protected $fillable = [
        'auto_followup_id',
        'user_id',
        'contact_id',
        'day',
        'schedule_time',
        'status'
    ];

    public function autoFollowup()
    {
        return $this->belongsTo(AutoFollowup::class);
    }

    public function contact()
    {
        return $this->belongsTo(Contact::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }


}
