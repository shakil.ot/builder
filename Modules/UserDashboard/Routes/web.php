<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth', 'web', 'checkUserJourneyMode','userHasActivePackage'])->group(function(){
    Route::get('', 'DashboardController@index')->name('dashboard');
    Route::post('dashboard/duration-wise/count', 'DashboardController@durationWiseCount')->name('dashboard-duration-wise-count');
    Route::get('dashboard/funnel/steps/count', 'DashboardController@funnelStepsCount')->name('dashboard-funnel-steps-count');
});

Route::prefix('user')->name('user.')->middleware(['auth', 'web', 'userHasActivePackage'])->group(function(){
    Route::get('profile', 'DashboardController@userProfile')->name('profile');
    Route::post('profile/update', 'DashboardController@updateUserProfile')->name('profile.update');
    Route::post('password/change', 'DashboardController@changePassword')->name('password.change');
    Route::post('profile/image/update', 'DashboardController@updateUserProfileImage')->name('profile.image.update');
});


Route::prefix('autofollowup')->name('autofollowup.')->middleware(['auth', 'web', 'userHasActivePackage'])->group(function(){
    Route::get('/add-new-followup-form', 'AutoFollowupController@addNewFollowupForm')->name('add-new-followup-form');
    Route::post('/followup-form-submit', 'AutoFollowupController@followupFormSubmit')->name('followup-form-submit');
    Route::post('/followup-edit-form', 'AutoFollowupController@followupEditForm')->name('followup-edit-form');
    Route::post('/edit/followup-form-submit', 'AutoFollowupController@editFollowupFormSubmit')->name('edit-followup-form-submit');
    Route::post('/followup-delete', 'AutoFollowupController@deleteFollowup')->name('followup-delete');

    Route::post('/add-new-followup-form-OnBoarding', 'AutoFollowupController@addNewFollowupFormOnBoarding')->name('add-new-followup-form-OnBoarding');
    Route::post('/add-in-onboarding', 'AutoFollowupController@addInOnboarding')->name('add-in-onboarding');
    Route::post('/view-email-template-OnBoarding', 'AutoFollowupController@viewEmailTemplateOnBoarding')->name('view-email-template-OnBoarding');

    Route::post('/get-email-template-by-id', 'AutoFollowupController@getEmailTemplateById')->name('get-email-template-by-id');
});

Route::prefix('user')->name('user.')->middleware(['auth', 'web'])->group(function(){
    Route::post('profile/image/update', 'DashboardController@updateUserProfileImage')->name('profile.image.update');
});

Route::post('/email/status/receive', 'DashboardController@emailStatusReceive')->name('email-status-receive');

