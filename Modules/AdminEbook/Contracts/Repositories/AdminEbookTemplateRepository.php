<?php

namespace Modules\AdminEbook\Contracts\Repositories;

interface AdminEbookTemplateRepository
{
    public function getById($ebookId);

    public function getAllAdminEbookTemplateList($request, $userId, $select = ['*']);

    public function storeEbook($data);

    public function getAllByCondition($where , $select = ["*"]);

    public function deleteEbookById($id);

    public function updateOrCreateData($where , $update);


}
