<?php

namespace Modules\AdminEbook\Contracts\Repositories;

interface AdminEbookTemplateDetailsRepository
{
    public function storeEbookPage($data);

    public function getByCondition($where);

    public function updateOrCreateData($where, $data);

    public function getAllByCondition($where);

    public function deleteByCondition($where);

    public function getAllPagesOrderByPage($where , $select);

    public function getPagesOrderByPage($where , $select = ['*']);


}
