<?php

namespace Modules\AdminEbook\Contracts\Service;

interface AdminEbookTemplateContact
{
    public function getAllAdminEbookTemplateList($request, $userId);

    public function storeEbook($request, $userId);

    public function previewEbook($ebookId);

    public function eBookNameAdd($request);

    public function getPageData($eBookId, $pageId);

    public function updateEbook($request);

    public function getFirstPageUsingEBookId($eBookId);

    public function getAllAdminEbookPagesByBookId($eBookId);

    public function newPageAdd($request);

    public function getEBookMainPage();

    public function deleteEbookById($id);

    public function adminEbookPageDelete($request);

    public function adminEbookPageSort($request);

    public function getFirstPageUsingEBookIdSortByPage($eBookId);

    public function getPageDataSortByPage($eBookId, $pageId);

    public function getAllAdminEbookPagesByBookIdSortByPage($eBookId);

    public function getSingleContentById($request);



}
