<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminEbookTemplateDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_ebook_template_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('admin_ebook_template_id');
            $table->foreign('admin_ebook_template_id')->on('admin_ebook_templates')->references('id')->onDelete('cascade');
            $table->smallInteger('page_no');
            $table->longText('image')->nullable();
            $table->longText('content');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_ebook_template_details');
    }
}
