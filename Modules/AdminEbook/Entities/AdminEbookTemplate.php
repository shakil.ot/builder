<?php


namespace Modules\AdminEbook\Entities;

use Illuminate\Database\Eloquent\Model;

class AdminEbookTemplate extends Model
{

    protected $fillable = [
        'id',
        'title',
        'status',
        'image',
    ];

    /* RELATION */
    public function admin_ebook_template_details()
    {
        return $this->hasMany(AdminEbookTemplateDetail::class);
    }

}