<?php


namespace Modules\AdminEbook\Entities;

use Illuminate\Database\Eloquent\Model;

class AdminEbookTemplateDetail extends Model
{

    protected $fillable = [
        'admin_ebook_template_id',
        'page_no',
        'image',
        'content',
    ];


}