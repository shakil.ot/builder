<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('admin/ebook')->name('admin.')->middleware('admin')->group(function() {
    Route::get('/', 'AdminEbookController@index')->name('ebook');
    Route::get('/add', 'AdminEbookController@addEbook')->name('ebook.add');
    Route::get('/list', 'AdminEbookController@ebookList')->name('ebook.list');
    Route::post('/store', 'AdminEbookController@storeEbook')->name('ebook.store');
    Route::get('/preview/{ebookId}', 'AdminEbookController@previewEbook')->name('ebook.preview');
    Route::post('e-book-name-add', 'AdminEbookController@eBookNameAdd')->name('e-book-name-add');
    Route::get('/edit/{ebookId}/{pageId?}', 'AdminEbookController@eBookEdit')->name('ebook.edit');
    Route::post('/update', 'AdminEbookController@updateEbook')->name('ebook.update');
    Route::post('/new-page-add', 'AdminEbookController@newPageAdd')->name('new-page-add');
    Route::post('/delete/ebook/template', 'AdminEbookController@adminEbookDelete')->name('delete-ebook-template');
    Route::post('/delete/page/form/book', 'AdminEbookController@adminEbookPageDelete')->name('ebook-page-delete-form-book');
    Route::post('/sort-pages', 'AdminEbookController@adminEbookPageSort')->name('sort-pages');
    Route::post('/get-single-content-by-id', 'AdminEbookController@getSingleContentById')->name('get-single-content-by-id');

});
