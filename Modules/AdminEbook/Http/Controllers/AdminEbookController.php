<?php

namespace Modules\AdminEbook\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\AdminEbook\Contracts\Service\AdminEbookTemplateContact;

class AdminEbookController extends Controller
{
    /**
     * @var AdminEbookTemplateContact
     */
    private $adminEbookTemplateService;

    /**
     * AdminEbookController constructor.
     * @param AdminEbookTemplateContact $adminEbookTemplateService
     */
    public function __construct(AdminEbookTemplateContact $adminEbookTemplateService)
    {

        $this->adminEbookTemplateService = $adminEbookTemplateService;
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|Response|\Illuminate\View\View
     */
    public function index()
    {
        return view('adminebook::index');
    }

    public function addEbook()
    {
        return view('adminebook::add-ebook');
    }

    public function ebookList(Request $request)
    {
        return $this->adminEbookTemplateService->getAllAdminEbookTemplateList($request, null);
    }

    public function storeEbook(Request $request)
    {
        return response()->json($this->adminEbookTemplateService->storeEbook($request, auth()->id()));
    }

    public function previewEbook($ebookId)
    {
        return $this->adminEbookTemplateService->previewEbook($ebookId);
    }

    public function eBookNameAdd(Request $request)
    {
        return response()->json($this->adminEbookTemplateService->eBookNameAdd($request));
    }

    public function eBookEdit($eBookId , $pageId = null)
    {
        if (is_null($pageId)){
            $result = $this->adminEbookTemplateService->getFirstPageUsingEBookId($eBookId);
        }else{
            $result = $this->adminEbookTemplateService->getPageData($eBookId, $pageId);
        }
        $allPages = $this->adminEbookTemplateService->getAllAdminEbookPagesByBookIdSortByPage($eBookId);


        return view('adminebook::edit-ebook')->with([
            'content' => $result->content,
            'page_no' => $result->page_no,
            'id' => $result->id,
            'admin_ebook_template_id' => $result->admin_ebook_template_id,
            'all_pages' => $allPages,
            'total_page' => count($allPages),
        ]);
    }

    public function updateEbook(Request $request)
    {
        return $this->adminEbookTemplateService->updateEbook($request);
    }

    public function newPageAdd(Request $request)
    {
        return $this->adminEbookTemplateService->newPageAdd($request);
    }

    public function adminEbookDelete(Request $request)
    {
        return response()->json( $this->adminEbookTemplateService->deleteEbookById($request['id']));
    }

    public function adminEbookPageDelete(Request $request)
    {
        return response()->json($this->adminEbookTemplateService->adminEbookPageDelete($request));
    }

    public function adminEbookPageSort(Request $request)
    {
        return response()->json($this->adminEbookTemplateService->adminEbookPageSort($request));
    }

    public function getSingleContentById(Request $request)
    {
        return response()->json($this->adminEbookTemplateService->getSingleContentById($request));
    }


}
