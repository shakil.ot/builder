<?php

namespace Modules\AdminEbook\Http\Services;

use Modules\AdminEbook\Contracts\Repositories\AdminEbookTemplateDetailsRepository;
use Modules\AdminEbook\Contracts\Service\AdminEbookTemplateContact;
use Modules\AdminEbook\Contracts\Repositories\AdminEbookTemplateRepository;
use phpDocumentor\Reflection\Types\This;
use Yajra\DataTables\DataTables;
use DB;
use Log;
use PDF;

class AdminEbookTemplateService implements AdminEbookTemplateContact
{
    /**
     * @var AdminEbookTemplateRepository
     */
    private $adminEbookTemplateRepository;
    /**
     * @var AdminEbookTemplateDetailsRepository
     */
    private $adminEbookTemplateDetailsRepository;

    /**
     * AdminEbookTemplateService constructor.
     * @param AdminEbookTemplateRepository $adminEbookTemplateRepository
     * @param AdminEbookTemplateDetailsRepository $adminEbookTemplateDetailsRepository
     */
    public function __construct(AdminEbookTemplateRepository $adminEbookTemplateRepository,
                                AdminEbookTemplateDetailsRepository $adminEbookTemplateDetailsRepository
    )
    {
        $this->adminEbookTemplateRepository = $adminEbookTemplateRepository;
        $this->adminEbookTemplateDetailsRepository = $adminEbookTemplateDetailsRepository;
    }

    public function getAllAdminEbookTemplateList($request, $userId)
    {
        $templates = $this->adminEbookTemplateRepository->getAllAdminEbookTemplateList($request, $userId);

        return Datatables::of($templates)
            ->addColumn('title', function ($templates) {
                if ($templates->title == null) {
                    return '<span class="label label-lg label-light-danger  label-inline">N/A</span>';
                } else {
                    return '<span class="label label-lg label-success label-inline">' . $templates->title . '</span>';
                }
            })
            ->addColumn('created_at', function ($templates) {
                return '<span class="label label-lg label-light-info label-inline">' . $templates->created_at->format('d-m-yy') . '</span>';
            })
            ->addColumn('action', function ($templates) {
                $btn = '<div><a href="' . route('admin.ebook.preview', ['ebookId' => $templates->id]) . '" 
                        class="viewTemplate" id="templateId"><i class="fas fa-eye"></i> Preview </span></a>'
                    .

                    '   <a href="' . route('admin.ebook.edit', ['ebookId' => $templates->id]) . '" class="editTemplate" id="templateId"
                    data-id="' . $templates->id . ' ">
                    <i class="la la-pencil"></i> Edit </span>
                    </a>'
                    .
                    '<a href="javascript:void(0)" class="action"
                    data-value="delete-admin-template" data-id="' . $templates->id . '"><i class="la la-trash-o"></i> Delete
                    </a>
                </div>';
                return $btn;
            })
            ->rawColumns(['title', 'created_at', 'action'])
            ->make(true);
    }

    public function storeEbook($request, $userId)
    {
        if (is_null($request['ebookContent'])) {
            return [
                'html' => 'Please Build first web page',
                'status' => false,
            ];
        }

        DB::beginTransaction();

        try {
            $ebook = $this->adminEbookTemplateRepository->storeEbook([
                "title" => $request['template_title'],
            ]);

            if (!empty($ebook->id)) {
                $pageId = $this->adminEbookTemplateDetailsRepository->storeEbookPage([
                    "admin_ebook_template_id" => $ebook->id,
                    "content" => $request['ebookContent'],
                    "page_no" => 1,
                ]);
            } else {
                throw new \Exception('Ebook Not Saved Successfully');
            }

            DB::commit();

            return ['status' => true, 'messsage' => 'Ebook Saved Successfully!', 'pageId' => $pageId];
        } catch (\Exception $e) {
            DB::rollback();
            Log::info($e->getMessage());
            return ['status' => false, 'messsage' => 'Ebook Saved Successfully!'];
        }

    }

    public function previewEbook($ebookId)
    {
        $ebook = $this->adminEbookTemplateRepository->getById($ebookId);

        $html = '';


        foreach($ebook->admin_ebook_template_details as $value)
        {
            $view = view('pdf.document')->with(['data' => $value->content]);
            $html .= $view->render(). '<div style="page-break-before: always;"></div>';

        }

        $start = '<html><body>';
        $end= '</body></html>';

        $pdf = PDF::loadHTML($start.$html.$end);
        return $pdf->stream('document.pdf');


        $pdf = PDF::loadHTML($html);
        return $pdf->stream('document.pdf');
    }

    public function eBookNameAdd($request)
    {
        DB::beginTransaction();

        try {
            $ebook = $this->adminEbookTemplateRepository->storeEbook([
                "title" => $request['name'],
            ]);

            if (!empty($ebook->id)) {
                $pageId = $this->adminEbookTemplateDetailsRepository->storeEbookPage([
                    "admin_ebook_template_id" => $ebook->id,
                    "content" => '',
                    "page_no" => 1,
                ]);
            } else {
                throw new \Exception('Ebook Not Saved Successfully');
            }

            DB::commit();

            return [
                'status' => true,
                'html' => 'Ebook Saved Successfully!',
                'pageId' => $pageId,
                'url' => route('admin.ebook.edit', ['ebookId' => $ebook->id, 'pageId' => $pageId->id])
            ];

        } catch (\Exception $e) {
            DB::rollback();
            Log::info($e->getMessage());
            return ['status' => false, 'html' => 'Ebook Saved Successfully!'];
        }
    }

    public function getPageData($eBookId, $pageId)
    {
        return $this->adminEbookTemplateDetailsRepository->getByCondition([
            'admin_ebook_template_id' => $eBookId,
            'id' => $pageId
        ]);
    }

    public function updateEbook($request)
    {
        $result = $this->adminEbookTemplateDetailsRepository->updateOrCreateData([
            'id' => $request['id'],
            'admin_ebook_template_id' => $request['admin_ebook_template_id'],
        ], [
            'content' => $request['ebookContent'],
            'image' => isset($request['image']) ? $request['image'] : '',
        ]);

        if ($result->page_no == 1){
            $this->adminEbookTemplateRepository->updateOrCreateData([
                'id' => $request['admin_ebook_template_id'],
            ] , [
                'image' => isset($request['image']) ? $request['image'] : '',
            ]);
        }

        if ($result) {
            return [
                'status' => true,
                'html' => 'Ebook Page Edited Successfully!',
            ];
        }

        return [
            'status' => false,
            'html' => 'Something is wrong !!',
        ];

    }

    public function getFirstPageUsingEBookId($eBookId)
    {
        return $this->adminEbookTemplateDetailsRepository->getByCondition([
            'admin_ebook_template_id' => $eBookId,
            'page_no' => 1
        ]);
    }

    public function getAllAdminEbookPagesByBookId($eBookId)
    {
        return $this->adminEbookTemplateDetailsRepository->getAllByCondition([
            'admin_ebook_template_id' => $eBookId,
        ]);
    }

    public function newPageAdd($request)
    {
        $result = $this->adminEbookTemplateDetailsRepository->storeEbookPage([
            "admin_ebook_template_id" => $request['admin_ebook_template_id'],
            "content" => '',
            "page_no" => $request['total_page'] + 1,
        ]);


        return [
            'status' => true,
            'html' => 'Ebook Added Successfully!',
            'pageId' => $result->id,
            'url' => route('admin.ebook.edit', ['ebookId' => $request['admin_ebook_template_id'], 'pageId' => $result->id])
        ];

    }


    public function getEBookMainPage()
    {
       return $this->adminEbookTemplateRepository->getAllByCondition(['status' => 1 ] , ['id' , 'title' , 'image']);
    }

    public function deleteEbookById($id)
    {
       $delet =  $this->adminEbookTemplateRepository->deleteEbookById($id);
       if($delet){
           return [
               'status' => true,
               'html' => 'Ebook Page Deleted Successfully!',
           ];
       }
        return [
            'status' => true,
            'html' => 'Ebook Page deleted unsuccessfully!',
        ];
    }

    public function adminEbookPageDelete($request)
    {
        $response = $this->adminEbookTemplateDetailsRepository->getAllPagesOrderByPage(
            [
                'admin_ebook_template_id' => $request['admin_ebook_template_id']
            ],
            ['id']
        );

        if (count($response) == 1){
            return [
                'html' => 'Can not delete last page!!',
                'status' => false,
            ];
        }

        $result = $this->adminEbookTemplateDetailsRepository->deleteByCondition([
            'admin_ebook_template_id' => $request['admin_ebook_template_id'],
            'id' => $request['page_id']
        ]);

        if ($result){

            $counter = 1;

            foreach ($response as $value){
                if ($value->id != $request['page_id']){
                    $this->adminEbookTemplateDetailsRepository->updateOrCreateData([
                        'id' => $value->id,
                    ] , [
                        'page_no' => $counter
                    ]);
                    $counter++;
                }
            }

            return [
                'html' => 'Page deleted successfully ',
                'status' => true,
                'redirect_url' => route('admin.ebook.edit', ['ebookId' => $request['admin_ebook_template_id']])
            ];
        }

        return [
            'status' => false,
            'html' => 'Something is wrong !!',
        ];
    }

    public function adminEbookPageSort($request)
    {
        $counter = 1;

        foreach (explode(",", rtrim($request['sortIds'], ",")) as $id) {
            $e = $this->adminEbookTemplateDetailsRepository->updateOrCreateData([
                'id' => $id,
            ] , [
                'page_no' => $counter
            ]);
            $counter++;

        }

        return [
            'html' => 'Page move successfully ',
            'status' => true
        ];

    }

    public function getFirstPageUsingEBookIdSortByPage($eBookId)
    {
        return $this->adminEbookTemplateDetailsRepository->getPagesOrderByPage([
            'admin_ebook_template_id' => $eBookId,
            'page_no' => 1
        ]);
    }

    public function getPageDataSortByPage($eBookId, $pageId)
    {
        return $this->adminEbookTemplateDetailsRepository->getPagesOrderByPage([
            'admin_ebook_template_id' => $eBookId,
            'id' => $pageId
        ]);
    }

    public function getAllAdminEbookPagesByBookIdSortByPage($eBookId)
    {
        return $this->adminEbookTemplateDetailsRepository->getAllPagesOrderByPage([
            'admin_ebook_template_id' => $eBookId,
        ]);
    }

    public function getSingleContentById($request)
    {
        $result = $this->adminEbookTemplateDetailsRepository->getByCondition([
            'id' => $request['id'],
        ]);

        if ($result) {
            return [
                'html' => '',
                'status' => true,
                'data' => $result,
            ];
        }

        return [
            'html' => 'Somethings is wrong !!',
            'status' => false
        ];
    }
}
