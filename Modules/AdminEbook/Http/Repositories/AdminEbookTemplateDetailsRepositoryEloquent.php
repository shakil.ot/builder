<?php

namespace Modules\AdminEbook\Http\Repositories;

use App\Repositories\BaseRepository\BaseRepository;
use Modules\AdminEbook\Contracts\Repositories\AdminEbookTemplateDetailsRepository;
use Modules\AdminEbook\Entities\AdminEbookTemplateDetail;


class AdminEbookTemplateDetailsRepositoryEloquent extends BaseRepository implements AdminEbookTemplateDetailsRepository
{
    protected function model()
    {
        return new AdminEbookTemplateDetail();
    }

    public function storeEbookPage($data)
    {
        return $this->model->create($data);
    }

    public function getByCondition($where)
    {
        return $this->model->where($where)->first();
    }

    public function updateOrCreateData($where, $data)
    {
        return $this->model->updateOrCreate($where, $data);
    }

    public function getAllByCondition($where)
    {
        return $this->model->where($where)->get();
    }

    public function deleteByCondition($where)
    {
        return $this->model->where($where)->delete();
    }

    public function getAllPagesOrderByPage($where , $select = ['*'])
    {
        return $this->model->select($select)->where($where)->orderBy('page_no', 'asc')->get();
    }

    public function getPagesOrderByPage($where , $select = ['*'])
    {
        return $this->model->select($select)->where($where)->orderBy('page_no', 'asc')->first();
    }
}
