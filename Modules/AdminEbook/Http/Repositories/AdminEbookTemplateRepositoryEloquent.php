<?php

namespace Modules\AdminEbook\Http\Repositories;

use App\Repositories\BaseRepository\BaseRepository;
use Modules\AdminEbook\Entities\AdminEbookTemplate;

use Modules\AdminEbook\Contracts\Repositories\AdminEbookTemplateRepository;


class AdminEbookTemplateRepositoryEloquent extends BaseRepository implements AdminEbookTemplateRepository
{
    protected function model()
    {
        return new AdminEbookTemplate();
    }

    public function getById($ebookId)
    {
        return $this->model->with('admin_ebook_template_details')->where('id', $ebookId)->first();
    }

    public function getAllAdminEbookTemplateList($request, $userId, $select = ["*"])
    {
        return $this->model->select($select)->orderBy('id' , 'DESC');
    }

    public function storeEbook($data)
    {
        return $this->model->create($data);
    }

    public function getAllByCondition($where , $select = ["*"])
    {
        return $this->model->select($select)->where($where)->orderBy('id' , 'DESC')->get();
    }

    public function deleteEbookById($id)
    {
        return $this->model->where('id', $id)->delete();
    }

    public function updateOrCreateData($where, $update)
    {
        return $this->model->updateOrCreate($where, $update);
    }
}
