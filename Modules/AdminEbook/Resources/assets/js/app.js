var AdminWebBuilder = (function () {

    $(function () {

        // Add overflow_visible to prevent hidden action option
        $('.visible.m-portlet__body.extra.p-0').parents('.m-grid__item.m-grid__item--fluid.m-wrapper').addClass('overflow_visible');

        var user_operation_callback = function (response) {
            Swal.fire(response.status, response.html, response.status);
            $('#admin-ebook-table').DataTable().ajax.reload();
        }


        const _token = document.head.querySelector("[property=csrf-token]").content;


        (function (global, $, _token) {

            // #### DATATABLE FOR USER-LIST. ALL USER LIST FITCH FROM DATABASED
            var dataTable = $('#admin-ebook-table').DataTable({
                responsive: true,
                lengthMenu: [10, 25, 50, 100, 500],
                pageLength: 10,
                language: {
                    'lengthMenu': 'Display _MENU_',
                },
                searchDelay: 500,
                processing: true,
                serverSide: true,
                ajax: {
                    url: route('admin.ebook.list') ,
                    type: 'get',
                },
                columns: [
                    {data: 'title', name: 'title', "searchable": true, "visible": true , width: "30%"},
                    {data: 'created_at', name: 'created_at', "orderable": true, "searchable": false, width: "20%"},
                    {data: 'action', name: 'action', "orderable": false, searchable: false, width: "20%"}
                ],
                dom: '<"top-toolbar row"<"top-left-toolbar col-md-9"lB><"top-right-toolbar col-md-3"f>>rt<"bottom-toolbar"<"bottom-left-toolbar"i><"bottom-right-toolbar"p>>',
                buttons: [
                    {
                        extend: 'colvis',
                        text: '<i class="fa \tfa-list-ul pr-1"></i>Columns',
                        attr: {
                            id: "custom-colvis-button",
                            class: 'btn btn-outline-primary btn-sm m-btn',
                            style: '',
                            title: 'Columns Visibility'
                        },
                        columns: ':not(.noVis)',
                        columnText: function (dt, idx, title) {
                            // return (idx+1)+': '+title;
                            return title;
                        },
                        postfixButtons: ['colvisRestore']
                    },
                    {
                        extend: 'colvisGroup',
                        text: '<i class="fa fa-refresh pr-1"></i>Refresh',
                        attr: {
                            class: 'btn btn-outline-primary btn-sm m-btn  m-btn--icon',
                            id: 'show-all-button'
                        },
                        show: ':hidden'
                    }
                ]
            });

            //Columns visibility buttons' css
            $("#custom-colvis-button").click(function () {
                $(".buttons-columnVisibility").removeClass("dt-button").addClass("btn btn-outline-primary btn-sm m-btn btn-block");
                $(".buttons-colvisRestore").removeClass("dt-button").addClass("btn btn-outline-brand btn-sm m-btn btn-block");
            });

            $("#show-all-button").click(function () {
                refreshTable();
            });

            /* reset search starts*/
            function refreshTable(params) {
                dataTable.columns().search('');
                dataTable.search('');
                dataTable.ajax.reload();
            }
            /* reset search ends*/


            $(document).on('click','.action', function (e) {
                var id = $(this).attr('data-id');
                var settingsType = $(this).attr('data-value');
                console.log(settingsType);

                if (!settingsType)
                    return false;

                switch (settingsType) {
                    case "delete-admin-template":
                        deleteAdminTemplate(id);
                        break;
                    default :
                        toastr.error("Invalid Setting type", "Server Notification");
                        break;
                }

                function deleteAdminTemplate(id) {


                    Swal.fire({
                        title: "Are you sure ?",
                        text: "You won't be able to retrieve this E-book!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete this E-book!",
                        cancelButtonText: "No, cancel please!",
                        closeOnConfirm: false,
                        closeOnCancel: true
                    }).then(function(result) {
                        if (result.value) {
                            let dataObject = {
                                url: route('admin.delete-ebook-template'),
                                type: 'post',
                                data: {id: id, _token: _token},
                                callback: user_operation_callback,
                                callBackError: callBackError,
                            }
                            globalAjax(dataObject);
                        }
                    });
                }
            });

            $(document).on('click', '#ebook-add-btn' , function (){
                $('#bookAddModal').modal('show');
            });

            $(document).on('click', '#add-button-for-ebook-name' , function (){
                if($('#book_name').val() == ''){
                    toastr.error('Name Can not be empty !!');
                    return false;
                }

                $.ajax({
                    url: route('admin.e-book-name-add'),
                    type: "POST",
                    data: {
                        name : $('#book_name').val(),
                        _token: _token,
                    },
                    success: function (response) {
                        if (response.status) {
                            toastr.success(response.html);
                            window.location = response.url
                        } else {
                            toastr.error(response.html);
                        }
                    }
                });

            });


        })(window, jQuery, _token);
    });

})();



