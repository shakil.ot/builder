<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Admin Web Builder</title>
    <link href="{{ asset('builder/grapes.min.css') }}" rel="stylesheet">
    <link href="{{ asset('builder/grapesjs-preset-newsletter.css') }}" rel="stylesheet">

{{--    <script src="{{ asset('builder/filestack-0.1.10.js') }}"></script>--}}
    <script src="{{ asset('assets/global/plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('builder/aviary.js') }}"></script>
    <script src="{{ asset('builder/grapes.min.js') }}"></script>
    <script src="{{ asset('builder/ckeditor.js') }}"></script>
    <script src="{{ asset('builder/grapesjs-plugin-ckeditor.min.js') }}"></script>
    <script src="{{ asset('builder/grapesjs-preset-newsletter.min.js') }}"></script>
    <script src="{{ asset('builder/grapesjs-aviary.min.js') }}"></script>

    <script src="{{ asset('assets/global/plugins/toastr/toastr.min.js') }}"></script>
    <link href="{{ asset('assets/global/plugins/toastr/toastr.min.css') }}" rel="stylesheet">


    <script src="https://unpkg.com/grapesjs-plugin-forms"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        body,
        html {
            height: 100%;
            margin: 0;
        }

        .save__as__table tr td {
            padding: 10px;
        }

        .template_title, .template-type {
            display: block;
            width: -moz-available;
            width: -webkit-fill-available;
            padding: .375rem .75rem;
            font-size: 1rem;
            line-height: 1.5;
            color: #495057;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            border-radius: .25rem;
            transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
        }

        .template_cell {
            width: 8%;
            display: table-cell;
            height: 75px;
        }

        .template_save_btn {
            width: -moz-available;
            width: -webkit-fill-available;
            background-color: #1c2c6b;
            cursor: pointer;
            border-radius: 5px;
            border: 1px solid #1c2c6b;
            font-size: 15px;
            color: white;
            padding: 7px;
        }

        .gjs-mdl-dialog-sm {
            width: 40%;
        }
    </style>
</head>
<body>

<div id="gjs" style="height:0px; overflow:hidden"></div>

<script type="text/javascript">

    var adminEbookBuilderObj = {
        form_action_url : 'https://google.com/shakil',
        form_action_name : 'google dsfsdg',
        grapesJsSelectAndSaveOption: '{!! ViewHelper::grapesJsEbookSelectAndSaveOption() !!}',
        update_image_url: '{!! route('update-image') !!}',
        admin_ebook_add_url: '{!! route('admin.ebook.store') !!}',
        admin_ebook_redirect_url: "{{ route('admin.ebook')}}",
        default_style: '{{ \Modules\WebBuilder\Entities\WebBuilder::DEFAULT_STYLE }}'
    };
</script>
<script type="text/javascript">
    var host = '//artf.github.io/grapesjs/';
    var images = [
        host + 'img/grapesjs-logo.png',
        host + 'img/tmp-blocks.jpg',
        host + 'img/tmp-tgl-images.jpg',
        host + 'img/tmp-send-test.jpg',
        host + 'img/tmp-devices.jpg',
    ];

    const _token = $('meta[name="csrf-token"]').attr('content');
    var ebookContent = '';

    // Set up GrapesJS editor with the Newsletter plugin
    var editor = grapesjs.init({
        clearOnRender: true,
        height: '100%',
        storageManager: {
            id: 'gjs',
        },
        assetManager: {
            assets: images,
            upload: 0,
            uploadText: 'Uploading is not available in this demo',
        },
        container : '#gjs',
        fromElement: true,
        plugins: ['gjs-preset-newsletter', 'gjs-plugin-ckeditor'],
        pluginsOpts: {
            'gjs-preset-newsletter': {
                modalLabelImport: 'Paste all your code here below and click import',
                modalLabelExport: 'Copy the code and use it wherever you want',
                codeViewerTheme: 'material',
                //defaultTemplate: templateImport,
                importPlaceholder: '<table class="table"><tr><td class="cell">Hello world!</td></tr></table>',
                cellStyle: {
                    'font-size': '12px',
                    'font-weight': 300,
                    'vertical-align': 'top',
                    color: 'rgb(111, 119, 125)',
                    margin: 0,
                    padding: 0,
                }
            },
            'gjs-plugin-ckeditor': {
                position: 'center',
                options: {
                    startupFocus: true,
                    extraAllowedContent: '*(*);*{*}', // Allows any class and any inline style
                    allowedContent: true, // Disable auto-formatting, class removing, etc.
                    enterMode: CKEDITOR.ENTER_BR,
                    extraPlugins: 'sharedspace,justify,colorbutton,panelbutton,font',
                    toolbar: [
                        { name: 'styles', items: ['Font', 'FontSize' ] },
                        ['Bold', 'Italic', 'Underline', 'Strike'],
                        {name: 'paragraph', items : [ 'NumberedList', 'BulletedList']},
                        {name: 'links', items: ['Link', 'Unlink']},
                        {name: 'colors', items: [ 'TextColor', 'BGColor' ]},
                    ],
                }
            }
        }
    });

    editor.Panels.addButton
    ('options',
        [{
            id: 'save-db',
            className: 'fa fa-floppy-o',
            command: 'save-db',
            attributes: {title: 'Upload'}
        }]
    );


    // Let's add in this demo the possibility to test our newsletters
    var mdlClass = 'gjs-mdl-dialog-sm';
    var pnm = editor.Panels;
    var cmdm = editor.Commands;
    var md = editor.Modal;

    // Add info command
    var infoContainer = document.getElementById("info-panel");
    cmdm.add('open-info', {
        run: function(editor, sender) {
            sender.set('active', 0);
            var mdlDialog = document.querySelector('.gjs-mdl-dialog');
            mdlDialog.className += ' ' + mdlClass;
            infoContainer.style.display = 'block';
            md.setTitle('About this demo');
            md.setContent('');
            md.setContent(infoContainer);
            md.open();
            md.getModel().once('change:open', function() {
                mdlDialog.className = mdlDialog.className.replace(mdlClass, '');
            })
        }
    });
    pnm.addButton('options', [{
        id: 'undo',
        className: 'fa fa-undo',
        attributes: {title: 'Undo'},
        command: function(){ editor.runCommand('core:undo') }
    },{
        id: 'redo',
        className: 'fa fa-repeat',
        attributes: {title: 'Redo'},
        command: function(){ editor.runCommand('core:redo') }
    },{
        id: 'clear-all',
        className: 'fa fa-trash icon-blank',
        attributes: {title: 'Clear canvas'},
        command: {
            run: function(editor, sender) {
                sender && sender.set('active', false);
                if(confirm('Are you sure to clean the canvas?')){
                    editor.DomComponents.clear();
                    setTimeout(function(){
                        localStorage.clear()
                    },0)
                }
            }
        }
    },{
        id: 'view-info',
        className: 'fa fa-question-circle',
        command: 'open-info',
        attributes: {
            'title': 'About',
            'data-tooltip-pos': 'bottom',
        },
    }]);

    // Simple warn notifier
    var origWarn = console.warn;
    toastr.options = {
        closeButton: true,
        preventDuplicates: true,
        showDuration: 250,
        hideDuration: 150
    };
    console.warn = function (msg) {
        toastr.warning(msg);
        origWarn(msg);
    };

    // Beautify tooltips
    var titles = document.querySelectorAll('*[title]');
    for (var i = 0; i < titles.length; i++) {
        var el = titles[i];
        var title = el.getAttribute('title');
        title = title ? title.trim(): '';
        if(!title)
            break;
        el.setAttribute('data-tooltip', title);
        el.setAttribute('title', '');
    }


    // Do stuff on load
    editor.on('load', function() {
        var $ = grapesjs.$;

        //Do something here after load
    });

    // Add the command
    var loadModal = editor.Modal;
    editor.Commands.add
    ('save-db',
        {
            run: function (editor, sender) {

                sender.set('active', 0);
                var modalContent = loadModal.getContentEl();
                var mdlDialog = document.querySelector('.gjs-mdl-dialog');
                // var cmdGetCode = cmdm.get('gjs-get-inlined-html');
                // contentEl.value = cmdGetCode && cmdGetCode.run(editor);
                mdlDialog.className += ' ' + mdlClass;
                // testContainer.style.display = 'block';
                loadModal.setTitle('Save Your Template');
                // md.setContent('');
                loadModal.setContent(adminEbookBuilderObj.grapesJsSelectAndSaveOption);
                loadModal.open();
                $('.gjs-mdl-dialog').find('.template_title').attr('placeholder', 'Enter your ebook title')
                loadModal.getModel().once('change:open', function () {
                    mdlDialog.className = mdlDialog.className.replace(mdlClass, '');
                    document.getElementsByClassName("template_title").placeholder = "Type name here..";
                    //clean status
                });
                ebookContent = editor.runCommand('gjs-get-inlined-html');

                sender && sender.set('active', 0); // turn off the button
                editor.store();
            }
        });

    $(document).on("click", ".template_save_btn", function () {

        if (ebookContent === ''){
            toastr.error('Please Build a Template');
            return false;
        }

        if ($('.template_title').val() === ''){
            toastr.error('Template Title Required');
            return false;
        }

        const ebookBuilder = {
            ebookContent: ebookContent,
            template_title: $('.template_title').val(),
            _token: _token,
        }

        $.ajax({
            url: adminEbookBuilderObj.admin_ebook_add_url,
            type: "POST",
            data: ebookBuilder,
            success: function (response) {
                if (response.status){
                    toastr.success(response.messsage)
                    // window.location.href = adminEbookBuilderObj.admin_ebook_redirect_url;
                }else {
                    toastr.error(response.messsage)
                }
            }
        });
    });
</script>
</body>
</html>