<div class="col-md-12 filter-options" style="background-color: #f0f0f0; display:none;">
    <form class="m-form">
        <div class="row mt-3 mb-3 pt-3 pb-3">
            <div class="col-md-6">
                <div class="row">
                    <div class="form-group col-md-10">
                        <label class="form-control-label m--font-bold">
                            Search Title
                        </label>
                        <input id="search-title" type="text" class="form-control m-input"
                               placeholder="Search title"
                               style="font-family: Poppins, FontAwesome;" data-column="1">

                    </div>
                    <div class="form-group col-md-2  m--margin-top-35">
                        <a href="javascript:void(0);"
                           class="btn btn-outline-metal m-btn m-btn--icon btn-xs m-btn--icon-only m-btn--pill reset-user-email-search"
                           data-column="1">
                            <i class="fa fa-remove"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="form-group col-md-10">
                        <label class="form-control-label m--font-bold">
                            Select Status
                        </label>

                        <select id="search-user-status"
                                class="form-control m-bootstrap-select m_selectpicker" data-column="2">
                            <option value="" data-icon="la la-arrow-circle-o-down">
                                Select Template Type
                            </option>
                            @foreach(\Modules\AdminWebBuilder\Entities\AdminWebTemplate::ADMIN_WEB_TYPE_ARRAY as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>

                    </div>
                    <div class="form-group col-md-2  m--margin-top-35">
                        <a href="javascript:void(0);"
                           class="btn btn-outline-metal m-btn m-btn--icon btn-xs m-btn--icon-only m-btn--pill reset-user-status-search"
                           data-column="2">
                            <i class="fa fa-remove"></i>
                        </a>
                    </div>
                </div>

            </div>


        </div>

    </form>
</div>
