<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Admin Web Builder</title>
    <link href="{{ asset('builder/grapes.min.css') }}" rel="stylesheet">
    <link href="{{ asset('builder/grapesjs-preset-newsletter.css') }}" rel="stylesheet">

    {{--    <script src="{{ asset('builder/filestack-0.1.10.js') }}"></script>--}}
    <script src="{{ asset('assets/global/plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('builder/aviary.js') }}"></script>
    <script src="{{ asset('builder/grapes.min.js') }}"></script>
    <script src="{{ asset('builder/ckeditor.js') }}"></script>
    <script src="{{ asset('builder/grapesjs-plugin-ckeditor.min.js') }}"></script>
    <script src="{{ asset('builder/grapesjs-preset-newsletter.min.js') }}"></script>
    <script src="{{ asset('builder/grapesjs-aviary.min.js') }}"></script>

    <script src="{{ asset('assets/global/plugins/toastr/toastr.min.js') }}"></script>
    <link href="{{ asset('assets/global/plugins/toastr/toastr.min.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('builder/sweetalert2.min.css') }}" crossorigin="anonymous"/>
    <script src="{{ asset('builder/sweetalert2.min.js') }}" crossorigin="anonymous"></script>

    <script src="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"></script>

    <link rel="stylesheet" href="{{asset('assets/preloader/preloader.css')}}">

    <script src="https://unpkg.com/grapesjs-plugin-forms"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        body,
        html {
            height: 100%;
            margin: 0;
        }

        .save__as__table tr td {
            padding: 10px;
        }

        .template_title, .template-type {
            display: block;
            width: -moz-available;
            width: -webkit-fill-available;
            padding: .375rem .75rem;
            font-size: 1rem;
            line-height: 1.5;
            color: #495057;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            border-radius: .25rem;
            transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
        }

        .template_cell {
            width: 8%;
            display: table-cell;
            height: 75px;
        }

        .template_save_btn {
            width: -moz-available;
            width: -webkit-fill-available;
            background-color: #1c2c6b;
            cursor: pointer;
            border-radius: 5px;
            border: 1px solid #1c2c6b;
            font-size: 15px;
            color: white;
            padding: 7px;
        }

        .gjs-mdl-dialog-sm {
            width: 40%;
        }

        .editor-row {
            display: flex;
            justify-content: flex-start;
            align-items: stretch;
            flex-wrap: nowrap;
            height: 100%;
        }

        .editor-canvas {
            flex-grow: 1;
        }

        .panel__right {
            flex-basis: 230px;
            position: relative;
            overflow-y: auto;
            height: calc(100vh - 80px);
            margin-top: 40px;
            margin-bottom: 40px;
        }
        .layers-container {
            display: flex;
            flex-direction: column;
            align-items: center;
            /* justify-content: space-evenly; */
            height: 100%;
            overflow-y: auto;
            padding-left: 30px;
        }

        .layers-container::-webkit-scrollbar {
            width: 1em;
        }

        .layers-container::-webkit-scrollbar-track {
            box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
        }

        .layers-container::-webkit-scrollbar-thumb {
            background-color: darkgrey;
            outline: 1px solid slategrey;
        }

        .page_list_box {
            text-align: center;
            border: 4px solid #ddd;
            padding: 0;
            background: aliceblue;
            font-size: 30px;
            margin-top: 2px;
            margin-right: 20px;
            margin-bottom: 2px;
            margin-left: 20px;
            /* padding: 0; */
            min-height: 150px;
            width: 110px;
            box-shadow: 0 3px 2px 0px rgba(157, 147, 147, 0.75);
            position: relative;
            background-size: cover;
            background-position: center center;
        }
        .page_list_box.sk_active::before {
            content: '';
            width: 20px;
            height: 20px;
            background: #023c64;
            display: block;
            position: absolute;
            z-index: 1;
            transform: rotate(45deg) translateY(-50%);
            right: -5px;
            top: 50%;
            z-index: -1;
        }

        .page_list_box + .page_list_box {
            margin-top: 10px;
        }
        a {
            display: block;
            text-decoration: none;
        }

        .sk_active {
            border-color:  #03385d ;
        }

        .sk_box {
            border: 1px solid red;
            width: 30%;
            height: 204px;
            float: left;
            margin: 4px;
            cursor: pointer;
        }

        .sk_overflow_y {
            max-height: 400px;
            overflow-y: auto;
        }

        .sk_image_set {
            width: 100%;
            height: 100%;
            cursor: pointer;
        }

        #page__number {
            position: absolute;
            left: -45px;
            font-size: 17px;
            color: #ddd;
            top: 50%;
            transform: translateY(-50%);
        }
        .sk_active #page__number,
        .sk_active #page__number i{
            color: #023c64;
        }
        .total__pages {
            margin: 0;
            padding: 0;
            font-size: 18px;
            position: absolute;
            top: 7px;
            z-index: 9999;
            color: #000;
            border-bottom: 2px solid #023c64;
            left: 0;
            right: 0;
            width: 206px;
            text-align: center;
            padding-bottom: 3px;
            font-family: Arial, Helvetica, sans-serif;
        }
        .total__pages span {
            color: #023c64;
        }

        .editor-row ::-webkit-scrollbar {
            width: 10px;
        }

        .editor-row ::-webkit-scrollbar-track {
            background: #000;
            border-radius: 10px;
        }

        .editor-row ::-webkit-scrollbar-thumb {
            background-color: #053a6f;
            border-radius: 10px;
        }


    </style>
</head>
<body>

<!--begin::Preloader-->
<div id="preloader">
    <div class="loading-wrapper">
        <div class="loading">
            <img class="logo-loading" src="{{ asset(env('LOGO_FAV')) }}" alt="logo">
            <span class="preloader-text">Loading...</span>
        </div>
    </div>
</div>
<!--begin::Preloader-->

<div class="editor-row">
    <p class="total__pages"><span>{{ $page_no }}</span> of {{ count($all_pages) }} Pages</p>
    <div class="panel__right">
        <div class="layers-container" id="sortable">
            @foreach($all_pages  as $page)
                <div
                   class="page_list_box {{ $page->id == $id  ?  'sk_active' : '' }}  ui-state-default change-page-content"
                   id="main_page_div_{{ $page->id }}"
                   data-id="{{ $page->id }}"
                   style="background-image: url({{ $page->image }}); cursor: pointer">
                    <span id="page__number"><i class="fa fa-list-ul"></i> {{ $page->page_no }} </span>
                <!-- <img src=" {{ $page->image }}" alt="">  -->
                </div>
            @endforeach
        </div>
    </div>
    <div class="editor-canvas">
        <div id="gjs">{!! $content !!}</div>
    </div>
</div>

<div id="gjs-new-div"></div>

<script src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript">

    setTimeout(function() {
        $('#preloader').hide();
    }, 2000);

    var adminEbookBuilderObj = {
        id : '{{ $id }}',
        grapesJsSelectAndSaveOption: '{!! ViewHelper::grapesJsEbookSelectAndSaveOption() !!}',
        update_image_url: '{!! route('update-image') !!}',
        admin_ebook_add_url: '{!! route('admin.ebook.update') !!}',
        admin_ebook_redirect_url: "{{ route('admin.ebook')}}",
        new_page_add: "{{ route('admin.new-page-add')}}",
        total_page: "{{ $total_page }}",
        admin_ebook_template_id: '{{ $admin_ebook_template_id }}',
        default_style: '{{ \Modules\WebBuilder\Entities\WebBuilder::DEFAULT_STYLE }}',
        delete_page: "{{ route('admin.ebook-page-delete-form-book')}}",
        sort_pages_url : "{{ route('admin.sort-pages')}}",
    };

</script>
<script type="text/javascript">


    const _token = $('meta[name="csrf-token"]').attr('content');

    $(function () {
        var sortIds = "";
        $("#sortable").sortable({
            stop: function (event, ui) {
                sortIds = "";
                var counter = 1;
                $(this).each(function () {
                    $(this).find("div").each(function () {
                        $(this).find('span').html('<i class="fa fa-list-ul"></i> ' + counter);
                        sortIds += $(this).attr('data-id') + ",";
                        counter++;
                    });
                });

                counter = 1;

                if (sortIds != '') {
                    $.ajax({
                        url: adminEbookBuilderObj.sort_pages_url,
                        type: "POST",
                        data: {
                            sortIds: sortIds,
                            _token: _token
                        },
                        success: function (response) {
                            if (response.status) {
                                toastr.success(response.html);
                            } else {
                                toastr.error(response.html);
                            }
                        }
                    });
                }
            }
        });
        $("#sortable").disableSelection();
    });


    localStorage.clear();

    var host = '//artf.github.io/grapesjs/';
    var images = [

    ];


    var ebookContent = '';

    // Set up GrapesJS editor with the Newsletter plugin
    var editor = grapesjs.init({
        clearOnRender: true,
        height: '100%',
        storageManager: {
            id: 'gjs',
        },
        assetManager: {
            assets: images,
            upload: 0,
            uploadText: 'Uploading is not available in this demo',
        },
        container: '#gjs',
        fromElement: true,
        plugins: ['gjs-preset-newsletter', 'gjs-plugin-ckeditor'],
        pluginsOpts: {
            'gjs-preset-newsletter': {
                modalLabelImport: 'Paste all your code here below and click import',
                modalLabelExport: 'Copy the code and use it wherever you want',
                codeViewerTheme: 'material',
                //defaultTemplate: templateImport,
                importPlaceholder: '<table class="table"><tr><td class="cell">Hello world!</td></tr></table>',
                cellStyle: {
                    'font-size': '12px',
                    'font-weight': 300,
                    'vertical-align': 'top',
                    color: 'rgb(111, 119, 125)',
                    margin: 0,
                    padding: 0,
                }
            },
            'gjs-plugin-ckeditor': {
                position: 'center',
                options: {
                    startupFocus: true,
                    extraAllowedContent: '*(*);*{*}', // Allows any class and any inline style
                    allowedContent: true, // Disable auto-formatting, class removing, etc.
                    enterMode: CKEDITOR.ENTER_BR,
                    extraPlugins: 'sharedspace,justify,colorbutton,panelbutton,font',
                    toolbar: [
                        {name: 'styles', items: ['Font', 'FontSize']},
                        ['Bold', 'Italic', 'Underline', 'Strike'],
                        {name: 'paragraph', items: ['NumberedList', 'BulletedList']},
                        {name: 'links', items: ['Link', 'Unlink']},
                        {name: 'colors', items: ['TextColor', 'BGColor']},
                    ],
                }
            }
        }
    });


    editor.Panels.addButton
    ('options',
        [{
            id: 'add-page',
            className: 'fa fa-plus-circle',
            command: 'add-page',
            attributes: {title: 'Add Page'}
        }]
    );


    // Let's add in this demo the possibility to test our newsletters
    var mdlClass = 'gjs-mdl-dialog-sm';
    var pnm = editor.Panels;
    var cmdm = editor.Commands;
    var md = editor.Modal;

    // Add info command
    var infoContainer = document.getElementById("info-panel");
    cmdm.add('open-info', {
        run: function (editor, sender) {
            sender.set('active', 0);
            var mdlDialog = document.querySelector('.gjs-mdl-dialog');
            mdlDialog.className += ' ' + mdlClass;
            infoContainer.style.display = 'block';
            md.setTitle('About this demo');
            md.setContent('');
            md.setContent(infoContainer);
            md.open();
            md.getModel().once('change:open', function () {
                mdlDialog.className = mdlDialog.className.replace(mdlClass, '');
            })
        }
    });
    pnm.addButton('options', [{
        id: 'undo',
        className: 'fa fa-undo',
        attributes: {title: 'Undo'},
        command: function () {
            editor.runCommand('core:undo')
        }
    }, {
        id: 'redo',
        className: 'fa fa-repeat',
        attributes: {title: 'Redo'},
        command: function () {
            editor.runCommand('core:redo')
        }
    }, {
        id: 'clear-all',
        className: 'fa fa-refresh icon-blank',
        attributes: {title: 'Clear canvas'},
        command: {
            run: function (editor, sender) {
                sender && sender.set('active', false);
                if (confirm('Are you sure to clean the canvas?')) {
                    editor.DomComponents.clear();
                    setTimeout(function () {
                        localStorage.clear()
                    }, 0)
                }
            }
        }
    }]);

    // Simple warn notifier
    var origWarn = console.warn;
    toastr.options = {
        closeButton: true,
        preventDuplicates: true,
        showDuration: 250,
        hideDuration: 150
    };
    console.warn = function (msg) {
        toastr.warning(msg);
        origWarn(msg);
    };

    // Beautify tooltips
    var titles = document.querySelectorAll('*[title]');
    for (var i = 0; i < titles.length; i++) {
        var el = titles[i];
        var title = el.getAttribute('title');
        title = title ? title.trim() : '';
        if (!title)
            break;
        el.setAttribute('data-tooltip', title);
        el.setAttribute('title', '');
    }


    // Do stuff on load
    editor.on('load', function () {
        var $ = grapesjs.$;

        //Do something here after load
    });

    editor.Commands.add
    ('add-page',
        {
            run: function (editor, sender) {
                $.ajax({
                    url: adminEbookBuilderObj.new_page_add,
                    type: "POST",
                    data: {
                        admin_ebook_template_id : adminEbookBuilderObj.admin_ebook_template_id,
                        total_page : adminEbookBuilderObj.total_page,
                        _token: _token,
                    },
                    success: function (response) {
                        if (response.status) {
                            toastr.success(response.html);
                            window.location = response.url
                        } else {
                            toastr.error(response.html);
                        }
                    }
                });
            }
        });


    editor.Panels.addButton
    ('options',
        [{
            id: 'delete-page-db',
            className: 'fa fa-trash',
            command: 'delete-page-db',
            attributes: {title: 'Delete Page'}
        }]
    );

    editor.Commands.add('delete-page-db', {
        run: function (editor, sender) {

            Swal.fire({
                title: 'Are you sure to remove this page ?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: adminEbookBuilderObj.delete_page,
                        type: "POST",
                        data: {
                            admin_ebook_template_id : adminEbookBuilderObj.admin_ebook_template_id,
                            page_id : adminEbookBuilderObj.id,
                            _token: _token
                        },
                        success: function (response) {
                            if (response.status) {
                                Swal.fire(
                                    'Deleted!',
                                    response.html,
                                    'success'
                                );
                                window.location.href = response.redirect_url;
                            } else {
                                toastr.error(response.html);
                            }
                            $('#gjs-new-div').html("");
                        }
                    });


                }
            })
        }
    });


    editor.Panels.addButton
    ('options',
        [{
            id: 'save-db',
            className: 'fa fa-floppy-o',
            command: 'save-db',
            attributes: {title: 'Upload'}
        }]
    );

    // Add the command
    var loadModal = editor.Modal;
    editor.Commands.add
    ('save-db',
        {
            run: function (editor, sender) {
                ebookContent = editor.runCommand('gjs-get-inlined-html');
                $('#preloader').show();
                if (ebookContent === '') {
                    toastr.error('Please Build a Template');
                    $('#preloader').hide();
                    return false;
                }

                if ($('.template_title').val() === '') {
                    toastr.error('Template Title Required');
                    $('#preloader').hide();
                    return false;
                }

                const ebookBuilder = {
                    ebookContent: ebookContent,
                    id: '{{ $id }}',
                    admin_ebook_template_id: adminEbookBuilderObj.admin_ebook_template_id,
                    _token: _token,
                }


                $('#gjs-new-div').html(ebookContent);

                html2canvas(document.getElementById("gjs-new-div"), {
                    height: 1050
                }).then(function (canvas) {
                    var image_data = canvas.toDataURL("image/jpeg");
                    ebookBuilder.image = image_data;
                    $('#main_page_div_'+ adminEbookBuilderObj.id).css('background-image','url('+ image_data +')');
                    $('#gjs-new-div').html("");

                    $.ajax({
                        url: adminEbookBuilderObj.admin_ebook_add_url,
                        type: "POST",
                        data: ebookBuilder,
                        success: function (response) {
                            if (response.status) {
                                toastr.success(response.html);
                            } else {
                                toastr.error(response.html);
                            }
                            $('#preloader').hide();

                        }
                    });

                });
            }
        });


    editor.Panels.addButton
    ('options',
        [{
            id: 'redirect-template',
            className: 'fa fa-external-link',
            command: 'redirect-template',
            attributes: {title: 'Redirect to EBook list'}
        }]
    );

    editor.Commands.add
    ('redirect-template',
        {
            run: function (editor, sender) {

                window.location.href = adminEbookBuilderObj.admin_ebook_redirect_url;
            }
        });

    $(document).on('click', '.change-page-content', function () {
        var el = $(this);
        if (el.attr('data-id') == null) {
            return false;
        }
        var id = el.attr('data-id');

        $.ajax({
            url: "{{ route('admin.get-single-content-by-id') }}",
            type: "POST",
            data: {
                id: id,
                _token: _token,
            },
            success: function (response) {
                console.log(response)
                if (response.status) {

                    editor.setComponents(response.data.content);
                    adminEbookBuilderObj.id = response.data.id;
                    $('.change-page-content').removeClass('sk_active');
                    el.addClass('sk_active');
                }
            }
        });
    });


</script>
</body>
</html>