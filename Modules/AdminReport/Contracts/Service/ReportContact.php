<?php

namespace Modules\AdminReport\Contracts\Service;

interface ReportContact
{

    public function getTransactionById($id);

    public function getAllTransaction($request = null);

    public function transactionListWithDatatable($request);

    public function todayTransaction($request);

    public function monthlyTransaction($request);

    public function yearlyTransaction($request);

    public function countTotalTransactionReports();

    public function paymentTransactionListByUser($request);

    public function stripErrorList($request);
}
