<?php

namespace Modules\AdminReport\Http\Controllers\PaymentReport;

use App\Contracts\Service\UserContact;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\AdminReport\Contracts\Service\ReportContact;

class StripeErrorReportController extends Controller
{

    /**
     * @var ReportContact
     */
    private $reportService;
    /**
     * @var UserContact
     */
    private $userService;

    public function __construct(ReportContact $reportService, UserContact $userService)
    {
        $this->reportService = $reportService;
        $this->userService = $userService;
    }


    public function stripErrorView()
    {
        return view('adminreport::StripError.index');
    }

    public function listSripeErrorDataList(Request $request)
    {
        if (!$request->ajax()) {
            return redirect()->route('admin.manage-user');
        }

        return $this->reportService->stripErrorList($request);
    }

}
