<?php

namespace Modules\AdminReport\Http\Controllers\PaymentReport;

use App\Contracts\Service\UserContact;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\AdminReport\Contracts\Service\ReportContact;

class PaymentReportController extends Controller
{

    /**
     * @var ReportContact
     */
    private $reportService;
    /**
     * @var UserContact
     */
    private $userService;

    public function __construct(ReportContact $reportService, UserContact $userService)
    {
        $this->reportService = $reportService;
        $this->userService = $userService;
    }


    public function index()
    {
        return view('adminreport::paymentReport.index');
    }


    public function paymentTransactionList(Request $request)
    {

        if (!$request->ajax()) {
            return redirect()->route('admin.manage-user');
        }

        return $this->reportService->transactionListWithDatatable($request);

    }


    public function transactionInvoiceCreate($id)
    {
        $transaction = $this->reportService->getTransactionById($id);
        $user = $this->userService->getUserById($transaction->user_id);


        return view('adminreport::paymentReport.transaction-invoice')->with(['transaction'=> $transaction, 'user' => $user]);

    }

    public function paymentTransactionListUserWisePanel($id)
    {
        return view('adminreport::paymentReport.paymentTransactionListByUserIndex')->with(['id' => base64_decode($id)]);
    }



    public function listUserTransactionData(Request $request)
    {
        if (!$request->ajax()) {
            return redirect()->route('admin.manage-user');
        }

        return $this->reportService->paymentTransactionListByUser($request);
    }

}
