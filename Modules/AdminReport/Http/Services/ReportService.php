<?php

namespace Modules\AdminReport\Http\Services;
;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Package\Contracts\Repositories\PaymentTransactionRepository;
use Modules\AdminReport\Contracts\Service\ReportContact;
use Modules\Package\Contracts\Repositories\StripeErrorRepository;
use Modules\Package\Entities\PaymentTransaction;
use Yajra\DataTables\Facades\DataTables;

class ReportService implements ReportContact
{
    /**
     * @var PaymentTransactionRepository
     */
    private $paymentTransactionRepository;
    /**
     * @var StripeErrorRepository
     */
    private $stripeErrorRepository;


    public function __construct(PaymentTransactionRepository $paymentTransactionRepository,
                                StripeErrorRepository $stripeErrorRepository)
    {

        $this->paymentTransactionRepository = $paymentTransactionRepository;

        $this->stripeErrorRepository = $stripeErrorRepository;
    }


    public function getAllTransaction($request = null)
    {
        return $this->paymentTransactionRepository->getAll($request);
    }

    public function transactionListWithDatatable($request = null)
    {
        if (!$request->ajax()) {
            return redirect()->route('admin.dashboard');
        }

        $transactionData = $this->getAllTransaction($request);
        $countTotalTransactionReports = $this->countTotalTransactionReports();

        return Datatables::of($transactionData)
            ->addColumn('action', function ($transactionData) {
                return '<a href="' . route("admin.transaction-invoice-create", [$transactionData->id]) . '"
                            class="btn btn-outline-primary  	btn-sm 	m-btn m-btn--icon m-btn--pill"
                            style=" text-decoration: none; "
                                target="_blank" ><i class="la la-money"></i> Invoice</a>
                          ';
            })->addColumn('transaction_type', function ($transactionData) {
                if ($transactionData->type == PaymentTransaction::TRANSACTION_IN) {
                    return '<span style="color:#57b62f"><i class="la la-arrow-down"></i>In</span>';
                } else {
                    return '<span style="color:#ee854f"><i class="la la-arrow-up"></i>Out</span>';

                }
            })->addColumn('package_type', function ($transactionData) {
                if ($transactionData->package_type == PaymentTransaction::PACKAGE_TYPE_PAID) {
                    return '<span style="color:#5A82C2">Paid</span>';
                } else if ($transactionData->package_type == PaymentTransaction::PACKAGE_TYPE_FREE) {
                    return '<span style="color:#5A82C2">Free</span>';
                } else {
                    return '<span style="color:#5A82C2">Other</span>';
                }
            })
            ->addColumn('price', function ($transactionData) {
                return "$" . $transactionData->price;
            })
            ->addColumn('email', function ($transactionData) {
                return $transactionData->users->email;
            })
            ->rawColumns(['action', 'transaction_type', 'package_type', 'price', 'email'])
            ->setTotalRecords($countTotalTransactionReports)
            ->make(true);

    }


    public function todayTransaction($request)
    {
        // TODO: Implement todayTransaction() method.

        $request['created_at_from_search'] = date('Y-m-d');
        $request['created_at_to_search'] = date('Y-m-d');

        return $this->paymentTransactionRepository->getTransactionByDateRange($request);

    }

    public function monthlyTransaction($request)
    {
        $request['created_at_from_search'] = date('Y-m-d', strtotime('-1 months'));
        $request['created_at_to_search'] = date('Y-m-d');

        return $this->paymentTransactionRepository->getTransactionByDateRange($request);

    }

    public function yearlyTransaction($request)
    {
        // TODO: Implement yearlyTransaction() method.
        $request['created_at_from_search'] = date('Y-m-d', strtotime('-12 months'));
        $request['created_at_to_search'] = date('Y-m-d');

        return $this->paymentTransactionRepository->getTransactionByDateRange($request);
    }


    public function getTransactionById($id)
    {
        // TODO: Implement getTransactionById() method.
        return $this->paymentTransactionRepository->getTransactionById($id);
    }

    public function countTotalTransactionReports()
    {
        return $this->paymentTransactionRepository->countTotalTransactionReports();
    }

    public function paymentTransactionListByUser($request)
    {

        if (!$request->ajax()) {
            return redirect()->route('admin.category');
        }

        $transactionData = $this->paymentTransactionRepository->getAllByUserId($request , $request->user_id);

        $countTotalTransactionReports = $transactionData->count();

        return Datatables::of($transactionData)
            ->addColumn('action', function ($transactionData) {
                return '<a href="' . route("admin.transaction-invoice-create", [$transactionData->id]) . '"
                            class="btn btn-outline-primary  	btn-sm 	m-btn m-btn--icon m-btn--pill"
                            style=" text-decoration: none; "
                                target="_blank" ><i class="la la-money"></i> Invoice</a>
                          ';
            })->addColumn('my_transaction_type', function ($transactionData) {
                if ($transactionData->type == PaymentTransaction::TRANSACTION_IN) {
                    return '<span style="color:#57b62f"><i class="la la-arrow-down"></i>In</span>';
                } else {
                    return '<span style="color:#ee854f"><i class="la la-arrow-up"></i>Out</span>';

                }
            })->addColumn('package_type', function ($transactionData) {
                if ($transactionData->package_type == PaymentTransaction::PACKAGE_TYPE_PAID) {
                    return '<span style="color:#5A82C2">Paid</span>';
                } else if ($transactionData->package_type == PaymentTransaction::PACKAGE_TYPE_FREE) {
                    return '<span style="color:#5A82C2">Free</span>';
                } else {
                    return '<span style="color:#5A82C2">Other</span>';
                }
            })
            ->addColumn('price', function ($transactionData) {
                return "$" . $transactionData->price;
            })
            ->addColumn('email', function ($transactionData) {
                return $transactionData->users->email;
            })
            ->rawColumns(['action', 'my_transaction_type', 'package_type', 'price', 'email'])
            ->setTotalRecords($countTotalTransactionReports)
            ->make(true);

    }

    public function stripErrorList($request)
    {
        $data = $this->getAllStripError();
        $countTotalStripeErrors = $this->countTotalStripeErrors();

        return Datatables::of($data)
            ->addColumn('delete', function ($data) {
                return '';
            })
            ->rawColumns(['delete'])
            ->setTotalRecords($countTotalStripeErrors)
            ->make(true);
    }

    public function getAllStripError()
    {
        // TODO: Implement getAllStripError() method.
        return $this->stripeErrorRepository->getAllStripeError();
    }

    public function countTotalStripeErrors()
    {
        // TODO: Implement countTotalStripeErrors() method.
        return $this->stripeErrorRepository->countTotalStripeErrors();
    }



}
