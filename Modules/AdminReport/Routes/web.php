<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('admin/report')->group(function() {

});

Route::prefix('admin')->name('admin.')->middleware(['admin', 'web'])->group(function(){

    Route::prefix('report')->group(function() {

        Route::get('/transaction', 'PaymentReport\PaymentReportController@index')->name('transaction-report');
        Route::get('/transaction/list', 'PaymentReport\PaymentReportController@paymentTransactionList')->name('transaction-list');
        Route::get('/transaction/invoice/{id}', 'PaymentReport\PaymentReportController@transactionInvoiceCreate')->name('transaction-invoice-create');



        Route::get('/user/transaction/{id}', 'PaymentReport\PaymentReportController@paymentTransactionListUserWisePanel')->name('transaction-report-user-wise');
        Route::get('/user/transaction/list/data', 'PaymentReport\PaymentReportController@listUserTransactionData')->name('transaction-report-user-wise-list-data');

        Route::get('/stripError', 'PaymentReport\StripeErrorReportController@stripErrorView')->name('strip-error-view');
        Route::get('/lists/stripError/', 'PaymentReport\StripeErrorReportController@listSripeErrorDataList')->name('get-all-strip-errors');

    });

});
