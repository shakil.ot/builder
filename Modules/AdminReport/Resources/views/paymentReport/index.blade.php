@extends('admin.layouts.app')


@section('container')

    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">

        @include("adminreport::paymentReport.filter")
        <!--begin::Entry-->
        <div class="card card-custom">
            <div class="card-header">
                <div class="card-title">
                    <span class="card-icon">
                        <i class="flaticon2-delivery-package text-primary"></i>
                    </span>
                    <h3 class="card-label">Transaction Manage</h3>
                </div>
            </div>
            <div class="card-body">
                <!--begin: Datatable-->
                <!--begin: Datatable-->
                <table class="table table-bordered table-hover table-checkable" id="user-transaction-table">
                    <thead>
                    <tr>
                        <th>Email</th>
                        <th>Type</th>
                        <th>Package Type</th>
                        <th>Transaction Id</th>
                        <th>Price</th>
                        <th>Action</th>
                    </tr>
                    </thead>

                </table>
                <!--end: Datatable-->
            </div>
        </div>
        <!--end::Entry-->
    </div>


@endsection

@push('scripts')



    <script src="{{ asset('js/payment-report.js') }}"></script>

@endpush
