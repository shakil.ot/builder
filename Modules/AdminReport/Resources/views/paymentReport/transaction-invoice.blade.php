@extends('admin.layouts.app')


@section('container')


        <!-- begin: Invoice-->
        <!-- begin: Invoice header-->
        <div class="row justify-content-center py-8 px-8 py-md-27 px-md-0">
            <div class="col-md-9">
                <div class="d-flex justify-content-between pb-10 pb-md-20 flex-column flex-md-row">
                    <h1 class="display-4 font-weight-boldest mb-10">INVOICE</h1>
                    <div class="d-flex flex-column align-items-md-end px-0">
                        <!--begin::Logo-->
                        <a href="#" class="mb-5">
                            <img src="{{ asset(env('MAIN_LOGO')) }} " style="width: 160px" alt="">
                        </a>

                    </div>
                </div>
                <div class="border-bottom w-100"></div>
                <div class="d-flex justify-content-between pt-6">
                    <div class="d-flex flex-column flex-root">
                        <span class="font-weight-bolder mb-2">DATA</span>
                        <span class="opacity-70"> {{ $transaction->created_at    }}</span>
                    </div>
                    <div class="d-flex flex-column flex-root">
                        <span class="font-weight-bolder mb-2">INVOICE NO.</span>
                        <span class="opacity-70">{{ "#InvoiceOLKFN".$transaction->id }}  </span>
                    </div>
                    <div class="d-flex flex-column flex-root">
                        <span class="font-weight-bolder mb-2">INVOICE TO.</span>
                        <span class="opacity-70">{{ $user->first_name }} {{ $user->last_name }}
														<br>{{ $user->email }}</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- end: Invoice header-->
        <!-- begin: Invoice body-->
        <div class="row justify-coinvoicentent-center py-8 px-8 py-md-10 px-md-0">
            <div class="col-md-9">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th class="pl-0 font-weight-bold text-muted text-uppercase">Created</th>
                            <th class="text-right font-weight-bold text-muted text-uppercase">Transaction Id</th>
                            <th class="text-right font-weight-bold text-muted text-uppercase">Type</th>
                            <th class="text-right pr-0 font-weight-bold text-muted text-uppercase">Amount</th>
                        </tr>
                        </thead>
                        <tbody>

                        <tr class="font-weight-boldest border-bottom-0">
                            <td class="border-top-0 pl-0 py-4">{{ $transaction->created_at }}</td>
                            <td class="border-top-0 text-right py-4">{{ trim($transaction->transaction_id) }}  </td>
                            <td class="border-top-0 text-right py-4">@if($transaction->type_for == \Modules\Package\Entities\PaymentTransaction::PAYMENT_TRANSACTION_TYPE_FOR_PACKAGE_SUBSCRIPTION)
                                    Subscription
                                @elseif($transaction->type_for == \Modules\Package\Entities\PaymentTransaction::PAYMENT_TRANSACTION_TYPE_FOR_PACKAGE_RENEW)
                                    Recurring
                                @endif</td>
                            <td class="text-danger border-top-0 pr-0 py-4 text-right">$  {{ $transaction->credit_amount != 0 ? $transaction->credit_amount: $transaction->price}} </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- end: Invoice body-->
        <!-- begin: Invoice footer-->
        <!-- end: Invoice footer-->
        <!-- begin: Invoice action-->
        <div class="row justify-content-center py-8 px-8 py-md-10 px-md-0">
            <div class="col-md-9">
                <div class="d-flex justify-content-between">
                    <button type="button" class="btn btn-light-primary font-weight-bold" onclick="window.print();">Download Invoice</button>
                    <button type="button" class="btn btn-primary font-weight-bold" onclick="window.print();">Print Invoice</button>
                </div>
            </div>
        </div>
        <!-- end: Invoice action-->
        <!-- end: Invoice-->

@endsection

@push('scripts')


    <script src="{{ URL::asset('/assets/global/html2pdf-master/dist/html2pdf.bundle.min.js') }}"></script>

    <script>
        let invoicePdfObj = {
                pdfName : 'invoice_{{$transaction->id .'_'. date('F') .'_'. date('d') .'_'. date('Y') .'_'. date('h: i:s')}}_sk.pdf',
        };
    </script>

    {{--            <script src="{{ Module::asset('report:js/transaction-invoice-report.js') }}"></script>--}}
    <script src="{{ asset('../js/transaction-invoice-report.js') }}"></script>






@endpush
