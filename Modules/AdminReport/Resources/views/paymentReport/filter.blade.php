<div class="col-md-12 filter-options" style="background-color: #f0f0f0; display:none;">
    <form class="m-form">
        <div class="row m--margin-top-10">
            <div class="col-md-6">
                <div class="row">
                    <div class="form-group col-md-10">
                        <label class="form-control-label m--font-bold">
                            Search Transaction ID
                        </label>
                        <input id="search-transaction-id" type="text" class="form-control m-input"
                               placeholder="&#xf2bd; Search Transaction ID"
                               style="font-family: Poppins, FontAwesome;" data-column="4">

                    </div>
                    <div class="form-group col-md-2  m--margin-top-35">
                        <a href="javascript:void(0);"
                           class="btn btn-outline-metal m-btn m-btn--icon btn-xs m-btn--icon-only m-btn--pill reset-search-transaction-id"
                           data-column="4">
                            <i class="fa fa-remove"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="form-group col-md-10">
                        <label class="form-control-label m--font-bold">
                            Select Transaction Type
                        </label>

                        <select id="search-transaction-type"
                                class="form-control m-bootstrap-select m_selectpicker" data-column="1">
                            <option value="" data-icon="la la-arrow-circle-o-down">
                                Select type
                            </option>
                            <option data-icon="la la-check-circle" value="{!! Modules\Package\Entities\PaymentTransaction::TRANSACTION_IN !!}">
                                In
                            </option>
                            <option data-icon="la la-ban"
                                    value="{!! Modules\Package\Entities\PaymentTransaction::TRANSACTION_OUT !!}">
                                Out
                            </option>
                        </select>

                    </div>
                    <div class="form-group col-md-2  m--margin-top-35">
                        <a href="javascript:void(0);"
                           class="btn btn-outline-metal m-btn m-btn--icon btn-xs m-btn--icon-only m-btn--pill reset-search-transaction-type reset-search-type"
                           data-column="1">
                            <i class="fa fa-remove"></i>
                        </a>
                    </div>
                </div>
            </div>


            <div class="col-md-6">
                <div class="row">
                    <div class="form-group col-md-10">
                        <label class="form-control-label m--font-bold">
                            Select Created Date
                        </label>

                        <div id="created_at_range" style="cursor: pointer; color: #9699a2;"
                             class="form-control">
                            <i class="fa fa-calendar"></i>&nbsp;
                            <span></span> <i class="la la-angle-down pull-right"
                                             style="font-size: inherit;"></i>
                        </div>

                    </div>
                    <div class="form-group col-md-2  m--margin-top-35">
                        <a href="javascript:void(0);"
                           class="btn btn-outline-metal m-btn m-btn--icon btn-xs m-btn--icon-only m-btn--pill reset-created_at_range">
                            <i class="fa fa-remove"></i>
                        </a>
                    </div>
                </div>
            </div>


        </div>

    </form>
</div>
