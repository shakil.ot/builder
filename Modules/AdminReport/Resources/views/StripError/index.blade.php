@extends('admin.layouts.app')

@section('container')

    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">

        <!--begin::Entry-->
        <div class="card card-custom">
            <div class="card-header">
                <div class="card-title">
                    <span class="card-icon">
                        <i class="flaticon2-delivery-package text-primary"></i>
                    </span>
                    <h3 class="card-label">Stripe Error List</h3>
                </div>
                <div class="card-toolbar">
                    <!--begin::Dropdown-->

                    <!--end::Dropdown-->
                    <!--begin::Button-->


                    <!--end::Button-->
                </div>
            </div>
            <div class="card-body">
            @include('admin.user.filter')
            <!--begin: Datatable-->
                <!--begin: Datatable-->


                <input type="hidden" name="_token" value="{{csrf_token()}}">

                <table id="strip-errors"
                       class="table table-bordered table-hover table-checkable"
                        >
                    <thead>
                    <tr>
                        <th>Email</th>
                        <th>Error</th>
                        <th>Ip</th>
                    </tr>
                    </thead>
                </table>





                <!--end: Datatable-->
            </div>
        </div>
        <!--end::Entry-->
    </div>



    <div class="m-portlet__body extra p-0">
        <div class="section-toggle change-content-section fb-page-management-wrapper">

            <div class="row pane-section-form">
                <!-- Grid Item -->
                <div class="col-12">

                    <!-- Card -->
                    <div class="dt-card">


                        <!-- /card header -->


                    </div>
                    <!-- /card -->

                </div>

            </div>


            <div class="row pane-section-content open">
                <!-- Grid Item -->
                <div class="col-12">

                    <!-- Card -->
                    <div class="dt-card">

                        <!-- Card Header -->
                        <div class="dt-card__header mb-0 extra pt-0">

                            <!-- Card Heading -->
                            <div class="dt-card__heading">
                                <h3 class="dt-card__title">  </h3>
                            </div>
                            <!-- /card heading -->

                            <!-- Card Tools -->
                            <div class="dt-card__tools">
                                <!-- List -->
                                <ul class="dt-list dt-list-sm dt-list-cm-0">
                                    <!-- List Item -->


                                </ul>
                                <!-- /list -->

                            </div>
                            <!-- /card tools -->

                        </div>

                        <!-- /card header -->

                        <!-- Card Body -->
                        <div class="dt-card__body">



                        </div>
                        <!-- /card body -->

                    </div>
                    <!-- /card -->

                </div>

            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(function () {
            $.extend(true, $.fn.dataTable.defaults, {
                "language": {
                    "search": "",
                    // search: "_INPUT_",
                    "searchPlaceholder": "Search",
                    "sLengthMenu": "_MENU_ records",
                    "paginate": {
                        "first": "<<",
                        "previous": '<span class="fa fa-chevron-left"></span>',
                        "next": '<span class="fa fa-chevron-right"></span>',
                        "last": ">>"
                    },
                }
            });

            const _token = '{!!  csrf_token() !!}'  ;

            (function (global, $, _token) {

                var AjaxContent = new global.AjaxContent({
                    dom: $('.change-content'),
                    others: $(".change-content-section")
                });

                $('.open-change-content').on('click', function () {
                    AjaxContent.back();
                });

                var dataTable = $('#strip-errors').DataTable({
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    async:false,
                    deferRender:    true,
                    ajax: '{!! route('admin.get-all-strip-errors') !!}',
                    columns: [

                        {data: 'email', name: 'users.email', "orderable": false, "searchable": true, width: "15%"},
                        {
                            data: 'error',
                            name: 'error',
                            "orderable": false,
                            "searchable": true,
                            width: "15%"
                        },
                        {
                            data: 'ip',
                            name: 'ip',
                            "orderable": false,
                            "searchable": true,
                            width: "10%"
                        },

                    ],
                    "createdRow": function (row, data) {
                    }
                });
                //Columns visibility buttons' css
                $("#custom-colvis-button").click(function () {
                    $(".buttons-columnVisibility").removeClass("dt-button").addClass("btn btn-outline-primary btn-sm m-btn btn-block");
                    $(".buttons-colvisRestore").removeClass("dt-button").addClass("btn btn-outline-brand btn-sm m-btn btn-block");
                });
                $("#refresh-button").click(function () {
                    refreshTable();
                });

                function refreshTable(params) {
                    dataTable.columns().search('');
                    dataTable.search('');
                    dataTable.ajax.reload();
                }
            })(window, jQuery, _token);
        });


    </script>


@endpush
