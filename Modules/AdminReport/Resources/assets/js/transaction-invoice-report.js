var InvoicePdf = (function (invoicePdfObj) {

    $('#pdf_creator').click(function () {
        var element = document.getElementById('main_div');
        $('#preloader').show();
        html2pdf(element, {
            margin: 0,
            filename: invoicePdfObj.pdfName ,
            image : {
                type: 'jpeg', quality : 1
        },
        html2canvas: {dpi: 200, letterRendering:true},
        jsPDF: {
                unit: 'in',
                format:'letter',
                orientation:'portrait'}});
        setTimeout(function () {
            $('#preloader').hide();
        }, 5000);
    });

})(invoicePdfObj);
