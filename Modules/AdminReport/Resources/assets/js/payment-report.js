var PaymentReport = (function () {

    $(function () {


    const _token = document.head.querySelector("[property=csrf-token]").content;


    (function (global, $, _token) {


        // #### DATATABLE FOR USER-LIST. ALL USER LIST FITCH FROM DATABASED
        var dataTable = $('#user-transaction-table').DataTable({
            responsive: true,
            // Pagination settings
            // read more: https://datatables.net/examples/basic_init/dom.html
            lengthMenu: [5, 10, 25, 50, 100, 500],
            pageLength: 10,
            language: {
                'lengthMenu': 'Display _MENU_',
            },
            searchDelay: 500,
            processing: true,
            serverSide: true,
            ajax: {
                url: route('admin.transaction-list'),
                data: function (data) {
                    // data.created_at_from_search = created_at_from;
                    // data.created_at_to_search = created_at_to;
                }
            },
            columns: [
                {data: 'email', name: 'email', "orderable": true, "searchable": true, width: "10%"},
                {data: 'transaction_type', name: 'type', "orderable": true, "searchable": true, width: "15%"},
                {data: 'package_type', name: 'package_type', "orderable": true, "searchable": true, width: "15%"},
                {data: 'transaction_id', name: 'transaction_id', "orderable": true, "searchable": true, width: "30%"},
                {data: 'price', name: 'price', "orderable": true, "searchable": true, width: "15%"},
                {data: 'action', name: 'action', "orderable": false, searchable: false, width: "15%"}

            ],
            columnDefs: [
                {responsivePriority: 1, targets: 4},
                {responsivePriority: 2, targets: 3}
            ],
            dom: '<"top-toolbar row"<"top-left-toolbar col-md-6"lB><"top-right-toolbar col-md-6"f>>rt<"bottom-toolbar"<"bottom-left-toolbar"i><"bottom-right-toolbar"p>>',
            buttons: [
                {
                    extend: 'colvis',
                    text: '<i class="fa \tfa-list-ul pr-1"></i>Columns',
                    attr: {
                        id: "custom-colvis-button",
                        class: 'btn btn-outline-primary btn-sm m-btn',
                        style: '',
                        title: 'Columns Visibility'
                    },
                    columns: ':not(.noVis)',
                    columnText: function (dt, idx, title) {
                        // return (idx+1)+': '+title;
                        return title;
                    },
                    postfixButtons: ['colvisRestore']
                },
                {
                    extend: 'colvisGroup',
                    text: '<i class="fa fa-refresh pr-1"></i>Refresh',
                    attr: {
                        class: 'btn btn-outline-primary btn-sm m-btn  m-btn--icon',
                        id: 'refresh'
                    },
                    show: ':hidden'
                },
                {
                    text: '<i class="fa fa-filter pr-1"></i>Filter',
                    attr: {
                        class: 'btn btn-outline-primary btn-sm m-btn  m-btn--icon',
                        id: 'show-all-button'
                    },
                    action: function (e, dt, node, config) {
                        $('.filter-options').slideToggle();
                    }
                },
                {
                    extend: 'collection',
                    text: 'Export To',
                    autoClose: true,
                    attr: {
                        id: "export_button",
                        class: 'btn btn-outline-primary btn-sm m-btn dropdown-toggle',
                        style: '',
                        title: 'Export'
                    },
                    buttons: [
                        {
                            extend: 'csv',
                            footer: true,
                            exportOptions: {
                                columns: [0,1,2,3,4]
                            }
                        },
                        {
                            extend: 'excel',
                            footer: true,
                            exportOptions: {
                                columns: [0,1,2,3,4]
                            }
                        },
                        {
                            extend: 'pdf',
                            title: "Transaction report",
                            footer: true,
                            exportOptions: {
                                columns: [0,1,2,3,]
                            }
                        },
                        {
                            extend: 'print',
                            footer: true,
                            exportOptions: {
                                columns: [0,1,2,3,4]
                            }
                        },
                        {
                            extend: 'copy',
                            footer: true,
                            exportOptions: {
                                columns: [0,1,2,3,4]
                            }
                        }
                    ],
                    fade: true
                }
            ]
        });

        function initializeCreatedAtDateRangePicker() {
            let start = moment().subtract(1, 'months');
            let end = moment();

            function cb(start, end) {
                $('#created_at_range span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }

            $('#created_at_range').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, function (starting, ending) {
                let startDate = moment(starting);
                let endDate = moment(ending);
                start = startDate;
                end = endDate;
                cb(start, end);
                created_at_from = moment(start, 'MMMM D, YYYY').format("YYYY-MM-DD");
                created_at_to = moment(end, 'MMMM D, YYYY').format("YYYY-MM-DD");
                dataTable.ajax.reload();
            });
            $('#created_at_range span').html('Select Created Date');
        }

        //Columns visibility buttons' css
        $("#custom-colvis-button").click(function(){
            $( ".buttons-columnVisibility" ).removeClass("dt-button").addClass("btn btn-outline-primary btn-sm m-btn btn-block");
            $( ".buttons-colvisRestore" ).removeClass("dt-button").addClass("btn btn-outline-brand btn-sm m-btn btn-block");
        });

        $(document).on('click','#refresh',function () {
            refreshTable();
        });
        /* search starts */
        $("#search-transaction-id").keyup(function () {
            dataTable.column($(this).attr('data-column'))
                .search($(this).val())
                .draw();
        });
        $("#search-transaction-type").change(function () {
            dataTable.column($(this).attr('data-column'))
                .search($(this).val())
                .draw();
        });
        $("#search-type-for").change(function () {
            dataTable.column($(this).attr('data-column'))
                .search($(this).val())
                .draw();
        });
        /* search ends */

        /* reset search starts*/
        $('.reset-search-transaction-id').on('click', function () {
            $("#search-transaction-id").val('');
            dataTable.column($(this).attr('data-column'))
                .search('')
                .draw();
        });
        $('.reset-search-transaction-type').on('click', function () {
            $("#search-transaction-type").val('').selectpicker('refresh');
            dataTable.column($(this).attr('data-column'))
                .search('')
                .draw();
        });
        $('.reset-search-type-for').on('click', function () {
            $("#search-type-for").val('').selectpicker('refresh');
            dataTable.column($(this).attr('data-column'))
                .search('')
                .draw();
        });
        $('.reset-created_at_range').on('click', function () {
            $('#created_at_range span').html('Select Created Date');
            created_at_from = null;
            created_at_to = null;
            dataTable.ajax.reload();
        });

        /* reset search ends */
        function refreshTable(params) {
            $("#search-transaction-type").val('').selectpicker('refresh');
            $("#search-type-for").val('').selectpicker('refresh');
            $("#search-transaction-id").val('');
            $('#created_at_range span').html('Select Created Date');
            created_at_from = null;
            created_at_to = null;
            dataTable.columns().search('');
            dataTable.search('');
            dataTable.ajax.reload();
        }



    })(window, jQuery, _token);
});

})();

