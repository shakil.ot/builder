<?php

namespace Modules\AdminWebBuilder\Contracts\Service;

interface AdminWebTemplateContact
{
    public function getAllEmailTemplate();

    public function getAllAdminWebTemplateList($request);

    public function createAdminTemplate($request);

    public function getAdminTemplateUsingTemplateId($id);

    public function updateAdminTemplate($request);

    public function deleteAdminTemplate($id);

    public function getDataByType($type);

    public function createTemplateUsingType($request);

    public function getAllEBookTemplate();


}
