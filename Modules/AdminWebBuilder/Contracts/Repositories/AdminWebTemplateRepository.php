<?php

namespace Modules\AdminWebBuilder\Contracts\Repositories;

interface AdminWebTemplateRepository
{
    public function getAllAdminWebTemplateList($request , $select = ['*']);

    public function addAdminTemplate($request);

    public function getEmailTemplateById($templateId);

    public function updateTemplateById($id, $data);

    public function deleteAdminTemplate($id);

    public function getTemplateByConditionFirst($where , $select = ['*']);

    public function updateAdminTemplate($where , $update);

    public function getTemplateByCondition($where , $select = ['*']);

}
