<?php


namespace Modules\AdminWebBuilder\Entities;

use Illuminate\Database\Eloquent\Model;

class AdminWebTemplate extends Model
{
    protected $table = 'admin_web_templates';

    protected $fillable = [
        'id',
        'title',
        'content',
        'css',
        'type',
        'image',
        'status'
    ];

    const ADMIN_WEB_TYPE_SUBSCRIPTION = 1;
    const ADMIN_WEB_TYPE_DOWNLOAD_INSTRUCTION = 2;
    const ADMIN_WEB_TYPE_OFFER = 3;
    const ADMIN_WEB_TYPE_THANK_YOU = 4;
    const ADMIN_WEB_TYPE_E_BOOK = 5;


    const ADMIN_WEB_TYPE_ARRAY = [
        self::ADMIN_WEB_TYPE_SUBSCRIPTION => 'SUBSCRIPTION',
        self::ADMIN_WEB_TYPE_DOWNLOAD_INSTRUCTION => 'DOWNLOAD INSTRUCTION',
        self::ADMIN_WEB_TYPE_OFFER => 'OFFER',
        self::ADMIN_WEB_TYPE_THANK_YOU => 'THANK YOU',
        self::ADMIN_WEB_TYPE_E_BOOK => 'E-BOOK',
    ];

    public static function getAdminWebTypeName($type)
    {
        return self::ADMIN_WEB_TYPE_ARRAY[$type];
    }


}