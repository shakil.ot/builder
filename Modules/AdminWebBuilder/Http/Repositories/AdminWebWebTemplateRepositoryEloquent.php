<?php

namespace Modules\AdminWebBuilder\Http\Repositories;

use App\Repositories\BaseRepository\BaseRepository;
use Modules\AdminWebBuilder\Contracts\Repositories\AdminWebTemplateRepository;
use Modules\AdminWebBuilder\Entities\AdminWebTemplate;


class AdminWebWebTemplateRepositoryEloquent extends BaseRepository implements AdminWebTemplateRepository
{
    protected function model()
    {
        return new AdminWebTemplate();
    }

    public function getAllAdminWebTemplateList($request , $select = ["*"])
    {
        return $this->model::select($select)->orderBy('id' , 'DESC');
    }

    public function addAdminTemplate($data)
    {
        return $this->model->create($data);
    }

    public function getEmailTemplateById($templateId)
    {
        return $this->model()
                ->where('id', $templateId)
                ->first();
    }

    public function updateTemplateById($id, $data)
    {
        return $this->model
                ->find($id)
                ->update([
                    'title' => $data['request']['title'],
                    'subject' => $data['request']['subject'],
                    'body' => $data['request']['body'],
                ]);
    }

    public function deleteAdminTemplate($id)
    {
        return $this->model()->where(['id' => $id])->delete();
    }

    public function getTemplateByConditionFirst($where, $select = ['*'])
    {
        return $this->model::select($select)->where($where)->first();
    }

    public function updateAdminTemplate($where , $update)
    {
        return $this->model::where($where)->update($update);
    }

    public function getTemplateByCondition($where, $select = ['*'])
    {
        return $this->model::select($select)->where($where)->get();
    }
}
