<?php

namespace Modules\AdminWebBuilder\Http\Services;

use Modules\AdminWebBuilder\Contracts\Repositories\AdminWebTemplateRepository;
use Modules\AdminWebBuilder\Contracts\Service\AdminWebTemplateContact;
use Modules\AdminWebBuilder\Entities\AdminWebTemplate;
use Modules\WebBuilder\Entities\WebBuilder;
use phpDocumentor\Reflection\Types\This;
use Yajra\DataTables\DataTables;

class AdminWebWebTemplateService implements AdminWebTemplateContact
{

    /**
     * @var AdminWebTemplateRepository
     */
    private $adminWebTemplateRepository;

    public function __construct(AdminWebTemplateRepository $adminWebTemplateRepository)
    {
        $this->adminWebTemplateRepository = $adminWebTemplateRepository;
    }

    public function getAllEmailTemplate()
    {

    }

    public function getAllAdminWebTemplateList($request)
    {
        $templates = $this->adminWebTemplateRepository->getAllAdminWebTemplateList($request, [ 'id' , 'type', 'title', 'created_at' ]);
        return Datatables::of($templates)
            ->addColumn('title', function ($templates) {
                if ($templates->title == null) {
                    return '<span class="label label-lg label-light-danger  label-inline">N/A</span>';
                } else {
                    return '' . $templates->title . '';
                }
            })
            ->addColumn('type', function ($templates) {
                if ($templates->type == WebBuilder::WEB_BUILDER_TYPE_E_BOOK){
                    return '<span class="label label-lg label-light-danger label-inline">' . AdminWebTemplate::getAdminWebTypeName($templates->type) . '</span>';
                }
                return '<span class="label label-lg label-light-success label-inline">' . AdminWebTemplate::getAdminWebTypeName($templates->type) . '</span>';


            })
            ->addColumn('created_at', function ($templates) {

                return '<span class="label label-lg label-light-info label-inline">' . $templates->created_at->format('d-m-yy') . '</span>';
            })
            ->addColumn('action', function ($templates) {
                $btn = '<div><a href="' . route('admin-template-view', ['id' => $templates->id]) . '" 
                        class="viewTemplate" id="templateId"><i class="fas fa-eye"></i> View </span></a>'
                    .

                    '   <a href="' . route('admin-template-edit', ['id' => $templates->id]) . '" class="editTemplate" id="templateId"
                    data-id="' . $templates->id . ' ">
                    <i class="la la-pencil"></i> Edit </span>
                    </a>'
                    .
                    '<a href="javascript:void(0)" class="deleteTemplate action"
                    data-value="delete-admin-template" data-id="' . $templates->id . '"><i class="la la-trash-o"></i> Delete
                    </a>
                </div>';
                return $btn;
            })
            ->rawColumns(['title', 'type', 'created_at', 'action'])
            ->make(true);
    }

    public function createAdminTemplate($request)
    {
        if (is_null($request['html_data'])) {
            return [
                'html' => 'Please Build first web page',
                'status' => false,
            ];
        }

        $response = $this->adminWebTemplateRepository->addAdminTemplate([
            "content" => $request['html_data'],
            "css" => $request['css_data'],
            "title" => $request['template_title'],
            "type" => $request['template_type'],
        ]);

        if ($response) {
            return [
                'html' => 'Template Added Successfully',
                'status' => true,
            ];
        }

        return [
            'html' => 'Something is wrong !!',
            'status' => false,
        ];

    }

    public function getAdminTemplateUsingTemplateId($id)
    {
        return $this->adminWebTemplateRepository->getTemplateByConditionFirst([
            'id' => $id
        ]);
    }

    public function updateAdminTemplate($request)
    {

        if (is_null($request['html_data'])) {
            return [
                'html' => 'Please Build first web page',
                'status' => false,
            ];
        }

        $response = $this->adminWebTemplateRepository->updateAdminTemplate([
            'id' => $request['adminTemplateId'],
        ], [
            "content" => $request['html_data'],
            "css" => $request['css_data'],
            "title" => $request['template_title'],
            "type" => $request['template_type'],
            "image" => $request['image'],
        ]);

        if ($response) {
            return [
                'html' => 'Template update Successfully',
                'status' => true,
            ];
        }

        return [
            'html' => 'Something is wrong !!',
            'status' => false,
        ];


    }

    public function deleteAdminTemplate($id)
    {
        $response = $this->adminWebTemplateRepository->deleteAdminTemplate($id);

        if ($response) {
            return [
                'html' => 'Template delete Successfully',
                'status' => 'success',
            ];
        }

        return [
            'html' => 'Something is wrong !!',
            'status' => 'error',
        ];
    }

    public function getDataByType($type)
    {
        return $this->adminWebTemplateRepository->getTemplateByCondition([
            'type' => $type
        ], ['id', 'title' , 'image']);
    }


    public function createTemplateUsingType($request)
    {
        $response = $this->adminWebTemplateRepository->addAdminTemplate([
            "content" => '',
            "css" => '',
            "title" => $request['template_title'],
            "type" => $request['template_category'],
        ]);

        if ($response) {
            return [
                'html' => 'Template Added Successfully',
                'status' => true,
                'url' => route('admin-template-edit', ['id' => $response->id ]),
            ];
        }

        return [
            'html' => 'Something is wrong !!',
            'status' => false,
        ];
    }

    public function getAllEBookTemplate()
    {
        return $this->adminWebTemplateRepository->getTemplateByCondition([
            'type' => AdminWebTemplate::ADMIN_WEB_TYPE_E_BOOK,
        ], ['id' , 'image']);
    }
}
