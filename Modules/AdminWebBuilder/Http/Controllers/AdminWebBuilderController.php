<?php

namespace Modules\AdminWebBuilder\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\AdminWebBuilder\Contracts\Service\AdminWebTemplateContact;
use Modules\AdminWebBuilder\Entities\AdminWebTemplate;

class AdminWebBuilderController extends Controller
{
    /**
     * @var AdminWebTemplateContact
     */
    private $adminWebTemplateService;

    public function __construct(AdminWebTemplateContact $adminWebTemplateService)
    {
        $this->adminWebTemplateService = $adminWebTemplateService;
    }

    public function index()
    {
        return view('adminwebbuilder::builder.index');
    }

    public function getAllAdminWebTemplateList(Request $request)
    {

        return $this->adminWebTemplateService->getAllAdminWebTemplateList($request);
    }

    public function adminWebBuilderAddPage()
    {
        return view('adminwebbuilder::builder.add-web-builder');

    }

    public function adminWebBuilderAdd(Request $request)
    {
        return $this->adminWebTemplateService->createAdminTemplate($request);
    }

    public function adminTemplateView($id)
    {
        $result = $this->adminWebTemplateService->getAdminTemplateUsingTemplateId($id);

        return view('adminwebbuilder::builder.admin-web-builder-view')->with([
            'adminTemplateHtml' => $result->content,
            'adminTemplateCss' => $result->css
        ]);

    }

    public function adminTemplateEdit($id)
    {
        $result = $this->adminWebTemplateService->getAdminTemplateUsingTemplateId($id);

        if ($result->type == AdminWebTemplate::ADMIN_WEB_TYPE_E_BOOK) {

            return view('adminwebbuilder::builder.admin-web-builder-edit-for-e-book')->with([
                'adminTemplateHtml' => $result->content,
                'adminTemplateId' => $result->id,
                'templateTitle' => $result->title,
                'templateType' => $result->type,
            ]);

        }


        return view('adminwebbuilder::builder.admin-web-builder-edit')->with([
            'adminTemplateHtml' => $result->content,
            'adminTemplateCss' => $result->css,
            'adminTemplateId' => $result->id,
            'templateTitle' => $result->title,
            'templateType' => $result->type,
        ]);

    }

    public function adminWebBuilderUpdate(Request $request)
    {
        return $this->adminWebTemplateService->updateAdminTemplate($request);
    }

    public function adminTemplateDelete(Request $request)
    {
        return $this->adminWebTemplateService->deleteAdminTemplate($request['id']);
    }

    public function createTemplateUsingType(Request $request)
    {
        return $this->adminWebTemplateService->createTemplateUsingType($request);
    }

}
