<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('admin/web/builder')->middleware(['admin', 'web'])->group(function () {
    Route::get('/', 'AdminWebBuilderController@index')->name('admin-template-list-show');
    Route::get('/admin-template-view/{id}', 'AdminWebBuilderController@adminTemplateView')->name('admin-template-view');
    Route::get('/admin-template-edit/{id}', 'AdminWebBuilderController@adminTemplateEdit')->name('admin-template-edit');
    Route::get('/get-all-admin-template-list', 'AdminWebBuilderController@getAllAdminWebTemplateList')->name('get-all-admin-template-list');
    Route::get('/add', 'AdminWebBuilderController@adminWebBuilderAddPage')->name('admin-web-builder-add-page');
    Route::post('/admin-web-builder-add', 'AdminWebBuilderController@adminWebBuilderAdd')->name('admin-web-builder-add');
    Route::post('/admin-web-builder-update', 'AdminWebBuilderController@adminWebBuilderUpdate')->name('admin-web-builder-update');
    Route::post('/delete-admin-template', 'AdminWebBuilderController@adminTemplateDelete')->name('delete-admin-template');
    Route::post('/create-template-using-type', 'AdminWebBuilderController@createTemplateUsingType')->name('create-template-using-type');
});
