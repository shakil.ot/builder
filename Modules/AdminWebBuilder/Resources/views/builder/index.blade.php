@extends('admin.layouts.app')

@push('styles')
    <style>
        div.dataTables_wrapper div.dataTables_filter input {
            margin-left: 0.5em;
            display: inline-block;
        }
        .top-left-toolbar{
            margin-right: -78px;
        }
    </style>
@endpush

@section('container')

    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">

        <!--begin::Entry-->
        <div class="card card-custom">
            <div class="card-header">
                <div class="card-title">
                    <span class="card-icon">
                        <i class="flaticon-users-1 text-primary"></i>
                    </span>
                    <h3 class="card-label">Admin Web Template</h3>
                </div>
                <div class="card-toolbar">
                    <!--begin::Dropdown-->
                    <!--end::Dropdown-->
                    <!--begin::Button-->
                    <button class="btn font-weight-bolder"
                            style="background-color: #023c64; color: #ffffff"
                            id="add_admin_template">+ Add
                        Template</button>

                    <!--end::Button-->
                </div>
            </div>
            <div class="card-body">
            @include('adminwebbuilder::builder.filter')
            <!--begin: Datatable-->
                <!--begin: Datatable-->
                <table class="table table-bordered table-hover table-checkable" id="admin-template-table">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Template Type</th>
                        <th>Created At</th>
                        <th>Action</th>
                    </tr>
                    </thead>

                </table>
                <!--end: Datatable-->
            </div>
        </div>
        <!--end::Entry-->
    </div>

    <!-- Modal -->
    <div id="add_admin_template_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Select Template Category</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group m-form__group row">
                        <div class="col-xl-12">
                            <input type="text" class="form-control" id="template_title" placeholder="Enter your template title" required>
                        </div>
                    </div>

                    <div class="form-group m-form__group row">
                        <div class="col-xl-12">
                            <select name="" id="template_category" class="form-control" required="">
                                @foreach(\Modules\WebBuilder\Entities\WebBuilder::WEB_BUILDER_TYPE_ARRAY as $key => $value)
                                    <option value="{{ $key }}">{{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="m-form__actions  text-right">
                        <button type="button" class="btn" style="background-color: #023c64; color: #ffffff" id="create_template_using_type">
                            <i class="flaticon-interface-5" style="color: #ffff"></i> Add Template
                        </button>
                    </div>

                </div>
            </div>

        </div>
    </div>


@endsection

@push('scripts')

    <script src="{{ asset('js/admin-template.js') }}"></script>

@endpush
