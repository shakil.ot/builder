<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{ env('APP_NAME') }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content="Best Free Open Source Responsive Websites Builder" name="description">
{{--    <link rel="stylesheet" href="https://grapesjs.com/stylesheets/toastr.min.css">--}}
    <link rel="stylesheet" href="https://grapesjs.com/stylesheets/grapes.min.css?v0.16.34">
    <link rel="stylesheet" href="https://grapesjs.com/stylesheets/grapesjs-preset-webpage.min.css">
    <link rel="stylesheet" href="https://grapesjs.com/stylesheets/tooltip.css">
    <link rel="stylesheet" href="https://grapesjs.com/stylesheets/grapesjs-plugin-filestack.css">
    <link rel="stylesheet" href="https://grapesjs.com/stylesheets/demos.css?v3">
    <link href="https://unpkg.com/grapick/dist/grapick.min.css" rel="stylesheet">

    <!-- <script src="//static.filestackapi.com/v3/filestack.js"></script> -->
    <!-- <script src="js/aviary.js"></script> old //feather.aviary.com/imaging/v3/editor.js -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- <script src="js/toastr.min.js"></script> -->

    <script src="https://grapesjs.com/js/grapes.min.js?v0.16.34"></script>
    <script src="https://grapesjs.com/js/grapesjs-preset-webpage.min.js?v0.1.11"></script>
    <script src="https://grapesjs.com/js/grapesjs-lory-slider.min.js?0.1.5"></script>
    <script src="https://grapesjs.com/js/grapesjs-tabs.min.js?0.1.1"></script>
    <script src="https://grapesjs.com/js/grapesjs-custom-code.min.js?0.1.2"></script>
    <script src="https://grapesjs.com/js/grapesjs-touch.min.js?0.1.1"></script>
{{--    <script src="https://grapesjs.com/js/grapesjs-parser-postcss.min.js?0.1.1"></script>--}}
    <script src="https://grapesjs.com/js/grapesjs-tooltip.min.js?0.1.1"></script>
    <script src="https://grapesjs.com/js/grapesjs-tui-image-editor.min.js?0.1.2"></script>
    <script src="https://grapesjs.com/js/grapesjs-typed.min.js?1.0.5"></script>
    <script src="https://grapesjs.com/js/grapesjs-style-bg.min.js?1.0.1"></script>


    {{--my code--}}
    <script src="{{ asset('assets/global/plugins/toastr/toastr.min.js') }}"></script>
    <link href="{{ asset('assets/global/plugins/toastr/toastr.min.css') }}" rel="stylesheet">

    <script src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>




</head>
<body>


<div id="gjs" style="height:0px; overflow:hidden">{!! $adminTemplateHtml !!}</div>
<div id="gjs-new-div" style="padding-left: 20px; padding-right: 20px;"></div>



<script>

    var form_action_value = '', form_action_name = '';
    if ("{{ $templateType }}" == '{{ \Modules\WebBuilder\Entities\WebBuilder::WEB_BUILDER_TYPE_SUBSCRIPTION }}') {
        form_action_value = '{{ route('action-url-for-subscription') }}';
        form_action_name = 'Subscription';
    } else if ("{{ $templateType }}" == '{{ \Modules\WebBuilder\Entities\WebBuilder::WEB_BUILDER_TYPE_DOWNLOAD_INSTRUCTION }}') {
        form_action_value = '{{ route('action-url-for-download-instruction') }}';
        form_action_name = 'Download Instruction';
    } else if ("{{ $templateType }}" == '{{ \Modules\WebBuilder\Entities\WebBuilder::WEB_BUILDER_TYPE_OFFER }}') {
        form_action_value = '{{ route('action-url-for-offer') }}';
        form_action_name = 'Offer';
    } else if ("{{ $templateType }}" == '{{ \Modules\WebBuilder\Entities\WebBuilder::WEB_BUILDER_TYPE_THANK_YOU }}') {
        form_action_value = '{{ route('action-url-for-thank-you') }}';
        form_action_name = 'Thank you';
    }


    var adminWebBuilderEditObj = {
        form_action_url : form_action_value,
        form_action_name : form_action_name,
        adminTemplateId : '{{ $adminTemplateId }}',
        templateTitle : '{!! $templateTitle !!} ',
        grapesJsSelectAndSaveOption : '{!! ViewHelper::grapesJsSelectAndSaveOptionEdit($templateType) !!}',
        style:  '{{ \Modules\WebBuilder\Entities\WebBuilder::DEFAULT_STYLE }}' +  '{{ $adminTemplateCss }}',
        update_image_url : '{!! route('update-image') !!}',
        admin_web_builder_update_url : '{!! route('admin-web-builder-update') !!}',
        admin_web_builder_redirect_url : "{{ route('admin-template-list-show')}}",
        templateType: '{{ $templateType }}',
    };
</script>


<script src="{{ mix('js/adminWebBuilderEdit.js') }}"></script>

<script type="text/javascript">



    // (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    //     (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    //     m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    // })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    // ga('create', 'UA-74284223-1', 'auto');
    // ga('send', 'pageview');

    localStorage.clear();
</script>

</body>
</html>