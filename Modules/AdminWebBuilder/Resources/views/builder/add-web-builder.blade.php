<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Admin Web Builder</title>
    <link href="{{ asset('builder/grapes.min.css') }}" rel="stylesheet">
    <link href="{{ asset('builder/grapesjs-preset-webpage.min.css') }}" rel="stylesheet">
    <script src="{{ asset('builder/filestack-0.1.10.js') }}"></script>
    <script src="{{ asset('builder/grapes.min.js') }}"></script>
    <script src="{{ asset('builder/grapesjs-preset-webpage.min.js') }}"></script>
    <link href="{{ asset('assets/global/plugins/toastr/toastr.min.css') }}" rel="stylesheet">


    <script src="https://unpkg.com/grapesjs-plugin-forms"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        body,
        html {
            height: 100%;
            margin: 0;
        }

        .save__as__table tr td {
            padding: 10px;
        }

        .template_title, .template-type {
            display: block;
            width: -moz-available;
            width: -webkit-fill-available;
            padding: .375rem .75rem;
            font-size: 1rem;
            line-height: 1.5;
            color: #495057;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            border-radius: .25rem;
            transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
        }

        .template_cell {
            width: 8%;
            display: table-cell;
            height: 75px;
        }

        .template_save_btn {
            width: -moz-available;
            width: -webkit-fill-available;
            background-color: #1c2c6b;
            cursor: pointer;
            border-radius: 5px;
            border: 1px solid #1c2c6b;
            font-size: 15px;
            color: white;
            padding: 7px;
        }

        .gjs-mdl-dialog-sm {
            width: 40%;
        }
    </style>
</head>
<body>

<div id="gjs" style="height:0px; overflow:hidden"></div>

<script type="text/javascript">

    var adminWebBuilderObj = {
        form_action_url : 'https://google.com/shakil',
        form_action_name : 'google dsfsdg',
        grapesJsSelectAndSaveOption: '{!! ViewHelper::grapesJsSelectAndSaveOption() !!}',
        update_image_url: '{!! route('update-image') !!}',
        admin_web_builder_add_url: '{!! route('admin-web-builder-add') !!}',
        admin_web_builder_redirect_url: "{{ route('admin-template-list-show')}}",
        default_style: '{{ \Modules\WebBuilder\Entities\WebBuilder::DEFAULT_STYLE }}'
    };
</script>

<script src="{{ mix('js/adminWebBuilder.js') }}"></script>

</body>
</html>







