<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Admin Web Builder</title>
    <link href="{{ asset('builder/grapes.min.css') }}" rel="stylesheet">
    <link href="{{ asset('builder/grapesjs-preset-newsletter.css') }}" rel="stylesheet">

    <script src="{{ asset('assets/global/plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('builder/aviary.js') }}"></script>
    <script src="{{ asset('builder/grapes.min.js') }}"></script>
    <script src="{{ asset('builder/ckeditor.js') }}"></script>
    <script src="{{ asset('builder/grapesjs-plugin-ckeditor.min.js') }}"></script>
    <script src="{{ asset('builder/grapesjs-preset-newsletter.min.js') }}"></script>
    <script src="{{ asset('builder/grapesjs-aviary.min.js') }}"></script>

    <script src="{{ asset('assets/global/plugins/toastr/toastr.min.js') }}"></script>
    <link href="{{ asset('assets/global/plugins/toastr/toastr.min.css') }}" rel="stylesheet">


    <script src="https://unpkg.com/grapesjs-plugin-forms"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        body,
        html {
            height: 100%;
            margin: 0;
        }

        .save__as__table tr td {
            padding: 10px;
        }

        .template_title, .template-type {
            display: block;
            width: -moz-available;
            width: -webkit-fill-available;
            padding: .375rem .75rem;
            font-size: 1rem;
            line-height: 1.5;
            color: #495057;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            border-radius: .25rem;
            transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
        }

        .template_cell {
            width: 8%;
            display: table-cell;
            height: 75px;
        }

        .template_save_btn {
            width: -moz-available;
            width: -webkit-fill-available;
            background-color: #1c2c6b;
            cursor: pointer;
            border-radius: 5px;
            border: 1px solid #1c2c6b;
            font-size: 15px;
            color: white;
            padding: 7px;
        }

        .gjs-mdl-dialog-sm {
            width: 40%;
        }

        .editor-row {
            display: flex;
            justify-content: flex-start;
            align-items: stretch;
            flex-wrap: nowrap;
            height: 100%;
        }

        .editor-canvas {
            flex-grow: 1;
        }

        .panel__right {
            flex-basis: 230px;
            position: relative;
            overflow-y: auto;
        }
        .page_list_box{
            text-align: center;
            border: 1px solid #255b36;
            padding: 60px;
            background: aliceblue;
            font-size: 30px;
            margin-top: 2px;
            margin-right: 20px;
            margin-bottom: 2px;
            margin-left: 20px;

        }
        a {
            display: block;
            text-decoration: none;
        }
        .sk_active { border: 1px solid red !important; }

    </style>
</head>
<body>

<div id="gjs">{!! $adminTemplateHtml !!}</div>
<div id="gjs-new-div"></div>
<script src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>

<script>

</script>

<script type="text/javascript">

    var adminEbookBuilderObj = {
        grapesJsSelectAndSaveOption: '{!! ViewHelper::grapesJsEbookSelectAndSaveOption() !!}',
        update_image_url: '{!! route('update-image') !!}',
        admin_ebook_add_url: '{!! route('admin-web-builder-update') !!}',
        admin_ebook_redirect_url: "{{ route('admin.ebook')}}",
        new_page_add: "{{ route('admin.new-page-add')}}",
        admin_template_page_url: "{{ route('admin-template-list-show')}}",
        adminTemplateId: '{{ $adminTemplateId }}',
        templateTitle: '{{ $templateTitle }}',
        templateType: '{{ $templateType }}',
        default_style: '{{ \Modules\WebBuilder\Entities\WebBuilder::DEFAULT_STYLE }}'
    };

</script>
<script type="text/javascript">

    localStorage.clear();

    var host = '//artf.github.io/grapesjs/';
    var images = [
        host + 'img/grapesjs-logo.png',
        host + 'img/tmp-blocks.jpg',
        host + 'img/tmp-tgl-images.jpg',
        host + 'img/tmp-send-test.jpg',
        host + 'img/tmp-devices.jpg',
    ];

    const _token = $('meta[name="csrf-token"]').attr('content');
    var ebookContent = '';

    // Set up GrapesJS editor with the Newsletter plugin
    var editor = grapesjs.init({
        clearOnRender: true,
        height: '100%',
        storageManager: {
            id: 'gjs',
        },
        assetManager: {
            assets: images,
            upload: 0,
            uploadText: 'Uploading is not available in this demo',
        },
        container: '#gjs',
        fromElement: true,
        plugins: ['gjs-preset-newsletter', 'gjs-plugin-ckeditor'],
        pluginsOpts: {
            'gjs-preset-newsletter': {
                modalLabelImport: 'Paste all your code here below and click import',
                modalLabelExport: 'Copy the code and use it wherever you want',
                codeViewerTheme: 'material',
                //defaultTemplate: templateImport,
                importPlaceholder: '<table class="table"><tr><td class="cell">Hello world!</td></tr></table>',
                cellStyle: {
                    'font-size': '12px',
                    'font-weight': 300,
                    'vertical-align': 'top',
                    color: 'rgb(111, 119, 125)',
                    margin: 0,
                    padding: 0,
                }
            },
            'gjs-plugin-ckeditor': {
                position: 'center',
                options: {
                    startupFocus: true,
                    extraAllowedContent: '*(*);*{*}', // Allows any class and any inline style
                    allowedContent: true, // Disable auto-formatting, class removing, etc.
                    enterMode: CKEDITOR.ENTER_BR,
                    extraPlugins: 'sharedspace,justify,colorbutton,panelbutton,font',
                    toolbar: [
                        {name: 'styles', items: ['Font', 'FontSize']},
                        ['Bold', 'Italic', 'Underline', 'Strike'],
                        {name: 'paragraph', items: ['NumberedList', 'BulletedList']},
                        {name: 'links', items: ['Link', 'Unlink']},
                        {name: 'colors', items: ['TextColor', 'BGColor']},
                    ],
                }
            }
        }
    });

    editor.Panels.addButton
    ('options',
        [{
            id: 'save-db',
            className: 'fa fa-floppy-o',
            command: 'save-db',
            attributes: {title: 'Upload'}
        }]
    );

    editor.Panels.addButton
    ('options',
        [{
            id: 'redirect-admin-template',
            className: 'fa fa-external-link',
            command: 'redirect-admin-template',
            attributes: {title: 'Redirect to template page'}
        }]
    );


    // Let's add in this demo the possibility to test our newsletters
    var mdlClass = 'gjs-mdl-dialog-sm';
    var pnm = editor.Panels;
    var cmdm = editor.Commands;
    var md = editor.Modal;

    // Add info command
    var infoContainer = document.getElementById("info-panel");
    cmdm.add('open-info', {
        run: function (editor, sender) {
            sender.set('active', 0);
            var mdlDialog = document.querySelector('.gjs-mdl-dialog');
            mdlDialog.className += ' ' + mdlClass;
            infoContainer.style.display = 'block';
            md.setTitle('About this demo');
            md.setContent('');
            md.setContent(infoContainer);
            md.open();
            md.getModel().once('change:open', function () {
                mdlDialog.className = mdlDialog.className.replace(mdlClass, '');
            })
        }
    });
    pnm.addButton('options', [{
        id: 'undo',
        className: 'fa fa-undo',
        attributes: {title: 'Undo'},
        command: function () {
            editor.runCommand('core:undo')
        }
    }, {
        id: 'redo',
        className: 'fa fa-repeat',
        attributes: {title: 'Redo'},
        command: function () {
            editor.runCommand('core:redo')
        }
    }, {
        id: 'clear-all',
        className: 'fa fa-trash icon-blank',
        attributes: {title: 'Clear canvas'},
        command: {
            run: function (editor, sender) {
                sender && sender.set('active', false);
                if (confirm('Are you sure to clean the canvas?')) {
                    editor.DomComponents.clear();
                    setTimeout(function () {
                        localStorage.clear()
                    }, 0)
                }
            }
        }
    }, {
        id: 'view-info',
        className: 'fa fa-question-circle',
        command: 'open-info',
        attributes: {
            'title': 'About',
            'data-tooltip-pos': 'bottom',
        },
    }]);

    // Simple warn notifier
    var origWarn = console.warn;
    toastr.options = {
        closeButton: true,
        preventDuplicates: true,
        showDuration: 250,
        hideDuration: 150
    };
    console.warn = function (msg) {
        toastr.warning(msg);
        origWarn(msg);
    };

    // Beautify tooltips
    var titles = document.querySelectorAll('*[title]');
    for (var i = 0; i < titles.length; i++) {
        var el = titles[i];
        var title = el.getAttribute('title');
        title = title ? title.trim() : '';
        if (!title)
            break;
        el.setAttribute('data-tooltip', title);
        el.setAttribute('title', '');
    }


    // Do stuff on load
    editor.on('load', function () {
        var $ = grapesjs.$;

        //Do something here after load
    });

    window.scrollTo(0, 0);


    // Add the command
    var loadModal = editor.Modal;
    editor.Commands.add
    ('save-db',
        {
            run: function (editor, sender) {


                ebookContent = editor.runCommand('gjs-get-inlined-html');
                console.log(ebookContent)
                if (ebookContent === '') {
                    toastr.error('Please Build a Template');
                    return false;
                }

                if ($('.template_title').val() === '') {
                    toastr.error('Template Title Required');
                    return false;
                }

                const ebookBuilder = {
                    ebookContent: ebookContent,
                    id: '',
                    admin_ebook_template_id: adminEbookBuilderObj.admin_ebook_add_url,
                    _token: _token,
                }

                const webBuilder = {
                    html_data: ebookContent,
                    css_data: '',
                    template_title: adminEbookBuilderObj.templateTitle,
                    template_type: adminEbookBuilderObj.templateType,
                    adminTemplateId: adminEbookBuilderObj.adminTemplateId,
                    _token: _token,
                }
                $('#gjs-new-div').html(ebookContent);

                html2canvas(document.getElementById("gjs-new-div")).then(function (canvas) {
                    var image_data = canvas.toDataURL("image/jpeg", 0.9);


                    webBuilder.image = image_data;
                    $.ajax({
                        url: adminEbookBuilderObj.admin_ebook_add_url,
                        type: "POST",
                        data: webBuilder,
                        success: function (response) {
                            if (response.status) {
                                toastr.success(response.html);
                                // window.location.href = adminWebBuilderEditObj.admin_web_builder_redirect_url;
                            } else {
                                toastr.error(response.html);
                            }
                            $('#gjs-new-div').html("");
                        }
                    });
                });


            }
        });


    editor.Commands.add
    ('redirect-admin-template',
    {
        run: function (editor, sender) {

            window.location.href = adminEbookBuilderObj.admin_template_page_url;
        }
    });


</script>
</body>
</html>