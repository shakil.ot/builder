var AdminWebBuilder = (function () {

    $(function () {

        // Add overflow_visible to prevent hidden action option
        $('.visible.m-portlet__body.extra.p-0').parents('.m-grid__item.m-grid__item--fluid.m-wrapper').addClass('overflow_visible');

        var user_operation_callback = function (response) {
            Swal.fire(response.status, response.html, response.status);
            $('#admin-template-table').DataTable().ajax.reload();
        }


        const _token = document.head.querySelector("[property=csrf-token]").content;


        (function (global, $, _token) {


            $("#search-user-status").val('').selectpicker('refresh');

            var parent_email = null;
            var user_type = null;
            var created_at_from = null;
            var created_at_to = null;
            initializeCreatedAtDateRangePicker();


            // #### DATATABLE FOR USER-LIST. ALL USER LIST FITCH FROM DATABASED
            var dataTable = $('#admin-template-table').DataTable({
                responsive: true,
                lengthMenu: [10, 25, 50, 100, 500],
                pageLength: 10,
                language: {
                    'lengthMenu': 'Display _MENU_',
                },
                searchDelay: 500,
                processing: true,
                serverSide: true,
                ajax: {
                    url: route('get-all-admin-template-list') ,
                    data: function (data) {
                        data.created_at_from_search = created_at_from;
                        data.created_at_to_search = created_at_to;
                    },
                    type: 'get',
                },
                columns: [
                    {data: 'title', name: 'title', "searchable": true, "visible": true , width: "30%"},
                    {data: 'type', name: 'type', "orderable": true, "searchable": true, width: "25%"},
                    {data: 'created_at', name: 'created_at', "orderable": true, "searchable": false, width: "20%"},
                    {data: 'action', name: 'action', "orderable": false, searchable: false, width: "20%"}
                ],
                dom: '<"top-toolbar row"<"top-left-toolbar col-md-9"lB><"top-right-toolbar col-md-3"f>>rt<"bottom-toolbar"<"bottom-left-toolbar"i><"bottom-right-toolbar"p>>',
                buttons: [
                    {
                        extend: 'colvis',
                        text: '<i class="fa \tfa-list-ul pr-1"></i>Columns',
                        attr: {
                            id: "custom-colvis-button",
                            class: 'btn btn-outline-primary btn-sm m-btn',
                            style: '',
                            title: 'Columns Visibility'
                        },
                        columns: ':not(.noVis)',
                        columnText: function (dt, idx, title) {
                            // return (idx+1)+': '+title;
                            return title;
                        },
                        postfixButtons: ['colvisRestore']
                    },
                    {
                        extend: 'colvisGroup',
                        text: '<i class="fa fa-refresh pr-1"></i>Refresh',
                        attr: {
                            class: 'btn btn-outline-primary btn-sm m-btn  m-btn--icon',
                            id: 'show-all-button'
                        },
                        show: ':hidden'
                    },
                    {
                        text: '<i class="fa fa-filter pr-1"></i>Filter',
                        attr: {
                            class: 'btn btn-outline-primary btn-sm m-btn  m-btn--icon',
                            id: 'show-all-button'
                        },
                        action: function (e, dt, node, config) {
                            $('.filter-options').slideToggle();
                        }
                    },
                ]
            });

            //Columns visibility buttons' css
            $("#custom-colvis-button").click(function () {
                $(".buttons-columnVisibility").removeClass("dt-button").addClass("btn btn-outline-primary btn-sm m-btn btn-block");
                $(".buttons-colvisRestore").removeClass("dt-button").addClass("btn btn-outline-brand btn-sm m-btn btn-block");
            });

            function initializeCreatedAtDateRangePicker() {
                let start = moment().subtract(1, 'months');
                let end = moment();

                function cb(start, end) {
                    $('#created_at_range span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }

                $('#created_at_range').daterangepicker({
                    startDate: start,
                    endDate: end,
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    }
                }, function (starting, ending) {
                    let startDate = moment(starting);
                    let endDate = moment(ending);
                    start = startDate;
                    end = endDate;
                    cb(start, end);
                    created_at_from = moment(start, 'MMMM D, YYYY').format("YYYY-MM-DD");
                    created_at_to = moment(end, 'MMMM D, YYYY').format("YYYY-MM-DD");
                    dataTable.ajax.reload();
                });
                $('#created_at_range span').html('Select Created Date');
            }


            $("#show-all-button").click(function () {
                refreshTable();
            });

            /* search starts */
            $("#search-title").keyup(function () {
                dataTable.column($(this).attr('data-column'))
                    .search($(this).val())
                    .draw();
            });

            $("#search-parent-email").keyup(function () {
                parent_email = $(this).val();
                dataTable.ajax.reload();
            });

            $("#search-user-status").change(function () {
                dataTable.column($(this).attr('data-column'))
                    .search($(this).val())
                    .draw();
            });
            /*Search ends */

            /* reset search starts*/
            $('.reset-user-email-search').on('click', function () {
                $("#search-title").val('');
                dataTable.column($(this).attr('data-column'))
                    .search('')
                    .draw();
            });

            $('.reset-parent-email-search').on('click', function () {
                $("#search-parent-email").val('');
                parent_email = null;
                dataTable.ajax.reload();
            });

            $('.reset-user-status-search').on('click', function () {
                $("#search-user-status").val('').selectpicker('refresh');
                dataTable.column($(this).attr('data-column'))
                    .search('')
                    .draw();
            });

            $('.reset-created_at_range').on('click', function () {
                $('#created_at_range span').html('Select Created Date');
                created_at_from = null;
                created_at_to = null;
                dataTable.ajax.reload();
            });

            /* reset search starts*/
            function refreshTable(params) {
                $("#search-user-email").val('');
                $("#search-parent-email").val('');
                parent_email = null;
                $("#search-user-status").val('').selectpicker('refresh');
                $('#created_at_range span').html('Select Created Date');
                created_at_from = null;
                created_at_to = null;
                dataTable.columns().search('');
                dataTable.search('');
                dataTable.ajax.reload();
            }


            $(document).on('click','.action', function (e) {
                var id = $(this).attr('data-id');
                var settingsType = $(this).attr('data-value');
                console.log(settingsType);

                if (!settingsType)
                    return false;

                switch (settingsType) {
                    case "delete-admin-template":
                        deleteAdminTemplate(id);
                        break;
                    default :
                        toastr.error("Invalid Setting type", "Server Notification");
                        break;
                }

                function deleteAdminTemplate(id) {

                    Swal.fire({
                        title: "Are you sure?",
                        text: "You won't be able to retrieve this user!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete this user!",
                        cancelButtonText: "No, cancel please!",
                        closeOnConfirm: false,
                        closeOnCancel: true
                    }).then(function(result) {
                        if (result.value) {
                            let dataObject = {
                                url: route('delete-admin-template'),
                                type: 'post',
                                data: {id: id, _token: _token},
                                callback: user_operation_callback,
                                callBackError: callBackError,
                            }
                            globalAjax(dataObject);
                        }
                    });
                }
            });

            $(document).on('click' , '#add_admin_template' , function () {
                $('#add_admin_template_modal').modal('show');
            });

            $(document).on('click' , '#create_template_using_type' , function () {

                if ($('#template_title').val() == ''){
                    toastr.error('Template title can not be empty !!');
                    return false;
                }

                if ($('#template_category').val() == ''){
                    toastr.error('Template category can not be empty !!');
                    return false;
                }

                $.ajax({
                    url: route('create-template-using-type'),
                    type: "POST",
                    data: {
                        template_title : $('#template_title').val(),
                        template_category : $('#template_category').val(),
                        _token : _token,
                    },
                    success: function (response) {
                        if (response.status){
                            toastr.success(response.html)
                            window.location.href = response.url;
                        }else {
                            toastr.error(response.html)
                        }
                    }
                });
            });



        })(window, jQuery, _token);
    });

})();



