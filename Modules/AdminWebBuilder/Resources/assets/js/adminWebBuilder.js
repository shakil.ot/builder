var builder = (function (adminWebBuilderObj) {

    $(function () {

        const _token = $('meta[name="csrf-token"]').attr('content');

        (function (global, $, _token) {
            var html_data = '';
            var css_data = '';
            const LandingPage = {
                style: adminWebBuilderObj.default_style,
            };

            var editor = grapesjs.init({
                height: '100%',
                showOffsets: 1,
                noticeOnUnload: 0,
                storageManager: {
                    autoload: false,
                },
                style: LandingPage.style,
                container: '#gjs',
                fromElement: true,
                assetManager:
                    {
                        // Upload endpoint, set `false` to disable upload, default `false`
                        upload: adminWebBuilderObj.update_image_url,

                        // The name used in POST to pass uploaded files, default: `'files'`
                        uploadName: 'files',
                    },

                plugins: ['gjs-preset-webpage', 'grapesjs-plugin-forms'],
                pluginsOpts:
                    {
                        'gjs-preset-webpage':
                            {}
                        ,
                        'formsOpts':
                            {}
                        ,
                    }
            });

            editor.DomComponents.addType('form', {
                // Make the editor understand when to bind `my-input-type`
                // Model definition
                model: {
                    defaults: {
                        tagName: 'input',
                        attributes: { // Default attributes
                            // type: 'input',
                            // name: 'default-name',
                            // placeholder: 'Insert text here',
                            // action  : 'google',
                            // method : 'post'
                        },
                        traits: [{
                            type: 'select',
                            name: 'method',
                            options: [
                                {value: 'post', name: 'POST'},
                            ],
                        }, {
                            type: 'select',
                            name: 'action',
                            options: [
                                {value: adminWebBuilderObj.form_action_url, name: adminWebBuilderObj.form_action_name },
                            ],
                        }
                        ],
                    }
                }
            });


            const domc = editor.DomComponents;

            editor.Panels.addButton
            ('options',
                [{
                    id: 'save-db',
                    className: 'fa fa-floppy-o',
                    command: 'save-db',
                    attributes: {title: 'Upload'}
                }]
            );

            var loadModal = editor.Modal;
            var loadCommands = editor.Commands;
            var mdlClass = 'gjs-mdl-dialog-sm';


            // Add the command
            editor.Commands.add
            ('save-db',
                {
                    run: function (editor, sender) {

                        sender.set('active', 0);
                        var modalContent = loadModal.getContentEl();
                        var mdlDialog = document.querySelector('.gjs-mdl-dialog');
                        // var cmdGetCode = cmdm.get('gjs-get-inlined-html');
                        // contentEl.value = cmdGetCode && cmdGetCode.run(editor);
                        mdlDialog.className += ' ' + mdlClass;
                        // testContainer.style.display = 'block';
                        loadModal.setTitle('Save Your Template');
                        // md.setContent('');
                        loadModal.setContent(adminWebBuilderObj.grapesJsSelectAndSaveOption);
                        loadModal.open();
                        $('.gjs-mdl-dialog').find('.template_title').attr('placeholder', 'Enter your template title')
                        loadModal.getModel().once('change:open', function () {
                            mdlDialog.className = mdlDialog.className.replace(mdlClass, '');
                            document.getElementsByClassName("template_title").placeholder = "Type name here..";
                            //clean status
                        });
                        html_data = editor.getHtml(true);
                        css_data = editor.getCss(true);

                        sender && sender.set('active', 0); // turn off the button
                        editor.store();
                    }
                });

            $(document).on("click", ".template_save_btn", function () {

                if (html_data == ''){
                    toastr.error('Please Build a Template');
                    return false;
                }

                if ($('.template_title').val() == ''){
                    toastr.error('Template Title Required');
                    return false;
                }



                const webBuilder = {
                    html_data: html_data,
                    css_data: css_data,
                    template_title: $('.template_title').val(),
                    template_type: $('.template-type').val(),
                    _token: _token,
                }

                $.ajax({
                    url: adminWebBuilderObj.admin_web_builder_add_url,
                    type: "POST",
                    data: webBuilder,
                    success: function (response) {
                        if (response.status){
                            toastr.success(response.html)
                            window.location.href = adminWebBuilderObj.admin_web_builder_redirect_url;
                        }else {
                            toastr.error(response.html)
                        }
                    }
                });
            });





        })(window, jQuery, _token);
    });

})(adminWebBuilderObj);



