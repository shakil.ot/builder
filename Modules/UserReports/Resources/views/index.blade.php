@extends('user.layout.app', ['menu' => 'reports'])

@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-2 mr-5">Reports</h5>
                        <!--end::Page Title-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Card-->
                <div class="card card-custom">
                    <div class="card-body">
                        <!--begin: Search Form-->
                        <!--begin::Search Form-->
                        <div class="mb-7">
                            <div class="row align-items-center mb-4">
                                <div class="col-lg-9 col-xl-8">
                                    <div class="row align-items-center">
                                        <div class="col-md-4 my-2 my-md-0">
                                            <div class="d-flex align-items-center">
                                                <label class="mr-3 mb-0">Type</label>
                                                <select class="form-control" id="kt_datatable_search_type">
                                                    <option value="">Choose Report</option>
                                                    <option value="1" {{ $type == 1 ? 'selected' : '' }}>Achievement Claimed</option>
                                                    <option value="2" {{ $type == 2 ? 'selected' : '' }}>Activity Log</option>
                                                    <option value="3" {{ $type == 3 ? 'selected' : '' }}>Perks Claimed</option>
                                                    <option value="4" {{ $type == 4 ? 'selected' : '' }}>Training Claimed</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row align-items-center">
                                <div class="col-lg-8 col-xl-8">
                                    <div class="row align-items-center">
                                        <div class="col-md-6 my-2 my-md-0">
                                            <div class="form-group">
                                                <label class="mr-3 mb-0">Start Date</label>
                                                <input type="text" class="form-control date_picker" placeholder="Select start date" id="kt_datepicker_4_3">
                                            </div>
                                        </div>
                                        <div class="col-md-6 my-2 my-md-0">
                                            <div class="form-group">
                                                <label class="mr-3 mb-0">End Date</label>
                                                <input type="text" class="form-control date_picker" placeholder="Select end date" id="kt_datepicker_4_3">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-xl-4 mt-5 mt-lg-0">
                                    <a href="#" class="btn btn-light-green px-6 font-weight-bold">Search</a>
                                    <a href="#" class="btn btn-green px-6 font-weight-bold float-right">Export</a>
                                </div>
                            </div>
                        </div>
                        <!--end::Search Form-->
                        <!--end: Search Form-->
                        <!--begin: Datatable-->
                        <div class="datatable datatable-bordered datatable-head-custom" id="kt_datatable"></div>
                        <!--end: Datatable-->
                    </div>
                </div>
                <!--end::Card-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>

@endsection

@section('script')
{{--    <script src="{{ asset('js/user-report.js') }}"></script>--}}
    <script src="{{ asset('assets/dummy-data/report-dummy.js') }}"></script>
<script>
    $('.date_picker').datepicker({
        rtl: KTUtil.isRTL(),
        orientation: "bottom left",
        todayHighlight: true,
        templates: {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    });
</script>
@endsection

