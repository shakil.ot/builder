@extends('admin.layouts.app')


@section('container')

    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">

        <!--begin::Entry-->
        <div class="card card-custom">
            <div class="card-header">
                <div class="card-title">
                    <span class="card-icon">
                        <i class="flaticon-users-1 text-primary"></i>
                    </span>
                    <h3 class="card-label">Auto Follow-ups</h3>
                </div>
                <div class="card-toolbar">
                    <!--begin::Dropdown-->

                    <!--end::Dropdown-->
                    <!--begin::Button-->
                    <a class="btn font-weight-bolder admin-followup-add" style="background-color: #053a6f; color: #ffffff">+ New Follow-up</a>

                    <!--end::Button-->
                </div>
            </div>
            <div class="card-body">
            @include('admin.user.filter')
            <!--begin: Datatable-->
                <!--begin: Datatable-->
                <table class="table table-bordered table-hover table-checkable" id="admin-auto-followup-table">
                    <thead>
                    <tr>
                        <th>Email Subject</th>
                        <th>Body</th>
                        <th>Created At</th>
                        <th>Cumulative Send Day</th>
                        <th>Action</th>

                    </tr>
                    </thead>

                </table>
                <!--end: Datatable-->
            </div>
        </div>
        <!--end::Entry-->
    </div>



@endsection

@push('scripts')
    <script src="{{mix('/js/admin-auto-followup.js')}}"> </script>

{{--    <script src="{{ mix('/js/admin-auto-followup.js') }}"></script>--}}

@endpush
