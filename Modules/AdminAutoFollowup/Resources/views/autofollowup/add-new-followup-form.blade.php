<div class="card-body">
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="personal_info" role="tabpanel" aria-labelledby="kt_tab_pane_2">
            <form id="auto-followup-form" autocomplete="off" data-parsley-validate>
                @csrf

                <div class="form-group w-100 d-flex justify-content-between bothOne">
                    <div class="leftOne" >
                        <button type="button" title="Personalize" class="btn btn-secondary dropdown-toggle personalize-toggle" style="color: #ffffff;background-color: #03375D;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Subject Personalize</button>
                        <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end"
                             style="height:400px; overflow-y:auto; position: absolute; transform: translate3d(-20px, 37px, 0px); top: 0px; left: 0px; will-change: transform;">
                            @foreach(\App\Models\PersonalizeType::$allType as $each => $key)
                                <button class="dropdown-item url-get-inbox-in-email" type="button"
                                        data-title-email="message-get--email" data-url-email="{{$key}}">{{$each}}
                                </button>
                            @endforeach
                        </div>
                    </div>
                    <div class="rightOne" >
                        <div class="btn-group" role="group" aria-label="Button group with nested dropdown" style="float:right">
                            <div class="btn-group" role="group">
                                <button id="btnGroupDrop1" type="button"  style="color: #ffffff;background-color: #03375D;" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Select Email Template
                                </button>
                                <div class="dropdown-menu" style="">
                                    @if(isset($emailTemplates[0]))
                                        @foreach($emailTemplates as $emailTemplate)
                                            <a class="dropdown-item email-template" data-id="{{$emailTemplate->id}}" href="javascript:void(0)" data-title="message-get">{{$emailTemplate->title}}</a>
                                        @endforeach
                                    @else
                                        No templates available
                                    @endif
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="form-group w-100">
                    <label for="subject" class="col-form-label">Email Subject</label>
                    <input class="form-control" type="text" name="subject" id="subject"
                           placeholder="Enter Email Subject" required>
                    <div class="custom-error-message"></div>
                </div>
                <div class="form-group w-100">
                    <label for="days" class="col-form-label">Send the number of days after the previous email : (Enter days)</label>
                    <input class="form-control" type="number" min="0" name="subject" id="day"
                           placeholder="Enter the number of day after the previous email" required>
                    <div class="custom-error-message"></div>
                </div>
                <div class="form-group w-100">
                    <label for="body" class="col-form-label">Email Body</label>
                    <textarea class="form-control body" name="body" id="emailBody" cols="30"
                              rows="10"></textarea>
                    <div class="custom-error-message"></div>
                </div>

                <div class="form-group mt-10 w-100 add__followUp">
                    <button type="submit" class="btn btn-green font-weight-bold d-flex align-items-center">
                        <i class="fa fa-plus"></i> Add Followup
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $(document).on('click', '.url-get-inbox-in-email', function (e) {
            e.preventDefault();
            let element = $(this);
            let areaId = element.data('title-email');
            let text_h = element.data('url-email');

            let text = $('#subject');
            text.val(text.val() + text_h);

        });

    });

</script>

