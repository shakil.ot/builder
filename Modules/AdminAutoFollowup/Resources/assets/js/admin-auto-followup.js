$(function () {

    const _token = document.head.querySelector("[property=csrf-token]").content;

    (function (global, $, _token) {
        // Datatable
        let dataTable = $('#admin-auto-followup-table').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            stateSave: false,
            "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
            pageLength: 10,
            ajax: {
                url: route('admin-auto-followup-list'),
                data: function (data) {},
                type: 'get',
            },
            columns: [
                {data: 'subject', name: 'email_subject', "orderable": true, "searchable": true, width: "20%"},
                {data: 'email_body', name: 'email_body', "orderable": true, "searchable": false, width: "25%"},
                {data: 'data_created_at', name: 'created_at', "orderable": true, "searchable": false, width: "20%"},
                {data: 'cumulative_send_day', name: 'cumulative_send_day', "orderable": true, "searchable": false, width: "15%"},
                {data: 'action', name: 'action', "orderable": true, "searchable": false, width: "40%"}
            ]
        });





        let addButton = $(".admin-followup-add");
        addButton.on('click', function () {
            $('#preloader').show();

            let detailPopUp = global.customPopup.show({
                header: 'Add Follow-up',
                message: '',
                dialogSize: 'lg',
                dialogClass: 'prevent-to-show'
            });

            detailPopUp.on('shown.bs.modal', function () {
                let modal = $(this);

                global.mySiteAjax({
                    url: route('admin-add-followup'),
                    type: 'get',
                    loadSpinner: true,
                    success: function (response) {
                        $('#preloader').hide();
                        window.nbUtility.ajaxErrorHandling(response, function () {
                            modal.find('.modal-body').html(response.html);
                            modal.removeClass('prevent-to-show');
                            function sendFile(file, editor) {
                                $('#preloader').show();
                                let data;
                                data = new FormData();
                                data.append("file", file);
                                data.append("_token", _token);
                                $.ajax({
                                    data: data,
                                    type: "POST",
                                    url: route('image-upload-from-summernote'),
                                    cache: false,
                                    contentType: false,
                                    processData: false,
                                    success: function (data) {
                                        $('#preloader').hide();
                                        if (data.status == 'success') {
                                            let image = $('<img>').attr('src', data.url);
                                            editor.summernote("insertNode", image[0]);
                                        } else {
                                            bootstrapNotify.error(data.message,'error');
                                        }

                                    }
                                });
                            }


                            let elem = $('#emailBody');
                            $('#emailBody').summernote({
                                airMode: false,
                                height: 300,
                                focus: true,
                                dialogsInBody: true,
                                placeholder: "Enter Text Here... Type @ with a whitespace to get personalize tag",
                                maximumImageFileSize: 500 * 512, // 500 KB

                                callbacks: {
                                    onInit: function () {
                                        let editor = elem.next(),
                                            placeholder = editor.find(".note-placeholder");

                                        function isEditorEmpty() {
                                            let code = elem.summernote("code");
                                            return code === "<p><br></p>" || code === "";
                                        }

                                        editor.on("focusin focusout", ".note-editable", function (e) {
                                            if (isEditorEmpty()) {
                                                placeholder[e.type === "focusout" ? "show" : "show"]();
                                            }
                                        });
                                    },
                                    onImageUpload: function (files) {
                                        let editor = $(this);
                                        sendFile(files[0], editor);
                                    },
                                    onImageUploadError: function(msg){
                                        bootstrapNotify.error('FileSize too large !!!','error');
                                    }
                                },
                                hint: {
                                    mentions: ['first_name', 'last_name', 'email', 'number', 'my_first_name', 'my_last_name','my_email','my_number'],
                                    match: /\B@(\w*)$/,
                                    search: function (keyword, callback) {
                                        callback($.grep(this.mentions, function (item) {
                                            return item.indexOf(keyword) == 0;
                                        }));
                                    },
                                    content: function (item) {
                                        return '[' + '[' + item + ']' + ']';
                                    }
                                },
                                toolbar: [
                                    ['style', ['highlight', 'personalize', 'bold', 'italic', 'underline', 'clear']],
                                    ['font', ['strikethrough', 'superscript', 'subscript']],
                                    ['para', ['ul', 'ol', 'paragraph']],
                                    ['table', [
                                        ['add', ['addRowDown', 'addRowUp', 'addColLeft', 'addColRight']],
                                        ['delete', ['deleteRow', 'deleteCol', 'deleteTable']],
                                    ]],
                                    ['image', [
                                        ['image', ['resizeFull', 'resizeHalf', 'resizeQuarter', 'resizeNone']],
                                        ['float', ['floatLeft', 'floatRight', 'floatNone']],
                                        ['remove', ['removeMedia']]
                                    ]],
                                    ['insert', ['link', 'picture','table']],
                                    ['height', ['height']],
                                    ['color', ['color']],
                                    ['fontsize', ['fontsize']],
                                    ['misc',['undo','redo']]
                                ],
                                buttons: {
                                    personalize: personalizeDropDown
                                }
                            });
                            let emailControls = [
                                ['FIRST_NAME', '[[first_name]]'],
                                ['LAST_NAME', '[[last_name]]'],
                                ['EMAIL', '[[email]]'],
                                ['NUMBER', '[[number]]'],
                                ['MY_FIRST_NAME', '[[my_first_name]]'],
                                ['MY_LAST_NAME', '[[my_last_name]]'],
                                ['MY_EMAIL', '[[my_email]]'],
                                ['MY_NUMBER', '[[my_number]]'],
                            ];
                            let personalizeDropDown = function () {
                                let ui = $.summernote.ui;
                                let keywordMap = new Map(emailControls);
                                let list = Array.from(keywordMap.keys());

                                let button = ui.buttonGroup([
                                    ui.button({
                                        contents: 'Personalize <i class="fa fa-caret-down" aria-hidden="true"></i>',
                                        tooltip: 'Personalize Tag',
                                        data: {
                                            toggle: 'dropdown'
                                        }
                                    }),
                                    ui.dropdown({
                                        items: list,
                                        callback: function (items) {
                                            $(items).find('a.dropdown-item').click(function (e) {
                                                let array_lenth = emailControls.length;
                                                let data_value = $(this).attr('data-value');
                                                let get_text = '';
                                                for (let increment = 0; increment < array_lenth; increment++) {
                                                    let key = Array.from(keywordMap.keys())[increment];
                                                    if (key == data_value) {
                                                        get_text = keywordMap.get(key);
                                                    }
                                                }
                                                $('#emailBody').summernote('editor.insertText', ' ' + get_text);
                                                e.preventDefault();
                                            })
                                        }
                                    })
                                ]);
                                return button.render();
                            }

                            let request = {
                                'subject': $('#subject').val(),
                                'day': $('#day').val(),
                                'body': $('#emailBody').summernote('code'),
                                _token: _token
                            }

                            $('#auto-followup-form').on('submit', function (e) {
                                e.preventDefault();
                                $('#preloader').show();
                                let request = {
                                    'subject': $('#subject').val(),
                                    'day': $('#day').val(),
                                    'body': $('#emailBody').summernote('code'),
                                    _token: _token
                                }

                                global.mySiteAjax({
                                    url: route('admin.followup-form-submit'),
                                    type: 'POST',
                                    data: request,
                                    contentType: false,
                                    processData: false,
                                    loadSpinner: true,
                                    success: function (response) {
                                        if (response.status === true) {
                                            bootstrapNotify.success(response.html, 'success');
                                            modal.modal('hide');
                                            $('#admin-auto-followup-table').DataTable().ajax.reload();
                                            $('#preloader').hide();
                                            location.reload();
                                        } else if (response.status === false) {
                                            $('#preloader').hide();
                                            bootstrapNotify.error(response.html, 'error');
                                        }
                                    }

                                })

                            });
                        });
                    }
                });
            });

            detailPopUp.on('hidden.bs.modal', function () {
                $(this).remove();
            });
        });

        $(document).on('click','.email-template',function () {
            let element = $(this);
            let id = element.attr('data-id');
            $.ajax({
                url: route('autofollowup.get-email-template-by-id'),
                type: "post",
                data: {id:id, _token: _token},
                success:function (response) {
                    $('#subject').val(response.subject);
                    $('#emailBody').summernote('code',response.body);

                }
            })
        });

        let editButton = $('.editFollow-up');
        // editButton.on('click', function () {
        $(document).on('click', '.editFollow-up', function () {
            $('#preloader').show();
            let followupId = $(this).attr('data-id');
            let editPopUp = global.customPopup.show({
                header: 'Edit Followup',
                message: '',
                dialogSize: 'lg',
                dialogClass: 'prevent-to-show'
            });

            editPopUp.on('shown.bs.modal', function () {
                let modal = $(this);

                global.mySiteAjax({
                    url: route('admin.followup-edit-form'),
                    type: 'post',
                    data: {followupId: followupId, _token: _token},
                    loadSpinner: true,
                    success: function (response) {
                        $('#preloader').hide();
                        window.nbUtility.ajaxErrorHandling(response, function () {
                            modal.find('.modal-body').html(response.html);
                            modal.removeClass('prevent-to-show');

                            function sendFileEdit(file, editor) {
                                $('#preloader').show();
                                let data;
                                data = new FormData();
                                data.append("file", file);
                                data.append("_token", _token);
                                $.ajax({
                                    data: data,
                                    type: "POST",
                                    url: route('image-upload-from-summernote'),
                                    cache: false,
                                    contentType: false,
                                    processData: false,
                                    success: function (data) {
                                        $('#preloader').hide();
                                        if (data.status == 'success') {
                                            let image = $('<img>').attr('src', data.url);
                                            editor.summernote("insertNode", image[0]);
                                        } else {
                                            bootstrapNotify.error(data.message,'error');
                                        }

                                    }
                                });
                            }


                            let elem = $('#edit_emailBody');
                            $('#edit_emailBody').summernote({
                                airMode: false,
                                height: 300,
                                focus: true,
                                dialogsInBody: true,
                                placeholder: "Enter Text Here... Type @ with a whitespace to get personalize tag",
                                maximumImageFileSize: 500 * 512, // 500 KB

                                callbacks: {
                                    onInit: function () {
                                        let editor = elem.next(),
                                            placeholder = editor.find(".note-placeholder");

                                        function isEditorEmpty() {
                                            let code = elem.summernote("code");
                                            return code === "<p><br></p>" || code === "";
                                        }

                                        editor.on("focusin focusout", ".note-editable", function (e) {
                                            if (isEditorEmpty()) {
                                                placeholder[e.type === "focusout" ? "show" : "show"]();
                                            }
                                        });
                                    },
                                    onImageUpload: function (files) {
                                        let editor = $(this);
                                        sendFileEdit(files[0], editor);
                                    },
                                    onImageUploadError: function(msg){
                                        bootstrapNotify.error('FileSize too large !!!','error');
                                    }
                                },
                                hint: {
                                    mentions: ['first_name', 'last_name', 'email', 'number', 'my_first_name', 'my_last_name','my_email','my_number'],
                                    match: /\B@(\w*)$/,
                                    search: function (keyword, callback) {
                                        callback($.grep(this.mentions, function (item) {
                                            return item.indexOf(keyword) == 0;
                                        }));
                                    },
                                    content: function (item) {
                                        return '[' + '[' + item + ']' + ']';
                                    }
                                },
                                toolbar: [
                                    ['style', ['highlight', 'personalize', 'bold', 'italic', 'underline', 'clear']],
                                    ['font', ['strikethrough', 'superscript', 'subscript']],
                                    ['para', ['ul', 'ol', 'paragraph']],
                                    ['table', [
                                        ['add', ['addRowDown', 'addRowUp', 'addColLeft', 'addColRight']],
                                        ['delete', ['deleteRow', 'deleteCol', 'deleteTable']],
                                    ]],
                                    ['image', [
                                        ['image', ['resizeFull', 'resizeHalf', 'resizeQuarter', 'resizeNone']],
                                        ['float', ['floatLeft', 'floatRight', 'floatNone']],
                                        ['remove', ['removeMedia']]
                                    ]],
                                    ['insert', ['link', 'picture','table']],
                                    ['height', ['height']],
                                    ['color', ['color']],
                                    ['fontsize', ['fontsize']],
                                    ['misc',['undo','redo']]
                                ],
                                buttons: {
                                    personalize: personalizeDropDown
                                }
                            });
                            let emailControls = [
                                ['FIRST_NAME', '[[first_name]]'],
                                ['LAST_NAME', '[[last_name]]'],
                                ['EMAIL', '[[email]]'],
                                ['NUMBER', '[[number]]'],
                                ['MY_FIRST_NAME', '[[my_first_name]]'],
                                ['MY_LAST_NAME', '[[my_last_name]]'],
                                ['MY_EMAIL', '[[my_email]]'],
                                ['MY_NUMBER', '[[my_number]]'],
                            ];
                            let personalizeDropDown = function () {
                                let ui = $.summernote.ui;
                                let keywordMap = new Map(emailControls);
                                let list = Array.from(keywordMap.keys());

                                let button = ui.buttonGroup([
                                    ui.button({
                                        contents: 'Personalize <i class="fa fa-caret-down" aria-hidden="true"></i>',
                                        tooltip: 'Personalize Tag',
                                        data: {
                                            toggle: 'dropdown'
                                        }
                                    }),
                                    ui.dropdown({
                                        items: list,
                                        callback: function (items) {
                                            $(items).find('a.dropdown-item').click(function (e) {
                                                let array_lenth = emailControls.length;
                                                let data_value = $(this).attr('data-value');
                                                let get_text = '';
                                                for (let increment = 0; increment < array_lenth; increment++) {
                                                    let key = Array.from(keywordMap.keys())[increment];
                                                    if (key == data_value) {
                                                        get_text = keywordMap.get(key);
                                                    }
                                                }
                                                $('#edit_emailBody').summernote('editor.insertText', ' ' + get_text);
                                                e.preventDefault();
                                            })
                                        }
                                    })
                                ]);
                                return button.render();
                            }

                            $('#edit-auto-followup-form').on('submit', function (e) {
                                // To DO
                                e.preventDefault();
                                $('#preloader').show();
                                let request = {
                                    'subject': $('#edit_subject').val(),
                                    'day': $('#edit_day').val(),
                                    'body': $('#edit_emailBody').summernote('code'),
                                    'followupId': $('#followupId').val(),
                                    'previousSendDay': $('#previousSendDay').val(),
                                    _token: _token
                                }

                                global.mySiteAjax({
                                    url: route('admin.edit-followup-form-submit'),
                                    type: 'POST',
                                    data: request,
                                    contentType: false,
                                    processData: false,
                                    loadSpinner: true,
                                    success: function (response) {
                                        if (response.status === true) {
                                            bootstrapNotify.success(response.html, 'success');
                                            modal.modal('hide');
                                            $('#admin-auto-followup-table').DataTable().ajax.reload();
                                            $('#preloader').hide();
                                            location.reload();
                                        } else if (response.status === false) {
                                            $('#preloader').hide();
                                            bootstrapNotify.error(response.html, 'error');
                                        }
                                    }

                                })

                            });

                        });
                    }
                });
            });

            editPopUp.on('hidden.bs.modal', function () {
                $(this).remove();
            });
        })

        $(document).on('click','.email-template-in-edit',function () {
            let element = $(this);
            let id = element.attr('data-id');
            $.ajax({
                url: route('autofollowup.get-email-template-by-id'),
                type: "post",
                data: {id:id, _token: _token},
                success:function (response) {
                    $('#edit_subject').val(response[0].subject);
                    $('#edit_emailBody').summernote('code',response[0].body);

                }
            })
        });

        let deleteButton = $('.deleteFollowup');
        // deleteButton.on('click', function () {
        $(document).on('click', '.deleteFollowup', function () {
            let followupId = $(this).attr('data-id');
            Swal.fire({
                title: 'Are you sure?',
                html: "<b class='text-danger'> You cannot retrieve this followup!</b>",
                type: 'warning',
                confirmButtonText: 'Yes, delete the followup',
                confirmButtonClass: 'btn btn-danger confirm-button',
                showCancelButton: true,
                cancelButtonText: 'No, keep it!',
                cancelButtonClass: 'btn btn-green cancel-button'
            }).then(function (result) {
                if (result.value) {

                    global.mySiteAjax({
                        url: route('admin.followup-delete'),
                        type: 'post',
                        data: {followupId: followupId, _token: _token},
                        loadSpinner: true,

                        beforeSend: function () {
                            $('#preloader').show();
                        },
                        complete: function (xhr, stat) {
                            $('#preloader').hide();
                        },
                        success: function (response) {
                            if (response.status === true) {
                                bootstrapNotify.success(response.html, "Success");
                                $('#admin-auto-followup-table').DataTable().ajax.reload();
                                // location.reload();
                            } else if (response.status === false) {
                                bootstrapNotify.error(response.html, "Error");
                            }

                        }
                    });
                } else {
                    $('#preloader').hide();
                }
            });

        })

        $(document).on('click','.copy-clipboard-click',function () {
            let copyText = document.getElementById("copy-clipboard");
            copyText.select();
            copyText.setSelectionRange(0, 99999)
            document.execCommand("copy");
        });

    })(window, jQuery, _token);
});