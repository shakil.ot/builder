<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminAutoFollowupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_auto_followups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('email_subject');
            $table->string('email_body');
            $table->integer('send_day_after_previous_email');
            $table->integer('cumulative_send_day');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_auto_followups');
    }
}
