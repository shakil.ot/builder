<?php

namespace Modules\AdminAutoFollowup\Contracts\Services;

interface AdminAutoFollowupContact
{
    public function getAdminAutoFollowup();

    public function getFollowupById($followupId);

    public function addAutoFollowup($request);

    public function updateFollowup($request);

    public function deleteFollowup($request);
}