<?php

namespace Modules\AdminAutoFollowup\Contracts\Repositories;

interface AdminAutoFollowupRepository
{
    public function getAdminAutoFollowup();

    public function addAutoFollowupByParam($data);

    public function getFollowupById($followupId);

    public function getAllFollowupsNewerThanThis($followupId);

    public function updateFollowupByParam($where, $data);

    public function deleteFollowupById($followupId);
}