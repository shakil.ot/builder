<?php

namespace Modules\AdminAutoFollowup\Http\Repositories;

use App\Repositories\BaseRepository\BaseRepository;
use Modules\AdminAutoFollowup\Contracts\Repositories\AdminAutoFollowupRepository;
use Modules\AdminAutoFollowup\Entities\AdminAutoFollowup;

class AdminAutoFollowupRepositoryEloquent extends BaseRepository implements AdminAutoFollowupRepository
{
    protected function model()
    {
        return new AdminAutoFollowup();
    }

    public function getAdminAutoFollowup()
    {
        return $this->model()->orderBy('created_at', 'desc');
    }

    public function addAutoFollowupByParam($data)
    {
        return $this->model()->create($data);
    }

    public function getFollowupById($followupId)
    {
        return $this->model()->where('id', $followupId)
            ->first();
    }

    public function getAllFollowupsNewerThanThis($followupId)
    {
        return $this->model()
            ->where('id', '>=', $followupId)
            ->get();
    }

    public function updateFollowupByParam($where, $data)
    {
        return $this->model()
            ->where($where)
            ->update($data);
    }

    public function deleteFollowupById($followupId)
    {
        return $this->model()
            ->where('id', $followupId)
            ->delete();
    }
}