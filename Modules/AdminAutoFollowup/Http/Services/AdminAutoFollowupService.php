<?php

namespace Modules\AdminAutoFollowup\Http\Services;

use App\Helper\UtilityHelper;
use Modules\AdminAutoFollowup\Contracts\Repositories\AdminAutoFollowupRepository;
use Modules\AdminAutoFollowup\Contracts\Services\AdminAutoFollowupContact;
use Yajra\DataTables\DataTables;

class AdminAutoFollowupService implements AdminAutoFollowupContact
{
    private $adminAutoFollowupRepository;
    public function __construct(AdminAutoFollowupRepository $adminAutoFollowupRepository)
    {
        $this->adminAutoFollowupRepository = $adminAutoFollowupRepository;
    }

    public function getAdminAutoFollowup()
    {
        $autoFollowups = $this->adminAutoFollowupRepository->getAdminAutoFollowup();
        return Datatables::of($autoFollowups)

            ->addColumn('subject', function ($autoFollowups){
                if ($autoFollowups->email_subject == null) {
                    return '<span class="label label-lg label-light-danger  label-inline">N/A</span>';
                } else {
                    return '<span class="label label-lg label-success label-inline" style="background-color: #023c64; color: #ffffff;">' . $autoFollowups->email_subject. '</span>';
                }
            })
            ->addColumn('email_body', function ($autoFollowups){
                if ($autoFollowups->email_body == null) {
                    return '<span class="label label-lg label-light-danger  label-inline">N/A</span>';
                } else {
                    return '<span class="label label-lg label-light-success label-inline" style="background-color: #023c64; color: #ffffff;">' . $autoFollowups->email_body . '</span>';
                }
            })
            ->addColumn('data_created_at', function ($autoFollowups){
                if ($autoFollowups->created_at == null) {
                    return '<span class="label label-lg label-light-danger  label-inline">N/A</span>';
                } else {
                    return '<span class="label label-lg label-light-success label-inline" style="background-color: #023c64; color: #ffffff;">' . $autoFollowups->created_at . '</span>';
                }
            })
            ->addColumn('cumulative_send_day', function ($autoFollowups){
                if ($autoFollowups->cumulative_send_day == null)
                {
                    return '<span class="label label-lg label-light-danger  label-inline">N/A</span>';
                }
                else
                {
                    return '<span class="label label-lg label-light-danger  label-inline" style="background-color: #023c64; color: #ffffff;">' . $autoFollowups->cumulative_send_day. '</span>';
                }

            })
            ->addColumn('action', function ($autoFollowups){
                $btn =
                    '   <a href="javascript:void(0)" class="editFollow-up" id="editFollowUp"
                            data-id="' . $autoFollowups->id . ' ">
                            <i class="la la-pencil"></i> Edit </span>
                            </a>'
                    .
                    '<a href="javascript:void(0)" class="deleteFollowup"
                            data-value="delete" data-id="' . $autoFollowups->id . '"><i class="la la-trash-o"></i> Delete
                            </a>
                        </div>'
                ;
                return $btn;
            })
            ->rawColumns(['subject','email_body', 'data_created_at', 'cumulative_send_day', 'action'])
            ->make(true);

    }

    public function addAutoFollowup($request)
    {
        if ($request->get('subject') == null || $request->get('subject') == '') {
            return UtilityHelper::RETURN_STATUS_FORMATE(false, 'Please enter email subject');
        }
        if ($request->get('body') == null || $request->get('body') == '') {
            return UtilityHelper::RETURN_STATUS_FORMATE(false, 'Please enter email body');
        }
        $sendDayAfterPreviousEmail = 0;
        $cumulativeSendDay = 0;
        if ($request->get('day') != null && $request->get('day') != '') {
            $sendDayAfterPreviousEmail = $request->get('day');
            $cumulativeSendDay = $request->get('day');
        }
//        $lastAddedFollowup = $this->autoFollowupRepository->getLastAddedFollowupByUserId($userId);
//        if ($lastAddedFollowup) {
//            $cumulativeSendDay = $cumulativeSendDay + $lastAddedFollowup['cumulative_send_day'];
//        }
        $data = [
            'email_subject' => $request->get('subject'),
            'email_body' => $request->get('body'),
            'send_day_after_previous_email' => $sendDayAfterPreviousEmail,
            'cumulative_send_day' => $cumulativeSendDay
        ];
        $response = $this->adminAutoFollowupRepository->addAutoFollowupByParam($data);
        if ($response) {
            return UtilityHelper::RETURN_STATUS_FORMATE(true, 'Followup Setting Added');
        }
        return UtilityHelper::RETURN_STATUS_FORMATE(false, 'Followup Setting Addition Failed! Please Try Again');
    }


    public function getFollowupById($followupId)
    {
        return $this->adminAutoFollowupRepository->getFollowupById($followupId);
    }

    public function updateFollowup($request)
    {
        $followupId = $request->get('followupId');
        if ($request->get('subject') == null || $request->get('subject') == '') {
            return UtilityHelper::RETURN_STATUS_FORMATE(false, 'Please enter email subject');
        }
        if ($request->get('body') == null || $request->get('body') == '') {
            return UtilityHelper::RETURN_STATUS_FORMATE(false, 'Please enter email body');
        }
        $sendDayAfterPreviousEmail = 0;
        if ($request->get('day') != null && $request->get('day') != '') {
            $sendDayAfterPreviousEmail = $request->get('day');
        }
        $where = [
            'id' => $followupId
        ];
        $data = [
            'email_subject' => $request->get('subject'),
            'email_body' => $request->get('body'),
            'send_day_after_previous_email' => $sendDayAfterPreviousEmail,
        ];
        $response = $this->adminAutoFollowupRepository->updateFollowupByParam($where, $data);
        if ($response) {
            $previousSendDay = $request->get('previousSendDay');
            $allNewerFollowups = $this->adminAutoFollowupRepository->getAllFollowupsNewerThanThis($followupId);
            if ($previousSendDay > $sendDayAfterPreviousEmail) {
                // Day decreased
                $decreasedDay = $previousSendDay - $sendDayAfterPreviousEmail;
                foreach ($allNewerFollowups as $followup) {
                    $where = [
                        'id' => $followup->id
                    ];
                    $data = [
                        'cumulative_send_day' => $followup->cumulative_send_day - $decreasedDay
                    ];
                    $this->adminAutoFollowupRepository->updateFollowupByParam($where, $data);
                }
            } else if ($previousSendDay < $sendDayAfterPreviousEmail) {
                //Day increased
                $increasedDay = $sendDayAfterPreviousEmail - $previousSendDay;
                foreach ($allNewerFollowups as $followup) {
                    $where = [
                        'id' => $followup->id
                    ];
                    $data = [
                        'cumulative_send_day' => $followup->cumulative_send_day + $increasedDay
                    ];
                    $this->adminAutoFollowupRepository->updateFollowupByParam($where, $data);
                }
            }
            return UtilityHelper::RETURN_STATUS_FORMATE(true, 'Followup Setting Updated Successfully');

        }
        return UtilityHelper::RETURN_STATUS_FORMATE(false, 'Followup Setting Updating Failed! Please Try Again');
    }

    public function deleteFollowup($request)
    {
        $followupId = $request->get('followupId');

        $followupDetails = $this->adminAutoFollowupRepository->getFollowupById($followupId);
        if ($followupDetails['send_day_after_previous_email'] > 0) {
            $allNewerFollowups = $this->adminAutoFollowupRepository->getAllFollowupsNewerThanThis($followupId);
            foreach ($allNewerFollowups as $followup) {
                $where = [
                    'id' => $followup->id
                ];
                $data = [
                    'cumulative_send_day' => $followup->cumulative_send_day - $followupDetails['send_day_after_previous_email']
                ];
                $this->adminAutoFollowupRepository->updateFollowupByParam($where, $data);
            }
        }
        $response = $this->adminAutoFollowupRepository->deleteFollowupById($followupId);
        if ($response) {
            return UtilityHelper::RETURN_STATUS_FORMATE(true, 'Followup Setting Deleted Successfully');
        }
        return UtilityHelper::RETURN_STATUS_FORMATE(true, 'Followup Setting Deletion Failed! Please Try Again');
    }
}