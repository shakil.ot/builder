<?php

namespace Modules\AdminAutoFollowup\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\AdminAutoFollowup\Contracts\Services\AdminAutoFollowupContact;
use Modules\AdminEmailTemplate\Contracts\Service\EmailTemplateContact;
use Modules\UserDashboard\Contracts\Service\AutoFollowupContact;

class AdminAutoFollowupController extends Controller
{
    private $autoFollowupService;
    private $emailTemplateService;

    public function __construct(AdminAutoFollowupContact $autoFollowupService,
                                EmailTemplateContact $emailTemplateService
    )
    {
        $this->autoFollowupService = $autoFollowupService;
        $this->emailTemplateService = $emailTemplateService;
    }

    public function index()
    {
        return view('adminautofollowup::index');
    }

    public function listAutoFollowup()
    {
        return $this->autoFollowupService->getAdminAutoFollowup();
    }

    public function addFollowup(Request $request)
    {
        return response()->json($this->autoFollowupService->addAutoFollowup($request));
    }

    public function addNewFollowupForm()
    {
        $emailTemplates = $this->emailTemplateService->getAllEmailTemplate();
        return response()->json([
            'html' => view('adminautofollowup::autofollowup.add-new-followup-form')->with([
                'emailTemplates' => $emailTemplates
            ])->render(),
            'status' => 'success'
        ]);
    }

    public function followupEditForm(Request $request)
    {
        $followupId = $request->get('followupId');
        $followup = $this->autoFollowupService->getFollowupById($followupId);
        $emailTemplates = $this->emailTemplateService->getAllEmailTemplate();
        return response()->json([
            'html' => view('adminautofollowup::autofollowup.edit-followup-form')->with([
                'followup' => $followup,
                'followupId' => $followupId,
                'emailTemplates' => $emailTemplates
            ])->render(),
            'status' => 'success'
        ]);
    }

    public function editFollowupFormSubmit(Request $request)
    {
        return response()->json($this->autoFollowupService->updateFollowup($request));
    }

    public function deleteFollowup(Request $request)
    {
        return response()->json($this->autoFollowupService->deleteFollowup($request));
    }


}
