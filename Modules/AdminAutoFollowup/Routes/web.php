<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('adminautofollowup')->middleware('admin')->group(function() {
    Route::get('/', 'AdminAutoFollowupController@index')->name('admin.autofollowup');
    Route::get('/add-new-followup-form', 'AdminAutoFollowupController@addNewFollowupForm')->name('admin-add-followup');
    Route::post('/edit-form', 'AdminAutoFollowupController@followupEditForm')->name('admin.followup-edit-form');
    Route::post('/edit/form-submit', 'AdminAutoFollowupController@editFollowupFormSubmit')->name('admin.edit-followup-form-submit');
    Route::post('/', 'AdminAutoFollowupController@addFollowup')->name('admin.followup-form-submit');
    Route::get('/list', 'AdminAutoFollowupController@listAutoFollowup')->name('admin-auto-followup-list');
    Route::post('/delete', 'AdminAutoFollowupController@deleteFollowup')->name('admin.followup-delete');
});
