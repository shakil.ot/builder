<?php

namespace Modules\AdminAutoFollowup\Entities;

use Illuminate\Database\Eloquent\Model;

class AdminAutoFollowup extends Model
{
    protected $fillable =
        [
          'email_subject',
          'email_body',
          'send_day_after_previous_email',
          'cumulative_send_day',
        ];
}