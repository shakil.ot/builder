<?php

namespace Modules\UserInvoice\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Package\Contracts\Service\PaymentTransactionContact;
use Modules\UserInvoice\Contracts\Services\InvoiceContact;

class UserInvoiceController extends Controller
{
    private $paymentTransactionService;

    public function __construct(PaymentTransactionContact $paymentTransactionService
    )
    {
        $this->paymentTransactionService = $paymentTransactionService;
    }

    public function index()
    {
        return view('userinvoice::index');
    }

    public function listInvoice()
    {
        return $this->paymentTransactionService->listTransactions(auth()->id());
    }

    public function invoiceDetailsById($id)
    {
        $invoice = $this->paymentTransactionService->getTransactionDetailsById($id);
        $user = Auth::user();
        return view('userinvoice::invoice-details', compact('invoice', 'user'));

    }
}
