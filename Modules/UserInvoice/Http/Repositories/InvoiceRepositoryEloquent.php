<?php

namespace Modules\UserInvoice\Http\Repositories;

use App\Repositories\BaseRepository\BaseRepository;
use Modules\UserInvoice\Entities\Invoice;
use Modules\UserInvoice\Contracts\Repositories\InvoiceRepository;

class InvoiceRepositoryEloquent extends BaseRepository implements InvoiceRepository
{
    protected function model()
    {
        return new Invoice();
    }

    public function getInvoiceDetails($userId)
    {
        return $this->model()::where('user_id', $userId)
            ->orderBy('created_at', 'desc')->get();
    }

    public function getInvoiceDetailsByid($invoiceId)
    {
        return $this->model()::where('id', $invoiceId)->first();
    }
}