<?php

namespace Modules\UserInvoice\Http\Services;

use Modules\UserInvoice\Contracts\Repositories\InvoiceRepository;
use Modules\UserInvoice\Contracts\Services\InvoiceContact;
use Yajra\DataTables\DataTables;

class InvoiceService implements InvoiceContact
{
    private $invoiceRepository;

    public function __construct(InvoiceRepository $invoiceRepository)
    {
        $this->invoiceRepository = $invoiceRepository;
    }


    public function getInvoiceDetails($userId)
    {
        $invoices = $this->invoiceRepository->getInvoiceDetails($userId);
        return DataTables::of($invoices)

            ->addColumn('data_plan', function ($invoices){
                if ($invoices->payment_type == null ) {
                    return '<span class="label label-lg label-light-danger  label-inline">N/A</span>';
                }

                elseif ($invoices->payment_type == \Modules\UserInvoice\Entities\Invoice::SUBSCRIPTION ) {
                    return '<span class="label label-lg label-success label-inline" style="background-color: #023c64; color: #ffffff;"> Invoice #' .$invoices->invoice_id.' (Subscription)</span>';
                }

                elseif ($invoices->payment_type == \Modules\UserInvoice\Entities\Invoice::RECURRING ) {
                    return '<span class="label label-lg label-success label-inline" style="background-color: #023c64; color: #ffffff;"> Invoice #' .$invoices->invoice_id.' (Recurrent)</span>';
                }
            })
            ->addColumn('data_created_at', function ($invoices){
                if ($invoices->created_at == null) {
                    return '<span class="label label-lg label-light-danger  label-inline">N/A</span>';
                } else {
                    return '<span class="label label-lg label-light-success label-inline" style="background-color: #023c64; color: #ffffff;">' . $invoices->created_at->format('Y-m-d') . '</span>';
                }
            })
            ->addColumn('data_amount', function ($invoices){
                if ($invoices->amount == null)
                {
                    return '<span class="label label-lg label-light-danger  label-inline">N/A</span>';
                }
                else
                {
                    return '<span class="label label-lg label-light-danger  label-inline" style="background-color: #023c64; color: #ffffff;">'.$invoices->amount .'</span>';
                }
            })

            ->addColumn('action', function ($invoices){
                $btn ='<div>'.
                        '<a href="/invoices/details/'. $invoices->id .'" target="_blank" class=""
                            data-value=" data-id=""><i class="fas fa-file-download"></i> Download
                            </a>
                       </div>'
                ;
                return $btn;
            })
            ->rawColumns(['data_plan', 'data_created_at', 'data_amount', 'action'])
            ->make(true);
    }

    public function getInvoiceDetailsById($invoiceId)
    {
        return $this->invoiceRepository->getInvoiceDetailsByid($invoiceId);
    }
}