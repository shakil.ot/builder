<?php

namespace Modules\UserInvoice\Entities;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable =
        [
          'user_id',
          'invoice_id',
          'description',
          'transaction_id',
          'amount',
          'payment_type'
        ];

    const SUBSCRIPTION = 1;
    const RECURRING = 2;
}