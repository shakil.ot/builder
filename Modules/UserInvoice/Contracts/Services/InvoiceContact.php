<?php

namespace Modules\UserInvoice\Contracts\Services;

interface InvoiceContact
{
    public function getInvoiceDetails($userId);

    public function getInvoiceDetailsById($invoiceId);
}