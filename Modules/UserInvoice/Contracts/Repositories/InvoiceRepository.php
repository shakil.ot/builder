<?php

namespace Modules\UserInvoice\Contracts\Repositories;

interface InvoiceRepository
{
    public function getInvoiceDetails($userId);

    public function getInvoiceDetailsByid($invoiceId);
}