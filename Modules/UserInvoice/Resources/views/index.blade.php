@extends('user.layout.app', ['menu' => 'dashboard'])
@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="d-flex flex-column-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-custom card-stretch gutter-b">
                            <!--begin::Header-->

                            <!--end::Header-->
                            <!--begin::Body-->
                            <div class="card-body pt-0 pb-3" id="contactsTable">
                                <!--begin::Subheader-->
                                <div class="py-2 py-lg-4 subheader-solid" id="kt_subheader" style=" color: #6c7293; margin-left: -20px; margin-right: -30px; height: 54px;top: 65px;left: 0;right: 0; background-color: #ffffff;border-top: 1px solid #ECF0F3;">
                                    <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                                        <!--begin::Info-->
                                        <div class="d-flex align-items-center flex-wrap mr-1">
                                            <!--begin::Page Heading-->
                                            <div class="d-flex align-items-baseline mr-5">
                                                <!--begin::Page Title-->
                                                <i class="icon-xl fas fa-file-invoice-dollar" style="color: #023B64;"></i>
                                                <h3 class="text-dark font-weight-bold my-2 ml-2 mr-5"> Invoices</h3>
                                                <!--end::Page Title-->
                                            </div>
                                            <hr>
                                            <!--end::Page Heading-->
                                        </div>
                                        <!--end::Info-->
                                        <!--begin::Toolbar-->
                                        <div class="d-flex align-items-center">
                                            <!--begin::Actions-->
                                            <ul class="nav nav-pills nav-pills--brand m-nav-pills--align-right m-nav-pills--btn-pill m-nav-pills--btn-sm" role="tablist">
                                                <li class="nav-item m-tabs__item">  </li>
                                            </ul>
                                            <!--end::Actions-->
                                        </div>
                                        <!--end::Toolbar-->
                                    </div>
                                </div>
                                <!--end::Subheader-->
                                <div style="margin:0 auto">
                                    <div class="pane-section-content open">
                                        <div class="m-subheader">
                                            <input type="hidden" id="contactId" value="{{ Auth::user()->id }}">
                                        </div>

                                        <!-- END: Subheader -->
                                        <div class="m-content">
                                            <div class="row">
                                                <div class="col-xl-12 margin-sm-dev" style="margin-top: 20px;">
                                                    <div class="m-portlet alterC">
                                                        <div class="m-portlet__body list_view_map_view">
                                                            <div class="tab-content quick-sends">
                                                                <div class="tab-pane active show" id="" role="tabpanel">
                                                                    <div class="table-design-border-wrapper">
                                                                        <div class="m-portlet__body table-responsive table-design-border mcOverflowVisible">
                                                                            <table id="invoice-table" class="mt-1 table table-hover contact-dataTable">
                                                                                <colgroup>
                                                                                    <col span="1" style="width: 15%;">
                                                                                    <col span="1" style="width: 15%;">
                                                                                    <col span="1" style="width: 15%;">
                                                                                    <col span="1" style="width: 15%;">
                                                                                    <col span="1" style="width: 15%;">
                                                                                    <col span="1" style="width: 25%;">
                                                                                </colgroup>
                                                                                <thead>
                                                                                <tr>
                                                                                    <th><span>Invoice ID</span></th>
                                                                                    <th><span>Date</span></th>
                                                                                    <th><span>Amount</span></th>
{{--                                                                                    <th><span>Action</span></th>--}}
{{--                                                                                    <th><span>Status</span></th>--}}
                                                                                    <th><span>Action</span></th>
                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!--end::Body-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('script')
    <script src="{{mix('/js/user-invoice.js')}}"></script>
@endsection

