@extends('user.layout.app', ['menu' => 'user-invoice'])
@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Card-->
                <div class="d-flex flex-row" data-sticky-container="">
                    <!--begin::Content-->
                    <div class="flex-row-fluid ml-lg-8">
                        <!--begin::Row-->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card card-custom">
                                    <div class="card-body" id="card-body">
                                        <div class="tab-content mt-5" id="myTabContent">
                                            <div class="tab-pane fade show active" id="personal_info" role="tabpanel" aria-labelledby="kt_tab_pane_2">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="m-portlet">
                                                                <div class="m-portlet__body m-portlet__body--no-padding">
                                                                    <div class="m-invoice__logo">
                                                                        <Button id="pdf_creator" class="btn btn-outline-info btn-sm float-right"><i class="fa fa-download"></i> Download</Button>
                                                                    </div>
                                                                    <div class="m-invoice-2 ml-5 mr-5 mt-5" id="main_div">
                                                                        <div class="m-invoice__wrapper">
                                                                            <div class="m-invoice__head">


                                                                                <div class="m-invoice__container m-invoice__container--centered">

                                                                                    <div class="m-invoice__logo">
                                                                                        <a href="{{ route('user.invoices') }}"><h1> INVOICE  </h1></a>
                                                                                        <a href="{{ route('user.invoices') }}">
                                                                                            <img alt="" src="{{ asset(env('MAIN_LOGO')) }} " style="width: 160px"/>
                                                                                            {{--                                            <img src="../../assets/app/media/img//logos/logo_client_color.png">--}}
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <div class="col-md-6">
                                                                                             <span class="">
{{--                                                                                                 <span class="m-invoice__subtitle">--}}
{{--                                                                                                     <h3>From:</h3>--}}
{{--                                                                                                     NewYork, USA--}}
{{--                                                                                                 </span>--}}
                                                                                            </span>
                                                                                            <br>
                                                                                            <div class="m-invoice__item">
                                                                                                <span class="m-invoice__subtitle">
                                                                                                    DATE :
                                                                                                </span>
                                                                                                <span class="m-invoice__text">
                                                                                                    {{ isset($invoice->created_at) ? $invoice->created_at->format('Y-m-d') : 'N/A'    }}
                                                                                                </span>
                                                                                                <br>
                                                                                                <span class="m-invoice__subtitle">
                                                                                                    INVOICE ID.
                                                                                                </span>
                                                                                                <span class="m-invoice__text">    {{ "#InvoiceOLKFN".$invoice->id }} 	</span>
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="col-md-6">
{{--                                                                                            <span class="">--}}
{{--                                                                                                 <span class="m-invoice__subtitle">--}}
{{--                                                                                                     <h3>To</h3>--}}
{{--                                                                                                     Somewhere, Europe--}}
{{--                                                                                                 </span>--}}
{{--                                                                                            </span>--}}
{{--                                                                                            <span class="m-invoice__desc">--}}
{{--                                                                                                <h3>To</h3>--}}
{{--                                                                                                John Smith,--}}
{{--                                                                                                Somewhere,--}}
{{--                                                                                                Europe--}}
{{--                                                                                                <span> {{ $user->first_name }} {{ $user->last_name }} 	</span>--}}
{{--                                                                                                <span>   {{ $user->email }}  </span>--}}
{{--                                                                                             </span>--}}
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="m-invoice__body m-invoice__body--centered mt-2">
                                                                                <div class="table-responsive report-transation-inv">
                                                                                    <table class="table">
                                                                                        <thead>
                                                                                        <tr>
                                                                                            <th> Created At</th>
                                                                                            <th> Transaction Id</th>
                                                                                            <th>Type</th>
                                                                                            <th style=" text-align: right; "> AMOUNT</th>
                                                                                        </tr>
                                                                                        </thead>
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td>  {{ $invoice->created_at }}  </td>
                                                                                            <td>  {{ trim($invoice->transaction_id) }}  </td>
                                                                                            <td>
                                                                                                @if($invoice->type_for == \Modules\Package\Entities\PaymentTransaction::PAYMENT_TRANSACTION_TYPE_FOR_PACKAGE_SUBSCRIPTION)
                                                                                                    Subscription
                                                                                                @elseif($invoice->type_for == \Modules\Package\Entities\PaymentTransaction::PAYMENT_TRANSACTION_TYPE_FOR_PACKAGE_RENEW)
                                                                                                    Recurring
                                                                                                @endif
                                                                                            </td>
                                                                                            <td style=" text-align: right; ">  $ {{ $invoice->price}}  </td>
                                                                                        <tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>

                                                                                <div class="m-invoice__table  m-invoice__table--centered table-responsive report-transation-inv">
                                                                                    <table class="table">
                                                                                        <thead>
                                                                                        <tr>
                                                                                            <th></th>
                                                                                            <th></th>
                                                                                            <th></th>
                                                                                            <th style=" text-align: right; "> TOTAL AMOUNT</th>
                                                                                        </tr>
                                                                                        </thead>
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td></td>
                                                                                            <td></td>
                                                                                            <td>

                                                                                            </td>
                                                                                            <td class="m--font-danger" style=" text-align: right; " >
                                                                                                $  {{ $invoice->price }}
                                                                                            </td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Row-->
                    </div>
                    <!--end::Content-->
                </div>
                <!--end::Card-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
@endsection

@section('script')

{{--    <script src="{{ URL::asset('/assets/global/html2pdf-master/dist/html2pdf.bundle.min.js') }}"></script>--}}

    <script src="{{ URL::asset('/assets/html2pdf/html2pdf.bundle.min.js') }}">
    </script>

    <script>
        let invoicePdfObj = {
            pdfName : 'invoice_{{$invoice->id .'_'. date('F') .'_'. date('d') .'_'. date('Y') .'_'. date('h: i:s')}}_sk.pdf',
        };
    </script>

    <script src="{{ mix('/js/invoice-pdf.js') }}"></script>






@endsection