$(function () {
    let _token = document.head.querySelector("[property=csrf-token]").content;
    let contactId = $('#contactId').val();
    $.extend(true, $.fn.dataTable.defaults, {
        "language": {
            "search": "",
            "searchPlaceholder": "Search",
            "sLengthMenu": "_MENU_ records",
            "paginate": {
                "first": "<<",
                "previous": '<span class="fa fa-chevron-left"></span>',
                "next": '<span class="fa fa-chevron-right"></span>',
                "last": ">>"
            },
        }
    });

    (function (global, $, _token) {

        let dataTable = $('#invoice-table').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            stateSave: false,
            "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
            pageLength: 10,

            ajax: {
                url: route('list-invoices'),
                data: function (data) {
                },
                type: 'get',
            },
            columns: [
                {data: 'data_plan', name: 'payment_type', "orderable": true, "searchable": true, width: "30%"},
                {data: 'data_created_at', name: 'created_at', "orderable": true, "searchable": false, width: "30%"},
                {data: 'data_amount', name: 'amount', "orderable": false, "searchable": false, width: "30%"},
                {data: 'action', name: 'action', "orderable": true, "searchable": false, width: "40%"}
            ]
        });
    })(window, jQuery, _token);
});