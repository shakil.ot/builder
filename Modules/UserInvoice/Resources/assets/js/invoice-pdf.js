
let InvoicePdf = (function (invoicePdfObj) {

    $('#pdf_creator').click(function () {
        let element = document.getElementById('main_div');
        let opt = {
            margin: 0,
            filename: invoicePdfObj.pdfName ,
            image : {
                type: 'jpeg',
                quality : 1
            },
            html2canvas: {
                dpi: 200, letterRendering:true
            },
            jsPDF: {
                unit: 'in',
                format:'letter',
                orientation:'portrait'
            }
        };

        $('#preloader').show();

        // html2pdf(element, opt );
        html2pdf().set(opt).from(element).save();
        setTimeout(function () {
            $('#preloader').hide();
        }, 5000);
    });

})(invoicePdfObj);