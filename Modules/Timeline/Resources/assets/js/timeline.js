var Timeline = (function (timelineVarObj) {

    var _token = $('input[name="_token"]').val();
    var BootstrapDatepicker = function () {
        var init = function (modal,clientId) {
            clientId = clientId || 0;
            modal.find('#m_datepicker_1_modal').datepicker({
                todayHighlight: true,
                autoClose: true,
                orientation: "bottom left",
                format: 'M d,yyyy',
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                },

            }).on("change", function() {
                // $('.modal-body').find('#m_calendar').fullCalendar('gotoDate', this.value);
            });
        }
        return {
            init: function(modal,clientId) {
                init(modal,clientId);
            }
        };
    }();

    (function (global, $, _token) {

        let contactId = timelineVarObj.contactId;

        let keyCode;
        $(document).on('keypress', '.single-field-input', function (event) {
            let keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode === 13) {
                $(this).blur();
                $(this).next().trigger('click');
            }
            event.stopPropagation();
        });

        $(document).on('click', '.check-icon', function () {
            $('#preloader').show();
            let that = $(this).prev('input');
            let name = $(that).attr('name');
            let value = $(that).val();
            global.mySiteAjax({
                url: route('contact-field-update'),
                type: 'post',
                data: {id: contactId, fieldName: name, value: value, _token: _token},
                loadSpinner: true,
                success: function (response) {
                    if (response.status === 'validation-error') {
                        toastr.error(response.html, "error");
                        $('#preloader').hide();
                    } else if (response.status === 'success') {
                        toastr.success(response.html, "success");
                        if (name === 'number') {
                            var numberGet = response.retValue;
                            $(that).val(numberGet.substr(1));
                            $('.timeline-top-phone-number').html(numberGet);
                        } else if (name === 'name') {
                            $('.contact-edit-min-name').html(response.retValue);
                        }
                        $('#preloader').hide();

                    } else if (response.status === 'error') {
                        $('#preloader').hide();
                        toastr.error(response.html, "error");
                    }
                }
                , error: function () {
                    $('#preloader').hide();
                }
            });
        });

        $('.member-title').click(function (e) {
            $(this).next().slideToggle();
            $(this).next().next().next().slideToggle();
        });

        $(document).on('click', '.delete-single-conversation', function () {
            let that = this;
            Swal.fire({
                title: 'Are you sure?',
                html: "<b class='text-danger'>Are you sure to delete this conversation?</b>",
                type: 'warning',
                confirmButtonText: 'Yes, delete it',
                confirmButtonClass: 'btn btn-danger',
                showCancelButton: true,
                cancelButtonText: 'No, keep it!',
                cancelButtonClass: 'btn btn-green'
            }).then(function(result) {
                if (result.value) {
                    let conversationId = $(that).attr('data-id');
                    global.mySiteAjax({
                        url: route('single-conversation-data-delete'),
                        type: 'post',
                        data: {conversation_id: conversationId, "_token": _token},
                        loadSpinner: true,

                        beforeSend: function () {
                            $('#preloader').show();
                        },
                        complete: function (xhr, stat) {
                            $('#preloader').hide();
                        },
                        success: function (response) {
                            if (response.status === 'success') {
                                toastr.success(response.html, "success");
                                location.reload();
                                $('#preloader').hide();
                            } else if (response.status === 'error') {
                                $('#preloader').hide();
                            }

                        }
                    });
                } else {
                    $('#preloader').hide();
                }
            });
        });
        $(document).on('click', '.delete-followup', function () {
            let that = this;
            Swal.fire({
                title: 'Are you sure to delete?',
                html: "<b class='text-success'>Next Followup will be sent if available</b>",
                type: 'warning',
                confirmButtonText: 'Yes, delete it',
                confirmButtonClass: 'btn btn-danger',
                showCancelButton: true,
                cancelButtonText: 'No, keep it!',
                cancelButtonClass: 'btn btn-green'
            }).then(function(result) {
                if (result.value) {
                    let autoFollowupId = $(that).attr('data-id');
                    let autoFollowupQueueId = $(that).attr('data-queue-id');
                    let day = $(that).attr('data-day');
                    let contactId = $(that).attr('data-contact-id');
                    global.mySiteAjax({
                        url: route('next-followup-delete'),
                        type: 'post',
                        data: {autoFollowupId: autoFollowupId,
                            autoFollowupQueueId:autoFollowupQueueId,
                            day:day,
                            contactId:contactId,
                            "_token": _token},
                        loadSpinner: true,

                        beforeSend: function () {
                            $('#preloader').show();
                        },
                        complete: function (xhr, stat) {
                            $('#preloader').hide();
                        },
                        success: function (response) {
                            if (response.status === 'success') {
                                toastr.success(response.html, "success");
                                location.reload();
                                $('#preloader').hide();
                            } else if (response.status === 'error') {
                                $('#preloader').hide();
                            }

                        }
                    });
                } else {
                    $('#preloader').hide();
                }
            });
        });

        $(document).on('click', '.block-contact', function () {
            let that = this;
            Swal.fire({
                title: 'Are you sure?',
                html: "<b class='text-danger'>Are you sure to block this contact?</b>",
                type: 'warning',
                confirmButtonText: 'Yes, block',
                confirmButtonClass: 'btn btn-danger',
                showCancelButton: true,
                cancelButtonText: 'No, keep it!',
                cancelButtonClass: 'btn btn-green'
            }).then(function(result) {
                if (result.value) {
                    let contactId = $(that).attr('data-id');
                    global.mySiteAjax({
                        url: route('block-contact-by-id'),
                        type: 'post',
                        data: {contactId: contactId, "_token": _token},
                        loadSpinner: true,

                        beforeSend: function () {
                            $('#preloader').show();
                        },
                        complete: function (xhr, stat) {
                            $('#preloader').hide();
                        },
                        success: function (response) {
                            if (response.status === 'success') {
                                toastr.success(response.html, "success");
                                location.reload();
                                $('#preloader').hide();
                            } else if (response.status === 'error') {
                                $('#preloader').hide();
                            }
                        }
                    });
                } else {
                    $('#preloader').hide();
                }
            });
        });

        $(document).on('click', '.activate-contact', function () {
            let that = this;
            Swal.fire({
                title: 'Are you sure?',
                html: "<b class='text-success'>Are you sure to activate this contact?</b>",
                type: 'warning',
                confirmButtonText: 'Yes, activate',
                confirmButtonClass: 'btn btn-success',
                showCancelButton: true,
                cancelButtonText: 'No, keep it!',
                cancelButtonClass: 'btn btn-danger'
            }).then(function(result) {
                if (result.value) {
                    let contactId = $(that).attr('data-id');
                    global.mySiteAjax({
                        url: route('activate-contact-by-id'),
                        type: 'post',
                        data: {contactId: contactId, "_token": _token},
                        loadSpinner: true,

                        beforeSend: function () {
                            $('#preloader').show();
                        },
                        complete: function (xhr, stat) {
                            $('#preloader').hide();
                        },
                        success: function (response) {
                            if (response.status === 'success') {
                                toastr.success(response.html, "success");
                                location.reload();
                                $('#preloader').hide();
                            } else if (response.status === 'error') {
                                $('#preloader').hide();
                            }
                        }
                    });
                } else {
                    $('#preloader').hide();
                }
            });
        });

        $(document).on('click','.view-email',function (){
           let broadcastLogId = $(this).attr('data-id');
           viewEmail(broadcastLogId);
        });
        $(document).on('click','.view-followup-email',function (){
            let autoFollowupId = $(this).attr('data-id');
            viewFollowupEmail(autoFollowupId);
        });
        function viewFollowupEmail(autoFollowupId)
        {
            $('#preloader').show();
            $('.preloader-text').html('Loading...');
            let detailPopUp = global.customPopup.show({
                header: 'Auto Followup Email',
                message: '',
                dialogSize: 'lg editContactModalAlterC',
                dialogClass: 'prevent-to-show'
            });

            detailPopUp.on('shown.bs.modal', function () {
                let modal = $(this);
                global.mySiteAjax({
                    url: route('email-view-form-autofollowup-id'),
                    type: 'post',
                    data: {'id': autoFollowupId, "_token": _token},
                    loadSpinner: false,
                    success: function (response) {
                        window.nbUtility.ajaxErrorHandling(response, function () {
                            modal.find('.modal-body').html(response.html);
                            $('#preloader').hide();
                            $("#custom-modal").css('overflow-y', 'auto');
                            $('#viewEmailBody').summernote({
                                height: 300,
                            });
                        });
                    }
                });
                modal.removeClass('prevent-to-show');
                $('#preloader').hide();
                global.spinnerDialog.hide();
            });

            detailPopUp.on('hidden.bs.modal', function () {
                $(this).remove();
                $('#preloader').hide();
            });
        }
        function viewEmail(broadcastLogId)
        {
            $('#preloader').show();
            $('.preloader-text').html('Loading...');
            let detailPopUp = global.customPopup.show({
                header: 'Sent Email',
                message: '',
                dialogSize: 'lg editContactModalAlterC',
                dialogClass: 'prevent-to-show'
            });

            detailPopUp.on('shown.bs.modal', function () {
                let modal = $(this);
                global.mySiteAjax({
                    url: route('email-view-form-broadcastlog-id'),
                    type: 'post',
                    data: {'id': broadcastLogId, "_token": _token},
                    loadSpinner: false,
                    success: function (response) {
                        window.nbUtility.ajaxErrorHandling(response, function () {
                            modal.find('.modal-body').html(response.html);
                            $('#preloader').hide();
                            $("#custom-modal").css('overflow-y', 'auto');
                            $('#viewEmailBody').summernote({
                                height: 300,
                            });
                        });
                    }
                });
                modal.removeClass('prevent-to-show');
                $('#preloader').hide();
                global.spinnerDialog.hide();
            });

            detailPopUp.on('hidden.bs.modal', function () {
                $(this).remove();
                $('#preloader').hide();
            });
        }

        function innerLoader(element, status) {
            if (status === 'show') {
                mApp.block(element, {
                    overlayColor: '#000000',
                    state: 'primary'
                });
            } else if (status === 'hide') {
                mApp.unblock(element);
            }
        }


    })(window, jQuery, _token);

})(timelineVarObj);

