<style>
    .m-list-timeline__items .m-list-timeline__item .m-list-timeline__time {
        display: table-cell;
        text-align: right;
        vertical-align: middle;
        width: 120px;
        padding: 0 7px 0 0;
        font-size: 0.85rem;
    }
    .block-contact{
        margin: 24px;
        border-radius: 8px;
    }
    .activate-contact{
        margin: 24px;
        background-color: #053a6f;
        color: white;
        border-radius: 8px;
    }
</style>
<div class="conversation_inbox_details_top conversation_inbox_right" id="contact-details-panel-top">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h4 class="m-portlet__head-text">
                        <span class="m-link m-link--brand m--font-bolder timeline-top-phone-number">{{ $contactInfo->email }}</span>
                    </h4>
                </div>
            </div>
        </div>
    <div class="conversation__item_header d-flex justify-content-start align-items-start">
        <div class="conversation__participent_name d-flex justify-content-between align-items-center">
            <div class="inbox_conversation_info_item test mb-0">
                <div class="inbox_single_item">
                <span class="test submit-field">
                    <input type="text" autocomplete="turn-off" name="name" class="form-control single-field-input" title="Full Name"
                           placeholder="Enter contact name" value="{{$contactInfo->first_name}} {{$contactInfo->last_name}}">
                    <a class="check-icon"><i class="la la-check"></i></a>
                </span>
                </div>
            </div>
        </div>
    </div>

    <div class="inbox_conversation_info mt-0">
        <div class="inbox_conversation_info_item test editByD">

            <div class="inbox_single_item inboxPhoneItem">
                <span><i class="la la-mobile"></i></span>
                <span class="test submit-field">
                    <input type="text" autocomplete="turn-off" name="number"
                           class="form-control single-field-input phone_usa" title="Number"
                           data-parsley-pattern="/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/"
                           placeholder="Please input contact number here" value="{{ substr($contactInfo->number,1) }}">
                    <a class="check-icon"><i class="la la-check"></i></a>
                </span>
            </div>

            <div class="inbox_single_item inboxEmailItem">
                <span><i class="la la-envelope"></i></span>
                <span class="test submit-field">
                    <input type="text" autocomplete="turn-off" name="email" class="form-control single-field-input" title="Email"
                           placeholder="No email added" value="{{$contactInfo->email}}">
                    <a class="check-icon"><i class="la la-check"></i></a>
                </span>
            </div>

            <div class="inbox_single_item singleItemThird">
                <span><i class="la la-map-marker"></i></span>
                <span class="test submit-field">
                    <input type="text" autocomplete="turn-off" name="city" class="form-control single-field-input" title="City"
                           placeholder="No city added" value="{{ $contactInfo->city }}">
                    <a class="check-icon"><i class="la la-check"></i></a>
                </span>
            </div>

            <div class="inbox_single_item singleItemThird">
                <span><i class="la la-map-marker"></i></span>
                <span class="test submit-field">
                    <input type="text" autocomplete="turn-off" name="zip" class="form-control single-field-input" title="Zip Code"
                           placeholder="No zip code added" value="{{ $contactInfo->zip }}">
                    <a class="check-icon"><i class="la la-check"></i></a>
                </span>
            </div>
        </div>

    </div>
    @if($contactInfo['status'] == \Modules\Contact\Entities\Contact::ACTIVE)
    <button class="btn btn-danger btn-sm block-contact" data-id="{{$contactInfo['id']}}">Block Contact</button>
        @endif

    @if($contactInfo['status'] == \Modules\Contact\Entities\Contact::BLOCKED)
        <button class="btn btn-sm activate-contact" data-id="{{$contactInfo['id']}}">Activate Contact</button>
    @endif
</div>

