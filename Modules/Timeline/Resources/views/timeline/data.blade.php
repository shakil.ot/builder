{{--    Auto Followups--}}
<div class="tab-pane active" id="m_tabs_1_1" role="tabpanel">
    <div class="m-scrollable m-scroller ps ps--active-y"  data-scrollable="true" data-height="380" data-mobile-height="450"
         style="overflow: hidden;">

        <div id="content">
            <ul class="timeline show-content-data">
                @php $autoFollowupFound = 0; @endphp
                    @foreach($autoFollowupData as $row)
                        @if($row['auto_followup_id'] != null)
                            @php $autoFollowupFound =1; @endphp
                        <li class="event sms" data-date="{!! ViewHelper::convertTimeFromYear($row->send_date) !!}">
                            <span class="date">{!! ViewHelper::convertTimeFrom12($row->send_date) !!}</span>
                            <div class="member-infos">
                                <h1 class='member-title'>{{$row->subject}}
                                    <span class="receive"><i title="Received" class="fa fa-mail-reply"> </i></span>
                                    <small><small>{!! $row->send_date !!}</small></small>
                                    <span class="delete-single-conversation" data-id="{{ $row->id }}"><i class="cumicon la la-trash"></i></span>
                                </h1>

                                <div class="timeline-body entypo-location">
                                   <button class="btn btn-sm view-email" data-id="{{$row->id}}"> View Email</button>
                                </div>

                                <span class="mcsi mt-4">
                                        <i class="fa fa-envelope"></i>
                                        <div class="member-title">
                                            To: <strong>{{ $row->to_email  }}</strong>
                                        </div>
                                        <span class="call_type">
                                            Status: <strong class="email_status">{!! ViewHelper::getEmailStatus($row->email_status) !!}</strong>
                                        </span>
                                    </span>
                            </div>
                        </li>
                        @endif
                    @endforeach

                @if($autoFollowupFound == 0)
                    <div class="no-activity d-flex flex-column justify-content-center align-items-center">
                        <div class="no-activity-block">
                            <i class="la la-comment-o"></i>
                            <p>No Activity yet!</p>
                            <a href="{{ route('dashboard') }}" class="btn btn-quick-sends">Create Auto Followup <i class="la la-send-o"></i></a>
                        </div>
                    </div>
                    <script>
                        $("ul").removeClass("timeline");
                    </script>
                    @endif

            </ul>
            {!! $autoFollowupData->render() !!}
        </div>

    </div>


</div>



{{--Broadcasts--}}

<div class="tab-pane" id="m_tabs_1_2" role="tabpanel">
    <div class="m-scrollable m-scroller ps ps--active-y"  data-scrollable="true" data-height="380" data-mobile-height="450"
         style="overflow: hidden;">

        <div id="content">
            <ul class="timeline show-content-data">

                @php $broadcastDataFound = 0; @endphp
                    @foreach($broadcastData as $row)
                        @if($row['broadcast_id'] != null)
                            @php $broadcastDataFound = 1; @endphp
                        <li class="event sms" data-date="{!! ViewHelper::convertTimeFromYear($row->send_date) !!}">
                            <span class="date">{!! ViewHelper::convertTimeFrom12($row->send_date) !!}</span>
                            <div class="member-infos">
                                <h1 class='member-title'>{{$row->subject}}
                                    <span class="receive"><i title="Received" class="fa fa-mail-reply"> </i></span>
                                    <small><small>{!! $row->send_date !!}</small></small>
                                    <span class="delete-single-conversation" data-id="{{ $row->id }}"><i class="cumicon la la-trash"></i></span>
                                </h1>

                                <div class="timeline-body entypo-location">
                                    <button class="btn btn-sm view-email" data-id="{{$row->id}}"> View Email</button>
                                </div>

                                <span class="mcsi mt-4">
                                        <i class="fa fa-envelope"></i>
                                        <div class="member-title">
                                            To: <strong>{{ $row->to_email  }}</strong>
                                        </div>
                                        <span class="call_type">
                                            Status: <strong class="email_status">{!! ViewHelper::getEmailStatus($row->email_status) !!}</strong>
                                        </span>
                                    </span>
                            </div>
                        </li>

                        @endif
                    @endforeach

                @if($broadcastDataFound == 0)
                    <div class="no-activity d-flex flex-column justify-content-center align-items-center">
                        <div class="no-activity-block">
                            <i class="la la-comment-o"></i>
                            <p>No Activity yet!</p>
                            <a href="{{ route('broadcast.index') }}" class="btn btn-quick-sends">Create Campaign <i class="la la-send-o"></i></a>
                        </div>
                    </div>
                    @endif

            </ul>
            {!! $broadcastData->render() !!}
        </div>

    </div>


</div>

<div class="tab-pane" id="m_tabs_1_3" role="tabpanel">
    <div class="m-scrollable m-scroller ps ps--active-y"  data-scrollable="true" data-height="380" data-mobile-height="450"
         style="overflow: hidden;">

        <div id="content">
            <ul class="timeline show-content-data">
                @if(isset($nextSentEmail->autoFollowup['email_subject']))
                        <li class="event sms" data-date="{!! ViewHelper::convertTimeFromYear($nextSentEmail->schedule_time) !!}">
                            <span class="date">{!! ViewHelper::convertTimeFrom12($nextSentEmail->schedule_time) !!}</span>
                            <div class="member-infos">
                                <h1 class='member-title'>{{$nextSentEmail->autoFollowup['email_subject']}}
                                    <span class="receive"><i title="Followup" class="fa fa-mail-reply"> </i></span>
                                    <small><small>{!! $nextSentEmail->schedule_time !!}</small></small>
                                    <span class="delete-followup" data-id="{{ $nextSentEmail->id }}" data-queue-id="{{$nextSentEmail['id']}}" data-contact-id="{{$nextSentEmail['contact_id']}}" data-day="{{$nextSentEmail['day']}}"><i class="cumicon la la-trash"></i></span>
                                </h1>

                                <div class="timeline-body entypo-location">
                                    <button class="btn btn-sm view-followup-email" data-id="{{$nextSentEmail->autoFollowup['id']}}" > View Email</button>
                                </div>

                                <span class="mcsi mt-4">
                                        <i class="fa fa-envelope"></i>
                                        <div class="member-title">
                                            To: <strong>{{ $contactInfo['email']  }}</strong>
                                        </div>
                                    </span>
                            </div>
                        </li>
                @else
                    <div class="no-activity d-flex flex-column justify-content-center align-items-center">
                        <div class="no-activity-block">
                            <i class="la la-comment-o"></i>
                            <p>No Followup!</p>
                            <a href="{{ route('dashboard') }}" class="btn btn-quick-sends">Create Followup <i class="la la-send-o"></i></a>
                        </div>
                    </div>
                    @endif

            </ul>
            {!! $broadcastData->render() !!}
        </div>

    </div>


</div>



{{--Notes--}}

{{--<div class="tab-pane" id="m_tabs_1_3" role="tabpanel">--}}
{{--    <div class="m-scrollable m-scroller ps ps--active-y"  data-scrollable="true" data-height="380" data-mobile-height="450"--}}
{{--         style="overflow: hidden;">--}}
{{--        {!! $noteDetailsPage !!}--}}
{{--    </div>--}}
{{--</div>--}}
