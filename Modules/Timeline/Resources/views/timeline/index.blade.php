@push('css')

    <link rel="stylesheet" href="{{asset('css/timeline-contact-profile.css')}}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        .view-email,.view-followup-email {
            background-color: #053a6f;
            color: white!important;
            border-radius: 16px;
        }
        .email_status{
            color: #053a6f;
        }
        .nav-item a{
            cursor: pointer;
        }
        body,
        #m_ver_menu {
            height: auto !important;
            overflow: unset !important;
        }

        @media (min-width: 992px){
            body.header-fixed.subheader-fixed.subheader-enabled .wrapper {
                padding-top: 78px;
            }

        }

        .contactName{
            margin-top: 11px;
            margin-left: 14px;
        }

        .contactNumber{
            margin-left: 15px;
        }

        .statusBtn{
            margin-right: 22px;
        }
        #addNoteBtn{
            margin:5px 2px 5px 100px;
        }

        .call_type {
            margin-bottom: -14px;
        }
    </style>
@endpush

@extends('user.layout.app', ['menu' => 'timeline'])

@section('content')
    <div class="container">
    <div class="row cusbg">
        <div class="col-md-4 col_extra pr-0">
            {!! $editContactPage !!}
        </div>
        <div class="col-md-8 pl-0 col8">
            <div class="m-portlet__head">
                <div class="timeline_tabs">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active content_click"
                               data-contact-id="{{$contactInfo->id}}"
                               data-toggle="tab" href="" data-target="#m_tabs_1_1">Auto Followups</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link content_click" data-toggle="tab" data-target="#m_tabs_1_2"
                               data-contact-id="{{$contactInfo->id}}"> Campaigns
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link content_click" data-toggle="tab" data-target="#m_tabs_1_3"
                               data-contact-id="{{$contactInfo->id}}"> Next Auto Followup
                            </a>
                        </li>
{{--                        <li class="nav-item">--}}
{{--                            <a class="nav-link content_click"--}}
{{--                               data-contact-id="{{$contactInfo->id}}"--}}
{{--                               data-toggle="tab" href="" data-target="#m_tabs_1_3">Notes--}}
{{--                            </a>--}}
{{--                        </li>--}}
                    </ul>
                    <div class="tab-content">
                        @include('timeline::timeline.data')
                    </div>
                </div>

            </div>
        </div>
    </div>
    </div>
@endsection

@push('scripts')
{{--    <script src="{{ mix('js/note.js') }}"></script>--}}

    <script>
        let timelineVarObj = {
            contactId : {{ $contactInfo->id }}
        };
    </script>

    <script src="{{ mix('js/timeline.js') }}"></script>
@endpush
