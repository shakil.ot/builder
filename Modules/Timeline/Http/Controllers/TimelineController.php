<?php

namespace Modules\Timeline\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Broadcast\Contracts\Repositories\BroadcastLogRepository;
use Modules\Contact\Contracts\Services\ContactContact;
use Modules\UserDashboard\Contracts\Repositories\AutoFollowupQueueRepository;
use Modules\UserDashboard\Contracts\Service\AutoFollowupContact;
use Response;

class TimelineController extends Controller
{
    /**
     * @var BroadcastLogRepository
     */
    private $broadcastLogRepository;
    /**
     * @var ContactContact
     */
    private $contactService;
    /**
     * @var AutoFollowupQueueRepository
     */
    private $autoFollowupQueueRepository;
    /**
     * @var AutoFollowupContact
     */
    private $autoFollowupService;

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function __construct(BroadcastLogRepository $broadcastLogRepository,
                                ContactContact $contactService,
                                AutoFollowupQueueRepository $autoFollowupQueueRepository,
                                AutoFollowupContact $autoFollowupService)
    {
        $this->broadcastLogRepository = $broadcastLogRepository;
        $this->contactService = $contactService;
        $this->autoFollowupQueueRepository = $autoFollowupQueueRepository;
        $this->autoFollowupService = $autoFollowupService;
    }

    public function index($contactId)
    {
        $userId = auth()->id();

        $autoFollowupData = $this->broadcastLogRepository->getAutoFollowupDataByContactId($userId, $contactId, $paginate = true);
        $broadcastData = $this->broadcastLogRepository->getBroadcastDataByContactId($userId, $contactId, $paginate = true);
        $nextSentEmail = $this->autoFollowupQueueRepository->getFollowupByUserIdAndContactId($userId, $contactId);
        $contactInfo = $this->contactService->getContactById($contactId);
        if ($contactInfo['user_id'] != $userId) {
            return redirect()->route('contact-index');
        }
        $editContactPage = view('timeline::contact.contact-profile')->with([
            'contactInfo' => $contactInfo,
            'contact_user_info' => $contactInfo['users']
        ])->render();

        return view('timeline::timeline.index')->with([
            'autoFollowupData' => $autoFollowupData,
            'broadcastData' => $broadcastData,
            'editContactPage' => $editContactPage,
            'contactInfo' => $contactInfo,
            'nextSentEmail' => $nextSentEmail
        ]);
    }

    public function deleteSingleConversation(Request $request)
    {

        if (!$request->ajax()) {
            return redirect()->route('Dashboard');
        }

        $responseData = $this->broadcastLogRepository->deleteSingleConversationByConversationId($request->get('conversation_id'));

        return response()->json([
            'html' => $responseData['html'],
            'status' => $responseData['status']
        ]);
    }

    public function emailViewFormBroadcastLogId(Request $request)
    {
        $broadcastLogId = $request->get('id');
        $broadcastLogDetails = $this->broadcastLogRepository->getDetailsById($broadcastLogId, ['subject', 'message_body']);
        return Response::json([
            'html' => view('timeline::timeline.view-email')
                ->with([
                    'broadcastLogDetails' => $broadcastLogDetails
                ])->render(),
            'status' => 'success'
        ]);
    }

    public function emailViewFormAutoFollowupId(Request $request)
    {
        $autoFollowupId = $request->get('id');
        $autoFollowupDetails = $this->autoFollowupService->getFollowupById($autoFollowupId, ['email_subject', 'email_body']);
        return Response::json([
            'html' => view('timeline::timeline.view-followup-email')
                ->with([
                    'autoFollowupDetails' => $autoFollowupDetails
                ])->render(),
            'status' => 'success'
        ]);
    }

    public function nextFollowupDelete(Request $request)
    {
        $request['userId'] = auth()->id();
        return Response::json($this->autoFollowupService->nextFollowupDelete($request));
    }


}
