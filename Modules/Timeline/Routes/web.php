<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('timeline')->middleware('auth','checkUserJourneyMode')->group(function() {

    Route::get('/{id}', 'TimelineController@index')->name('timeline-index');
    Route::post('/get/data/content/type/wise', 'TimelineController@getDataContentTypeWise')->name('get-content-type-wise-data');
    Route::post('single/conversation/data/delete', 'TimelineController@deleteSingleConversation')->name('single-conversation-data-delete');
    Route::post('email-view-form-broadcastlog-id', 'TimelineController@emailViewFormBroadcastLogId')->name('email-view-form-broadcastlog-id');
    Route::post('email-view-form-autofollowup-id', 'TimelineController@emailViewFormAutoFollowupId')->name('email-view-form-autofollowup-id');
    Route::post('next-followup-delete', 'TimelineController@nextFollowupDelete')->name('next-followup-delete');

});
