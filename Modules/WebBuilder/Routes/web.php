<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('webbuilder')->middleware(['auth', 'web', 'userHasActivePackage'])->group(function() {
    Route::get('/v/{template}', 'WebBuilderController@index')->name('template-view');
    Route::get('/e/{template}', 'WebBuilderController@templateViewForEdit')->name('template-view-for-edit');
    Route::post('/update-page-content', 'WebBuilderController@updatePageContent')->name('update-page-content');
    Route::post('/update-image', 'WebBuilderController@updateImage')->name('update-image');
    Route::post('/default-template-set-in-web-builder', 'WebBuilderController@defaultTemplateSet')->name('default-template-set-in-web-builder');
    Route::post('/add-template-in-web-builder', 'WebBuilderController@addTemplate')->name('add-template-in-web-builder');

});

Route::prefix('webbuilder')->group(function() {
    Route::post('/action-url-for-subscription', 'WebBuilderController@actionUrlForSubscription')->name('action-url-for-subscription');
    Route::post('/action-url-for-download-instruction', 'WebBuilderController@actionUrlForDownloadInstruction')->name('action-url-for-download-instruction');
    Route::post('/action-url-for-offer', 'WebBuilderController@actionUrlForOffer')->name('action-url-for-offer');
    Route::post('/action-url-for-thank-you', 'WebBuilderController@actionUrlForThankYou')->name('action-url-for-thank-you');
    Route::post('/thank-you-page-add', 'WebBuilderController@thankYouPageAdd')->name('thank-you-page-add');
});


Route::get('/subscriber/{id}/v', 'WebBuilderController@subscriberSlug')->name('subscriber-slug');
Route::get('/download-instruction/{id}/v', 'WebBuilderController@downloadInstructionSlug')->name('subscriber-slug');
Route::get('/offer/{id}/v', 'WebBuilderController@offerSlug')->name('offer-slug');
Route::get('/thank-you/{id}/v', 'WebBuilderController@thankYouSlug')->name('thank-you-slug');