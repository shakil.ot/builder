<?php

namespace Modules\WebBuilder\Contracts\Services;

interface WebBuilderContact
{
    public function getTemplateData($templateType, $userId);

    public function pageBuilderUpdate($request);

    public function defaultTemplateSetById($id , $userId);

    public function addTemplate($request, $userId);

    public function getTemplateDataForView($templateType, $userId);

    public function actionUrlForSubscription($request);

    public function actionUrlForDownloadInstruction($request);

    public function getTemplateDataForPublicView($id, $type);

    public function thankYouPageAdd($request, $userId);
}