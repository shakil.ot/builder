<?php

namespace Modules\WebBuilder\Contracts\Repositories;

interface WebBuilderRepository
{
    public function checkHasDataByCondition($where);

    public function createWebBuilderTemplate($data);

    public function UpdateOrCreateWebBuilderTemplate($where , $data);

    public function getTemplateByCondition($where);


}
