<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dog Funnel </title>
    <style type="text/css">
        #outlook a {
            padding: 0;
        }

        body {
            font-family: 'Arial', 'Helvetica', sans-serif;
            background: white;
            width: 100% !important;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
            margin-top: 0px;
            margin-right: 0px;
            margin-bottom: 0px;
            margin-left: 0px;
            padding-top: 0px;
            padding-right: 0px;
            padding-bottom: 0px;
            padding-left: 0px;
        }

        .ExternalClass {
            width: 100%;
        }

        .ExternalClass * {
            line-height: 100%;
        }

        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
            line-height: 100%;
        }

        img {
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
        }

        a img {
            border: none;
        }

        .image-fix {
            display: block;
        }

        .unsub-text {
            mso-line-height-rule: exactly;
            font-family: 'Arial', 'Helvetica', sans-serif;
            font-size: 15px;
            line-height: 15px;
            font-weight: normal;
            text-decoration: none;
            color: #cbd0d6;
        }

        .unsub-text a {
            text-decoration: none;
            color: #cbd0d6;
        }

        .address-text {
            mso-line-height-rule: exactly;
            font-family: 'Arial', 'Helvetica', sans-serif;
            font-size: 15px;
            line-height: 15px;
            font-weight: normal;
            text-decoration: none;
            color: #cbd0d6;
        }

        .address-text a {
            text-decoration: none;
            color: #cbd0d6;
        }

        @media only screen and (max-width: 480px) {
            /*video blocks*/
            table[class="content-table"] {
                width: 320px !important;
            }

            table[class="mobile-frame"] {
                width: 280px !important;
            }

            table[class="video-table"] {
                width: 290px !important;
            }

            td[class="header-block-l"] {
                padding-top: 10px !important;
                padding-right: 0px !important;
                padding-bottom: 0px !important;
                padding-left: 0px !important;
                display: block !important;
                text-align: center !important;
                width: 320px !important;
            }

            td[class="header-block-r"] {
                padding-top: 0px !important;
                padding-right: 0px !important;
                padding-bottom: 20px !important;
                padding-left: 0px !important;
                display: block !important;
                width: 320px !important;
            }

            td[class="hbr"] {
                text-align: center !important;
            }

            td[class="top-video-image"] img {
                width: 290px !important;
            }

            td[class="three-col-padding"] {
                padding-top: 20px !important;
                padding-right: 0px !important;
                padding-bottom: 20px !important;
                padding-left: 0px !important;
            }

            td[class="three-col-headline"] {
                text-align: left !important;
            }

            td[class="three-col-description"] {
                text-align: left !important;
            }

            td[class="three-col-cta"] {
                text-align: left !important;
            }

            td[class="video-split-l"] {
                padding-top: 0px !important;
                padding-right: 0px !important;
                padding-bottom: 0px !important;
                padding-left: 0px !important;
                display: block !important;
            }

            td[class="video-split-l"] img {
                width: 275px !important;
            }

            td[class="video-split-m"] {
                border-top: 1px solid #cbd0d6;
                display: block !important;
                padding-top: 20px !important;
                padding-right: 0px !important;
                padding-bottom: 0px !important;
                padding-left: 0px !important;
            }

            td[class="video-split-m"] img {
                width: 275px !important;
            }

            td[class="video-split-r"] {
                display: block !important;
                padding-top: 20px !important;
                padding-right: 0px !important;
                padding-bottom: 0px !important;
                padding-left: 0px !important;
                border-top: 1px solid #cbd0d6;
            }

            td[class="video-split-r"] img {
                width: 275px !important;
            }

            table[class="two-col-width"] {
                width: 320px !important;
            }

            td[class="two-col-split-l"] {
                display: block !important;
                padding-top: 20px !important;
                padding-right: 0px !important;
                padding-bottom: 0px !important;
                padding-left: 0px !important;
            }

            td[class="two-col-split-r"] {
                border-top: 1px solid #cbd0d6;
                display: block !important;
                padding-top: 20px !important;
                padding-right: 0px !important;
                padding-bottom: 0px !important;
                padding-left: 0px !important;
            }

            td[class="footer-block-l"] {
                display: block !important;
                width: 320px !important;
            }

            td[class="footer-block-r"] {
                padding-top: 30px !important;
                padding-right: 0px !important;
                padding-bottom: 0px !important;
                padding-left: 0px !important;
                display: block !important;
                width: 320px !important;
            }

            td[class="company-name"] {
                text-align: center !important;
            }

            td[class="address"] {
                text-align: center !important;
            }

            td[class="edit"] {
                text-align: center !important;
            }

            td[class="unsub"] {
                text-align: center !important;
            }

            *[class="is-mobile"] {
                display: block !important;
                max-height: none !important;
            }

            *[class="is-desktop"] {
                display: none;
                max-height: 0px;
                overflow: hidden;
            }
        }
    </style>
</head>

<body style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background: white; font-family: 'Arial', 'Helvetica', sans-serif; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; margin-top: 0px; padding-bottom: 0px; padding-left: 0px; padding-right: 0px; padding-top: 0px; width: 100%;">
<table bgcolor="#ffffff" width="100%" cellspacing="0" cellpadding="0" border="0" class="table-body"
       style="line-height: 100%; margin: 0; padding: 0; width: 100%;">
    <tr>
        <td align="center" style="border-collapse: collapse;">
            <table width="600" cellspacing="0" cellpadding="0" border="0" align="center" class="content-table">
                <tr>
                    <td style="border-collapse: collapse;">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#e2e2e2">
                            <tr>
                                <td class="top-video-padding" align="center" valign="top"
                                    style="border-collapse: collapse; padding-bottom: 40px; padding-left: 0px; padding-right: 0px; padding-top: 40px;">
                                    <table width="460" border="0" cellspacing="0" cellpadding="0" class="video-table">
                                        <tr>
                                            <td align="center" valign="top" style="border-collapse: collapse;">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td align="center" valign="top" class="featured-text"
                                                            style="border-collapse: collapse; color: #fba92b; font-family: 'Arial', 'Helvetica', sans-serif; font-size: 12px; letter-spacing: 1px; font-weight: bold; line-height: 12px; mso-line-height-rule: exactly; text-decoration: none; display: none;">
                                                            <!--CHANGEME AND DELETE THIS COMMENT-->FEATURED
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" valign="top" class="featured-text-headline"
                                                            style="border-collapse: collapse; color: #595959; font-family: 'Arial', 'Helvetica', sans-serif; font-size: 20px; font-weight: bold; line-height: 20px; mso-line-height-rule: exactly; padding-bottom: 0px; padding-left: 0px; padding-right: 0px; padding-top: 10px; text-decoration: none;">
                                                            <!--CHANGEME AND DELETE THIS COMMENT--> {{ $subject  }}
                                                        </td>
                                                    </tr>
                                                </table>

                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td align="center" style="border-collapse: collapse;">
                                                            <table width="170" cellspacing="0" cellpadding="0"
                                                                   border="0">
                                                                <tr>
                                                                    <td align="center" width="170" height="48"
                                                                        bgcolor="#FBA92B" class="button-wrapper"
                                                                        style="-moz-border-radius: 7px; -webkit-border-radius: 7px; background-clip: padding-box; border-collapse: collapse; border-radius: 7px; color: white; display: block;">
                                                                        <a href="{{ $link }}"
                                                                           target="_blank" class="inner-button"
                                                                           style="color: black;
                                                                            display: inline-block;
                                                                            font-family: 'Arial', 'Helvetica',
                                                                             sans-serif; font-size: 17px; line-height: 50px; text-decoration: none; width: 100%;">
                                                                            <span style="color: white;">Download E-Book PDF</span>
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>