<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{!! !empty($title) ? $title : '' !!} </title>

    <link rel="shortcut icon" href="{{ asset(env('LOGO_FAV')) }}" />
    <link rel="stylesheet" href="{{asset('assets/preloader/preloader.css')}}">
    <link rel="stylesheet" href="{{asset('assets/global/plugins/toastr/toastr.min.css')}}">

    <style>
        body,
        html {
            height: 100%;
            margin: 0;
        }
        {!!  $cssData !!}
    </style>
</head>
<body>


<!--begin::Preloader-->
<div id="preloader">
    <div class="loading-wrapper">
        <div class="loading">
            <img class="logo-loading" src="{{ asset(env('LOGO_FAV')) }}" alt="logo">
            <span class="preloader-text">Loading...</span>
        </div>
    </div>
</div>
<!--begin::Preloader-->
{!! $htmlData !!}
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="{{asset('assets/preloader/preloader.js')}}"></script>
<script src="{{asset('assets/global/plugins/toastr/toastr.min.js')}}"></script>

<script>

    $("form").on("submit", function (event) {
        event.preventDefault();
        var data = $(this).serializeArray(); //all input variables
        data.push({
            name: 'user_id',
            value: '{{ $userId }}'
        });


        $('#preloader').show();
        $.ajax({
            url: $(this)[0].action,
            type: "POST",
            data: data,
            success: function (response) {
                if (response.status) {

                    $('#preloader').hide();
                    if (response.page != undefined) {
                        if (response.page == 'redirect') {
                            toastr.success(response.message);
                            setTimeout(function(){
                                window.location.href = response.url;
                            }, 2000);
                        }
                    }
                } else {
                    toastr.error(response.html);
                }

            }
        });
        $('#preloader').hide();

    });

</script>


</html>







