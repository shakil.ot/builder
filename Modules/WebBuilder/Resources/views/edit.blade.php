<!doctype html>

<html>
<head>
    <meta charset="utf-8">
    <title>{{ env('APP_NAME') }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ env('APP_NAME') }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content="Best Free Open Source Responsive Websites Builder" name="description">
    {{--    <link rel="stylesheet" href="https://grapesjs.com/stylesheets/toastr.min.css">--}}
    <link rel="stylesheet" href="https://grapesjs.com/stylesheets/grapes.min.css?v0.16.34">
    <link rel="stylesheet" href="https://grapesjs.com/stylesheets/grapesjs-preset-webpage.min.css">
    <link rel="stylesheet" href="https://grapesjs.com/stylesheets/tooltip.css">
    <link rel="stylesheet" href="https://grapesjs.com/stylesheets/grapesjs-plugin-filestack.css">
    <link rel="stylesheet" href="https://grapesjs.com/stylesheets/demos.css?v3">
    <link href="https://unpkg.com/grapick/dist/grapick.min.css" rel="stylesheet">

    <!-- <script src="//static.filestackapi.com/v3/filestack.js"></script> -->
    <!-- <script src="js/aviary.js"></script> old //feather.aviary.com/imaging/v3/editor.js -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- <script src="js/toastr.min.js"></script> -->

    <script src="https://grapesjs.com/js/grapes.min.js?v0.16.34"></script>
    <script src="https://grapesjs.com/js/grapesjs-preset-webpage.min.js?v0.1.11"></script>
    <script src="https://grapesjs.com/js/grapesjs-lory-slider.min.js?0.1.5"></script>
    <script src="https://grapesjs.com/js/grapesjs-tabs.min.js?0.1.1"></script>
    <script src="https://grapesjs.com/js/grapesjs-custom-code.min.js?0.1.2"></script>
    <script src="https://grapesjs.com/js/grapesjs-touch.min.js?0.1.1"></script>
    {{--    <script src="https://grapesjs.com/js/grapesjs-parser-postcss.min.js?0.1.1"></script>--}}
    <script src="https://grapesjs.com/js/grapesjs-tooltip.min.js?0.1.1"></script>
    <script src="https://grapesjs.com/js/grapesjs-tui-image-editor.min.js?0.1.2"></script>
    <script src="https://grapesjs.com/js/grapesjs-typed.min.js?1.0.5"></script>
    <script src="https://grapesjs.com/js/grapesjs-style-bg.min.js?1.0.1"></script>


    {{--my code--}}
    <script src="{{ asset('assets/global/plugins/toastr/toastr.min.js') }}"></script>
    <link href="{{ asset('assets/global/plugins/toastr/toastr.min.css') }}" rel="stylesheet">

    <script src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>

    <link rel="shortcut icon" href="{{ asset(env('LOGO_FAV')) }}" />
    <link rel="stylesheet" href="{{asset('assets/preloader/preloader.css')}}">

    <style>
        body,
        html {
            height: 100%;
            margin: 0;
        }

        h1 {
            color: #0a6aa1;
        }

    </style>

</head>
<body>

<!--begin::Preloader-->
<div id="preloader">
    <div class="loading-wrapper">
        <div class="loading">
            <img class="logo-loading" src="{{ asset(env('LOGO_FAV')) }}" alt="logo">
            <span class="preloader-text">Loading...</span>
        </div>
    </div>
</div>
<!--begin::Preloader-->

<div id="gjs" style="height:0px; overflow:hidden">{!! $htmlData !!}</div>


<script>

    window.setTimeout(function(){
        localStorage.clear();
        $("#preloader").css("display", "none");

    },3000)


    var form_action_value = '', form_action_name = '';

    if ("{{ $template }}" == '{{ \Modules\WebBuilder\Entities\WebBuilder::WEB_BUILDER_TYPE_SUBSCRIPTION }}') {
        form_action_value = '{{ route('action-url-for-subscription') }}';
        form_action_name = 'Subscription';
    } else if ("{{ $template }}" == '{{ \Modules\WebBuilder\Entities\WebBuilder::WEB_BUILDER_TYPE_DOWNLOAD_INSTRUCTION }}') {
        form_action_value = '{{ route('action-url-for-download-instruction') }}';
        form_action_name = 'Download Instruction';
    } else if ("{{ $template }}" == '{{ \Modules\WebBuilder\Entities\WebBuilder::WEB_BUILDER_TYPE_OFFER }}') {
        form_action_value = '{{ route('action-url-for-offer') }}';
        form_action_name = 'Offer';
    } else if ("{{ $template }}" == '{{ \Modules\WebBuilder\Entities\WebBuilder::WEB_BUILDER_TYPE_THANK_YOU }}') {
        form_action_value = '{{ route('action-url-for-thank-you') }}';
        form_action_name = 'Thank you';
    }


    var WebBuilderObj = {
        style: '{{ \Modules\WebBuilder\Entities\WebBuilder::DEFAULT_STYLE }}' + '{{ $cssData }}',
        template: "{{ $template }}",
        id: "{{ $templateId }}",
        form_action: [
            {value: form_action_value, name: form_action_name},
        ],
        image_upload_url : '{{ route('update-image') }}',
        update_page_content : '{{ route('update-page-content') }}',
        app_url : '{{ env('APP_URL')  }}',

    };



    console.log(WebBuilderObj)
</script>
<script src="{{ mix('/js/UserWebBuilder.js') }}"></script>
</body>
</html>







