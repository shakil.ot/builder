var WebBuilderManage = (function (WebBuilderObj) {
    $(function () {

        const _token = $('meta[name="csrf-token"]').attr('content');

        (function (global, $, _token) {

            console.log(WebBuilderObj);
            const LandingPage = {
                style: WebBuilderObj.style, // load style in builder page
            };

            // var editor = grapesjs.init({
            //     height: '100%',
            //     showOffsets: 1,
            //     noticeOnUnload: 0,
            //     storageManager: {
            //         autoload: false,
            //     },
            //     style: LandingPage.style,
            //     container: '#gjs',
            //     fromElement: true,
            //     assetManager:
            //         {
            //             // Upload endpoint, set `false` to disable upload, default `false`
            //             upload: WebBuilderObj.image_upload_url, // for upload image in server
            //
            //             // The name used in POST to pass uploaded files, default: `'files'`
            //             uploadName: 'files',
            //         },
            //
            //     plugins: ['gjs-preset-webpage', 'grapesjs-plugin-forms'],
            //     pluginsOpts:
            //         {
            //             'gjs-preset-webpage':
            //                 {}
            //             ,
            //             'formsOpts':
            //                 {}
            //             ,
            //         }
            // });


            var lp = './img/';
            var plp = '//placehold.it/350x250/';
            var images = [

            ];

            var editor  = grapesjs.init({
                avoidInlineStyle: 1,
                height: '100%',
                container : '#gjs',
                style: LandingPage.style,
                fromElement: 1,
                showOffsets: 1,
                assetManager: {
                    embedAsBase64: 1,
                    assets: images
                },
                selectorManager: { componentFirst: true },
                styleManager: { clearProperties: 1 },
                plugins: [
                    'grapesjs-lory-slider',
                    'grapesjs-tabs',
                    'grapesjs-custom-code',
                    'grapesjs-touch',
                    'grapesjs-parser-postcss',
                    'grapesjs-tooltip',
                    'grapesjs-tui-image-editor',
                    'grapesjs-typed',
                    'grapesjs-style-bg',
                    'gjs-preset-webpage',
                ],
                pluginsOpts: {
                    'grapesjs-lory-slider': {
                        sliderBlock: {
                            category: 'Extra'
                        }
                    },
                    'grapesjs-tabs': {
                        tabsBlock: {
                            category: 'Extra'
                        }
                    },
                    'grapesjs-typed': {
                        block: {
                            category: 'Extra',
                            content: {
                                type: 'typed',
                                'type-speed': 40,
                                strings: [
                                    'Text row one',
                                    'Text row two',
                                    'Text row three',
                                ],
                            }
                        }
                    },
                    'gjs-preset-webpage': {
                        modalImportTitle: 'Import Template',
                        modalImportLabel: '<div style="margin-bottom: 10px; font-size: 13px;">Paste here your HTML/CSS and click Import</div>',
                        modalImportContent: function(editor) {
                            return editor.getHtml() + '<style>'+editor.getCss()+'</style>'
                        },
                        filestackOpts: null, //{ key: 'AYmqZc2e8RLGLE7TGkX3Hz' },
                        aviaryOpts: false,
                        blocksBasicOpts: { flexGrid: 1 },
                        customStyleManager: [{
                            name: 'General',
                            buildProps: ['float', 'display', 'position', 'top', 'right', 'left', 'bottom'],
                            properties:[{
                                name: 'Alignment',
                                property: 'float',
                                type: 'radio',
                                defaults: 'none',
                                list: [
                                    { value: 'none', className: 'fa fa-times'},
                                    { value: 'left', className: 'fa fa-align-left'},
                                    { value: 'right', className: 'fa fa-align-right'}
                                ],
                            },
                                { property: 'position', type: 'select'}
                            ],
                        },{
                            name: 'Dimension',
                            open: false,
                            buildProps: ['width', 'flex-width', 'height', 'max-width', 'min-height', 'margin', 'padding'],
                            properties: [{
                                id: 'flex-width',
                                type: 'integer',
                                name: 'Width',
                                units: ['px', '%'],
                                property: 'flex-basis',
                                toRequire: 1,
                            },{
                                property: 'margin',
                                properties:[
                                    { name: 'Top', property: 'margin-top'},
                                    { name: 'Right', property: 'margin-right'},
                                    { name: 'Bottom', property: 'margin-bottom'},
                                    { name: 'Left', property: 'margin-left'}
                                ],
                            },{
                                property  : 'padding',
                                properties:[
                                    { name: 'Top', property: 'padding-top'},
                                    { name: 'Right', property: 'padding-right'},
                                    { name: 'Bottom', property: 'padding-bottom'},
                                    { name: 'Left', property: 'padding-left'}
                                ],
                            }],
                        },{
                            name: 'Typography',
                            open: false,
                            buildProps: ['font-family', 'font-size', 'font-weight', 'letter-spacing', 'color', 'line-height', 'text-align', 'text-decoration', 'text-shadow'],
                            properties:[
                                { name: 'Font', property: 'font-family'},
                                { name: 'Weight', property: 'font-weight'},
                                { name:  'Font color', property: 'color'},
                                {
                                    property: 'text-align',
                                    type: 'radio',
                                    defaults: 'left',
                                    list: [
                                        { value : 'left',  name : 'Left',    className: 'fa fa-align-left'},
                                        { value : 'center',  name : 'Center',  className: 'fa fa-align-center' },
                                        { value : 'right',   name : 'Right',   className: 'fa fa-align-right'},
                                        { value : 'justify', name : 'Justify',   className: 'fa fa-align-justify'}
                                    ],
                                },{
                                    property: 'text-decoration',
                                    type: 'radio',
                                    defaults: 'none',
                                    list: [
                                        { value: 'none', name: 'None', className: 'fa fa-times'},
                                        { value: 'underline', name: 'underline', className: 'fa fa-underline' },
                                        { value: 'line-through', name: 'Line-through', className: 'fa fa-strikethrough'}
                                    ],
                                },{
                                    property: 'text-shadow',
                                    properties: [
                                        { name: 'X position', property: 'text-shadow-h'},
                                        { name: 'Y position', property: 'text-shadow-v'},
                                        { name: 'Blur', property: 'text-shadow-blur'},
                                        { name: 'Color', property: 'text-shadow-color'}
                                    ],
                                }],
                        },{
                            name: 'Decorations',
                            open: false,
                            buildProps: ['opacity', 'border-radius', 'border', 'box-shadow', 'background-bg'],
                            properties: [{
                                type: 'slider',
                                property: 'opacity',
                                defaults: 1,
                                step: 0.01,
                                max: 1,
                                min:0,
                            },{
                                property: 'border-radius',
                                properties  : [
                                    { name: 'Top', property: 'border-top-left-radius'},
                                    { name: 'Right', property: 'border-top-right-radius'},
                                    { name: 'Bottom', property: 'border-bottom-left-radius'},
                                    { name: 'Left', property: 'border-bottom-right-radius'}
                                ],
                            },{
                                property: 'box-shadow',
                                properties: [
                                    { name: 'X position', property: 'box-shadow-h'},
                                    { name: 'Y position', property: 'box-shadow-v'},
                                    { name: 'Blur', property: 'box-shadow-blur'},
                                    { name: 'Spread', property: 'box-shadow-spread'},
                                    { name: 'Color', property: 'box-shadow-color'},
                                    { name: 'Shadow type', property: 'box-shadow-type'}
                                ],
                            },{
                                id: 'background-bg',
                                property: 'background',
                                type: 'bg',
                            },],
                        },{
                            name: 'Extra',
                            open: false,
                            buildProps: ['transition', 'perspective', 'transform'],
                            properties: [{
                                property: 'transition',
                                properties:[
                                    { name: 'Property', property: 'transition-property'},
                                    { name: 'Duration', property: 'transition-duration'},
                                    { name: 'Easing', property: 'transition-timing-function'}
                                ],
                            },{
                                property: 'transform',
                                properties:[
                                    { name: 'Rotate X', property: 'transform-rotate-x'},
                                    { name: 'Rotate Y', property: 'transform-rotate-y'},
                                    { name: 'Rotate Z', property: 'transform-rotate-z'},
                                    { name: 'Scale X', property: 'transform-scale-x'},
                                    { name: 'Scale Y', property: 'transform-scale-y'},
                                    { name: 'Scale Z', property: 'transform-scale-z'}
                                ],
                            }]
                        },{
                            name: 'Flex',
                            open: false,
                            properties: [{
                                name: 'Flex Container',
                                property: 'display',
                                type: 'select',
                                defaults: 'block',
                                list: [
                                    { value: 'block', name: 'Disable'},
                                    { value: 'flex', name: 'Enable'}
                                ],
                            },{
                                name: 'Flex Parent',
                                property: 'label-parent-flex',
                                type: 'integer',
                            },{
                                name      : 'Direction',
                                property  : 'flex-direction',
                                type    : 'radio',
                                defaults  : 'row',
                                list    : [{
                                    value   : 'row',
                                    name    : 'Row',
                                    className : 'icons-flex icon-dir-row',
                                    title   : 'Row',
                                },{
                                    value   : 'row-reverse',
                                    name    : 'Row reverse',
                                    className : 'icons-flex icon-dir-row-rev',
                                    title   : 'Row reverse',
                                },{
                                    value   : 'column',
                                    name    : 'Column',
                                    title   : 'Column',
                                    className : 'icons-flex icon-dir-col',
                                },{
                                    value   : 'column-reverse',
                                    name    : 'Column reverse',
                                    title   : 'Column reverse',
                                    className : 'icons-flex icon-dir-col-rev',
                                }],
                            },{
                                name      : 'Justify',
                                property  : 'justify-content',
                                type    : 'radio',
                                defaults  : 'flex-start',
                                list    : [{
                                    value   : 'flex-start',
                                    className : 'icons-flex icon-just-start',
                                    title   : 'Start',
                                },{
                                    value   : 'flex-end',
                                    title    : 'End',
                                    className : 'icons-flex icon-just-end',
                                },{
                                    value   : 'space-between',
                                    title    : 'Space between',
                                    className : 'icons-flex icon-just-sp-bet',
                                },{
                                    value   : 'space-around',
                                    title    : 'Space around',
                                    className : 'icons-flex icon-just-sp-ar',
                                },{
                                    value   : 'center',
                                    title    : 'Center',
                                    className : 'icons-flex icon-just-sp-cent',
                                }],
                            },{
                                name      : 'Align',
                                property  : 'align-items',
                                type    : 'radio',
                                defaults  : 'center',
                                list    : [{
                                    value   : 'flex-start',
                                    title    : 'Start',
                                    className : 'icons-flex icon-al-start',
                                },{
                                    value   : 'flex-end',
                                    title    : 'End',
                                    className : 'icons-flex icon-al-end',
                                },{
                                    value   : 'stretch',
                                    title    : 'Stretch',
                                    className : 'icons-flex icon-al-str',
                                },{
                                    value   : 'center',
                                    title    : 'Center',
                                    className : 'icons-flex icon-al-center',
                                }],
                            },{
                                name: 'Flex Children',
                                property: 'label-parent-flex',
                                type: 'integer',
                            },{
                                name:     'Order',
                                property:   'order',
                                type:     'integer',
                                defaults :  0,
                                min: 0
                            },{
                                name    : 'Flex',
                                property  : 'flex',
                                type    : 'composite',
                                properties  : [{
                                    name:     'Grow',
                                    property:   'flex-grow',
                                    type:     'integer',
                                    defaults :  0,
                                    min: 0
                                },{
                                    name:     'Shrink',
                                    property:   'flex-shrink',
                                    type:     'integer',
                                    defaults :  0,
                                    min: 0
                                },{
                                    name:     'Basis',
                                    property:   'flex-basis',
                                    type:     'integer',
                                    units:    ['px','%',''],
                                    unit: '',
                                    defaults :  'auto',
                                }],
                            },{
                                name      : 'Align',
                                property  : 'align-self',
                                type      : 'radio',
                                defaults  : 'auto',
                                list    : [{
                                    value   : 'auto',
                                    name    : 'Auto',
                                },{
                                    value   : 'flex-start',
                                    title    : 'Start',
                                    className : 'icons-flex icon-al-start',
                                },{
                                    value   : 'flex-end',
                                    title    : 'End',
                                    className : 'icons-flex icon-al-end',
                                },{
                                    value   : 'stretch',
                                    title    : 'Stretch',
                                    className : 'icons-flex icon-al-str',
                                },{
                                    value   : 'center',
                                    title    : 'Center',
                                    className : 'icons-flex icon-al-center',
                                }],
                            }]
                        }
                        ],
                    },
                },
            });

            editor.I18n.addMessages({
                en: {
                    styleManager: {
                        properties: {
                            'background-repeat': 'Repeat',
                            'background-position': 'Position',
                            'background-attachment': 'Attachment',
                            'background-size': 'Size',
                        }
                    },
                }
            });

            var pn = editor.Panels;
            var modal = editor.Modal;
            var cmdm = editor.Commands;
            cmdm.add('canvas-clear', function() {
                if(confirm('Areeee you sure to clean the canvas?')) {
                    var comps = editor.DomComponents.clear();
                    setTimeout(function(){ localStorage.clear()}, 0)
                }
            });
            cmdm.add('set-device-desktop', {
                run: function(ed) { ed.setDevice('Desktop') },
                stop: function() {},
            });
            cmdm.add('set-device-tablet', {
                run: function(ed) { ed.setDevice('Tablet') },
                stop: function() {},
            });
            cmdm.add('set-device-mobile', {
                run: function(ed) { ed.setDevice('Mobile portrait') },
                stop: function() {},
            });



            // Add info command
            var mdlClass = 'gjs-mdl-dialog-sm';
            var infoContainer = document.getElementById('info-panel');
            cmdm.add('open-info', function() {
                var mdlDialog = document.querySelector('.gjs-mdl-dialog');
                mdlDialog.className += ' ' + mdlClass;
                infoContainer.style.display = 'block';
                modal.setTitle('About this demo');
                modal.setContent(infoContainer);
                modal.open();
                modal.getModel().once('change:open', function() {
                    mdlDialog.className = mdlDialog.className.replace(mdlClass, '');
                })
            });
            pn.addButton('options', {
                id: 'open-info',
                className: 'fa fa-question-circle',
                command: function() { editor.runCommand('open-info') },
                attributes: {
                    'title': 'About',
                    'data-tooltip-pos': 'bottom',
                },
            });


            // Simple warn notifier
            var origWarn = console.warn;
            toastr.options = {
                closeButton: true,
                preventDuplicates: true,
                showDuration: 250,
                hideDuration: 150
            };
            console.warn = function (msg) {
                if (msg.indexOf('[undefined]') == -1) {
                    toastr.warning(msg);
                }
                origWarn(msg);
            };


            // Add and beautify tooltips
            [['sw-visibility', 'Show Borders'], ['preview', 'Preview'], ['fullscreen', 'Fullscreen'],
                ['export-template', 'Export'], ['undo', 'Undo'], ['redo', 'Redo'],
                ['gjs-open-import-webpage', 'Import'], ['canvas-clear', 'Clear canvas']]
                .forEach(function(item) {
                    pn.getButton('options', item[0]).set('attributes', {title: item[1], 'data-tooltip-pos': 'bottom'});
                });
            [['open-sm', 'Style Manager'], ['open-layers', 'Layers'], ['open-blocks', 'Blocks']]
                .forEach(function(item) {
                    pn.getButton('views', item[0]).set('attributes', {title: item[1], 'data-tooltip-pos': 'bottom'});
                });
            var titles = document.querySelectorAll('*[title]');

            for (var i = 0; i < titles.length; i++) {
                var el = titles[i];
                var title = el.getAttribute('title');
                title = title ? title.trim(): '';
                if(!title)
                    break;
                el.setAttribute('data-tooltip', title);
                el.setAttribute('title', '');
            }

            // Show borders by default
            pn.getButton('options', 'sw-visibility').set('active', 1);


            // Store and load events
            // editor.on('storage:load', function(e) { console.log('Loaded ', e) });
            // editor.on('storage:store', function(e) { console.log('Stored ', e) });


            console.log(WebBuilderObj.form_action[0].name)

            editor.DomComponents.addType('form', {
                // Make the editor understand when to bind `my-input-type`
                // Model definition
                model: {
                    defaults: {
                        tagName: 'input',
                        // draggable: 'form, form *', // Can be dropped only inside `form` elements
                        // droppable: true, // Can't drop other elements inside
                        attributes: { // Default attributes
                            type: 'input',
                            name: 'default-name',
                            // placeholder: 'Insert text here',
                            action  : WebBuilderObj.form_action[0].value,
                            method : 'post'
                        },
                        traits: [{
                            type: 'select',
                            name: 'method',
                            options: [
                                {value: 'post', name: 'POST'},
                            ],
                        }, {
                            type: 'select',
                            name: 'action',
                            options: WebBuilderObj.form_action,
                        }
                        ],
                    }
                }
            });

            editor.DomComponents.addType('input', {
                // Make the editor understand when to bind `my-input-type`
                // Model definition
                model: {
                    defaults: {
                        tagName: 'input',
                        attributes: { // Default attributes
                            type: 'text',
                            name : 'first_name'
                        },
                        traits: [{
                            type: 'select',
                            name: 'name',
                            options: [
                                {value: 'first_name', name: 'first_name'},
                                {value: 'last_name', name: 'last_name'},
                                {value: 'email', name: 'email'},
                            ],
                        }, {
                            type: 'select',
                            name: 'type',
                            options: [
                                {value: 'email', name: 'email'},
                                {value: 'text', name: 'text'},
                            ],
                        }
                        ],
                    }
                }
            });

            editor.DomComponents.addType('textarea', {
                // Make the editor understand when to bind `my-input-type`
                // Model definition
                model: {
                    defaults: {
                        tagName: 'textarea',
                        attributes: { // Default attributes
                            type: 'textarea',
                            name : 'address'
                        },
                        traits: [{
                            type: 'select',
                            name: 'name',
                            options: [
                                {value: 'address', name: 'address'},
                                {value: 'others', name: 'others'},
                            ],
                        }
                        ],
                    }
                }
            });

            // const domc = editor.DomComponents;

            editor.Panels.addButton
            ('options',
                [{
                    id: 'save-db',
                    className: 'fa fa-floppy-o',
                    command: 'save-db',
                    attributes: {title: 'Upload'}
                }]
            );

            // Add the command
            editor.Commands.add
            ('save-db',
                {
                    run: function (editor, sender) {

                        sender && sender.set('active', 0); // turn off the button
                        // editor.store();

                        const _token = $('meta[name="csrf-token"]').attr('content');


                        $.ajax({
                            url: WebBuilderObj.update_page_content,
                            type: "POST",
                            data: {
                                id: WebBuilderObj.id,
                                template: WebBuilderObj.template,
                                html_data: editor.getHtml(true),
                                css_data: editor.getCss(true),
                                _token: _token,
                            },
                            success: function (response) {
                                if (response.status) {
                                    toastr.success(response.message)
                                } else {
                                    toastr.error(response.message);
                                }
                            }
                        });
                    }
                });


            editor.Panels.addButton
            ('options',
                [{
                    id: 'redirect-template',
                    className: 'fa fa-external-link',
                    command: 'redirect-template',
                    attributes: {title: 'Redirect to dashboard'}
                }]
            );

            editor.Commands.add
            ('redirect-template',
                {
                    run: function (editor, sender) {

                        window.location.href = WebBuilderObj.app_url;
                    }
                });

        })(window, jQuery, _token);
    });
})(WebBuilderObj);

