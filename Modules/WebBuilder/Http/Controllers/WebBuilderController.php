<?php

namespace Modules\WebBuilder\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Modules\Contact\Contracts\Services\ContactContact;
use Modules\Contact\Entities\Contact;
use Modules\UserDashboard\Contracts\Repositories\AutoFollowupQueueRepository;
use Modules\UserDashboard\Contracts\Repositories\AutoFollowupRepository;
use Modules\WebBuilder\Contracts\Repositories\WebBuilderRepository;
use Modules\WebBuilder\Contracts\Services\WebBuilderContact;
use Modules\WebBuilder\Entities\WebBuilder;
use Modules\WebBuilder\Http\Services\WebBuilderService;
use Response;

class WebBuilderController extends Controller
{
    /**
     * @var WebBuilderContact
     */
    private $webBuilderService;
    private $contactService;
    private $webBuilderRepository;
    private $autoFollowupQueueRepository;

    public function __construct(WebBuilderContact $webBuilderService,
                                ContactContact $contactService,
                                WebBuilderRepository $webBuilderRepository,
                                AutoFollowupQueueRepository $autoFollowupQueueRepository)
    {
        $this->webBuilderService = $webBuilderService;
        $this->contactService = $contactService;
        $this->webBuilderRepository = $webBuilderRepository;
        $this->autoFollowupQueueRepository = $autoFollowupQueueRepository;
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index($templateType)
    {
        return view('webbuilder::web-builder-page-view')->with($this->webBuilderService->getTemplateDataForView(decrypt($templateType), Auth::id()));
    }

    public function templateViewForEdit($templateType)
    {
        return view('webbuilder::edit')->with($this->webBuilderService->getTemplateData(decrypt($templateType), Auth::id()));
    }

    public function updatePageContent(Request $request)
    {
        $request['user_id'] = Auth::id();

        return response()->json($this->webBuilderService->pageBuilderUpdate($request));

    }

    public function updateImage(Request $request)
    {
        $resultArray = array();

        foreach ($_FILES as $key => $file) {

            $fileName = $file['name'];
            $tmpName = $file['tmp_name'];
            $fileSize = $file['size'];
            $fileType = $file['type'];

            $fp = fopen($tmpName[0], 'r');


            $fp = fopen($tmpName[0], 'r');
            $content = fread($fp, filesize($tmpName[0]));
            fclose($fp);

            $result = array(
                'name' => $file['name'][0],
                'type' => 'image',
                'src' => "data:" . $fileType[0] . ";base64," . base64_encode($content),
                'height' => 350,
                'width' => 250
            );

            array_push($resultArray, $result);

        }

        $response = array('data' => $resultArray[0]);

        echo json_encode($response);
    }

    public function defaultTemplateSet(Request $request)
    {
        return $this->webBuilderService->defaultTemplateSetById($request['id'], Auth::id());
    }

    public function addTemplate(Request $request)
    {
        return $this->webBuilderService->addTemplate($request, Auth::id());

    }

    public function actionUrlForSubscription(Request $request)
    {
        $responseData = $this->webBuilderService->actionUrlForSubscription($request);

        return Response::json([
            'page' => 'redirect',
            'status' => $responseData['status'],
            'message' => 'Thank you for subscription',
            'html' => $responseData['html'],
            'url' => isset($responseData['url']) ? $responseData['url'] : ""
        ]);
    }

    public function actionUrlForDownloadInstruction(Request $request)
    {
        $responseData = $this->webBuilderService->actionUrlForDownloadInstruction($request);

        return Response::json([
            'page' => 'redirect',
            'status' => $responseData['status'],
            'message' => 'Thank you for download instruction',
            'url' => isset($responseData['url']) ? $responseData['url'] : ""
        ]);

    }

    public function actionUrlForOffer(Request $request)
    {
        $email = $request->get('email');
        $userId = $request->get('user_id');
        Log::info("Offer Action");
        Log::info($email);
        Log::info($userId);


        $where = [
          'email' => $email,
          'user_id' => $userId,
        ];
        //Update or insert as contact lead
        $contactId = null;
        $contactData = $this->contactService->getContactByWhere($where);
        Log::info($contactData);

        if ($contactData) {
            $contactId = $contactData->id;
            $update = [
                'type' => Contact::LEAD
            ];
            $this->contactService->updateContactByWhere($where, $update);
        } else {
            $data = [
                'user_id' => $userId,
                'email' => $email,
                'type' => Contact::LEAD,
                'status' => Contact::ACTIVE,
            ];
            $contactData = $this->contactService->addContactByData($data);
            $contactId = $contactData->id;
        }

        //If auto follow ups, then delete
        $this->autoFollowupQueueRepository->deleteQueuesByContactByCondition(['contact_id' => $contactId]);


        App::make(WebBuilderService::class)->updateCount($userId, 'total_offer_submits');

        $webBuilderData = $this->webBuilderRepository->getTemplateByCondition([
            'user_id' => $userId,
            'type' => WebBuilder::WEB_BUILDER_TYPE_THANK_YOU,
        ]);

        return Response::json([
            'page' => 'redirect',
            'status' => true,
            'message' => 'Thank you for accepting offer',
            'url' => route('thank-you-slug', [
                'id' => $webBuilderData->id
            ])
        ]);
    }

    public function actionUrlForThankYou(Request $request)
    {
        dd($request->all());

    }

    public function subscriberSlug($id)
    {
        return view('webbuilder::web-builder-page-view')->with($this->webBuilderService->getTemplateDataForPublicView($id, WebBuilder::WEB_BUILDER_TYPE_SUBSCRIPTION));

    }

    public function downloadInstructionSlug($id)
    {
        return view('webbuilder::web-builder-page-view')->with($this->webBuilderService->getTemplateDataForPublicView($id, WebBuilder::WEB_BUILDER_TYPE_DOWNLOAD_INSTRUCTION));
    }

    public function offerSlug($id)
    {
        return view('webbuilder::web-builder-page-view')->with($this->webBuilderService->getTemplateDataForPublicView($id, WebBuilder::WEB_BUILDER_TYPE_OFFER));
    }

    public function thankYouSlug($id)
    {
        return view('webbuilder::web-builder-page-view')->with($this->webBuilderService->getTemplateDataForPublicView($id, WebBuilder::WEB_BUILDER_TYPE_THANK_YOU));
    }

    public function thankYouPageAdd(Request $request)
    {
        return $this->webBuilderService->thankYouPageAdd($request, Auth::id());
    }

}
