<?php

namespace Modules\WebBuilder\Http\Services;

use App\Contracts\Repository\UserRepository;
use App\Services\SendgridMailService;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;
use Modules\AdminWebBuilder\Contracts\Repositories\AdminWebTemplateRepository;
use Modules\AdminWebBuilder\Entities\AdminWebTemplate;
use Modules\Contact\Contracts\Repositories\ContactRepository;
use Modules\Contact\Entities\Contact;
use Modules\EmailSetting\Contracts\Repositories\EmailSettingRepository;
use Modules\Package\Contracts\Service\UserBalanceContact;
use Modules\UserDashboard\Contracts\Repositories\OverallStatRepository;
use Modules\UserDashboard\Contracts\Service\AutoFollowupContact;
use Modules\WebBuilder\Contracts\Repositories\WebBuilderRepository;
use Modules\WebBuilder\Contracts\Services\WebBuilderContact;
use Modules\WebBuilder\Entities\WebBuilder;

class WebBuilderService implements WebBuilderContact
{
    /**
     * @var WebBuilderRepository
     */
    private $webBuilderRepository;
    /**
     * @var AdminWebTemplateRepository
     */
    private $adminWebTemplateRepository;
    /**
     * @var ContactRepository
     */
    private $contactRepository;
    /**
     * @var SendgridMailService
     */
    private $sendgridMailService;
    /**
     * @var AutoFollowupContact
     */
    private $autoFollowupService;
    /**
     * @var OverallStatRepository
     */
    private $overallStatRepository;
    /**
     * @var UserBalanceContact
     */
    private $userBalanceService;

    public function __construct(WebBuilderRepository $webBuilderRepository,
                                AdminWebTemplateRepository $adminWebTemplateRepository,
                                ContactRepository $contactRepository,
                                AutoFollowupContact $autoFollowupService,
                                SendgridMailService $sendgridMailService,
                                OverallStatRepository $overallStatRepository,
                                UserBalanceContact $userBalanceService
    )
    {
        $this->webBuilderRepository = $webBuilderRepository;
        $this->adminWebTemplateRepository = $adminWebTemplateRepository;
        $this->contactRepository = $contactRepository;
        $this->sendgridMailService = $sendgridMailService;
        $this->autoFollowupService = $autoFollowupService;
        $this->overallStatRepository = $overallStatRepository;
        $this->userBalanceService = $userBalanceService;
    }


    public function getTemplateData($templateType, $userId)
    {
        $result = $this->webBuilderRepository->getTemplateByCondition([
            'type' => $templateType,
            'user_id' => $userId
        ]);

        if (is_null($result) && $templateType == AdminWebTemplate::ADMIN_WEB_TYPE_THANK_YOU) {

            $result = $this->adminWebTemplateRepository->getTemplateByConditionFirst([
                'type' => $templateType,
            ]);

            $this->webBuilderRepository->UpdateOrCreateWebBuilderTemplate([
                'user_id' => $userId,
                'type' => $result->type,
            ],
                [
                    'content' => $result->content,
                    'css' => $result->css,
                ]
            );
        }

        return [
            'template' => $templateType,
            'htmlData' => $result->content,
            'cssData' => $result->css,
            'templateId' => $result->id,
            'userId' => $userId,
        ];
    }


    public function pageBuilderUpdate($request)
    {

        $builderUpdate = $this->webBuilderRepository->UpdateOrCreateWebBuilderTemplate([
            'id' => $request['id'],
            'user_id' => $request['user_id']
        ], [
            'content' => $request['html_data'],
            'css' => $request['css_data'],
        ]);

        if ($builderUpdate) {
            return ['status' => true, 'message' => 'Template Updated Successfully!'];
        } else {
            return ['status' => false, 'message' => 'Template Not Updated!'];
        }


    }

    public function defaultTemplateSetById($id, $userId)
    {
        $result = $this->adminWebTemplateRepository->getTemplateByConditionFirst(['id' => $id]);

        $response = $this->webBuilderRepository->checkHasDataByCondition([
            'type' => $result->type,
            'user_id' => $userId
        ]);

        if (!$response) {
            $res = $this->webBuilderRepository->createWebBuilderTemplate([
                'content' => $result->content,
                'css' => $result->css,
                'type' => $result->type,
                'user_id' => $userId,
            ]);

            if ($res) {
                return [
                    'status' => true,
                ];
            }
        }

        return [
            'status' => false,
        ];
    }

    public function addTemplate($request, $userId)
    {
        $result = $this->adminWebTemplateRepository->getTemplateByConditionFirst(['id' => $request['id']]);

        $response = $this->webBuilderRepository->UpdateOrCreateWebBuilderTemplate([
            'user_id' => $userId,
            'type' => $result->type,
        ],
            [
                'content' => $result->content,
                'css' => $result->css,
                'image' => $result->image,

            ]
        );

        if ($response) {
            return [
                'html' => 'Template Added Successfully',
                'status' => true
            ];
        }

        return [
            'html' => 'Something is wrong !!',
            'status' => false
        ];

    }

    public function getTemplateDataForView($templateType, $userId)
    {
        $result = $this->webBuilderRepository->getTemplateByCondition([
            'type' => $templateType,
            'user_id' => $userId
        ]);

        if (is_null($result) && $templateType == AdminWebTemplate::ADMIN_WEB_TYPE_THANK_YOU) {

            $result = $this->adminWebTemplateRepository->getTemplateByConditionFirst([
                'type' => $templateType,
            ]);

            $this->webBuilderRepository->UpdateOrCreateWebBuilderTemplate([
                'user_id' => $userId,
                'type' => $result->type,
            ],
                [
                    'content' => $result->content,
                    'css' => $result->css,
                    'image' => $result->image,
                ]
            );
        }

        return [
            'template' => $templateType,
            'htmlData' => isset($result->content) ? $result->content : '',
            'cssData' => isset($result->css) ? $result->css : '',
            'userId' => $userId,
            'id' => isset($result->id) ? $result->id : null,
            'title' => WebBuilder::getAdminWebTypeName($templateType),
        ];
    }

    private function formFieldsMatcher($request)
    {
        $form_array = [];
        foreach (WebBuilder::FORM_FIELDS as $key => $field) {
            $form_array[$field] = $request[$field];
        }

        return $form_array;
    }

    private function subscriptionEmailSend($email, $userid)
    {

        $trackingId = "TRACKINGID" . strtotime("now");
        $webBuilderData = $this->webBuilderRepository->getTemplateByCondition([
            'user_id' => $userid,
            'type' => WebBuilder::WEB_BUILDER_TYPE_DOWNLOAD_INSTRUCTION,
        ]);

        $data['subject'] = "Download Instruction";
        $data['email'] = $email;
        $data['redirect_url'] = env('APP_URL') . '/download-instruction/' . $webBuilderData->id . '/v';
        $data['link'] = env('APP_URL') . '/pdf_download_url/' . $userid;

        $message = View::make('webbuilder::mail.subscription', $data)->render();

        $companyInfo = App::make(EmailSettingRepository::class)->getEmailDataByUserId($userid);

        $userInfo = App::make(UserRepository::class)->getUserById($userid);

       $companyName = !is_null($companyInfo) ? !is_null($companyInfo->company_name) ? $companyInfo->company_name : '' : '';

       if (empty($companyName)){
           $companyName = $userInfo->first_name;
       }

        Log::info('companyName : '.$companyName);

        $this->sendgridMailService->sendMailWithSendGrid('Download EBook', $email, env('FROM_EMAIL'), $message, $companyName, $trackingId);

        return $data['redirect_url'];
    }

    public function actionUrlForSubscription($request)
    {
        $form_array = $this->formFieldsMatcher($request);

        // check user id exist or not
        if (is_null($form_array['user_id'])) {
            return [
                'html' => 'Something is wrong !!',
                'status' => false,
                'reason' => 'user id not found'
            ];
        }

        if (is_null($form_array['email'])) {
            return [
                'html' => 'Need Email address !!',
                'status' => false,
                'reason' => 'email not found'
            ];
        }

        // check already has this email
        $check = $this->contactRepository->checkByCondition([
            'user_id' => $form_array['user_id'],
            'email' => $form_array['email'],
        ]);

        if ($check) {
            return [
                'html' => 'Already Subscribe by this email',
                'status' => false,
                'reason' => 'Already Subscribe by this email'
            ];
        }

        // contact insert as a subscriber
        $contact = $this->contactRepository->updateOrCreateContact([
            'user_id' => $form_array['user_id'],
            'email' => $form_array['email'],
        ], [
            'first_name' => $form_array['first_name'],
            'last_name' => $form_array['last_name'],
            'number' => $form_array['number'],
            'type' => Contact::SUBSCRIBER
        ]);

        //Cut balance
        $checkUserBalance = $this->userBalanceService->checkBalanceByUserIdAndType($contact['user_id']);
        if ($checkUserBalance->remaining_no_of_visitors > 0) {
            App::make(UserBalanceContact::class)->reduceBalanceByUserIdAndType($contact['user_id'], 'remaining_no_of_visitors', 1);

            $this->updateCount($contact['user_id'], 'total_subscription_submits');
            $this->autoFollowupService->addFirstAutoFollowupMessageInCampaignQueueForNewSubscriber($contact['id'], $contact['user_id']);


            // send email download instruction
            $response = $this->subscriptionEmailSend($form_array['email'], $form_array['user_id']);


            if ($response) {

                return [
                    'html' => 'Subscribe successfully',
                    'url' => $response,
                    'status' => true,
                ];
            }

            return [
                'html' => 'Something is wrong !!',
                'status' => false,
                'reason' => 'email not send'
            ];
        } else {
            return [
                'html' => 'You cannot subscribe. Please contact with the user!',
                'status' => false,
                'reason' => 'Unique visitor count has finished!!'
            ];
        }

    }

    public function actionUrlForDownloadInstruction($request)
    {
//        $this->autoFollowupService->addFirstAutoFollowupMessageInCampaignQueueForNewSubscriber($request['contact_id'], $request['user_id']);
        $where = [
            'user_id' => $request['user_id'],
            'type' => WebBuilder::WEB_BUILDER_TYPE_OFFER
        ];
        $result = $this->webBuilderRepository->getTemplateByCondition($where);
        $url = env('APP_URL') . '/offer/' . $result->id . '/v';
        return [
            'url' => $url,
            'status' => true,
            'message' => 'Thank you for being with us.'
        ];
    }

    public function getTemplateDataForPublicView($id, $type)
    {
        $result = $this->webBuilderRepository->getTemplateByCondition([
            'id' => $id,
            'type' => $type,
        ]);

        $userId = $result->user_id;

        if ($type == WebBuilder::WEB_BUILDER_TYPE_SUBSCRIPTION) {
            $isVisited = $this->visitorCookieSet('subscription_page_visited');
            if ($isVisited == false) {
                $this->updateCount($userId, 'total_unique_visitors');
                $this->updateCount($userId, 'total_subscription_visits');


            }
        } else if ($type == WebBuilder::WEB_BUILDER_TYPE_DOWNLOAD_INSTRUCTION) {
            $isVisited = $this->visitorCookieSet('download_instruction_page_visited');
            if ($isVisited == false) {
                $this->updateCount($userId, 'total_download_instruction_visits');
            }

        } else if ($type == WebBuilder::WEB_BUILDER_TYPE_OFFER) {
            $isVisited = $this->visitorCookieSet('total_offer_visits');
            if ($isVisited == false) {
                $this->updateCount($userId, 'total_offer_visits');
            }
        } else if ($type == WebBuilder::WEB_BUILDER_TYPE_THANK_YOU) {
            $isVisited = $this->visitorCookieSet('total_thank_you_visits');
            if ($isVisited == false) {
                $this->updateCount($userId, 'total_thank_you_visits');
            }
        }

        return [
            'template' => $type,
            'htmlData' => $result->content,
            'cssData' => $result->css,
            'userId' => $result->user_id,
        ];
    }

    public function updateCount($userId, $fieldName)
    {
        $data = [
            'user_id' => $userId,
            $fieldName => 1,
            'activity_date_string' => date('Y-m-d')
        ];

        $checkIfExits = $this->overallStatRepository->checkDataIsExitsByDate($userId, date('Y-m-d'));


        if (!$checkIfExits) {
            //// Insert new data in stat count table
            $this->overallStatRepository->insertOverallStatisticsData($data, $userId);
        } else {

            $dataArray = [
                'user_id' => $userId,
                'activity_date_string' => date('Y-m-d')
            ];

            $this->overallStatRepository->updateTotalCount($dataArray, $data, $fieldName);
        }

        /******###### OVER ALL COUNT REPORT UPDATE END #####******/
    }


    public function visitorCookieSet($page)
    {
        if (!Cookie::get($page)) {
            Cookie::queue($page, true, 60 * 24 * 30);
            return false;

        }
        return true;
    }

    public function thankYouPageAdd($request, $userId)
    {
        $result = $this->adminWebTemplateRepository->getTemplateByConditionFirst(['type' => AdminWebTemplate::ADMIN_WEB_TYPE_THANK_YOU]);

        $response = $this->webBuilderRepository->UpdateOrCreateWebBuilderTemplate([
            'user_id' => $userId,
            'type' => WebBuilder::WEB_BUILDER_TYPE_THANK_YOU,
        ],
            [
                'content' => isset($result->content) ? $result->content : '',
                'css' => isset($result->css) ? $result->css : '',
            ]
        );
    }
}
