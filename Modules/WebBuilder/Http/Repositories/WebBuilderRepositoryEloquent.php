<?php

namespace Modules\WebBuilder\Http\Repositories;

use App\Repositories\BaseRepository\BaseRepository;
use Modules\WebBuilder\Contracts\Repositories\WebBuilderRepository;
use Modules\WebBuilder\Entities\WebBuilder;

class WebBuilderRepositoryEloquent extends BaseRepository implements WebBuilderRepository
{
    protected function model()
    {
        return new WebBuilder();
    }

    public function checkHasDataByCondition($where)
    {
        return $this->model()->where($where)->exists();
    }

    public function createWebBuilderTemplate($data)
    {
        return $this->model()->create($data);
    }

    public function UpdateOrCreateWebBuilderTemplate($where , $data)
    {
        return $this->model->updateOrCreate($where, $data);
    }

    public function getTemplateByCondition($where)
    {
        return $this->model()->where($where)->first();
    }
}

