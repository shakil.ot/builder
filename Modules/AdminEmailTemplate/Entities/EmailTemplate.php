<?php

namespace Modules\AdminEmailTemplate\Entities;

use Illuminate\Database\Eloquent\Model;

class EmailTemplate extends Model
{
    protected $fillable = [
        'id',
        'title',
        'subject',
        'body'
    ];

    const DAYARRAY = [
        'Imediate',
        '2 days',
        '4 days',
        '6 days',
        '8 days',
    ];

}
