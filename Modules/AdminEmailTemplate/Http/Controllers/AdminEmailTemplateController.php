<?php

namespace Modules\AdminEmailTemplate\Http\Controllers;

use App\Http\Requests\EmailTemplateRequest;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Routing\Controller;
use Modules\AdminEmailTemplate\Contracts\Service\EmailTemplateContact;

class AdminEmailTemplateController extends Controller
{
    /**
     * @var EmailTemplateContact
     */
    private $emailTemplateService;

    public function __construct(EmailTemplateContact $emailTemplateService)
    {
        $this->emailTemplateService = $emailTemplateService;
    }

    public function index()
    {
        $templates = $this->emailTemplateService->getAllEmailTemplate();
        return view('adminemailtemplate::index', compact('templates'));
    }

    public function getAllEmailTemplate()
    {
        return $this->emailTemplateService->getAllEmailTemplate();
    }

    public function listEmailTemplate()
    {
        return $this->emailTemplateService->listEmailTemplate();
    }

    public function getEmailTemplateAddForm()
    {
        $emailTemplates = $this->emailTemplateService->getAllEmailTemplate();
        return Response::json([
            'html' => view('adminemailtemplate::add-template')->with([
                'emailTemplates' => $emailTemplates
            ])->render(),
            'status' => 'success'
        ]);
    }


    public function addEmail(EmailTemplateRequest $request)
    {
        $result = $this->emailTemplateService->addEmailTemplate($request);

        return Response::json($result);
    }

    public function getTemplateViewForm(Request $request)
    {
        if (!$request->ajax()) {
            return redirect()->route('Dashboard');
        }

        $templateId = $request['id'];
        $response = $this->emailTemplateService->getTemplateViewForm($templateId);
        return Response::json([
            'html' => $response['html'],
            'status' => $response['status']
        ]);
    }

    public function getEmailTemplateEditForm(Request $request)
    {
        if (!$request->ajax()) {
            return redirect()->route('Dashboard');
        }
        $response = $this->emailTemplateService->getTemplateEditForm($request);
        return Response::json([
            'html' => $response['html'],
            'status' => $response['status']
        ]);
    }

    public function updateEmailTemplate(Request $request)
    {
        $templateData = $this->emailTemplateService->updateTemplateById($request);

        if ($templateData) {
            return Response::json([
                'html' => $templateData['html'],
                'status' => $templateData['status']
            ]);
        }
    }

    public function deleteEmailTemplate(Request $request)
    {
        return $this->emailTemplateService->deleteTemplateById($request);

    }

}
