<?php

namespace Modules\AdminEmailTemplate\Http\Services;


use App\Helper\UtilityHelper;
use Illuminate\Support\Str;
use Modules\AdminEmailTemplate\Contracts\Repositories\EmailTemplateRepository;
use Modules\AdminEmailTemplate\Contracts\Service\EmailTemplateContact;
use Yajra\DataTables\DataTables;

class EmailTemplateService implements EmailTemplateContact
{
    /**
     * @var EmailTemplateRepository
     */
    private $emailTemplateRepository;

    public function __construct(EmailTemplateRepository $emailTemplateRepository)
    {
        $this->emailTemplateRepository = $emailTemplateRepository;
    }

    public function getAllEmailTemplate()
    {
        return $this->emailTemplateRepository->getAllEmailTemplate();
    }

    public function getTotalEmailTemplate()
    {
        return $this->emailTemplateRepository->getTotalEmailTemplate();
    }
    public function getTemplateById($id)
    {
        return $this->emailTemplateRepository->getTemplateById($id);
    }

    public function listEmailTemplate()
    {
        $templates = $this->emailTemplateRepository->getAllEmailTemplate();


        return Datatables::of($templates)

            ->addColumn('email_title', function ($templates){
                if ($templates->title == null) {
                    return '<span class="label label-lg label-light-danger  label-inline">N/A</span>';
                } else {
                    return '<span class="label label-lg label-success label-inline">' . $templates->title. '</span>';
                }
            })
            ->addColumn('email_subject', function ($templates){
                if ($templates->subject == null) {
                    return '<span class="label label-lg label-light-danger  label-inline">N/A</span>';
                } else {
                    return '<span class="label label-lg label-light-success label-inline">' . $templates->subject . '</span>';
                }
            })
            ->addColumn('action', function ($templates){
                $btn = '<div><a href="javascript:void(0)" class="viewTemplate" id="templateId"
                            data-id="' . $templates->id . ' " data-value="view">
                            <i class="fas fa-eye"></i> View </span>
                            </a>'
                    .

                    '   <a href="javascript:void(0)" class="editTemplate" id="templateId"
                            data-id="' . $templates->id . ' ">
                            <i class="la la-pencil"></i> Edit </span>
                            </a>'
                    .
                    '<a href="javascript:void(0)" class="deleteTemplate"
                            data-value="delete" data-id="' . $templates->id . '"><i class="la la-trash-o"></i> Delete
                            </a>
                        </div>'
                ;
                return $btn;
            })
            ->rawColumns(['email_title','email_subject', 'action'])
            ->make(true);
    }

    public function addEmailTemplate($request)
    {
        $data = [
          'title' => $request['title'],
          'subject' => $request['subject'],
          'body' => $request['body'],
        ];

        $data =  $this->emailTemplateRepository->addEmailTemplate($data);

        if($data)
        {
            return UtilityHelper::RETURN_STATUS_FORMATE('success', 'Template added.', '');
        }

        return UtilityHelper::RETURN_STATUS_FORMATE('error', 'Failed. Try again');
    }

    public function getTemplateViewForm($templateId)
    {
        $template = $this->emailTemplateRepository->getEmailTemplateById($templateId);
        return [
            'html' => view('adminemailtemplate::view-template')->with([
                'template' => $template
            ])->render(),
            'status' => 'success'
        ];
    }

    public function getTemplateEditForm($request)
    {
        $templateId = $request['id'];
        $template =  $this->emailTemplateRepository->getEmailTemplateById($templateId);
        $emailTemplates = $this->getAllEmailTemplate();

        return [
            'html' => view('adminemailtemplate::edit-template')->with([
                'template' => $template,
                'emailTemplates' => $emailTemplates,
            ])->render(),
            'status' => 'success'
        ];
    }

    public function updateTemplateById($request)
    {
        $request = $request['request'];
        $params = [
            'title' => $request['title'],
            'subject' => $request['subject'],
            'body' => $request['body'],
        ];

        $where = [
          'id' => $request['id'],
        ];

        $template = $this->emailTemplateRepository->updateTemplateById($where, $params);

        if($template)
        {
            return [
                'html' => 'Template Updated.',
                'status' => 'success'
            ];
        }

        else {
            return [
                'html' => "Sorry! Template updating failed! Try again",
                'status' => 'error',
            ];
        }

    }

    public function deleteTemplateById($request)
    {
        $templateId = $request->templateId;
        $result = $this->emailTemplateRepository->deleteTemplateById($templateId);

        if($result)
        {
            return [
                'html' => 'Template Deleted!',
                'status' => 'success'
            ];
        }

        else {
            return [
                'html' => "Sorry! Template Delete failed! try again",
                'status' => 'error',
            ];
        }
    }

    public function getByLimitData()
    {
        return $this->emailTemplateRepository->getByLimitData();
    }
}
