<?php

namespace Modules\AdminEmailTemplate\Http\Repositories;

use App\Repositories\BaseRepository\BaseRepository;
use Modules\AdminEmailTemplate\Entities\EmailTemplate;
use Modules\AdminEmailTemplate\Contracts\Repositories\EmailTemplateRepository;

class EmailTemplateRepositoryEloquent extends BaseRepository implements EmailTemplateRepository
{
    protected function model()
    {
        return new EmailTemplate();
    }

    public function getAllEmailTemplate()
    {
        return $this->model::select('id', 'title', 'subject', 'body')->get();
    }

    public function getTotalEmailTemplate()
    {
        return $this->model()
            ->count();
    }

    public function getTemplateById($id)
    {
        return $this->model()
            ->where('id', $id)
            ->first();
    }

    public function addEmailTemplate($data)
    {
        return $this->model->create($data);
    }

    public function getEmailTemplateById($templateId)
    {
        return $this->model()
            ->where('id', $templateId)
            ->first();
    }

    public function updateTemplateById($where, $params)
    {
        return $this->model
            ->where($where)
            ->update($params);
    }

    public function deleteTemplateById($id)
    {
        return $this->model()
            ->find($id)
            ->delete();
    }

    public function getByLimitData($limitSize = 5)
    {
        return $this->model()->limit($limitSize)->get();
    }
}
