<?php

namespace Modules\AdminEmailTemplate\Contracts\Repositories;

interface EmailTemplateRepository
{
    public function getAllEmailTemplate();

    public function getTotalEmailTemplate();

    public function getTemplateById($id);

    public function addEmailTemplate($request);

    public function getEmailTemplateById($templateId);

    public function updateTemplateById($where, $params);

    public function deleteTemplateById($id);

    public function getByLimitData($limitSize = 5);
}
