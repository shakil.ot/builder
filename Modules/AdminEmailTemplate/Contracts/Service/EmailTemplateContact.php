<?php

namespace Modules\AdminEmailTemplate\Contracts\Service;

interface EmailTemplateContact
{
    public function getAllEmailTemplate();

    public function getTotalEmailTemplate();

    public function getTemplateById($id);

    public function listEmailTemplate();

    public function addEmailTemplate($request);

    public function getTemplateViewForm($templateId);

    public function getTemplateEditForm($request);

    public function updateTemplateById($request);

    public function deleteTemplateById($request);

    public function getByLimitData();
}
