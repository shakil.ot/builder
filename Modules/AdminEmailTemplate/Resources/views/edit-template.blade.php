<div class="col-md-12">
    <div class="card-body" style="padding: 0;">
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="personal_info" role="tabpanel" aria-labelledby="kt_tab_pane_2">
                <form  id="email-template-edit-form" autocomplete="off" data-parsley-validate="">
                    @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group w-100 d-flex justify-content-between bothOne">
                                <div class="leftOne">
                                    <button type="button" title="Personalize"
                                            class="btn dropdown-toggle personalize-toggle"
                                            style="background-color: #03385d; color: #ffffff;"
                                            data-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="true"
                                    >
                                        Subject Personalize
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end"
                                         style="height:400px; overflow-y:auto; position: absolute; transform: translate3d(-20px, 37px, 0px); top: 0px; left: 0px; will-change: transform;">
                                        @foreach(\App\Models\PersonalizeType::$allType as $each => $key)
                                            <button class="dropdown-item url-get-inbox-in-email" type="button"
                                                    data-title-email="message-get--email" data-url-email="{{$key}}">{{$each}}
                                            </button>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="rightOne">
                                    <div class="btn-group" role="group" aria-label="Button group with nested dropdown" style="float:right">
                                        <div class="btn-group" role="group">
                                            <button id="btnGroupDrop1" type="button"
                                                    class="btn dropdown-toggle"
                                                    style="background-color: #03385d; color: #ffffff;"
                                                    data-toggle="dropdown" aria-haspopup="true"
                                                    aria-expanded="false">
                                                Select Email Template
                                            </button>
                                            <div class="dropdown-menu" style="">
                                                @if(isset($emailTemplates[0]))
                                                    @foreach($emailTemplates as $emailTemplate)
                                                        <a class="dropdown-item email-template" data-id="{{$emailTemplate->id}}" href="javascript:void(0)" data-title="message-get">{{$emailTemplate->title}}</a>
                                                    @endforeach
                                                @else
                                                    No templates available
                                                @endif
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="title" class="col-form-label">Title</label>
                                <input class="form-control" type="text"
                                       name="title" id="title" value="{{ $template->title }}"
                                       placeholder="Enter Title" required=""
                                       data-parsley-trigger="change focusout"
                                       data-parsley-required-message="Title is required!"
                                >
                                <div class="custom-error-message"></div>
                            </div>
                            <div class="form-group">
                                <label for="subject" class="col-form-label">Subject</label>
                                <input class="form-control" type="text"
                                       name="subject" id="subject" value="{{ $template->subject }}"
                                       placeholder="Enter Title" required=""
                                       data-parsley-trigger="change focusout"
                                       data-parsley-required-message="Subject required!"
                                >
                                <div class="custom-error-message"></div>
                            </div>
                            <div class="form-group">
                                <label for="body" class="col-form-label">Email Body</label>
                                <textarea class="form-control body" name="body"
                                          id="emailBody" cols="30" rows="10"
                                          required=""
                                          data-parsley-required-message="Body required!">{!! $template->body !!}</textarea>
                                <div class="custom-error-message"></div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group mt-10">
                        <button type="submit" class="btn btn-green float-right font-weight-bold">
                                    <span class="svg-icon svg-icon-white">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                <path d="M6,2 L18,2 C19.6568542,2 21,3.34314575 21,5 L21,19 C21,20.6568542 19.6568542,22 18,22 L6,22 C4.34314575,22 3,20.6568542 3,19 L3,5 C3,3.34314575 4.34314575,2 6,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000"></path>
                                            </g>
                                        </svg>
                                    </span>
                            Update Template
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

