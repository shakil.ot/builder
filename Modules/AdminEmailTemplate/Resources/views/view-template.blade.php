<div class="col-md-12">
    <div class="card-body" style="padding: 0;">
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="personal_info" role="tabpanel" aria-labelledby="kt_tab_pane_2">
                    <div class="form-group">
                        <label for="title" class="col-form-label">Title</label>
                        <input class="form-control" type="text" name="title" id="title" value="{{ $template->title }}" readonly >
                        <div class="custom-error-message"></div>
                    </div>
                    <div class="form-group">
                        <label for="subject" class="col-form-label">Subject</label>
                        <input class="form-control" type="text" name="subject" id="subject" value="{{ $template->subject }}"  readonly>
                        <div class="custom-error-message"></div>
                    </div>
                    <div class="form-group">
                        <label for="body" class="col-form-label">Email Body</label>
                        <textarea class="form-control body" name="body"
                                  id="emailBody" cols="30" rows="10" readonly>{!! $template->body !!}</textarea>
                        <div class="custom-error-message"></div>
                    </div>
            </div>
        </div>
    </div>
</div>
