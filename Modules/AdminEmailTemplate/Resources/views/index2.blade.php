@extends('admin.layouts.app')


@section('container')

    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Entry-->
        <div class="card card-custom">
            <div class="card-header">
                <div class="card-title">
                    <span class="card-icon">
                        <i class="flaticon2-delivery-package text-primary"></i>
                    </span>
                    <h3 class="card-label">Email Template Manage</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="card-body">
                        <div class="tab-content mt-5" id="myTabContent">
                            <div class="tab-pane fade show active" id="personal_info" role="tabpanel" aria-labelledby="kt_tab_pane_2">
                                <form  id="email-template-form" autocomplete="off">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <h3 class="font-size-lg text-dark-75 font-weight-bold">
                                                <span class="svg-icon svg-icon-green svg-icon-2x">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                            <path d="M6,2 L18,2 C19.6568542,2 21,3.34314575 21,5 L21,19 C21,20.6568542 19.6568542,22 18,22 L6,22 C4.34314575,22 3,20.6568542 3,19 L3,5 C3,3.34314575 4.34314575,2 6,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000"></path>
                                                        </g>
                                                    </svg>
                                                </span>
                                                        Email Template
                                                    </h3>
                                                    <hr>
                                                    <div class="form-group">
                                                        <label for="title" class="col-form-label">Title</label>
                                                        <input class="form-control" type="text" name="title" value="" placeholder="Enter Title" required>
                                                        <div class="custom-error-message"></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="subject" class="col-form-label">Subject</label>
                                                        <input class="form-control" type="text" name="subject" value="" placeholder="Enter Title" required>
                                                        <div class="custom-error-message"></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="body" class="col-form-label">Email Body</label>
                                                        {{--                                                <input class="form-control" type="text" name="body" value="" id="emailBody" placeholder="Enter description" required>--}}
                                                        <textarea class="form-control body" name="body" id="emailBody" cols="30" rows="10" ></textarea>
                                                        <div class="custom-error-message"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group mt-10">
                                        <button type="submit" class="btn btn-green font-weight-bold">
                                    <span class="svg-icon svg-icon-white">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                <path d="M6,2 L18,2 C19.6568542,2 21,3.34314575 21,5 L21,19 C21,20.6568542 19.6568542,22 18,22 L6,22 C4.34314575,22 3,20.6568542 3,19 L3,5 C3,3.34314575 4.34314575,2 6,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000"></path>
                                            </g>
                                        </svg>
                                    </span>
                                            Add Template
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    {{--View Template--}}
                    @foreach($templates as $template)
                        <div class="card-body pt-8">
                            <!--begin::Item-->
                            <div class="d-flex align-items-center mb-10">
                                <!--begin::Text-->
                                <div class="d-flex flex-column font-weight-bold">
                                    <a href="#" class="text-dark text-hover-primary mb-1 font-size-lg">Title: {{ $template->title }}</a>
                                    <span class="text-muted">Subject: {{ $template->subject }}</span>
                                </div>
                                <!--end::Text-->
                            </div>
                            <!--end::Item-->
                        </div>
                    @endforeach
                    {{--/View Template--}}
                </div>
            </div>
        </div>
        <!--end::Entry-->
    </div>


@endsection

@push('scripts')

    <script type="text/javascript">
        $(document).ready(function() {
            $('#emailBody').summernote({
                height: 300,
            });
        });
    </script>

    <script>
        $(function () {

            const _token = document.head.querySelector("[property=csrf-token]").content;

            (function (global, $, _token) {

                /*Submit add form*/
                $('#email-template-form')
                    .parsley(global.parsleySettings.settings)
                    .on('form:success', function (formInstance) {
                        if (formInstance.isValid()) {
                            $('#preloader').show();


                            var formData = new FormData($('#email-template-form').get(0));

                            $.ajax({
                                url: route('admin-email-add'),
                                type: 'post',
                                data: formData,
                                contentType: false,
                                processData: false,
                                loadSpinner: true,
                                success: function (response) {
                                    $("#email-template-form")[0].reset();
                                    $('#preloader').hide();
                                    response.status ? bootstrapNotify.success(response.html, 'Success') : bootstrapNotify.error(response.html, 'Error');
                                },
                                error: function (response) {
                                    if (response.status == 422) {
                                        global.generateErrorMessage(formInstance, response.responseJSON.errors);
                                        bootstrapNotify.error(response.responseJSON.message, "Error");
                                    }
                                    $('#preloader').hide();
                                }
                            });
                        }
                    }).on('form:submit', function () {
                    return false;
                });
            })(window, jQuery, _token);
        });

    </script>
@endpush

