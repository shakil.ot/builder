@extends('admin.layouts.app')


@section('container')

    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Entry-->
        <div class="card card-custom">
            <div class="card-header">
                <div class="card-title">
                    <span class="card-icon">
                        <i class="flaticon2-delivery-package text-primary"></i>
                    </span>
                    <h3 class="card-label">Email Template Manage</h3>
                </div>
            </div>
            <div class="row">
                <div class="container">
                    <div style="margin:0 auto">
                        <div class="pane-section-content open">

                            <!-- END: Subheader -->
                            <div class="m-content">
                                <div class="row">
                                    <div class="col-xl-12 margin-sm-dev">
                                        <div class="m-portlet alterC">
                                            <button class="btn btn-sm btn-primary btn-green submit-file float-right mt-2 mb-2 ml-auto mr-4 addTemplate" id="addTemplateBtn">Add New Template</button>
                                            <div class="m-portlet__body list_view_map_view">
                                                <div class="tab-content quick-sends">
                                                    <div class="tab-pane active show" id="" role="tabpanel">
                                                        <div class="table-design-border-wrapper">
                                                            <div class="m-portlet__body table-responsive table-design-border mcOverflowVisible">
                                                                <table id="email-template-table" class="mt-1 table table-hover template-dataTable">
                                                                    <colgroup>
                                                                        <col span="1" style="width: 45%;">
                                                                        <col span="1" style="width: 25%;">
                                                                        <col span="1" style="width: 25%;">
                                                                    </colgroup>
                                                                    <thead>
                                                                    <tr>
                                                                        <th><span>Title</span></th>
                                                                        <th><span>Subject</span></th>
                                                                        <th><span>Action</span></th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!--end::Entry-->
    </div>


@endsection

@push('scripts')

{{--    For Email Template Datatable--}}
    <script src="{{ mix('js/email-template.js') }}"></script>
@endpush

