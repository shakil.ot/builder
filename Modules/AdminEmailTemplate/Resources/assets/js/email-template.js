$(function () {
    let _token = document.head.querySelector("[property=csrf-token]").content;
    let templateId = $('#templateId').val();
    $.extend(true, $.fn.dataTable.defaults, {
        "language": {
            "search": "",
            "searchPlaceholder": "Search",
            "sLengthMenu": "_MENU_ records",
            "paginate": {
                "first": "<<",
                "previous": '<span class="fa fa-chevron-left"></span>',
                "next": '<span class="fa fa-chevron-right"></span>',
                "last": ">>"
            },
        }
    });

    (function (global, $, _token) {

        var dataTable = $('#email-template-table').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            stateSave: false,
            "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
            pageLength: 10,

            ajax: {
                url: route('admin-email-all'),
                // data: function (data) {
                //     data.contactId = contactId;
                // },
                type: 'get',
            },
            columns: [
                {data: 'email_title', name: 'description', "orderable": true, "searchable": true, width: "30%"},
                {data: 'email_subject', name: 'created_at', "orderable": true, "searchable": false, width: "30%"},
                {data: 'action', name: 'action', "orderable": true, "searchable": false, width: "40%"}
            ]
        });



        $(document).on('click', '.addTemplate', function (e) {
            e.preventDefault();
            e.stopImmediatePropagation();
            $('#preloader').show();
            $('.preloader-text').html('Loading!!');
            let detailPopUp = global.customPopup.show({
                header: 'Add New Template',
                message: '',
                dialogSize: 'lg editContactModalAlterC',
                dialogClass: 'prevent-to-show'
            });


            detailPopUp.on('shown.bs.modal', function () {
                let modal = $(this);
                modal.find('.modal-body').html();
                $('#preloader').show();

                global.mySiteAjax({
                    url: route('admin-email-add-form'),
                    type: 'post',
                    data: {"_token": _token},
                    loadSpinner: true,
                    success: function (response) {
                        window.nbUtility.ajaxErrorHandling(response, function () {
                            modal.find('.modal-body').html(response.html);
                            modal.removeClass('prevent-to-show');
                            function sendFile(file, editor) {
                                $('#preloader').show();
                                var data;
                                data = new FormData();
                                data.append("file", file);
                                data.append("_token", _token);
                                $.ajax({
                                    data: data,
                                    type: "POST",
                                    url: route('image-upload-from-summernote'),
                                    cache: false,
                                    contentType: false,
                                    processData: false,
                                    success: function (data) {
                                        $('#preloader').hide();
                                        if (data.status == 'success') {
                                            var image = $('<img>').attr('src', data.url);
                                            editor.summernote("insertNode", image[0]);
                                        } else {
                                            bootstrapNotify.error(data.message,'error');
                                        }

                                    }
                                });
                            }
                            $("#custom-modal").css('overflow-y', 'auto');
                            Inputmask.init();
                            let elem = $('#emailBody');
                            $('#emailBody').summernote({
                                airMode: false,
                                height: 300,
                                focus: true,
                                dialogsInBody: true,
                                placeholder: "Enter Text Here... Type @ with a whitespace to get personalize tag",
                                maximumImageFileSize: 500 * 512, // 500 KB

                                callbacks: {
                                    onInit: function () {
                                        var editor = elem.next(),
                                            placeholder = editor.find(".note-placeholder");

                                        function isEditorEmpty() {
                                            var code = elem.summernote("code");
                                            return code === "<p><br></p>" || code === "";
                                        }

                                        editor.on("focusin focusout", ".note-editable", function (e) {
                                            if (isEditorEmpty()) {
                                                placeholder[e.type === "focusout" ? "show" : "show"]();
                                            }
                                        });
                                    },
                                    onImageUpload: function (files) {
                                        var editor = $(this);
                                        sendFile(files[0], editor);
                                    },
                                    onImageUploadError: function(msg){
                                        bootstrapNotify.error('FileSize too large !!!','error');
                                    }
                                },
                                hint: {
                                    mentions: ['first_name', 'last_name', 'email', 'number', 'my_first_name', 'my_last_name','my_email','my_number', 'offer_page'],
                                    match: /\B@(\w*)$/,
                                    search: function (keyword, callback) {
                                        callback($.grep(this.mentions, function (item) {
                                            return item.indexOf(keyword) == 0;
                                        }));
                                    },
                                    content: function (item) {
                                        return '[' + '[' + item + ']' + ']';
                                    }
                                },
                                toolbar: [
                                    ['style', ['highlight', 'personalize', 'bold', 'italic', 'underline', 'clear']],
                                    ['font', ['strikethrough', 'superscript', 'subscript']],
                                    ['para', ['ul', 'ol', 'paragraph']],
                                    ['table', [
                                        ['add', ['addRowDown', 'addRowUp', 'addColLeft', 'addColRight']],
                                        ['delete', ['deleteRow', 'deleteCol', 'deleteTable']],
                                    ]],
                                    ['image', [
                                        ['image', ['resizeFull', 'resizeHalf', 'resizeQuarter', 'resizeNone']],
                                        ['float', ['floatLeft', 'floatRight', 'floatNone']],
                                        ['remove', ['removeMedia']]
                                    ]],
                                    ['insert', ['link', 'picture','table']],
                                    ['height', ['height']],
                                    ['color', ['color']],
                                    ['fontsize', ['fontsize']],
                                    ['misc',['undo','redo']]
                                ],
                                buttons: {
                                    personalize: personalizeDropDown
                                }
                            });

                            let emailControls = [
                                ['FIRST_NAME', '[[first_name]]'],
                                ['LAST_NAME', '[[last_name]]'],
                                ['EMAIL', '[[email]]'],
                                ['NUMBER', '[[number]]'],
                                ['MY_FIRST_NAME', '[[my_first_name]]'],
                                ['MY_LAST_NAME', '[[my_last_name]]'],
                                ['MY_EMAIL', '[[my_email]]'],
                                ['MY_NUMBER', '[[my_number]]'],
                                ['OFFER_PAGE', '[[offer_page]]'],
                            ];

                            let personalizeDropDown = function () {
                                var ui = $.summernote.ui;
                                var keywordMap = new Map(emailControls);
                                var list = Array.from(keywordMap.keys());

                                var button = ui.buttonGroup([
                                    ui.button({
                                        contents: 'Personalize <i class="fa fa-caret-down" aria-hidden="true"></i>',
                                        tooltip: 'Personalize Tag',
                                        data: {
                                            toggle: 'dropdown'
                                        }
                                    }),
                                    ui.dropdown({
                                        items: list,
                                        callback: function (items) {
                                            $(items).find('a.dropdown-item').click(function (e) {
                                                var array_lenth = emailControls.length;
                                                var data_value = $(this).attr('data-value');
                                                var get_text = '';
                                                for (var increment = 0; increment < array_lenth; increment++) {
                                                    var key = Array.from(keywordMap.keys())[increment];
                                                    if (key == data_value) {
                                                        get_text = keywordMap.get(key);
                                                    }
                                                }
                                                $('#emailBody').summernote('editor.insertText', ' ' + get_text);
                                                e.preventDefault();
                                            })
                                        }
                                    })
                                ]);
                                return button.render();
                            }
                            // $('#preloader').hide();

                            $('#email-template-form').on('submit', function (e) {
                                e.preventDefault();
                                let request = {
                                    'title': $('#title').val(),
                                    'subject': $('#subject').val(),
                                    'body': $('#emailBody').val(),
                                    _token: _token
                                }

                                global.mySiteAjax({
                                    url: route('admin-email-add'),
                                    type: 'POST',
                                    data: request,
                                    contentType: false,
                                    processData: false,
                                    loadSpinner: true,
                                    success: function (response) {
                                        if (response.status === 'validation-error') {
                                            bootstrapNotify.error(response.html, 'error');
                                            $('#preloader').hide();
                                        } else if (response.status === 'success') {
                                            bootstrapNotify.success(response.html, 'success');
                                            modal.modal('hide');
                                            $("#email-template-table").DataTable().ajax.reload();
                                            $('#preloader').hide();

                                        } else if (response.status == 'error') {
                                            $('#preloader').hide();
                                            bootstrapNotify.error(response.html, 'error');
                                        }
                                    },
                                    error: function (jqXHR) {
                                        if (jqXHR == 'error') {
                                            bootstrapNotify.error('Unique Title required', 'error');
                                            return false;
                                        }
                                        return false;
                                        $('#preloader').hide();
                                    }

                                })

                            });

                        });
                    }
                });
                $('#preloader').hide();
                global.spinnerDialog.hide();
            });

            detailPopUp.on('hidden.bs.modal', function () {
                $(this).remove();
                $('#preloader').hide();
            });
        });

        $(document).on('click','.email-template',function () {
            let element = $(this);
            let id = element.attr('data-id');
            $.ajax({
                url: route('autofollowup.get-email-template-by-id'),
                type: "post",
                data: {id:id, _token: _token},
                success:function (response) {
                    $('#subject').val(response.subject);
                    $('#emailBody').summernote('code',response.body);

                }
            })
        });

        // View Template
        $(document).on('click', '.viewTemplate', function () {
            let templateId = $(this).attr("data-id");

            $('#preloader').show();
            $('.preloader-text').html('Incoming');
            let detailPopUp = global.customPopup.show({
                header: 'Template',
                message: '',
                dialogSize: 'lg editContactModalAlterC',
                dialogClass: 'prevent-to-show'
            });

            detailPopUp.on('shown.bs.modal', function () {
                let modal = $(this);
                global.mySiteAjax({
                    url: route('admin-email-view-form'),
                    type: 'post',
                    data: {'id': templateId, "_token": _token},
                    loadSpinner: false,
                    success: function (response) {
                        window.nbUtility.ajaxErrorHandling(response, function () {
                            modal.find('.modal-body').html(response.html);
                            $('#preloader').hide();
                            $("#custom-modal").css('overflow-y', 'auto');
                            Inputmask.init();
                            $('#emailBody').summernote({
                                height: 300,
                            });
                        });
                    }
                });
                modal.removeClass('prevent-to-show');
                $('#preloader').hide();
                global.spinnerDialog.hide();
            });

            detailPopUp.on('hidden.bs.modal', function () {
                $(this).remove();
                $('#preloader').hide();
            });
        });


        // Edit Template
        $(document).on('click', '.editTemplate', function () {
            let templateId = $(this).attr("data-id");

            $('#preloader').show();
            $('.preloader-text').html('Incoming!!');
            let detailPopUp = global.customPopup.show({
                header: 'Edit Template',
                message: '',
                dialogSize: 'lg editContactModalAlterC',
                dialogClass: 'prevent-to-show'
            });

            detailPopUp.on('shown.bs.modal', function () {
                let modal = $(this);
                global.mySiteAjax({
                    url: route('admin-email-edit-form'),
                    type: 'post',
                    data: {'id': templateId, "_token": _token},
                    loadSpinner: false,
                    success: function (response) {
                        window.nbUtility.ajaxErrorHandling(response, function () {
                            modal.find('.modal-body').html(response.html);
                            $('#preloader').hide();
                            $("#custom-modal").css('overflow-y', 'auto');
                            function sendFile(file, editor) {
                                $('#preloader').show();
                                var data;
                                data = new FormData();
                                data.append("file", file);
                                data.append("_token", _token);
                                $.ajax({
                                    data: data,
                                    type: "POST",
                                    url: route('image-upload-from-summernote'),
                                    cache: false,
                                    contentType: false,
                                    processData: false,
                                    success: function (data) {
                                        $('#preloader').hide();
                                        if (data.status == 'success') {
                                            var image = $('<img>').attr('src', data.url);
                                            editor.summernote("insertNode", image[0]);
                                        } else {
                                            bootstrapNotify.error(data.message,'error');
                                        }

                                    }
                                });
                            }
                            let elem = $('#emailBody');
                            $('#emailBody').summernote({
                                airMode: false,
                                height: 300,
                                focus: true,
                                dialogsInBody: true,
                                placeholder: "Enter Text Here... Type @ with a whitespace to get personalize tag",
                                maximumImageFileSize: 500 * 512, // 500 KB

                                callbacks: {
                                    onInit: function () {
                                        var editor = elem.next(),
                                            placeholder = editor.find(".note-placeholder");

                                        function isEditorEmpty() {
                                            var code = elem.summernote("code");
                                            return code === "<p><br></p>" || code === "";
                                        }

                                        editor.on("focusin focusout", ".note-editable", function (e) {
                                            if (isEditorEmpty()) {
                                                placeholder[e.type === "focusout" ? "show" : "show"]();
                                            }
                                        });
                                    },
                                    onImageUpload: function (files) {
                                        var editor = $(this);
                                        sendFile(files[0], editor);
                                    },
                                    onImageUploadError: function(msg){
                                        bootstrapNotify.error('FileSize too large !!!','error');
                                    }
                                },
                                hint: {
                                    mentions: ['first_name', 'last_name', 'email', 'number', 'my_first_name', 'my_last_name','my_email','my_number', 'offer_page'],
                                    match: /\B@(\w*)$/,
                                    search: function (keyword, callback) {
                                        callback($.grep(this.mentions, function (item) {
                                            return item.indexOf(keyword) == 0;
                                        }));
                                    },
                                    content: function (item) {
                                        return '[' + '[' + item + ']' + ']';
                                    }
                                },
                                toolbar: [
                                    ['style', ['highlight', 'personalize', 'bold', 'italic', 'underline', 'clear']],
                                    ['font', ['strikethrough', 'superscript', 'subscript']],
                                    ['para', ['ul', 'ol', 'paragraph']],
                                    ['table', [
                                        ['add', ['addRowDown', 'addRowUp', 'addColLeft', 'addColRight']],
                                        ['delete', ['deleteRow', 'deleteCol', 'deleteTable']],
                                    ]],
                                    ['image', [
                                        ['image', ['resizeFull', 'resizeHalf', 'resizeQuarter', 'resizeNone']],
                                        ['float', ['floatLeft', 'floatRight', 'floatNone']],
                                        ['remove', ['removeMedia']]
                                    ]],
                                    ['insert', ['link', 'picture','table']],
                                    ['height', ['height']],
                                    ['color', ['color']],
                                    ['fontsize', ['fontsize']],
                                    ['misc',['undo','redo']]
                                ],
                                buttons: {
                                    personalize: personalizeDropDown
                                }
                            });

                            let emailControls = [
                                ['FIRST_NAME', '[[first_name]]'],
                                ['LAST_NAME', '[[last_name]]'],
                                ['EMAIL', '[[email]]'],
                                ['NUMBER', '[[number]]'],
                                ['MY_FIRST_NAME', '[[my_first_name]]'],
                                ['MY_LAST_NAME', '[[my_last_name]]'],
                                ['MY_EMAIL', '[[my_email]]'],
                                ['MY_NUMBER', '[[my_number]]'],
                                ['OFFER_PAGE', '[[offer_page]]'],
                            ];

                            let personalizeDropDown = function () {
                                var ui = $.summernote.ui;
                                var keywordMap = new Map(emailControls);
                                var list = Array.from(keywordMap.keys());

                                var button = ui.buttonGroup([
                                    ui.button({
                                        contents: 'Personalize <i class="fa fa-caret-down" aria-hidden="true"></i>',
                                        tooltip: 'Personalize Tag',
                                        data: {
                                            toggle: 'dropdown'
                                        }
                                    }),
                                    ui.dropdown({
                                        items: list,
                                        callback: function (items) {
                                            $(items).find('a.dropdown-item').click(function (e) {
                                                var array_lenth = emailControls.length;
                                                var data_value = $(this).attr('data-value');
                                                var get_text = '';
                                                for (var increment = 0; increment < array_lenth; increment++) {
                                                    var key = Array.from(keywordMap.keys())[increment];
                                                    if (key == data_value) {
                                                        get_text = keywordMap.get(key);
                                                    }
                                                }
                                                $('#emailBody').summernote('editor.insertText', ' ' + get_text);
                                                e.preventDefault();
                                            })
                                        }
                                    })
                                ]);
                                return button.render();
                            }

                            $('#email-template-edit-form').on('submit', function (e) {
                                e.preventDefault();
                                let request = {
                                    'id': templateId,
                                    'title': $('#title').val(),
                                    'subject': $('#subject').val(),
                                    'body': $('#emailBody').val(),
                                }

                                global.mySiteAjax({
                                    url: route('admin-email-update'),
                                    type: 'post',
                                    data: {request: request, _token: _token},
                                    loadSpinner: true,
                                    success: function (response) {
                                        if (response.status === 'validation-error') {
                                            bootstrapNotify.error(response.html, 'error');
                                            $('#preloader').hide();
                                        } else if (response.status === 'success') {
                                            bootstrapNotify.success(response.html, 'success');
                                            modal.modal('hide');
                                            $("#email-template-table").DataTable().ajax.reload();
                                            $('#preloader').hide();

                                        } else if (response.status == 'error') {
                                            $('#preloader').hide();
                                            bootstrapNotify.error(response.html, 'error');
                                        }


                                    },
                                    error: function () {
                                        $('#preloader').hide();
                                    }

                                })
                            });
                        });
                    }
                });
                modal.removeClass('prevent-to-show');
                $('#preloader').hide();
                global.spinnerDialog.hide();
            });

            detailPopUp.on('hidden.bs.modal', function () {
                $(this).remove();
                $('#preloader').hide();
            });
        });

        $(document).on('click','.email-template',function () {
            let element = $(this);
            let id = element.attr('data-id');
            $.ajax({
                url: route('autofollowup.get-email-template-by-id'),
                type: "post",
                data: {id:id, _token: _token},
                success:function (response) {
                    $('#subject').val(response.subject);
                    $('#emailBody').summernote('code',response.body);

                }
            })
        });


        // Delete Template
        $(document).on('click', '.deleteTemplate', function () {
            let templateId = $(this).attr('data-id');
            Swal.fire({
                title: 'Are you sure?',
                html: "<b class='text-danger'> Sure to Delete this Template?</b>",
                type: 'warning',
                confirmButtonText: 'Yes, delete the template',
                confirmButtonClass: 'btn btn-danger',
                showCancelButton: true,
                cancelButtonText: 'No, keep it!',
                cancelButtonClass: 'btn btn-green'
            }).then(function (result) {
                if (result.value) {

                    global.mySiteAjax({
                        url: route('admin-email-delete'),
                        type: 'post',
                        data: {templateId: templateId, _token: _token},
                        loadSpinner: true,

                        beforeSend: function () {
                            $('#preloader').show();
                        },
                        complete: function (xhr, stat) {
                            $('#preloader').hide();
                        },
                        success: function (response) {
                            if (response.status == 'success') {
                                toastr.success(response.html, "Success");
                                $("#email-template-table").DataTable().ajax.reload();

                            } else if (response.status == 'error') {
                                toastr.error(response.html, "Error");
                            }

                        }
                    });
                } else {
                    $('#preloader').hide();
                }
            });
        });

    })(window, jQuery, _token);
});
