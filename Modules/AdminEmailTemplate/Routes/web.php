<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('admin/emailtemplate')->name('admin-email-')->middleware('admin')->group(function() {
    Route::get('/', 'AdminEmailTemplateController@index')->name('index');
    Route::get('/get/templates', 'AdminEmailTemplateController@listEmailTemplate')->name('all');
    Route::post('/update/template', 'AdminEmailTemplateController@updateEmailTemplate')->name('update');
    Route::post('/delete/template', 'AdminEmailTemplateController@deleteEmailTemplate')->name('delete');
    Route::post('/', 'AdminEmailTemplateController@addEmail')->name('add');
    Route::post('/add/form', 'AdminEmailTemplateController@getEmailTemplateAddForm')->name('add-form');
    Route::post('/edit/form', 'AdminEmailTemplateController@getEmailTemplateEditForm')->name('edit-form');
    Route::post('/view/form', 'AdminEmailTemplateController@getTemplateViewForm')->name('view-form');
});
