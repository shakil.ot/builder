<?php

namespace Modules\UserPackage\Http\Controllers;

use App\Contracts\Service\UserContact;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Package\Contracts\Service\PackageContact;
use Modules\Package\Contracts\Service\UserCurrentPackageContact;
use Modules\Package\Entities\Package;
use Modules\Package\Entities\UserCurrentPackage;

class UserPackageController extends Controller
{
    /**
     * @var UserContact
     */
    private $userService;
    private $packageService;
    private $userCurrentPackageService;

    /**
     * UserPackageController constructor.
     * @param UserContact $userService
     */
    public function __construct(UserContact $userService,
                                PackageContact $packageService,
                                UserCurrentPackageContact $userCurrentPackageService)
    {
        $this->userService = $userService;
        $this->packageService = $packageService;
        $this->userCurrentPackageService = $userCurrentPackageService;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $data['userData'] = $this->userService->getUserPackageInformationByUserId(auth()->id());

        if (is_null($data['userData'])){
            return view('userpackage::package-not-found');
        }

        return view('userpackage::index')->with($data);
    }

    public function showPackages()
    {
        $userCountry = $this->userService->getCountryByUserId(Auth::id());
        if ($userCountry['country'] == User::COUNTRY_BANGLADESH) {
            $data['country'] = "Bangladesh";
            $data['packages'] = $this->packageService->getAllPaidPackageForBangladesh();
        } else {
            $data['country'] = "International";
            $data['packages'] = $this->packageService->getAllPaidPackageForInternational();
        }
        return view('userpackage::show')->with($data);
    }

    public function buyPackages($packageId, $isRenew = 0, $billingType = UserCurrentPackage::BILLING_PERIOD_MONTHLY)
    {
        $billingType = base64_decode($billingType);
        $userCountry = $this->userService->getCountryByUserId(Auth::id());
        if ($userCountry['country'] == User::COUNTRY_BANGLADESH) {
            $data['country'] = "Bangladesh";
        } else {
            $data['country'] = "International";
        }

        $data['package'] = $this->packageService->getByPackageId(base64_decode($packageId));
        $data['isRenew'] = $isRenew;
        $data['billingType'] =$billingType;
        if ($isRenew == 1 && $data['country'] != "Bangladesh") {
            return view('userpackage::checkout_with_old_card')->with($data);

        } else {
            return view('userpackage::checkout')->with($data);

        }

    }

    public function packageRenew()
    {
        $userId = Auth::id();
        $packages = [];
        $userCurrentPackage = $this->userCurrentPackageService->getUserCurrentPackageInformationByUserId($userId);
        if(!$userCurrentPackage){
            return redirect()->route('dashboard');
        }
        if ((strtotime(date('Y-m-d H:i:s')) < strtotime($userCurrentPackage->expire_date))) {
            return redirect()->route('dashboard');
        }
        $isCurrentPackageFree = 0;
        if($userCurrentPackage->is_use_trial_or_paid == UserCurrentPackage::IS_FREE_PERIOD && $userCurrentPackage->subscription_fee == 0){
            $isCurrentPackageFree = 1;
        }
        $subscriptionFee = $userCurrentPackage->packages->subscription_fee;
        $country = Auth::user()->country == User::COUNTRY_BANGLADESH ? 'Bangladesh' : 'International';
        $packageCreatedFrom = Auth::user()->package_created_from;


        if ($packageCreatedFrom == User::PACKAGE_CREATED_FROM_DIRECT_REGISTRATION) {
            if ($country == "Bangladesh") {
                $packages = $this->packageService->getAllUpdatedPaidPackageForBangladesh($subscriptionFee);
            } else {
                $packages = $this->packageService->getAllUpdatedPaidPackageForInternational($subscriptionFee);
            }
        }
        return view('userpackage::packageRenew')
            ->with('userCurrentPackage', $userCurrentPackage)
            ->with('isCurrentPackageFree', $isCurrentPackageFree)
            ->with('country', $country)
            ->with('packages', $packages);

    }



}
