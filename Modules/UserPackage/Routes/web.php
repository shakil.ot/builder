<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('user')->name('user.')->middleware(['auth', 'web'])->group(function(){
    Route::get('package', 'UserPackageController@index')->name('package');
    Route::get('package/all', 'UserPackageController@showPackages')->name('show-package');
    Route::get('package/checkout/{packageId}/{isRenew}', 'UserPackageController@buyPackages')->name('package-renew-with-parameter');
    Route::get('package/checkout/{packageId}/{isRenew}/{billingType}', 'UserPackageController@buyPackages')->name('buy-package');
    Route::get('package/renew', 'UserPackageController@packageRenew')->name('package-renew');
    Route::post('coupon', 'UserPackageController@registrationOrRenewByCoupon')->name('register-or-renew-by-coupon-code');

});
