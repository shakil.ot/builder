@extends('user.layout.app', ['menu' => 'achievement'])

@section('title', 'Dashboard')

@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Card-->
                <div class="card">
                    <!-- begin: custom background-->
                    <div class="position-absolute w-100 h-50 rounded-card-top"></div>
                    <!-- end: custom background-->
                    <div class="card-body position-relative">

                        <!-- begin: Tabs-->
                        <!-- end: Tabs-->
                        <div class="card-body bg-white col-11 col-lg-12 col-xxl-10 mx-auto">
                            <div class="row">
                                <!-- begin: Pricing-->
                                <div class="col-md-12">

                                    <div class="card card-custom card-stretch gutter-b">
                                        <div class="card-body d-flex p-0">
                                            <div class="flex-grow-1 p-20 pb-40 card-rounded flex-grow-1 bgi-no-repeat" style="background-color: #1B283F; background-position: calc(100% + 0.5rem) bottom; background-size: 50% auto; background-image:  url({{ asset('assets/theme/media/svg/humans/custom-10.svg')  }})">

                                                <p class="text-muted pb-5 font-size-h5" >
                                                    <br>
                                                    <br>
                                                    <br>
                                                </p>
                                                <h2 class="text-white pb-5 font-weight-bolder">No Package found</h2>
                                                <br>
                                                <br>
                                                <br>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!-- end: Pricing-->
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Card-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>

@endsection

{{--@section('script')--}}
{{--    <script src="{{ asset('js/user-achievement.js') }}"></script>--}}
{{--@endsection--}}
