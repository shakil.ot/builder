@extends('user.layout.app', ['menu' => 'category'])

@section('css')
<style>
    .checkout__item {
        background: #fff;
        /* border-radius: 10px; */
        padding: 20px 30px;
        border-radius: 4px;
        box-shadow: 13px 13px 0px 0px rgb(129 196 77 / 0.7);
    }

    .action-submit,
    .action-submit-to{
        width: 100%;
        background: #4fbd43 !important;
        border-color: #4fbd43 !important;
        border-radius: 0.42rem;
        font-weight: 700 !important;
        font-size: 18px !important;
    }

    td {
        font-size: 16px;
    }

    .checkout__table .first__td small {
        font-weight: 500;
        text-transform: capitalize;
    }

    .nav.nav-tabs.nav-tabs-line .nav-link:hover:not(.disabled), .nav.nav-tabs.nav-tabs-line .nav-link.active, .nav.nav-tabs.nav-tabs-line .show > .nav-link {
        border-color: #10BC41;
        color: #10BC41;
    }

    table.border-custom {
        border-collapse: collapse;

    }

    table.border-custom tr {
        border: 1px solid #dddddd42;
    }

    .discount-prize {
        color: red;
    }

    .subtotal {
        font-size: 14px;
    }
    .stripe-badge-img {
        width: 90%;
        margin: 15px auto 0;
        display: block;
    }
    .error-topbar{
        color: red;
    }
</style>
@endsection

@section('content')
    <?php
    $subscription_fee = $package->subscription_fee;
        if ($billingType == \Modules\Package\Entities\UserCurrentPackage::BILLING_PERIOD_YEARLY) {
            $subscription_fee = intval(($package->yearly_discount_price * 360) / $package->life_line);
            $normal_price = intval(($package->subscription_fee * 360) / $package->life_line);
            $discount_price = $normal_price-$subscription_fee;
        }
    ?>
    <div class="pt-4 pt-sm-3 bg-light bg-wave-white-short">
        <div class="container">
            <div class="row justify-content-center pt-4 pb-4 mb-3">
                <div class="col-11 col-sm-9 col-md-7 col-lg-5 col-xl-4">
                    <div class="white-box-shadow top-white-sm mt-4 pb-4">
                        <div class="px-4 checkout__item">
                            <input type="hidden" name="isRenew" value="{{$isRenew}}" id="isRenew">
                            <input type="hidden" name="billingType" value="{{$billingType}}" id="billingType">
                            <h1 class="text-center pt-4 pb-3">Checkout</h1>

                            <ul class="nav nav-tabs nav-bold nav-tabs-line mb-3">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#existing_card">Existing Card</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#new_card">Add New Card</a>
                                </li>
                            </ul>

                            <div class="tab-content">
                            <div id="existing_card" class="tab-pane show fade active">
                                <div class="">
                                    <table class="w-100 checkout__table">
                                        @if($billingType == \Modules\Package\Entities\UserCurrentPackage::BILLING_PERIOD_YEARLY && $package->yearly_discount_percentage != 0)
                                            <tbody>
                                            <tr>
                                                <td class="first__td">
                                                    <small>{{ $package->name }}</small>
                                                </td>
                                                <td class="text-right">
                                                    <small>
                                                        <del>$ {{ $normal_price }}</del>
                                                    </small>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td class="first__td">
                                                    <small>Discount Percentage</small>
                                                </td>
                                                <td class="text-right">
                                                    <small>
                                                        {{ $package->yearly_discount_percentage }}%
                                                    </small>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="first__td">
                                                    <small>Discount: </small>
                                                </td>
                                                <td class="text-right discount-prize">
                                                    <small>$ {{$discount_price}}</small>
                                                </td>
                                            </tr>
                                            <tr>

                                                <td class="text-right" colspan="2">
                                                    <small class="subtotal"><b>Subtotal:</b> $ {{$subscription_fee}}</small>
                                                </td>
                                            </tr>
                                            </tbody>

                                        @else
                                            <tbody>
                                            <tr>
                                                <td class="first__td">
                                                    <small>{{ $package->name }}</small>
                                                </td>
                                                <td class="text-right">
                                                    <small>$ {{ $package->subscription_fee }}</small>
                                                </td>
                                            </tr>
                                            </tbody>
                                            @endif


                                    </table>
                                    <div class="hr-white my-2"></div>
                                    <p class="line-1 pt-1"><small class="text-secondary">{{ env('SITE_NAME') }} will be charged on regular package life line basis..<br>Cancel your subscription anytime.</small></p>
                                </div>
                                <div class="text-center my-3">
                                <button class="btn btn-primary btn-lg btn-arrow make-payment action-submit-to" type="button">
                                    <input type="hidden" name="isExistCard" value="1" class="isExistCard">
                                    Make Payment
                                </button>
                                </div>
                            </div>
                            <div id="new_card" class="tab-pane fade">
                                <form method="post" id="payment-form" class="form onsubmit-disable" data-stripe-publishable-key="{{ env('STRIPE_PUB_KEY') }}" autoComplete="off">
                                    @csrf
                                    <input type="hidden" name="isExistCard" value="0">

                                    <input type="hidden" name="price" class="package_id" value="{{ $package->id }}">
                                    <div>
                                        <table class="w-100 checkout__table">
                                            @if($billingType == \Modules\Package\Entities\UserCurrentPackage::BILLING_PERIOD_YEARLY && $package->yearly_discount_percentage != 0)
                                                <tbody>
                                                <tr>
                                                    <td class="first__td">
                                                        <small>{{ $package->name }}</small>
                                                    </td>
                                                    <td class="text-right">
                                                        <small>
                                                            <del>$ {{ $normal_price }}</del>
                                                        </small>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td class="first__td">
                                                        <small>Discount Percentage</small>
                                                    </td>
                                                    <td class="text-right">
                                                        <small>
                                                            {{ $package->yearly_discount_percentage }}%
                                                        </small>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="first__td">
                                                        <small>Discount: </small>
                                                    </td>
                                                    <td class="text-right discount-prize">
                                                        <small>$ {{$discount_price}}</small>
                                                    </td>
                                                </tr>
                                                <tr>

                                                    <td class="text-right" colspan="2">
                                                        <small class="subtotal"><b>Subtotal:</b> $ {{$subscription_fee}}</small>
                                                    </td>
                                                </tr>
                                                </tbody>

                                            @else
                                                <tbody>
                                                <tr>
                                                    <td class="first__td">
                                                        <small>{{ $package->name }}</small>
                                                    </td>
                                                    <td class="text-right">
                                                        <small>$ {{ $package->subscription_fee }}</small>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            @endif
                                        </table>
                                        <div class="hr-white my-2"></div>
                                        <p class="line-1 pt-1"><small class="text-secondary">{{ env('SITE_NAME') }} will be charged on regular package life line basis.<br>Cancel your subscription anytime.</small></p>
                                    </div>

                                    <script src='https://js.stripe.com/v3/' type='text/javascript'></script>
                                    <div class="form-group">
                                        <label>
                                            Enter card details
                                        </label>
                                        <div class="payment-block-top-bar">
                                            <div id="card-topbar" class="form-control"></div>
                                            <div id="card-error-topbar" class="error-topbar"></div>
                                            <div class="fv-plugins-message-container"></div>
                                        </div>
                                    </div>

                                    <!-- Used to display form errors -->
                                    <div id="card-errors" role="alert"></div>

                                    <div class="text-center my-3">
                                        <button class="btn btn-primary btn-lg btn-arrow action-submit" type="button">
                                            Make Payment
                                        </button>
                                    </div>
                                </form>
                            </div>
                                <img class="stripe-badge-img" src="{{asset('assets/stripe-badge.png')}}" alt="">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(function () {

            const _token = document.head.querySelector("[property=csrf-token]").content;;

            (function (global, $, _token) {

                let stripe = null;
                let cardElement = null;
                setStripeForm();

                function setStripeForm(){
                    let publishKey = $("#payment-form").data('stripe-publishable-key');
                    stripe = Stripe(publishKey);
                    let elements = stripe.elements();
                    let style = {
                        base: {
                            color: '#303238',
                            fontSize: '16px',
                            fontSmoothing: 'antialiased',
                            '::placeholder': {
                                color: '#ccc',
                            },
                        },
                        invalid: {
                            color: '#e5424d',
                            ':focus': {
                                color: '#303238',
                            },
                        },
                    };
                    cardElement = elements.create('card', {hidePostalCode: true, style: style});
                    cardElement.mount('#card-topbar');

                    cardElement.addEventListener('change', function (event) {
                        let displayError = document.getElementById('card-error-topbar');
                        if (event.error) {
                            displayError.textContent = event.error.message;
                        } else {
                            displayError.textContent = '';
                        }
                    });
                }

                var profileInfo = {};

                $('.action-submit').click(function() {
                    profileInfo._token = _token;
                    stripe.createToken(cardElement).then(function(result) {
                        if (result.error) {
                            // Inform the user if there was an error
                            var errorElement = document.getElementById('card-error-topbar');
                            errorElement.textContent = result.error.message;
                            bootstrapNotify.error(result.error.message);
                        } else {
                            // Send the token to your server
                            profileInfo.packageId = $('.package_id').val();
                            profileInfo.cardNumber = result.token.card.last4;
                            profileInfo.stripeToken = result.token.id;
                            profileInfo.isRenew = $('#isRenew').val();
                            profileInfo.billingType = $('#billingType').val();

                            $('#preloader').show();
                            $.ajax({
                                url: route('user.payment.submit'),
                                type: 'post',
                                data: profileInfo,
                                loadSpinner: true,
                                success: function (response) {
                                    $('#preloader').hide();
                                    if(response.status){
                                        bootstrapNotify.success(response.message, 'Success');
                                        setTimeout(function(){
                                            window.location.href = route('dashboard');
                                        }, 1000);
                                    } else {
                                        bootstrapNotify.error(response.message, 'Error');
                                    }
                                },
                                error: function (response) {
                                    $('#preloader').hide();
                                    if (response.status == 419) {
                                        bootstrapNotify.error(response.responseJSON.message+' Please try again.', 'Error');
                                    } else {
                                        bootstrapNotify.error(response.responseJSON.message, 'Error');
                                    }
                                }
                            });
                        }
                    });
                });

                $('.make-payment').click(function () {
                    $('#preloader').show();
                    profileInfo._token = _token;
                    profileInfo.packageId = $('.package_id').val();
                    profileInfo.isExistCard = $('.isExistCard').val();
                    profileInfo.isRenew = $('#isRenew').val();
                    profileInfo.billingType = $('#billingType').val();
                    $.ajax({
                        url: route('user.payment.existing_card_submit'),
                        type: 'post',
                        data: profileInfo,
                        loadSpinner: true,
                        success: function (response) {
                            $('#preloader').hide();
                            if(response.status){
                                bootstrapNotify.success(response.message, 'Success');
                                setTimeout(function(){
                                    window.location.href = route('dashboard');
                                }, 1000);
                            } else {
                                bootstrapNotify.error(response.message, 'Error');
                            }
                        },
                        error: function (response) {
                            $('#preloader').hide();
                            if (response.status == 419) {
                                bootstrapNotify.error(response.responseJSON.message+' Please try again.', 'Error');
                            } else {
                                bootstrapNotify.error(response.responseJSON.message, 'Error');
                            }
                        }
                    });
                })
            })(window, jQuery, _token);
        });

    </script>
@endsection
