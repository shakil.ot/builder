@extends('user.layout.app', ['menu' => 'packageRenew'])

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css"  />
    <style>
        .package-renew-header{
            margin: 14px;
        }
    </style>
@endsection

@section('content')
    <div class="pb-4 pt-4 bg-light bg-wave-white">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-lg-7">
                    @if(Session::has('error'))
                        <div class="alert alert-danger alert-dismissible fade show mt-3" role="alert">
                            <strong>Oh Snap!</strong> {{ Session::get('error') }}
                            <button type="button" class="close p-1" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                </div>
                <div class="col-12 col-lg-7 text-center package-renew-header">
                    <h1>Package Renew</h1>
                </div>
            </div>

        </div>
        <div class="container">
            <!--begin::Dashboard-->
            <div class="card card-custom gutter-b">
                <div class="card-header">
                    <div class="card-title">
											<span class="card-icon">
												<i class="flaticon2-chart text-primary"></i>
											</span>
                        <h3 class="card-label">Buy package to get started</h3>
                    </div>
                </div>

                <div class="card-body">
                    <div class="row justify-content-center">
                    @if($isCurrentPackageFree == 0)
                        @if($userCurrentPackage->life_line < 730 )
                                <div class="col-md-4 col-xxl-3 my-10 border-bottom">
                                    <div class="pb-15 px-5 text-center">
                                        <!--begin::Icon-->
                                        <div class="d-flex flex-center position-relative">
                                            <h1>{{ $userCurrentPackage->packages->name }}</h1>
                                        </div>
                                        <!--end::Icon-->
                                        <!--begin::Content-->
                                        <span class="font-size-h1 d-block font-weight-boldest text-dark-75 py-2">{{ $userCurrentPackage->packages->subscription_fee }}
                                                <sup class="font-size-h3 font-weight-normal pl-1">$</sup>
                                            </span>
                                        <p class="mb-15 d-flex flex-column">
                                            <span>{{ $userCurrentPackage->packages->description }}</span>
                                        </p>

                                        <div class="dropdown">
                                            <button class="btn btn-green dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Purchase Now
                                            </button>
                                            <div class="dropdown-menu p-0" aria-labelledby="dropdownMenuButton">
                                                <div class="d-flex justify-content-between text-center">
                                                    @if($userCurrentPackage->packages->life_line == 30)
                                                        <a href="{{ route('user.buy-package', ['packageId' => base64_encode($userCurrentPackage->packages->id),'isRenew'=>1, 'billingType' => base64_encode(\Modules\Package\Entities\UserCurrentPackage::BILLING_PERIOD_YEARLY)]) }}" class="px-10 py-3 bg-hover-primary text-hover-white" data-package_id="{{ $userCurrentPackage->packages->id }}" data-billing_period="{{ \Modules\Package\Entities\UserCurrentPackage::BILLING_PERIOD_YEARLY }}">
                                                            <div class="font-weight-bolder font-size-sm">Yearly</div>
                                                            <div class="font-weight-bolder font-size-h5">
                                                                <span class="font-weight-bold">$</span>{{ $userCurrentPackage->packages->yearly_discount_price }}/mo<sup class="text-danger">{{$userCurrentPackage->packages->yearly_discount_percentage > 0 ? "-".$userCurrentPackage->packages->yearly_discount_percentage."%" : ''}}</sup>
                                                            </div>
                                                        </a>
                                                        <a href="{{ route('user.buy-package', ['packageId' => base64_encode($userCurrentPackage->packages->id),'isRenew'=>1, 'billingType' => base64_encode(\Modules\Package\Entities\UserCurrentPackage::BILLING_PERIOD_MONTHLY)]) }}" class="px-10 py-3 bg-green text-white" data-package_id="{{ $userCurrentPackage->packages->id }}" data-billing_period="{{ \Modules\Package\Entities\UserCurrentPackage::BILLING_PERIOD_MONTHLY }}">
                                                            <div class="font-weight-bolder font-size-sm">Monthly</div>
                                                            <div class="font-weight-bolder font-size-h5">
                                                                <span class="font-weight-bold">$</span>{{ $userCurrentPackage->packages->subscription_fee }}/mo
                                                            </div>
                                                        </a>
                                                    @else
                                                        <a href="{{ route('user.buy-package', ['packageId' => base64_encode($userCurrentPackage->packages->id),'isRenew'=>1, 'billingType' => base64_encode(\Modules\Package\Entities\UserCurrentPackage::BILLING_PERIOD_DAYWISE)]) }}" class="px-10 py-3 bg-green text-white" data-package_id="{{ $userCurrentPackage->packages->id }}" data-billing_period="{{ \Modules\Package\Entities\UserCurrentPackage::BILLING_PERIOD_MONTHLY }}">
                                                            <div class="font-weight-bolder font-size-sm">{{\App\Helper\UtilityHelper::compoundingPeriod($userCurrentPackage->packages->life_line)}}</div>
                                                            <div class="font-weight-bolder font-size-h5">
                                                                <span class="font-weight-bold">$</span>{{ $userCurrentPackage->packages->subscription_fee }}/{{\App\Helper\UtilityHelper::compoundingPeriod($userCurrentPackage->packages->life_line)}}
                                                            </div>
                                                        </a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <!--end::Content-->
                                    </div>
                                </div>

                            @endif

                        @foreach($packages as $key => $package)
                        <!--begin: Pricing-->
                            <div class="col-md-4 col-xxl-3 my-10 {{ $packages->count() > 3 ? 'border-bottom' : '' }} {{ $key + 1 != $packages->count() ? 'border-right' : '' }}">
                                <div class="pb-15 px-5 text-center">
                                    <!--begin::Icon-->
                                    <div class="d-flex flex-center position-relative">
                                        <h1>{{ $package->name }}</h1>
                                    </div>
                                    <!--end::Icon-->
                                    <!--begin::Content-->
                                    <span class="font-size-h1 d-block font-weight-boldest text-dark-75 py-2">{{ $package->subscription_fee }}
                                                <sup class="font-size-h3 font-weight-normal pl-1">$</sup>
                                            </span>
                                    <p class="mb-15 d-flex flex-column">
                                        <span>{{ $package->description }}</span>
                                    </p>

                                    <div class="dropdown">
                                        <button class="btn btn-green dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Purchase Now
                                        </button>
                                        <div class="dropdown-menu p-0" aria-labelledby="dropdownMenuButton">
                                            <div class="d-flex justify-content-between text-center">
                                                @if($package->life_line == 30)
                                                    <a href="{{ route('user.buy-package', ['packageId' => base64_encode($package->id),'isRenew'=>1, 'billingType' => base64_encode(\Modules\Package\Entities\UserCurrentPackage::BILLING_PERIOD_YEARLY)]) }}" class="px-10 py-3 bg-hover-primary text-hover-white" data-package_id="{{ $package->id }}" data-billing_period="{{ \Modules\Package\Entities\UserCurrentPackage::BILLING_PERIOD_YEARLY }}">
                                                        <div class="font-weight-bolder font-size-sm">Yearly</div>
                                                        <div class="font-weight-bolder font-size-h5">
                                                            <span class="font-weight-bold">$</span>{{ $package->yearly_discount_price }}/mo<sup class="text-danger">{{$userCurrentPackage->packages->yearly_discount_percentage > 0 ? "-".$userCurrentPackage->packages->yearly_discount_percentage."%" : ''}}</sup>
                                                        </div>
                                                    </a>
                                                    <a href="{{ route('user.buy-package', ['packageId' => base64_encode($package->id),'isRenew'=>1, 'billingType' => base64_encode(\Modules\Package\Entities\UserCurrentPackage::BILLING_PERIOD_MONTHLY)]) }}" class="px-10 py-3 bg-green text-white" data-package_id="{{ $package->id }}" data-billing_period="{{ \Modules\Package\Entities\UserCurrentPackage::BILLING_PERIOD_MONTHLY }}">
                                                        <div class="font-weight-bolder font-size-sm">Monthly</div>
                                                        <div class="font-weight-bolder font-size-h5">
                                                            <span class="font-weight-bold">$</span>{{ $package->subscription_fee }}/mo
                                                        </div>
                                                    </a>
                                                @else
                                                    <a href="{{ route('user.buy-package', ['packageId' => base64_encode($package->id),'isRenew'=>1, 'billingType' => base64_encode(\Modules\Package\Entities\UserCurrentPackage::BILLING_PERIOD_DAYWISE)]) }}" class="px-10 py-3 bg-green text-white" data-package_id="{{ $package->id }}" data-billing_period="{{ \Modules\Package\Entities\UserCurrentPackage::BILLING_PERIOD_MONTHLY }}">
                                                        <div class="font-weight-bolder font-size-sm">{{\App\Helper\UtilityHelper::compoundingPeriod($package->life_line)}}</div>
                                                        <div class="font-weight-bolder font-size-h5">
                                                            <span class="font-weight-bold">$</span>{{ $package->subscription_fee }}/{{\App\Helper\UtilityHelper::compoundingPeriod($package->life_line)}}
                                                        </div>
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Content-->
                                </div>
                            </div>
                            <!--end: Pricing-->
                        @endforeach
                        @else
                            <div class="alert alert-warning" role="alert">
                                Your free package has expired. Please contact with administrator.
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <!--end::Dashboard-->
        </div>


    </div>
@endsection

@section('content')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" ></script>
    <script>
        $(document).on('click', '.coupon_button' ,  function () {
            if ($('#coupon_get').val() == '') {
                toastr.error('Please Enter a coupon ', "Error");
                return false;
            }
            $('#preloader').show();
            $.ajax({
                url: route('user.register-or-renew-by-coupon-code'),
                type: 'post',
                data: {
                    coupon: $('#coupon_get').val(),
                    using_for : 'renew',
                    _token: '{{ csrf_token() }}'
                },
                success: function (response) {

                    $('#preloader').hide();
                    if (response.status == 'success') {
                        toastr.success(response.html, "Success");
                        window.location.href = '{{ url("/") }}';

                    } else if (response.status == 'error') {
                        toastr.error(response.html, "Error");
                    }
                }
            });

        });

    </script>

@endsection
