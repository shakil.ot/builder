<?php
/**
 * User: Rafiqul Islam
 * Date: 7/8/20
 * Time: 6:52 PM
 */

namespace Modules\Onboarding\Contracts\Services;


interface OnboardContact
{
    public function setOnboardStep($user_id, $current_step);

    public function completeOnboarding($user_id);

    public function importCategoryData($category);
}
