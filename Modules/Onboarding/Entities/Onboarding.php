<?php

namespace Modules\Onboarding\Entities;

class Onboarding
{
    const PROFILE_SETTING = 1;
    const SUBSCRIPTION_SETTING = 2;
    const DOWNLOAD_INSTRUCTION_SETTING = 3;
    const OFFER_SETTING = 4;
    const AUTO_FOLLOWUP_SETTING = 5;
    const E_BOOK = 6;
    const THANK_YOU_SETTING = 7;
    const TIMEZONE_SETTING = 8;
    const COMPLETE = null;

    const INSTRUCTION_TO_NEXT = 1;
    const INSTRUCTION_TO_PREVIOUS = 2;

    const ONBOARDING_COMPLETE = 0;

    public static $getAllStep = [
        self::PROFILE_SETTING => "Profile Setting",
        self::SUBSCRIPTION_SETTING => 'SUBSCRIPTION',
        self::DOWNLOAD_INSTRUCTION_SETTING => 'DOWNLOAD INSTRUCTION',
        self::OFFER_SETTING => 'OFFER',
        self::AUTO_FOLLOWUP_SETTING => 'AUTO FOLLOWUP',
//        self::THANK_YOU_SETTING => 'THANK YOU',
        self::E_BOOK => 'E BOOK',

    ];

    public static $getWithIndex = [
        self::PROFILE_SETTING,
        self::SUBSCRIPTION_SETTING,
        self::DOWNLOAD_INSTRUCTION_SETTING,
        self::OFFER_SETTING,
        self::AUTO_FOLLOWUP_SETTING,
        self::E_BOOK,
        self::COMPLETE,

    ];

    public static $nonMajorSteps = [
        self::PROFILE_SETTING => true,
        self::SUBSCRIPTION_SETTING => true,
        self::DOWNLOAD_INSTRUCTION_SETTING => true,
        self::OFFER_SETTING => true,
        self::AUTO_FOLLOWUP_SETTING => true,
        self::E_BOOK => true,
        self::THANK_YOU_SETTING => true,

    ];

    public static $needAutoSkip = [
        self::PROFILE_SETTING => true,
        self::SUBSCRIPTION_SETTING => false,
        self::DOWNLOAD_INSTRUCTION_SETTING => false,
        self::OFFER_SETTING => false,
        self::AUTO_FOLLOWUP_SETTING => false,
        self::E_BOOK => false,
        self::THANK_YOU_SETTING => false,
    ];

}
