<?php
/**
 * User: Rafiqul Islam
 * Date: 7/8/20
 * Time: 6:52 PM
 */

namespace Modules\Onboarding\Http\Services;


use App\Contracts\Repository\UserRepository;
use Modules\AdminCategory\Contracts\Service\AdminCategoryContact;
use Modules\Onboarding\Contracts\Services\OnboardContact;
use Modules\Onboarding\Entities\Onboarding;

class OnboardService implements OnboardContact
{
    /**
     * @var UserRepository
     */
    private $userRepo;

    /**
     * OnboardService constructor.
     * @param UserRepository $userRepo
     */
    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    public function setOnboardStep($user_id, $current_step)
    {
        return $this->userRepo->setOnboardStep($user_id, $current_step);
    }

    public function completeOnboarding($user_id)
    {
        return $this->userRepo->update(['journey_mode' => Onboarding::ONBOARDING_COMPLETE], $user_id);
    }

    public function importCategoryData($category_id)
    {
        $this->adminCategoryService->getCategoryById($category_id);
    }
}
