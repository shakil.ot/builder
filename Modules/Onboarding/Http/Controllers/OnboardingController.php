<?php

namespace Modules\Onboarding\Http\Controllers;

use App\Contracts\Service\UserContact;
use App\Models\UserSetting;
use App\User;
use DateTimeZone;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\AdminEbook\Contracts\Service\AdminEbookTemplateContact;
use Modules\AdminEmailTemplate\Contracts\Service\EmailTemplateContact;
use Modules\AdminWebBuilder\Contracts\Service\AdminWebTemplateContact;
use Modules\AdminWebBuilder\Entities\AdminWebTemplate;
use Modules\Onboarding\Contracts\Services\OnboardContact;
use Modules\Onboarding\Entities\Onboarding;
use Modules\WebBuilder\Contracts\Services\WebBuilderContact;

class OnboardingController extends Controller
{
    /**
     * @var UserContact
     */
    private $userService;
    /**
     * @var OnboardContact
     */
    private $onboardService;
    /**
     * @var AdminWebTemplateContact
     */
    private $adminWebTemplateService;
    /**
     * @var WebBuilderContact
     */
    private $webBuilderService;

    private $adminEbookTemplateService;

    private $emailTemplateService;

    /**
     * OnboardingController constructor.
     * @param UserContact $userService
     * @param OnboardContact $onboardService
     * @param WebBuilderContact $webBuilderService
     * @param AdminEbookTemplateContact $adminEbookTemplateService
     * @param EmailTemplateContact $emailTemplateService
     * @param AdminWebTemplateContact $adminWebTemplateService
     */
    public function __construct(UserContact $userService,
                                OnboardContact $onboardService,
                                WebBuilderContact $webBuilderService,
                                AdminEbookTemplateContact $adminEbookTemplateService,
                                EmailTemplateContact $emailTemplateService,
                                AdminWebTemplateContact $adminWebTemplateService)
    {
        $this->userService = $userService;
        $this->onboardService = $onboardService;
        $this->adminWebTemplateService = $adminWebTemplateService;
        $this->webBuilderService = $webBuilderService;
        $this->adminEbookTemplateService = $adminEbookTemplateService;
        $this->emailTemplateService = $emailTemplateService;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $user = auth()->user();
        $maxStep = $user->journey_mode_last_landing_step;
        $email = $user->email;
        $user_id = $user->id;

        return view('onboarding::index')->with([
            'user_id' => $user_id,
            'maxStep' => $maxStep,
            'email' => $email
        ]);

    }

    public function onboardingGetForm(Request $request)
    {

        $user_info = auth()->user();
        $current_step = $this->getCurrentStep($request->get('step_number'), $user_info, $request->get('instruction'));
        $max_step = count(Onboarding::$getWithIndex);


        $this->onboardService->setOnboardStep($user_info->id, (int)($current_step));

        //If finished
        if ($current_step >= $max_step || $user_info->journey_mode == '0') {
            $this->completeOnboarding($user_info);
            return [
                'html' => '',
                'status' => 'finished'
            ];
        }

        $currentData = $this->getCurrentData($current_step, $user_info->id);
        $currentData['current_step'] = $current_step;
        $currentData['max_step'] = $max_step;
        $currentForm = $this->getCurrentForm($current_step, $currentData);

        $responseValue = isset($currentData['responseValue']) ? $currentData['responseValue'] : null;

        $nonMajor = Onboarding::$nonMajorSteps[$current_step];

        return response()->json([
            'html' => $currentForm,
            'user_info' => $user_info,
            'current_step' => $current_step,
            'max_step' => $max_step,
            'current_step_name' => Onboarding::$getAllStep[$current_step],
            'status' => 'success',
            'nonMajor' => $nonMajor,
            'responseValue' => $responseValue
        ]);
    }

    //To get the current step
    private function getCurrentStep($stepFromPage, $userInfo, $instruction)
    {
//        dd($stepFromPage);
        $userCurrentStep = $userInfo['journey_mode_last_landing_step'];
        if ($stepFromPage == "" || $stepFromPage == null || $stepFromPage == 0) {
            if ($userCurrentStep == null) {
                $userCurrentStep = 1;
            }
        } else {
            $userCurrentStep = $stepFromPage;
        }
//        if (isset(Onboarding::$needAutoSkip[$userCurrentStep]) && Onboarding::$needAutoSkip[$userCurrentStep]) {   //AutoSkipping finished step.
//            $currentData = $this->getCurrentData($userCurrentStep, $userInfo['id']);
//            if (count($currentData) > 0) {
//                if ($instruction == Onboarding::INSTRUCTION_TO_NEXT) {
//                    return $this->getCurrentStep($userCurrentStep + 1, $userInfo, $instruction);
//                } else if ($instruction == Onboarding::INSTRUCTION_TO_PREVIOUS) {
//                    return $this->getCurrentStep($userCurrentStep - 1, $userInfo, $instruction);
//                }
//            }
//        }
        return $userCurrentStep;
    }

    private function getCurrentData($currentStep, $userId)
    {

        switch ($currentStep) {
            case Onboarding::PROFILE_SETTING :
                return [];
            case Onboarding::TIMEZONE_SETTING :
                return $this->getTimezoneData($userId);
            case Onboarding::SUBSCRIPTION_SETTING :
                return $this->getTimezoneData($userId);
        }
    }

    private function getTimezoneData($userId)
    {
        $currentTimeZone = $this->userService->getUserTimeZoneByUser($userId);

        if ($currentTimeZone) {
            $currentTimeZone = $currentTimeZone->value;
        } else {
            $this->userService->insertOrUpdateUserSetting(['key' => UserSetting::USER_TIME_ZONE, 'value' => env('TIMEZONE')], $userId);
            $currentTimeZone = env('TIMEZONE');
        }
        return [
            'timezones' => DateTimeZone::listIdentifiers(DateTimeZone::PER_COUNTRY, 'US'),
            'responseValue' => ['currentTimeZone' => $currentTimeZone]
        ];
    }

    private function getCategoryData($userId)
    {
//        $categories = $this->adminCategoryService->getAll();
//
//        return ['categories' => $categories];
        ;
    }

    public function getTemplateForm($currentData , $type)
    {
        $adminTemplateData = $this->adminWebTemplateService->getDataByType($type);

        return view('onboarding::add-forms.templateShow')->with([
            'adminTemplateData' => $adminTemplateData,
            'current_step' => $currentData['current_step'],
        ])->render();

    }

    public function getEBookForm($currentData)
    {
        $adminTemplateData = $this->adminEbookTemplateService->getEBookMainPage();

        return view('onboarding::add-forms.templateShowForEBook')->with([
            'adminTemplateData' => $adminTemplateData,
            'current_step' => $currentData['current_step'],
        ])->render();

    }

    public function getEmailTemplate($currentData)
    {
        $emailTemplate = $this->emailTemplateService->getByLimitData();

        return view('onboarding::add-forms.emailTemplateShow')->with([
            'emailTemplate' => $emailTemplate,
            'current_step' => $currentData['current_step'],
        ])->render();

    }

    private function getCurrentForm($currentStep, $currentData)
    {
        switch ($currentStep) {
            case Onboarding::PROFILE_SETTING :
                return $this->getProfileForm($currentData);
            case Onboarding::TIMEZONE_SETTING :
                return $this->getTimezoneForm($currentData);
            case Onboarding::SUBSCRIPTION_SETTING :
                return $this->getTemplateForm($currentData , AdminWebTemplate::ADMIN_WEB_TYPE_SUBSCRIPTION);
            case Onboarding::DOWNLOAD_INSTRUCTION_SETTING :
                return $this->getTemplateForm($currentData , AdminWebTemplate::ADMIN_WEB_TYPE_DOWNLOAD_INSTRUCTION);
            case Onboarding::OFFER_SETTING :
                return $this->getTemplateForm($currentData , AdminWebTemplate::ADMIN_WEB_TYPE_OFFER);
            case Onboarding::AUTO_FOLLOWUP_SETTING :
                return $this->getEmailTemplate($currentData);
            case Onboarding::E_BOOK :
                return $this->getEBookForm($currentData);
        }
    }

    public function onboardingSubmit(Request $request)
    {
        $user = Auth::user();
        $user_id = $user->id;

//        User::where('id', $user_id)->update(['journey_mode' => 0]);

        return \Illuminate\Support\Facades\Response::json([
            'html' => "Done",
            'status' => 'success'
        ]);
    }

    private function completeOnboarding($user_info)
    {
        $user_id = is_object($user_info) ? $user_info->id : $user_info;

        $this->onboardService->completeOnboarding($user_id);
    }

    private function getProfileForm($currentData)
    {
        return view('onboarding::add-forms.profile')->with([
            'current_step' => $currentData['current_step'],
        ])->render();
    }

    private function getTimezoneForm($currentData)
    {
        return view('onboarding::add-forms.timezone')->with([
            'timeZones' => $currentData['timezones'],
            'current_step' => $currentData['current_step'],
        ])->render();
    }

    private function getCategoryForm($currentData)
    {
        return view('onboarding::add-forms.category')->with($currentData)->render();
    }

    public function updateTimeZone(Request $request)
    {
        if (!$request->ajax()) {
            return redirect()->route('dashboard');
        }

        $request['journeyMode'] = auth()->user()->journey_mode;

        return response()->json($this->userService->insertOrUpdateUserSetting($request, auth()->id()));
    }

    public function categoryImport(Request $request)
    {
        if (!$request->ajax()) {
            return redirect()->route('dashboard');
        }

        $this->completeOnboarding(auth()->id());
        return response()->json(['status' => true, 'message' => 'All template related to this category imported!']);

        return $this->onboardService->importCategoryData($request->get('category'));
    }

    public function showPreviewForType(Request $request)
    {

        $result = $this->adminWebTemplateService->getAdminTemplateUsingTemplateId($request['id']);

        return response()->json([
            'status' => true,
            'html' => view('adminwebbuilder::builder.admin-web-builder-view')->with([
                'adminTemplateHtml' => $result->content,
                'adminTemplateCss' => $result->css
            ])->render(),
        ]);
    }

    public function defaultTemplateSet(Request $request)
    {
//        $result =  $this->webBuilderService->;
        dd($request->all());

    }

    public function webPageViewByIframe($id)
    {
        $result = $this->adminWebTemplateService->getAdminTemplateUsingTemplateId($id);

        return view('onboarding::add-forms.pageShow')->with([
            'content' => $result->content,
            'css' => $result->css
        ]);
    }


}
