<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('user/onboarding')->name('user.')->middleware('auth', 'web' , 'userHasActivePackage')->group(function() {
    Route::get('/', 'OnboardingController@index')->name('onboarding');
    Route::post('/onboarding/submit', 'OnboardingController@onboardingSubmit')->name('onboarding.submit');
    Route::post('/onboarding/form','OnboardingController@onboardingGetForm')->name('onboarding.get-form');
    Route::post('/onboarding/update/timezone', 'OnboardingController@updateTimeZone')->name('onboarding.timezone.update');
    Route::post('/onboarding/category/import', 'OnboardingController@categoryImport')->name('onboarding.category.import');
    Route::post('/show-preview-for-type', 'OnboardingController@showPreviewForType')->name('show-preview-for-type');
    Route::get('web-page-view-by-iframe/{id}', 'OnboardingController@webPageViewByIframe')->name('web-page-view-by-iframe');

});
