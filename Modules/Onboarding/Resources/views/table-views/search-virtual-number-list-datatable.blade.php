<div class="dt-card__body" id="data_table_body">
    <table class="table table-striped table-bordered table-hover table-checkable no-wrap dataTable dtr-inline" role="grid" style="width: 100%;" id="data_table_virtual_number_list">
        <thead>
        <tr>
            <th> <span>Phone Number</span> </th>
            <th> <span>Friendly Name</span> </th>
            <th> <span>ISO Country</span> </th>
            <th> <span>Postal Code</span> </th>
            <th> <span>Buy</span> </th>
        </tr>
        </thead>
        <tbody>
        @foreach($virtual_number_list as $each)
            <tr>
                <td> <span>{!! $each['phone_number'] !!}</span> </td>
                <td> <span>{!! $each['friendly_name'] !!}</span> </td>
                <td> <span>{!! $each['iso_country'] !!}</span> </td>
                <td> <span>{!! $each['postal_code'] !!}</span> </td>
                <td>
                    <button type="submit" id="buy-{!! $each['phone_number'] !!}_{!! $each['getaway_type'] !!}" class="btn custom-outlet-btn buy_virtual_number_from_search_list" value="buy-{!! $each['phone_number'] !!}_{!! $each['getaway_type'] !!}">Select </button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
