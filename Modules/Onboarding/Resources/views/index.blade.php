@extends('user.layout.app', ['menu' => 'onboarding'])
@section('css')
    {{--    <link href="{{ asset('css/onboarding.css') }}" rel="stylesheet" type="text/css"/>--}}
    <link href="{{ asset('css/onboarding_backup.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Entry-->
        <div class="container">
            <div class="modal fade show" id="exampleModalSizeXl" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalSizeXl" style="display: block; padding-right: 12px;" aria-modal="true">
                <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">WELCOME
                                TO {{ strtoupper(env('APP_NAME')) }}</h5>
                            <div class="">
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <button type="submit" id="previous-step-btn" data-previous-step-value=""
                                            class="btn btn-green btn-sm journey-mode-button font-weight-bolder">
                                <span class="svg-icon svg-icon-white">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                         width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <polygon points="0 0 24 0 24 24 0 24"/>
                                            <path d="M5.29288961,6.70710318 C4.90236532,6.31657888 4.90236532,5.68341391 5.29288961,5.29288961 C5.68341391,4.90236532 6.31657888,4.90236532 6.70710318,5.29288961 L12.7071032,11.2928896 C13.0856821,11.6714686 13.0989277,12.281055 12.7371505,12.675721 L7.23715054,18.675721 C6.86395813,19.08284 6.23139076,19.1103429 5.82427177,18.7371505 C5.41715278,18.3639581 5.38964985,17.7313908 5.76284226,17.3242718 L10.6158586,12.0300721 L5.29288961,6.70710318 Z"
                                                  fill="#000000" fill-rule="nonzero"
                                                  transform="translate(8.999997, 11.999999) scale(-1, 1) translate(-8.999997, -11.999999) "/>
                                            <path d="M10.7071009,15.7071068 C10.3165766,16.0976311 9.68341162,16.0976311 9.29288733,15.7071068 C8.90236304,15.3165825 8.90236304,14.6834175 9.29288733,14.2928932 L15.2928873,8.29289322 C15.6714663,7.91431428 16.2810527,7.90106866 16.6757187,8.26284586 L22.6757187,13.7628459 C23.0828377,14.1360383 23.1103407,14.7686056 22.7371482,15.1757246 C22.3639558,15.5828436 21.7313885,15.6103465 21.3242695,15.2371541 L16.0300699,10.3841378 L10.7071009,15.7071068 Z"
                                                  fill="#000000" fill-rule="nonzero" opacity="0.3"
                                                  transform="translate(15.999997, 11.999999) scale(-1, 1) rotate(-270.000000) translate(-15.999997, -11.999999) "/>
                                        </g>
                                    </svg><!--end::Svg Icon-->
                                </span>
                                        Previous
                                    </button>
                                    <button type="submit" id="next-step-btn" data-previous-step-value=""
                                            class="btn btn-green btn-sm journey-mode-button font-weight-bolder">

                                    <span class="next-btn">
                                    <span id="next-button-text">Next</span>
                                        <span class="svg-icon svg-icon-white">
                                        <svg xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                             viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <polygon points="0 0 24 0 24 24 0 24"/>
                                                <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z"
                                                      fill="#000000" fill-rule="nonzero"/>
                                                <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z"
                                                      fill="#000000" fill-rule="nonzero" opacity="0.3"
                                                      transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
                                            </g>
                                        </svg><!--end::Svg Icon-->
                                    </span>
                                </span>

                                    </button>
                                    <a type="button"
                                       onclick="event.preventDefault();document.getElementById('logout-form').submit();"
                                       class="btn btn-sm btn-light-green font-weight-bolder">
                                        <i class="fa fa-power-off"></i>
                                        Sign Out
                                    </a>
                                </div>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="m-portlet__body mc__action_journey_form_body" id="action_journey_form_body">
                                {{-- action form is leanding here by ajax response --}}
                            </div>

                            <ul id="progressbar">
                                @php
                                    $count = 1;
                                @endphp
                                @foreach(\Modules\Onboarding\Entities\Onboarding::$getAllStep as $key => $value)
                                    @if($key != \Modules\Onboarding\Entities\Onboarding::THANK_YOU_SETTING || $key != \Modules\Onboarding\Entities\Onboarding::PROFILE_SETTING)
                                        <li class="progress-step-item" id="progress-step-{{ $count}}"
                                            data-before="{{ $count }}" data-done="{{ $count  }}"
                                            data-step="{{ $count }}"> {{ $value }}</li>
                                    @endif
                                    @php
                                        $count++;
                                    @endphp
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Entry-->
    </div>

    <div class="modal fade bd-example-modal-lg"
         tabindex="-1" role="dialog"
         id="email-message-show"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Email Message</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">X</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12" id="email-message-body-show">
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        let defaultTimeZone = "{{ env('TIMEZONE') }}";
        let imageFile = null;
        let maxStep = parseInt("{{ $maxStep }}");

        let Instruction = {
            NEXT: '{{ \Modules\Onboarding\Entities\Onboarding::INSTRUCTION_TO_NEXT }}',
            PREVIOUS: '{{ \Modules\Onboarding\Entities\Onboarding::INSTRUCTION_TO_PREVIOUS }}',
        };

        let Steps = {
            PROFILE_SETTING: "{{ \Modules\Onboarding\Entities\Onboarding::PROFILE_SETTING }}",
            TIMEZONE_SETTING: "{{ \Modules\Onboarding\Entities\Onboarding::TIMEZONE_SETTING }}",
            SUBSCRIPTION_SELECT: "{{ \Modules\Onboarding\Entities\Onboarding::SUBSCRIPTION_SETTING }}",
            DOWNLOAD_INSTRUCTION_SELECT: "{{ \Modules\Onboarding\Entities\Onboarding::DOWNLOAD_INSTRUCTION_SETTING }}",
            OFFER_SELECT: "{{ \Modules\Onboarding\Entities\Onboarding::OFFER_SETTING }}",
            THANK_YOU_SELECT: "{{ \Modules\Onboarding\Entities\Onboarding::THANK_YOU_SETTING }}",
            AUTO_FOLLOWUP_SETTING: "{{ \Modules\Onboarding\Entities\Onboarding::AUTO_FOLLOWUP_SETTING }}",
            E_BOOK : "{{ \Modules\Onboarding\Entities\Onboarding::E_BOOK }}",
        };
    </script>
    <script src="{{ asset('js/user-onboarding.js') }}"></script>
@endsection
