@extends('user.layout.app')
@section('css')
    <link href="{{ asset('assets/theme/css/pages/wizard/wizard-1.css') }}" rel="stylesheet" type="text/css" />
    <style>
        .contactManagePage div:hover,
        .contactManagePage div.activeItem {
            background-color: rgba(114, 220, 70, 0.55);
            cursor: pointer;
        }
    </style>
@endsection

@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-2 mr-5">Complete your journey</h5>
                        <!--end::Page Title-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Card-->
                <div class="card card-custom">
                    <div class="card-body p-0">
                        <!--begin::Wizard-->
                        <div class="wizard wizard-1" id="kt_wizard_v1" data-wizard-state="between" data-wizard-clickable="false">
                            <!--begin::Wizard Nav-->
                            <div class="wizard-nav border-bottom">
                                <div class="wizard-steps p-8 p-lg-10">
                                    <!--begin::Wizard Step 2 Nav-->
                                    <div class="wizard-step" data-wizard-type="step" data-wizard-state="done">
                                        <div class="wizard-label">
                                            <i class="wizard-icon flaticon-list"></i>
                                            <h3 class="wizard-title">Company Information</h3>
                                        </div>
                                        <span class="svg-icon svg-icon-xl wizard-arrow">
                                            <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Arrow-right.svg-->
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                    <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000)" x="11" y="5" width="2" height="14" rx="1"></rect>
                                                    <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)"></path>
                                                </g>
                                            </svg>
                                            <!--end::Svg Icon-->
                                        </span>
                                    </div>
                                    <!--end::Wizard Step 2 Nav-->
                                    <!--begin::Wizard Step 3 Nav-->
                                    <div class="wizard-step" data-wizard-type="step" data-wizard-state="done">
                                        <div class="wizard-label">
                                            <i class="wizard-icon flaticon-responsive"></i>
                                            <h3 class="wizard-title">Category</h3>
                                        </div>
                                        <span class="svg-icon svg-icon-xl wizard-arrow">
                                            <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Arrow-right.svg-->
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                    <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000)" x="11" y="5" width="2" height="14" rx="1"></rect>
                                                    <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)"></path>
                                                </g>
                                            </svg>
                                            <!--end::Svg Icon-->
                                        </span>
                                    </div>
                                    <!--end::Wizard Step 3 Nav-->
                                </div>
                            </div>
                            <!--end::Wizard Nav-->
                            <!--begin::Wizard Body-->
                            <div class="row justify-content-center px-8 px-lg-10">
                                <div class="col-xl-12 col-xxl-7">
                                    <!--begin::Wizard Form-->
                                    <form class="form fv-plugins-bootstrap fv-plugins-framework" id="kt_form">
                                        <!--begin::Wizard Step 2-->
                                        <div class="pb-5" data-wizard-type="step-content">
                                            <!--begin::Input-->
                                            <div class="form-group row fv-plugins-icon-container pt-10">
                                                <div class="col-lg-8 mx-auto">
                                                    <label>Company Name:</label>
                                                    <input type="text" name="name" class="form-control" placeholder="Enter company name" value="" required>
                                                    <div class="custom-error-message"></div>
                                                </div>
                                            </div>
                                            <!--end::Input-->
                                        </div>
                                        <!--end::Wizard Step 2-->
                                        <!--begin::Wizard Step 3-->
                                        <div class="pb-5" data-wizard-type="step-content">
                                            <h4 class="my-10 font-weight-bold text-dark">Choose category</h4>

                                            <div class="row contactManagePage">
                                                <div class="col-xl-4 clickSelect" >
                                                    <!--begin::Stats Widget 4-->
                                                    <div class="card card-custom card-stretch gutter-b">
                                                        <!--begin::Body-->
                                                        <div class="card-body d-flex align-items-center py-0 mt-8">
                                                            <div class="d-flex flex-column flex-grow-1 py-2 py-lg-5">
                                                                <a href="#" class="card-title font-weight-bolder text-dark-75 font-size-h5 mb-2 text-hover-primary">Insurance</a>
                                                            </div>
                                                            <img src="{{ asset('assets/theme/media/svg/illustrations/working.svg') }}" alt="" class="align-self-end h-100px w-50">
                                                        </div>
                                                        <!--end::Body-->
                                                    </div>
                                                    <!--end::Stats Widget 4-->
                                                </div>
                                                <div class="col-xl-4 clickSelect">
                                                    <!--begin::Stats Widget 5-->
                                                    <div class="card card-custom card-stretch gutter-b">
                                                        <!--begin::Body-->
                                                        <div class="card-body d-flex align-items-center py-0 mt-8">
                                                            <div class="d-flex flex-column flex-grow-1 py-2 py-lg-5">
                                                                <a href="#" class="card-title font-weight-bolder text-dark-75 font-size-h5 mb-2 text-hover-primary">Real Estate</a>
                                                            </div>
                                                            <img src="{{ asset('assets/theme/media/svg/illustrations/features.svg') }}" alt="" class="align-self-end h-100px w-50">
                                                        </div>
                                                        <!--end::Body-->
                                                    </div>
                                                    <!--end::Stats Widget 5-->
                                                </div>
                                                <div class="col-xl-4 clickSelect">
                                                    <!--begin::Stats Widget 6-->
                                                    <div class="card card-custom card-stretch gutter-b">
                                                        <!--begin::Body-->
                                                        <div class="card-body d-flex align-items-center py-0 mt-8">
                                                            <div class="d-flex flex-column flex-grow-1 py-2 py-lg-5">
                                                                <a href="#" class="card-title font-weight-bolder text-dark-75 font-size-h5 mb-2 text-hover-primary">Social Media</a>
                                                            </div>
                                                            <img src="{{ asset('assets/theme/media/svg/illustrations/contact.svg') }}" alt="" class="align-self-end h-100px w-50">
                                                        </div>
                                                        <!--end::Body-->
                                                    </div>
                                                    <!--end::Stats Widget 6-->
                                                </div>

                                                <div class="col-xl-4 clickSelect">
                                                    <!--begin::Stats Widget 4-->
                                                    <div class="card card-custom card-stretch gutter-b">
                                                        <!--begin::Body-->
                                                        <div class="card-body d-flex align-items-center py-0 mt-8">
                                                            <div class="d-flex flex-column flex-grow-1 py-2 py-lg-5">
                                                                <a href="#" class="card-title font-weight-bolder text-dark-75 font-size-h5 mb-2 text-hover-primary">Loan payer</a>
                                                            </div>
                                                            <img src="{{ asset('assets/theme/media/svg/illustrations/login-visual-1.svg') }}" alt="" class="align-self-end h-100px w-50">
                                                        </div>
                                                        <!--end::Body-->
                                                    </div>
                                                    <!--end::Stats Widget 4-->
                                                </div>

                                                <div class="col-xl-4 clickSelect ">
                                                    <!--begin::Stats Widget 4-->
                                                    <div class="card card-custom card-stretch gutter-b">
                                                        <!--begin::Body-->
                                                        <div class="card-body d-flex align-items-center py-0 mt-8">
                                                            <div class="d-flex flex-column flex-grow-1 py-2 py-lg-5">
                                                                <a href="#" class="card-title font-weight-bolder text-dark-75 font-size-h5 mb-2 text-hover-primary">Communication</a>
                                                            </div>
                                                            <img src="{{ asset('assets/theme/media/svg/illustrations/login-visual-2.svg') }}" alt="" class="align-self-end h-100px w-50">
                                                        </div>
                                                        <!--end::Body-->
                                                    </div>
                                                    <!--end::Stats Widget 4-->
                                                </div>
                                            </div>
                                        </div>
                                        <!--end::Wizard Step 3-->

                                        <div class="d-flex justify-content-between border-top py-10">
                                            <div class="mr-2">
                                                <button type="button" class="btn btn-light-primary font-weight-bold text-uppercase px-9 py-4" data-wizard-type="action-prev">Previous</button>
                                            </div>
                                            <div>
                                                <button type="submit" class="btn btn-success font-weight-bold text-uppercase px-9 py-4 onboarding-submit" data-wizard-type="action-submit">Submit</button>
{{--                                                <a href="{{ route('onboarding-submit') }}" class="btn btn-success font-weight-bold text-uppercase px-9 py-4" data-wizard-type="action-submit">Submit</a>--}}
                                                <button type="button" class="btn btn-primary font-weight-bold text-uppercase px-9 py-4" data-wizard-type="action-next">Next</button>
                                            </div>
                                        </div>
                                    </form>
                                    <!--end::Wizard Form-->
                                </div>
                            </div>
                            <!--end::Wizard Body-->
                        </div>
                    </div>
                    <!--end::Wizard-->
                </div>
                <!--end::Card-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
@endsection

@section('script')
    <script src="{{ asset('assets/theme/js/pages/custom/wizard/wizard-1.js') }}"></script>
<script>
            $(function () {
            const _token = document.head.querySelector("[property=csrf-token]").content;


            (function (global, $, _token) {

                $(document).on("click", ".clickSelect", function(e){
                    e.preventDefault();
                    $('.clickSelect').removeClass('activeItem')
                    $(this).closest("div").addClass("activeItem");
                });

                $(document).on("click", ".onboarding-submit", function(e){


                    $('#preloader').show();
                    global.mySiteAjax({
                        url: route('onboarding-submit'),
                        type: 'post',
                        data: { '_token' : _token},
                        loadSpinner: true,
                        success: function (response) {
                            if (response.status == 'validation-error') {
                                global.generateErrorMessage(formInstance, response.html, 'div.form-div:last-child');
                                $('#preloader').hide();
                            } else if (response.status == 'success') {

                                bootstrapNotify.success(response.html, '');
                                return window.nbUtility.redirect('/');
                                $('#preloader').hide();


                            }
                        }
                    });

                });

            })(window, jQuery, _token);
        });

    </script>
@endsection
