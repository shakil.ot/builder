<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin Template</title>
    <style>
        body,
        html {
            height: 100%;
            margin: 0;
        }

        {!! $css !!}
    </style>
</head>
<body>
{!!$content  !!}
</body>
</html>







