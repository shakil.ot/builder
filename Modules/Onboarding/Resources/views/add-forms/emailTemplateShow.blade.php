<h3>
    <span class="title-of-page"></span>
    <span id="panel-description-text" data-trigger="hover" data-toggle="popover" title="Hint" data-html="true"
          data-content="">
        <span class="svg-icon svg-icon-green">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                 height="24px" viewBox="0 0 24 24" version="1.1">
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <rect x="0" y="0" width="24" height="24"/>
                    <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10"/>
                    <path d="M12,16 C12.5522847,16 13,16.4477153 13,17 C13,17.5522847 12.5522847,18 12,18 C11.4477153,18 11,17.5522847 11,17 C11,16.4477153 11.4477153,16 12,16 Z M10.591,14.868 L10.591,13.209 L11.851,13.209 C13.447,13.209 14.602,11.991 14.602,10.395 C14.602,8.799 13.447,7.581 11.851,7.581 C10.234,7.581 9.121,8.799 9.121,10.395 L7.336,10.395 C7.336,7.875 9.31,5.922 11.851,5.922 C14.392,5.922 16.387,7.875 16.387,10.395 C16.387,12.915 14.392,14.868 11.851,14.868 L10.591,14.868 Z"
                          fill="#000000"/>
                </g>
            </svg><!--end::Svg Icon-->
        </span>
    </span>
</h3>

<form action=""
      class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed"
      data-parsley-validate=""
      id='e-book-form'
      method='post'
>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="current_step_value" name="current_step" value="{{ $current_step }}">
    <input type="hidden" name="isFromJourneyMode" value="true">

    <div class="m-portlet__body" style="margin-top: 25px;">
        <div class="row customRowOnb email__">
            @foreach($emailTemplate as $key => $value)
                <div class="col-md-4 template__container __email">
                    <div class="template__email">
                        <div class="wrapper__title__desc">
                            <div class="email__title">
                                {{ \Modules\AdminEmailTemplate\Entities\EmailTemplate::DAYARRAY[$key] }}
                            </div>
                            <div class="email__desc" id="email-desc-{{$key}}" >
                                {!! substr($value->title , 0 , 50) !!}
                            </div>
                        </div>
                        <img src="{{asset('/assets/images/env-svg.png')}}" alt="">
                        <img class="forward__arrow" src="{{asset('/assets/images/fa.png')}}" alt="">
                        <div class="btns__group">
                            <button class="btn btn-sm btn-primary edit-emailTemplate"
                                    id="change-data-{{$key}}"
                                    data-id="{{ $value->id }}"
                                    data-key="{{ $key }}"
                            >  <i class="far fa-edit"></i> Change</button>

                            <button class="btn btn-sm btn-warning view-emailTemplate"
                                    data-id="{{ $value->id }}"
                            >  <i class="far fa-eye"></i> view</button>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</form>



