<h3>
    Select <span class="title-of-page"></span>
    <span id="panel-description-text" data-trigger="hover" data-toggle="popover" title="Hint" data-html="true"
          data-content="">
        <span class="svg-icon svg-icon-green">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                 height="24px" viewBox="0 0 24 24" version="1.1">
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <rect x="0" y="0" width="24" height="24"/>
                    <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10"/>
                    <path d="M12,16 C12.5522847,16 13,16.4477153 13,17 C13,17.5522847 12.5522847,18 12,18 C11.4477153,18 11,17.5522847 11,17 C11,16.4477153 11.4477153,16 12,16 Z M10.591,14.868 L10.591,13.209 L11.851,13.209 C13.447,13.209 14.602,11.991 14.602,10.395 C14.602,8.799 13.447,7.581 11.851,7.581 C10.234,7.581 9.121,8.799 9.121,10.395 L7.336,10.395 C7.336,7.875 9.31,5.922 11.851,5.922 C14.392,5.922 16.387,7.875 16.387,10.395 C16.387,12.915 14.392,14.868 11.851,14.868 L10.591,14.868 Z"
                          fill="#000000"/>
                </g>
            </svg><!--end::Svg Icon-->
        </span>
    </span>
</h3>

<form action=""
      class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed"
      data-parsley-validate=""
      id='subscription-form'
      method='post'
>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="current_step_value" name="current_step" value="{{ $current_step }}">
    <input type="hidden" name="isFromJourneyMode" value="true">

    <div class="m-portlet__body" style="margin-top: 25px;">
        <div class="row customRowOnb">
            @foreach($adminTemplateData as $key => $value)

                <div class="col-md-4 template__container submit-button"   data-id="{{ $value->id }}">
                    <div class="template__container__inner">
                        <div class="image__container"
                             style="background-image: url({{ $value->image }});">
                        </div>
                    </div>

                </div>
            @endforeach
        </div>

        <div class="form-group row d-none">
            <label class="col-form-label col-lg-1 col-sm-12">
                Template :
            </label>
            <div class="col-lg-3 col-md-5 col-sm-12">
                <select class="form-control m-select2  " id="subscription-select2" name="value">
                    @foreach($adminTemplateData as $key => $value)
                        <option value="{{ $value->id }}">
                            {{ $value->title }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>


</form>



