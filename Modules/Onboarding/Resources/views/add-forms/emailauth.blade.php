<div id="main-wrapper" class="h-100 email-providers-div d-flex w-100">
    <div class="container-fluid px-0">
        <div class="row">
            <div class="col-12 col-md-12">
                <div class="main-section d-flex justify-content-between align-items-center">
                    <div class="main-top px-4 py-3 border-bottom">
                        <h1 class="h3 mb-0">Email Provider</h1>
                        <p><b>Email provider that will be use to send emails.</b></p>
                    </div>
                    <?php $email = \Illuminate\Support\Facades\Auth::user()->auth_email ?>
                    <div class="@if(!$email) d-none @endif email-details-div" id="email-status-div">
                        Your are connected with<br>
                        <b class="text-success" id="auth-email-label">{{ $email }}</b><br>
                        <button class="btn btn-sm btn-danger mt-2 float-right" id="email-disconnect-button">Disconnect</button>
                    </div>
                    <div class="@if($email) d-none @endif email-details-div" id="system-email-div">
                        Please connect an email account.<br>
                    </div>
                </div>
                <div class="main-content-wrapper primary-bg px-4 py-3" id="email-providers">
                    <div class="main-content ">
                        <div id="enter-email-div" class="col-12 col-md-12 pt-5 @if($email) d-none @endif">
                            <div class="main-section ">
                                <div class="main-top px-3 py-3 border-bottom">
                                    <form id="auth-form">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" id="current_step_value" name="current_step" value="{{ $current_step }}">
                                        <div class="primary-bg px-3 py-3">
                                            <div class="row">
                                                <div class="col-12">
                                                    <h5 class="mb-4 font-weight-light">Simply connect your email provider to get started</h5>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group-label">
                                                        <input id="imapEmail" type="email" name="email" value="{{ \Illuminate\Support\Facades\Auth::user()->email }}" autocomplete="off" class="form-control custom-input-field" required placeholder="Sender email">
                                                        <label for="imapEmail">Sender email</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mt-4 d-flex justify-content-center">
                                                <button class="btn btn-primary " type="submit">Connect</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div id="connected-email-div" class="col-12 col-md-12 pt-5 @if(!$email) d-none @endif">
                            <div class="main-section ">
                                <div class="main-top px-3 py-3 border-bottom">
                                    <h1 class="font-weight-light text-center text-success mb-3">You are connected!</h1>
                                    <h5 class="font-weight-light text-center text-dark mb-5">
                                        We will use <span class="text-primary">{{$email}}</span> to send out your emails
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

