<form class="m-form m-form--label-align-left- m-form--state-" id="action_journey_form">
    <div class="m-portlet__body">

        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="user_id" value="{{ $user_info['id'] }}">
        <input type="hidden" name="type" value="{{ \App\Models\Message::TYPE_MESSAGE }}">
        <input type="hidden" name="previous_step" value="{{ $previous_step }}">
        <input type="hidden" name="current_step" value="{{ $current_step }}">
        <input type="hidden" name="skip_step" value="{{ $skip_step }}">

        <div class="row">
            <div class="offset-xl-2 col-xl-8">
                <div class="m-form__section m-form__section--first">
                    <div class="form-group m-form__group row">
                        <label class="col-xl-3 col-lg-3 col-form-label">* Account SID:</label>
                        <div class="col-xl-9 col-lg-9">
                            <input type="text" name="account_sid" class="form-control m-input required_field" required placeholder="Please enter  Account SID" value="">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-xl-3 col-lg-3 col-form-label">* Auth Token:</label>
                        <div class="col-xl-9 col-lg-9">
                            <input type="text" name="auth_token" class="form-control m-input required_field" required placeholder="Please enter  Auth Token" value="">
                        </div>
                    </div>
                    <div class="col-xl-12 col-lg-12 text-right twillo-btn">
                        <a href="javascript:void(0)" id="" class="btn btn-outline-primary m-btn m-btn--square m-btn--icon m-btn--pill help_information_button">
							<span>
								<i class="la la-question"></i>
								<span>Help me about twilio auth</span>
							</span>
                        </a>
                        <button type="submit" id="gateway-auth-setting-button" class="btn btn-outline-success m-btn m-btn--square m-btn--pill">Save Twilio API Credentials</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</form>