<h2>Virtual Number Setup</h2>
<input type="hidden" id="current_step_value" name="current_step" value="{{ $current_step }}">
<div>
    <div id="virtual_number_add_form_panel">
        <div id="virtual_number_search">
            <form class="m-form m-form--label-align-left- m-form--state- m-form--group-seperator-dashed" id="action_journey_form_get_virtual_number_list">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-md-12">
                        <div class="m-form__section m-form__section--first">
                            <div class="form-group m-form__group row">
                                <div class="col-lg-6">
                                    <label class="">Area code:</label>
                                    <input type="text" class="form-control m-input" placeholder="Enter area code" name="areaCode">
                                    <span class="m-form__help">Please enter your area code</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-right steps-btns">
                                    <button type="submit" id="gateway-auth-setting-button" class="btn">Search Virtual Number <i class="la la-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
