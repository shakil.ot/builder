<h2>Cut Off Day Setup</h2>
{!! Form::open(array('class'=>'m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed',
       'data-parsley-validate=""','id'=>'cutoff-day-form','method'=>'post')) !!}

<input type="hidden" name="_token" value="{{ csrf_token() }}">
<input type="hidden" id="current_step_value" name="current_step" value="{{ $current_step }}">
<input type="hidden" name="saturday-key" value="{{ \App\Models\UserSetting::SEND_FOLLOWUP_ON_SATURDAY }}">
<input type="hidden" name="sunday-key" value="{{ \App\Models\UserSetting::SEND_FOLLOWUP_ON_SUNDAY }}">

<div style="margin-top: 25px;">
    <div class="">
        <label> By default our system will send automated messages on Saturdays and Sundays. If you wish to turn off automated messages on Saturdays and/or Sundays you can adjust your setttings below. </label>
        <div class="m-checkbox-list mt-2">
            <div class="row ml-3">
                <span class="m-switch m-switch--icon m-switch--sm m-switch--success">
                    <label>
                        <input type="checkbox" name="saturday-value"
                           @if (isset($cutOffDay[\App\Models\UserSetting::SEND_FOLLOWUP_ON_SATURDAY]) && $cutOffDay[\App\Models\UserSetting::SEND_FOLLOWUP_ON_SATURDAY] == "0"))
                            checked
                           @endif
                        >
                        <span></span>
                    </label>
                </span>
                <span class="ml-1 mt-1">Saturday</span>
            </div>
            <div class="row ml-3">
                <span class="m-switch m-switch--icon m-switch--sm m-switch--success">
                    <label>
                        <input type="checkbox" name="sunday-value"
                           @if (isset($cutOffDay[\App\Models\UserSetting::SEND_FOLLOWUP_ON_SUNDAY]) && $cutOffDay[\App\Models\UserSetting::SEND_FOLLOWUP_ON_SUNDAY] == "0"))
                               checked
                           @endif
                        >
                        <span></span>
                    </label>
                </span>
                <span class="ml-1 mt-1">Sunday</span>
            </div>
        </div>
        <span class="m-form__help"> If none selected no cutoff days will be assigned. </span>
    </div>
</div>

<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
    <div class="m-form__actions p-0">
        <div class="row">
            <div class="col">
                <button type="submit" id="cutoff-day-submit-button" class="btn btn-outline-main float-right journey-mode-button">
                    <i class="la la-pencil"></i> Update Cutoff Day
                </button>
            </div>
        </div>
    </div>
</div>


{!! Form::close() !!}


