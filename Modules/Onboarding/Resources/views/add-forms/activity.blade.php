<h2>Notifications</h2>
<form id="notification-edit-form" >

    <div class="row">

    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="user_id" value="{{ $userData->id }}">
    <input type="hidden" id="current_step_value" name="current_step" value="{{ $current_step }}">

    @if($userData->role ==1)
        <!--##################################### -->
            <!-- employee_create_new_proposal_status -->
            <div class="col-md-7">
                <strong class="flex-1 heading-font-family fw-500  align-self-start">
                    Notify me when one of my employees creates a new proposal:
                </strong>
            </div>
            <div class="col-md-5">
                <input type="checkbox" name="employee_create_new_proposal_status"
                       @if(($notificationData)!=null)
                       @if(($notificationData->employee_create_new_proposal_status)==1)
                       {{'checked="checked"'}}
                       @endif
                       @endif
                       class="js-switch" data-color="#3a8ab7">
                <a style="cursor: pointer;"
                   id="use_custom_email_employee_create_new_proposal">
                    Use a custom email address
                </a>
                <a style="cursor: pointer;"
                   id="dont_custom_email_employee_create_new_proposal">
                    Don't use a custom email address
                </a>
            </div>


            <div class="col-md-6" id="custom_email_employee_create_new_proposal">

                <div class="contact-details-cell">
                    {!!  Form::email('custom_email_employee_create_new_proposal', !empty($notificationData->custom_email_employee_create_new_proposal)?$notificationData->custom_email_employee_create_new_proposal:null, [
                       'class'                         => 'form-control col-md-12 confirmEmail',
                       'id'                            => 'sample1UserName',
                       "required"                      => "",
                        'Placeholder'                   => 'Custom Email Address'
                    ],null,true)!!}
                </div>
                <!-- /.contact-details-cell -->
            </div>

            <!--##################################### -->
            <!-- END employee_create_new_proposal_status -->

            <hr class="mr-tb-30">


            <!--##################################### -->
            <!-- employee_create_new_client_status -->

            <div class="col-md-7">
                <strong class="flex-1 heading-font-family fw-500  align-self-start">
                    Notify me when one of my employees creates a new client:
                </strong>
            </div>
            <div class="col-md-5">
                <input type="checkbox" name="employee_create_new_client_status"

                       @if(($notificationData)!=null)
                       @if(($notificationData->employee_create_new_client_status)==1)
                       {{'checked="checked"'}}
                       @endif
                       @endif
                       checked="checked" class="js-switch" data-color="#3a8ab7">
                <a style="cursor: pointer;"
                   id="use_custom_email_employee_create_new_client">Use a custom email address</a>
                <a style="cursor: pointer;"
                   id="dont_custom_email_employee_create_new_client">Don't use a custom email address</a>
            </div>

            <div class="col-md-6" id="custom_email_employee_create_new_client">
                <div class="contact-details-cell">
                    {!!  Form::email('custom_email_employee_create_new_client', !empty($notificationData->custom_email_employee_create_new_client)?$notificationData->custom_email_employee_create_new_client:null, [
                       'class'                         => 'form-control col-md-12 confirmEmail',
                       'id'                            => 'sample1UserName',
                       "required"                      => "",
                       'Placeholder'                   => 'Custom Email Address'
                    ],null,true)!!}
                </div>
                <!-- /.contact-details-cell -->
            </div>

            <!--##################################### -->
            <!-- END employee_create_new_client_status -->


            <hr class="mr-tb-30">


            <!--##################################### -->
            <!-- employee_create_new_client_status -->

            <div class="col-md-7">
                <strong class="flex-1 heading-font-family fw-500  align-self-start">
                    Notify me when one of my employees' clients views a proposal:
                </strong>
            </div>
            <div class="col-md-5">
                <input type="checkbox" name="employee_client_views_proposal_status"
                       @if(($notificationData)!=null)
                       @if(($notificationData->employee_client_views_proposal_status)==1)
                       {{'checked="checked"'}}
                       @endif
                       @endif
                       class="js-switch" data-color="#3a8ab7">
                <a style="cursor: pointer;"
                   id="use_custom_email_employee_client_views_proposal">Use a custom email address</a>
                <a style="cursor: pointer;"
                   id="dont_custom_email_employee_client_views_proposal">Don't use a custom email address</a>
            </div>

            <div class="col-md-6" id="custom_email_employee_client_views_proposal">
                <div class="contact-details-cell">
                    {!!  Form::email('custom_email_employee_client_views_proposal', !empty($notificationData->custom_email_employee_client_views_proposal)?$notificationData->custom_email_employee_client_views_proposal:null, [
                       'class'                         => 'form-control col-md-12 confirmEmail',
                       'id'                            => 'sample1UserName',
                       "required"                      => "",
                       'Placeholder'                   => 'Custom Email Address'
                    ],null,true)!!}
                </div>
                <!-- /.contact-details-cell -->
            </div>

            <!--##################################### -->
            <!-- END employee_create_new_client_status -->


            <hr class="mr-tb-30">


            <!--##################################### -->
            <!-- employee_client_accepts_proposal_status -->

            <div class="col-md-7">
                <strong class="flex-1 heading-font-family fw-500  align-self-start">
                    Notify me when one of my employees' clients accepts a proposal:
                </strong>
            </div>
            <div class="col-md-5">
                <input type="checkbox" name="employee_client_accepts_proposal_status"

                       @if(($notificationData)!=null)
                       @if(($notificationData->employee_client_accepts_proposal_status)==1)
                       {{'checked="checked"'}}
                       @endif
                       @endif
                       class="js-switch" data-title="employee_client_accepts_proposal_status"
                       data-color="#3a8ab7">
                <a style="cursor: pointer;"
                   id="use_custom_email_employee_client_accepts_proposal">Use a custom email address</a>
                <a style="cursor: pointer;"
                   id="dont_custom_email_employee_client_accepts_proposal">Don't use a custom email address</a>
            </div>

            <div class="col-md-6" id="custom_email_employee_client_accepts_proposal">
                <div class="contact-details-cell">
                    {!!  Form::email('custom_email_employee_client_accepts_proposal', !empty($notificationData->custom_email_employee_client_accepts_proposal)?$notificationData->custom_email_employee_client_accepts_proposal:null, [
                       'class'                         => 'form-control col-md-12 confirmEmail',
                       'id'                            => 'sample1UserName',
                       "required"                      => "",
                       'Placeholder'                   => 'Custom Email Address'
                    ],null,true)!!}
                </div>
                <!-- /.contact-details-cell -->
            </div>


            <!--##################################### -->
            <!-- END employee_client_accepts_proposal_status -->


            <hr class="mr-tb-30">


            <!--##################################### -->
            <!-- employee_client_has_a_question_status -->

            <div class="col-md-7">
                <strong class="flex-1 heading-font-family fw-500  align-self-start">
                    Notify me when one of my employees' clients has a question:
                </strong>
            </div>
            <div class="col-md-5">
                <input type="checkbox" name="employee_client_has_a_question_status"

                       @if(($notificationData)!=null)
                       @if(($notificationData->employee_client_has_a_question_status)==1)
                       {{'checked="checked"'}}
                       @endif
                       @endif
                       class="js-switch" data-color="#3a8ab7">
                <a style="cursor: pointer;"
                   id="use_custom_email_employee_client_has_a_question">Use a custom email address</a>
                <a style="cursor: pointer;"
                   id="dont_custom_email_employee_client_has_a_question">Don't use a custom email address</a>
            </div>

            <div class="col-md-6" id="custom_email_employee_client_has_a_question">
                <div class="contact-details-cell">
                    {!!  Form::email('custom_email_employee_client_has_a_question', !empty($notificationData->custom_email_employee_client_has_a_question)?$notificationData->custom_email_employee_client_has_a_question:null, [
                       'class'                         => 'form-control col-md-12 confirmEmail',
                       'id'                            => 'sample1UserName',
                       "required"                      => "",
                       'Placeholder'                   => 'Custom Email Address'
                    ],null,true)!!}
                </div>
                <!-- /.contact-details-cell -->
            </div>
            <!--##################################### -->
            <!-- END employee_client_has_a_question_status -->

            <hr class="mr-tb-30">

            <!-- Start employee_send_proposal-->
            <!--##################################### -->
            <div class="col-md-7">
                <strong class="flex-1 heading-font-family fw-500  align-self-start">
                    Notify me when me or one of my team members sends a proposal:
                </strong>
            </div>
            <div class="col-md-5">
                <input type="checkbox" name="client_sends_proposal_status"
                       @if(($notificationData)!=null)
                       @if(($notificationData->client_sends_proposal_status)==1)
                       {{'checked="checked"'}}
                       @endif
                       @endif
                       class="js-switch" data-color="#3a8ab7">
                <a style="cursor: pointer;"
                   id="use_custom_client_sends_proposal">Use a custom email address</a>
                <a style="cursor: pointer;"
                   id="dont_custom_client_sends_proposal">Don't use a custom email address</a>
            </div>

            <div class="col-md-6" id="custom_client_sends_proposal">
                <div class="contact-details-cell">
                    {!!  Form::email('custom_client_sends_proposal', !empty($notificationData->custom_client_sends_proposal	)?$notificationData->custom_client_sends_proposal	:null, [
                       'class'                         => 'form-control col-md-12 confirmEmail',
                       'id'                            => 'sample1UserName',
                       "required"                      => "",
                       'Placeholder'                   => 'Custom Email Address'
                    ],null,true)!!}
                </div>
                <!-- /.contact-details-cell -->
            </div>
            <!-- END employee_send_proposal-->

        @endif

        <hr class="mr-tb-30">


        <!--##################################### -->
        <!-- client_views_proposal_status -->

        <div class="col-md-7">
            <strong class="flex-1 heading-font-family fw-500  align-self-start">
                Notify me when one of my clients views a proposal:
            </strong>
        </div>
        <div class="col-md-5">
            <input type="checkbox" name="client_views_proposal_status"
                   @if(($notificationData)!=null)
                   @if(($notificationData->client_views_proposal_status)==1)
                   {{'checked="checked"'}}
                   @endif
                   @endif
                   class="js-switch" data-color="#3a8ab7">
            <a style="cursor: pointer;"
               id="use_custom_client_views_proposal">Use a custom email address</a>
            <a style="cursor: pointer;"
               id="dont_custom_client_views_proposal">Don't use a custom email address</a>
        </div>

        <div class="col-md-6" id="custom_client_views_proposal">
            <div class="contact-details-cell">
                {!!  Form::email('custom_client_views_proposal', !empty($notificationData->custom_client_views_proposal)?$notificationData->custom_client_views_proposal:null, [
                   'class'                         => 'form-control col-md-12 confirmEmail',
                   'id'                            => 'sample1UserName',
                   "required"                      => "",
                   'Placeholder'                   => 'Custom Email Address'
                ],null,true)!!}
            </div>
            <!-- /.contact-details-cell -->
        </div>

        <!--##################################### -->
        <!-- END client_views_proposal_status -->

        <hr class="mr-tb-30">


        <!--##################################### -->
        <!-- END client_accepts_proposal_status -->
        <div class="col-md-7">
            <strong class="flex-1 heading-font-family fw-500  align-self-start">
                Notify me when one of my clients accepts a proposal:
            </strong>
        </div>
        <div class="col-md-5">
            <input type="checkbox" name="client_accepts_proposal_status"
                   @if(($notificationData)!=null)
                   @if(($notificationData->client_accepts_proposal_status)==1)
                   {{'checked="checked"'}}
                   @endif
                   @endif
                   class="js-switch" data-color="#3a8ab7">
            <a style="cursor: pointer;"
               id="use_custom_client_accepts_proposal">Use a custom email address</a>
            <a style="cursor: pointer;"
               id="dont_custom_client_accepts_proposal">Don't use a custom email address</a>
        </div>

        <div class="col-md-6" id="custom_client_accepts_proposal">
            <div class="contact-details-cell">
                {!!  Form::email('custom_client_accepts_proposal', !empty($notificationData->custom_client_accepts_proposal	)?$notificationData->custom_client_accepts_proposal	:null, [
                   'class'                         => 'form-control col-md-12 confirmEmail',
                   'id'                            => 'sample1UserName',
                   "required"                      => "",
                   'Placeholder'                   => 'Custom Email Address'
                ],null,true)!!}
            </div>
            <!-- /.contact-details-cell -->
        </div>
        <!--##################################### -->
        <!-- END client_accepts_proposal_status -->

        <hr class="mr-tb-30">

        {{--Submit button--}}
        <div class="col-sm-12 btn-list">
            <button type="submit" class="btn btn-primary float-right" id="submit-notification">Update</button>

        </div>
    </div>
</form>
