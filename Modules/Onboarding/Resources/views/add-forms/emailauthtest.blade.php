<div id="main-wrapper" class="h-100 email-providers-div d-flex w-100">
    <div class="container-fluid px-0">
        <div class="row">
            <div class="col-12 col-md-12">
                <div class="main-section d-flex justify-content-between align-items-center">
                    <div class="main-top px-4 py-3 border-bottom">
                        <h1 class="h3 mb-0">Email Provider</h1>
                        <p><b>Email provider that will be use to send emails.</b></p>
                    </div>
                    <?php $email = \Illuminate\Support\Facades\Auth::user()->auth_email ?>
                    <div class="@if(!$email) d-none @endif email-details-div" id="email-status-div">
                        Your are connected with<br>
                        <b class="text-success" id="auth-email-label">{{ $email }}</b><br>
                        <button class="btn btn-sm btn-danger mt-2 float-right" id="email-disconnect-button">Disconnect</button>
                    </div>
                    <div class="@if($email) d-none @endif email-details-div" id="system-email-div">
                        Please connect an email account.<br>
                    </div>
                </div>

                <div class="main-content-wrapper primary-bg px-4 py-3" id="email-providers">
                    <div class="main-content ">
                        <h5 class="mb-4 font-weight-light">Simply connect your email provider to get started</h5>
                        <div class="row justify-content-center text-center">
                            <div class="col mb-2">
                                <button class="px-1 py-2 provider-item redirect-button" id="google-button">
                                    <img src="{{ asset("assets/images/Google.png") }}"><br>Connect a<br><b>Google</b>
                                    account
                                </button>
                            </div>
                            <div class="cocategoriesl  mb-2">
                                <button class="px-1 py-2 provider-item redirect-button" id="office365-button">
                                    <img src="{{ asset("assets/images/offce365.png") }}"><br>Connect an<br><b>Office
                                        365</b> account
                                </button>
                            </div>
                            <div class="col mb-2">
                                <button  class=" py-2 provider-item provider-item-outlook" id="outlook-button">
                                    <img class="logo" src="{{ URL::asset('/assets/images/outlook.png') }}"  ><br>Connect an<br><b>Outlook</b> account
                                </button>
                            </div>
                            <div class="col  mb-2">
                                <button class="px-1 py-2 provider-item" id="exchange-button">
                                    <img src="{{ asset("assets/images/exchange.png") }}"><br>Connect
                                    an<br><b>Exchange</b> account
                                </button>
                            </div>
                            <div class="col  mb-2 ">
                                <button class=" py-2 provider-item" id="smtp-button">
                                    <img src="{{ asset("assets/images/smtp.png") }}"><br>Connect a<br><b>SMTP / IMAP</b>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{--IMAP div--}}
<div class="col-12 col-md-12 pt-5 imap-provider-div d-none">
    <div class="main-section ">
        <div class="main-top px-3 py-3 border-bottom">
            <div class="d-flex align-items-center justify-content-between w-100">
                <div class="mr-auto">
                    <h1 class="h3 mb-0">Email Provider</h1>
                    <p><b>You're using a custom SMTP / IMAP email provider. </b></p>
                </div>
                <div class="">
                    <button class="btn btn-primary provider-back-button" data-provider-class="imap-provider-div">Choose another
                        email provider
                    </button>
                </div>
            </div>
        </div>
        <form id="smtp-auth-form">

            <input type="hidden" name="type" value="smtp">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" id="current_step_value" name="current_step" value="{{ $current_step }}">


            <div class="primary-bg px-3 py-3">
                <div class="row">
                    <div class="col-12">
                        <h5 class="mb-4 font-weight-bold">Sender settings</h5>
                    </div>
                    <div class="col">
                        <div class="form-group-label">
                            <input id="imapEmail" type="email" name="email" autocomplete="off" class="form-control custom-input-field" required placeholder="Sender email">
                            <label for="imapEmail">Sender email</label>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group-label">
                            <input id="sname" type="text" name="user-name" autocomplete="off" class="form-control custom-input-field" required placeholder="Sender name">
                            <label for="sname">Sender name</label>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group-label">
                            <input id="pass" type="password" name="password" autocomplete="off" class="form-control custom-input-field" required placeholder="Password">
                            <label for="pass">Password</label>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col">
                        <h5 class="mb-4 font-weight-bold">SMTP settings
                            {{--                        <button class="btn btn-primary btn-sm float-right" disabled="true">Test</button>--}}
                        </h5>
                        <div class="form-group-label">
                            <input id="smtpserver" type="search" name="smtpHost" autocomplete="off" required placeholder="SMTP Server / Host" class="form-control custom-input-field"
                                   data-trigger="focus" data-offset="10px 10px" data-toggle="m-popover" data-html="true"
                                   data-content="">
                            <label for="smtpserver">Server address</label>
                        </div>
                        <div class="form-group-label">
                            <input id="smtpPort" type="search" name="smtpPort" autocomplete="off" required  placeholder="SMTP Port" class="form-control custom-input-field"
                                   oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');"
                                   data-trigger="focus" data-offset="10px 10px" data-toggle="m-popover" data-html="true"
                                   data-content="Wanna pick one?<br><button class='btn btn-sm btn-primary pop-button' data-id='#smtpPort'>587</button> <button class='btn btn-sm btn-primary pop-button' data-id='#smtpPort'>465</button> <button class='btn btn-sm btn-primary pop-button' data-id='#smtpPort'>25</button>">
                            <label for="smtpPort">Port</label>
                        </div>
                        {{--                    <div class="form-check form-group-check">--}}
                        {{--                        <p>Enable SSL</p>--}}
                        {{--                        <input type="checkbox" class="form-check-input" id="smtp-secure">--}}
                        {{--                        <label class="form-check-label" for="smtp-secure">Enable SSL</label>--}}
                        {{--                    </div>--}}
                    </div>
                    <div class="col">
                        <h5 class="mb-4 font-weight-bold">IMAP settings
                            {{--                        <button class="btn btn-primary btn-sm float-right" disabled="true">Test</button>--}}
                        </h5>
                        <div class="form-group-label">
                            <input id="imapserver" type="search" name="imapHost" autocomplete="off" required placeholder="IMAP Server / Host" class="form-control custom-input-field"
                                   data-trigger="focus" data-offset="10px 10px" data-toggle="m-popover" data-html="true"
                                   data-content=""
                                   value="">
                            <label for="imapserver">Server address</label>
                        </div>
                        <div class="form-group-label">
                            <input id="imapport" type="search" name="imapPort" autocomplete="off" required placeholder="IMAP Port" class="form-control custom-input-field"
                                   oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');"
                                   data-trigger="focus" data-offset="10px 10px" data-toggle="m-popover" data-html="true"
                                   data-content="Wanna pick one?<br><button class='btn btn-sm btn-primary pop-button' data-id='#imapport'>993</button> <button class='btn btn-sm btn-primary pop-button' data-id='#imapport'>143</button>">
                            <label for="imapport">Port</label>
                        </div>

                        {{--                    <div class="form-check form-group-check">--}}
                        {{--                        <p>Enable SSL</p>--}}
                        {{--                        <input type="checkbox" class="form-check-input" id="imap-secure">--}}
                        {{--                        <label class="form-check-label" for="imap-secure">Enable SSL</label>--}}
                        {{--                    </div>--}}

                    </div>
                </div>
                <div class="row mt-4 d-flex justify-content-center">
                    <button class="btn btn-primary " type="submit">Test &amp; Save</button>
                </div>
            </div>
        </form>
    </div>
</div>

{{-- Exchange div --}}
<div class="col-12 col-md-12 pt-5 exchange-provider-div d-none">
    <div class="main-section ">
        <div class="main-top px-3 py-2 border-bottom">
            <div class="d-flex align-items-center justify-content-between w-100">
                <div class="mr-auto">
                    <h1 class="h3 mb-0">Email Provider</h1>
                    <p><b>You are using Exchange as email provider.</b></p>
                </div>
                <div class="">
                    <button class="btn btn-primary provider-back-button" data-provider-class="exchange-provider-div">Choose another
                        email provider
                    </button>
                </div>
            </div>
        </div>
        <form id="exchange-auth-form">

            <input type="hidden" name="type" value="exchange">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" id="current_step_value" name="current_step" value="{{ $current_step }}">


            <div class="main-content-wrapper primary-bg px-3 py-3">
                <div class="row">
                    <div class="col-12">
                        <h5 class="mb-4 font-weight-bold">Sender settings</h5>
                    </div>
                    <div class="col">
                        <div class="form-group-label">
                            <input id="semail" type="email" name="email" class="form-control custom-input-field " placeholder="Sender email">
                            <label for="semail">Sender email</label>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group-label">
                            <input id="sname" type="text" name="name" class="form-control custom-input-field" placeholder="Sender name">
                            <label for="sname">Sender name</label>
                        </div>
                    </div>
                </div>
                <div class="row mt-4 justify-content-center">
                    <div class="col-6 offset-3">
                        <h5 class="mb-4 font-weight-bold">Credential
                            {{--                        <button class="btn btn-primary btn-sm float-right" disabled="true">Test</button>--}}
                        </h5>
                        <div class="form-group-label">
                            <input id="smtplogin" type="text" class="form-control custom-input-field" name="username" placeholder="User Name / Email">
                            <label for="smtplogin">Login</label>
                        </div>
                        <div class="form-group-label">
                            <input id="smtppass" type="password" class="form-control custom-input-field" name="password" placeholder="Password">
                            <label for="smtppass">Password</label>
                        </div>
                        <div class="d-flex justify-content-center">
                            <button class="btn btn-primary" type="submit" >Test & Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


{{-- Outlook div --}}
<div class="col-12 col-md-12 pt-5 outlook-provider-div d-none">
    <div class="main-section ">
        <div class="main-top px-3 py-2 border-bottom">
            <div class="d-flex align-items-center justify-content-between w-100">
                <div class="">
                    <h1 class="h3 mb-0">Email Provider</h1>
                    <p><b>You are using Outlook as email provider.</b></p>
                </div>
                <div class="">
                    <button class="btn btn-primary provider-back-button" data-provider-class="outlook-provider-div">Choose another
                        email provider
                    </button>
                </div>
            </div>
        </div>
        <form id="outlook-auth-form">

            <input type="hidden" name="type" value="outlook">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" id="current_step_value" name="current_step" value="{{ $current_step }}">

            <div class="main-content-wrapper primary-bg px-3 py-3">
                <div class="row">
                    <div class="col-12">
                        <h5 class="mb-4 font-weight-bold">Sender settings</h5>
                    </div>
                    <div class="col">
                        <div class="form-group-label">
                            <input id="semail" type="email" name="email" class="form-control custom-input-field " placeholder="Sender email">
                            <label for="semail">Sender email</label>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group-label">
                            <input id="sname" type="text" name="name" class="form-control custom-input-field" placeholder="Sender name">
                            <label for="sname">Sender name</label>
                        </div>
                    </div>
                </div>
                <div class="row mt-4 justify-content-center">
                    <div class="col-6 offset-3">
                        <h5 class="mb-4 font-weight-bold">Credential
                            {{--                        <button class="btn btn-primary btn-sm float-right" disabled="true">Test</button>--}}
                        </h5>
                        <div class="form-group-label">
                            <input id="smtplogin" type="text" class="form-control custom-input-field" name="username" placeholder="User Name / Email">
                            <label for="smtplogin">Login</label>
                        </div>
                        <div class="form-group-label">
                            <input id="smtppass" type="password" class="form-control custom-input-field" name="password" placeholder="Password">
                            <label for="smtppass">Password</label>
                        </div>
                        <div class="d-flex justify-content-center">
                            <button class="btn btn-primary" type="submit" >Test & Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
