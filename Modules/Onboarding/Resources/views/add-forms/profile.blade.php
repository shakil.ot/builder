<h3>
    Upload Profile Picture
    <span id="panel-description-text" data-trigger="hover" data-toggle="popover" title="Hint" data-html="true" data-content="">
        <span class="svg-icon svg-icon-green">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <rect x="0" y="0" width="24" height="24"/>
                    <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10"/>
                    <path d="M12,16 C12.5522847,16 13,16.4477153 13,17 C13,17.5522847 12.5522847,18 12,18 C11.4477153,18 11,17.5522847 11,17 C11,16.4477153 11.4477153,16 12,16 Z M10.591,14.868 L10.591,13.209 L11.851,13.209 C13.447,13.209 14.602,11.991 14.602,10.395 C14.602,8.799 13.447,7.581 11.851,7.581 C10.234,7.581 9.121,8.799 9.121,10.395 L7.336,10.395 C7.336,7.875 9.31,5.922 11.851,5.922 C14.392,5.922 16.387,7.875 16.387,10.395 C16.387,12.915 14.392,14.868 11.851,14.868 L10.591,14.868 Z" fill="#000000"/>
                </g>
            </svg><!--end::Svg Icon-->
        </span>
    </span>
</h3>
<form id="profile-edit-form">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="current_step_value" name="current_step" value="{{ $current_step }}">
    <div class="form-group row">
        <label class="col-xl-3 col-lg-3 col-form-label text-right"></label>
        <div class="col-lg-9 col-xl-6 text-center">
            <div class="image-input image-input-outline " id="profile_image">
                <div class="image-input-wrapper" style="background-image: url({{ asset('assets/theme/media/users/blank.png') }});"></div>
                <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                    <i class="fa fa-pen icon-sm text-muted"></i>
                    <input type="file" id="profile_image"
                           name="profile_image"
                           accept=".png, .jpg, .jpeg"
                           data-parsley-required-message="Profile image is required"
                           data-parsley-errors-container="#profile_image_error" required>
                </label>
            </div>
            <span class="form-text text-muted">Allowed file types: png, jpg, jpeg.</span>
        </div>
        <div class="custom-error-message w-100 text-center" id="profile_image_error"></div>
    </div>
    <div class="form-group text-right">
        <button type="submit" class="btn btn-green journey-mode-button" id="submit-profile"> <i class="la la-cloud-upload"></i>Update Profile</button>
    </div>
</form>
