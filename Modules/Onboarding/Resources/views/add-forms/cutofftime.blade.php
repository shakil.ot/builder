<h2>Cut off Time Setup</h2>
{!! Form::open(array('class'=>'m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed',
       'data-parsley-validate=""','id'=>'cutoff-time-form','method'=>'post')) !!}

<input type="hidden" name="_token" value="{{ csrf_token() }}">
<input type="hidden" id="current_step_value" name="current_step" value="{{ $current_step }}">
<input type="hidden" name="start-time-key" value="{{ \App\Models\UserSetting::CUT_OFF_START_TIME }}">
<input type="hidden" name="end-time-key" value="{{ \App\Models\UserSetting::CUT_OFF_END_TIME }}">

<div class="m-portlet__body" style="margin-top: 25px;">
    <div class="form-group row">
        <label class="col-form-label col-lg-4 col-sm-12" for="cutoff-start-timepicker">
            Send no Automated communications before
        </label>
        <div class="col-lg-4 col-md-9 col-sm-12">
            <input type='text' class="form-control" name="start-time" id="cutoff-start-timepicker" readonly placeholder="Select time"/>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-form-label col-lg-4 col-sm-12" for="cutoff-end-timepicker">
            Send no Automated communications after
        </label>
        <div class="col-lg-4 col-md-9 col-sm-12">
            <input type='text' class="form-control" id="cutoff-end-timepicker" name="end-time" readonly placeholder="Select time"/>
        </div>
    </div>
</div>

<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
    <div class="m-form__actions p-0">
        <div class="row">
            <div class="col">
                <button type="submit" id="cutoff-time-submit-button" class="btn btn-outline-main float-right journey-mode-button">
                    <i class="la la-pencil"></i> Update Cutoff Time
                </button>
            </div>
        </div>
    </div>
</div>


{!! Form::close() !!}


