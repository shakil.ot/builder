<h5 class="mr-b-20">Company Profile</h5>

<form id="company-profile-edit-form" >

    <div class="row">

        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="user_id" value="{{ $user_info['id'] }}">
        <input type="hidden" id="current_step_value" name="current_step" value="{{ $current_step }}">

        <input type="hidden" id="company-logo-image" name="company_logo_image" value="{{!empty($companyData->logo)?$companyData->logo:null}}">

        <div class="col-md-12">
            <div class="contact-details-cell">

                {!!  Form::text('company_name', !empty($companyData->company_name)?$companyData->company_name:null, [
                    'class'                         => 'form-control col-md-12 company_name',
                    'id'                            => 'sample1UserName',
                    "required"                      => "required",
                    "data-validation-error-msg"     => "Company name is require",
                    'Placeholder'                  => 'Company Name ',

                 ],null,true)!!}
            </div>
            <!-- /.contact-details-cell -->
        </div>
        <!-- /.col-md-6 -->
        <div class="col-md-6">
            <div class="contact-details-cell">
                {!!  Form::text('company_phone', !empty($companyData->phone)?$companyData->phone:null, [
                       'class'                         => 'form-control col-md-12 company_phone',
                       'id'                            => 'sample1UserName',
                       "required"                      => "",
                       "data-validation"               => "number",
                       "data-validation-error-msg"     => "Phone has to be an character",
                       'Placeholder'                   => 'Phone number ',

                    ],null,true)!!}
            </div>
            <!-- /.contact-details-cell -->
        </div>
        <!-- /.col-md-6 -->
        <div class="col-md-6">
            <div class="contact-details-cell">
                {!!  Form::text('web', !empty($companyData->web)?$companyData->web:null, [
                     'class'                         => 'form-control col-md-12 web',
                     'id'                            => 'sample1UserName',
                     "required"                      => "",
                     'Placeholder'                   => 'Website'
                  ],null,true)!!}
            </div>
            <!-- /.contact-details-cell -->
        </div>
        <!-- /.col-md-6 -->
        <div class="col-md-12">
            <p class="mt-3">About Us</p>
            <div class="contact-details-cell">
                {!!  Form::textarea('company_bio_data', !empty($companyData->company_bio_data)?$companyData->company_bio_data:null, [
                       'class'                         => 'form-control col-md-12 company_bio_data',
                       'id'                            => 'company_bio_data',
                       "required"                      => "",
                       'Placeholder'                   => 'Company Bio-data',
                       'data-toggle'                   => 'tinymce',
                        'style'                         => 'height:220px !important'
                    ],null,true)!!}
            </div>
            <!-- /.contact-details-cell -->
        </div>
        <!-- /.col-md-6 -->
        <div class="col-md-12">

            <label  >Color</label>
            <div class="contact-details-cell ">

                <div class="field required-field col-md-4">
                    <input required="" class="color" name="color" id="cal-color" value="{{ViewHelper::getHeaderColor(Auth::user())}}" type="hidden">
                    <span style="background-color: #fff" class="cal-color-box" data-color="#fff">&nbsp;</span>
                    <span style="background-color: #ff5a0d" class="cal-color-box" data-color="#ff5a0d">&nbsp;</span>
                    <span style="background-color: #a82b1d" class="cal-color-box" data-color="#a82b1d">&nbsp;</span>
                    <span style="background-color: #f99089" class="cal-color-box" data-color="#f99089">&nbsp;</span>
                    <span style="background-color: #4bb66b" class="cal-color-box" data-color="#4bb66b">&nbsp;</span>
                    <span style="background-color: #109710" class="cal-color-box" data-color="#109710">&nbsp;</span>
                    <span style="background-color: #14cc89" class="cal-color-box" data-color="#14cc89">&nbsp;</span>
                    <span style="background-color: #0c19dd" class="cal-color-box" data-color="#0c19dd">&nbsp;</span>
                    <span style="background-color: #17cccc" class="cal-color-box" data-color="#17cccc">&nbsp;</span>
                    <span style="background-color: #3a87ad" class="cal-color-box" data-color="#3a87ad">&nbsp;</span>
                    <span style="background-color: #eaff00" class="cal-color-box" data-color="#eaff00">&nbsp;</span>
                    <span style="background-color: #f903a5" class="cal-color-box" data-color="#f903a5">&nbsp;</span>
                    <span style="background-color: #b58682" class="cal-color-box" data-color="#b58682">&nbsp;</span>
                    <span style="background-color: #ddcf9e" class="cal-color-box" data-color="#ddcf9e">&nbsp;</span>
                    <span style="background-color: #9753cc" class="cal-color-box" data-color="#9753cc">&nbsp;</span>
                    <span style="background-color: #dd3a8b" class="cal-color-box" data-color="#dd3a8b">&nbsp;</span>
                    <span style="background-color: #cc2c00" class="cal-color-box" data-color="#cc2c00">&nbsp;</span>
                    <span style="background-color: #7c0015" class="cal-color-box" data-color="#7c0015">&nbsp;</span>
                    <span style="background-color: #100705" class="cal-color-box" data-color="#100705">&nbsp;</span>
                    <span style="background-color: #054118" class="cal-color-box" data-color="#054118">&nbsp;</span>
                    <span style="background-color: #917078" class="cal-color-box" data-color="#917078">&nbsp;</span>
                    <span style="background-color: #d4d7dd" class="cal-color-box" data-color="#d4d7dd">&nbsp;</span>
                    <span style="background-color: #939322" class="cal-color-box" data-color="#939322">&nbsp;</span>
                    <span style="background-color: #93906d" class="cal-color-box" data-color="#93906d">&nbsp;</span>
                    <span style="background-color: #447466" class="cal-color-box" data-color="#447466">&nbsp;</span>
                    <span style="background-color: #8c7870" class="cal-color-box" data-color="#8c7870">&nbsp;</span>
                    <span style="background-color: #15844c" class="cal-color-box" data-color="#15844c">&nbsp;</span>
                    <span style="background-color: #746b63" class="cal-color-box" data-color="#746b63">&nbsp;</span>
                    <span style="background-color: #744245" class="cal-color-box" data-color="#744245">&nbsp;</span>
                    <span style="background-color: #496e77" class="cal-color-box" data-color="#496e77">&nbsp;</span>
                    <span style="background-color: #405158" class="cal-color-box" data-color="#405158">&nbsp;</span>
                    <span style="background-color: #100860" class="cal-color-box" data-color="#100860">&nbsp;</span>
                    <span style="background-color: #16615a" class="cal-color-box" data-color="#16615a">&nbsp;</span>
                    <span style="background-color: #515715" class="cal-color-box" data-color="#515715">&nbsp;</span>
                    <span style="background-color: #552767" class="cal-color-box" data-color="#552767">&nbsp;</span>
                    <span style="background-color: #083c27" class="cal-color-box" data-color="#083c27">&nbsp;</span>
                    <span style="background-color: #00ff53" class="cal-color-box" data-color="#00ff53">&nbsp;</span>
                    <span style="background-color: #59b6e6" class="cal-color-box" data-color="#59b6e6">&nbsp;</span>
                    <span style="background-color: #e6b29b" class="cal-color-box" data-color="#e6b29b">&nbsp;</span>
                    <span style="background-color: #cddd81" class="cal-color-box" data-color="#cddd81">&nbsp;</span>
                    <span style="background-color: #cf6de7" class="cal-color-box" data-color="#cf6de7">&nbsp;</span>
                    <span style="background-color: #8d7fe1" class="cal-color-box" data-color="#8d7fe1">&nbsp;</span>
                    <span style="background-color: #b556dc" class="cal-color-box" data-color="#b556dc">&nbsp;</span>
                    <span style="background-color: #0079ff" class="cal-color-box" data-color="#0079ff">&nbsp;</span>
                    <span style="background-color: #e4eb9b" class="cal-color-box" data-color="#e4eb9b">&nbsp;</span>
                    <span style="background-color: #6bc4a3" class="cal-color-box" data-color="#6bc4a3">&nbsp;</span>
                    <span style="background-color: #7bbd0b" class="cal-color-box" data-color="#7bbd0b">&nbsp;</span>
                    <span style="background-color: #b9d085" class="cal-color-box" data-color="#b9d085">&nbsp;</span>
                    <span style="background-color: #bccd93" class="cal-color-box" data-color="#bccd93">&nbsp;</span>

                </div>
            </div>
            <!-- /.contact-details-cell -->
        </div>


        <div class="col-md-6">
            <label>Old Logo</label>
            <img src=" {{ViewHelper::getCompanyLogoForPublic(Auth::user()->id)}}"
                 style=" width:160px; ">

            <br/> <br/> <br/>
            <label>Change Logo</label>
            <div class="contact-details-cell">
                <div id = "file-uploader-company-logo-image" >

                </div >

                <code>allow image (JPEG,JPG,PNG) and size minimum(180 x 45 ) maximum(954 x 300)</code>
            </div>
            <!-- /.contact-details-cell -->
        </div>

    {{--<div class="col-md-6">--}}
    {{--<label>Favicon</label>--}}
    {{--<div class="contact-details-cell">--}}

    {{--<div id = "file-uploader-company-favicon-image" >--}}

    {{--</div >--}}
    {{--</div>--}}
    {{--<!-- /.contact-details-cell -->--}}
    {{--</div>--}}
    <!-- /.col-md-6 -->



        <div class="col-sm-12 btn-list">
            <button type="submit" class="btn btn-primary" id="submit-company-profile">Submit</button>
            <button type="button" class="btn btn-default">Cancel</button>
        </div>
        <!-- /.col-md-6 -->

    </div>
</form>