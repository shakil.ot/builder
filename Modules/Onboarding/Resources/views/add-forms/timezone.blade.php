<h3>
    Time Zone Setup
    <span id="panel-description-text" data-trigger="hover" data-toggle="popover" title="Hint" data-html="true" data-content="">
        <span class="svg-icon svg-icon-green">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <rect x="0" y="0" width="24" height="24"/>
                    <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10"/>
                    <path d="M12,16 C12.5522847,16 13,16.4477153 13,17 C13,17.5522847 12.5522847,18 12,18 C11.4477153,18 11,17.5522847 11,17 C11,16.4477153 11.4477153,16 12,16 Z M10.591,14.868 L10.591,13.209 L11.851,13.209 C13.447,13.209 14.602,11.991 14.602,10.395 C14.602,8.799 13.447,7.581 11.851,7.581 C10.234,7.581 9.121,8.799 9.121,10.395 L7.336,10.395 C7.336,7.875 9.31,5.922 11.851,5.922 C14.392,5.922 16.387,7.875 16.387,10.395 C16.387,12.915 14.392,14.868 11.851,14.868 L10.591,14.868 Z" fill="#000000"/>
                </g>
            </svg><!--end::Svg Icon-->
        </span>
    </span>
</h3>
{!! Form::open(array('class'=>'m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed',
       'data-parsley-validate=""','id'=>'timezone-form','method'=>'post')) !!}

<input type="hidden" name="_token" value="{{ csrf_token() }}">
<input type="hidden" id="current_step_value" name="current_step" value="{{ $current_step }}">
<input type="hidden" name="key" value="{{ \App\Models\UserSetting::USER_TIME_ZONE }}">
<input type="hidden" name="isFromJourneyMode" value="true">

<div class="m-portlet__body" style="margin-top: 25px;">
    <div class="form-group row">
        <label class="col-form-label col-lg-3 col-sm-12">
            Select A Timezone
        </label>
        <div class="col-lg-4 col-md-9 col-sm-12">
            <select class="form-control m-select2" id="timezone-select2" name="value">
{{--                @foreach($timeZones as $timeZone)--}}
{{--                    <option value="{{ $timeZone }}">--}}
{{--                        {{ str_replace('_',' ',$timeZone) }}--}}
{{--                    </option>--}}
{{--                @endforeach--}}
                    @foreach(TIMEZONE_LIST as $index=>$value)
                        <option value="{{ $index }}">
                        {{ $value }}
                        </option>
                    @endforeach
            </select>
            <small class="col-sm-12 ml-2 text-danger">***Time zone can't be updated later, please choose wisely</small>
        </div>
    </div>
</div>

<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
    <div class="m-form__actions p-0">
        <div class="row">
            <div class="col text-right">
                <button type="submit" id="timezone-submit-button" class="btn btn-green journey-mode-button">
                    <span class="svg-icon svg-icon-white">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <path d="M12,22 C7.02943725,22 3,17.9705627 3,13 C3,8.02943725 7.02943725,4 12,4 C16.9705627,4 21,8.02943725 21,13 C21,17.9705627 16.9705627,22 12,22 Z" fill="#000000" opacity="0.3"/>
                                <path d="M11.9630156,7.5 L12.0475062,7.5 C12.3043819,7.5 12.5194647,7.69464724 12.5450248,7.95024814 L13,12.5 L16.2480695,14.3560397 C16.403857,14.4450611 16.5,14.6107328 16.5,14.7901613 L16.5,15 C16.5,15.2109164 16.3290185,15.3818979 16.1181021,15.3818979 C16.0841582,15.3818979 16.0503659,15.3773725 16.0176181,15.3684413 L11.3986612,14.1087258 C11.1672824,14.0456225 11.0132986,13.8271186 11.0316926,13.5879956 L11.4644883,7.96165175 C11.4845267,7.70115317 11.7017474,7.5 11.9630156,7.5 Z" fill="#000000"/>
                            </g>
                        </svg><!--end::Svg Icon-->
                    </span>
                    Update Timezone Setting
                </button>
            </div>
        </div>
    </div>
</div>


{!! Form::close() !!}


