$(function () {

    const _token = document.head.querySelector("[property=csrf-token]").content;

    (function (global, $, _token) {


        var skipOrNot = false;
        var currentStepStore = 0;
        //We are mainly calling from this function.
        //Before adding new option to step form,
        //add the route used in submit form to the route file in case of access.
        var loadActionJourneyForm = function (step_number, instruction) {
            $("#preloader").show();
            global.mySiteAjax({
                url: route('user.onboarding.get-form'),
                type: 'post',
                data: {_token: _token, step_number: step_number, instruction: instruction},
                loadSpinner: true,
                success: function (response) {
                    if (response.status === 'validation-error') {
                        $("#preloader").hide();
                    } else if (response.status === 'success') {
                        if (response.nonMajor) {
                            $('#next-step-btn').show();
                        } else {
                            $('#next-step-btn').hide();
                        }
                        $('#action_journey_form_body').html(response.html);
                        changeButtons(response.current_step, response.max_step, response.user_info.journey_mode_last_landing_step);
                        formSetting(response.current_step, response.responseValue);
                        $('[data-toggle="popover"]').popover();

                        let currentStep = parseInt(response.current_step);
                        if (currentStep > maxStep) maxStep = currentStep;
                        setStepFormProgress(currentStep);

                        $("#preloader").hide();
                    } else if (response.status === 'error') {
                        $("#preloader").hide();
                        bootstrapNotify.error(response.html, "error");
                    } else if (response.status === 'finished') {
                        $("#preloader").hide();
                        window.location.replace(route("dashboard"));
                    }
                }
            });
        };
        loadActionJourneyForm(null, Instruction.NEXT);


        $('#next-step-btn').click(function () {
            if (!skipOrNot) {
                return false;
            }

            if (Steps.E_BOOK == currentStepStore) {
                bootstrapNotify.error("Please Select Any E-Book ", "Error");
                return false;
            }

            $(this).blur();
            var current_step = parseInt($(document).find('#current_step_value').val());

            if (current_step == Steps.AUTO_FOLLOWUP_SETTING) {
                autoFollowUp();
            } else {
                loadActionJourneyForm(current_step + 1, Instruction.NEXT);
            }
        });

        $('#previous-step-btn').click(function () {
            $(this).blur();
            var current_step = parseInt($(document).find('#current_step_value').val());
            loadActionJourneyForm(current_step - 1, Instruction.PREVIOUS);
        });

        function formSetting(currentStep, value) {

            currentStep += "";
            switch (currentStep) {
                case Steps.PROFILE_SETTING:
                    setThingsForProfile();
                    break;
                case Steps.TIMEZONE_SETTING:
                    setThingsForTimezone(value);
                    break;
                case Steps.SUBSCRIPTION_SELECT:
                    setThingsForSubscription(value);
                    break;
                case Steps.DOWNLOAD_INSTRUCTION_SELECT:
                    setThingsForDownload(value);
                    break;
                case Steps.OFFER_SELECT:
                    setThingsForOffer(value);
                    break;
                case Steps.AUTO_FOLLOWUP_SETTING:
                    setThingsForAutoFollowUp(value);
                    break;
                case Steps.THANK_YOU_SELECT:
                    setThingsForThinkYou(value);
                    break;
                case Steps.E_BOOK:
                    setThingsForEBook(value);
                    break;
            }
        }

        function setThingsForProfile() {
            new KTImageInput('profile_image');

            /*Submit profile image*/
            $('#profile-edit-form')
                .parsley(global.parsleySettings.settings)
                .on('form:success', function (formInstance) {
                    if (formInstance.isValid()) {
                        $('#preloader').show();
                        var formData = new FormData($('#profile-edit-form').get(0));

                        $.ajax({
                            url: route('user.profile.image.update'),
                            type: 'post',
                            data: formData,
                            contentType: false,
                            processData: false,
                            success: function (response) {
                                $('#preloader').hide();
                                if (response.status) {
                                    $('#profile-edit-form').trigger('reset');
                                    bootstrapNotify.success(response.message, 'Success');

                                    setTimeout(function () {
                                        var current_step = parseInt($(document).find('#current_step_value').val());
                                        loadActionJourneyForm(current_step + 1, Instruction.NEXT);
                                    }, 1000);
                                } else {
                                    bootstrapNotify.error(response.message, 'Error');
                                }
                            },
                            error: function (response) {
                                $('#preloader').hide();
                                if (response.status == 422) {
                                    global.generateErrorMessage(formInstance, response.responseJSON.errors);
                                    bootstrapNotify.error(response.responseJSON.message, "Error");
                                }
                            }
                        });

                    }
                }).on('form:submit', function () {
                console.log('submitted');
                return false;
            });

            $("#panel-description-text").attr("data-content", "<strong class='mb-2 d-block'>Please complete your profile with an image.</strong>" +
                "<span class='badge text-danger'>*</span>This section is optional and will be displayed on your profile.");

        }

        function setThingsForTimezone(value) {
            let timeZoneSelect2 = $('#timezone-select2');

            $(timeZoneSelect2).select2({
                placeholder: "Select a timezone"
            });
            $(timeZoneSelect2).val((value == null) ? defaultTimeZone : value.currentTimeZone); // Select the option with a value of '1'
            $(timeZoneSelect2).trigger('change'); // Notify any JS components that the value changed

            var addButton = $(document).find("#timezone-submit-button");
            addButton.on('click', function (event) {
                event.preventDefault();
                global.mySiteAjax({
                    url: route('user.onboarding.timezone.update'),
                    type: 'post',
                    data: $('#timezone-form').serialize(),
                    loadSpinner: true,
                    success: function (response) {
                        $('#preloader').hide();
                        if (response.status) {
                            bootstrapNotify.success(response.message, "success");
                            setTimeout(function () {
                                var current_step = parseInt($(document).find('#current_step_value').val());
                                loadActionJourneyForm(current_step + 1, Instruction.NEXT);
                            }, 1000);
                        } else if (response.status == 'error') {
                            bootstrapNotify.error(response.html, "Error");
                        }
                    }
                });
            });
            $("#panel-description-text").attr("data-content", "Please select your timezone below.\n" +
                "<br> " +
                "<code>Note :</code> Your time zone cannot be updated later so please choose wisely.");
        }

        function setThingsForSubscription(value) {
            let subscriptionSelect2 = $('#subscription-select2');

            $(subscriptionSelect2).select2({
                placeholder: "Select a timezone"
            });
            // subscription($('#subscription-select2').val());

            // defaultTemplateDataInsert($('#subscription-select2').val());
            thankYouPageAddDefault();

            $('.title-of-page').html('Subscription Template ');
            $("#panel-description-text").attr("data-content", "Select Your template From below .\n" +
                "<br>" +
                "<code>Note :</code> .");
        }

        function setThingsForDownload(value) {


            $('.title-of-page').html('Download Instruction Template ');

            $("#panel-description-text").attr("data-content", "Select Your template From below .\n" +
                "<br>  " +
                "<code>Note :</code> .");

        }

        function setThingsForOffer(value) {

            $('.title-of-page').html('Offer Template ');

            $("#panel-description-text").attr("data-content", " Sect Your Template from below.\n" +
                "<br> " +
                "<code>Note :</code> .");
        }

        function setThingsForAutoFollowUp(value) {

            $('.title-of-page').html('Auto Followup ');

            $("#panel-description-text").attr("data-content", ".\n" +
                "<br> " +
                "<code>Note :</code> .");
        }

        function setThingsForEBook(value) {

            $('.title-of-page').html('E Book  ');

            $("#panel-description-text").attr("data-content", "Please Select your e book from here  .\n" +
                "<br>  " +
                "<code>Note :</code> .");
        }

        function setThingsForThinkYou(value) {

            $('.title-of-page').html('Thank you Template ');

            $("#panel-description-text").attr("data-content", ".\n" +
                "<br> " +
                "<code>Note :</code> .");
        }

        function setStepFormProgress(currentStep) {
            if (currentStep !== maxStep) {
                $('#progress-step-' + (maxStep + 1)).removeClass('active');
            }
            for (let i = 0; i < maxStep; i++) {
                $('#progress-step-' + (i + 1)).attr('data-before', $('#progress-step-' + (i + 1)).attr('data-done')).addClass('step-click').click(function () {
                    loadActionJourneyForm($(this).attr('data-step'), Instruction.NEXT);
                });
                if (i !== currentStep) {
                    $('#progress-step-' + (i + 1)).addClass('done').removeClass('active');
                }
            }
            $('#progress-step-' + (currentStep + 1)).removeClass('done').addClass('active');
        }

        $(document).on('click', '.clickSelect', function () {
            $('.clickSelect').removeClass('active');
            $(this).addClass('active');
            $('#category').val($(this).attr('data-category_id'));
        });

        function changeButtons(currentStep, maxStep, setStep) {
            currentStepStore = currentStep;
            if (currentStep == 0) {
                $('#previous-step-btn').hide();
            } else {
                $('#previous-step-btn').show();
            }

            if (currentStep < maxStep - 1) {

                if (currentStep < setStep) {
                    skipOrNot = true;
                    $('#next-button-text').html("Skip");

                } else {
                    skipOrNot = false;

                    $('#next-button-text').html("");

                    if (currentStep == Steps.AUTO_FOLLOWUP_SETTING) {
                        $('#next-button-text').html("Save");
                        skipOrNot = true;
                    }
                }
            } else {
                $('#next-button-text').html("Finish");

            }
        }

        // global work

        function ajaxCall(url, data, callBack, errorCallBack = function (response) {
        }, type = 'POST') {
            $.ajax({
                url: url,
                type: type,
                data: data,
                success: function (response) {
                    $('#preloader').hide();
                    callBack(response);
                },
                error: function (response) {
                    $('#preloader').hide();
                    errorCallBack(response);
                }
            });
        }

        function defaultTemplateDataInsert(id) {
            let data = {
                id: id,
                _token: _token
            }
            ajaxCall(route('default-template-set-in-web-builder'), data, function (response) {
                console.log(response);
            });
        }


        $(document.body).on("click", ".submit-button", function (event) {
            event.preventDefault();

            $('#preloader').show();
            ajaxCall(route('add-template-in-web-builder'),
                {id: $(this).attr('data-id'), _token: _token},
                function (response) {

                    $('#preloader').hide();
                    if (response.status) {
                        $('#category-form').trigger('reset');
                        bootstrapNotify.success(response.html, 'Success');
                        setTimeout(function () {
                            var current_step = parseInt($(document).find('#current_step_value').val());
                            loadActionJourneyForm(current_step + 1, Instruction.NEXT);
                        }, 1000);
                    } else {
                        bootstrapNotify.error(response.message, 'Error');
                    }
                });
        });


        $(document).on('click', '.e-book-submit-button', function (event) {
            event.preventDefault();
            if ($(this).attr('data-id') == null) {
                return false;
            }

            $.ajax({
                url: route('ebook.set-book-by-select'),
                type: "POST",
                data: {
                    id: $(this).attr('data-id'),
                    _token: _token,
                },
                success: function (response) {
                    $('#preloader').hide();
                    if (response.status) {
                        bootstrapNotify.success(response.html, 'Success');
                        setTimeout(function () {
                            var current_step = parseInt($(document).find('#current_step_value').val());
                            loadActionJourneyForm(current_step + 1, Instruction.NEXT);
                        }, 1000);
                    } else {
                        bootstrapNotify.error(response.html, 'Error');
                    }
                }
            });
        });

        $(document).on('click', '.view-emailTemplate', function (event) {
            event.preventDefault();
            if ($(this).attr('data-id') == null) {
                return false;
            }

            let idOfTemplate = $(this).attr('data-id');

            $('#preloader').show();

            var detailPopUp = global.customPopup.show({
                header: 'Email Template',
                message: '',
                dialogSize: 'lg',
                dialogClass: 'prevent-to-show'
            });

            detailPopUp.on('shown.bs.modal', function () {
                var modal = $(this);

                global.mySiteAjax({
                    url: route('autofollowup.view-email-template-OnBoarding'),
                    type: 'post',
                    data: {
                        id: idOfTemplate,
                        _token: _token,
                    },
                    success: function (response) {
                        $('#preloader').hide();
                        window.nbUtility.ajaxErrorHandling(response, function () {
                            modal.find('.modal-body').html(response.html);
                            modal.removeClass('prevent-to-show');
                        });
                    }
                });
            });

            detailPopUp.on('hidden.bs.modal', function () {
                $(this).remove();
            });
        });

        $(document).on('click', '.view-emailTemplate-new', function (event) {
            event.preventDefault();
            if ($(this).attr('data-id') == null) {
                return false;
            }

            let idOfTemplate = $(this).attr('data-id');

            $('#preloader').show();
            $('#email-message-show').modal('show');

            $.ajax({
                url: route('autofollowup.view-email-template-OnBoarding'),
                type: 'post',
                data: {
                    id: idOfTemplate,
                    _token: _token,
                },
                success: function (response) {
                    $('#preloader').hide();
                    $('#email-message-body-show').html(response.html);
                }
            });
        });

        $(document).on('click', '.email-template-change', function (event) {
            event.preventDefault();
            if ($(this).attr('data-id') == null) {
                return false;
            }

            $('#email-desc-' + $(this).attr('data-key')).html($(this).attr('data-title'));
            $("#change-data-" + $(this).attr('data-key')).attr("data-id", $(this).attr('data-id'));

            $('#form-modal').modal('hide');


        });

        $(document).on('click', '.edit-emailTemplate', function (event) {
            event.preventDefault();
            if ($(this).attr('data-id') == null) {
                return false;
            }
            var id = $(this).attr('data-id');
            var keyValue = $(this).attr('data-key');

            $('#preloader').show();

            var detailPopUp = global.customPopup.show({
                header: 'Select your template',
                message: '',
                dialogSize: 'lg',
                dialogClass: 'prevent-to-show'
            });

            detailPopUp.on('shown.bs.modal', function () {
                var modal = $(this);

                global.mySiteAjax({
                    url: route('autofollowup.add-new-followup-form-OnBoarding'),
                    type: 'post',
                    data: {
                        id: id,
                        keyValue: keyValue,
                        _token: _token,
                    },
                    success: function (response) {
                        $('#preloader').hide();
                        window.nbUtility.ajaxErrorHandling(response, function () {
                            modal.find('.modal-body').html(response.html);
                            modal.removeClass('prevent-to-show');
                        });
                    }
                });
            });

            detailPopUp.on('hidden.bs.modal', function () {
                $(this).remove();
            });
        });

        $(document).on('click', '.email-template-change-selected', function () {
            bootstrapNotify.error('Already Selected ', 'Error');
        });


        function autoFollowUp() {
            var followUpIdsArray = [];
            followUpIdsArray.push($('#change-data-0').attr('data-id'));
            followUpIdsArray.push($('#change-data-1').attr('data-id'));
            followUpIdsArray.push($('#change-data-2').attr('data-id'));
            followUpIdsArray.push($('#change-data-3').attr('data-id'));
            followUpIdsArray.push($('#change-data-4').attr('data-id'));

            ajaxCall(route('autofollowup.add-in-onboarding'),
                {followUpIdsArray: followUpIdsArray, _token: _token},
                function (response) {
                    $('#preloader').hide();
                    if (response.status) {
                        bootstrapNotify.success(response.html, 'Success');
                        setTimeout(function () {
                            var current_step = parseInt($(document).find('#current_step_value').val());
                            loadActionJourneyForm(current_step + 1, Instruction.NEXT);
                        }, 1000);
                    } else {
                        bootstrapNotify.error(response.html, 'Error');
                    }
                });
        }

        function thankYouPageAddDefault() {
            console.log(" thank You Page Add Default ")
            $.ajax({
                url: route('thank-you-page-add'),
                type: 'post',
                data: {
                    _token: _token,
                },
                success: function (response) {
                    console.log(response);
                    console.log('thank-you-page-added');
                    console.log(" thank You Page Add Default ")
                }
            });
        }


    })(window, jQuery, _token);
});
