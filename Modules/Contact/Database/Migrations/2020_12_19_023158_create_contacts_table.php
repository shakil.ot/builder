<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('first_name')->index()->nullable();
            $table->string('last_name')->nullable();
            $table->string('number')->index()->nullable();
            $table->string('email')->nullable();
            $table->string('address')->nullable();
            $table->string('city', 64)->nullable();
            $table->string('zip', 12)->nullable();
            $table->tinyInteger('is_active_automated_email_sequence')->default(0)->comment(' (yes =  1 and no = 0)');
            $table->text('personal_note')->nullable();
            $table->tinyInteger('type')->comment('0 = subscriber, 1 = lead, 2 = client');
            $table->tinyInteger('source')->default(1);
            $table->tinyInteger('status')->comment('0 = inactive, 1 = active , 2 = block')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
