<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_infos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->comment('user as client');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('file_name',64);
            $table->string('original_name');
            $table->tinyInteger('status')->comment('0 = pending, 1 = queued, 2 = processing, 3 = success, 4 = failed')->default(0);
            $table->tinyInteger('file_type')->comment('1=csv, 2=text,3=xls, 4=xlsx')->default(1);
            $table->tinyInteger('contact_type')->comment('0 = subscriber, 1 = lead, 2 = client')->default(0);
            $table->integer('total_contacts')->default(0);
            $table->tinyInteger('action_type')->default(0);
            $table->integer('total_contacts_uploaded')->default(0);
            $table->text('error_infos')->nullable();
            $table->longText('mapped_contacts')->comment('All Mapped Contacts with headers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file_infos');
    }
}
