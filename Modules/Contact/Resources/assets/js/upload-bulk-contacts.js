importFileObj = '';
let importFile = (function (importFileObj) {
    $(function () {


        const _token = document.head.querySelector("[property=csrf-token]").content;

        $(document).on('click', '#importContactBtn', function (){
            $('#contactsTable').hide();
            $('#import-contact').show();
        });
        //
        $(document).on('click', '.cancleBTN', function (){
            $('#import-contact').hide();
            $('#contactsTable').show();
        });


        (function (global, $, _token) {
            function firstPageSetting() {
                $(document).on('click', '#next-bulk', function () {
                    submitImportForm($(this).parent().parent().parent());
                });
            }

            function submitImportForm(current_fs) {
                $('#preloader').show();
                $('.preloader-text').html('File Loading in progress!!');

                let form = $('#form1').get(0);
                let next_fs = $(current_fs).next();
                $.ajax({
                    url: route('import_parse'),
                    method: "POST",
                    data: new FormData(form),
                    loadSpinner: true,
                    contentType: false,
                    cache: false,
                    processData: false,

                    success: function (response) {
                        $('#preloader').hide();
                        if (response.status == 'success') {
                            $("#msform2").html(response.html);
                            toTheNextForm(current_fs, next_fs);
                            $(document).find(".prev-to-first").click(function () {
                                toThePreviousForm(next_fs, current_fs);
                            });
                            $(document).find(".next-to-third").click(function () {
                                submitMappingForm(next_fs);
                            });
                        } else if (response.status == 'validation-error') {
                            sweetAlert("File type not supported, please check and try again");
                        }
                    }

                });
            }

            function submitMappingForm(current_fs) {
                $('#preloader').show();
                $('.preloader-text').html('File mapping in Progress!!');
                let values = [];
                let error = 0;
                $('.map_field').each(function () {
                    // alert(
                    //
                    // );
                    if (jQuery.inArray($(this).val(), values) === -1) {
                        values.push($(this).val());
                    } else {
                        bootstrapNotify.error('Duplicate Input Column', 'error');
                        error++;
                    }
                });
                if (error > 0) {
                    $('#preloader').hide();
                    return false;
                } else {
                    if (jQuery.inArray("Email", values) === -1) {
                        bootstrapNotify.error("Email Field required", 'error');
                        $('#preloader').hide();
                        return false;
                    }
                    else {
                        let next_fs = $(current_fs).next();
                        let form = $('#form2').get(0);
                        $.ajax({
                            url: route('mapping_process'),
                            method: "POST",
                            data: new FormData(form),
                            loadSpinner: true,
                            //  dataType:'JSON',
                            contentType: false,
                            cache: false,
                            processData: false,
                            success: function (response) {
                                $('#preloader').hide();
                                $("#msform3").html(response.html);
                                toTheNextForm(current_fs, next_fs);
                                $(document).find(".prev-to-second").click(function () {
                                    toThePreviousForm(next_fs, current_fs);
                                });
                                $(document).find(".next-to-fourth").click(function () {
                                    submitFinalizeContacts(next_fs);
                                });
                            }
                        });
                    }
                }
            }

            function submitFinalizeContacts(current_fs) {
                $('#preloader').show();
                $('.preloader-text').html('File upload Finalize in progress!!');
                let form = $('#form3').get(0);
                let next_fs = $(current_fs).next();
                let baseUrl = route("imported-files");
                $.ajax({
                    url: route('finalize_import'),
                    method: "POST",
                    data: new FormData(form),
                    loadSpinner: true,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (response) {
                        if (response.status === 'success') {
                            $('#preloader').hide();
                            toTheNextForm(current_fs, next_fs);
                            $(document).find("#finish-button").click(function () {
                                // window.open(baseUrl + 'contact/manage/' + response.resultData['resultData']['file_id'], '_self');
                                window.open(baseUrl, '_self');
                            });
                        }
                    }

                });
            }

            //* Select js
            function nice_Select() {
                if ($('.product_select').length) {
                    $('select').niceSelect();
                }
            }

            function toTheNextForm(current_fs, next_fs) {

                let left, opacity, scale;
                $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

                //show the next fieldset
                next_fs.show();
                //hide the current fieldset with style
                current_fs.animate({
                        opacity: 0
                    },
                    {
                        step: function (now, mx) {
                            //as the opacity of current_fs reduces to 0 - stored in "now"
                            //1. scale current_fs down to 80%
                            scale = 1 - (1 - now) * 0.2;
                            //2. bring next_fs from the right(50%)
                            left = (now * 50) + "%";
                            //3. increase opacity of next_fs to 1 as it moves in
                            opacity = 1 - now;
                            current_fs.css({
                                'transform': 'scale(' + scale + ')',
                                'position': 'absolute'
                            });
                            next_fs.css({
                                'left': left,
                                'opacity': opacity
                            });
                        },
                        duration: 800,
                        complete: function () {
                            current_fs.hide();
                            // animating = false;
                        },
                        //this comes from the custom easing plugin
                        easing: 'easeInOutBack'
                    }
                );
            }

            function toThePreviousForm(current_fs, previous_fs) {
                let left, opacity, scale;

                // let previous_fs = $(this).parent().parent().prev();

                //de-activate current step on progressbar
                $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

                //show the previous fieldset
                previous_fs.show();
                //hide the current fieldset with style
                current_fs.animate({
                    opacity: 0
                }, {
                    step: function (now, mx) {
                        //as the opacity of current_fs reduces to 0 - stored in "now"
                        //1. scale previous_fs from 80% to 100%
                        scale = 0.8 + (1 - now) * 0.2;
                        //2. take current_fs to the right(50%) - from 0%
                        left = ((1 - now) * 50) + "%";
                        //3. increase opacity of previous_fs to 1 as it moves in
                        opacity = 1 - now;
                        current_fs.css({
                            'left': left
                        });
                        previous_fs.css({
                            'transform': 'scale(' + scale + ')',
                            'opacity': opacity
                        });
                    },
                    duration: 800,
                    complete: function () {
                        current_fs.hide();
                        // animating = false;
                    },
                    //this comes from the custom easing plugin
                    easing: 'easeInOutBack'
                });
            }

            /*Function Calls*/
            firstPageSetting();
            nice_Select();
            //Changing select as the file name
            $("#upload-file").change(function (e) {
                let fileName = e.target.files[0].name;
                $('.custom-file-label').html(fileName);
                $('#next-bulk').trigger('click');
            });


        })(window, jQuery, _token);
    });



})(importFileObj)
