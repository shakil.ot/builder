$(function () {
    let _token = document.head.querySelector("[property=csrf-token]").content;
    let contactId = $('#contactId').val();
    if (!$.curCSS) {
        $.curCSS = $.css;
    }
    $.extend(true, $.fn.dataTable.defaults, {
        "language": {
            "search": "",
            "searchPlaceholder": "Search",
            "sLengthMenu": "_MENU_ records",
            "paginate": {
                "first": "<<",
                "previous": '<span class="fa fa-chevron-left"></span>',
                "next": '<span class="fa fa-chevron-right"></span>',
                "last": ">>"
            },
        }
    });

    (function (global, $, _token) {

        let filterId = null;
        let request = null;

        let dataTable = $('#contact-table').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            stateSave: false,
            "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
            pageLength: 10,

            ajax: {
                url: route('contact-list'),
                data: function (data) {
                    data.contactId = contactId;
                    data.id= filterId,
                        data.requestData= request
                },
                type: 'get',
            },
            columns: [
                {data: 'full_name', name: 'first_name', "orderable": true, "searchable": true, width: "10%"},
                {data: 'data_phone', name: 'number', "orderable": true, "searchable": false, width: "10%"},
                {data: 'data_email', name: 'email', "orderable": true, "searchable": false, width: "10%"},
                {data: 'data_created_at', name: 'created_at', "orderable": true, "searchable": false, width: "10%"},
                {data: 'data_type', name: 'type', "orderable": true, "searchable": false, width: "10%"},
                {data: 'data_status', name: 'status', "orderable": true, "searchable": false, width: "10%"},
                {data: 'action', name: 'action', "orderable": true, "searchable": false, width: "40%"}
            ],
            dom: '<"top-toolbar row"<"top-left-toolbar col-md-9"lB><"top-right-toolbar col-md-3"f>>rt<"bottom-toolbar"<"bottom-left-toolbar"i><"bottom-right-toolbar"p>>',
            buttons: [
                {
                    extend: 'collection',
                    text: 'Export To',
                    autoClose: true,
                    attr: {
                        id: "export_button",
                        class: 'btn btn-outline-primary btn-sm m-btn dropdown-toggle ml-2',
                        style: '',
                        title: 'Export'
                    },
                    buttons: [
                        {
                            extend: 'csv',
                            title: "Contact List " + (new Date()).toDateString(),

                            exportOptions: {
                                columns: [0, 1, 2, 3, 4],
                                modifier: {selected: null}
                            }
                        }, {
                            extend: 'excel',
                            title: "Contact List " + (new Date()).toDateString(),

                            exportOptions: {
                                columns: [0, 1, 2, 3, 4],
                                modifier: {selected: null}
                            }
                        },{
                            extend: 'pdf',
                            title: "Contact List " + (new Date()).toDateString(),

                            exportOptions: {
                                columns: [0, 1, 2, 3, 4],
                                modifier: {selected: null}
                            }
                        },{
                            extend: 'print',
                            title: "Contact List " + (new Date()).toDateString(),

                            exportOptions: {
                                columns: [0, 1, 2, 3, 4],
                                modifier: {selected: null}
                            }
                        },{
                            extend: 'copy',
                            title: "Contact List " + (new Date()).toDateString(),

                            exportOptions: {
                                columns: [0, 1, 2, 3, 4],
                                modifier: {selected: null}
                            }
                        }
                    ],
                    fade: true
                },
            ]
        });

        //Columns visibility buttons' css
        $("#custom-colvis-button").click(function () {
            $(".buttons-columnVisibility").removeClass("dt-button").addClass("btn btn-outline-primary btn-sm m-btn btn-block");
            $(".buttons-colvisRestore").removeClass("dt-button").addClass("btn btn-outline-brand btn-sm m-btn btn-block");
        });

        // Add a contact
        $(document).on('click', '.addContact', function (e) {
            e.preventDefault();
            e.stopImmediatePropagation();
            $('#preloader').show();
            $('.preloader-text').html('Form incoming!!');
            let detailPopUp = global.customPopup.show({
                header: "Add New Contact",
                message: '',
                dialogSize: 'lg',
                dialogClass: 'prevent-to-show'
            });

            detailPopUp.on('shown.bs.modal', function () {
                let modal = $(this);
                modal.find('.modal-body').html();
                $('#preloader').show();
                global.mySiteAjax({
                    url: route('contact-add-form'),
                    type: 'post',
                    data: {"_token": _token},
                    loadSpinner: true,
                    success: function (response) {
                        window.nbUtility.ajaxErrorHandling(response, function () {
                            modal.find('.modal-body').html(response.html);
                            $('#preloader').hide();
                            $("#custom-modal").css('overflow-y', 'auto');
                            // Inputmask.init();


                            $('#contact-form').parsley().on('field:validated', function () {
                                let ok = $('.parsley-error').length === 0;
                                $('.bs-callout-info').toggleClass('hidden', !ok);
                                $('.bs-callout-warning').toggleClass('hidden', ok);
                            }).on('form:submit', function (e, formInstance) {
                                $('#preloader').show();
                                $('.preloader-text').html('Adding contact');
                                let phone = $('#user_phone').val();
                                $('#contact-form').find('.error-block').html('');

                                let data = {
                                    'first_name': $('#user_fname').val(),
                                    'last_name': $('#user_lname').val(),
                                    'user_id': $('#user_id').val(),
                                    'number': phone,
                                    'email': $('#user_email').val(),
                                    'note': $('#note').val(),
                                    'user_status': $('input[name="user_status"]:checked').val(),
                                    'automated_email_sequence': $('input[name="automated_email_sequence"]:checked') ? $('input[name="automated_email_sequence"]:checked').val() : '',
                                    'type': $('#type option:selected').val(),
                                };

                                global.mySiteAjax({
                                    url: route('add-contact'),
                                    type: 'post',
                                    data: {request: data, _token: _token},
                                    loadSpinner: true,
                                    success: function (response) {
                                        if (response.status === 'validation-error') {
                                            $('#preloader').hide();
                                        } else if (response.status == 'success') {
                                            bootstrapNotify.success(response.html, 'success');
                                            modal.modal('hide');
                                            $('#preloader').hide();
                                            dataTable.draw();

                                        } else if (response.status == 'error') {
                                            $('#preloader').hide();
                                            bootstrapNotify.error(response.html, 'error');
                                        }
                                        $('#preloader').hide();
                                    }
                                    , error: function () {
                                        $('#preloader').hide();
                                    }


                                });
                                $('html, body').css({
                                    overflow: 'auto',
                                    height: 'auto'
                                });
                                $('#preloader').hide();

                                // else {
                                //     bootstrapNotify.error("Please provide phone number", 'error');
                                //     $('#preloader').hide();
                                // }
                            });
                        });
                    }
                });
                modal.removeClass('prevent-to-show');
                global.spinnerDialog.hide();
            });

            detailPopUp.on('hidden.bs.modal', function () {
                $(this).remove();
                $('#preloader').hide();
            });

            $('#preloader').hide();
        });


        // Edit Contact
        $(document).on('click', '.editContact', function () {
            let contactId = $(this).attr("data-id");

            $('#preloader').show();
            $('.preloader-text').html('Contact Edit Form incoming!!');
            let detailPopUp = global.customPopup.show({
                header: 'Edit Contact',
                message: '',
                dialogSize: 'lg editContactModalAlterC',
                dialogClass: 'prevent-to-show'
            });

            detailPopUp.on('shown.bs.modal', function () {
                let modal = $(this);
                global.mySiteAjax({
                    url: route('contact-edit-form'),
                    type: 'post',
                    data: {'id': contactId, "_token": _token},
                    loadSpinner: false,
                    success: function (response) {
                        window.nbUtility.ajaxErrorHandling(response, function () {
                            modal.find('.modal-body').html(response.html);
                            $('#preloader').hide();
                            $("#custom-modal").css('overflow-y', 'auto');
                            // Inputmask.init();

                            $('#contact-edit-form').on('submit', function (e) {
                                e.preventDefault();
                                let data = $(this).serialize();

                                global.mySiteAjax({
                                    url: route('contact-update'),
                                    type: 'post',
                                    data: data,
                                    loadSpinner: true,
                                    success: function (response) {
                                        if (response.status === 'validation-error') {
                                            bootstrapNotify.error(response.html, 'error');
                                            $('#preloader').hide();
                                        } else if (response.status === 'success') {
                                            bootstrapNotify.success(response.html, 'success');
                                            modal.modal('hide');
                                            $("#contact-table").DataTable().ajax.reload();
                                            $('#preloader').hide();
                                            //setTimeout(location.reload.bind(location), 1000);

                                        } else if (response.status == 'error') {
                                            $('#preloader').hide();
                                            bootstrapNotify.error(response.html, 'error');
                                        }


                                    },
                                    error: function () {
                                        $('#preloader').hide();
                                    }

                                })
                            });
                        });
                    }
                });
                modal.removeClass('prevent-to-show');
                $('#preloader').hide();
                global.spinnerDialog.hide();
            });

            detailPopUp.on('hidden.bs.modal', function () {
                $(this).remove();
                $('#preloader').hide();
            });
        });



        // Delete Contact
        $(document).on('click', '.deleteContact', function () {
            let contactId = $(this).attr('data-id');
            Swal.fire({
                title: 'Are you sure?',
                html: "<b class='text-danger'> Sure to Delete this contact?</b>",
                type: 'warning',
                confirmButtonText: 'Yes, delete the contact',
                confirmButtonClass: 'btn btn-danger',
                showCancelButton: true,
                cancelButtonText: 'No, keep it!',
                cancelButtonClass: 'btn btn-green'
            }).then(function(result) {
                if (result.value) {

                    global.mySiteAjax({
                        url: route('contact-delete'),
                        type: 'post',
                        data: {contactId: contactId, _token: _token},
                        loadSpinner: true,

                        beforeSend: function () {
                            $('#preloader').show();
                        },
                        complete: function (xhr, stat) {
                            $('#preloader').hide();
                        },
                        success: function (response) {
                            if (response.status == 'success') {
                                toastr.success(response.html, "Success");
                                $("#contact-table").DataTable().ajax.reload();

                            } else if (response.status == 'error') {
                                toastr.error(response.html, "Error");
                            }

                        }
                    });
                } else {
                    $('#preloader').hide();
                }
            });
        });

        // Block Contact
        $(document).on('click', '.blockContact', function () {
            let contactId = $(this).attr('data-id');
            Swal.fire({
                title: 'Are you sure?',
                html: "<b class='text-danger'> Sure to block this contact?</b>",
                type: 'warning',
                confirmButtonText: 'Yes, block the contact',
                confirmButtonClass: 'btn btn-danger',
                showCancelButton: true,
                cancelButtonText: 'No, keep it!',
                cancelButtonClass: 'btn btn-green'
            }).then(function(result) {
                if (result.value) {

                    global.mySiteAjax({
                        url: route('block-contact-by-id'),
                        type: 'post',
                        data: {contactId: contactId, _token: _token},
                        loadSpinner: true,

                        beforeSend: function () {
                            $('#preloader').show();
                        },
                        complete: function (xhr, stat) {
                            $('#preloader').hide();
                        },
                        success: function (response) {
                            if (response.status == 'success') {
                                toastr.success(response.html, "Success");
                                $("#contact-table").DataTable().ajax.reload();

                            } else if (response.status == 'error') {
                                toastr.error(response.html, "Error");
                            }

                        }
                    });
                } else {
                    $('#preloader').hide();
                }
            });
        });

        // Activate Contact
        $(document).on('click', '.activateContact', function () {
            let contactId = $(this).attr('data-id');
            Swal.fire({
                title: 'Are you sure?',
                html: "<b class='text-success'> Sure to activate this contact?</b>",
                type: 'warning',
                confirmButtonText: 'Yes, activate the contact',
                confirmButtonClass: 'btn btn-green',
                showCancelButton: true,
                cancelButtonText: 'No, keep it!',
                cancelButtonClass: 'btn btn-danger'
            }).then(function(result) {
                if (result.value) {

                    global.mySiteAjax({
                        url: route('activate-contact-by-id'),
                        type: 'post',
                        data: {contactId: contactId, _token: _token},
                        loadSpinner: true,

                        beforeSend: function () {
                            $('#preloader').show();
                        },
                        complete: function (xhr, stat) {
                            $('#preloader').hide();
                        },
                        success: function (response) {
                            if (response.status == 'success') {
                                toastr.success(response.html, "Success");
                                $("#contact-table").DataTable().ajax.reload();

                            } else if (response.status == 'error') {
                                toastr.error(response.html, "Error");
                            }

                        }
                    });
                } else {
                    $('#preloader').hide();
                }
            });
        });


        // Filtering

        // Show and Hide Filter
        $(document).on('click','.showFilter', function (){
            $('.filter-div-load').html('<i class="fas fa-spinner fa-spin" style=" color: red; "></i>');
            setTimeout(function(){
                $('.filter-div-load').html('');
                showFilter();
            },500);

        });

        let filterShowHtml = '<i class="icon-xl fas fa-filter"></i> Filter';
        let filterHideHtml = '<div class="closeFilter"><i class="icon-sm far fa-window-close"></i> Close</div>';

        function showFilter() {
            let contactDiv = document.getElementById("contactsFilter");
            if (contactDiv.style.display === "none") {
                contactDiv.style.display = "block";
                $('.filerBtn').html(filterHideHtml);
            } else {
                contactDiv.style.display = "none";
                $('.filerBtn').html(filterShowHtml);

            }
        }

        $(document).on('click', '.closeFilter', function (){
            filterId = null;
            request = null;
            $("#contact-table").DataTable().ajax.reload();
        })

        $(document).on('click', '.addMoreCategory', function () {
            $('.new-category-append-div-load').html('<i class="fas fa-spinner fa-spin" style=" color: red; "></i>');
            setTimeout(function(){
                $('.new-category-append-div-load').html('');
                $('.new-category-append-div').append($("#new-category").html()).fadeIn(2000);
            },400);
        });

        $(document).on('click', '.removeEntry', function () {
            let el = $(this);
            $(this).html('<i class="fas fa-spinner fa-spin" style=" color: red; margin-top: 12px"></i>');
            setTimeout(function(){
                el.parent().parent().parent().remove();
                getTotalContact();
            },100);

        });


        $(document).on('change', '.event_action', function (e) {

            e.preventDefault();
            const _token = document.head.querySelector("[property=csrf-token]").content;

            var that = this;
            var filterId = this.value;

            $.ajax({
                url: route('get-action-condition-input-data'),
                type: 'post',
                data: {
                    _token: _token,
                    'id': filterId
                },
                loadSpinner: false,
                success: function (response) {
                    $(that).parent().parent().find('.event_action_condition_div').html('<i class="fas fa-spinner fa-spin" style=" color: red; margin-top: 12px"></i>');
                    $(that).parent().parent().find('.event_action_condition_value_div').html('<i class="fas fa-spinner fa-spin" style=" color: red; margin-top: 12px"></i>');

                    setTimeout(function(){
                        $(that).parent().parent().find('.event_action_condition_div').html(response.htmlCondition);
                        $(that).parent().parent().find('.event_action_condition_value_div').html(response.inputConditionValue);
                        getTotalContact(filterId);
                    },300);
                }
            });

        });


        $(document).on('change', '.event_action_condition', function (e) {

            e.preventDefault();
            const _token = document.head.querySelector("[property=csrf-token]").content;

            var that = this;
            var filterId = $(that).parent().parent().find('.event_action').val();
            getTotalContact(filterId);
        });


        $(document).on('keyup', '.input_value', function (e) {

            e.preventDefault();
            const _token = document.head.querySelector("[property=csrf-token]").content;

            var that = this;
            var filterId = $(that).parent().parent().find('.event_action').val();
            getTotalContact(filterId);
        });

        $(document).on('keyup', '.input_email', function (e) {

            e.preventDefault();
            filterId = $('#event_action option:selected').val();
            getTotalContact(filterId);
        });


        $(document).on('keyup', '.first_name', function (e) {

            e.preventDefault();
            const _token = document.head.querySelector("[property=csrf-token]").content;

            var that = this;
            var filterId = $(that).parent().parent().find('.event_action').val();
            getTotalContact(filterId);
        });


        $(document).on('keyup', '.last_name', function (e) {

            e.preventDefault();
            const _token = document.head.querySelector("[property=csrf-token]").content;

            var that = this;
            var filterId = $(that).parent().parent().find('.event_action').val();
            getTotalContact(filterId);
        });

        $(document).on('change', '.contact_type', function (e) {

            e.preventDefault();
            const _token = document.head.querySelector("[property=csrf-token]").content;

            var that = this;
            var filterId = $(that).parent().parent().find('.event_action').val();
            getTotalContact(filterId);


        });
        $(document).on('change', '.date_added', function (e) {

            e.preventDefault();
            const _token = document.head.querySelector("[property=csrf-token]").content;

            var that = this;
            var filterId = $(that).parent().parent().find('.event_action').val();
            getTotalContact(filterId);
        });

        function getTotalContact(params) {
            const _token = document.head.querySelector("[property=csrf-token]").content;

            let requestArray = [];
            $(document).find('.all-array-data').each(function (index) {

                requestArray.push({
                    event_action: $(this).find('.event_action').val(),
                    event_action_condition: $(this).find('.event_action_condition').val(),
                    input_value: $(this).find('.input_value').val(),
                    input_email: $(this).find('.input_email').val(),
                    first_name: $(this).find('.first_name').val(),
                    last_name: $(this).find('.last_name').val(),
                    contact_type: $(this).find('.contact_type').val(),
                    date_added: $(this).find('.date_added').val()
                });
            });

            filterId = params;
            request = requestArray;
            $("#contact-table").DataTable().ajax.reload();
        }

    })(window, jQuery, _token);
});