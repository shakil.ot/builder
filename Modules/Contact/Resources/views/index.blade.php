@extends('user.layout.app', ['menu' => 'contact'])
@section('title')
    Contacts
@endsection
@section('css')
    <style>
        #contact-table_filter input {
            padding: 0.55rem 0.75rem;
            font-size: 0.925rem;
            line-height: 1.35;
            border-color: #3699FF;
            border-radius: 0.42rem;
            outline: none;
        }
        #contact-table_length select {
            color: #3699FF;
            background-color: transparent;
            border-color: #3699FF;
            padding: 0.55rem 0.75rem;
            font-size: 0.925rem;
            line-height: 1.35;
            border-radius: 0.42rem;
        }
    </style>

    @endsection

@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="d-flex flex-column-fluid">
            <div class="container">
{{---------------- File Upload start Comment--}}
                <div class="importContact pane-section-form" style="display: none" id="import-contact">
                    <div class="col-12">
                        <div class="dt-card">
                            <div class="dt-card__header mb-0 custom-header-add-contact-file">
                                <div class="dt-card__heading d-block text-center w-100">
                                    <h3 class="dt-card__title">Add Contact File</h3>
                                </div>
                                <div class="dt-card__tools d-none">
                                    <ul class="dt-list dt-list-sm dt-list-cm-0 mt-0">
                                        <li class="dt-list__item">
                                            <button type="button" class="btn btn-primary open-section-content " data-role="open-section-content">Back</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <section class="multi_step_form">
                                <div id="msform">
                                    <div class="row">
                                        <div class="col-6 m-auto">
                                            <ul id="progressbar">
                                                <li class="active"><strong>Upload Contact</strong></li>
                                                <li><strong>Map Contacts</strong></li>
                                                <li><strong>Preview upload</strong></li>
                                                <li><strong>Finishing</strong></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <fieldset id="msform1">
                                        <form id="form1" data-validate="parsley">
                                            <input id="tokenValue" type="hidden" name="_token" value="{{csrf_token()}}">
                                            <h3>Upload Contact</h3>
                                            <h6>Please upload contact document.</h6>
{{--                                            <div class="form-row form-group">--}}
{{--                                                <div class="col-sm-12 my-3 contact-fields form-div"--}}
{{--                                                     id="new-group-append-in-add-bulk-contact-page">--}}
{{--                                                    <ul class="dt-list justify-content-center dt-list-cm-0 list-group">--}}
{{--                                                        <li class="dt-list__item list-inline-item mt-2 {{ !$lookUpPermission ? 'hide_o' : ''  }} " id="">--}}
{{--                                                            If you want to check lookup please check " Lookup Check " below--}}
{{--                                                            <span class="">--}}
{{--                                                            <div id="" class="tag-box active">--}}
{{--                                                                <label class="m-checkbox m-checkbox--check-bold m-checkbox--state-success">--}}
{{--                                                                    <input name="lookup_check" class="lookup_check" type="checkbox" value="0">--}}
{{--                                                                    <span style=" font-size: 16px;font-weight: bold; ">Lookup Check</span>--}}
{{--                                                                </label>--}}
{{--                                                            </div>--}}
{{--                                                        </span>--}}
{{--                                                        </li>--}}
{{--                                                    </ul>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
                                            <div class="row">
                                                <div class="col-sm-2 m-auto">
                                                    <div class="form-group">
                                                        <label for="contact-type"><h4>Contact Type</h4></label>
                                                        <select class="form-control form-control-lg" name="contact_type" id="contactType">
                                                            <option value="{{\Modules\Contact\Entities\Contact::SUBSCRIBER}}">Subscriber</option>
                                                            <option value="{{\Modules\Contact\Entities\Contact::LEAD}}">Lead</option>
                                                            <option value="{{\Modules\Contact\Entities\Contact::CLIENT}}" selected>Client</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-6 m-auto">
                                                    <div class="form-group fg_2">
                                                        <div class="custom-file">
                                                            <input type="file" class="custom-file-input csv_file" name="csv_file" id="upload-file" accept=".txt,.csv,.xlsx">
                                                            <label class="custom-file-label" for="upload-file">Select File</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="dt-card__tools addContactFilesButtons">
                                                <button type="button" class="action-button cancleBTN" data-role="open-section-content">  close &nbsp;<i class="la la-close" aria-hidden="true"></i></button>
                                                <button type="button" id="next-bulk" class="action-button" style="background:#0D9634">Next <i class="la la-arrow-right"></i></button>

                                            </div>

                                        </form>
                                    </fieldset>

                                    <!-- fieldsets -->

                                    <fieldset id="msform2"></fieldset>
                                    <fieldset id="msform3"></fieldset>
                                    <fieldset id="msform4">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <h1 class="pb-5">Data has been uploaded successfully</h1>
                                        <div class="dt-card__tools addContactFilesButtons">
                                            <button type="button" id="finish-button" class="action-button" style="background:#0D9634">Finish </button>

                                        </div>
                                    </fieldset>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
{{----------------/File Upload--}}


                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-custom card-stretch gutter-b">
                            <!--begin::Header-->

                            <!--end::Header-->
                            <!--begin::Body-->
                            <div class="card-body pt-0 pb-3" id="contactsTable" style="margin-top: -1px;">
                                            <!--begin::Subheader-->
                                                <div class="py-2 py-lg-4 subheader-solid" id="kt_subheader" style=" color: #6c7293; margin-left: -20px; margin-right: -30px; height: 54px;top: 65px;left: 0;right: 0; background-color: #ffffff;border-top: 1px solid #ECF0F3;">
                                                    <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                                                        <!--begin::Info-->
                                                        <div class="d-flex align-items-center flex-wrap mr-1">
                                                            <!--begin::Page Heading-->
                                                            <div class="d-flex align-items-baseline mr-5">
                                                                <!--begin::Page Title-->
                                                                <i class="icon-xl fas fa-address-book" style="color: #023B64;"></i>
                                                                <h3 class="text-dark font-weight-bold my-2 ml-2 mr-5"> Contact Details</h3>
                                                                <!--end::Page Title-->
                                                            </div>
                                                            <hr>
                                                            <!--end::Page Heading-->
                                                        </div>
                                                        <!--end::Info-->
                                                        <!--begin::Toolbar-->
                                                        <div class="d-flex align-items-center">
                                                            <!--begin::Actions-->
                                                            <ul class="nav nav-pills nav-pills--brand m-nav-pills--align-right m-nav-pills--btn-pill m-nav-pills--btn-sm" role="tablist">
                                                                <li class="nav-item m-tabs__item">  </li>
                                                            </ul>
                                                            <!--end::Actions-->
                                                        </div>
                                                        <!--end::Toolbar-->
                                                    </div>
                                                </div>
                                            <!--end::Subheader-->
                                <div style="margin:0 auto">
                                    <div class="pane-section-content open">
                                        <div class="m-subheader">
                                            <input type="hidden" id="contactId" value="{{ Auth::user()->id }}">
                                        </div>

                                        <!-- END: Subheader -->
                                        <div class="m-content">
                                            <div class="row">
                                                <div class="col-xl-12 margin-sm-dev">
                                                    <div class="m-portlet alterC">
                                                        <div style="width: 100%">
                                                            <div style="width: 30%" class="float-right">
                                                                <button class="btn btn-sm btn-primary btn-green submit-file float-right mt-2 mb-3 ml-auto mr-4 import-contact" id="importContactBtn">
                                                                    <i class="icon-sm fas fa-file-import"></i>
                                                                    Import Contacts
                                                                </button>
                                                                <button class="btn btn-sm btn-primary btn-green float-right mt-2 mb-3 ml-auto mr-4 addContact" id="addContactBtn">
                                                                    <i class="icon-sm far fa-address-book"></i>
                                                                    Add New Contact
                                                                </button>
                                                            </div>
                                                            <div style="width: 70%">
                                                                {{--    FILTER--}}
                                                                <div class="contactsFilter" id="contactsFilter" style="display: none; margin-top: 15px;">
                                                                    <div class="form-group row fullRow all-array-data">

                                                                        <div class="form-group col-md-4">
                                                                            <select class="form-control event_action" id="event_action" name="event_action[]">
                                                                                @foreach(\Modules\Contact\Entities\EventAction::$getAllTriggerEventAction as $action => $value)
                                                                                    <option value="{{ $action }}">{{ $value }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                        <div class="form-group col-md-4 event_action_condition_div">
                                                                            <select class="form-control event_action_condition" id="event_action_condition"
                                                                                    name="event_action_condition[]">
                                                                                @foreach(\Modules\Contact\Entities\EventAction::$getAllTriggerEventActionCondition1 as $action => $value)
                                                                                    <option value="{{ $action }}" {{ $value === 'is' ? 'selected' : '' }}>{{ $value }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                        <div class="form-group col-md-3 event_action_condition_value_div">
                                                                            <input type="text" value="" class="input_value form-control" name="input_value[]">
                                                                        </div>
                                                                    </div>

                                                                    {{--/FILTER--}}
                                                                    <div class="new-category-append-div"></div>
                                                                    <div class="new-category-append-div-load" style=" text-align: center; "></div>
                                                                    <div class="add-more-button-div mb-3" style="margin-top: -15px;">
                                                                        <button type="button" class="addMoreCategory btn btn-primary btn-green font-weight-bold btn-sm ">
                                                                            <i class="ki ki-plus icon-sm"></i>
                                                                            Add More
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                                <div class="filter-div-load" style=" text-align: center; "></div>
                                                                <div class="float-left showFilter">
                                                                    <button type="button" class="btn btn-sm btn-primary btn-green float-right mt-2 mb-3 ml-auto mr-4 filerBtn">
                                                                        <i class="icon-sm fas fa-filter"></i>
                                                                        Filter
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="m-portlet__body list_view_map_view">
                                                            <div class="tab-content quick-sends">
                                                                <div class="tab-pane active show" id="" role="tabpanel">
                                                                    <div class="table-design-border-wrapper">
                                                                        <div class="m-portlet__body table-responsive table-design-border mcOverflowVisible">
                                                                            <table id="contact-table" class="mt-1 table table-hover contact-dataTable">
                                                                                <colgroup>
                                                                                    <col span="1" style="width: 15%;">
                                                                                    <col span="1" style="width: 15%;">
                                                                                    <col span="1" style="width: 15%;">
                                                                                    <col span="1" style="width: 15%;">
                                                                                    <col span="1" style="width: 15%;">
                                                                                    <col span="1" style="width: 25%;">
                                                                                </colgroup>
                                                                                <thead>
                                                                                <tr>
                                                                                    <th><span>Name</span></th>
                                                                                    <th><span>Phone</span></th>
                                                                                    <th><span>Email</span></th>
                                                                                    <th><span>Created At</span></th>
                                                                                    <th><span>Type</span></th>
                                                                                    <th><span>Status</span></th>
                                                                                    <th><span>Action</span></th>
                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!--end::Body-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('script')
    <script src="{{ URL::asset('/assets/global/plugins/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
    <script src="{{ URL::asset('/assets/global/plugins/datatables/extensions/Buttons/js/buttons.colVis.min.js') }}"></script>

    <script type="text/template" id="new-category">
        {{--    FILTER--}}
        <div class="form-group row fullRow all-array-data">

        <div class="form-group col-md-4">
            <select class="form-control event_action" id="event_action" name="event_action[]">
                @foreach(\Modules\Contact\Entities\EventAction::$getAllTriggerEventAction as $action => $value)
                <option value="{{ $action }}">{{ $value }}</option>
                @endforeach
            </select>
{{--            <span class="form-text text-muted">Select action from search result.</span>--}}
        </div>
        <div class="form-group col-md-4 event_action_condition_div">
            <select class="form-control event_action_condition" id="event_action_condition"
                    name="event_action_condition[]">
                @foreach(\Modules\Contact\Entities\EventAction::$getAllTriggerEventActionCondition1 as $action => $value)
                <option value="{{ $action }}" {{ $value === 'is' ? 'selected' : '' }}>{{ $value }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-md-3 event_action_condition_value_div">
            <input type="text" value="" class="input_value form-control" name="input_value[]">
        </div>
        <div class="col-md-1">
            <div class="d-flex flex-column">
                <div class="removeEntry">
                    <i class="fa fa-times-circle" style="color:red; margin-top: 13px; cursor: pointer"></i>
                </div>
            </div>
        </div>

        </div>

        {{--    FILTER--}}
    </script>
        <script src="{{ asset('/assets/js/jquery-ui.min.js') }}"></script>
        <script src="{{ mix('js/upload-bulk-contacts.js') }}"></script>
{{--    <script>--}}
{{--        let Inputmask = {--}}
{{--            init: function () {--}}
{{--                $(".phone_usa").inputmask("mask", {mask: "(999) 999-9999"});--}}
{{--            }--}}
{{--        };--}}
{{--    </script>--}}

    <script src="{{asset('assets/dataTable/js/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/dataTable/js/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/dataTable/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/dataTable/js/buttons.print.min.js')}}"></script>

    <script src="{{ mix('js/contact-manage.js') }}"></script>
@endsection

