@extends('user.layout.app')

@section('content')

    <div class="full_width section-toggle change-content-section fb-page-management-wrapper">
        <div class="full-width pane-section-content open">
            <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
                <!--begin::Subheader-->
                <!--end::Subheader-->
                <!--begin::Entry-->
                <div class="d-flex flex-column-fluid">
                    <!--begin::Container-->
                    <div class="container">
                        <!--begin::Card-->
                        <div class="card card-custom">
                            <div class="card-body">

                                <div class="m-portlet ">
                                    <div class="m-portlet__body  m-portlet__body--no-padding">
                                        <div class="row m-row--no-padding m-row--col-separator-xl">
                                            <div class="col-md-12 col-lg-6 col-xl-3">
                                                <!--begin::Total Profit-->
                                                <div class="m-widget24">
                                                    <div class="m-widget24__item">
                                                        <h4 class="m-widget24__title mt-3">File Name</h4>
                                                        <br>
                                                        <span class="m-widget24__desc m--font-success">{{$fileData->original_name}}</span>
                                                        <div class="m--space-10"></div>

                                                    </div>
                                                </div>
                                                <!--end::Total Profit-->
                                            </div>
                                            <div class="col-md-12 col-lg-6 col-xl-3">
                                                <!--begin::New Feedbacks-->
                                                <div class="m-widget24">
                                                    <div class="m-widget24__item">
                                                        <h4 class="m-widget24__title mt-3">Total Contacts</h4>
                                                        <br>
                                                        <span class="m-widget24__desc m--font-info">Total Contacts</span>
                                                        <span class="m-widget24__stats m--font-info">{{$totalContact}}</span>
                                                        <div class="m--space-10"></div>

                                                    </div>
                                                </div>
                                                <!--end::New Feedbacks-->
                                            </div>
                                            <div class="col-md-12 col-lg-6 col-xl-3">
                                                <!--begin::New Orders-->
                                                <div class="m-widget24">
                                                    <div class="m-widget24__item">
                                                        <h4 class="m-widget24__title mt-3">Success</h4>
                                                        <br>
                                                        <span class="m-widget24__desc m--font-success">Uploaded Contacts</span>
                                                        <span class="m-widget24__stats m--font-success">{{$fileData->total_contacts_uploaded}}</span>
                                                        <div class="m--space-10"></div>

                                                    </div>
                                                </div>
                                                <!--end::New Orders-->
                                            </div>
                                            <div class="col-md-12 col-lg-6 col-xl-3">
                                                <!--begin::New Users-->
                                                <div class="m-widget24">
                                                    <div class="m-widget24__item">
                                                        <h4 class="m-widget24__title mt-3">
                                                            Failed
                                                        </h4>
                                                        <br>
                                                        <span class="m-widget24__desc m--font-danger">
													Failed Contacts
												</span>

                                                        @php
                                                            $totalFailed  = $fileData->total_contacts - $fileData->total_contacts_uploaded;
                                                         switch ($fileData->status) {
                                                            case \Modules\Contact\Entities\FileInfo::STATUS_PENDING:
                                                              echo 'will update after file process start.';
                                                              break;
                                                            case \Modules\Contact\Entities\FileInfo::STATUS_QUEUED:
                                                              echo 'will update after file process start.';
                                                              break;
                                                            case \Modules\Contact\Entities\FileInfo::STATUS_PROCESSING:
                                                              echo 'will update after file process complete.';
                                                              break;
                                                            case \Modules\Contact\Entities\FileInfo::STATUS_SUCCESS:
                                                              echo '<span class="m-widget24__stats m--font-danger">'.$totalFailed.'</span>';
                                                              break;
                                                            default:
                                                              echo "will update after file process start.";
                                                              break;

                                                          }
                                                        @endphp


                                                        <div class="m--space-10"></div>
                                                    </div>
                                                </div>
                                                <!--end::New Users-->
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <!--begin::Search Form-->

                                <!--end::Search Form-->
                                <!--begin: Datatable-->
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" class="fileId_in_imported_files" value="{{$fileId}}">

                                <table id="file_contacts_table"
                                       class="table table-striped table-bordered table-hover table-checkable no-wrap dataTable dtr-inline collapsed "
                                       style="width:100%">
                                    <thead>
                                    <tr>

                                        <th>Number</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Email</th>
                                        <th>Created at</th>

                                    </tr>
                                    </thead>
                                </table>


                                <!--end: Datatable-->
                            </div>
                        </div>
                        <!--end::Card-->
                    </div>
                    <!--end::Container-->
                </div>
                <!--end::Entry-->
            </div>
        </div>
    </div>


@endsection

@push('scripts')

    <script>

        $(function () {
            // Custom styling can be passed to options when creating an Element.
            // (Note that this demo uses a wider set of styles than the guide below.)
            $.extend(true, $.fn.dataTable.defaults, {
                "language": {
                    "search": "",
                    // search: "_INPUT_",
                    "searchPlaceholder": "Search",
                    "sLengthMenu": "_MENU_ records",
                    "paginate": {
                        "first": "<<",
                        "previous": '<span class="fa fa-chevron-left"></span>',
                        "next": '<span class="fa fa-chevron-right"></span>',
                        "last": ">>"
                    },
                }
            });

            const _token = '{{ csrf_token() }}';


            (function (global, $, _token) {

                var fileId = $('.fileId_in_imported_files').val();
                var dataTable = $('#file_contacts_table').DataTable({
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    stateSave: false,
                    "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                    pageLength: 10,
                    "renderer": "bootstrap",
                    ajax: {
                        url: "{{route('file-contacts-by-file-id')}}",
                        data: {
                            fileId: fileId
                        }
                    },
                    columns: [
                        {
                            data: 'contacts_number',
                            name: 'contacts.number',
                            "orderable": true,
                            "searchable": true,
                            width: "20%"
                        },
                        {
                            data: 'contacts_first_name',
                            name: 'contacts.first_name',
                            "orderable": true,
                            "searchable": true,
                            width: "15%"
                        },
                        {
                            data: 'contacts_last_name',
                            name: 'contacts.last_name',
                            "orderable": true,
                            "searchable": true,
                            width: "15%"
                        },
                        {
                            data: 'contacts_email',
                            name: 'contacts.email',
                            "orderable": true,
                            "searchable": true,
                            width: "10%"
                        },
                        {
                            data: 'captured_at',
                            name: 'contacts.created_at',
                            "orderable": true,
                            "searchable": true,
                            width: "20%"
                        }

                    ],
                    "createdRow": function (row, data) {
                        //  $('td', row).eq(0).html(data.first_name + " " + data.last_name);
                    },
                    // columnDefs: [
                    //     { responsivePriority: 1, targets: 0, className: 'noVis' },
                    //     { responsivePriority: 2, targets: 1, className: 'noVis' },
                    //     { responsivePriority: 3, targets: 2, className: 'noVis' },
                    //     { responsivePriority: 4, targets: -1 },
                    // ],
                    dom: '<"top-toolbar"<"top-left-toolbar"lB><"top-right-toolbar"f>>rt<"bottom-toolbar"<"bottom-left-toolbar"i><"bottom-right-toolbar"p>>',
                    buttons: [
                        {
                            extend: 'colvisGroup',
                            text: '<i class="fa fa-refresh pr-1"></i>Refresh',
                            attr: {
                                class: 'btn btn-outline-green btn-sm m-btn  m-btn--icon ml-4',
                                id: 'show-all-button'
                            },
                            show: ':hidden'
                        }
                    ]
                });

                //Columns visibility buttons' css
                $("#custom-colvis-button").click(function () {
                    $(".buttons-columnVisibility").removeClass("dt-button").addClass("btn btn-outline-green btn-sm m-btn btn-block");
                    $(".buttons-colvisRestore").removeClass("dt-button").addClass("btn btn-outline-green btn-sm m-btn btn-block");
                });

                $("#show-all-button").click(function () {
                    refreshTable();
                });

                function refreshTable(params) {
                    dataTable.columns().search('');
                    dataTable.search('');
                    dataTable.ajax.reload();
                }

                function openTab(url) {
                    // Create link in memory
                    var a = window.document.createElement("a");
                    a.target = '_blank';
                    a.href = url;

                    // Dispatch fake click
                    var e = window.document.createEvent("MouseEvents");
                    e.initMouseEvent("click", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
                    a.dispatchEvent(e);
                };


            })(window, jQuery, _token);
        });
    </script>



@endpush
