<form action="" autocomplete="off" id="contact-import-form" class="form-horizontal" enctype="multipart/form-data" onsubmit="event.preventDefault();">
    <input type="hidden" class="token" name="_token" value="{{csrf_token()}}">
    <input type="hidden" class="token" name="user_id" id="user_id" value="{{$userId}}">

    <div class="modal-body quick-send-setup-modal p-0">
        <div class="m-portlet">
            <div class="m-portlet__body p-0">
                <div class="add-address-wrapper">
                    <div class="row">
                        <div class="form-group m-form__group">
                            <label for="number">Type</label>
                            <select name="contact_type" id="contactType">
                                <option value="{{\Modules\Contact\Entities\Contact::SUBSCRIBER}}" selected>Subscriber</option>
                                <option value="{{\Modules\Contact\Entities\Contact::LEAD}}">Lead</option>
                                <option value="{{\Modules\Contact\Entities\Contact::CLIENT}}">Client</option>
                            </select>
                        </div>

                        <div class="col-md-6">
                            <div class="house-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group m-form__group">
                                            <label for="number">Contact File</label>
                                            <input class="form-control csv_file" id="csv_file" name="csv_file" placeholder="Upload File Here" type="file" required/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group m-form__group">
                                        <button class="btn btn-primary btn-green submit-file float-right" type="submit"><i class="la la-save"></i>Upload</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>{{-- {!! Form::close() !!}--}}


</form>

