@php
    const DB_FIELDS_FOR_IMPORTFILE =  [
      'First Name',
      'Last Name',
      'Address',
      'Phone No',
      'City',
      'State',
      'Country',
      'zip',
      'Email',
      'Skip Field'
      ];
@endphp


{{--@if($tagType != \Modules\AddressManagement\Entities\Tag::TYPE_CAMPAIGN)--}}
{{--    <span class="font-weight-bold">Selected Tag :</span>--}}
{{--    @if(isset($tag_name))--}}
{{--        @foreach($tag_name as $tag)--}}
{{--            <span class="badge communikit-bg mb-1 mr-1">{{$tag}}</span>--}}
{{--        @endforeach--}}
{{--    @else--}}
{{--        <span class="badge badge-danger mb-1 mr-1">No Tag Selected</span>--}}
{{--    @endif--}}
{{--@endif--}}

<form class="table-responsive" method="POST" {{--action="{{ route('mapping_process') }}"--}} id="form3">
    {{ csrf_field() }}
    <input type="hidden" name="fileData" value="{{$fileData}}"/>
    <input type="hidden" name="headers" value="{{$new_headers_file}}"/>
    <input type="hidden" name="lookup_check" value="{{ $lookup_check }}"/>
    {{--    <input type="hidden" name="tag_id" value="{{$tag_name_file}}" />--}}
    {{--    <input type="hidden" name="check_header" value="{{$check_header}}" />--}}
    <table class="table selected-tag-table">
        <tr>
            @foreach ($new_headers as $header)
                @if($header == 'Number')
                    <th class="table-header-add-contact-file"
                        style="background-color: #28a745!important;color: #f6dcdc">
                        Phone No
                    </th>
                @elseif(!in_array($header,DB_FIELDS_FOR_IMPORTFILE))

                    <th class="table-header-add-contact-file"
                        style="background-color: darkred!important;color: #f6dcdc">
                        Skip Field
                    </th>
                @else
                    <th class="table-header-add-contact-file"
                        style="background-color: #28a745!important;color: #f6dcdc">
                        {{ $header}}
                    </th>
                @endif
            @endforeach
        </tr>

        @foreach ($new_data as $rows)
            <tr>
                @foreach ($rows as $key => $value)
                    <td>{{ $value }}</td>
                @endforeach
            </tr>
        @endforeach
    </table>
</form>
<div class="dt-card__tools addContactFilesButtons">
    <button type="button" class="prev-to-second action-button"><i class="la la-arrow-left"></i> Back</button>
    <button type="button" class="next-to-fourth action-button" style="background:#0d9634">Confirm & Import <i
            class="la la-arrow-right"></i></button>

</div>

