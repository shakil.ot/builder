@php
    const DB_FIELDS_FOR_IMPORTFILE =  [
    'First Name',
    'Last Name',
    'Number',
    'Email',
    'Address',
    'State',
    'City',
    'zip',
    'Skip Field'
    ];



@endphp
<form class="table-responsive" method="POST" {{--action="{{ route('mapping_process') }}"--}} id="form2">
    {{ csrf_field() }}
    <input type="hidden" name="csv_data_file_id" value="{{ $csv_data_file }}"/>
    <input type="hidden" name="fileData" value="{{$fileData}}"/>
{{--    <input type="hidden" name="tag_name" value="{{$tag_name_file}}"/>--}}
{{--    <input type="hidden" name="tag_type" value="{{$tagType}}"/>--}}
    <input type="hidden" name="lookup_check" value="{{ $lookup_check }}"/>
    <table class="table selected-tag-table">
        <tr>
            @foreach ($client_headers as $key => $value)
                <td>
                    <select name="fields[{{ $key }}]" style="background-color:#0d9634;color: #fff;width: 100%!important"
                            class="map_field form-control">
                        <option value="No Field Selected[{{ $key }}]"
                                selected
                                style="background-color:#b59231;color: #f6dcdc"
                        > Select Field
                        </option>
                        @foreach (DB_FIELDS_FOR_IMPORTFILE as $db_field)
                            {{-- <option value="{{ (\Request::has('header')) ? $db_field : $loop->index --}}
                            @if($db_field == 'Skip Field')
                                <option value="No Field Selected[{{ $key }}]"
                                        style="background-color: darkred;color: #f6dcdc">{{ $db_field }}</option>
                            @elseif($db_field == 'Phone No')
                                <option value="Number"
                                        style="background-color: #28a745;color: #f6dcdc">{{ $db_field }}</option>
                            @else
                                <option value="{{ $db_field }}"
                                        style="background-color: #28a745;color: #f6dcdc">
                                    {{ $db_field }}
                                </option>
                            @endif
                        @endforeach
                    </select>
                </td>
            @endforeach
        </tr>
        @foreach ($csv_data as $rows)
            <tr>
                @foreach ($rows as $key => $value)
                    <td>{{ $value }}</td>
                @endforeach
            </tr>
        @endforeach

    </table>
</form>
<div class="dt-card__tools addContactFilesButtons">
    <button type="button" class="prev-to-first action-button"><i class="la la-arrow-left"></i> Back</button>
    <button type="button" class="next-to-third action-button" style="background:#0D9634">Import Data <i
            class="la la-arrow-right"></i></button>
</div>



<script>
    $(document).on('change', '.map_field', function () {
        $(this).removeAttr('style');
        if ($(this).val() == 'First Name' ||
            $(this).val() == 'Last Name' ||
            $(this).val() == 'Address' ||
            $(this).val() == 'Number' ||
            $(this).val() == 'City' ||
            $(this).val() == 'State' ||
            $(this).val() == 'Country' ||
            $(this).val() == 'zip' ||
            $(this).val() == 'Email'
        ) {
            $(this).css({'background-color': '#28a745', 'color': '#f6dcdc'});
        } else {
            $(this).css({'background-color': 'darkred', 'color': '#f6dcdc'});
        }

    })
</script>

