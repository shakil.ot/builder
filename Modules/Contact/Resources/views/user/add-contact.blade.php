<form action="" autocomplete="off" id="contact-form" class="form-horizontal" data-parsley-validate="" onsubmit="event.preventDefault();">
    <input type="hidden" class="token" name="_token" value="{{csrf_token()}}">
    <input type="hidden" class="token" name="user_id" id="user_id" value="{{$userId}}">

    <div class="modal-body quick-send-setup-modal p-0">
        <div class="m-portlet">
            <div class="m-portlet__body p-0">
                <div class="add-address-wrapper">
                    <div class="row">

                        <div class="col-md-6">
                            <div class="house-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group m-form__group">
                                            <label for="state">Type</label>
                                            <select class="form-control type" id="type" name="type" autocomplete="nope">
{{--                                                <option disabled selected>Select Type</option>--}}
                                                <option value="{{\Modules\Contact\Entities\Contact::SUBSCRIBER}}" selected>Subscriber</option>
                                                <option value="{{\Modules\Contact\Entities\Contact::LEAD}}">Lead</option>
                                                <option value="{{\Modules\Contact\Entities\Contact::CLIENT}}">Client</option>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group m-form__group">
                                            <label for="fname">First Name</label>
                                            <input type="text" autocomplete="nope"
                                                   class="form-control" id="user_fname"
                                                   name="first_name" placeholder="Enter First Name"
                                                   required
                                                   data-parsley-trigger="change focusout"
                                                   data-parsley-required-message="First Name is required!"
                                            />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group m-form__group">
                                            <label for="lname">Last Name</label>
                                            <input class="form-control" id="user_lname"
                                                   name="last_name" placeholder="Last Name"
                                                   type="text" autocomplete="nope"
                                                   required
                                                   data-parsley-trigger="change focusout"
                                                   data-parsley-required-message="Last Name is required!"
                                            />
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group m-form__group">
                                            <label for="number">Number</label>
                                            <input class="form-control phone_usa" id="user_phone" name="number" placeholder="Cell No." type="text" data-parsley-pattern="/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/" data-parsley-error-message="Complete phone number!" data-parsley-trigger="change focusout"/>
                                        </div>
                                    </div>
{{--                                    <div class="col-md-12">--}}
{{--                                        <div class="form-group m-form__group">--}}
{{--                                            <label for="email">Email</label>--}}
{{--                                            <input class="form-control" id="user_email" name="email"--}}
{{--                                                   placeholder="Valid Email Address" type="email"--}}
{{--                                                   data-parsley-trigger="change focusout" autocomplete="nope"--}}
{{--                                                   data-parsley-type="email"--}}
{{--                                                   required--}}
{{--                                                   data-parsley-required-message="Valid Email required!"--}}
{{--                                            />--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

                                    <div class="col-md-12">
                                        <div class="form-group m-form__group chooseRadioBtn">
                                            <label for="status">Status</label>

                                            <span>
                                                <input class="user_status" name="user_status" id="user_status_active" type="radio" checked="checked" value="{{\Modules\Contact\Entities\Contact::ACTIVE}}" /> <label for="user_status_active">Active</label>
                                            </span>
{{--                                            <span>--}}
{{--                                                <input class="user_status" name="user_status" id="user_status_inactive" type="radio" value="{{\Modules\Contact\Entities\Contact::INACTIVE}}" /> <label for="user_status_inactive">In-active</label>--}}
{{--                                            </span>--}}
                                            <span><input class="user_status" name="user_status" type="radio" id="user_status_blocked" value="{{\Modules\Contact\Entities\Contact::BLOCKED}}" /> <label for="user_status_blocked">Blocked</label>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
{{--                                <div class="col-md-12">--}}
{{--                                    <div id="locationField" class="form-group m-form__group">--}}
{{--                                        <label for="address">Address</label>--}}
{{--                                        <input id="user_address" class="form-control address" name="address" placeholder="Enter your address" type="search" />--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-md-6">--}}
{{--                                    <div class="form-group m-form__group">--}}
{{--                                        <label for="city">City</label>--}}
{{--                                        <input class="form-control field user_city" id="city" name="city" placeholder=" City" type="text" autocomplete="nope"  />--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                                <div class="col-md-6">--}}
{{--                                    <div class="form-group m-form__group">--}}
{{--                                        <label for="zip">Zip</label>--}}
{{--                                        <input class="form-control field user_zipcode" id="zip" name="zip" placeholder="Enter Zip Code" type="text" />--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                                <div class="col-md-12">
                                    <div class="form-group m-form__group">
                                        <label for="email">Email</label>
                                        <input class="form-control" id="user_email" name="email"
                                               placeholder="Valid Email Address" type="email"
                                               data-parsley-trigger="change focusout" autocomplete="nope"
                                               data-parsley-type="email"
                                               required
                                               data-parsley-required-message="Valid Email required!"
                                        />
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group m-form__group">
                                        <label for="note">Note</label>
                                        <textarea class="form-control" name="note" id="note"
                                                  cols="5" rows="5"
                                                  placeholder="Note" data-parsley-trigger="change focusout"
                                                  autocomplete="nope"
                                                  style="margin-top: 0px; margin-bottom: 0px; height: 82px;"
                                        ></textarea>
                                    </div>
                                <div class="col-md-12">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                               <span class="switch switch-sm switch-icon mt-5">
                                                <label>
                                                 <input type="checkbox" checked="" value="1" name="automated_email_sequence"/>
                                                 <span></span>
                                                </label>
                                               </span>
                                            </div>
                                            <label class="col-md-8 col-form-label mt-4" style="margin-left: -71px;">Automated Email Sequence</label>
                                        </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group m-form__group">
                                        <button class="btn btn-primary btn-green submit-file float-right" type="submit"><i class="la la-save"></i>Save Contact</button>
                                    </div>
                                </div>


{{--                            <div class="col-md-6">--}}
{{--                                <button class="btn btn-primary btn-green submit-file float-right" type="submit"><i class="la la-save"></i>Save Contact</button>--}}
{{--                            </div>--}}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    </div>{{-- {!! Form::close() !!}--}}


</form>

