@extends('user.layout.app',['menu' => 'imported-files'])
@section('title')
    Imported-Files
@endsection

@push('styles')
    <style>
        @media (max-width: 575px) {
            #imported-files-table_wrapper .top-left-toolbar div#imported-files-table_length {
                padding-bottom: 10px !important;
            }
        }
    </style>
@endpush

@section('content')

    <div class="container" style="margin-top: 15px;">
        <div class="alert alert-custom alert-white alert-shadow" role="alert">
            <div class="alert-icon">
                <span class="svg-icon svg-icon-primary svg-icon-xl">
                    <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><path d="M10.29 3.86L1.82 18a2 2 0 0 0 1.71 3h16.94a2 2 0 0 0 1.71-3L13.71 3.86a2 2 0 0 0-3.42 0z"></path><line x1="12" y1="9" x2="12" y2="13"></line><line x1="12" y1="17" x2="12.01" y2="17"></line></svg>
                </span>
            </div>
            <div class="alert-text">It takes up 1-5 minutes to start processing files.</div>
        </div>
    </div>

    <div class="full_width section-toggle change-content-section fb-page-management-wrapper">
        <div class="full-width pane-section-content open">
            <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
                <!--begin::Entry-->
                <div class="d-flex flex-column-fluid">
                    <!--begin::Container-->
                    <div class="container">
                        <!--begin::Card-->
                        <div class="card card-custom" style="margin-top: -15px;">
                            <div class="card-body">
                                <div class="" style="margin-bottom: 20px;">
                                    <h3><i class="icon-xl fas fa-file-import" style="color: #023B64; margin-right: 8px;"></i><strong>Imported Files</strong></h3>
                                </div>
                                <!--begin::Search Form-->

                                <!--end::Search Form-->
                                <!--begin: Datatable-->

                                <input type="hidden" name="_token" value="{{csrf_token()}}">

                                <table id="imported-files-table"
                                       class=" mt-1 table table-striped table-bordered table-hover table-checkable no-wrap dataTable dtr-inline collapsed "
                                       style="width:100%">
                                    <thead>
                                    <tr>

                                        <th>File Name</th>
                                        <th>Total Contact</th>
                                        <th>Uploaded</th>
                                        <th>Status</th>
                                        <th>Time</th>
                                        <th>Error Logs</th>
                                        <th>Action</th>

                                    </tr>
                                    </thead>
                                </table>

                            </div>
                        </div>
                        <!--end::Card-->
                    </div>
                    <!--end::Container-->
                </div>
                <!--end::Entry-->
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="tagModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body">
                    <p>Some text in the modal.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
@endsection

@push('scripts')

    <script !src="">
        var importFileObj = {personalTagData : '',};
    </script>
    {{--    <script src="{{ Module::asset('addressmanagement:js/import-file.js') }}"></script>--}}
    <script>
        var importFile = (function (importFileObj) {
            $(function () {
                $.extend(true, $.fn.dataTable.defaults, {
                    "language": {
                        "search": "",
                        // search: "_INPUT_",
                        "searchPlaceholder": "Search",
                        "sLengthMenu": "_MENU_ records",
                        "paginate": {
                            "first": "<<",
                            "previous": '<span class="fa fa-chevron-left"></span>',
                            "next": '<span class="fa fa-chevron-right"></span>',
                            "last": ">>"
                        },
                    }
                });

                const _token = document.head.querySelector("[property=csrf-token]").content;


                (function (global, $, _token) {

                    var dataTable = $('#imported-files-table').DataTable({
                        processing: true,
                        serverSide: true,
                        responsive: true,
                        stateSave: false,
                        "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                        pageLength: 10,

                        ajax: {
                            url: route('imported-files-by-user-id-datatable')
                        },
                        columns: [
                            {
                                data: 'original_name',
                                name: 'original_name',
                                "orderable": true,
                                "searchable": true,
                                width: "15%"
                            },
                            {
                                data: 'total_contacts',
                                name: 'total_contacts',
                                "orderable": true,
                                "searchable": true,
                                width: "15%"
                            },
                            {
                                data: 'total_contacts_uploaded',
                                name: 'total_contacts_uploaded',
                                "orderable": true,
                                "searchable": true,
                                width: "15%"
                            },
                            {
                                data: 'my_status',
                                name: 'status',
                                "orderable": true,
                                "searchable": true,
                                width: "15%"
                            },
                            {data: 'captured_at', name: 'created_at', "orderable": true, "searchable": true, width: "15%"},
                            {data: 'error_logs', name: 'error_infos', "orderable": true, "searchable": true, width: "20%"},
                            {
                                data: 'action',
                                name: 'action',
                                "orderable": false,
                                searchable: false,
                                width: "20%"
                            }

                        ],
                        "createdRow": function (row, data) {
                            //  $('td', row).eq(0).html(data.first_name + " " + data.last_name);
                        },
                        // columnDefs: [
                        //     { responsivePriority: 1, targets: 0, className: 'noVis' },
                        //     { responsivePriority: 2, targets: 1, className: 'noVis' },
                        //     { responsivePriority: 3, targets: 2, className: 'noVis' },
                        //     { responsivePriority: 4, targets: -1 },
                        // ],
                        dom: '<"top-toolbar row"<"top-left-toolbar col-md-6"lB><"top-right-toolbar col-md-6"f>>rt<"bottom-toolbar"<"bottom-left-toolbar"i><"bottom-right-toolbar"p>>',
                        buttons: [
                            {
                                extend: 'colvisGroup',
                                text: '<i class="fa fa-refresh pr-1"></i>Refresh',
                                attr: {
                                    class: 'btn btn-outline-green btn-sm m-btn  m-btn--icon ml-4',
                                    id: 'show-all-button'
                                },
                                show: ':hidden'
                            }
                        ]
                    });

                    //Columns visibility buttons' css
                    $("#custom-colvis-button").click(function () {
                        $(".buttons-columnVisibility").removeClass("dt-button").addClass("btn btn-outline-green btn-sm m-btn btn-block");
                        $(".buttons-colvisRestore").removeClass("dt-button").addClass("btn btn-outline-brand btn-sm m-btn btn-block");
                    });

                    $("#show-all-button").click(function () {
                        refreshTable();
                    });


                    function refreshTable(params) {
                        dataTable.columns().search('');
                        dataTable.search('');
                        dataTable.ajax.reload();
                    }

                    function openTab(url) {
                        // Create link in memory
                        var a = window.document.createElement("a");
                        a.target = '_blank';
                        a.href = url;

                        // Dispatch fake click
                        var e = window.document.createEvent("MouseEvents");
                        e.initMouseEvent("click", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
                        a.dispatchEvent(e);
                    };
                })(window, jQuery, _token);
            });
        })(importFileObj)

    </script>
@endpush
