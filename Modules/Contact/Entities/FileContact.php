<?php

namespace Modules\Contact\Entities;

use Illuminate\Database\Eloquent\Model;

class FileContact extends Model
{
    protected $fillable = [
        'file_info_id',
        'contact_id'
    ];
}
