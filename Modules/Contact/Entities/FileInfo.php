<?php

namespace Modules\Contact\Entities;


use Illuminate\Database\Eloquent\Model;

class FileInfo extends Model
{
    const STATUS_PENDING     = 0;
    const STATUS_QUEUED      = 1;
    const STATUS_PROCESSING  = 2;
    const STATUS_SUCCESS     = 3;
    const STATUS_FAILED      = 4;


    const FILE_TYPE_CSV      = 1;
    const FILE_TYPE_TXT      = 2;


    const ACTION_TYPE_DEFAULT =1;
    const ACTION_TYPE_BLOCK_CONTACTS =2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'user_id',
        'file_name',
        'original_name',
        'status',
        'total_contacts',
        'total_contacts_uploaded',
        'mapped_contacts',
        'contact_type',
        'action_type',
        'lookup_status',
    ];

//    public function files()
//    {
//        return $this->belongsTo(File::class, 'file_id','id');
//    }
}
