<?php

namespace Modules\Contact\Entities;

class EventAction
{

    //// ACTION LIST
    const PHONE_NUMBER = 1;
    const DATE_ADDED = 2;
    const EMAIL_ADDRESS = 3;
    const FIRST_NAME = 4;
    const LAST_NAME = 5;
    const TYPE = 6;

    //// CONDITION LIST 1
    const IS = 1;
    const START_WITH = 2;
    const ENDS_WITH = 3;
    const CONTAINS = 4;
    const DOES_NOT_CONTAINS = 5;
    const IS_NOT = 6;
    const IS_KNOW = 7;
    const IS_NOT_KNOWN = 8;
    const IS_IN_LIST = 9;
    const IS_NOT_IN_LIST = 10;

    const IS_EQUAL = 11;
    const IS_NOT_EQUAL = 12;




    public static $getAllTriggerEventAction = [

        self::PHONE_NUMBER => 'Phone Number',
        self::EMAIL_ADDRESS => 'Email Address',
        self::FIRST_NAME => 'First Name',
        self::LAST_NAME => 'Last Name',
        self::TYPE => 'Type',
        self::DATE_ADDED => 'Date',
    ];


    public static $getAllTriggerEventActionCondition1 = [

        self::IS => 'is',
        self::START_WITH => 'start with',
        self::ENDS_WITH => 'end with',
        self::CONTAINS => 'contains',
        self::DOES_NOT_CONTAINS => 'does not contains',
        self::IS_NOT => 'is not',
//        self::IS_KNOW => 'is know',
//        self::IS_NOT_KNOWN => 'is not known',
        self::IS_IN_LIST => 'is in list',
        self::IS_NOT_IN_LIST => 'is not in list'

    ];


    public static $getAllTriggerEventActionCondition2 = [

        self::IS_EQUAL => 'is equal',
        self::IS_NOT_EQUAL => 'is not equal'

    ];


    public static function getAllConditionDataType()
    {
        return [
            self::IS => '=',
            self::IS_EQUAL => '=',
            self::START_WITH => '%',
            self::ENDS_WITH => '%',
            self::CONTAINS => 'LIKE',
            self::DOES_NOT_CONTAINS => 'NOT LIKE',
            self::IS_NOT => '!=',
            self::IS_NOT_EQUAL => '!=',
//            self::IS_KNOW => 'is know',
//            self::IS_NOT_KNOWN => 'is not known',
            self::IS_IN_LIST => 'in',
            self::IS_NOT_IN_LIST => 'not in'
        ];
    }




}
