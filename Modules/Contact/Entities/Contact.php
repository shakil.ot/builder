<?php

namespace Modules\Contact\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    const INACTIVE = 0;
    const ACTIVE = 1;
    const BLOCKED = 2;

    const SUBSCRIBER = 0;
    const  LEAD = 1;
    const CLIENT = 2;




    protected $fillable = [
        'id',
        'user_id',
        'first_name',
        'last_name',
        'number',
        'email',
        'personal_note',
        'city',
        'zip',
        'type',
        'source',
        'status',
        'is_active_automated_email_sequence'
    ];



    public function users(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
