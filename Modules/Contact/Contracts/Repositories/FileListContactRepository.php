<?php

namespace Modules\Contact\Contracts\Repositories;

interface FileListContactRepository
{
    public function getContactsByFileId($file_id);
}