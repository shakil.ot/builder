<?php

namespace Modules\Contact\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserRepository
 * @package namespace App\Contracts\Repositories;
 */
interface FileContactRepository /*extends RepositoryInterface*/
{
    public function insert($request);

    public function insertFileContact($file_id, $contact_id);

    public function isExists( $contact_id, $file_id);
}
