<?php

namespace Modules\Contact\Contracts\Repositories;

interface FileInfoRepository
{
    public function insertFileInfo($fileData);

    public function getAllImportedFilesByUserId($userId);

    public function getFileByStatus($status);

    public function getTotalContactUploadedByFileId($fileId);

    public function updateErrorInfo($fileId, $errorInfos);

    public function updateTotalContact($id, $count);

    public function changeFileStatus($currentStatus, $changeStatus);

    public function getFileInfoByFileId($fileId);

    public function getUserByFileId($fileId);


}
