<?php

namespace Modules\Contact\Contracts\Repositories;

interface ContactRepository
{
    public function addContactByData($data);

    public function getAllContactsByUserId($userId);

    public function getContactById($id);

    public function updateByField($where, $field);

    public function getContactByUserId($userId);

    public function findByUserIdAndNumber($userId, $number);

    public function findByUserIdAndEmail($userId, $email);

    public function addContactWithEmailSequence($request);

    public function addContactWithoutEmailSequence($request);

    public function blockContact($userId, $contactId);

    public function getContactDataById($contactId);

    public function updateContact($contactId, $request);

    public function updateContactByWhere($where, $update);

    public function getContactByEmail($email);

    public function getContactByWhere($where);

    public function deleteContactById($contactId);

    public function getTotalSubscribersByDuration($userId, $durationType);

    public function getTotalLeadsByDuration($userId, $durationType);

    public function getAllContactsByUserIdAndType($userId, $type);

    public function insert($request, $userId, $source);

    public function updateOrCreateContact($where, $value);

    public function checkByCondition($where);

    public function getContactByCondition($where);

    public function activateContactById($id);

    public function blockContactById($id);

}
