<?php

namespace Modules\Contact\Contracts\Services;

interface ContactContact
{
    public function listContactData($request, $userId);

    public function addContact($request);

    public function addContactByData($data);

    public function getContactById($id);

    public function updateContactByField($request);

    public function getContactDataById($request);

    public function getContactByEmail($email);

    public function getContactByWhere($where);

    public function updateContact($request);

    public function updateContactByWhere($where, $update);

    public function deleteContactById($contactId, $userId);

    public function parseImport($request, $userId);

    public function processImportService($request);

    public function finalizeImport($request, $user);

    public function getTotalSubscribersByDuration($userId, $durationType);

    public function getTotalLeadsByDuration($userId, $durationType);

    public function getContactByCondition($where);

    public function activateContactById($id);

    public function blockContactById($id);

}
