<?php

namespace Modules\Contact\Contracts\Services;

interface FileListContactContact
{
    public function getContactsByFileId($file_id);

    public function getContactsAndCheckByFileId($fileId, $userId);
}