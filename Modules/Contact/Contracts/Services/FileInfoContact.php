<?php

namespace Modules\Contact\Contracts\Services;

interface FileInfoContact
{
    public function getAllImportedFilesByUserId($userId);
}
