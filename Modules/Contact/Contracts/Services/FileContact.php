<?php

namespace Modules\Contact\Contracts\Services;



interface FileContact
{
    public function insertCsvFile($request, $user_id);

    public function moveContactFile($fileData, $userId, $mapped_contacts ,  $lookup_check);

    public function getFileInfoByFileId($fileId);
}
