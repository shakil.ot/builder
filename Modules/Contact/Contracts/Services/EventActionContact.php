<?php

namespace Modules\Contact\Contracts\Services;

interface EventActionContact{

    public function getActionConditionAndInputData($constantValue);

    public function getAllContactInfo($request, $userId);

}


