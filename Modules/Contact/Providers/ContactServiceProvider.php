<?php

namespace Modules\Contact\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Modules\Contact\Contracts\Repositories\FileContactRepository;
use Modules\Contact\Contracts\Repositories\FileInfoRepository;
use Modules\Contact\Contracts\Repositories\FileListContactRepository;
use Modules\Contact\Contracts\Services\ContactContact;
use Modules\Contact\Contracts\Services\EventActionContact;
use Modules\Contact\Contracts\Services\FileContact;
use Modules\Contact\Contracts\Services\FileInfoContact;
use Modules\Contact\Contracts\Services\FileListContactContact;
use Modules\Contact\Http\Repositories\ContactRepositoryEloquent;
use Modules\Contact\Http\Repositories\FileContactRepositoryEloquent;
use Modules\Contact\Http\Repositories\FileInfoRepositoryEloquent;
use Modules\Contact\Http\Repositories\FileListContactRepositoryEloquent;
use Modules\Contact\Http\Services\ContactService;
use Modules\Contact\Contracts\Repositories\ContactRepository;
use Modules\Contact\Http\Services\EventActionService;
use Modules\Contact\Http\Services\FileInfoService;
use Modules\Contact\Http\Services\FileListContactService;
use Modules\Contact\Http\Services\FileService;

class ContactServiceProvider extends ServiceProvider
{
    /**
     * @var string $moduleName
     */
    protected $moduleName = 'Contact';

    /**
     * @var string $moduleNameLower
     */
    protected $moduleNameLower = 'contact';

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(module_path($this->moduleName, 'Database/Migrations'));
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ContactContact::class, ContactService::class);
        $this->app->bind(FileInfoContact::class, FileInfoService::class);
        $this->app->bind(FileContact::class, FileService::class);
        $this->app->bind(FileContactRepository::class, FileContactRepositoryEloquent::class);
        $this->app->bind(FileInfoRepository::class, FileInfoRepositoryEloquent::class);
        $this->app->bind(FileListContactContact::class, FileListContactService::class);
        $this->app->bind(FileListContactRepository::class, FileListContactRepositoryEloquent::class);
        $this->app->bind(ContactRepository::class, ContactRepositoryEloquent::class);
        $this->app->bind(EventActionContact::class, EventActionService::class);
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path($this->moduleName, 'Config/config.php') => config_path($this->moduleNameLower . '.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path($this->moduleName, 'Config/config.php'), $this->moduleNameLower
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/' . $this->moduleNameLower);

        $sourcePath = module_path($this->moduleName, 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], ['views', $this->moduleNameLower . '-module-views']);

        $this->loadViewsFrom(array_merge($this->getPublishableViewPaths(), [$sourcePath]), $this->moduleNameLower);
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/' . $this->moduleNameLower);

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, $this->moduleNameLower);
        } else {
            $this->loadTranslationsFrom(module_path($this->moduleName, 'Resources/lang'), $this->moduleNameLower);
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path($this->moduleName, 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function getPublishableViewPaths(): array
    {
        $paths = [];
        foreach (\Config::get('view.paths') as $path) {
            if (is_dir($path . '/modules/' . $this->moduleNameLower)) {
                $paths[] = $path . '/modules/' . $this->moduleNameLower;
            }
        }
        return $paths;
    }
}
