<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('contact/')->middleware(['auth'])->group(function() {
    Route::get('manage', 'ContactController@index')->name('contact-index');
    Route::get('list', 'ContactController@listContacts')->name('contact-list');
    Route::get('imported-files', 'ContactController@showImportedFiles')->name('imported-files');
    Route::get('imported-files/user', 'ContactController@showImportedFilesByUserIdDatatable')->name('imported-files-by-user-id-datatable');
    Route::get('file/{fileId}','ContactController@getContactsFileManagePage')->name('contacts-file-manage-page');
    Route::get('filecontacts/manage','ContactController@fileTotalContacts')->name('file-contacts-by-file-id');
    Route::post('import/form', 'ContactController@getContactImportForm')->name('contact-import-form');
    Route::post('add/form', 'ContactController@getContactAddForm')->name('contact-add-form');
    Route::post('edit/form', 'ContactController@getContactEditForm')->name('contact-edit-form');
    Route::post('add', 'ContactController@addContact')->name('add-contact');
    Route::post('update', 'ContactController@updateContact')->name('contact-update');
    Route::post('delete', 'ContactController@deleteContact')->name('contact-delete');
    Route::post('/importparse', 'ContactController@parseImport')->name('import_parse');
    Route::post('importprocess', 'ContactController@processImport')->name('mapping_process');
    Route::post('finalize-import', 'ContactController@finalizeImport')->name('finalize_import');
    Route::post('/get/action/condition/input/data', 'ContactController@getConditionAndValue')->name('get-action-condition-input-data');
    Route::post('/get/action/condition/input/data/total', 'ContactController@getConditionAndValueSubscribe')->name('get-action-condition-input-data-total');
    Route::post('/contact/field/update', 'ContactController@editContactByField')->name('contact-field-update');
    Route::post('/activate-contact-by-id', 'ContactController@activateContactById')->name('activate-contact-by-id');
    Route::post('/block-contact-by-id', 'ContactController@blockContactById')->name('block-contact-by-id');

});
