<?php

namespace Modules\Contact\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Modules\Contact\Contracts\Services\EventActionContact;
use Modules\Contact\Contracts\Services\FileContact;
use Modules\Contact\Contracts\Services\FileInfoContact;
use Modules\Contact\Contracts\Services\FileListContactContact;
use Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Contact\Contracts\Services\ContactContact;
use Modules\Contact\Entities\Contact;
use Yajra\DataTables\DataTables;

class ContactController extends Controller
{

    private $contactService;
    private $fileInfoService;
    private $fileService;
    private $eventActionService;
    private $fileListContactService;

    public function __construct(ContactContact $contactService,
                                FileInfoContact $fileInfoService,
                                EventActionContact $eventActionService,
                                FileContact $fileService,
                                FileListContactContact $fileListContactService
)
    {
        $this->contactService = $contactService;
        $this->fileInfoService = $fileInfoService;
        $this->fileService = $fileService;
        $this->eventActionService = $eventActionService;
        $this->fileListContactService = $fileListContactService;
    }

    public function index()
    {
        return view('contact::index');
    }

    public function listContacts(Request $request)
    {
        if (!$request->ajax()) {
            return redirect()->route('Dashboard');
        };

        return $this->contactService->listContactData($request, Auth::id());
    }

    public function getContactAddForm()
    {
        $userId = Auth::id();
        return Response::json([
            'html' => view('contact::user.add-contact', compact('userId'))->render(),
            'status' => 'success'
        ]);
    }


    public function getContactEditForm(Request $request)
    {
        $response = $this->contactService->getContactDataById($request);
        return Response::json([
            'html' => $response['html'],
            'status' => $response['status']
        ]);
    }

    public function addContact(Request $request)
    {
        $result = $this->contactService->addContact($request);

        return Response::json([
            'html' => $result['html'],
            'status' => $result['status'],
        ]);
    }

    public function updateContact(Request $request)
    {
        $request['userId'] = auth()->id();
        $result = $this->contactService->updateContact($request);

        return Response::json([
            'html' => $result['html'],
            'status' => $result['status'],
        ]);
    }


    public function deleteContact(Request $request)
    {
        $userId = auth()->id();
        $contactId = $request['contactId'];
        return $this->contactService->deleteContactById($contactId, $userId);
    }


//    public function getContactImportForm()
//    {
//        $userId = Auth::id();
//        return Response::json([
//            'html' => view('contact::user.import-contact', compact('userId'))->render(),
//            'status' => 'success'
//        ]);
//    }


    public function parseImport(Request $request)
    {
        $userId = Auth::id();
        $response = $this->contactService->parseImport($request, $userId);
        if($response['status'] == 'success'){
            return Response::Json([
                'html' => $response['html'],
                'status' => 'success'
            ]);
        }
        else {
            return Response::Json([
                'html' => 'Sorry! File validating failed!',
                'status' => 'validation-error'
            ]);
        }
    }

    public function processImport(Request $request)
    {
        $userId = Auth::id();

        $html = $this->contactService->processImportService($request);
        return Response::Json([
            'html' => $html,
            'status' => 'success'
        ]);
    }

    public function finalizeImport(Request $request)
    {
        $user = Auth::user();
        $resultData = $this->contactService->finalizeImport($request, $user);
        return Response::Json([
            'resultData' => $resultData,
            'userid' =>$user['id'],
            'status' => 'success'
        ]);
    }

    public function getContactsFileManagePage($fileId)
    {
        $userId = Auth::id();
        $fileData = $this->fileService->getFileInfoByFileId($fileId);
//        $totalContact = $this->fileListContactService->getContactsByFileId($fileId)->count();
        $totalContact = $fileData['total_contacts'];
        $data['menu'] = '';

        return view('contact::user.contact.file_contacts', $data)->with([
            'totalContact' => $totalContact,
            'userId' => $userId,
            'fileId' => $fileId,
            'fileData' => $fileData,
            'contactType' => 1
        ]);

    }

    public function fileTotalContacts(Request $request)
    {
        if (!$request->ajax()) {
            return redirect()->route('Dashboard');
        }

        $fileId = $request->get('fileId');
        $userId = Auth::id();

        $contactData = $this->fileListContactService->getContactsAndCheckByFileId($fileId, $userId);

        if($contactData)
        {
            $contactData = $this->fileListContactService->getContactsByFileId($fileId);
            return Datatables::of($contactData)
                ->addColumn('captured_at', function ($contactData){
                    if ($contactData->created_at != null && $contactData->created_at != '') {
                        return Carbon::parse($contactData->created_at)->format('m-d-yy h:i:sa');
                    } else {
                        return '<span class="label label-lg label-light-danger label-inline">N/A</span>';
                    }

                })
                ->addColumn('contacts_first_name', function ($contactData){
                    if($contactData->first_name != null && $contactData->first_name != ""){
                        return $contactData->first_name;
                    }
                    return "N/A";

                })
                ->addColumn('contacts_last_name', function ($contactData){
                    if($contactData->last_name != null && $contactData->last_name != ""){
                        return $contactData->last_name;
                    }
                    return "N/A";

                })
                ->addColumn('contacts_email', function ($contactData){
                    if($contactData->email != null && $contactData->email != ""){
                        return $contactData->email;
                    }
                    return "N/A";

                })
                ->addColumn('contacts_number', function ($contactData){
                    if($contactData->number != null && $contactData->number != ""){
                        return $contactData->number;
                    }
                    return "N/A";

                })
                ->rawColumns(['captured_at','contacts_number','contacts_email','contacts_last_name','contacts_first_name'])
                ->make(true);
        }
    }

    public function showImportedFiles()
    {
        return view('contact::user.imported-files')->with([
            'menu' => ''
        ]);
    }

    public function showImportedFilesByUserIdDatatable(Request $request)
    {
        if (!$request->ajax()) {
            return redirect()->route('Dashboard');
        }

        return $this->fileInfoService->getAllImportedFilesByUserId(auth()->id());
    }

    public function getConditionAndValue(Request $request)
    {
        if (!$request->ajax()) {
            return redirect()->route('dashboard');
        }

        $data = $this->eventActionService->getActionConditionAndInputData($request->get('id'));

        return Response::json([
            'htmlCondition' => $data['html_condition'],
            'inputConditionValue' => $data['input_condition_value'],
            'status' => 'success'
        ]);
    }

    public function getConditionAndValueSubscribe(Request $request)
    {
        if (!$request->ajax()) {
            return redirect()->route('dashboard');
        }

        $userId = Auth::id();

        $totalCount = $this->eventActionService->getAllContactInfo($request, $userId)->count();
        $dataContact = $this->eventActionService->getAllContactInfo($request, $userId)->pluck('id');


        return Response::json([
            'totalCount' => $totalCount,
            'data' => json_encode($dataContact),
            'status' => 'success'
        ]);
    }

    public function editContactByField(Request $request)
    {
        $request['user_id'] = Auth::id();
        $request[$request['fieldName']] = $request['value'];
        $response = $this->contactService->updateContactByField($request);

        if ($response['status'] == 'success') {
            return Response::json([
                'html' => $response['html'],
                'status' => $response['status'],
                'retValue' => $response['retValue']
            ]);
        } else {
            return Response::json([
                'html' => $response['html'],
                'status' => $response['status']
            ]);
        }

    }

    public function activateContactById(Request $request)
    {
        $contactId = $request->get('contactId');
        return Response::json($this->contactService->activateContactById($contactId));
    }
    public function blockContactById(Request $request)
    {
        $contactId = $request->get('contactId');
        return Response::json($this->contactService->blockContactById($contactId));
    }



}
