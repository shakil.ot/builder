<?php

namespace Modules\Contact\Http\Services;

use Carbon\Carbon;
use Modules\Contact\Contracts\Repositories\FileInfoRepository;
use Modules\Contact\Entities\FileInfo;
use Modules\Contact\Contracts\Services\FileInfoContact;
use Yajra\DataTables\DataTables;

class FileInfoService implements FileInfoContact
{
    private $fileInfoRepository;

    public function __construct(FileInfoRepository $fileInfoRepository)
    {
        $this->fileInfoRepository = $fileInfoRepository;
;
    }

    public function getAllImportedFilesByUserId($userId)
    {
        $importedFiles = $this->fileInfoRepository->getAllImportedFilesByUserId($userId);
        return Datatables::of($importedFiles)
            ->addColumn('action', function ($importedFiles) {

                return '<div class="dropdown dropup" data-dropdown-toggle="click" aria-expanded="true">
                        <a href="#" class="" data-toggle="dropdown" aria-expanded="false">
                            <button class="btn btn-primary btn-sm m-btn dropdown-toggle btn-green">Select an Action</button>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                              <a class="m-nav__link action"  href="' . route('contacts-file-manage-page', [$importedFiles->id]) . '">
                                <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                                <span class="m-nav__link-text">
                                    Details
                                </span>
                               </a>
                            <span class="m-nav__separator m-nav__separator--fit"></span>
                            <a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm d-block" style="margin: 0 20px !important;"> Close </a>

                        </div>

                    </div>';
            })
            ->addColumn('my_status', function ($importedFiles) {
                if ($importedFiles->status == FileInfo::STATUS_PENDING) {
                    return "Pending";

                } else if ($importedFiles->status == FileInfo::STATUS_QUEUED) {
                    return "Queued";

                } else if ($importedFiles->status == FileInfo::STATUS_PROCESSING) {
                    return "Processing";

                } else if ($importedFiles->status == FileInfo::STATUS_SUCCESS) {
                    return "Success";

                } else {
                    return "Failed";
                }
            })
            ->editColumn('captured_at', function ($importedFiles) {
                if ($importedFiles->created_at != null && $importedFiles->created_at != '') {
                    return '<span class="label label-lg label-light-danger label-inline">'.$importedFiles->created_at.'</span>';

                } else {
                    return '<span class="label label-lg label-light-danger label-inline">N/A</span>';
                }

            })
            ->addColumn('error_logs', function ($importedFiles) {
                if ($importedFiles->error_infos != null && $importedFiles->error_infos != '') {
                    $errors = json_decode($importedFiles->error_infos, true);
                    $errorMsg = "";
                    if (isset($errors['duplicate'])) {
                        $errorMsg =  '<span class="label label-lg label-light-danger label-inline">'.$errors['duplicate'].' duplicates</span>';
                    }
                    if (isset($errors['invalid'])) {
                        $errorMsg .=  '<span class="label label-lg label-light-danger label-inline mt-1">'.$errors['invalid'].' invalids</span>';
                    }

                    return $errorMsg;
                }
                return '<span class="label label-lg label-light-success label-inline"> No Error</span> <br>';


            })
            ->rawColumns(['action', 'my_status', 'captured_at', 'error_logs'])
            ->make(true);
    }
}
