<?php

namespace Modules\Contact\Http\Services;

use App\Services\Utility;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Contact\Http\Services\FileServiceForExcel;
use Modules\Contact\Contracts\Repositories\FileInfoRepository;
use Modules\Contact\Contracts\Services\FileContact;
use Modules\Contact\Entities\FileInfo;

class FileService implements FileContact
{
    private $fileInfoRepository;

    public function __construct(FileInfoRepository $fileInfoRepository)
    {
        $this->fileInfoRepository = $fileInfoRepository;
    }

    public function insertCsvFile($request, $userId)
    {
        $file = $request->file('csv_file');
        $fileData['custom_name'] = Utility::generateRandomString() . '.' . $file->getClientOriginalExtension();
        $fileData['originalName'] = $file->getClientOriginalName();
        $fileData['contact_type'] = $request->contact_type;
        $fileData['user_id'] = $userId;
        $basePath = base_path() . '/public/';
        if ($userId) {
            $targetRealPath = $basePath . 'upload/files/tmp/' . $userId;
        }
        if (file_exists($targetRealPath)) {
            File::deleteDirectory($targetRealPath);
        }
        if (!File::isDirectory($targetRealPath))
            File::makeDirectory($targetRealPath, 0775, true, true);

        $file->move($targetRealPath, $fileData['custom_name']);

        return $fileData;
    }

    public function moveContactFile($fileData, $userId, $mapped_contacts,  $lookup_status = 0)
    {
        if (is_null($lookup_status)){
            $lookup_status = 0;
        }
        $basePath = base_path() . '/public/';
        $oldPath = $basePath . 'upload/files/tmp/' . $userId . '/';
        $targetRealPath = $basePath . 'upload/files/Csv_Files/' . $userId . '/';
        $file_name = array_diff(scandir($oldPath), array('.', '..'))[2];

        $oldPath_file = $oldPath.''.$file_name;

        $new_file_name = '' . rand(40, 1100) . '_' . $file_name;
        $duplicate_file = public_path() . '/upload/files/Csv_Files/' . $userId;

        if (!file_exists($duplicate_file)) {
            File::makeDirectory($duplicate_file, 0775, true, true);
        }

        copy($oldPath_file, public_path() . '/upload/files/Csv_Files/' . $userId . '/' . $new_file_name);



        $total_contacts = 0;
        $fileData = json_encode($fileData);
        $fileData = json_decode($fileData, true);
        $old_path = $oldPath . $fileData['custom_name'];
        $new_path = $targetRealPath . $fileData['custom_name'];
        if (($handle = fopen($old_path, 'r')) !== FALSE) {
            $fp = file($old_path, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
            $total_contacts = count($fp);
            fclose($handle);
        }
        $fileData['custom_name'] = $new_file_name;

//        $tag_id = $fileData['group_id'];
        $fileData['mapped_contacts'] = $mapped_contacts;
        $fileData['total_contacts'] = $total_contacts;
        $fileData['lookup_status'] = $lookup_status;
        $fileData['contact_type'] = $fileData['contact_type'];

        $result = $this->fileInfoRepository->insertFileInfo($fileData);
        File::delete($old_path);
        if ($result) {
            return [
                'resultData' => $result,
                'status' => 'success',
                'success' => true
            ];
        }
        return [
            'resultData' => $result,
            'status' => 'error',
            'success' => false
        ];
    }

    public function processFileList()
    {
        $file = $this->fileInfoRepository->getFileByStatus(FileInfo::STATUS_PENDING);

        if ($file) {
            $this->fileInfoRepository->changeFileStatus(FileInfo::STATUS_PENDING, FileInfo::STATUS_PROCESSING);
            $basePath = base_path() . '/public/';
            $targetRealPath = $basePath . 'upload/files/Csv_Files/' . $file->user_id . '/' . $file->file_name;

            $this->processContactFile($targetRealPath, $file);
        }
        return false;
    }

    public function processContactFile($path, $file)
    {
        try {
            Excel::import((new FileServiceForExcel($file , $this->fileInfoRepository)) , $path);

            $this->fileInfoRepository->changeFileStatus(\Modules\Contact\Entities\FileInfo::STATUS_PROCESSING, FileInfo::STATUS_SUCCESS);

        } catch (Exception $e) {
            $this->fileInfoRepository->changeFileStatus(FileInfo::STATUS_PROCESSING, FileInfo::STATUS_FAILED);
            Log::info('Error :' . $e->getMessage());
        }
    }

    public function getFileInfoByFileId($fileId)
    {
        return $this->fileInfoRepository->getFileInfoByFileId($fileId);
    }
}
