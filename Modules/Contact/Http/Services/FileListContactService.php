<?php

namespace Modules\Contact\Http\Services;

use Modules\Contact\Contracts\Repositories\FileInfoRepository;
use Modules\Contact\Contracts\Repositories\FileListContactRepository;
use Modules\Contact\Contracts\Services\FileListContactContact;

class FileListContactService implements FileListContactContact
{
    private $fileListContactRepository;
    private $fileInfoRepository;

    public function __construct(
        FileListContactRepository $fileListContactRepository,
        FileInfoRepository $fileInfoRepository
    )
    {
        $this->fileListContactRepository = $fileListContactRepository;
        $this->fileInfoRepository = $fileInfoRepository;
    }

    public function getContactsByFileId($file_id)
    {
        return $this->fileListContactRepository->getContactsByFileId($file_id);
    }

    public function getContactsAndCheckByFileId($file_id, $userId)
    {
        $user_id = $this->fileInfoRepository->getUserByFileId($file_id)->user_id;

        if ($user_id == $userId) {
            return $this->fileListContactRepository->getContactsByFileId($file_id);
        } else {
            return false;
        }
    }
}