<?php

namespace Modules\Contact\Http\Services;

use App\Models\UserSetting;
use App\Services\Utility;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Contact\Contracts\Services\EventActionContact;
use Modules\Contact\Contracts\Services\FileContact;
use Modules\Contact\Entities\EventAction;
use Modules\Contact\Http\Services\FileServiceForExcelFrontend;
use Modules\Contact\Contracts\Repositories\ContactRepository;
use Modules\Contact\Contracts\Services\ContactContact;
use Illuminate\Validation\Rule;
use Modules\Contact\Entities\Contact;
use Modules\Package\Contracts\Service\UserBalanceContact;
use ViewHelper;
use Yajra\DataTables\DataTables;
use Modules\Contact\Http\Services\Validation\ContactValidator;


class ContactService implements ContactContact
{
    private $contactRepository;
    private $fileService;
    private $eventActionService;
    private $userBalanceService;
    private $contactValidator;

    public function __construct(ContactRepository $contactRepository,
                                FileContact $fileService,
                                EventActionContact $eventActionService,
                                UserBalanceContact $userBalanceService,
                                ContactValidator $contactValidator
    )
    {
        $this->contactRepository = $contactRepository;
        $this->fileService = $fileService;
        $this->eventActionService = $eventActionService;
        $this->userBalanceService = $userBalanceService;
        $this->contactValidator = $contactValidator;
    }

    public function getAllContactInfo($request, $userId)
    {
        $contactData = Contact::where('user_id', $userId);

        foreach ($request->requestData as $data) {
            /****### IF EVENT ACTION IS PHONE_NUMBER[10] ###****/

            if ($data['event_action'] == EventAction::PHONE_NUMBER) {
                if ($data['event_action_condition'] == EventAction::IS) {

                    $condition = EventAction::getAllConditionDataType()[EventAction::IS];
                    $contactData = $contactData->where('number', $condition, $data['input_value']);

                } elseif ($data['event_action_condition'] == EventAction::START_WITH) {

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::CONTAINS];
                    $startWith = EventAction::getAllConditionDataType()[EventAction::START_WITH];

                    $contactData = $contactData->where('number', $condition1, $startWith . '' . $data['input_value']);
                } elseif ($data['event_action_condition'] == EventAction::ENDS_WITH) {

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::CONTAINS];
                    $endWith = EventAction::getAllConditionDataType()[EventAction::ENDS_WITH];

                    $contactData = $contactData->where('number', $condition1, $data['input_value'] . '' . $endWith);
                } elseif ($data['event_action_condition'] == EventAction::CONTAINS) {

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::CONTAINS];
                    $startWith = EventAction::getAllConditionDataType()[EventAction::START_WITH];
                    $endWith = EventAction::getAllConditionDataType()[EventAction::ENDS_WITH];

                    $contactData = $contactData->where('number', $condition1, $startWith . '' . $data['input_value'] . '' . $endWith);
                } elseif ($data['event_action_condition'] == EventAction::DOES_NOT_CONTAINS) {

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::DOES_NOT_CONTAINS];
                    $startWith = EventAction::getAllConditionDataType()[EventAction::START_WITH];
                    $endWith = EventAction::getAllConditionDataType()[EventAction::ENDS_WITH];

                    $contactData = $contactData->where('number', $condition1, $startWith . '' . $data['input_value'] . '' . $endWith);

                } elseif ($data['event_action_condition'] == EventAction::IS_NOT) {
                    $condition1 = EventAction::getAllConditionDataType()[EventAction::IS_NOT];
                    $contactData = $contactData->where('number', $condition1, $data['input_value']);
                } elseif ($data['event_action_condition'] == EventAction::IS_IN_LIST) {

                    $contactData = $contactData->whereIn('number', [$data['input_value']]);
                } elseif ($data['event_action_condition'] == EventAction::IS_NOT_IN_LIST) {
                    $contactData = $contactData->whereNotIn('number', [$data['input_value']]);
                }
            }


            /****### IF EVENT ACTION IS EMAIL_ADDRESS[10] ###****/

            if ($data['event_action'] == EventAction::EMAIL_ADDRESS) {
                if ($data['event_action_condition'] == EventAction::IS) {

                    $condition = EventAction::getAllConditionDataType()[EventAction::IS];
                    $contactData = $contactData->where('email', $condition, $data['input_email']);

                } elseif ($data['event_action_condition'] == EventAction::START_WITH) {

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::CONTAINS];
                    $startWith = EventAction::getAllConditionDataType()[EventAction::START_WITH];

                    $contactData = $contactData->where('email', $condition1, $startWith . '' . $data['input_email']);
                } elseif ($data['event_action_condition'] == EventAction::ENDS_WITH) {

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::CONTAINS];
                    $endWith = EventAction::getAllConditionDataType()[EventAction::ENDS_WITH];

                    $contactData = $contactData->where('email', $condition1, $data['input_email'] . '' . $endWith);
                } elseif ($data['event_action_condition'] == EventAction::CONTAINS) {

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::CONTAINS];
                    $startWith = EventAction::getAllConditionDataType()[EventAction::START_WITH];
                    $endWith = EventAction::getAllConditionDataType()[EventAction::ENDS_WITH];

                    $contactData = $contactData->where('email', $condition1, $startWith . '' . $data['input_email'] . '' . $endWith);
                } elseif ($data['event_action_condition'] == EventAction::DOES_NOT_CONTAINS) {

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::DOES_NOT_CONTAINS];
                    $startWith = EventAction::getAllConditionDataType()[EventAction::START_WITH];
                    $endWith = EventAction::getAllConditionDataType()[EventAction::ENDS_WITH];

                    $contactData = $contactData->where('email', $condition1, $startWith . '' . $data['input_email'] . '' . $endWith);

                } elseif ($data['event_action_condition'] == EventAction::IS_NOT) {
                    $condition1 = EventAction::getAllConditionDataType()[EventAction::IS_NOT];
                    $contactData = $contactData->where('email', $condition1, $data['input_email']);
                } elseif ($data['event_action_condition'] == EventAction::IS_IN_LIST) {

                    $contactData = $contactData->whereIn('email', [$data['input_email']]);
                } elseif ($data['event_action_condition'] == EventAction::IS_NOT_IN_LIST) {
                    $contactData = $contactData->whereNotIn('email', [$data['input_email']]);
                }
            }

            /****### IF EVENT ACTION IS FIRST_NAME[10] ###****/
            if ($data['event_action'] == EventAction::FIRST_NAME) {

                /****### IF EVENT ACTION CONDITION IS[EQUAL TO(=) ] [1] ###****/
                if ($data['event_action_condition'] == EventAction::IS) {

                    $condition = EventAction::getAllConditionDataType()[EventAction::IS];
                    $contactData = $contactData->where('first_name', $condition, $data['first_name']);

                    /****### IF EVENT ACTION CONDITION START_WITH [LIKE %phonenumber] [2] ###****/
                } elseif ($data['event_action_condition'] == EventAction::START_WITH) {

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::CONTAINS];
                    $startWith = EventAction::getAllConditionDataType()[EventAction::START_WITH];

                    $contactData = $contactData->where('first_name', $condition1, $startWith . '' . $data['first_name']);
                } /****### IF EVENT ACTION CONDITION ENDS_WITH [LIKE phonenumber%] [3] ###****/
                elseif ($data['event_action_condition'] == EventAction::ENDS_WITH) {

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::CONTAINS];
                    $endWith = EventAction::getAllConditionDataType()[EventAction::ENDS_WITH];

                    $contactData = $contactData->where('first_name', $condition1, $data['first_name'] . '' . $endWith);
                } /****### IF EVENT ACTION CONDITION CONTAINS [LIKE %phonenumber%] [4] ###****/
                elseif ($data['event_action_condition'] == EventAction::CONTAINS) {

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::CONTAINS];
                    $startWith = EventAction::getAllConditionDataType()[EventAction::START_WITH];
                    $endWith = EventAction::getAllConditionDataType()[EventAction::ENDS_WITH];

                    $contactData = $contactData->where('first_name', $condition1, $startWith . '' . $data['first_name'] . '' . $endWith);
                } /****### IF EVENT ACTION CONDITION DOES_NOT_CONTAINS [NOT LIKE phonenumber] [5] ###****/
                elseif ($data['event_action_condition'] == EventAction::DOES_NOT_CONTAINS) {

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::DOES_NOT_CONTAINS];
                    $startWith = EventAction::getAllConditionDataType()[EventAction::START_WITH];
                    $endWith = EventAction::getAllConditionDataType()[EventAction::ENDS_WITH];

                    $contactData = $contactData->where('first_name', $condition1, $startWith . '' . $data['first_name'] . '' . $endWith);

                } /****### IF EVENT ACTION CONDITION IS_NOT [IS NOT] [6] ###****/
                elseif ($data['event_action_condition'] == EventAction::IS_NOT) {
                    $condition1 = EventAction::getAllConditionDataType()[EventAction::IS_NOT];
                    $contactData = $contactData->where('first_name', $condition1, $data['first_name']);
                } /****### IF EVENT ACTION CONDITION IS_IN_LIST [IN] [9] ###****/
                elseif ($data['event_action_condition'] == EventAction::IS_IN_LIST) {

                    $contactData = $contactData->whereIn('first_name', [$data['first_name']]);
                } /****### IF EVENT ACTION CONDITION IS_NOT_IN_LIST [IN] [10] ###****/
                elseif ($data['event_action_condition'] == EventAction::IS_NOT_IN_LIST) {
                    $contactData = $contactData->whereNotIn('first_name', [$data['first_name']]);
                }
            }

            /****### IF EVENT ACTION IS LAST_NAME[10] ###****/
            if ($data['event_action'] == EventAction::LAST_NAME) {

                /****### IF EVENT ACTION CONDITION IS[EQUAL TO(=) ] [1] ###****/
                if ($data['event_action_condition'] == EventAction::IS) {

                    $condition = EventAction::getAllConditionDataType()[EventAction::IS];
                    $contactData = $contactData->where('last_name', $condition, $data['last_name']);

                    /****### IF EVENT ACTION CONDITION START_WITH [LIKE %phonenumber] [2] ###****/
                } elseif ($data['event_action_condition'] == EventAction::START_WITH) {

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::CONTAINS];
                    $startWith = EventAction::getAllConditionDataType()[EventAction::START_WITH];

                    $contactData = $contactData->where('last_name', $condition1, $startWith . '' . $data['last_name']);
                } /****### IF EVENT ACTION CONDITION ENDS_WITH [LIKE phonenumber%] [3] ###****/
                elseif ($data['event_action_condition'] == EventAction::ENDS_WITH) {

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::CONTAINS];
                    $endWith = EventAction::getAllConditionDataType()[EventAction::ENDS_WITH];

                    $contactData = $contactData->where('last_name', $condition1, $data['last_name'] . '' . $endWith);
                } /****### IF EVENT ACTION CONDITION CONTAINS [LIKE %phonenumber%] [4] ###****/
                elseif ($data['event_action_condition'] == EventAction::CONTAINS) {

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::CONTAINS];
                    $startWith = EventAction::getAllConditionDataType()[EventAction::START_WITH];
                    $endWith = EventAction::getAllConditionDataType()[EventAction::ENDS_WITH];

                    $contactData = $contactData->where('last_name', $condition1, $startWith . '' . $data['last_name'] . '' . $endWith);
                } /****### IF EVENT ACTION CONDITION DOES_NOT_CONTAINS [NOT LIKE phonenumber] [5] ###****/
                elseif ($data['event_action_condition'] == EventAction::DOES_NOT_CONTAINS) {

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::DOES_NOT_CONTAINS];
                    $startWith = EventAction::getAllConditionDataType()[EventAction::START_WITH];
                    $endWith = EventAction::getAllConditionDataType()[EventAction::ENDS_WITH];

                    $contactData = $contactData->where('last_name', $condition1, $startWith . '' . $data['last_name'] . '' . $endWith);

                } /****### IF EVENT ACTION CONDITION IS_NOT [IS NOT] [6] ###****/
                elseif ($data['event_action_condition'] == EventAction::IS_NOT) {
                    $condition1 = EventAction::getAllConditionDataType()[EventAction::IS_NOT];
                    $contactData = $contactData->where('last_name', $condition1, $data['last_name']);
                } /****### IF EVENT ACTION CONDITION IS_IN_LIST [IN] [9] ###****/
                elseif ($data['event_action_condition'] == EventAction::IS_IN_LIST) {

                    $contactData = $contactData->whereIn('last_name', [$data['last_name']]);
                } /****### IF EVENT ACTION CONDITION IS_NOT_IN_LIST [IN] [10] ###****/
                elseif ($data['event_action_condition'] == EventAction::IS_NOT_IN_LIST) {
                    $contactData = $contactData->whereNotIn('last_name', [$data['last_name']]);
                }
            }

            /****### IF EVENT ACTION IS TYPE[10] ###****/
            if ($data['event_action'] == EventAction::TYPE) {

                /****### IF EVENT ACTION CONDITION IS[EQUAL TO(=) ] [1] ###****/
                if ($data['event_action_condition'] == EventAction::IS_EQUAL) {

                    $condition = EventAction::getAllConditionDataType()[EventAction::IS];
                    $contactData = $contactData->where('type', $condition, $data['contact_type']);

                    /****### IF EVENT ACTION CONDITION START_WITH [LIKE %phonenumber] [2] ###****/
                } /****### IF EVENT ACTION CONDITION IS_NOT [IS NOT] [6] ###****/
                elseif ($data['event_action_condition'] == EventAction::IS_NOT_EQUAL) {
                    $condition1 = EventAction::getAllConditionDataType()[EventAction::IS_NOT];
                    $contactData = $contactData->where('type', $condition1, $data['contact_type']);
                }
            }

            if ($data['event_action'] == EventAction::DATE_ADDED) {

                /****### IF EVENT ACTION CONDITION IS[EQUAL TO(=) ] [1] ###****/
                if ($data['event_action_condition'] == EventAction::IS) {

                    $condition = EventAction::getAllConditionDataType()[EventAction::IS];
                    $contactData = $contactData->whereDate('created_at', $condition, $data['date_added'] . ' 00:00:00');

                    /****### IF EVENT ACTION CONDITION START_WITH [LIKE %phonenumber] [2] ###****/
                } elseif ($data['event_action_condition'] == EventAction::START_WITH) {

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::CONTAINS];
                    $startWith = EventAction::getAllConditionDataType()[EventAction::START_WITH];

                    $contactData = $contactData->where('created_at', $condition1, $startWith . '' . $data['date_added'] . ' 00:00:00');
                } /****### IF EVENT ACTION CONDITION ENDS_WITH [LIKE phonenumber%] [3] ###****/
                elseif ($data['event_action_condition'] == EventAction::ENDS_WITH) {

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::CONTAINS];
                    $endWith = EventAction::getAllConditionDataType()[EventAction::ENDS_WITH];

                    $contactData = $contactData->where('created_at', $condition1, $data['date_added'] . ' 00:00:00' . '' . $endWith);
                } /****### IF EVENT ACTION CONDITION CONTAINS [LIKE %phonenumber%] [4] ###****/
                elseif ($data['event_action_condition'] == EventAction::CONTAINS) {

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::CONTAINS];
                    $startWith = EventAction::getAllConditionDataType()[EventAction::START_WITH];
                    $endWith = EventAction::getAllConditionDataType()[EventAction::ENDS_WITH];

                    $contactData = $contactData->where('created_at', $condition1, $startWith . '' . $data['date_added'] . ' 00:00:00' . '' . $endWith);
                } /****### IF EVENT ACTION CONDITION DOES_NOT_CONTAINS [NOT LIKE phonenumber] [5] ###****/
                elseif ($data['event_action_condition'] == EventAction::DOES_NOT_CONTAINS) {

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::DOES_NOT_CONTAINS];
                    $startWith = EventAction::getAllConditionDataType()[EventAction::START_WITH];
                    $endWith = EventAction::getAllConditionDataType()[EventAction::ENDS_WITH];

                    $contactData = $contactData->where('created_at', $condition1, $startWith . '' . $data['date_added'] . ' 00:00:00' . '' . $endWith);

                } /****### IF EVENT ACTION CONDITION IS_NOT [IS NOT] [6] ###****/
                elseif ($data['event_action_condition'] == EventAction::IS_NOT) {
                    $condition1 = EventAction::getAllConditionDataType()[EventAction::IS_NOT];
                    $contactData = $contactData->where('created_at', $condition1, $data['date_added'] . ' 00:00:00');
                } /****### IF EVENT ACTION CONDITION IS_IN_LIST [IN] [9] ###****/
                elseif ($data['event_action_condition'] == EventAction::IS_IN_LIST) {

                    $contactData = $contactData->whereIn('created_at', [$data['date_added'] . ' 00:00:00']);
                } /****### IF EVENT ACTION CONDITION IS_NOT_IN_LIST [IN] [10] ###****/
                elseif ($data['event_action_condition'] == EventAction::IS_NOT_IN_LIST) {
                    $contactData = $contactData->whereNotIn('created_at', [$data['date_added'] . ' 00:00:00']);
                }
            }

        }

        return $contactData->orderBy('id', 'desc');

    }

    public function getActionConditionAndInputData($constantValue)
    {

        if ($constantValue == EventAction::PHONE_NUMBER) {

            $htmlCondition = ' <select class="form-control event_action_condition" id="event_action" name="action">';
            foreach (EventAction::$getAllTriggerEventActionCondition1 as $action => $value) {
                $htmlCondition .= '<option value="' . $action . '">' . $value . '</option>';
            }
            $htmlCondition .= '</select><span class="form-text text-muted">Select action from search result.</span>';


            $htmlInputConditionValue = ' <input type="text" value="" class="input_value form-control" name="input_value[]">';


        } elseif ($constantValue == EventAction::EMAIL_ADDRESS) {

            $htmlCondition = ' <select class="form-control event_action_condition" id="event_action" name="action">';
            foreach (EventAction::$getAllTriggerEventActionCondition1 as $action => $value) {
                $htmlCondition .= '<option value="' . $action . '">' . $value . '</option>';
            }
            $htmlCondition .= '</select><span class="form-text text-muted">Select action from search result.</span>';


            $htmlInputConditionValue = ' <input type="text" value="" class="input_email form-control" name="input_email[]">';


        } elseif ($constantValue == EventAction::FIRST_NAME) {

            $htmlCondition = ' <select class="form-control event_action_condition" id="event_action" name="action">';
            foreach (EventAction::$getAllTriggerEventActionCondition1 as $action => $value) {
                $htmlCondition .= '<option value="' . $action . '">' . $value . '</option>';
            }
            $htmlCondition .= '</select><span class="form-text text-muted">Select action from search result.</span>';


            $htmlInputConditionValue = ' <input type="text" value="" class="first_name form-control" name="first_name[]">';


        } elseif ($constantValue == EventAction::LAST_NAME) {

            $htmlCondition = ' <select class="form-control event_action_condition" id="event_action" name="action">';
            foreach (EventAction::$getAllTriggerEventActionCondition1 as $action => $value) {
                $htmlCondition .= '<option value="' . $action . '">' . $value . '</option>';
            }
            $htmlCondition .= '</select><span class="form-text text-muted">Select action from search result.</span>';


            $htmlInputConditionValue = ' <input type="text" value="" class="last_name form-control" name="last_name[]">';


        }
//        elseif($constantValue == EventAction::DATE_ADDED){
//
//            $htmlCondition = ' <select class="form-control event_action_condition" id="event_action" name="action">';
//            foreach (EventAction::$getAllTriggerEventActionCondition1 as $action => $value){
//                $htmlCondition .='<option value="'.$action.'">'.$value.'</option>';
//            }
//            $htmlCondition .='</select><span class="form-text text-muted">Select action from search result.</span>';
//
//
//            $htmlInputConditionValue = ' <input type="date" value="' .date("Y-m-d", strtotime(date('m/d/Y')) )      .'" class="date_added form-control" name="date_added[]">';
//
//
//        }

        return
            [
                'html_condition' => $htmlCondition,
                'input_condition_value' => $htmlInputConditionValue
            ];

    }

    public function listContactData($request, $userId)
    {
        if (is_null($request->requestData)) {
            $contacts = $this->contactRepository->getAllContactsByUserId($userId);
        } else {
            $contacts = $this->getAllContactInfo($request, $userId);
        }

        return Datatables::of($contacts)
            ->addColumn('full_name', function ($contacts) {
                if ($contacts->first_name == null && $contacts->last_name == null) {
                    return '<span class="label label-lg label-light-danger  label-inline">N/A</span>';
                } else {
                    return $contacts->first_name . ' ' . $contacts->last_name;
                }
            })
            ->addColumn('data_phone', function ($contacts) {
                if ($contacts->number == null) {
                    return '<span class="label label-lg label-light-danger  label-inline">N/A</span>';
                } else {
                    return $contacts->number;
                }
            })
            ->addColumn('data_email', function ($contacts) {
                if ($contacts->email == null) {
                    return '<span class="label label-lg label-light-danger  label-inline">N/A</span>';
                } else {
                    return $contacts->email;
                }
            })
            ->addColumn('data_created_at', function ($contacts) {
                if ($contacts->created_at == null) {
                    return '<span class="label label-lg label-light-danger  label-inline">N/A</span>';
                } else {
                    return $contacts->created_at;
                }
            })
            ->addColumn('data_type', function ($contacts) {
                if ($contacts->type == Contact::SUBSCRIBER) {
                    return '<span class="label label-lg label-light-danger  label-inline" style="background-color: #023c64; color: #ffffff;">Subscriber</span>';
                } else if ($contacts->type == Contact::LEAD) {
                    return '<span class="label label-lg label-light-danger  label-inline" style="background-color: #023c64; color: #ffffff;">Lead</span>';
                } else if ($contacts->type == Contact::CLIENT) {
                    return '<span class="label label-lg label-light-danger  label-inline" style="background-color: #023c64; color: #ffffff;">Client</span>';
                } else {
                    return '<span class="label label-lg label-light-danger  label-inline">N/A</span>';
                }
            })
            ->addColumn('data_status', function ($contacts) {
                if ($contacts->status == Contact::INACTIVE) {
                    return '<span class="label label-lg label-light-danger  label-inline">Inactive</span>';
                } else if ($contacts->status == Contact::ACTIVE) {
                    return '<span class="label label-lg label-light-success label-inline" style="background-color: #023c64; color: #ffffff;" >Active</span>';
                } else if ($contacts->status == Contact::BLOCKED) {
                    return '<span class="label label-lg label-light-danger label-inline">Blocked</span>';
                }
            })
            ->addColumn('action', function ($contacts) {
                $blockActivateButton = '';
                if($contacts->status == Contact::ACTIVE){
                    $blockActivateButton = '<a href="javascript:void(0)" class="blockContact m-nav__link dropdown-item" id="contactId"
                            data-id="' . $contacts->id . ' ">
                            <span class="m-nav__link-text"><i class="la la-ban"></i> Block </span>
                            </a>';
                }else{
                    $blockActivateButton = '<a href="javascript:void(0)" class="activateContact m-nav__link dropdown-item" id="contactId"
                            data-id="' . $contacts->id . ' ">
                            <span class="m-nav__link-text"><i class="la la-check"></i> Activate </span>
                            </a>';
                }
                return '<div class="dropdown dropup" data-dropdown-toggle="click" aria-expanded="true">
                        <a href="#" class="" data-toggle="dropdown" aria-expanded="false">
                            <button class="btn btn-sm m-btn dropdown-toggle btn-green" style="background-color: #03385d">Select an Action</button>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">

                               <a href="javascript:void(0)" class="editContact m-nav__link dropdown-item" id="contactId"
                            data-id="' . $contacts->id . ' ">

                            <span class="m-nav__link-text"><i class="la la-pencil"></i> Edit </span>
                            </a>
                              <a href="javascript:void(0)" class="deleteContact m-nav__link dropdown-item" id="contactId"
                            data-value="delete" data-id="' . $contacts->id . '">

                            <span class="m-nav__link-text"><i class="la la-trash-o"></i> Delete </span>
                            </a>
                             <a href="/timeline/' . $contacts->id . ' "
                            target="_blank" class="m-nav__link dropdown-item">

                            <span class="m-nav__link-text"><i class="fa fa-history"></i> View History </span>
                            </a>'.$blockActivateButton.'
                            <span class="m-nav__separator m-nav__separator--fit"></span>
                            <a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm d-block" style="margin: 0 20px !important;"> Close </a>
                        </div>
                    </div>';

            })
            ->rawColumns(['full_name','data_phone','data_email','data_created_at','data_type','data_status','action'])
            ->make(true);
    }

    public function addContact($request)
    {
        $data = $request['request'];

        $validator = Validator::make($data, [
            'email' => 'required|unique:contacts,email,NULL,id,user_id,' . $data['user_id'],
        ]);


        if ($validator->fails()) {

            $errors = $validator->errors();

            return [
                'status' => 'error',
                'html' => $errors->first(),
            ];
        }

        $checkUserBalance = $this->userBalanceService->checkBalanceByUserIdAndType($data['user_id']);
        if ($checkUserBalance->remaining_no_of_contacts <= 0) {
            return [
                'status' => 'error',
                'html' => 'Sorry! You have reached the contact addition limit!',
            ];
        }


        if (isset($request['request']['automated_email_sequence'])) {
            $result = $this->contactRepository->addContactWithEmailSequence($request);
        } else {
            $result = $this->contactRepository->addContactWithoutEmailSequence($request);
        }


        if ($result) {
            if ($checkUserBalance->remaining_no_of_contacts > 0) {
                App::make(UserBalanceContact::class)->reduceBalanceByUserIdAndType($data['user_id'], 'remaining_no_of_contacts', 1);
            }


            return [
                'status' => 'success',
                'html' => "Contact Added."
            ];
        }

        return [
            'status' => 'error',
            'html' => "Contact Add Failed."
        ];

    }

    public function addContactByData($data)
    {
        return $this->contactRepository->addContactByData($data);
    }

    public function getContactById($id)
    {
        return $this->contactRepository->getContactById($id);
    }

    public function updateContactByField($request)
    {
        $where = [];
        $retValue = '';
        if (!empty($request->get('number'))) {
            $where = ['number' => $request->get('number')];
            $retValue = $request->get('number');
        }

        if (!empty($request->get('email'))) {
            $this->contactValidator->setEmailRulesValidationFormat();
            if (!$this->contactValidator->with($request->all())->passes()) {
                return [
                    'html' => 'Invalid email format',
                    'status' => 'validation-error'
                ];
            }
            $where = ['email' => $request->get('email')];
        }

        if (!empty($request->get('address'))) {
            $where = ['address' => $request->get('address')];
        }

        if (!empty($request->get('city'))) {
            $where = ['city' => $request->get('city')];
        }

        if (!empty($request->get('zip'))) {
            $where = ['zip' => $request->get('zip')];
        }

        if (!empty($request->get('country'))) {
            $where = ['country' => $request->get('country')];
        }

        if (!empty($request->get('important'))) {
            if ($request->get('important') == 'true') {
                $where = ['is_favourite' => 1];
            } else {
                $where = ['is_favourite' => 0];
            }
        }

        if (!empty($request->get('archive'))) {
            if ($request->get('archive') == 'true') {
                $where = ['is_archived' => 1];
            } else {
                $where = ['is_archived' => 0];
            }
        }


        if (isset($request['name'])) {
            if ($request->get('name') == '') {
                $where = [
                    'first_name' => '',
                    'last_name' => ''
                ];
            } else {
                $data_explode = explode(" ", $request->get('name'));
                $first_name_part = ceil(count($data_explode) / 2);
                $last_name_part = count($data_explode) - $first_name_part;
                $last_name_part = count($data_explode) - $last_name_part;
                $first_name = '';
                $last_name = '';
                for ($i = 0; $i < $first_name_part; $i++) {
                    $first_name .= $data_explode[$i] . ' ';
                }

                for ($i = $last_name_part; $i < count($data_explode); $i++) {
                    $last_name .= $data_explode[$i] . ' ';
                }
                $where['first_name'] = trim($first_name);
                $where['last_name'] = trim($last_name);
                $retValue = ViewHelper::getMinifiedName($where['first_name'], $where['last_name']);
            }

        }

        if (count($where) > 0) {
            //todo update contact
            $id = $request->get('id');
            $userId = $request->get('user_id');
            $response = $this->contactRepository->updateByField(['id' => $id], $where);
            if ($response) {
                return [
                    'html' => 'Contact Updated Successfully',
                    'status' => 'success',
                    'retValue' => $retValue
                ];
            }
        }
        return [
            'html' => 'Field cannot be empty!',
            'status' => 'error',
        ];
    }


    public function getContactDataById($request)
    {
        $contactId = $request->id;

        $contactData = $this->contactRepository->getContactDataById($contactId);

        return [
            'html' => view('contact::user.edit-contact')->with([
                'contactData' => $contactData,
            ])->render(),
            'status' => 'success'
        ];
    }

    public function getContactByEmail($email)
    {
        return $this->contactRepository->getContactByEmail($email);
    }

    public function getContactByWhere($where)
    {
        return $this->contactRepository->getContactByWhere($where);
    }

    public function updateContact($request)
    {
        $data = $request->toArray();

        $validator = Validator::make($data, [
            'email' => 'nullable|unique:contacts,email,'.$request->contact_id.',id,user_id,' . $request->userId,
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return [
                'status' => 'error',
                'html' => $errors->first(),
            ];
        }

        $contactId = $request->contact_id;

        $result = $this->contactRepository->updateContact($contactId, $request);
        if ($result) {

            return [
                'status' => 'success',
                'html' => "Contact Updated."
            ];
        }

        return [
            'status' => 'error',
            'html' => "Failed Try again."
        ];
    }

    public function updateContactByWhere($where, $update)
    {
        return $this->contactRepository->updateContactByWhere($where, $update);
    }

    public function deleteContactById($contactId, $userId)
    {
        $result = $this->contactRepository->deleteContactById($contactId);

        if ($result) {
            App::make(UserBalanceContact::class)->increaseBalanceByUserIdAndType($userId, 'remaining_no_of_contacts', 1);
            return [
                'html' => 'Contact Deleted!',
                'status' => 'success'
            ];
        } else {
            return [
                'html' => "Failed! Please try again",
                'status' => 'error',
            ];
        }
    }

    public function processCsvFileData($path)
    {
        $key = 0;
        //if ($header) {
        $row = 0;

        if (($handle = fopen($path, "r")) !== FALSE) {
            while (($result = fgetcsv($handle)) !== false) {
                if (array(null) !== $result) { // ignore blank lines
                    $csv[] = $result;
                    $row++;
                    if ($row == 7) {
                        break;
                    }
                }

            }
            fclose($handle);
        }
        return $csv;
    }

    public function processExcelFileData($fileInfo, $path)
    {
        $data = Excel::toArray(new FileServiceForExcelFrontend(), $fileInfo);
        return $data[0];
    }

    public function parseImport($request, $userId)
    {
        $validator = Validator::make($request->all(), [
            'csv_file' => 'required|mimes:xls,xlsx,csv,txt',
            'file' => 'size:50000',
        ]);
        $csv_header_fields = [];
        if ($validator->passes()) {
            //$document = $request->file('csv_file');
            $fileInfo = $request->file('csv_file');
            $path = $fileInfo->getRealPath();
            if ($fileInfo->getMimeType() == 'text/plain') {
                $data = $this->processCsvFileData($path);
            } else {
                $data = $this->processExcelFileData($fileInfo, $path);
            }
            //Convert File Information into Data
            $fileData = $this->fileService->insertCsvFile($request, $userId);
            //Store csv File in tmp folder and convert Xlsx file into csv


            //Data process
            if (count($data) > 0) {
                if ($request->has('header')) {

                    foreach ($data[0] as $key => $value) {
                        $csv_header_fields[] = $key; // Taking array key as header fields
                    }
                }
                $csv_data = array_slice($data, 0, 5);
            } //if no data in array
            else {
                return redirect()->back();
            }
            //searching longest array in that multidimensional array
            $longest_array = max($csv_data);
            $longest_array_index = array_search($longest_array, $csv_data);
            //clearing special characters from client input headers
            $client_headers = [];
            foreach (max($csv_data) as $key => $value) {
                $key = str_replace(array(' ', '-', '_', '/'), '', $key);
                $key = preg_replace('/[^A-Za-z0-9\-]/', '', $key);
                $key = strtolower($key);
                array_push($client_headers, $key);
            }
            $client_headers = array_combine(array_keys($csv_data[$longest_array_index]), $client_headers);
            //$client_headers = $this->fileImportService()->clientHeaders($csv_data);

            //Checked upto this part

            $csv_data_file = json_encode($data);

            if (isset($request['lookup_check']) && !is_null($request['lookup_check'])) {
                $getSettingDetails = $this->userSettingRepository->getSettingByUserIdAndKey($userId, UserSetting::LOOK_UP_PERMISSION_KEY);
                if ($getSettingDetails) {
                    if ($getSettingDetails->status == UserSetting::LOOK_UP_PERMISSION_STATUS_ON) {
                        $lookup_check = UserSetting::LOOK_UP_PERMISSION_STATUS_ON;
                    } else {
                        $lookup_check = UserSetting::LOOK_UP_PERMISSION_STATUS_OFF;
                    }
                } else {
                    $lookup_check = UserSetting::LOOK_UP_PERMISSION_STATUS_OFF;
                }

            }
            if (is_null($request['lookup_check'])) {
                $lookup_check = UserSetting::LOOK_UP_PERMISSION_STATUS_OFF;
            }

            $fileData = json_encode($fileData);
            $html = view('contact::user.import_fields', compact('csv_header_fields', 'csv_data', 'csv_data_file'
                , 'client_headers', 'fileData', 'lookup_check'))->render();

            return [
                'html' => $html,
                'status' => "success"
            ];
        }
    }

    public function processImportService($request)
    {
        $csv_data = json_decode($request->csv_data_file_id, true);
        $new_headers = $request->fields;
        $avoid_Field = [];

//            foreach ($new_headers as $key => $value){
//                if(! in_array($value,DB_FIELDS_FOR_IMPORTFILE)){
//                    $new_headers[$key] = 'Skip Field';
//                }
//            }
        $fileData = $request->input('fileData');
//        $tag_name_file = $request->tag_name;
//        $tagType = $request->tag_type;
//        $tag_name = json_decode($tag_name_file, true);

        //redirect any duplicate type
        /*if(count(array_unique($new_headers)) < count($new_headers)){
            return redirect()->back();
        } */
        //copy file one directory to another
        //$bulkContactService->moveContactFile($filename);
        //$csv_data = json_decode($data, true);
        if (array_keys($new_headers) !== range(0, count($new_headers) - 1)) {
            foreach ($csv_data as $key => $value) {
                foreach ($value as $k => $v) {
                    $new_data[$key][$new_headers[$k]] = $v;
                }
                //$new_data = str_replace($keys, $value, $data['csv_data']);
                //$new_data= json_decode($new_data,true);
            }
        } else {
            foreach ($csv_data as $key => $value) {
                $new_data[$key] = array_combine($new_headers, array_values($csv_data[$key]));
            }
        }
        //$new_data = $this->fileImportService()->processMappedData($new_headers, $csv_data);
        //$new_data_file = json_encode($new_data);
        //$new_data = array_slice($new_data, 0 , 10);
        $lookup_check = $request['lookup_check'];
        $new_headers_file = json_encode(array_values($new_headers));
        $html = view('contact::user.finish_mapping',
            compact('new_headers_file', 'new_headers', 'new_data', 'fileData', 'lookup_check'))->render();

        return $html;
    }

    public function finalizeImport($request, $user)
    {
        $fileData = json_decode($request->input('fileData'));

        $headers = json_decode($request->input('headers'), true);
        $new_headers = [];
        foreach ($headers as $key => $value) {
            $mapped_contacts[$value] = $key;
        }
        $mapped_contacts = json_decode(json_encode(strtolower(str_replace(array(' '), '_', json_encode($mapped_contacts)))));
        $resultData = $this->fileService->moveContactFile($fileData, $user['id'], $mapped_contacts, $request['lookup_check']);
        $response = $this->postRequestIfSubmitted($resultData['resultData']->id, $user['auth_token']);
        return $resultData;
    }


    public function getTotalSubscribersByDuration($userId, $durationType)
    {
        return $this->contactRepository->getTotalSubscribersByDuration($userId, $durationType);
    }

    public function getTotalLeadsByDuration($userId, $durationType)
    {
        return $this->contactRepository->getTotalLeadsByDuration($userId, $durationType);
    }

    public function getSubscriberDataOnMonthlyBasis($userId)
    {
        return $this->contactRepository->getSubscriberDataOnMonthlyBasis($userId);
    }

    public function getLeadDataOnWeeklyBasis($userId)
    {
        return $this->contactRepository->getLeadDataOnWeeklyBasis($userId);
    }

    public function getLeadDataOnMonthlyBasis($userId)
    {
        return $this->contactRepository->getLeadDataOnMonthlyBasis($userId);
    }

    public function getSubscriberDataForever($userId)
    {
        return $this->contactRepository->getSubscriberDataForever($userId);
    }

    public function getLeadDataForever($userId)
    {
        return $this->contactRepository->getLeadDataForever($userId);
    }

    private function postRequestIfSubmitted($fileId, $userToken)
    {
        return false;
//        $curl = curl_init();
//
//        $submitUrl = "https://api.mapyoursales.com/core/api/engine/file/startprocess/" . $fileId;
//        $auth_token = 'Bearer ' . $userToken;
//
//        curl_setopt_array($curl, array(
//            CURLOPT_URL => $submitUrl,
//            CURLOPT_RETURNTRANSFER => true,
//            CURLOPT_ENCODING => "",
//            CURLOPT_MAXREDIRS => 10,
//            CURLOPT_TIMEOUT => 30,
//            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//            CURLOPT_CUSTOMREQUEST => "POST",
////            CURLOPT_POSTFIELDS => $jsonData,
//            CURLOPT_HTTPHEADER => array(
//                "Content-Type: application/json",
//                "Authorization: " . $auth_token,
//            ),
//        ));
//
//        $response = curl_exec($curl);
//        $err = curl_error($curl);
//        curl_close($curl);
//
//        Log::channel('backend')->info($response);
//        return $response;
    }

    public function getContactByCondition($where)
    {

        return $this->contactRepository->getContactByCondition($where);
    }

    public function activateContactById($id)
    {
        $response = $this->contactRepository->activateContactById($id);
        if($response)
        {
            return [
               'status' => 'success',
               'html' => 'Contact activated successfully',
            ];
        }
        return [
            'status' => 'error',
            'html' => 'Contact activation failed!',
        ];

    }

    public function blockContactById($id)
    {
        $response = $this->contactRepository->blockContactById($id);
        if($response)
        {
            return [
                'status' => 'success',
                'html' => 'Contact blocked successfully',
            ];
        }
        return [
            'status' => 'error',
            'html' => 'Contact blocking failed!',
        ];
    }

}
