<?php


namespace Modules\Contact\Http\Services;

use Illuminate\Support\Facades\Auth;
use Modules\Contact\Entities\Contact;
use Modules\Contact\Contracts\Services\EventActionContact;
use Modules\Contact\Entities\EventAction;
use Illuminate\Support\Facades\DB;

class EventActionService implements EventActionContact
{
    public function __construct()
    {

    }

    public function getActionConditionAndInputData($constantValue)
    {
        if($constantValue == EventAction::PHONE_NUMBER){

            $htmlCondition = ' <select class="form-control event_action_condition" id="event_action" name="action">';
            foreach (EventAction::$getAllTriggerEventActionCondition1 as $action => $value){
                $htmlCondition .='<option value="'.$action.'">'.$value.'</option>';
            }
            $htmlCondition .='</select><span class="form-text text-muted">Select action from search result.</span>';


            $htmlInputConditionValue = ' <input type="text" value="" class="input_value form-control" name="input_value[]">';


        }
        elseif($constantValue == EventAction::EMAIL_ADDRESS){

            $htmlCondition = ' <select class="form-control event_action_condition" id="event_action" name="action">';
            foreach (EventAction::$getAllTriggerEventActionCondition1 as $action => $value){
                $htmlCondition .='<option value="'.$action.'">'.$value.'</option>';
            }
            $htmlCondition .='</select><span class="form-text text-muted">Select action from search result.</span>';


            $htmlInputConditionValue = ' <input type="text" value="" class="input_email form-control" name="input_email[]">';


        }
        elseif($constantValue == EventAction::FIRST_NAME){

            $htmlCondition = ' <select class="form-control event_action_condition" id="event_action" name="action">';
            foreach (EventAction::$getAllTriggerEventActionCondition1 as $action => $value){
                $htmlCondition .='<option value="'.$action.'">'.$value.'</option>';
            }
            $htmlCondition .='</select><span class="form-text text-muted">Select action from search result.</span>';


            $htmlInputConditionValue = ' <input type="text" value="" class="first_name form-control" name="first_name[]">';


        }
        elseif($constantValue == EventAction::LAST_NAME){

            $htmlCondition = ' <select class="form-control event_action_condition" id="event_action" name="action">';
            foreach (EventAction::$getAllTriggerEventActionCondition1 as $action => $value){
                $htmlCondition .='<option value="'.$action.'">'.$value.'</option>';
            }
            $htmlCondition .='</select><span class="form-text text-muted">Select action from search result.</span>';


            $htmlInputConditionValue = ' <input type="text" value="" class="last_name form-control" name="last_name[]">';


        }
        elseif($constantValue == EventAction::TYPE){

            $htmlCondition = ' <select class="form-control event_action_condition" id="event_action" name="action">';
            foreach (EventAction::$getAllTriggerEventActionCondition2 as $action => $value){
                $htmlCondition .='<option value="'.$action.'">'.$value.'</option>';
            }
            $htmlCondition .='</select><span class="form-text text-muted">Select action from search result.</span>';

            $htmlInputConditionValue = ' <select class="form-control contact_type" id="contact_type" name="contact_type">';
            $htmlInputConditionValue .='<option value="0">Subscribers</option>';
            $htmlInputConditionValue .='<option value="1">Leads</option>';
            $htmlInputConditionValue .='<option value="2">Clients</option>';
            $htmlInputConditionValue .='</select><span class="form-text text-muted">Select action from search result.</span>';

        }
        elseif($constantValue == EventAction::DATE_ADDED){

            $htmlCondition = ' <select class="form-control event_action_condition" id="event_action" name="action">';
            foreach (EventAction::$getAllTriggerEventActionCondition1 as $action => $value){
                $htmlCondition .='<option value="'.$action.'">'.$value.'</option>';
            }
            $htmlCondition .='</select><span class="form-text text-muted">Select action from search result.</span>';


            $htmlInputConditionValue = ' <input type="date" value="' .date("Y-m-d", strtotime(date('m/d/Y')) )      .'" class="date_added form-control" name="date_added[]">';

        }

        return
            [
                'html_condition' => $htmlCondition,
                'input_condition_value' => $htmlInputConditionValue
            ];

    }


    public function getAllContactInfo($request, $userId)
    {
//        $contactData = DB::table('contacts')
//            ->selectRaw('contacts.id as contact_id')->where('contacts.user_id', $userId);

        $contactData = Contact::where('user_id', $userId)->orderBy('id', 'desc');

        foreach ($request->requestData as $data){

            /****### IF EVENT ACTION IS PHONE_NUMBER[1] ###****/
            if($data['event_action'] == EventAction::PHONE_NUMBER){

                /****### IF EVENT ACTION CONDITION IS[EQUAL TO(=) ] [1] ###****/
                if($data['event_action_condition'] == EventAction::IS){

                    $condition = EventAction::getAllConditionDataType()[EventAction::IS];
                    $contactData = $contactData->where('number', $condition, $data['input_value']);

                    /****### IF EVENT ACTION CONDITION START_WITH [LIKE %phonenumber] [2] ###****/
                }elseif ($data['event_action_condition'] == EventAction::START_WITH){

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::CONTAINS];
                    $startWith = EventAction::getAllConditionDataType()[EventAction::START_WITH];

                    $contactData = $contactData->where('number', $condition1, $startWith.''.$data['input_value']);
                }
                /****### IF EVENT ACTION CONDITION ENDS_WITH [LIKE phonenumber%] [3] ###****/
                elseif ($data['event_action_condition'] == EventAction::ENDS_WITH){

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::CONTAINS];
                    $endWith = EventAction::getAllConditionDataType()[EventAction::ENDS_WITH];

                    $contactData = $contactData->where('number', $condition1, $data['input_value'].''.$endWith);
                }
                /****### IF EVENT ACTION CONDITION CONTAINS [LIKE %phonenumber%] [4] ###****/
                elseif ($data['event_action_condition'] == EventAction::CONTAINS){

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::CONTAINS];
                    $startWith = EventAction::getAllConditionDataType()[EventAction::START_WITH];
                    $endWith = EventAction::getAllConditionDataType()[EventAction::ENDS_WITH];

                    $contactData = $contactData->where('number', $condition1, $startWith.''.$data['input_value'].''.$endWith);
                }
                /****### IF EVENT ACTION CONDITION DOES_NOT_CONTAINS [NOT LIKE phonenumber] [5] ###****/
                elseif ($data['event_action_condition'] == EventAction::DOES_NOT_CONTAINS){

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::DOES_NOT_CONTAINS];
                    $startWith = EventAction::getAllConditionDataType()[EventAction::START_WITH];
                    $endWith = EventAction::getAllConditionDataType()[EventAction::ENDS_WITH];

                    $contactData = $contactData->where('number', $condition1, $startWith.''.$data['input_value'].''.$endWith);

                }
                /****### IF EVENT ACTION CONDITION IS_NOT [IS NOT] [6] ###****/
                elseif ($data['event_action_condition'] == EventAction::IS_NOT){
                    $condition1 = EventAction::getAllConditionDataType()[EventAction::IS_NOT];
                    $contactData = $contactData->where('number', $condition1, $data['input_value']);
                }
                /****### IF EVENT ACTION CONDITION IS_IN_LIST [IN] [9] ###****/
                elseif ($data['event_action_condition'] == EventAction::IS_IN_LIST){

                    $contactData = $contactData->whereIn('number', [$data['input_value']]);
                }
                /****### IF EVENT ACTION CONDITION IS_NOT_IN_LIST [IN] [10] ###****/
                elseif ($data['event_action_condition'] == EventAction::IS_NOT_IN_LIST){
                    $contactData = $contactData->whereNotIn('number', [$data['input_value']]);
                }
            }



            /****### IF EVENT ACTION IS EMAIL_ADDRESS[8] ###****/
            if($data['event_action'] == EventAction::EMAIL_ADDRESS){
                /****### IF EVENT ACTION CONDITION IS[EQUAL TO(=) ] [1] ###****/
                if($data['event_action_condition'] == EventAction::IS){

                    $condition = EventAction::getAllConditionDataType()[EventAction::IS];
                    $contactData = $contactData->where('email', $condition, $data['input_email']);

                    /****### IF EVENT ACTION CONDITION START_WITH [LIKE %phonenumber] [2] ###****/
                }elseif ($data['event_action_condition'] == EventAction::START_WITH){

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::CONTAINS];
                    $startWith = EventAction::getAllConditionDataType()[EventAction::START_WITH];

                    $contactData = $contactData->where('email', $condition1, $startWith.''.$data['input_email']);
                }
                /****### IF EVENT ACTION CONDITION ENDS_WITH [LIKE phonenumber%] [3] ###****/
                elseif ($data['event_action_condition'] == EventAction::ENDS_WITH){

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::CONTAINS];
                    $endWith = EventAction::getAllConditionDataType()[EventAction::ENDS_WITH];

                    $contactData = $contactData->where('email', $condition1, $data['input_email'].''.$endWith);
                }
                /****### IF EVENT ACTION CONDITION CONTAINS [LIKE %phonenumber%] [4] ###****/
                elseif ($data['event_action_condition'] == EventAction::CONTAINS){

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::CONTAINS];
                    $startWith = EventAction::getAllConditionDataType()[EventAction::START_WITH];
                    $endWith = EventAction::getAllConditionDataType()[EventAction::ENDS_WITH];

                    $contactData = $contactData->where('email', $condition1, $startWith.''.$data['input_email'].''.$endWith);
                }
                /****### IF EVENT ACTION CONDITION DOES_NOT_CONTAINS [NOT LIKE phonenumber] [5] ###****/
                elseif ($data['event_action_condition'] == EventAction::DOES_NOT_CONTAINS){

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::DOES_NOT_CONTAINS];
                    $startWith = EventAction::getAllConditionDataType()[EventAction::START_WITH];
                    $endWith = EventAction::getAllConditionDataType()[EventAction::ENDS_WITH];

                    $contactData = $contactData->where('email', $condition1, $startWith.''.$data['input_email'].''.$endWith);

                }
                /****### IF EVENT ACTION CONDITION IS_NOT [IS NOT] [6] ###****/
                elseif ($data['event_action_condition'] == EventAction::IS_NOT){
                    $condition1 = EventAction::getAllConditionDataType()[EventAction::IS_NOT];
                    $contactData = $contactData->where('email', $condition1, $data['input_email']);
                }
                /****### IF EVENT ACTION CONDITION IS_IN_LIST [IN] [9] ###****/
                elseif ($data['event_action_condition'] == EventAction::IS_IN_LIST){

                    $contactData = $contactData->whereIn('email', [$data['input_email']]);
                }
                /****### IF EVENT ACTION CONDITION IS_NOT_IN_LIST [IN] [10] ###****/
                elseif ($data['event_action_condition'] == EventAction::IS_NOT_IN_LIST){
                    $contactData = $contactData->whereNotIn('email', [$data['input_email']]);
                }
            }

            /****### IF EVENT ACTION IS FIRST_NAME[10] ###****/
            if($data['event_action'] == EventAction::FIRST_NAME){

                /****### IF EVENT ACTION CONDITION IS[EQUAL TO(=) ] [1] ###****/
                if($data['event_action_condition'] == EventAction::IS){

                    $condition = EventAction::getAllConditionDataType()[EventAction::IS];
                    $contactData = $contactData->where('first_name', $condition, $data['first_name']);

                    /****### IF EVENT ACTION CONDITION START_WITH [LIKE %phonenumber] [2] ###****/
                }elseif ($data['event_action_condition'] == EventAction::START_WITH){

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::CONTAINS];
                    $startWith = EventAction::getAllConditionDataType()[EventAction::START_WITH];

                    $contactData = $contactData->where('first_name', $condition1, $startWith.''.$data['first_name']);
                }
                /****### IF EVENT ACTION CONDITION ENDS_WITH [LIKE phonenumber%] [3] ###****/
                elseif ($data['event_action_condition'] == EventAction::ENDS_WITH){

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::CONTAINS];
                    $endWith = EventAction::getAllConditionDataType()[EventAction::ENDS_WITH];

                    $contactData = $contactData->where('first_name', $condition1, $data['first_name'].''.$endWith);
                }
                /****### IF EVENT ACTION CONDITION CONTAINS [LIKE %phonenumber%] [4] ###****/
                elseif ($data['event_action_condition'] == EventAction::CONTAINS){

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::CONTAINS];
                    $startWith = EventAction::getAllConditionDataType()[EventAction::START_WITH];
                    $endWith = EventAction::getAllConditionDataType()[EventAction::ENDS_WITH];

                    $contactData = $contactData->where('first_name', $condition1, $startWith.''.$data['first_name'].''.$endWith);
                }
                /****### IF EVENT ACTION CONDITION DOES_NOT_CONTAINS [NOT LIKE phonenumber] [5] ###****/
                elseif ($data['event_action_condition'] == EventAction::DOES_NOT_CONTAINS){

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::DOES_NOT_CONTAINS];
                    $startWith = EventAction::getAllConditionDataType()[EventAction::START_WITH];
                    $endWith = EventAction::getAllConditionDataType()[EventAction::ENDS_WITH];

                    $contactData = $contactData->where('first_name', $condition1, $startWith.''.$data['first_name'].''.$endWith);

                }
                /****### IF EVENT ACTION CONDITION IS_NOT [IS NOT] [6] ###****/
                elseif ($data['event_action_condition'] == EventAction::IS_NOT){
                    $condition1 = EventAction::getAllConditionDataType()[EventAction::IS_NOT];
                    $contactData = $contactData->where('first_name', $condition1, $data['first_name']);
                }
                /****### IF EVENT ACTION CONDITION IS_IN_LIST [IN] [9] ###****/
                elseif ($data['event_action_condition'] == EventAction::IS_IN_LIST){

                    $contactData = $contactData->whereIn('first_name', [$data['first_name']]);
                }
                /****### IF EVENT ACTION CONDITION IS_NOT_IN_LIST [IN] [10] ###****/
                elseif ($data['event_action_condition'] == EventAction::IS_NOT_IN_LIST){
                    $contactData = $contactData->whereNotIn('first_name', [$data['first_name']]);
                }
            }

            /****### IF EVENT ACTION IS LAST_NAME[10] ###****/
            if($data['event_action'] == EventAction::LAST_NAME){

                /****### IF EVENT ACTION CONDITION IS[EQUAL TO(=) ] [1] ###****/
                if($data['event_action_condition'] == EventAction::IS){

                    $condition = EventAction::getAllConditionDataType()[EventAction::IS];
                    $contactData = $contactData->where('last_name', $condition, $data['last_name']);

                    /****### IF EVENT ACTION CONDITION START_WITH [LIKE %phonenumber] [2] ###****/
                }elseif ($data['event_action_condition'] == EventAction::START_WITH){

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::CONTAINS];
                    $startWith = EventAction::getAllConditionDataType()[EventAction::START_WITH];

                    $contactData = $contactData->where('last_name', $condition1, $startWith.''.$data['last_name']);
                }
                /****### IF EVENT ACTION CONDITION ENDS_WITH [LIKE phonenumber%] [3] ###****/
                elseif ($data['event_action_condition'] == EventAction::ENDS_WITH){

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::CONTAINS];
                    $endWith = EventAction::getAllConditionDataType()[EventAction::ENDS_WITH];

                    $contactData = $contactData->where('last_name', $condition1, $data['last_name'].''.$endWith);
                }
                /****### IF EVENT ACTION CONDITION CONTAINS [LIKE %phonenumber%] [4] ###****/
                elseif ($data['event_action_condition'] == EventAction::CONTAINS){

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::CONTAINS];
                    $startWith = EventAction::getAllConditionDataType()[EventAction::START_WITH];
                    $endWith = EventAction::getAllConditionDataType()[EventAction::ENDS_WITH];

                    $contactData = $contactData->where('last_name', $condition1, $startWith.''.$data['last_name'].''.$endWith);
                }
                /****### IF EVENT ACTION CONDITION DOES_NOT_CONTAINS [NOT LIKE phonenumber] [5] ###****/
                elseif ($data['event_action_condition'] == EventAction::DOES_NOT_CONTAINS){

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::DOES_NOT_CONTAINS];
                    $startWith = EventAction::getAllConditionDataType()[EventAction::START_WITH];
                    $endWith = EventAction::getAllConditionDataType()[EventAction::ENDS_WITH];

                    $contactData = $contactData->where('last_name', $condition1, $startWith.''.$data['last_name'].''.$endWith);

                }
                /****### IF EVENT ACTION CONDITION IS_NOT [IS NOT] [6] ###****/
                elseif ($data['event_action_condition'] == EventAction::IS_NOT){
                    $condition1 = EventAction::getAllConditionDataType()[EventAction::IS_NOT];
                    $contactData = $contactData->where('last_name', $condition1, $data['last_name']);
                }
                /****### IF EVENT ACTION CONDITION IS_IN_LIST [IN] [9] ###****/
                elseif ($data['event_action_condition'] == EventAction::IS_IN_LIST){

                    $contactData = $contactData->whereIn('last_name', [$data['last_name']]);
                }
                /****### IF EVENT ACTION CONDITION IS_NOT_IN_LIST [IN] [10] ###****/
                elseif ($data['event_action_condition'] == EventAction::IS_NOT_IN_LIST){
                    $contactData = $contactData->whereNotIn('last_name', [$data['last_name']]);
                }
            }


            /****### IF EVENT ACTION IS ZIP_CODE[11] ###****/
            if($data['event_action'] == EventAction::DATE_ADDED){

                /****### IF EVENT ACTION CONDITION IS[EQUAL TO(=) ] [1] ###****/
                if($data['event_action_condition'] == EventAction::IS){

                    $condition = EventAction::getAllConditionDataType()[EventAction::IS];
                    $contactData = $contactData->whereDate('created_at', $condition, $data['date_added'].' 00:00:00');

                    /****### IF EVENT ACTION CONDITION START_WITH [LIKE %phonenumber] [2] ###****/
                }elseif ($data['event_action_condition'] == EventAction::START_WITH){

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::CONTAINS];
                    $startWith = EventAction::getAllConditionDataType()[EventAction::START_WITH];

                    $contactData = $contactData->where('created_at', $condition1, $startWith.''.$data['date_added'] .' 00:00:00');
                }
                /****### IF EVENT ACTION CONDITION ENDS_WITH [LIKE phonenumber%] [3] ###****/
                elseif ($data['event_action_condition'] == EventAction::ENDS_WITH){

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::CONTAINS];
                    $endWith = EventAction::getAllConditionDataType()[EventAction::ENDS_WITH];

                    $contactData = $contactData->where('created_at', $condition1, $data['date_added'] .' 00:00:00' .''.$endWith);
                }
                /****### IF EVENT ACTION CONDITION CONTAINS [LIKE %phonenumber%] [4] ###****/
                elseif ($data['event_action_condition'] == EventAction::CONTAINS){

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::CONTAINS];
                    $startWith = EventAction::getAllConditionDataType()[EventAction::START_WITH];
                    $endWith = EventAction::getAllConditionDataType()[EventAction::ENDS_WITH];

                    $contactData = $contactData->where('created_at', $condition1, $startWith.''.$data['date_added']. ' 00:00:00'.''.$endWith);
                }
                /****### IF EVENT ACTION CONDITION DOES_NOT_CONTAINS [NOT LIKE phonenumber] [5] ###****/
                elseif ($data['event_action_condition'] == EventAction::DOES_NOT_CONTAINS){

                    $condition1 = EventAction::getAllConditionDataType()[EventAction::DOES_NOT_CONTAINS];
                    $startWith = EventAction::getAllConditionDataType()[EventAction::START_WITH];
                    $endWith = EventAction::getAllConditionDataType()[EventAction::ENDS_WITH];

                    $contactData = $contactData->where('created_at', $condition1, $startWith.''.$data['date_added'].' 00:00:00'.''.$endWith);

                }
                /****### IF EVENT ACTION CONDITION IS_NOT [IS NOT] [6] ###****/
                elseif ($data['event_action_condition'] == EventAction::IS_NOT){
                    $condition1 = EventAction::getAllConditionDataType()[EventAction::IS_NOT];
                    $contactData = $contactData->where('created_at', $condition1, $data['date_added'].' 00:00:00');
                }
                /****### IF EVENT ACTION CONDITION IS_IN_LIST [IN] [9] ###****/
                elseif ($data['event_action_condition'] == EventAction::IS_IN_LIST){

                    $contactData = $contactData->whereIn('created_at', [$data['date_added'].' 00:00:00']);
                }
                /****### IF EVENT ACTION CONDITION IS_NOT_IN_LIST [IN] [10] ###****/
                elseif ($data['event_action_condition'] == EventAction::IS_NOT_IN_LIST){
                    $contactData = $contactData->whereNotIn('created_at', [$data['date_added'].' 00:00:00']);
                }
            }




        }

        return $contactData->orderBy('id', 'desc') ;

    }



}