<?php

namespace Modules\Contact\Http\Services;

use Illuminate\Support\Collection;
use Response;
use Maatwebsite\Excel\Concerns\ToCollection;


class FileServiceForExcelFrontend implements ToCollection
{


    public function __construct()
    {
    }

    public function collection(Collection $collection)
    {
        return $collection->toArray();
    }


}
