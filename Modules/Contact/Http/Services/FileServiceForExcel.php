<?php

namespace Modules\Contact\Http\Services;


use App\Services\Utility;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Modules\Contact\Contracts\Repositories\ContactRepository;
use Modules\Contact\Contracts\Repositories\FileContactRepository;
use Modules\Contact\Contracts\Repositories\FileInfoRepository;
use Modules\Contact\Contracts\Services\ContactContact;
use Modules\Contact\Entities\Contact;
use Modules\Contact\Entities\FileInfo;
use Modules\Contact\Http\Services\Validation\ContactValidator;
use Modules\Package\Contracts\Service\UserBalanceContact;
use Response;
use Maatwebsite\Excel\Concerns\ToCollection;


class FileServiceForExcel implements ToCollection
{

    /**
     * @var FileInfoRepository
     */
    private $fileInfoRepository;
    /**
     * @var TagRepository
     */
    private $tagRepository;
    /**
     * @var FileRepository
     */
    private $fileRepository;

    private $tagContactRepository;
    private $file;


    public function __construct($file, FileInfoRepository $fileInfoRepository)
    {
        $this->fileInfoRepository = $fileInfoRepository;
        $this->file = $file;

    }

    public function collection(Collection $collection)
    {
        $fileId = $this->file->id;
        $totalContact = $this->fileInfoRepository->getTotalContactUploadedByFileId($fileId)->total_contacts;
        $this->fileProcessWork($collection, $this->file, $fileId, $totalContact);
    }

    private function fileProcessWork($results, $file, $fileId, $totalContact)
    {
        $uploadedContact = $this->fileInfoRepository->getTotalContactUploadedByFileId($fileId)->total_contacts_uploaded;
        $userId = $file->user_id;
        $contactType = $file->contact_type;
        $fileExtension = $file->file_type;
        $fileAction = $file->action_type;

        $csvResult = $results->toArray();
        $contact = null;
        $isInActive = null;
        $contactInfo = null;
        $contactRepo = App::make(ContactRepository::class);
        $contactValidator = App::make(ContactValidator::class);

        $fileMap = json_decode($file->mapped_contacts, true);

        $numberIndex = isset($fileMap['number']) ? $fileMap['number'] : '';
        $firstNameIndex = isset($fileMap['first_name']) ? $fileMap['first_name'] : '';
        $lastNameIndex = isset($fileMap['last_name']) ? $fileMap['last_name'] : '';
        $addressIndex = isset($fileMap['last_name']) ? $fileMap['last_name'] : '';
        $emailIndex = isset($fileMap['email']) ? $fileMap['email'] : '';
        $countryIndex = isset($fileMap['country']) ? $fileMap['country'] : '';
        $cityIndex = isset($fileMap['city']) ? $fileMap['city'] : '';
        $stateIndex = isset($fileMap['state']) ? $fileMap['state'] : '';
        $zipIndex = isset($fileMap['zip']) ? $fileMap['zip'] : '';

        $duplicateContacts = 0;
        $invalidContacts = 0;

        foreach ($csvResult as $eachRow) {
            $contactId = null;
            $eachRow = array_values($eachRow);

            if ($fileExtension == FileInfo::FILE_TYPE_CSV) {
                $contactArray = [
                    'user_id' => $userId,
                    'number' => !empty($eachRow[$numberIndex]) ? $eachRow[$numberIndex] : "",
                    'email' => $eachRow[$emailIndex],
                    'first_name' => !empty($eachRow[$firstNameIndex]) ? $eachRow[$firstNameIndex] : "",
                    'last_name' => !empty($eachRow[$lastNameIndex]) ? $eachRow[$lastNameIndex] : "",
                    'address' => !empty($eachRow[$addressIndex]) ? $eachRow[$addressIndex] : "",
                    'city' => !empty($eachRow[$cityIndex]) ? $eachRow[$cityIndex] : "",
                    'state' => !empty($eachRow[$stateIndex]) ? $eachRow[$stateIndex] : "",
                    'country' => !empty($eachRow[$countryIndex]) ? $eachRow[$countryIndex] : "",
                    'zip' => !empty($eachRow[$zipIndex]) ? $eachRow[$zipIndex] : "",
                    'user_status' => $fileAction != Contact::BLOCKED ? Contact::ACTIVE : Contact::BLOCKED,
                    'type' => $contactType,
                ];

            } else if ($fileExtension == FileInfo::FILE_TYPE_TXT) {
                $contactArray = [
                    'user_id' => $userId,
                    'number' => !empty($eachRow[$numberIndex]) ? $eachRow[$numberIndex] : "",
                    'email' => $eachRow[$emailIndex],
                    'first_name' => !empty($eachRow[$firstNameIndex]) ? $eachRow[$firstNameIndex] : "",
                    'last_name' => !empty($eachRow[$lastNameIndex]) ? $eachRow[$lastNameIndex] : "",
                    'address' => !empty($eachRow[$addressIndex]) ? $eachRow[$addressIndex] : "",
                    'city' => !empty($eachRow[$cityIndex]) ? $eachRow[$cityIndex] : "",
                    'state' => !empty($eachRow[$stateIndex]) ? $eachRow[$stateIndex] : "",
                    'country' => !empty($eachRow[$countryIndex]) ? $eachRow[$countryIndex] : "",
                    'zip' => !empty($eachRow[$zipIndex]) ? $eachRow[$zipIndex] : "",
                    'user_status' => $fileAction != Contact::BLOCKED ? Contact::ACTIVE : Contact::BLOCKED,
                    'type' => $contactType,
                ];
            }


            $checkContactHas = $contactRepo->findByUserIdAndEmail($userId, $eachRow[$emailIndex]);


            if (!$checkContactHas) {
                if (!is_null($eachRow[$emailIndex]) && !empty($eachRow[$emailIndex])) {
                    $checkUserBalance = App::make(UserBalanceContact::class)->checkBalanceByUserIdAndType($userId);

                    if ($checkUserBalance) {
                        if ($checkUserBalance->remaining_no_of_contacts > 0) {
                            if (!filter_var($contactArray['email'], FILTER_VALIDATE_EMAIL)) {
                                $invalidContacts++;
                            } else {

                                $contact = $contactRepo->insert($contactArray, $userId, 2);

                                $contactId = $contact->id;

                                App::make(UserBalanceContact::class)->reduceBalanceByUserIdAndType($userId, 'remaining_no_of_contacts', 1);
                                $uploadedContact++;
                            }
                        }
                    }
                }


            } else {
                $duplicateContacts++;
                $contactId = $checkContactHas->id;
            }

            if ($contactArray) {
                if (!is_null($eachRow[$emailIndex]) && !empty($eachRow[$emailIndex])) {
                    if (!is_null($contactId) && $contactId != "") {
                        if (App::make(FileContactRepository::class)->isExists($contactId, $file->id)) {
                            $fileListData = array(
                                'file_id' => $file->id,
                                'contact_id' => $contactId
                            );
                        } else {
                            $fileListData = array(
                                'file_id' => $file->id,
                                'contact_id' => $contactId
                            );
                            App::make(FileContactRepository::class)->insert($fileListData);
                        }

                    }
                }
                $this->fileInfoRepository->updateTotalContact($fileId, $uploadedContact);
            }
            $contact = null;
            $isInActive = null;
        }


        if ($duplicateContacts > 0 || $invalidContacts > 0) {
            $jsonArray = [];
            if ($duplicateContacts > 0) {
                $jsonArray['duplicate'] = $duplicateContacts;
            }
            if ($invalidContacts > 0) {
                $jsonArray['invalid'] = $invalidContacts;
            }
            $this->fileInfoRepository->updateErrorInfo($fileId, json_encode($jsonArray));

        }
    }

}
