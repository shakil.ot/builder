<?php

namespace Modules\Contact\Http\Services\Validation;


/**
 * @method with(array $inputData)
 */
class ContactValidator extends AbstractLaravelValidator
{
    /**
     * @var array
     */
    protected $rules;

    //private $rules;
    public function setContactRulesAdmin($id = null)
    {
        $this->rules = array(
            'number' => 'required|unique:admin_contacts,number,' . $id,
            'first_name' => 'max:255',
            'last_name' => 'max:255',
            'country' => 'max:255',
            'address' => 'max:500',
            'state' => 'max:255',
        );
    }

    public function setContactRules($userId, $id = null)
    {
        $this->rules = array(
            'first_name' => 'max:255',
            'last_name' => 'max:255',
            'address' => 'max:500',
            'state' => 'max:255',
            'medicare' => 'max:20',
            'confirmation' => 'max:20',
            'email' => 'nullable|unique:contacts,email,'.$id.',id,user_id,' . $userId,
            'number' => 'nullable|unique:contacts,number,'.$id.',id,user_id,' . $userId,
        );
    }

    public function setContactRulesValidtion($userId, $id = null)
    {
        $this->rules = array(
            'first_name' => 'max:255',
            'last_name' => 'max:255',
            'address' => 'max:500',
//            'number' => 'required|unique:contacts,number,' . $id,'id','user_id,'.$userId,
//            'email' => 'unique:contacts,email,' . $id,'id','user_id,'.$userId
            'email' => 'nullable|unique:contacts,email,NULL,id,user_id,' . $userId,
            'number' => 'nullable|unique:contacts,number,NULL,id,user_id,' . $userId
        );
    }

    public function setEmailRulesValidtion($locationId, $id = null)
    {
        $this->rules = array(
            'email' => 'required',
        );
    }


    public function setContactRulesForBroadcast($user_id, $id = null)
    {

        $this->rules = array(
            'number' => 'required'
        );
    }

    public function setEmailRulesValidationFormat()
    {
        $this->rules = array(
            'email' => 'email',
        );
    }

}
