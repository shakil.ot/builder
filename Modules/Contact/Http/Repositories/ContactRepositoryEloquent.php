<?php

namespace Modules\Contact\Http\Repositories;

use App\Repositories\BaseRepository\BaseRepository;
use Carbon\Carbon;
use Modules\Contact\Entities\Contact;
use Modules\Contact\Contracts\Repositories\ContactRepository;

class ContactRepositoryEloquent extends BaseRepository implements ContactRepository
{
    public function model()
    {
        return new Contact();
    }

    public function addContactByData($data)
    {
        return $this->model()
            ->create($data);
    }

    public function getAllContactsByUserId($userId)
    {
        return $this->model()
            ->where('user_id', $userId)
            ->orderBy('id', 'desc');
    }

    public function updateByField($where, $field)
    {
        return $this->model
            ->where($where)
            ->update($field);
    }

    public function getContactById($id)
    {

        return $this->model
            ->where(['id' => $id])
            ->with(['users'])
            ->first();
    }

    public function getContactByUserId($userId)
    {
        return Contact::where('user_id', $userId)->first();
    }


    public function findByUserIdAndNumber($userId, $email)
    {
        return $this->model
            ->where('user_id', $userId)
            ->where('email', $email)
            ->first();


    }

    public function findByUserIdAndEmail($userId, $email)
    {
        return $this->model
            ->where('user_id', $userId)
            ->where('email', $email)
            ->first();


    }


    public function addContactWithEmailSequence($request)
    {
        return Contact::create([
            'first_name' => $request['request']['first_name'],
            'last_name' => $request['request']['last_name'],
            'user_id' => $request['request']['user_id'],
            'number' => $request['request']['number'] ? $request['request']['number'] : '',
            'email' => $request['request']['email'] ? $request['request']['email'] : '',
            'is_active_automated_email_sequence' => $request['request']['automated_email_sequence'],
            'personal_note' => $request['request']['note'] ? $request['request']['note'] : '',
            'type' => $request['request']['type'],
            'status' => $request['request']['user_status'],
        ]);
    }

    public function addContactWithoutEmailSequence($request)
    {
        return Contact::create([
            'first_name' => $request['request']['first_name'],
            'last_name' => $request['request']['last_name'],
            'user_id' => $request['request']['user_id'],
            'number' => $request['request']['number'] ? $request['request']['number'] : '',
            'email' => $request['request']['email'] ? $request['request']['email'] : '',
            'personal_note' => $request['request']['note'] ? $request['request']['note'] : '',
            'type' => $request['request']['type'],
            'status' => $request['request']['user_status'],
        ]);
    }

    public function blockContact($userId, $contactId)
    {
        return $this->model->where('user_id', $userId)
            ->where('id', $contactId)
            ->update(['status' => \Modules\Contact\Entities\Contact::BLOCKED]);
    }


    public function getContactDataById($contactId)
    {
        return Contact::find($contactId);
    }

    public function updateContact($contactId, $request)
    {
        return Contact::find($contactId)
            ->update([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'number' => $request->number,
                'email' => $request->email,
                'address' => $request->address,
                'city' => $request->city,
                'zip' => $request->zip,
                'personal_note' => $request->note,
                'type' => $request->type,
                'status' => $request->user_status,
            ]);
    }

    public function updateContactByWhere($where, $update)
    {
        return $this->model()
            ->where($where)
            ->update($update);
    }

    public function getContactByEmail($email)
    {
        return $this->model()
            ->where('email', $email)
            ->first();
    }

    public function getContactByWhere($where)
    {
        return $this->model()
            ->where($where)
            ->first();
    }

    public function deleteContactById($contactId)
    {
        return Contact::find($contactId)
            ->delete();
    }

    public function getAllContactsByUserIdAndType($userId, $type)
    {
        return $this->model()
            ->where('user_id', $userId)
            ->where('type', $type)
            ->select('email', 'id','status', 'first_name', 'last_name', 'number')
            ->get();
    }

    public function getTotalSubscribersByDuration($userId, $durationType)
    {
        if ($durationType == 'week') {
            $data = Contact::where('created_at', '>=', Carbon::now()
                ->subdays(7))
                ->where('user_id', $userId)
                ->where('type', Contact::SUBSCRIBER);
            return $data->count();
        } elseif ($durationType == 'month') {
            $data = Contact::where('created_at', '>=', Carbon::now()
                ->subdays(30))
                ->where('user_id', $userId)
                ->where('type', Contact::SUBSCRIBER);
            return $data->count();
        } elseif ($durationType == 'forever') {
            $data = Contact::where('user_id', $userId)
                ->where('type', Contact::SUBSCRIBER);
            return $data->count();
        }

    }


    public function getTotalLeadsByDuration($userId, $durationType)
    {
        if ($durationType == 'week') {
            $data = Contact::where('created_at', '>=', Carbon::now()
                ->subdays(7))
                ->where('user_id', $userId)
                ->where('type', Contact::LEAD);
            return $data->count();
        } elseif ($durationType == 'month') {
            $data = Contact::where('created_at', '>=', Carbon::now()
                ->subdays(30))
                ->where('user_id', $userId)
                ->where('type', Contact::LEAD);
            return $data->count();
        } elseif ($durationType == 'forever') {
            $data = Contact::where('user_id', $userId)
                ->where('type', Contact::LEAD);
            return $data->count();
        }

    }


    public function insert($request, $userId, $source = ContactSourceType::UNKNOWN)
    {
        return $this->create([
            'user_id' => $userId,
            'first_name' => !empty($request['first_name']) ? $request['first_name'] : null,
            'last_name' => !empty($request['last_name']) ? $request['last_name'] : null,
            'number' => !empty($request['number']) ? $request['number'] : null,
            'email' => !empty($request['email']) ? strtolower($request['email']) : null,
            'address' => !empty($request['address']) ? $request['address'] : null,
            'city' => !empty($request['city']) ? $request['city'] : null,
            'state' => !empty($request['state']) ? $request['state'] : null,
            'zip' => !empty($request['zip']) ? $request['zip'] : null,
            'type' => !is_null($request['type']) ? $request['type'] : Contact::SUBSCRIBER,
            'status' => !is_null($request['user_status']) ? $request['user_status'] : Contact::ACTIVE,
        ]);
    }

    public function updateOrCreateContact($where, $value)
    {
        return $this->model->updateOrCreate($where, $value);
    }

    public function checkByCondition($where)
    {
        return $this->model()->where($where)->exists();

    }

    public function getContactByCondition($where)
    {
        return $this->model()->where($where)->first();
    }

    public function activateContactById($id)
    {
        return $this->model()
            ->where('id', $id)
            ->update(['status' => Contact::ACTIVE]);
    }

    public function blockContactById($id)
    {
        return $this->model()
            ->where('id', $id)
            ->update(['status' => Contact::BLOCKED]);
    }
}
