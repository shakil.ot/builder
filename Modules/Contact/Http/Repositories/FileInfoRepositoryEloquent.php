<?php

namespace Modules\Contact\Http\Repositories;

use App\Repositories\BaseRepository\BaseRepository;
use Modules\Contact\Contracts\Repositories\FileInfoRepository;
use Modules\Contact\Entities\FileInfo;

class FileInfoRepositoryEloquent extends BaseRepository implements FileInfoRepository
{
    public function model()
    {
        return new FileInfo();
    }

    public function insertFileInfo($fileData)
    {
        try {
            return $this->create([
                'user_id' => $fileData['user_id'],
                'file_name' => $fileData['custom_name'],
                'original_name' => $fileData['originalName'],
                'mapped_contacts' => $fileData['mapped_contacts'],
                'total_contacts' => $fileData['total_contacts'],
                'action_type' => isset($fileData['action_type']) ? $fileData['action_type'] : FileInfo::ACTION_TYPE_DEFAULT,
                'contact_type' => $fileData['contact_type'],
            ]);
        } catch (ValidatorException $e) {
        }
    }

    public function getAllImportedFilesByUserId($userId)
    {
        return $this->model->where('user_id', $userId)->where('action_type', \Modules\Contact\Entities\FileInfo::ACTION_TYPE_DEFAULT)->orderBy('updated_at', 'desc');
    }

    public function getFileByStatus($status)
    {
        return $this->model()
            ->where('file_infos.status', $status)
//            ->leftJoin('files', 'files.id', '=', 'file_infos.file_id')
            ->select
            (
                'file_infos.user_id as user_id',
                'file_infos.id as id',
                'file_infos.file_name as file_name',
                'file_infos.total_contacts as total_contacts',
                'file_infos.mapped_contacts as mapped_contacts',
                'file_infos.file_type as file_type',
                'file_infos.action_type as action_type',
                'file_infos.contact_type as contact_type'
            )
            ->first();
    }

    public function getTotalContactUploadedByFileId($id)
    {
        return $fileList = $this->model
            ->where('id', $id)
            ->select('total_contacts', 'total_contacts_uploaded')
            ->first();
    }

    public function  updateErrorInfo($fileId, $errorInfos)
    {
        return $this->model->where('id', $fileId)
            ->update(['error_infos'=> $errorInfos]);
    }

    public function updateTotalContact($id, $count)
    {
        $file = $this->findByField('id', $id)->first();
        return $this->find($file->id)->update([
            'total_contacts_uploaded' => $count,
        ]);
    }

    public function changeFileStatus($currentStatus, $changeStatus)
    {
        $file = $this->findByField('status', $currentStatus)->first();
        return $this->find($file->id)->update([
            'status' => $changeStatus
        ]);
    }

    public function getUserByFileId($fileId)
    {
        return $this->model
            ->where('id', $fileId)
            ->first();
    }


    public function getFileInfoByFileId($fileId)
    {
        return $this->model
            ->where('id', $fileId)
            ->first();
    }



}
