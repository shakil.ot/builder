<?php

namespace Modules\Contact\Http\Repositories;

use App;

use App\Repositories\BaseRepository\BaseRepository;
use DB;
use Modules\Contact\Contracts\Repositories\FileContactRepository;
use Modules\Contact\Entities\FileContact;


/**
 * Class UserRepositoryEloquent
 * @package namespace App\Repositories;
 */
class FileContactRepositoryEloquent extends BaseRepository implements FileContactRepository
{

    protected $model;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return FileContact::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function insert($request)
    {

        return FileContact::create([
            'file_info_id' => $request['file_id'],
            'contact_id' => $request['contact_id'],

        ]);
    }

    public function insertFileContact($file_id, $contact_id)
    {

        return $this->create([
            'file_info_id' => $file_id,
            'contact_id' => $contact_id,

        ]);
    }

    public function isExists( $contact_id, $file_id)
    {
        return FileContact::where([
            'contact_id'  => $contact_id,
            'file_info_id'  => $file_id
        ])->first();
    }
}
