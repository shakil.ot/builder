<?php

namespace Modules\Contact\Http\Repositories;

use App\Repositories\BaseRepository\BaseRepository;
use Modules\Contact\Contracts\Repositories\FileListContactRepository;
use Modules\Contact\Entities\FileContact;

class FileListContactRepositoryEloquent extends BaseRepository implements FileListContactRepository
{
    protected function model()
    {
        return new FileContact();
    }

    public function getContactsByFileId($file_id)
    {
        return $this->model
            ->join('file_infos','file_infos.id','=','file_contacts.file_info_id')
            ->join('contacts','contacts.id','=','contact_id')
            ->where('file_infos.id', $file_id)
            ->select('contacts.id as id','contacts.first_name as first_name','contacts.last_name as last_name', 'contacts.number as number', 'contacts.email as email', 'contacts.city as city', 'contacts.created_at as created_at')
            ->orderBy('contacts.id','desc');
    }
}