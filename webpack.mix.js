const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
// mix.react('resources/components/Cart.js', 'public/js')

mix.scripts([
    'public/assets/theme/plugins/global/plugins.bundle.js',
    'public/assets/theme/plugins/custom/prismjs/prismjs.bundle.js',
    'public/assets/theme/js/scripts.bundle.js',
    'public/assets/theme/js/pages/widgets.js',
    'public/assets/global/scripts/parsleyjs/dist/parsley.min.js',
    'public/assets/global/scripts/global.js',
    'public/assets/theme/plugins/custom/datatables/datatables.bundle.js',
    'public/assets/preloader/preloader.js',
    // 'public/assets/global/scripts/sweetalert2.all.min.js',
    'resources/js/tab.form.js',
    'resources/js/bootstrap-notify.js',
    'public/assets/theme/js/pages/crud/forms/widgets/select2.js',
    'public/assets/global/plugins/moment-timezone-with-data-2012-2022.min.js'
], 'public/js/scripts.js');



mix.js('resources/js/bootstrap-notify.js', 'public/js/notify_js.js').version();

/* start: User Modules js */
mix.scripts([
    'Modules/UserRegistration/Resources/assets/js/wizard-1.js',
    'Modules/UserRegistration/Resources/assets/js/app.js',
], 'public/js/user-registration.js').version();

mix.js('Modules/UserReports/Resources/assets/js/app.js', 'public/js/user-report.js').version();
mix.js('Modules/UserDashboard/Resources/assets/js/profile.js', 'public/js/user-profile.js').version();
mix.js('Modules/Onboarding/Resources/assets/js/app.js', 'public/js/user-onboarding.js').version();
mix.js('Modules/UserRegistration/Resources/assets/js/app.js', 'public/js/user-registration.js').version();
mix.js('Modules/UserDashboard/Resources/assets/js/dashboard.js', 'public/js/dashboard.js').version();
mix.js('Modules/UserDashboard/Resources/assets/js/overall-stat-count.js', 'public/js/overall-stat-count.js').version();
mix.js('Modules/Contact/Resources/assets/js/contact-manage.js', 'public/js').version();
mix.js('Modules/Contact/Resources/assets/js/upload-bulk-contacts.js', 'public/js').version();
mix.js('Modules/EmailSetting/Resources/assets/js/email-setting.js', 'public/js').version();
mix.js('Modules/UserInvoice/Resources/assets/js/user-invoice.js', 'public/js').version();
mix.js('Modules/UserInvoice/Resources/assets/js/invoice-pdf.js', 'public/js').version();
mix.js('Modules/Timeline/Resources/assets/js/timeline.js', 'public/js').version();

/* end: User Modules js */

/* start: Admin  js */
mix.js('resources/js/admin-user.js', 'public/js/admin-user.js').version();
mix.js('Modules/Package/Resources/assets/js/package.js', 'public/js/package.js').version();
mix.js("Modules/AdminReport/Resources/assets/js/payment-report-user-wise.js", "public/js/payment-report-user-wise.js").version();
mix.js("Modules/AdminReport/Resources/assets/js/payment-report.js", "public/js/payment-report.js").version();
mix.js("Modules/AdminEmailTemplate/Resources/assets/js/email-template.js", "public/js/email-template.js").version();
mix.js("Modules/AdminAutoFollowup/Resources/assets/js/admin-auto-followup.js", "public/js/admin-auto-followup.js").version();


mix.styles(['Modules/Onboarding/Resources/assets/css/onboarding_backup.css'], 'public/css/onboarding_backup.css').version();


/* end: Admin js */

/* start: Category  js */
// mix.js('Modules/Category/Resources/assets/js/category.js', 'public/js/category.js').version();
/* end: Category js */

/* start: AdminWebBuilder  js */
mix.js("Modules/AdminWebBuilder/Resources/assets/js/app.js", "public/js/admin-template.js").version();
/* end: AdminWebBuilder js */

/* start: Setting  js */
// mix.js('Modules/Setting/Resources/assets/js/app.js', 'public/js/businessSetting.js').version();
/* end: Setting,js js */


/* start: ThemeSelect  js */
// mix.js('Modules/Setting/Resources/assets/js/themeSelect.js', 'public/js/').version();
/* end: ThemeSelect js */

/* Password reset for user */
mix.js('resources/js/password-reset.js', 'public/js/password-reset.js').version();



mix.styles([
    'public/assets/theme/plugins/global/plugins.bundle.css',
    'public/assets/theme/plugins/custom/prismjs/prismjs.bundle.css',
    'public/assets/theme/css/style.bundle.css',
    'public/assets/theme/css/themes/layout/header/base/light.css',
    'public/assets/theme/css/themes/layout/header/menu/light.css',
    'public/assets/theme/css/themes/layout/brand/light.css',
    'public/assets/theme/css/themes/layout/aside/light.css',
    'resources/css/tab.form.css',
    'public/assets/theme/plugins/custom/datatables/datatables.bundle.css',
    'public/assets/global/scripts/parsleyjs/src/parsley.css',
    'public/assets/preloader/preloader.css',
    'resources/css/green.css',
    'resources/css/custom.css',
], 'public/css/all.css');

mix.styles([
    'public/assets/theme/plugins/global/plugins.bundle.css',
    'public/assets/theme/plugins/custom/prismjs/prismjs.bundle.css',
    'public/assets/theme/css/style.bundle.css',
    'public/assets/theme/css/themes/layout/header/base/light.css',
    'public/assets/theme/css/themes/layout/header/menu/light.css',
    'public/assets/theme/css/themes/layout/brand/light.css',
    'public/assets/theme/css/themes/layout/aside/light.css',
    'resources/css/tab.form.css',
    'public/assets/theme/plugins/custom/datatables/datatables.bundle.css',
    'public/assets/global/scripts/parsleyjs/src/parsley.css',
    'public/assets/preloader/preloader.css',
    'resources/css/green.css',
    'resources/css/custom.css',
    // 'Modules/Cart/Resources/assets/css/cart.css',
], 'public/css/cart.css');

mix.styles([
    'Modules/Onboarding/Resources/assets/css/app.css'
], 'public/css/onboarding.css');

mix.scripts([
    'public/assets/global/plugins/jquery/jquery.min.js',
    'Modules/WebBuilder/Resources/assets/js/app.js',
    'public/assets/global/plugins/toastr/toastr.min.js',
], 'public/js/UserWebBuilder.js').version();

mix.scripts([
    'public/assets/global/plugins/jquery/jquery.min.js',
    'public/assets/global/plugins/toastr/toastr.min.js',
    'Modules/AdminWebBuilder/Resources/assets/js/adminWebBuilder.js'
] ,  'public/js/adminWebBuilder.js').version();

mix.scripts([
    'public/assets/global/plugins/jquery/jquery.min.js',
    'public/assets/global/plugins/toastr/toastr.min.js',
    'Modules/AdminWebBuilder/Resources/assets/js/adminWebBuilderEdit.js'
] ,  'public/js/adminWebBuilderEdit.js').version();

/* ADMIN EBOOK */

mix.js("Modules/AdminEbook/Resources/assets/js/app.js", "public/js/adminEbook.js").version();


// mix.scripts([
//     'public/assets/global/plugins/jquery/jquery.min.js',
//     'public/assets/global/plugins/toastr/toastr.min.js',
//     'Modules/AdminEbook/Resources/assets/js/adminEbook.js'
// ] ,  'public/js/adminEbookBuilder.js').version();
