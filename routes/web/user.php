<?php
/**
 * User: Orangetoolz.com
 * Date: 6/11/20
 * Time: 4:05 PM
 */

Auth::routes();
Route::prefix('user')->name('user.')->namespace('User')->middleware(['auth', 'web', 'checkUserJourneyMode'])->group(function(){
    Route::post('/filter/getData','HomeController@getFilterData')->name('filter.get-data');
    Route::get('/app/screens', 'HomeController@appScreen')->name('app.screen');
    Route::get('/tester', 'HomeController@test')->name('test');

});






