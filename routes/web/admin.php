<?php
/**
 * User: Rafiqul Islam
 * Date: 6/11/20
 * Time: 1:25 PM
 */

Route::prefix('admin')->name('admin.')->namespace('Admin\Auth')->middleware(['web'])->group(function(){
    //Login Routes
    Route::get('/','LoginController@showLoginForm')->name('login');
    Route::get('/login','LoginController@showLoginForm')->name('login');
    Route::post('/login','LoginController@login');
    Route::post('/logout','LoginController@logout')->name('logout');

    /* Logs */
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

});


//// FOR USER MANAGEMENT
Route::prefix('admin')->name('admin.')->namespace('Admin')->middleware(['admin', 'web'])->group(function(){


    //Login Routes
    Route::get('/dashboard','DashboardController@index')->name('dashboard');

    // USER MANAGEMENT ROUTES
    Route::get('/user', 'User\UsersController@index')->name('manage-user');
    Route::get('/user/list', 'User\UsersController@listUserData')->name('user-list');
    Route::get('/user/form', 'User\UsersController@getUserAddForm')->name('user-add-form');
    Route::post('/user/form/submit', 'User\UsersController@userAddFormSubmit')->name('user-add-submit');

    //// EDIT ROUTE
    Route::post('/user/edit/form', 'User\UsersController@getUserEditForm')->name('user-edit-form');
    Route::post('/user/edit/submit', 'User\UsersController@userEditFormSubmit')->name('user-edit-submit');
    //// ACTIVE DEACTIVATED ROUTE
    Route::post('/user/deactivate-user', 'User\UsersController@deactivateUser')->name('deactivate-user');
    Route::post('/user/activate-user', 'User\UsersController@activateUser')->name('activate-user');
    Route::post('/user/details', 'User\UsersController@getViewUserInformation')->name('user-details');
    //Auto Recurring stop/start
    Route::post('/user/stop-auto-recurring', 'User\UsersController@stopAutoRecurring')->name('stop-auto-recurring');
    Route::post('/user/start/auto-recurring', 'User\UsersController@startAutoRecurring')->name('start-auto-recurring');

    Route::get('/user/force/login/{userId}', 'User\UsersController@forceLogin')->name('login-as-user');

    Route::post('/user/delete', 'User\UsersController@deleteUser')->name('delete-user');


    //Package allocated
    Route::post('/user/package/allocate/view', 'User\UsersController@packageAllocateView')->name('package-allocate-view');
    Route::post('/user/package/allocate/in/user', 'User\UsersController@packageAllocateInUser')->name('package-allocate-in-user');


});

