<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\User;

Auth::routes();

require base_path('routes/web/admin.php');
require base_path('routes/web/user.php');


Route::post('/password/token/submit', 'Auth\LoginController@forgetPasswordTokenSet')->name('forget-password-token-submit');
Route::get('/password/change/{email}', 'Auth\LoginController@passwordReset')->name('change-password');
Route::post('/password/reset/submit', 'Auth\LoginController@resetPasswordSubmit')->name('reset-password-submit');



