var bootstrapNotify = function() {

    var notifyFunctionObj = function (content) {

        var notifyFunctionObj = $.notify(content, {
            type: content.type,
            allow_dismiss: true,
            newest_on_top: true,
            mouse_over:  true,
            showProgressbar:  true,
            spacing: 10,
            timer: 2000,
            placement: {
                from: 'top',
                align: 'right'
            },
            offset: {
                x: 30,
                y: 30
            },
            delay: 1000,
            z_index: 10000,
            animate: {
                enter: 'animate__animated animate__bounce',
                exit: 'animate__animated animate__bounce'
            }
        });

    };


    return {
        success: function (message, title, icon ) {
            var content = {};
            content.message = message;
            content.title = title;
            content.type = "success";
            content.icon = "flaticon-signs" ;
            content.target = '';
            notifyFunctionObj(content);
        },
        error: function (message, title, icon) {
            var content = {};
            content.message = message;
            content.title = title;
            content.type = "danger";
            content.icon = "la la-warning" ;
            content.target = '';
            notifyFunctionObj(content);
        }
    };
}();
