<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->integer('parent_id')->index()->nullable();
            $table->string('ip')->nullable();
            $table->string('profile_image')->nullable();
            $table->string('phone')->index()->unique();
            $table->text('address')->nullable();
            $table->string('state')->nullable();
            $table->string('city')->nullable();
            $table->string('zip_code')->nullable();
            $table->string('country_name')->nullable();
            $table->integer('user_roles_id')->nullable();
            $table->integer('code')->nullable();
            $table->dateTime('expire_date')->nullable();
            $table->tinyInteger('status')->default(1)->comment('1=active|0=inactive|2=unregistered');
            $table->string('auth_email')->nullable();
            $table->string('access_token')->nullable();
            $table->tinyInteger('journey_mode')->default(1);
            $table->tinyInteger('journey_mode_last_landing_step')->default(1);
            $table->tinyInteger('user_created_from')->default(1)->comment('1=Direct Registration | 2=From admin side | 3=From API');
            $table->tinyInteger('country')->default(1)->comment('1=International | 2=Bangladesh');
            $table->tinyInteger('package_created_from')->default(0)->comment('0=No Package | 1=Direct Registration | 2=From admin side | 3=From API');

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
