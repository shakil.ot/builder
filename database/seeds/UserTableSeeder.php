<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        DB::table('users')->insert([
            'first_name' => 'Demo',
            'last_name' => 'User',
            'email' => 'user@gmail.com',
            'phone' => '12345678901',
            'status' => 1,
            'email_verified_at' => now(),
            'password' => bcrypt('secret'),
            'remember_token' => Str::random(10),

        ]);
    }
}
