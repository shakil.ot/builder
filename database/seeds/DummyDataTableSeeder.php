<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Modules\VirtualNumber\Entities\GatewayProviders;

class DummyDataTableSeeder extends Seeder
{
    /**
     * @var Faker\Generator
     */
    private $faker;

    public function __construct(Faker\Generator $faker)
    {
        $this->faker = $faker;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\Modules\UserCompany\Entities\Company::class, 5)->create();
        factory(\Modules\UserCompany\Entities\CompanyLevel::class, 20)->create();
        factory(\Modules\UserCompany\Entities\CompanyGroup::class, 20)->create();
        factory(\Modules\UserCompany\Entities\CompanyRole::class, 20)->create();
        factory(\Modules\UserAchievement\Entities\CompanyAchievement::class, 100)->create();
        factory(\Modules\UserLeaderboard\Entities\CompanyLeaderboard::class, 100)->create();
        factory(\Modules\UserReward\Entities\CompanyReward::class, 100)->create();
        factory(\Modules\UserTraining\Entities\CompanyTraining::class, 100)->create();

        factory(App\User::class, 30)->create();

        factory(Modules\UserEmployee\Entities\Employee::class, 30)->create();
        factory(Modules\UserEmployee\Entities\EmployeeXpPoint::class, 100)->create();
    }
}
