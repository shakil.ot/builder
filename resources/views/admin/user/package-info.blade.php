<div class="row">

<div class="card-body col-xl-6">

    <!--begin::User-->
    <div class="d-flex align-items-center">
        <div class="symbol symbol-60 symbol-xxl-100 mr-5 align-self-start align-self-xxl-center">
            <div class="symbol-label" style="background-image:url('{{asset('/assets/theme/media/users/default.jpg')}}')"></div>
            <i class="symbol-badge bg-success"></i>
        </div>
        <div>
            <a href="#" class="font-weight-bolder font-size-h5 text-dark-75 text-hover-primary">{{ $userInfo->first_name }} {{ $userInfo->last_name }} </a>
            <div class="mt-2">

            </div>
        </div>
    </div>
    <!--end::User-->
    <!--begin::Contact-->
    <div class="py-9">
        <div class="d-flex align-items-center justify-content-between mb-2">
            <span class="font-weight-bold mr-2">Email:</span>
            <a href="#" class="text-muted text-hover-primary">
                <span class="label label-lg label-light-primary label-inline font-weight-bold py-4">
                {{ $userInfo->email }}
                </span>
            </a>
        </div>
        <div class="d-flex align-items-center justify-content-between mb-2">
            <span class="font-weight-bold mr-2">Phone:</span>
            <span class="text-muted">{{ $userInfo->phone }}</span>
        </div>
        <div class="d-flex align-items-center justify-content-between">
            <span class="font-weight-bold mr-2">address:</span>
            <span class="text-muted">@if($userInfo->address!="") {{ $userInfo->address }} @else {{ "Address not found" }} @endif</span>
        </div>
    </div>
    <!--end::Contact-->

</div>
<div class="card-body col-xl-6">
    <!--begin::User-->
    <div class="d-flex align-items-center">
        <div class="symbol symbol-60 symbol-xxl-100 mr-5 align-self-start align-self-xxl-center">
            <div class="symbol-label" ></div>
        </div>
        <div>
            <a href="#" class="font-weight-bolder font-size-h5 text-dark-75 text-hover-primary">Package Information</a>
        </div>
    </div>
    <!--end::User-->
    <!--begin::Contact-->
    <div class="py-9">
        @if($packageData != null)
        <div class="d-flex align-items-center justify-content-between mb-2">
            <span class="font-weight-bold mr-2">Package Name:</span>
            <a href="#" class="text-muted text-hover-primary text-green ">
                <span class="label label-lg label-light-primary label-inline font-weight-bold py-4">
                {{ $packageData->package_name }}
                </span>
            </a>
        </div>
        <div class="d-flex align-items-center justify-content-between mb-2">
            <span class="font-weight-bold mr-2">Subscription Fee:</span>
            <span class="text-muted">
                <span class="label label-lg label-light-danger label-inline font-weight-bold py-4">
                    {{ $packageData->subscription_fee }}
                </span>
                </span>
        </div>
        <div class="d-flex align-items-center justify-content-between mb-2">
            <span class="font-weight-bold mr-2">Expire Date:</span>
            <span class="text-muted">{{ $packageData->expire_date }}</span>
        </div>

        @else

            <div class="col-xl-12">
                <div>
                    <span class="btn-danger btn d-block mt-3 p-2 rounded text-center">
                        <span>This user does not have a package! </span>
                    </span>
                </div>
            </div>

        @endif
    </div>
    <!--end::Contact-->

</div>
</div>
