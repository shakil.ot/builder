@extends('admin.layout.main')

@section('container')
    <div class="visible m-portlet__body extra p-0">
        <div class="section-toggle change-content-section fb-page-management-wrapper">
            <div class="row pane-section-form">
                <!-- Grid Item -->
                <div class="col-12">
                    <!-- Card Header -->
                    <div class="dt-card__header mb-0 extra pt-0">
                        <!-- Card Heading -->
                        <div class="dt-card__heading">
                            <h3 class="dt-card__title">Un-registered Users' List</h3>
                        </div>
                        <!-- /card heading -->
                        <!-- /card tools -->
                    </div>

                </div>
                <!-- /card -->

            </div>

        </div>


        <div class="row pane-section-content open mt-4">
            <!-- Grid Item -->
            <div class="col-12">

                <!-- Card -->
                <div class="dt-card">

                    <!-- /card header -->

                    <!-- Card Body -->
                    <div class="dt-card__body mt-0">

                        <input type="hidden" name="_token" value="{{csrf_token()}}">


                        <table id="unregistered-user-table"
                               class="table table-striped table-bordered table-hover table-checkable no-wrap dataTable dtr-inline collapsed "
                               style="width:100%">
                            <thead>
                            <tr>
                                <th>User Type</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Status</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                        </table>

                    </div>
                    <!-- /card body -->

                </div>
                <!-- /card -->

            </div>

        </div>
    </div>


@endsection

@push('scripts')

    <script src="{{asset('assets/dataTable/js/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/dataTable/js/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/dataTable/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/dataTable/js/buttons.print.min.js')}}"></script>

    <script>


        $(function () {

            // Add overflow_visible to prevent hidden action option
            $('.visible.m-portlet__body.extra.p-0').parents('.m-grid__item.m-grid__item--fluid.m-wrapper').addClass('overflow_visible');

            var user_operation_callback = function (response) {
                swal(response.status, response.html, response.status);
                $('#unregistered-user-table').DataTable().ajax.reload();
            }
            $.extend(true, $.fn.dataTable.defaults, {
                "language": {
                    "search": "",
                    // search: "_INPUT_",
                    "searchPlaceholder": "Search",
                    "sLengthMenu": "_MENU_ records",
                    "paginate": {
                        "first": "<<",
                        "previous": '<span class="fa fa-chevron-left"></span>',
                        "next": '<span class="fa fa-chevron-right"></span>',
                        "last": ">>"
                    },
                }
            });

            const _token = '{{ csrf_token() }}';

            (function (global, $, _token) {

                var dataTable = $('#unregistered-user-table').DataTable({
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    stateSave: false,
                    "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                    pageLength: 10,
                    "renderer": "bootstrap",
                    ajax: {
                        url: '{!! route('unregistered-users-list') !!}'
                    },
                    columns: [
                        {data: 'user_type', name: 'parent_id', "orderable": true, "searchable": true, width: "11%",visible: false},
                        {data: 'first_name', name: 'first_name', "orderable": true, "searchable": true, width: "11%"},
                        {data: 'last_name', name: 'last_name', "orderable": true, "searchable": true, width: "11"},
                        {data: 'email', name: 'email', "orderable": true, "searchable": true, width: "15%"},
                        {data: 'my_phone', name: 'phone', "orderable": true, "searchable": true, width: "15%"},
                        {data: 'my_status', name: 'status', "orderable": true, "searchable": true, width: "11%"},
                        {data: 'created_at', name: 'created_at', "orderable": true, "searchable": false, width: "11%"},
                        {data: 'action', name: 'action', "orderable": false, searchable: false, width: "15%"}
                    ],

                    columnDefs: [
                        {responsivePriority: 1, targets: 7},
                        {responsivePriority: 2, targets: 1}
                    ],
                    dom: '<"top-toolbar row"<"top-left-toolbar col-md-9"lB><"top-right-toolbar col-md-3"f>>rt<"bottom-toolbar"<"bottom-left-toolbar"i><"bottom-right-toolbar"p>>',
                    buttons: [
                        {
                            extend: 'colvis',
                            text: '<i class="fa \tfa-list-ul pr-1"></i>Columns',
                            attr: {
                                id: "custom-colvis-button",
                                class: 'btn btn-outline-primary btn-sm m-btn',
                                style: '',
                                title: 'Columns Visibility'
                            },
                            columns: ':not(.noVis)',
                            columnText: function (dt, idx, title) {
                                // return (idx+1)+': '+title;
                                return title;
                            },
                            postfixButtons: ['colvisRestore']
                        },
                        {
                            extend: 'collection',
                            text: 'Export To',
                            autoClose: true,
                            attr: {
                                id: "export_button",
                                class: 'btn btn-outline-primary btn-sm m-btn dropdown-toggle',
                                style: '',
                                title: 'Export'
                            },
                            buttons: [
                                {
                                    extend: 'csv',
                                    title: "Unregistered Users List " + (new Date()).toDateString(),

                                    exportOptions: {
                                        columns: [1, 2, 3, 4,6],
                                        modifier: {selected: null}
                                    }
                                }, {
                                    extend: 'excel',
                                    title: "Unregistered Users List " + (new Date()).toDateString(),

                                    exportOptions: {
                                        columns: [1, 2, 3, 4,6],
                                        modifier: {selected: null}
                                    }
                                },{
                                    extend: 'pdf',
                                    title: "Unregistered Users List " + (new Date()).toDateString(),

                                    exportOptions: {
                                        columns: [1, 2, 3, 4,6],
                                        modifier: {selected: null}
                                    }
                                },{
                                    extend: 'print',
                                    title: "Unregistered Users List " + (new Date()).toDateString(),

                                    exportOptions: {
                                        columns: [1, 2, 3, 4],
                                        modifier: {selected: null}
                                    }
                                },{
                                    extend: 'copy',
                                    title: "Unregistered Users List " + (new Date()).toDateString(),

                                    exportOptions: {
                                        columns: [1, 2, 3, 4,6],
                                        modifier: {selected: null}
                                    }
                                }
                            ],
                            fade: true
                        },
                    ]
                });
                //Columns visibility buttons' css
                $("#custom-colvis-button").click(function () {
                    $(".buttons-columnVisibility").removeClass("dt-button").addClass("btn btn-outline-primary btn-sm m-btn btn-block");
                    $(".buttons-colvisRestore").removeClass("dt-button").addClass("btn btn-outline-brand btn-sm m-btn btn-block");
                });

                var userId = "";
                dataTable.on('click', '.action', function () {
                    userId = $(this).attr('data-id');
                    var settingsType = $(this).attr('data-value');
                    //var settingsType = $(this).val();

                    if (!settingsType)
                        return false;

                    switch (settingsType) {

                        case "edit-user":
                            editUser(userId);
                            break;

                        case "delete-user":
                            deleteUser(userId);
                            break;

                        default :
                            toastr.error("Invalid Setting type", "Server Notification");
                            break;

                    }


                    function editUser(userId) {
                        $('#preloader').show();
                        var editPopUp = global.customPopup.show({
                            header: 'Edit User',
                            message: '',
                            dialogSize: 'lg',
                            dialogClass: 'prevent-to-show'
                        });

                        editPopUp.on('shown.bs.modal', function () {
                            var modal = $(this);

                            global.mySiteAjax({
                                url: '{!! route('user-edit-form') !!}',
                                type: 'post',
                                data: {user_id: userId, _token: _token,},
                                loadSpinner: true,
                                success: function (response) {
                                    $('#preloader').hide();
                                    window.nbUtility.ajaxErrorHandling(response, function () {
                                        modal.find('.modal-body').html(response.html);
                                        modal.removeClass('prevent-to-show');
                                        Inputmask.init();

                                        modal.find('.user-type-select-option').prop('disabled', true);

                                        $('#user-from').parsley().on('field:validated', function () {
                                            var ok = $('.parsley-error').length === 0;
                                            $('.bs-callout-info').toggleClass('hidden', !ok);
                                            $('.bs-callout-warning').toggleClass('hidden', ok);
                                        }).on('form:submit', function (formInstance) {
                                            $('.error-message').html('');
                                            $('#preloader').show();

                                            var values = $("#user-from").serializeArray();
                                            var phone = $('#phone').val().replace('(', '').replace(')', '').replace('-', '').replace(' ', '');

                                            if (phone.length == '10' || phone.length == 10) {
                                                phone = 1 + '' + phone;
                                            }

                                            values.find(input => input.name == 'phone').value = phone;
                                            $.ajax({
                                                url: '{!! route('user-edit-submit') !!}',
                                                type: 'post',
                                                data: values,
                                                loadSpinner: true,
                                                success: function (data) {
                                                    $('#preloader').hide();
                                                    if (data.status == 'success') {
                                                        $("#unregistered-user-table").DataTable().ajax.reload();
                                                        modal.modal('hide');
                                                        toastr.success(data.html, '', {"closeButton": true})
                                                    } else {
                                                        toastr.error(data.html, '', {"closeButton": true})
                                                    }
                                                },
                                                //error messages
                                                error: function (data) {
                                                    $('#preloader').hide();
                                                    if (data.status == 422) {
                                                        global.generateErrorMessage(formInstance, data.responseJSON.errors);

                                                        if (data.responseJSON.errors.first_name !== undefined)
                                                            toastr.error(data.responseJSON.errors.first_name, '', {"closeButton": true})

                                                        if (data.responseJSON.errors.last_name !== undefined)
                                                            toastr.error(data.responseJSON.errors.last_name, '', {"closeButton": true})

                                                        if (data.responseJSON.errors.email !== undefined)
                                                            toastr.error(data.responseJSON.errors.email, '', {"closeButton": true})

                                                        if (data.responseJSON.errors.phone !== undefined)
                                                            toastr.error(data.responseJSON.errors.phone, '', {"closeButton": true})

                                                    }

                                                }
                                            });

                                        });
                                    });
                                }
                            });
                        });

                        editPopUp.on('hidden.bs.modal', function () {
                            $(this).remove();
                        });
                    }


                    function deleteUser(userId) {
                        swal({
                                title: "Are you sure?",
                                text: "Delete this user !!",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Yes, delete it!",
                                cancelButtonText: "No, cancel please!",
                                closeOnConfirm: false,
                                closeOnCancel: true
                            },
                            function (isConfirm) {
                                if (isConfirm) {
                                    let dataObject = {
                                        url: '{!! route('delete-user') !!}',
                                        type: 'post',
                                        data: {user_id: userId, _token: _token},
                                        callback: user_operation_callback,
                                        callBackError: callBackError,
                                    }
                                    globalAjax(dataObject);
                                }
                            });
                    }
                });

                $(document).on('click', '.delete-user', function () {
                    var userId = $(this).attr('data-id');

                    swal({
                            title: "Are you sure?",
                            text: "Delete this user !!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Yes, delete it!",
                            cancelButtonText: "No, cancel please!",
                            closeOnConfirm: false,
                            closeOnCancel: true
                        },
                        function (isConfirm) {
                            if (isConfirm) {
                                let dataObject = {
                                    url: '{!! route('delete-user') !!}',
                                    type: 'post',
                                    data: {user_id: userId, _token: _token},
                                    callback: user_operation_callback,
                                    callBackError: callBackError,
                                }
                                globalAjax(dataObject);
                            }
                        });
                });
            })(window, jQuery, _token);
        });
    </script>
@endpush
