<form action="" id="user-from" class="form-horizontal va_gb_form" data-parsley-validate="" onsubmit="event.preventDefault();">
    @csrf
    @if(!empty($userData))
        <input type="hidden" name="user_id" value={{$userData->id}}>
    @endif
    <input type="hidden" name="country" value="{{\App\User::COUNTRY_INTERNATIONAL}}">
    <div class="form-group m-form__group row">
        <div class="col-lg-6">
            <label>
                First Name
            </label>
            <span class="required-field">*</span>

            <div class="m-input-icon m-input-icon--right">
                <input type="text" name="first_name" class="form-control m-input" placeholder="Enter first name"
                       autocomplete="off" value="{{empty($userData->first_name) ? null : $userData->first_name}}"
                       data-parsley-trigger="change focusout"
                       data-parsley-required-message="First Name is required!"
                       required>

                <span class="m-input-icon__icon m-input-icon__icon--right">

                </span>
            </div>
        </div>
        <div class="col-lg-6">
            <label>
                Last Name
            </label>
            <span class="required-field">*</span>
            <div class="m-input-icon m-input-icon--right">
                <input type="text" name="last_name" class="form-control m-input" placeholder="Enter last name"
                       autocomplete="off" value="{{empty($userData->last_name) ? null : $userData->last_name}}"
                       data-parsley-trigger="change focusout"
                       data-parsley-required-message="Last Name is required!"
                       required>

                <span class="m-input-icon__icon m-input-icon__icon--right">

                </span>
            </div>
        </div>
    </div>


    <div class=" m-form__group row">
        <div class="form-group col-lg-6">
            <div class="custom-error-message">
                <label>
                    Email
                </label>

                <span class="required-field">*</span>
                <div class="m-input-icon m-input-icon--right">
                    <input id="email" type="email" name="email" class="form-control m-input" placeholder="Enter email"
                           autocomplete="off" value="{{empty($userData->email) ? null : $userData->email}}" required
                           data-parsley-required-message="Email is required!"
                           data-parsley-trigger="change focusout">

                    <span class="m-input-icon__icon m-input-icon__icon--right">

                </span>
                </div>
            </div>
        </div>
        <div class="form-group col-lg-6 ">
            <div class="custom-error-message">
                <label>
                    Phone
                </label>
                <span class="required-field">*</span>
                <div class="m-input-icon m-input-icon--right">
                    <input type="text"
                           id="phone"
                           name="phone" class="form-control m-input"
                           placeholder="Enter phone number"
                           autocomplete="off"
                           value="{{empty($userData->phone) ? null : $userData->phone}}"
                           required
                           data-parsley-required-message= "Phone Number is required!"
                           data-parsley-trigger="change focusout">
                    <span class="help-block"></span>
                    <span class="m-input-icon__icon m-input-icon__icon--right">
                </span>
                </div>
            </div>

            </div>
        </div>

{{--    <div class=" m-form__group row">--}}
{{--        <div class="form-group col-lg-6">--}}
{{--            <div class="custom-error-message">--}}
{{--                <label>--}}
{{--                    Country--}}
{{--                </label>--}}

{{--                <span class="required-field">*</span>--}}
{{--                <div class="m-input-icon m-input-icon--right">--}}
{{--                    <select name="country" id="country" class="form-control" required>--}}
{{--                        <option value="{{\App\User::COUNTRY_INTERNATIONAL}}" {{isset($userData->country) ? ($userData->country == \App\User::COUNTRY_INTERNATIONAL ? 'selected':''):''}}>International</option>--}}
{{--                        <option value="{{\App\User::COUNTRY_BANGLADESH}}" {{isset($userData->country) ? ($userData->country == \App\User::COUNTRY_BANGLADESH ? 'selected':''):''}}>Bangladesh</option>--}}

{{--                    </select>--}}

{{--                    <span class="m-input-icon__icon m-input-icon__icon--right">--}}

{{--                </span>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--    </div>--}}



        <div class="m-form__actions  text-right">
            @if($submit_button == 'Update User')
                <button type="submit" class="btn" style="background-color: #023c64; color: #ffffff">
                    <i class="flaticon-interface-5" style="color: #ffffff"></i> {{$submit_button}}
                </button>
            @else
                <button type="submit" class="btn" style="background-color: #023c64; color: #ffffff">
                    <i class="flaticon-add-circular-button" style="color: #ffffff"></i> {{$submit_button}}
                </button>
            @endif
        </div>

</form>
