<div class="modal-body p-0">
    <form action="" id="package_allocate_form">
        @csrf

        @if(count($packages) > 0)
            <div class="form-group m-form__group row">
            <div class="col-xl-12">
                <select name="package_id" id="package-allocate-id" class="form-control" required="">
                    @foreach($packages as $package)
                        <option value="{{ $package->id  }}">{{ $package->name  }}</option>
                    @endforeach
                </select>
            </div>
            </div>


                <input type="hidden" name="userId" value="{{ $user_id }}">


            <div class="m-form__actions  text-right">
                    <button type="submit" class="btn btn-primary"><i class="flaticon-interface-5"></i> Allocate Package
                    </button>
            </div>
        @else
            <div class="col-xl-12">
                <div>
                    <span class="btn-danger btn d-block mt-3 p-2 rounded text-center">
                    <span>Free Package not found! </span>
                </span>
                </div>
            </div>
        @endif

    </form>
</div>

