@extends('admin.layouts.app')

@push('styles')
    <style>
        .top-left-toolbar{
            margin-right: -78px;
        }
    </style>
@endpush


@section('container')

    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">

        <!--begin::Entry-->
        <div class="card card-custom">
            <div class="card-header">
                <div class="card-title">
                    <span class="card-icon">
                        <i class="flaticon-users-1 text-primary"></i>
                    </span>
                    <h3 class="card-label">Users</h3>
                </div>
                <div class="card-toolbar">
                    <!--begin::Dropdown-->

                    <!--end::Dropdown-->
                    <!--begin::Button-->
                    <a class="btn font-weight-bolder add-new-item" style="background-color: #023c64; color: #ffffff">+ New User</a>

                    <!--end::Button-->
                </div>
            </div>
            <div class="card-body">
            @include('admin.user.filter')
                <!--begin: Datatable-->
                <!--begin: Datatable-->
                <table class="table table-bordered table-hover table-checkable" id="user-table">
                    <thead>
                    <tr>
                        <th>Email</th>
                        <th>User Info</th>
                        <th>Package Info</th>
                        <th>Status</th>
                        <th>Billing</th>
                        <th>Created At</th>
                        <th>Action</th>

                    </tr>
                    </thead>

                </table>
                <!--end: Datatable-->
            </div>
        </div>
        <!--end::Entry-->
    </div>



@endsection

@push('scripts')

    <script src="{{ asset('js/admin-user.js') }}"></script>

@endpush
