<head><base href="../../">
    <meta charset="utf-8" />
    <title>{{ env('SITE_NAME') }} || Admin</title>
    <meta name="keyword" content="employee,reward" />
    <meta name="description" content="Employee Reward system" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta property="csrf-token" content="{{ csrf_token() }}"/>
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <!--end::Fonts-->z
    <!--begin::Page Vendors Styles(used by this page)-->
    <!--begin::Layout Themes-->
    <link href="{{ asset('css/all.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/custom-admin.css') }}" rel="stylesheet" type="text/css" />
    <!--end::Layout Themes-->
    <link rel="shortcut icon" href="{{ env('LOGO_FAV') }}" />
    @stack('styles')
    {{--####  PRE-LOADER  ###--}}
    <div id="preloader">
        <div class="loading-wrapper">
            <div class="loading">
                <img class="logo-loading" src="{{ env('LOGO_FAV') }}" alt="logo">
                <span class="preloader-text">Loading...</span>
            </div>
        </div>
    </div>


</head>
