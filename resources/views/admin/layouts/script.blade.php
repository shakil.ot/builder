
<script>var HOST_URL = "https://keenthemes.com/metronic/tools/preview";</script>
<!--begin::Global Config(global config for global JS scripts)-->
<script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1200 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#3699FF", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#8950FC", "warning": "#FFA800", "danger": "#F64E60", "light": "#F3F6F9", "dark": "#212121" }, "light": { "white": "#ffffff", "primary": "#E1F0FF", "secondary": "#ECF0F3", "success": "#C9F7F5", "info": "#EEE5FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#212121", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#ECF0F3", "gray-300": "#E5EAEE", "gray-400": "#D6D6E0", "gray-500": "#B5B5C3", "gray-600": "#80808F", "gray-700": "#464E5F", "gray-800": "#1B283F", "gray-900": "#212121" } }, "font-family": "Poppins" };</script>
<!--end::Global Config-->
<!--begin::Global Theme Bundle(used by all pages)-->
<script src="{{ asset('js/scripts.js') }}"></script>
<!--end::Global Theme Bundle(used by all pages)-->

<script>
    document.getElementById('preloader').style.display = 'block';

    setTimeout(function () {
        $("#preloader").hide();
    }, 1000);

    $(document).ready(function () {
        $(window).on('load', function () {
            $('#preloader').hide();
        });
        $('#preloader').hide();
    });


    var Inputmask = {
        init: function () {
            $(".phone_usa").inputmask("mask", {mask: "(999) 999-9999"});
        }
    };


    var callBackError = function (response) {
        console.log("Call back calling error...");
        console.log(response);
    };

    function globalAjax(data) {
        if (typeof data.url == "undefined") {
            console.log("url undefined");
            return false;
        }
        if (typeof data.type == "undefined") {
            data.type = "post";
            console.log("data type " + data.type);
        }

        if (typeof data.data == "undefined") {
            console.log("data type " + data.type);
        }

        if (typeof data.callBackError == "undefined") {
            data.callBackError = callBackError;
        }


        $.ajax({
            url: data.url,
            type: data.type,
            data: data.data,
            success: function (response) {
                if (typeof data.data != "undefined") {
                    data.callback(response);
                }
            },
            error: function (result) {
                data.callBackError(result);
            }
        });
    }



    var bootstrapNotify = function() {

        var notifyFunctionObj = function (content) {

        var notifyFunctionObj = $.notify(content, {
            type: content.type,
            allow_dismiss: true,
            newest_on_top: true,
            mouse_over:  true,
            showProgressbar:  true,
            spacing: 10,
            timer: 2000,
            placement: {
                from: 'bottom',
                align: 'right'
            },
            offset: {
                x: 30,
                y: 30
            },
            delay: 1000,
            z_index: 10000,
            animate: {
                enter: 'animate__animated animate__bounce',
                exit: 'animate__animated animate__bounce'
            }
        });

        };


        return {
            success: function (message, title, icon ) {
                var content = {};
                content.message = message;
                content.title = title;
                content.type = "success";
                content.icon = "flaticon-signs" ;
                content.target = '';
                notifyFunctionObj(content);
            },
            error: function (message, title, icon) {
                var content = {};
                content.message = message;
                content.title = title;
                content.type = "danger";
                content.icon = "la la-warning" ;
                content.target = '';
                notifyFunctionObj(content);
            }
        };
    }();
</script>

@stack('scripts')
