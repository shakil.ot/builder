<style>
    .ot_admin_dashboard_wrapper {
        display: flex;
        justify-content: center;
        align-items: center;
        height: calc(100vh - 180px);
        background-color: #f4f5f8;
        color: #222;
        font-weight: 400;
    }

    .ot_admin_dashboard {
        text-align: center;
    }

    .ot_admin_dashboard_wrapper h1 {
        font-size: 26px;
        line-height: 32px;
        font-weight: 500;
    }

    p {
        margin-top: 0;
        margin-bottom: 1rem;
    }

    .ot_admin_btns .btn.btn-first {
        background: #5a82c2;
        border-color: #5a82c2;
        color: #fff;
    }

    .ot_admin_btns .btn.btn-second {
        background: #ffcb30;
        border-color: #ffcb30;
        color: #fff;
    }

</style>
@extends('admin.layouts.app')

@section('title', 'Dashboard')

@section('container')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Dashboard-->
                <!--begin::Row-->
                <div class="row">
                    <div class="col-xl-3">
                        <!--begin::Stats Widget 30-->
                        <div class="card card-custom bg-green card-stretch gutter-b">
                            <!--begin::Body-->
                            <div class="card-body">
                                <span class="svg-icon svg-icon-2x svg-icon-white">
                                        <svg xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                             viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                <path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z"
                                                      fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                                <path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z"
                                                      fill="#000000" fill-rule="nonzero"></path>
                                            </g>
                                        </svg>
                                    <!--end::Svg Icon-->
                                </span>
                                <a href="{{route('admin.manage-user')}}">
                                    <span class="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-6 d-block">{{$totalUsers}}</span>
                                    <span class="font-weight-bold text-white font-size-sm">Active Users</span>
                                </a>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::Stats Widget 30-->
                    </div>
                    <div class="col-xl-3">
                        <!--begin::Stats Widget 31-->
                        <div class="card card-custom bg-green card-stretch gutter-b">
                            <!--begin::Body-->
                            <div class="card-body">
                                <span class="svg-icon svg-icon-2x svg-icon-white">
                                    <!--begin::Svg Icon | path:assets/media/svg/icons/Media/Equalizer.svg-->
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                         width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24"></rect>
                                            <rect fill="#000000" opacity="0.3" x="13" y="4" width="3" height="16"
                                                  rx="1.5"></rect>
                                            <rect fill="#000000" x="8" y="9" width="3" height="11" rx="1.5"></rect>
                                            <rect fill="#000000" x="18" y="11" width="3" height="9" rx="1.5"></rect>
                                            <rect fill="#000000" x="3" y="13" width="3" height="7" rx="1.5"></rect>
                                        </g>
                                    </svg>
                                    <!--end::Svg Icon-->
                                </span>
                                <a href="javascript:void(0)">
                                    <span class="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-6 d-block">5</span>
                                    <span class="font-weight-bold text-white font-size-sm">Funnels</span>
                                </a>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::Stats Widget 31-->
                    </div>
                    <div class="col-xl-3">
                        <!--begin::Stats Widget 32-->
                        <div class="card card-custom bg-green card-stretch gutter-b">
                            <!--begin::Body-->
                            <div class="card-body">
                                <span class="svg-icon svg-icon-2x svg-icon-white">
                                   <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                        width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24"></rect>
                                            <path d="M4,6 L20,6 C20.5522847,6 21,6.44771525 21,7 L21,8 C21,8.55228475 20.5522847,9 20,9 L4,9 C3.44771525,9 3,8.55228475 3,8 L3,7 C3,6.44771525 3.44771525,6 4,6 Z M5,11 L10,11 C10.5522847,11 11,11.4477153 11,12 L11,19 C11,19.5522847 10.5522847,20 10,20 L5,20 C4.44771525,20 4,19.5522847 4,19 L4,12 C4,11.4477153 4.44771525,11 5,11 Z M14,11 L19,11 C19.5522847,11 20,11.4477153 20,12 L20,19 C20,19.5522847 19.5522847,20 19,20 L14,20 C13.4477153,20 13,19.5522847 13,19 L13,12 C13,11.4477153 13.4477153,11 14,11 Z"
                                                  fill="#000000"></path>
                                            <path d="M14.4452998,2.16794971 C14.9048285,1.86159725 15.5256978,1.98577112 15.8320503,2.4452998 C16.1384028,2.90482849 16.0142289,3.52569784 15.5547002,3.83205029 L12,6.20185043 L8.4452998,3.83205029 C7.98577112,3.52569784 7.86159725,2.90482849 8.16794971,2.4452998 C8.47430216,1.98577112 9.09517151,1.86159725 9.5547002,2.16794971 L12,3.79814957 L14.4452998,2.16794971 Z"
                                                  fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                        </g>
                                    </svg>
                                </span>
                                <a href="{{ route('admin.package') }}">
                                    <span class="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-6 text-hover-primary d-block">{{$totalPackages}}</span>
                                    <span class="font-weight-bold text-white font-size-sm">Active Packages</span>
                                </a>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::Stats Widget 32-->
                    </div>
                    <div class="col-xl-3">
                        <!--begin::Stats Widget 32-->
                        <div class="card card-custom bg-green card-stretch gutter-b">
                            <!--begin::Body-->
                            <div class="card-body">
                                <span class="svg-icon svg-icon-2x svg-icon-white">
                                   <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                        width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                         <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24"/>
                                            <path d="M6,2 L18,2 C18.5522847,2 19,2.44771525 19,3 L19,12 C19,12.5522847 18.5522847,13 18,13 L6,13 C5.44771525,13 5,12.5522847 5,12 L5,3 C5,2.44771525 5.44771525,2 6,2 Z M7.5,5 C7.22385763,5 7,5.22385763 7,5.5 C7,5.77614237 7.22385763,6 7.5,6 L13.5,6 C13.7761424,6 14,5.77614237 14,5.5 C14,5.22385763 13.7761424,5 13.5,5 L7.5,5 Z M7.5,7 C7.22385763,7 7,7.22385763 7,7.5 C7,7.77614237 7.22385763,8 7.5,8 L10.5,8 C10.7761424,8 11,7.77614237 11,7.5 C11,7.22385763 10.7761424,7 10.5,7 L7.5,7 Z"
                                                  fill="#000000" opacity="0.3"/>
                                            <path d="M3.79274528,6.57253826 L12,12.5 L20.2072547,6.57253826 C20.4311176,6.4108595 20.7436609,6.46126971 20.9053396,6.68513259 C20.9668779,6.77033951 21,6.87277228 21,6.97787787 L21,17 C21,18.1045695 20.1045695,19 19,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,6.97787787 C3,6.70173549 3.22385763,6.47787787 3.5,6.47787787 C3.60510559,6.47787787 3.70753836,6.51099993 3.79274528,6.57253826 Z"
                                                  fill="#000000"/>
                                        </g>
                                    </svg>
                                </span>
                                <a href="{{ route('admin-email-index') }}">
                                    <span class="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-6 text-hover-primary d-block">{{$totalEmailTemplates}}</span>
                                    <span class="font-weight-bold text-white font-size-sm">Email Templates</span>
                                </a>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::Stats Widget 32-->
                    </div>
                </div>
                <!--end::Row-->
                <!--end::Dashboard-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->
    {{--    <div class="ot_admin_dashboard_wrapper">--}}
    {{--        <div class="ot_admin_dashboard">--}}
    {{--            <h1>Welcome to our Whatsapp Shopping</h1>--}}
    {{--            <p>Manage your application settings, users and other things from admin area</p>--}}
    {{--            <div class="ot_admin_btns">--}}
    {{--                <a href="{{route('admin.manage-user')}}" class="mt-2 btn btn-first">Manage User  <i class="la la-user-plus"></i></a>--}}
    {{--                <a href="{{ route('admin.package') }}" class="mt-2 btn btn-second">Manage package <i class="la la-paper-plane"></i></a>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}
@endsection
