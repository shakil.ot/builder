@extends('user.layout.app', ['menu' => 'app_screen'])

@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-2 mr-5">
                            <span class="svg-icon svg-icon-md svg-icon-green">
                                <!--begin::Svg Icon | path:assets/media/svg/icons/Shopping/Chart-bar1.svg-->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24"/>
                                        <path d="M7.5,4 L7.5,19 L16.5,19 L16.5,4 L7.5,4 Z M7.71428571,2 L16.2857143,2 C17.2324881,2 18,2.8954305 18,4 L18,20 C18,21.1045695 17.2324881,22 16.2857143,22 L7.71428571,22 C6.76751186,22 6,21.1045695 6,20 L6,4 C6,2.8954305 6.76751186,2 7.71428571,2 Z" fill="#000000" fill-rule="nonzero"/>
                                        <polygon fill="#000000" opacity="0.3" points="7.5 4 7.5 19 16.5 19 16.5 4"/>
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>
                            Employee App
                        </h5>
{{--                        <h5 class="text-dark font-weight-bold my-2 mr-5">Mobile App</h5>--}}
                        <!--end::Page Title-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Card-->
                <div class="card card-custom">
                    <form class="form fv-plugins-bootstrap fv-plugins-framework" id="kt_form_2" novalidate="novalidate">
                        <div class="card-body">
                            <div class="alert alert-custom alert-light-success d-none" role="alert" id="kt_form_2_msg">
                                <div class="alert-icon">
                                    <i class="flaticon2-bell-5"></i>
                                </div>
                                <div class="alert-text font-weight-bold">Oh snap! Change a few things up and try
                                    submitting again.
                                </div>
                                <div class="alert-close">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span>
                                            <i class="ki ki-close"></i>
                                        </span>
                                    </button>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-md-4 mb-20">
                                    <img class="d-block w-100 border border-1 border-primary p-2" src="{{ asset('assets/mobileScreen/home-screen.png') }}" alt="First slide">
                                </div>

                                <div class="col-md-4 mb-20">
                                    <img class="d-block w-100 border border-1 border-primary p-2" src="{{ asset('assets/mobileScreen/login-screen.png') }}" alt="First slide">
                                </div>

                                <div class="col-md-4 mb-20">
                                    <img class="d-block w-100 border border-1 border-primary p-2" src="{{ asset('assets/mobileScreen/dashboard-achivements.png') }}" alt="Second slide">
                                </div>

                                <div class="col-md-4 mb-20">
                                    <img class="d-block w-100 border border-1 border-primary p-2" src="{{ asset('assets/mobileScreen/dashboard-training.png') }}" alt="Second slide">
                                </div>

                                <div class="col-md-4 mb-20">
                                    <img class="d-block w-100 border border-1 border-primary p-2" src="{{ asset('assets/mobileScreen/dashboard-loot.png') }}" alt="Third slide">
                                </div>

                                <div class="col-md-4 mb-20">
                                    <img class="d-block w-100 border border-1 border-primary p-2" src="{{ asset('assets/mobileScreen/dashboard-achievements-popup-1.png') }}" alt="Third slide">
                                </div>

                                <div class="col-md-4 mb-20">
                                    <img class="d-block w-100 border border-1 border-primary p-2" src="{{ asset('assets/mobileScreen/dashboard-loot-popup.png') }}" alt="Third slide">
                                </div>

                                <div class="col-md-4 mb-20">
                                    <img class="d-block w-100 border border-1 border-primary p-2" src="{{ asset('assets/mobileScreen/reward.png') }}" alt="Third slide">
                                </div>

                                <div class="col-md-4 mb-20">
                                    <img class="d-block w-100 border border-1 border-primary p-2" src="{{ asset('assets/mobileScreen/hiring-there-attitude-1.png') }}" alt="Third slide">
                                </div>

                                <div class="col-md-4 mb-20">
                                    <img class="d-block w-100 border border-1 border-primary p-2" src="{{ asset('assets/mobileScreen/hiring-there-attitude.png') }}" alt="Third slide">
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
                <!--end::Card-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>

@endsection
