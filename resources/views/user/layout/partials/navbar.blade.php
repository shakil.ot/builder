<!--begin::Header Nav-->
<ul class="menu-nav">
    <li class="menu-item menu-item-submenu {{ $menu == 'dashboard' ? 'menu-item-active' : '' }}">
        <a href="{{ route('dashboard') }}" class="menu-link">
            <span class="menu-text">Dashboard</span>
            <i class="menu-arrow"></i>
        </a>
    </li>

    <li class="menu-item menu-item-submenu
                    {{ $menu == 'contact' ? 'menu-item-active' : '' }}
{{--    {{ $menu == 'tag-contact' ? 'menu-item-open' : '' }}--}}
    {{ $menu == 'imported-files' ? 'menu-item-open' : '' }}
    {{ $menu == 'upload-block-contacts' ? 'menu-item-open' : '' }}
    {{ $menu == 'block-contact-files' ? 'menu-item-open' : '' }}
        "
        aria-haspopup="true" data-menu-toggle="hover">
        <a href="javascript:;" class="menu-link menu-toggle">
            <span class="menu-text"> My Contacts</span>
            <i class="menu-arrow"></i>
        </a>
        <div class="menu-submenu" style="" kt-hidden-height="80">
            <i class="menu-arrow"></i>
            <ul class="menu-subnav">
{{--                <li class="menu-item menu-item-parent" aria-haspopup="true">--}}
{{--                    <span class="menu-link">--}}
{{--                        <span class="menu-text">My Contacts</span>--}}
{{--                    </span>--}}
{{--                </li>--}}
                <li class="menu-item {{ $menu == 'contact' ? 'menu-item-active' : '' }}" aria-haspopup="true">
                    <a href="{{ route('contact-index') }}" class="menu-link ">
                        <i class="menu-bullet menu-bullet-dot">
                            <span></span>
                        </i>
                        <span class="menu-text">All Contacts</span>
                    </a>
                </li>
                <li class="menu-item {{ $menu == 'imported-files' ? 'menu-item-active' : '' }}" aria-haspopup="true">
                    <a href="{{ route('imported-files') }}" class="menu-link">
                        <i class="menu-bullet menu-bullet-dot">
                            <span></span>
                        </i>
                        <span class="menu-text">Imported Files</span>
                    </a>
                </li>
            </ul>
        </div>
    </li>

    <li class="menu-item menu-item-submenu {{ $menu == 'broadcasts' ? 'menu-item-active' : '' }}">
        <a href="{{ route('broadcast.index') }}" class="menu-link">
            <span class="menu-text">Campaigns</span>
            <i class="menu-arrow"></i>
        </a>
    </li>

    <li class="menu-item menu-item-submenu {{ (request()->segment(1) == 'ebook') ? 'menu-item-active' : '' }}">
        <a href="{{ route('ebook.index') }}" class="menu-link">
            <span class="menu-text">Ebook</span>
            <i class="menu-arrow"></i>
        </a>
    </li>

    <li class="menu-item menu-item-submenu {{ $menu == 'email-setting' ? 'menu-item-active' : '' }}">
        <a href="{{ route('email-setting') }}" class="menu-link">
            <span class="menu-text">Email Setting</span>
            <i class="menu-arrow"></i>
        </a>
    </li>


    <li class="menu-item menu-item-submenu
                    {{ $menu == 'report' ? 'menu-item-active' : '' }}
                    {{ $menu == 'autofollowup-schedule-report' ? 'menu-item-active' : '' }}
                    {{ $menu == 'autofollowup-report' ? 'menu-item-active' : '' }}
                    {{ $menu == 'broadcast-report' ? 'menu-item-active' : '' }}
            "
        aria-haspopup="true" data-menu-toggle="hover">
        <a href="javascript:;" class="menu-link menu-toggle">
            <span class="menu-text"> Reports</span>
            <i class="menu-arrow"></i>
        </a>
        <div class="menu-submenu" style="" kt-hidden-height="80">
            <i class="menu-arrow"></i>
            <ul class="menu-subnav">
                <li class="menu-item menu-item-submenu {{ $menu == 'autofollowup-schedule-report' ? 'menu-item-active' : '' }}">
                    <a href="{{ route('broadcast.report-autoFollowupQue') }}" class="menu-link">
                        <i class="menu-bullet menu-bullet-dot">
                            <span></span>
                        </i>
                        <span class="menu-text">Auto Followup Schedule Message Report</span>
                    </a>
                </li>
                <li class="menu-item menu-item-submenu {{ $menu == 'autofollowup-report' ? 'menu-item-active' : '' }}">
                    <a href="{{ route('broadcast.report') }}" class="menu-link">
                        <i class="menu-bullet menu-bullet-dot">
                            <span></span>
                        </i>
                        <span class="menu-text">Auto-followup Report</span>
                    </a>
                </li>
                <li class="menu-item menu-item-submenu {{ $menu == 'broadcast-report' ? 'menu-item-active' : '' }}">
                    <a href="{{ route('broadcast.report-broadcast') }}" class="menu-link">
                        <i class="menu-bullet menu-bullet-dot">
                            <span></span>
                        </i>
                        <span class="menu-text">Broadcast Report</span>
                    </a>
                </li>
            </ul>
        </div>
    </li>

</ul>
<!--end::Header Nav-->
