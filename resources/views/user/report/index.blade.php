@extends('user.layout.app')

@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-2 mr-5">Reports</h5>
                        <!--end::Page Title-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Card-->
                <div class="card card-custom">
                    <div class="card-body">
                        <!--begin: Search Form-->
                        <!--begin::Search Form-->
                        <div class="mb-7">
                            <div class="row align-items-center mb-4">
                                <div class="col-lg-9 col-xl-8">
                                    <div class="row align-items-center">
                                        <div class="col-md-4 my-2 my-md-0">
                                            <div class="d-flex align-items-center">
                                                <label class="mr-3 mb-0">Type</label>
                                                <select class="form-control" id="kt_datatable_search_type">
                                                    <option value="">Choose Report</option>
                                                    <option value="1">Achievement Claimed</option>
                                                    <option value="2">Activity Log</option>
                                                    <option value="3">Reward Claimed</option>
                                                    <option value="3">Training Claimed</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row align-items-center">
                                <div class="col-lg-9 col-xl-8">
                                    <div class="row align-items-center">
                                        <div class="col-md-6 my-2 my-md-0">
                                            <div class="d-flex align-items-center">
                                                <label class="mr-3 mb-0">Start Date</label>
                                                <input type="text" class="form-control kt_datetimepicker_5" name="start_date" placeholder="Select start date">
                                            </div>
                                        </div>
                                        <div class="col-md-6 my-2 my-md-0">
                                            <div class="d-flex align-items-center">
                                                <label class="mr-3 mb-0">End Date</label>
                                                <input type="text" class="form-control kt_datetimepicker_5" name="end_date" placeholder="Select end date">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                                    <a href="#" class="btn btn-light-green px-6 font-weight-bold">Search</a>
                                    <a href="#" class="btn btn-green px-6 font-weight-bold float-right">Export</a>
                                </div>
                            </div>
                        </div>
                        <!--end::Search Form-->
                        <!--end: Search Form-->
                        <!--begin: Datatable-->
                        <div class="datatable datatable-bordered datatable-head-custom" id="kt_datatable"></div>
                        <!--end: Datatable-->
                    </div>
                </div>
                <!--end::Card-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>

@endsection

@section('script')
    <script src="{{ asset('assets/theme/js/pages/custom/user/list-datatable.js?v=7.0.4') }}"></script>

    <script type="text/javascript">
        // Class definition
        var script = function () {

            // Private functions
            var scriptInit = function () {

                $('.kt_datetimepicker_5').datepicker({
                    format: "dd MM yyyy",
                    showMeridian: true,
                    todayHighlight: true,
                    autoclose: true,
                    pickerPosition: 'bottom-left'
                });
            }

            return {
                // public functions
                init: function() {
                    scriptInit();
                }
            };
        }();

        jQuery(document).ready(function() {
            script.init();
        });
    </script>
@endsection

