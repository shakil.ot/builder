<!DOCTYPE html>
<html lang="en">
<!--begin::Head-->
<head>
    <base href="../../../../">
    <meta charset="utf-8"/>
    <title>{{ env('SITE_NAME') }}</title>
    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta property="csrf-token" content="{{ csrf_token() }}"/>
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700"/>
    <!--end::Fonts-->
    <!--begin::Page Custom Styles(used by this page)-->
    <link href="{{asset('assets/theme/css/pages/login/login-6.css?v=7.0.4')}}" rel="stylesheet" type="text/css" />
    <!--end::Page Custom Styles-->
    <!--begin::Global Theme Styles(used by all pages)-->
    <link href="{{ asset('css/all.css?v=7.0.4') }}" rel="stylesheet" type="text/css"/>
    <!--end::Layout Themes-->
    <link rel="shortcut icon" href="{{ env('LOGO_FAV') }}"/>

    <link href="{{ asset('assets/preloader/preloader.css') }}" rel="stylesheet">
</head>
<!--end::Head-->
<!--begin::Body-->
<body id="kt_body" class="header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading">
<!--begin::Preloader-->
<div id="preloader">
    <div class="loading-wrapper">
        <div class="loading">
            <img class="logo-loading" src="{{ asset(env('LOGO_MAIN')) }}" alt="logo">
            <span class="preloader-text">Loading...</span>
        </div>
    </div>
</div>
<!--begin::Preloader-->
<!--begin::Main-->
<div class="d-flex flex-column flex-root">
    <!--begin::Login-->
    <div class="login login-6 login-signin-on login-signin-on d-flex flex-row-fluid" id="kt_login">
        <div class="d-flex flex-column flex-lg-row flex-row-fluid text-center"
             style="{{ asset('background-image: url(assets/theme/media/bg/bg-3.jpg)') }}">
            <!--begin:Aside-->
            <div class="d-flex w-100 flex-center p-15">
                <div class="login-wrapper">
                    <!--begin:Aside Content-->
                    <div class="text-dark-75">
                        <a href="#">
                            <img src="{{ env('LOGO_BIG') }}" class="max-h-75px" alt=""/>
                        </a>
                        <h3 class="mb-8 mt-22 font-weight-bold welcome"></h3>
                        <p class="mb-15 text-muted font-weight-bold message"></p>
                    </div>
                    <!--end:Aside Content-->
                </div>
            </div>
            <!--end:Aside-->
            <!--begin:Divider-->
            <div class="login-divider">
                <div></div>
            </div>
            <!--end:Divider-->
            <!--begin:Content-->
            <div class="d-flex w-100 flex-center p-15 position-relative overflow-hidden">
                <div class="login-wrapper">
                    <!--begin:Sign In Form-->
                    <div class="login-signin">
                        <div class="text-center mb-10 mb-lg-20">
                            <h2 class="font-weight-bold">Reset Password</h2>
                            <p class="text-muted font-weight-bold">Enter below details to change password</p>
                        </div>
                        <form class="form text-left" id="reset-password">
                            @csrf
                            <input type="hidden" name="email" value="{{$email}}">
                            <div class="form-group py-2 m-0">
                                <input class="form-control h-auto border-0 px-2 placeholder-dark-75" type="text"
                                       placeholder="Enter Verification Code" name="code" autocomplete="off"/>
                            </div>
                            <div class="form-group py-2 border-top m-0">
                                <input class="form-control h-auto border-0 px-2 placeholder-dark-75" type="Password"
                                       placeholder="Password" name="password"/>
                            </div>
                            <div class="form-group py-2 border-top m-0">
                                <input class="form-control h-auto border-0 px-2 placeholder-dark-75" type="Password"
                                       placeholder="Enter Password Again" name="password_confirmation"/>
                            </div>
                            <div class="form-group d-flex flex-wrap justify-content-between align-items-center mt-5">
{{--                                <label class="checkbox m-0 text-muted font-weight-bold">--}}
{{--                                    <input type="checkbox" name="remember"/>Remember me--}}
{{--                                    <span></span></label>--}}
                                <a href="{{route('login')}}" id="kt_login_forgot"
                                   class="text-muted text-hover-primary font-weight-bold ml-auto">Login</a>
                            </div>
                            <div class="text-center mt-15">
                                <button id="emr_login_signin_submit"
                                        class="btn btn-primary btn-pill shadow-sm py-4 px-9 font-weight-bold">Change Password
                                </button>

                                <hr>

                                <span class="text-muted font-weight-bold font-size-h4">New Here?
									<a href="{{ route('user.registration.index') }}"
                                       id=""
                                       class="font-weight-bolder register-btn"
                                       style="color: #5bbd42;"
                                    >Create an Account</a></span>

{{--                                <a href="" class="btn btn-outline-primary btn-pill py-4 px-9 font-weight-bold ">Get An Account</a>--}}


                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--end:Content-->
        </div>
    </div>
    <!--end::Login-->
</div>


<script src="{{ asset('js/scripts.js') }}"></script>
<script>
    var KTAppSettings = {"breakpoints": {"sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1200},
        "colors": {
            "theme": {
                "base": {
                    "white": "#ffffff",
                    "primary": "#3699FF",
                    "secondary": "#E5EAEE",
                    "success": "#1BC5BD",
                    "info": "#8950FC",
                    "warning": "#FFA800",
                    "danger": "#F64E60",
                    "light": "#F3F6F9",
                    "dark": "#212121"
                },
                "light": {
                    "white": "#ffffff",
                    "primary": "#E1F0FF",
                    "secondary": "#ECF0F3",
                    "success": "#C9F7F5",
                    "info": "#EEE5FF",
                    "warning": "#FFF4DE",
                    "danger": "#FFE2E5",
                    "light": "#F3F6F9",
                    "dark": "#D6D6E0"
                },
                "inverse": {
                    "white": "#ffffff",
                    "primary": "#ffffff",
                    "secondary": "#212121",
                    "success": "#ffffff",
                    "info": "#ffffff",
                    "warning": "#ffffff",
                    "danger": "#ffffff",
                    "light": "#464E5F",
                    "dark": "#ffffff"
                }
            },
            "gray": {
                "gray-100": "#F3F6F9",
                "gray-200": "#ECF0F3",
                "gray-300": "#E5EAEE",
                "gray-400": "#D6D6E0",
                "gray-500": "#B5B5C3",
                "gray-600": "#80808F",
                "gray-700": "#464E5F",
                "gray-800": "#1B283F",
                "gray-900": "#212121"
            }
        },
        "font-family": "Poppins"
    };
</script>
@routes
<script src="{{ asset('assets/theme/js/pages/custom/login/login-user-general.js?v=7.0.4') }}"></script>
<script src="{{ asset('assets/preloader/preloader.js') }}"></script>
<script src="{{ mix('js/password-reset.js') }}"></script>

</body>
<!--end::Body-->
</html>
