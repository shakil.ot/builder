@component('mail::message')
# Hello, {{ $userName }}

<h4 style="text-align: center; color: red" >Your account has been created</h4>

Please Login with these credentials, <br>
<strong>Email:</strong> {{ $email }}<br>
<strong>Password:</strong> {{ $password }}

@component('mail::button', ['url' => $url])
Login Here
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
