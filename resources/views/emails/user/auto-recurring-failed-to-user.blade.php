@component('mail::message')
@if(isset($body))
    {!! $body !!}
@else
# Hello, {{ $userName }}

Auto recharge has been failed. Please renew your package.
<strong>Failed Reason:</strong> {{ $error }} <br>
<strong>PackageName:</strong> {{ $packageName }} <br>
<strong>Subscription Fee:</strong> ${{ $subscriptionFee }} <br>
<strong>Card No:</strong> {{ $cardNumber }} <br>

Thanks,<br>
{{ config('app.name') }}
@endif
@endcomponent
