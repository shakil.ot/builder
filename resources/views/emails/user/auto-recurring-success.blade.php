@component('mail::message')
@if(isset($body))
    {!! $body !!}
@else
# Hello, {{ $userName }}

Congratulations! Your package has been successfully upgraded. <br>
<strong>Your Current Package Name:</strong> {{ $packageName }} <br>
<strong>Charged Amount:</strong> {{ $subscriptionFee }} <br>
<strong>Charged Card:</strong> **** **** **** {{ $cardNumber }} <br>

Thanks,<br>
{{ config('app.name') }}
@endif
@endcomponent
