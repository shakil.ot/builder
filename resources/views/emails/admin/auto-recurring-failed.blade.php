@component('mail::message')
# Package Recurring Failed!

<strong>User ID:</strong> {{ $userId }} <br>
<strong>PackageName:</strong> {{ $packageName }} <br>
<strong>Subscription Fee:</strong> ${{ $subscriptionFee }} <br>
<strong>Message:</strong> {!! $message !!} <br>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
