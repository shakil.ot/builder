@component('mail::message')
# Free package auto recurring happened

<strong>User Name:</strong> {{ $userName }} <br>
<strong>User Email:</strong> {{ $email }} <br>
<strong>Package Name:</strong> {{ $packageName }} <br>
<strong>Next Recurring Date:</strong> {{ $nextRecurringDate }} <br>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
