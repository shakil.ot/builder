var AdminUser = (function () {

    $(function () {

        // Add overflow_visible to prevent hidden action option
        $('.visible.m-portlet__body.extra.p-0').parents('.m-grid__item.m-grid__item--fluid.m-wrapper').addClass('overflow_visible');

        var user_operation_callback = function (response) {
            Swal.fire(response.status, response.html, response.status);
            $('#user-table').DataTable().ajax.reload();
        }


        const _token = document.head.querySelector("[property=csrf-token]").content;


        (function (global, $, _token) {


            $("#search-user-status").val('').selectpicker('refresh');

            var parent_email = null;
            var user_type = null;
            var created_at_from = null;
            var created_at_to = null;
            initializeCreatedAtDateRangePicker();


            // #### DATATABLE FOR USER-LIST. ALL USER LIST FITCH FROM DATABASED
            var dataTable = $('#user-table').DataTable({
                responsive: true,
                // Pagination settings
                // read more: https://datatables.net/examples/basic_init/dom.html
                lengthMenu: [5, 10, 25, 50, 100, 500],
                pageLength: 10,
                language: {
                    'lengthMenu': 'Display _MENU_',
                },
                searchDelay: 500,
                processing: true,
                serverSide: true,
                ajax: {
                    url: route('admin.user-list') ,
                    data: function (data) {
                        data.created_at_from_search = created_at_from;
                        data.created_at_to_search = created_at_to;
                    },
                    type: 'get',
                },
                columns: [
                    {data: 'email', name: 'email', "searchable": true, "visible": false},
                    {data: 'user_info', name: 'user_info', "orderable": true, "searchable": true, width: "35%"},
                    {data: 'package_info', name: 'package_info', "orderable": true, "searchable": true, width: "35%"},
                    {data: 'my_status', name: 'status', "orderable": true, "searchable": true, width: "5%"},
                    {data: 'billing_status', name: 'billing_status', "orderable": true, "searchable": true, width: "5%"},
                    {data: 'created_at', name: 'created_at', "orderable": true, "searchable": false, width: "10%"},
                    {data: 'action', name: 'action', "orderable": false, searchable: false, width: "10%"}
                ],
                dom: '<"top-toolbar row"<"top-left-toolbar col-md-9"lB><"top-right-toolbar col-md-3"f>>rt<"bottom-toolbar"<"bottom-left-toolbar"i><"bottom-right-toolbar"p>>',
                buttons: [
                    {
                        extend: 'colvis',
                        text: '<i class="fa \tfa-list-ul pr-1"></i>Columns',
                        attr: {
                            id: "custom-colvis-button",
                            class: 'btn btn-outline-primary btn-sm m-btn',
                            style: '',
                            title: 'Columns Visibility'
                        },
                        columns: ':not(.noVis)',
                        columnText: function (dt, idx, title) {
                            // return (idx+1)+': '+title;
                            return title;
                        },
                        postfixButtons: ['colvisRestore']
                    },
                    {
                        extend: 'colvisGroup',
                        text: '<i class="fa fa-refresh pr-1"></i>Refresh',
                        attr: {
                            class: 'btn btn-outline-primary btn-sm m-btn  m-btn--icon',
                            id: 'show-all-button'
                        },
                        show: ':hidden'
                    },
                    {
                        text: '<i class="fa fa-filter pr-1"></i>Filter',
                        attr: {
                            class: 'btn btn-outline-primary btn-sm m-btn  m-btn--icon',
                            id: 'show-all-button'
                        },
                        action: function (e, dt, node, config) {
                            $('.filter-options').slideToggle();
                        }
                    },
                    {
                        extend: 'collection',
                        text: 'Export To',
                        autoClose: true,
                        attr: {
                            id: "export_button",
                            class: 'btn btn-outline-primary btn-sm m-btn dropdown-toggle',
                            style: '',
                            title: 'Export'
                        },
                        buttons: [
                            {
                                extend: 'csv',
                                title: "User List " + (new Date()).toDateString(),

                                exportOptions: {
                                    columns: [1, 2, 3],
                                    modifier: {selected: null}
                                }
                            }, {
                                extend: 'excel',
                                title: "User List " + (new Date()).toDateString(),

                                exportOptions: {
                                    columns: [1, 2, 3],
                                    modifier: {selected: null}
                                }
                            }, {
                                extend: 'pdf',
                                title: "User List " + (new Date()).toDateString(),

                                exportOptions: {
                                    columns: [1, 2, 3],
                                    modifier: {selected: null}
                                }
                            }, {
                                extend: 'print',
                                title: "User List " + (new Date()).toDateString(),

                                exportOptions: {
                                    columns: [1, 2, 3],
                                    modifier: {selected: null}
                                }
                            }, {
                                extend: 'copy',
                                title: "User List " + (new Date()).toDateString(),

                                exportOptions: {
                                    columns: [1, 2, 3],
                                    modifier: {selected: null}
                                }
                            }
                        ],
                        fade: true
                    },
                ]
            });

            //Columns visibility buttons' css
            $("#custom-colvis-button").click(function () {
                $(".buttons-columnVisibility").removeClass("dt-button").addClass("btn btn-outline-primary btn-sm m-btn btn-block");
                $(".buttons-colvisRestore").removeClass("dt-button").addClass("btn btn-outline-brand btn-sm m-btn btn-block");
            });

            function initializeCreatedAtDateRangePicker() {
                let start = moment().subtract(1, 'months');
                let end = moment();

                function cb(start, end) {
                    $('#created_at_range span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }

                $('#created_at_range').daterangepicker({
                    startDate: start,
                    endDate: end,
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    }
                }, function (starting, ending) {
                    let startDate = moment(starting);
                    let endDate = moment(ending);
                    start = startDate;
                    end = endDate;
                    cb(start, end);
                    created_at_from = moment(start, 'MMMM D, YYYY').format("YYYY-MM-DD");
                    created_at_to = moment(end, 'MMMM D, YYYY').format("YYYY-MM-DD");
                    dataTable.ajax.reload();
                });
                $('#created_at_range span').html('Select Created Date');
            }


            $("#show-all-button").click(function () {
                refreshTable();
            });

            /* search starts */
            $("#search-user-email").keyup(function () {
                dataTable.column($(this).attr('data-column'))
                    .search($(this).val())
                    .draw();
            });

            $("#search-parent-email").keyup(function () {
                parent_email = $(this).val();
                dataTable.ajax.reload();
            });

            $("#search-user-status").change(function () {
                dataTable.column($(this).attr('data-column'))
                    .search($(this).val())
                    .draw();
            });
            /*Search ends */

            /* reset search starts*/
            $('.reset-user-email-search').on('click', function () {
                $("#search-user-email").val('');
                dataTable.column($(this).attr('data-column'))
                    .search('')
                    .draw();
            });

            $('.reset-parent-email-search').on('click', function () {
                $("#search-parent-email").val('');
                parent_email = null;
                dataTable.ajax.reload();
            });

            $('.reset-user-status-search').on('click', function () {
                $("#search-user-status").val('').selectpicker('refresh');
                dataTable.column($(this).attr('data-column'))
                    .search('')
                    .draw();
            });
            $('.reset-created_at_range').on('click', function () {
                $('#created_at_range span').html('Select Created Date');
                created_at_from = null;
                created_at_to = null;
                dataTable.ajax.reload();
            });

            /* reset search starts*/
            function refreshTable(params) {
                $("#search-user-email").val('');
                $("#search-parent-email").val('');
                parent_email = null;
                $("#search-user-status").val('').selectpicker('refresh');
                $('#created_at_range span').html('Select Created Date');
                created_at_from = null;
                created_at_to = null;
                dataTable.columns().search('');
                dataTable.search('');
                dataTable.ajax.reload();
            }



            /****### -ADD-BUTTON FUNCTION START- ###***/
            /****###  ADD NEW USER FUNCTION. WHEN CLICK .add-new-item IT WILL OPEN A MODAL WITH A FORM  ###***/
            var addButton = $(".add-new-item");
            addButton.on('click', function () {
                $('#preloader').show();

                var detailPopUp = global.customPopup.show({
                    header: 'Add User',
                    message: '',
                    dialogSize: 'lg',
                    dialogClass: 'prevent-to-show'
                });

                detailPopUp.on('shown.bs.modal', function () {
                    var modal = $(this);

                    global.mySiteAjax({
                        url: route('admin.user-add-form'),
                        type: 'get',
                        loadSpinner: true,
                        success: function (response) {
                            $('#preloader').hide();
                            window.nbUtility.ajaxErrorHandling(response, function () {
                                modal.find('.modal-body').html(response.html);
                                modal.removeClass('prevent-to-show');
                                Inputmask.init();

                                $('#user-from').parsley().on('field:validated', function () {
                                    var ok = $('.parsley-error').length === 0;
                                    $('.bs-callout-info').toggleClass('hidden', !ok);
                                    $('.bs-callout-warning').toggleClass('hidden', ok);
                                }).on('form:submit', function (formInstance) {
                                    $('#preloader').show();

                                    var values = $("#user-from").serializeArray();

                                    $.ajax({
                                        url: route('admin.user-add-submit'),
                                        type: 'post',
                                        data: values,  // $('#user-from').serialize(),
                                        loadSpinner: true,
                                        success: function (data) {
                                            $('#preloader').hide();
                                            if (data.status == 'success') {
                                                $("#user-table").DataTable().ajax.reload();
                                                modal.modal('hide');
                                                //toastr.success(data.html, '', {"closeButton": true})
                                                bootstrapNotify.success(data.html, 'Success');
                                                dataTable.ajax.reload();
                                            } else {
                                                // toastr.error(data.html, '', {"closeButton": true})
                                                bootstrapNotify.error(data.html, 'error');
                                            }
                                        },
                                        //error messages
                                        error: function (data) {
                                            $('#preloader').hide();
                                            if (data.status == 422) {
                                                global.generateErrorMessage(formInstance, data.responseJSON.errors);
                                                if (data.responseJSON.errors.first_name !== undefined)
                                                    bootstrapNotify.error(data.responseJSON.errors.first_name, 'error');
                                                if (data.responseJSON.errors.last_name !== undefined)
                                                    bootstrapNotify.error(data.responseJSON.errors.last_name, 'error');
                                                if (data.responseJSON.errors.email !== undefined)
                                                    bootstrapNotify.error(data.responseJSON.errors.email, 'error');
                                                if (data.responseJSON.errors.phone !== undefined)
                                                    bootstrapNotify.error(data.responseJSON.errors.phone, 'error');
                                            }
                                        }

                                    });

                                });
                            });
                        }
                    });
                });

                detailPopUp.on('hidden.bs.modal', function () {
                    $(this).remove();
                });
            });
            /****### -END- ###***/


            var userId = "";

            $(document).on('click','.action', function (e) {
                userId = $(this).attr('data-id');
                var settingsType = $(this).attr('data-value');


                if (!settingsType)
                    return false;

                switch (settingsType) {

                    case "edit-user":
                        editUser(userId);
                        break;

                    case "allow-permission":
                        allowPermission(userId);
                        break;

                    case "deactivate-user":
                        deactivateUser(userId);
                        break;

                    case "active-user":
                        activeUser(userId);
                        break;

                    case "stop-auto-recurring":
                        stopAutoRecurring(userId);
                        break;

                    case "start-auto-recurring":
                        startAutoRecurring(userId);
                        break;

                    case "login-as-user":
                        loginAsUser(userId);
                        break;

                    case "view-package-info":
                        viewPackageInfo(userId);
                        break;

                    case "user-details":
                        userDetails(userId);
                        break;
                    case "package-allocate":
                        packageAllocate(userId);
                        break;

                    case "assign-package":
                        assignPackage(userId);
                        break;

                    case "delete-user":
                        deleteUser(userId);
                        break;

                    default :
                        toastr.error("Invalid Setting type", "Server Notification");
                        break;

                }

                /****### -PACKAGE-ALLOCATE FUNCTION START - ###***/
                /****### allocated a package of a user   ###***/
                // function packageAllocate(userId) {
                //
                //     var packagePopUp = global.customPopup.show({
                //         header: 'Package Allocate',
                //         message: '',
                //         dialogSize: 'medium',
                //         dialogClass: 'prevent-to-show'
                //     });
                //
                //     packagePopUp.on('shown.bs.modal', function () {
                //         var modal = $(this);
                //
                //         global.mySiteAjax({
                //             url: route('package-allocate-view'),
                //             type: 'post',
                //             data: {user_id: userId, _token: _token,},
                //             loadSpinner: true,
                //             success: function (response) {
                //                 window.nbUtility.ajaxErrorHandling(response, function () {
                //                     modal.find('.modal-body').html(response.html);
                //                     modal.removeClass('prevent-to-show');
                //
                //                 });
                //             }
                //         });
                //     });
                //
                //     packagePopUp.on('hidden.bs.modal', function () {
                //         $(this).remove();
                //     });
                // }
                /****### -PACKAGE-ALLOCATE FUNCTION END- ###***/



                /****### -EDIT USER FUNCTION START - ###***/
                /****### edit an user information using this function   ###***/
                function editUser(userId) {
                    $('#preloader').show();
                    var editPopUp = global.customPopup.show({
                        header: 'Edit User',
                        message: '',
                        dialogSize: 'lg',
                        dialogClass: 'prevent-to-show'
                    });

                    editPopUp.on('shown.bs.modal', function () {
                        var modal = $(this);

                        global.mySiteAjax({
                            url: route('admin.user-edit-form'),
                            type: 'post',
                            data: {user_id: userId, _token: _token,},
                            loadSpinner: true,
                            success: function (response) {
                                $('#preloader').hide();
                                window.nbUtility.ajaxErrorHandling(response, function () {
                                    modal.find('.modal-body').html(response.html);
                                    modal.removeClass('prevent-to-show');
                                    Inputmask.init();

                                    modal.find('.user-type-select-option').prop('disabled', true);

                                    $('#user-from').parsley().on('field:validated', function () {
                                        var ok = $('.parsley-error').length === 0;
                                        $('.bs-callout-info').toggleClass('hidden', !ok);
                                        $('.bs-callout-warning').toggleClass('hidden', ok);
                                    }).on('form:submit', function (formInstance) {
                                        $('.error-message').html('');
                                        $('#preloader').show();

                                        var values = $("#user-from").serializeArray();
                                        var phone = $('#phone').val().replace('(', '').replace(')', '').replace('-', '').replace(' ', '');

                                        if (phone.length == '10' || phone.length == 10) {
                                            phone = 1 + '' + phone;
                                        }

                                        values.find(input => input.name == 'phone').value = phone;
                                        $.ajax({
                                            url: route('admin.user-edit-submit'),
                                            type: 'post',
                                            data: values,
                                            loadSpinner: true,
                                            success: function (data) {
                                                $('#preloader').hide();
                                                if (data.status == 'success') {
                                                    $("#user-table").DataTable().ajax.reload();
                                                    modal.modal('hide');
                                                    bootstrapNotify.success(data.html, '', {"closeButton": true})
                                                    dataTable.ajax.reload();
                                                } else {
                                                    bootstrapNotify.error(data.html, '', {"closeButton": true})
                                                }
                                            },
                                            //error messages
                                            error: function (data) {
                                                $('#preloader').hide();
                                                if (data.status == 422) {
                                                    global.generateErrorMessage(formInstance, data.responseJSON.errors);

                                                    if (data.responseJSON.errors.first_name !== undefined)
                                                        bootstrapNotify.error(data.responseJSON.errors.first_name, '', {"closeButton": true})

                                                    if (data.responseJSON.errors.last_name !== undefined)
                                                        bootstrapNotify.error(data.responseJSON.errors.last_name, '', {"closeButton": true})

                                                    if (data.responseJSON.errors.email !== undefined)
                                                        bootstrapNotify.error(data.responseJSON.errors.email, '', {"closeButton": true})

                                                    if (data.responseJSON.errors.phone !== undefined)
                                                        bootstrapNotify.error(data.responseJSON.errors.phone, '', {"closeButton": true})

                                                }

                                            }
                                        });

                                    });
                                });
                            }
                        });
                    });

                    editPopUp.on('hidden.bs.modal', function () {
                        $(this).remove();
                    });
                }
                function allowPermission(userId) {
                    $('#preloader').show();
                    var editPopUp = global.customPopup.show({
                        header: 'Allow Permission',
                        message: '',
                        dialogSize: 'lg',
                        dialogClass: 'prevent-to-show'
                    });

                    editPopUp.on('shown.bs.modal', function () {
                        var modal = $(this);

                        global.mySiteAjax({
                            url: route('admin.allow-permission-form'),
                            type: 'post',
                            data: {user_id: userId, _token: _token,},
                            loadSpinner: true,
                            success: function (response) {
                                $('#preloader').hide();
                                window.nbUtility.ajaxErrorHandling(response, function () {
                                    modal.find('.modal-body').html(response.html);
                                    modal.removeClass('prevent-to-show');

                                    $('#allow-permission-form').parsley().on('field:validated', function () {
                                        var ok = $('.parsley-error').length === 0;
                                        $('.bs-callout-info').toggleClass('hidden', !ok);
                                        $('.bs-callout-warning').toggleClass('hidden', ok);
                                    }).on('form:submit', function (formInstance) {
                                        $('.error-message').html('');
                                        $('#preloader').show();

                                        var values = $("#allow-permission-form").serializeArray();

                                        $.ajax({
                                            url: route('admin.allow-permission-form-submit'),
                                            type: 'post',
                                            data: values,
                                            loadSpinner: true,
                                            success: function (data) {
                                                $('#preloader').hide();
                                                if (data.status == 'success') {
                                                    $("#user-table").DataTable().ajax.reload();
                                                    modal.modal('hide');
                                                    bootstrapNotify.success(data.html, '', {"closeButton": true})
                                                    dataTable.ajax.reload();
                                                } else {
                                                    bootstrapNotify.error(data.html, '', {"closeButton": true})
                                                }
                                            },
                                            //error messages
                                            error: function (data) {
                                                $('#preloader').hide();
                                                if (data.status == 422) {
                                                    global.generateErrorMessage(formInstance, data.responseJSON.errors);

                                                    if (data.responseJSON.errors.first_name !== undefined)
                                                        bootstrapNotify.error(data.responseJSON.errors.first_name, '', {"closeButton": true})

                                                    if (data.responseJSON.errors.last_name !== undefined)
                                                        bootstrapNotify.error(data.responseJSON.errors.last_name, '', {"closeButton": true})

                                                    if (data.responseJSON.errors.email !== undefined)
                                                        bootstrapNotify.error(data.responseJSON.errors.email, '', {"closeButton": true})

                                                    if (data.responseJSON.errors.phone !== undefined)
                                                        bootstrapNotify.error(data.responseJSON.errors.phone, '', {"closeButton": true})

                                                }

                                            }
                                        });

                                    });
                                });
                            }
                        });
                    });

                    editPopUp.on('hidden.bs.modal', function () {
                        $(this).remove();
                    });
                }
                /****### -EDIT USER FUNCTION END- ###***/


                /****### -DEACTIVATE USER  FUNCTION START - ###***/
                /****###  deactivate an user using this function   ###***/
                function deactivateUser(userId) {

                    Swal.fire({
                        title: "Are you sure?",
                        text: "The user won't be able to login",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, deactivate this user!",
                        cancelButtonText: "No, cancel please!",
                        closeOnConfirm: false,
                        closeOnCancel: true
                    }).then(function(result) {
                        if (result.value) {
                            let dataObject = {
                                url: route('admin.deactivate-user'),
                                type: 'post',
                                data: {user_id: userId, _token: _token},
                                callback: user_operation_callback,
                                callBackError: callBackError,
                            }

                            globalAjax(dataObject);

                        }
                    });
                }
                /****### -DEACTIVATE USER FUNCTION END- ###***/



                /****### -ACTIVE USER  FUNCTION START - ###***/
                /****###  active an user using this function   ###***/
                function activeUser(userId) {

                    Swal.fire({
                        text: "The user will be able to login again",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, activate this user!",
                        cancelButtonText: "No, cancel please!",
                        closeOnConfirm: false,
                        closeOnCancel: true
                    }).then(function(result) {
                        if (result.value) {
                            let dataObject = {
                                url: route('admin.activate-user'),
                                type: 'post',
                                data: {user_id: userId, _token: _token},
                                callback: user_operation_callback,
                                callBackError: callBackError,
                            }

                            globalAjax(dataObject);

                        }
                    });
                }
                /****### -ACTIVE USER FUNCTION END- ###***/


                /****### -USER-DETAILS FUNCTION START - ###***/
                /****###  view user information using this function   ###***/
                function userDetails(userId) {

                    var editPopUp = global.customPopup.show({
                        header: 'User Information',
                        message: '',
                        dialogSize: 'lg',
                        dialogClass: 'prevent-to-show'
                    });

                    editPopUp.on('shown.bs.modal', function () {
                        var modal = $(this);

                        global.mySiteAjax({
                            url: route('admin.user-details'),
                            type: 'post',
                            data: {user_id: userId, _token: _token,},
                            loadSpinner: true,
                            success: function (response) {
                                window.nbUtility.ajaxErrorHandling(response, function () {
                                    modal.find('.modal-body').html(response.html);
                                    modal.removeClass('prevent-to-show');

                                });
                            }
                        });
                    });

                    editPopUp.on('hidden.bs.modal', function () {
                        $(this).remove();
                    });
                }

                function stopAutoRecurring(userId) {
                    Swal.fire({
                        title: "Are you sure?",
                        text: "Auto Recurring Will Be Stopped For This User",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, stop auto recurring!",
                        cancelButtonText: "No, cancel please!",
                        closeOnConfirm: false,
                        closeOnCancel: true
                    }).then(function(result) {
                        if (result.value) {
                            let dataObject = {
                                url: route('admin.stop-auto-recurring'),
                                type: 'post',
                                data: {user_id: userId, _token: _token},
                                callback: user_operation_callback,
                                callBackError: callBackError,
                            }

                            globalAjax(dataObject);

                        }
                    });
                }

                function startAutoRecurring(userId) {
                    Swal.fire({
                        title: "Are you sure?",
                        text: "Auto Recurring Will Be Started For This User",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, start auto recurring!",
                        cancelButtonText: "No, cancel please!",
                        closeOnConfirm: false,
                        closeOnCancel: true
                    }).then(function(result) {
                        if (result.value) {
                            let dataObject = {
                                url: route('admin.start-auto-recurring'),
                                type: 'post',
                                data: {user_id: userId, _token: _token},
                                callback: user_operation_callback,
                                callBackError: callBackError,
                            }

                            globalAjax(dataObject);

                        }
                    });
                }

                function packageAllocate(userId) {

                    var packagePopUp = global.customPopup.show({
                        header: 'Package Allocate',
                        message: '',
                        dialogSize: 'medium',
                        dialogClass: 'prevent-to-show'
                    });

                    packagePopUp.on('shown.bs.modal', function () {
                        var modal = $(this);

                        global.mySiteAjax({
                            url:  route('admin.package-allocate-view'),
                            type: 'post',
                            data: {user_id: userId, _token: _token,},
                            loadSpinner: true,
                            success: function (response) {
                                window.nbUtility.ajaxErrorHandling(response, function () {
                                    modal.find('.modal-body').html(response.html);
                                    modal.removeClass('prevent-to-show');

                                });
                            }
                        });
                    });

                    packagePopUp.on('hidden.bs.modal', function () {
                        $(this).remove();
                    });

                }

                /****### -DELETE-USER FUNCTION START - ###***/
                /****###  view user information using this function   ###***/
                function deleteUser(userId) {

                    Swal.fire({
                        title: "Are you sure?",
                        text: "You won't be able to retrieve this user!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete this user!",
                        cancelButtonText: "No, cancel please!",
                        closeOnConfirm: false,
                        closeOnCancel: true
                    }).then(function(result) {
                        if (result.value) {
                            let dataObject = {
                                url: route('admin.delete-user'),
                                type: 'post',
                                data: {user_id: userId, _token: _token},
                                callback: user_operation_callback,
                                callBackError: callBackError,
                            }
                            globalAjax(dataObject);
                        }
                    });

                }
                /****### -DELETE-USER FUNCTION END- ###***/


                /****### -LOGIN AS USER FUNCTION START - ###***/
                /****###  login to an user  using this function   ###***/
                function loginAsUser(userId) {
                    var url = route('admin.login-as-user',[userId]);
                    openTab(url);
                }

                /****### -USER-DETAILS FUNCTION END- ###***/

            })


            $(document).on('submit', '#package_allocate_form', function (e) {

                e.preventDefault(); // avoid to execute the actual submit of the form.

                Swal.fire({
                    title: "Are you sure to allocate free package?",
                    text: ".If user has any paid package then it will be overwritten by free package!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, Allocate it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }).then(function(result) {
                    if (result.value) {
                        $("#preloader").show();
                        $.ajax({
                            url: route('admin.package-allocate-in-user'),
                            type: 'post',
                            data: $('#package_allocate_form').serialize(),
                            loadSpinner: true,
                            success: function (data) {
                                $("#preloader").hide();
                                if (data.status == 'success') {
                                    $(".modal").modal('hide');
                                    toastr.success(data.html, '', {"closeButton": true})
                                    dataTable.ajax.reload();
                                } else if (data.status == 'error') {

                                    toastr.error(data.html, '', {"closeButton": true})
                                }

                            }

                        });
                    }
                });
            });

            /****### -START- ###***/
            /****### IT WILL OPEN A NEW TAB OR WINDOWS WITH BLINK ###***/
            function openTab(url) {
                // Create link in memory
                var a = window.document.createElement("a");
                a.target = '_blank';
                a.href = url;

                // Dispatch fake click
                var e = window.document.createEvent("MouseEvents");
                e.initMouseEvent("click", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
                a.dispatchEvent(e);
            };
            /****### -END- ###***/

        })(window, jQuery, _token);
    });

})();



