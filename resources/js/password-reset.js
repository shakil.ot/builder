$(function () {

    const _token = document.head.querySelector("[property=csrf-token]").content;
    (function (global, $, _token) {
        $("#reset-password").submit(function (e) {
            e.preventDefault();
            $('#preloader').show();
            $.ajax({
                url: route('reset-password-submit'),
                data: $('#reset-password').serialize(),
                type: "post",
                success: function (data) {
                    $('#preloader').hide();
                    if(data.status == 'error'){
                        toastr.error(data.html, '', {"closeButton": true})
                    }
                    if(data.status == 'success'){
                        toastr.success(data.html, '', {"closeButton": true})
                        setTimeout(function(){
                            window.location.href = route('login');
                        }, 2000);
                    }
                },
                //error messages
                error: function (data) {
                    $('#preloader').hide();
                    if (data.status == 422) {

                        if (data.responseJSON.errors.password !== undefined)
                            toastr.error(data.responseJSON.errors.password, '', {"closeButton": true})

                        if (data.responseJSON.errors.password_confirmation !== undefined)
                            toastr.error(data.responseJSON.errors.password_confirmation, '', {"closeButton": true})
                    }
                }
            });
        });

    })(window, jQuery, _token);
});
