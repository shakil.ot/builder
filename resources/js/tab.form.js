$(function () {

    var _token = $('input[name="_token"]').val();

    (function (global, $, _token) {


        var Form = new global.SingleForm({
            dom: $(".tab-holder")
        });

        var AjaxContent = new global.AjaxContent({
            dom: $('.change-content'),
            others: $(".change-content-section")
        });


        $('.open-change-content').on('click', function () {
            AjaxContent.back();  //On back button click at edit page.
        });


        //**** Content handling voice,image end****//
    })(window, jQuery, _token);
});
