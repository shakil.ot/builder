import React from 'react';
import ReactDOM from 'react-dom';
import Cart from './Cart/App';

function App() {
    return (
        <Cart/>
    );
}

ReactDOM.render(<App />, document.getElementById('app'))
