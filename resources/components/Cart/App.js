import React, {useState, useEffect} from "react";
import Loader from "./Loader";
import OutOfStock from "./OutOfStock";
import Category from "./Category";
import OrderDetails from "./OrderDetails";
import Product from "./Product";

function App() {
    const [state, setState] = useState({
        isLoading: true,
        currency: '$',
        brandName: '',
        openOrClose: '',
        discount: '',
        minimum_order: '',
        minimum_order_for_free: '',
        delivery_fee: '',
        tax: '',
        categories: [],

        cart: [],
    });

    useEffect(() => {
        loadData();
    }, []);

    const decreaseQuantity = (productId) => {
        let cart = state.cart;
        let removeItem = false;

        let newCart =  cart.map((product, i) => {
            if (product.id === productId) {
                if (product.quantity > 1) {
                    return {
                        ...product,
                        quantity: product.quantity - 1
                    }
                } else {
                    removeItem = product.id;
                }
            }
            return product;
        });

        if (removeItem) {
            newCart =  newCart.filter(item => item.id !== removeItem);
        }

        setState({
            ...state,
            cart: newCart
        });
    }

    const increaseQuantity = (productId) => {
        let cart = state.cart;
        let newCart =  cart.map((product, i) => {
            if (product.id === productId) {
                return {
                    ...product,
                    quantity: product.quantity + 1
                }
            }
            return product;
        });

        setState({
            ...state,
            cart: newCart
        });
    }

    const loadData = () => {
        setState({...state, isLoading: true});

        let formData = new FormData();
        formData.append('userId', userObject.userId);
        formData.append('_token', userObject._token);

        fetch(`shop/products`, {
            method: 'POST',
            body: formData,
        })
            .then(response => response.json())
            .then(data => {
                setState({
                    ...state,
                    isLoading: false,
                    categories: data.categories,
                    currency: data.setting.currency,
                    openOrClose: data.setting.open_or_closed,
                    brandName: data.setting.brand_name,
                    discount: data.setting.discount,
                    minimum_order: data.setting.minimum_order,
                    minimum_order_for_free: data.setting.minimum_order_for_free,
                    delivery_fee: data.setting.delivery_fee,
                    tax: data.setting.tax,
                });
            })
            .catch((error) => {
                console.error('Error:', error);
            });

    }

    const addToCart = (productId, productName, productPrice, productImage) => {
        let newItem = [...state.cart];

        if (newItem.length) {
            let isExist = false;
            newItem = newItem.map((existingItem, index) => {
                if (existingItem.id === productId) {
                    isExist = true;
                    return {
                        ...existingItem,
                        quantity: existingItem.quantity + 1
                    }
                } else {
                    return existingItem;
                }
            });
            if (!isExist) {
                newItem.push({
                    id: productId,
                    name: productName,
                    image: productImage,
                    price: productPrice,
                    quantity: 1
                })
            }
        } else {
            newItem.push({
                id: productId,
                name: productName,
                image: productImage,
                price: productPrice,
                quantity: 1
            })
        }

        setState({
            ...state,
            cart: newItem
        });
    }

    const renderCategoryWithItem = () => {
        return state.categories.map((category, i) => {
            return <Category
                key={i}
                products={category.products}
                categoryName={category.name}
                currency={state.currency}
                addToCart={addToCart}
            />;
        });
    }

    const renderPage = () => {
        if (state.isLoading) {
            return (<Loader/>);
        } else if(state.categories.length === 0) {
            return (<OutOfStock/>);
        } else {
            return (
                <div className="container">
                    <div className="row">
                        <div className="col-md-8">
                            <div className="my-5">
                                <h2>{state.brandName}</h2>
                                <span className={"label "+(state.openOrClose ? 'label-primary' : 'label-danger')+" label-inline font-weight-bolder mr-2"}>{state.openOrClose ? 'Open' : 'Close'}</span>
                            </div>
                            <div className="section__container">
                                {renderCategoryWithItem()}
                            </div>
                        </div>
                        <OrderDetails
                            initiateOrder={initiateOrder}
                            increaseQuantity={increaseQuantity}
                            decreaseQuantity={decreaseQuantity}
                            cart={state.cart}
                            currency={state.currency}
                            discount={state.discount}
                            minimum_order={state.minimum_order}
                            minimum_order_for_free={state.minimum_order_for_free}
                            delivery_fee={state.delivery_fee}
                            tax={state.tax}
                        />
                    </div>
                </div>
            );
        }
    }

    const isFormVerified = (name, number, address) => {

        if (name === '') {
            return {
                status: false,
                message: 'Name field is required'
            }
        } else if (number === '') {
            return {
                status: false,
                message: 'Number field is required'
            }
        } else if (address === '') {
            return {
                status: false,
                message: 'Address field is required'
            }
        } else {
            return {
                status: true
            }
        }
    }

    const initiateOrder = (name, number, address, instruction) => {
        if (!state.openOrClose) {
            bootstrapNotify.error('We are not taking order now. Please try again later!');
        }

        let verify = isFormVerified(name, number, address);
        if (verify.status) {
            let formData = new FormData();
            formData.append('shopId', userObject.shopId);
            formData.append('userId', userObject.userId);
            formData.append('cart', JSON.stringify(state.cart));
            formData.append('name', name);
            formData.append('number', number);
            formData.append('address', address);
            formData.append('instruction', instruction);
            formData.append('_token', userObject._token);

            fetch(`cart/order`, {
                method: 'POST',
                body: formData,
            })
                .then(response => response.json())
                .then(data => {
                    let href = '';
                    let isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
                        navigator.userAgent &&
                        navigator.userAgent.indexOf('CriOS') == -1 &&
                        navigator.userAgent.indexOf('FxiOS') == -1;
                    if (isSafari) {
                        href = "https://wa.me/" + data.number + "?text=" + data.message.toString();
                    } else {
                        href = "https://api.whatsapp.com/send?phone=" + data.number + "&text=" + data.message.toString();
                    }
                    window.open(href, '_blank');
                })
                .catch((error) => {
                    console.error('Error:', error);
                });
        } else {
            bootstrapNotify.error(verify.message);
        }
    }

    return (
        renderPage()
    );
}

export default App;
