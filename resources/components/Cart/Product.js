import React from "react";
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';

function Product(props) {
    return (
        <div className="col-md-4 pt-2">
            <div className="card">
                <div className="single__product">
                    <div className="product__image bg-white m-3">
                        <LazyLoadImage
                            alt={props.product.product_name}
                            effect="blur"
                            src={props.product.image} />
                    </div>
                    <div className="product__description p-3">
                        <p><strong>{props.product.product_name}</strong></p>
                        <p>{props.product.description.length < 60
                            ? props.product.description
                            : `${props.product.description.substring(0, 57)}...`}</p>
                    </div>
                    <div
                        className="product__bottom bg-success d-flex justify-content-between align-items-center p-2">
                        <div className="product__price text-white font-weight-bolder">
                            {props.currency} {(props.product.price).toFixed(2)}
                        </div>
                        <button className="btn btn-outline-secondary p-1" onClick={() => props.addToCart(props.product.id, props.product.product_name, props.product.price, props.product.image)}>
                            Add
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Product;
