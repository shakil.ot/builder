import React, {useEffect, useState} from "react";
import CartSection from "./CartSection";
import Product from "./Product";

function OrderDetails(props) {

    const [state, setState] = useState({
        name: '',
        number: '',
        address: '',
        subtotal: 0,
        discountPercent: props.discount,
        discountAmount: 0,
        afterDiscount: 0,
        deliveryCost: props.delivery_fee,
        tax: props.tax,
        totalInclTax: 0,
        instruction: '',
        isAbleToOrder: false
    });

    useEffect(() => {
        loadData();
    }, [props.cart]);

    const loadData = () => {
        let subTotal = 0;
        let discountAmount = 0;
        let totalInclTax = 0;
        props.cart.map((product, i) => {
            subTotal += product.price * product.quantity;
        });

        discountAmount = (subTotal / 100) * state.discountPercent;
        let afterDiscount = subTotal - discountAmount;
        totalInclTax = afterDiscount + ((afterDiscount / 100) * state.tax) + (props.minimum_order_for_free >= afterDiscount ? state.deliveryCost : 0) ;

        let isAbleToOrder = afterDiscount >= props.minimum_order;
        let deliveryCost = props.minimum_order_for_free > afterDiscount ? props.delivery_fee : 0;

        setState({
            ...state,
            subtotal: subTotal,
            totalInclTax: totalInclTax,
            discountAmount: discountAmount,
            deliveryCost: deliveryCost,
            isAbleToOrder: isAbleToOrder,
            afterDiscount: afterDiscount
        });
    }

    const makeFreeDelivery = () => {
        setState({
            ...state,
            deliveryCost : 0
        });
    }

    const onChangeHandle = (e) =>{
        const { name, value } = e.target;
        setState({
            ...state,
            [name] : value
        });
    }

    const showOrderDetailsInMobileDevices = (e) =>{
        e.preventDefault();
        let element = document.getElementById('mobile__slide_order');
        element.classList.add("open");
    }

    const hideOrderDetailsInMobileDevices = (e) =>{
        e.preventDefault();
        let element = document.getElementById('mobile__slide_order');
        element.classList.remove("open");
    }

    const getCartButton = () => {
        if (props.cart.length) {
            return (
                <a className="cart__btn_wrapper" href="#" onClick={(e) => showOrderDetailsInMobileDevices(e)}>
                    <div className="cart-button">
                        <div className="icon-cart" id="whatshopz-cart">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink"
                                 width="30" height="26" viewBox="0 0 49 48">
                                <g id="icomoon-ignore">
                                    <line stroke-width="1" stroke="#449FDB" opacity=""></line>
                                </g>
                                <path
                                    d="M9.405 42c0 1.657 1.342 3 3 3s3-1.343 3-3-1.342-3-3-3-3 1.343-3 3zM36.405 42c0 1.657 1.343 3 3 3s3-1.343 3-3-1.343-3-3-3-3 1.343-3 3zM0.318 4.5c0 0.828 0.672 1.5 1.5 1.5h3.117l1.863 7.968 2.607 13.032c0 0.108 0.051 0.201 0.063 0.306l-1.527 6.87c-0.099 0.444 0.009 0.909 0.294 1.263 0.285 0.357 0.714 0.561 1.17 0.561h34.032c0.828 0 1.5-0.672 1.5-1.5s-0.672-1.5-1.5-1.5h-32.16l0.687-3.090c0.15 0.024 0.285 0.090 0.441 0.090h27.345c1.656 0 2.655-0.327 3.453-2.25l4.872-14.946c0.846-2.679-1.011-3.804-2.67-3.804h-36c-0.234 0-0.438 0.081-0.657 0.132l-1.164-4.974c-0.159-0.678-0.762-1.158-1.461-1.158h-4.305c-0.831 0-1.5 0.672-1.5 1.5zM9.465 12h35.715l-4.788 14.697c-0.057 0.129-0.105 0.222-0.141 0.285-0.099 0.009-0.255 0.018-0.501 0.018h-27.345v-0.297l-0.057-0.291-2.883-14.412z"></path>
                            </svg>
                        </div>
                        <div className="whatshopz--cart_count">{props.cart.length}</div>
                    </div>
                </a>
            );
        }
    }

    return (
        <div className="col-md-4" id='mobile__slide_order'>
            <div className="my-9 text-center">
                <h2>Order Details</h2>
                <button className='order__close' onClick={(e) => hideOrderDetailsInMobileDevices(e)}>
                    X
                </button>
            </div>
            <div className="section__container">
                <div className="category__item">
                    <form id="cart__form" data-parsley-validate="">
                        <div className="card card-custom gutter-b">
                            <div className="card-body p-5">
                                <div className="row">
                                    <div className="col-md-12">
                                        <h3 className="font-size-lg text-dark-75 font-weight-bold">
                                            <span className="svg-icon svg-icon-order svg-icon-2x">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     xmlnsXlink="http://www.w3.org/1999/xlink" width="24px"
                                                     height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" strokeWidth="1" fill="none"
                                                       fillRule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"/>
                                                        <path
                                                            d="M5,10.5 C5,6 8,3 12.5,3 C17,3 20,6.75 20,10.5 C20,12.8325623 17.8236613,16.03566 13.470984,20.1092932 C12.9154018,20.6292577 12.0585054,20.6508331 11.4774555,20.1594925 C7.15915182,16.5078313 5,13.2880005 5,10.5 Z M12.5,12 C13.8807119,12 15,10.8807119 15,9.5 C15,8.11928813 13.8807119,7 12.5,7 C11.1192881,7 10,8.11928813 10,9.5 C10,10.8807119 11.1192881,12 12.5,12 Z"
                                                            fill="#000000" fillRule="nonzero"/>
                                                    </g>
                                                </svg>
                                            </span>
                                            Delivery Details
                                        </h3>
                                        <hr/>
                                        <div className="form-group">
                                            <label htmlFor="name" className="col-form-label">Name</label>
                                            <input className="form-control" type="text" name="name" value={state.name} onChange={onChangeHandle} id="name" placeholder="Enter your name"/>
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="number" className="col-form-label">Contact Number</label>
                                            <input className="form-control" type="text" name="number" value={state.number} onChange={onChangeHandle} id="number" placeholder="Enter contact number"/>
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="address" className="col-form-label">Address</label>
                                            <textarea
                                                value={state.address}
                                                onChange={onChangeHandle}
                                                className="form-control"
                                                name='address'
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="card card-custom gutter-b">
                            <div className="card-body p-5">
                                <div className="row">
                                    <div className="col-md-12">
                                        <h3 className="font-size-lg text-dark-75 font-weight-bold">
                                            <span className="svg-icon svg-icon-order svg-icon-2x">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     xmlnsXlink="http://www.w3.org/1999/xlink" width="24px"
                                                     height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" strokeWidth="1" fill="none"
                                                       fillRule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"/>
                                                        <path
                                                            d="M18.1446364,11.84388 L17.4471627,16.0287218 C17.4463569,16.0335568 17.4455155,16.0383857 17.4446387,16.0432083 C17.345843,16.5865846 16.8252597,16.9469884 16.2818833,16.8481927 L4.91303792,14.7811299 C4.53842737,14.7130189 4.23500006,14.4380834 4.13039941,14.0719812 L2.30560137,7.68518803 C2.28007524,7.59584656 2.26712532,7.50338343 2.26712532,7.4104669 C2.26712532,6.85818215 2.71484057,6.4104669 3.26712532,6.4104669 L16.9929851,6.4104669 L17.606173,3.78251876 C17.7307772,3.24850086 18.2068633,2.87071314 18.7552257,2.87071314 L20.8200821,2.87071314 C21.4717328,2.87071314 22,3.39898039 22,4.05063106 C22,4.70228173 21.4717328,5.23054898 20.8200821,5.23054898 L19.6915238,5.23054898 L18.1446364,11.84388 Z"
                                                            fill="#000000" opacity="0.3"/>
                                                        <path
                                                            d="M6.5,21 C5.67157288,21 5,20.3284271 5,19.5 C5,18.6715729 5.67157288,18 6.5,18 C7.32842712,18 8,18.6715729 8,19.5 C8,20.3284271 7.32842712,21 6.5,21 Z M15.5,21 C14.6715729,21 14,20.3284271 14,19.5 C14,18.6715729 14.6715729,18 15.5,18 C16.3284271,18 17,18.6715729 17,19.5 C17,20.3284271 16.3284271,21 15.5,21 Z"
                                                            fill="#000000"/>
                                                    </g>
                                                </svg>
                                            </span>
                                            Cart
                                        </h3>
                                        <hr/>
                                        {props.cart.length ?
                                            <CartSection
                                                increaseQuantity={props.increaseQuantity}
                                                decreaseQuantity={props.decreaseQuantity}
                                                cart={props.cart}
                                                currency={props.currency}
                                                subTotal={state.subtotal}
                                                minimum_order={props.minimum_order}
                                                minimum_order_for_free={props.minimum_order_for_free}
                                                afterDiscount={state.afterDiscount}
                                                discountPercent={state.discountPercent}
                                                discountAmount={state.discountAmount}
                                                deliveryCost={state.deliveryCost}
                                                tax={state.tax}
                                                totalInclTax={state.totalInclTax}
                                                makeFreeDelivery={makeFreeDelivery}
                                            /> :
                                            <h3 className="text-center">Cart is Empty</h3>
                                        }

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="card card-custom gutter-b">
                            <div className="card-body p-5">
                                <div className="row">
                                    <div className="col-md-12">
                                        <h3 className="font-size-lg text-dark-75 font-weight-bold">
                                            Special instructions (optional)
                                        </h3>
                                        <hr/>
                                        <div className="cart__section">
                                            <input className="form-control" name="instruction" id="special_instruction" value={state.instruction} onChange={onChangeHandle} placeholder="Enter spacial instruction"/>
                                            <small>We'll never share your details with anyone else.</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="form-group mt-10">
                            <button type="button" disabled={!state.isAbleToOrder} className="btn btn-order font-weight-bolder w-100" onClick={() => props.initiateOrder(state.name, state.number, state.address, state.instruction)}>
                                <img width="30" className='mr-2' src="assets/images/logo.png" alt=""/>
                                Order on whatsapp
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            {getCartButton()}
        </div>
    );
}

export default OrderDetails;
