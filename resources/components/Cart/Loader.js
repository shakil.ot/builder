import React from "react";
import { Preloader, Grid } from 'react-preloader-icon';


function Loader() {
    return (
        <Preloader
            use={Grid}
            size={85}
            strokeWidth={6}
            strokeColor="#10BC41"
            duration={2000}
        />
    );
}

export default Loader;
