import React from "react";
import Product from "./Product";

function CartSection(props) {

    const renderCartItem = () => {
        return props.cart.map((product, i) => {
            return (<div className="item__section w-100 d-flex justify-content-between align-items-center mb-3">
                        <div className="item__information d-flex align-items-center">
                            <img className='item__image' width="50" height="50" src={product.image} alt={product.name}/>
                            <div className="product__description">
                                <p className="m-0"><strong>{product.name}</strong></p>
                                <p className="m-0">{props.currency} {(product.price).toFixed(2)}</p>
                            </div>
                        </div>
                        <div className="item__count d-flex">
                            <button className="value-button" type="button" onClick={() => props.decreaseQuantity(product.id)}>-</button>
                            <input className="form-control" type="text" id="number" value={product.quantity}/>
                            <button className="value-button" type="button" onClick={() => props.increaseQuantity(product.id)}>+</button>
                        </div>
                    </div>
            );
        });
    }

    const getCartStatus = () => {
        if (props.minimum_order > props.afterDiscount) {
            return (
                <div className="alert alert-secondary" role="alert">
                    Minimum order is <strong>{props.currency} {props.minimum_order}</strong> for Delivery. Add <strong>{props.currency} {(props.minimum_order - props.afterDiscount).toFixed(2)}</strong> to place your order
                </div>
            );
        } else if (props.minimum_order_for_free > props.afterDiscount) {
            return (
                <div className="alert alert-secondary" role="alert">
                    Add more <strong>{props.currency} {(props.minimum_order_for_free - props.afterDiscount).toFixed(2)}</strong> to avail free delivery.
                </div>
            );
        } else {
            if (props.deliveryCost !== 0) {
                props.makeFreeDelivery();
            }
            return '';
        }
    }

    return (
        <div className="cart__section">
            {renderCartItem()}
            <hr/>
            <div className="calculation__section">
                <table className="table">
                    <tbody>
                    <tr>
                        <td className="title">Subtotal</td>
                        <td className="value">{props.currency} {(props.subTotal).toFixed(2)}</td>
                    </tr>
                    <tr>
                        <td className="title">Discount({props.discountPercent}%)</td>
                        <td className="value">{props.currency} {(props.discountAmount).toFixed(2)}</td>
                    </tr>
                    <tr>
                        <td className="title">Delivery</td>
                        <td className="value">{props.currency} {props.deliveryCost ? props.deliveryCost : 'Free'}</td>
                    </tr>
                    <tr>
                        <td className="title">Total Incl Tax
                        </td>
                        <td className="value">{props.currency} {(props.totalInclTax).toFixed(2)}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            {getCartStatus()}
        </div>
    );
}

export default CartSection;
