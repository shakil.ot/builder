import React from "react";
import Product from "./Product";

function Category(props) {
    const renderProduct = () => {
        if (props.products.length) {
            return props.products.map((product, i) => {
                return <Product
                    key={i}
                    product={product}
                    currency={props.currency}
                    addToCart={props.addToCart}
                />;
            });
        } else {
            return (
                <div className="col-md-12 pt-2">
                    <div className="card">
                        <h2 className='text-center p-5'>No product in this category!</h2>
                    </div>
                </div>
            );
        }

    }

    return (
        <div className="category__item mb-15">
            <div className="card card-custom gutter-b bg-transparent">
                <div className="card-header bg-success">
                    <div className="card-title">
                        <h3 className="card-label text-white font-weight-bolder">
                            {props.categoryName} ({props.products.length})
                        </h3>
                    </div>
                </div>
                <div className="card-body p-0 pt-7">
                    <div className="row">
                        {renderProduct()}
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Category;
