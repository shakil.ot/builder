import React from "react";
import './out_of_stock.css'

function OutOfStock() {
    return (
        <div className="error error-4 d-flex flex-row-fluid bgi-size-cover bgi-position-center" style={{backgroundImage: "url('/assets/theme/media/error/bg4.jpg')"}}>
            <div className="d-flex flex-column flex-row-fluid align-items-center align-items-md-start justify-content-md-center text-center text-md-left px-10 px-md-30 py-10 py-md-0 line-height-xs">
                <h3 className="error-title text-info font-weight-boldest line-height-sm">Sorry!</h3>
                <p className="error-subtitle text-info font-weight-boldest mb-10">Out of stock!</p>
                <p className="display-4 font-weight-boldest mt-md-0 line-height-md">Stay with us, we will be back soon</p>
            </div>
        </div>
    );
}

export default OutOfStock;
